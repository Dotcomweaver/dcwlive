<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Eleven
*/
get_header(); ?>
<section class="inner-header-work">
    	<div class="container">
        	<div class="row">
            	
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					} ?>
                  <!--<span class="fa fa-cog"></span>
               	  <h1><?php the_title(); ?></h1>-->
                <h1> <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
              </div>
          </div>
        </div>
</section>
<section id="secondnav" class="work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="navbar secondary-nav navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".second-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="#">Navigation</a>
                    </div>
                    <div class="navbar-collapse second-nav collapse">
                      <?php
                        wp_nav_menu( array(
                                              'menu' => 'work',
                                              'depth' => 2,
                                              'container' => false,
                                              'menu_class' => 'nav navbar-nav',
                                              //Process nav menu using our custom nav walker
                                              'walker' => new wp_bootstrap_navwalker()
											)
                                     );
                     
                    ?>
                   </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="light-gray-wraper divider video-testimonials">
  <div class="container" id="container-blog">
  		<div class="row">
            	<div class="blog-filter">
                </div><!-- blog-filter -->
        </div>
        <div id="posts" class="row">
        <?php
		//$query = array( 'post_type' => 'video-testimonials', 'posts_per_page'=>'12' , 'post_status' => array('publish'));
		$args = array(  'post_type' => 'video-testimonials',
						'post_status' => 'publish',
						'posts_per_page' => -1,
						'nopaging' => true
					);
		
        query_posts( $args );
		if ( have_posts() ) : 
			while ( have_posts() ) : 
				the_post();
				$post_id = $post->ID;
				$title = $post->title;
				$content = get_the_content();
				$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
				//var_dump($v_test_post_meta->vt_categories);
				if($v_test_post_meta->vt_url != ''): 
					/*$i = 0; 
					$cat_id=''; 
					foreach($v_test_post_meta->vt_categories as $category_id)
					{   
						$cat_id[$i] = $category_id; 
						$i++; 
					}*/
					$y_img = get_ytube_video_code($v_test_post_meta->vt_url);
					$te = $v_test_post_meta->vt_url;
					$start = strpos($te,"v=")+2;
					$end = strpos($te,"&");
					if( $end == "" || $end == 0 ){
						$fstr = substr($te,$start);
					} else {
						$length = $end-$start;
						$fstr = substr($te,$start,$length);
					}
					//echo $fstr;
		?>
        	<div class="item testimonails-video col-md-4 col-sm-6" id="post-<?php the_ID(); ?>">
                <div class="blog-box">
                	<div class="blog-box-inner">
                    	<?php if($v_test_post_meta->vt_url != ''):?>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                    	<img  alt="<?php the_title() ?> - Testimonial for NJ Web Design company" class="img-responsive" src="http://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" >
                                        <!-- Play Button -->
                                        <a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/<?php echo trim($fstr); ?>" class="hvideo vt_play" href="javascript:void();">
                                            <span class="icomoon-icon-play-2"></span>
                                        </a>
                                    </div>
                                </div>
                        <?php endif; ?>
                    	<div class="row">
                            <div class="col-xs-6 col-md-12 col-lg-6">
                            	<span class="icomoon-icon-clock"></span>
								<?php the_date('M j, Y'); ?>
                            </div>
                            <div class="col-xs-6 col-md-12 col-lg-6 text-right">
                            	<span class="icomoon-icon-user"></span>
								<?php the_author();?>
                            </div>
                        </div>
                        <hr>
                        <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                        <div class="info <?php if(strlen($content)>210): //503?> hght190 <?php endif;?>">
							<?php echo $content; ?>
                        </div><!-- .entry-content -->
                        <?php //echo strlen($content); ?>
                        <?php if(strlen($content)>210): //503?>
                        <a class="blog-but readmorevt" href="javascript:void(0)" title="Permanent Link to <?php the_title_attribute(); ?>">Read More »</a>
                        <?php endif; ?>
                    </div>
                </div>
             </div>
         <?php 
		 		endif; 
			endwhile; // end of the loop. ?>
            
			<?php else: ?>
				<p>Sorry, no posts matched your criteria.</p>
		<?php endif; ?>
		
        <?php //if(function_exists('wp_paginate')) {
                                              //  wp_paginate();
                                              //  } 
            ?>
        </div>
  </div>
</section>
<?php get_footer(); ?>