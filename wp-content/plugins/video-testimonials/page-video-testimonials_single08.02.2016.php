<?php

    // calling the header.php
    get_header();

?>

<!-- Start the Loop. -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>

<?php
$post_id = $post->ID;
$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
$ytube_code = get_ytube_video_code($v_test_post_meta->vt_url);
$ytube_url = 'http://www.youtube.com/embed/'.$ytube_code;
//print_r($v_text_post_meta);
?>

  <iframe id="ytplayer" type="text/html" width="650" height="495" src="<?php echo $ytube_url; ?>?autoplay=1" frameborder="0"/></iframe>

  <!-- Stop The Loop (but note the "else:" - see next line). -->
  <?php endwhile; else: ?>

    <!-- The very first "if" tested to see if there were any Posts to -->
    <!-- display.  This "else" part tells what do if there weren't any. -->
    <p>Sorry, no posts matched your criteria.</p>

<!-- REALLY stop The Loop. -->
<?php endif;

    	// calling footer.php
    	get_footer();
?>

