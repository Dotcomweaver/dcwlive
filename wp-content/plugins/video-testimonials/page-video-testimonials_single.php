<?php
/**
 * The Template for displaying all Single video-testimonials posts.
 */
    // calling the header.php
    get_header();

?>
<div class="ecommerce clearfix">
			<div class="header-content">
				<div class="header-content-inner">
				 <h1>Video Testimonials</h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/client-cion.png"></div>
<br />
<div class="clearfix"></div>
<!-- Start the Loop. -->
<div class="container video-singlepage">
<div class="row">
<div class="vline"></div>
<div class="clearfix"></div>

  <a href="/video-testimonials/" class="back_article"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to the Videos</a>
    <div class="col-sm-9">
    <div class="blog-box-new">                
  
        
    <?php if ( is_search() ) :?>
        <div class="info">
          <?php 
             the_excerpt(); ?>
         </div>
    <?php 
  else: ?>
          
            <h2><?php echo the_title();?></h2>
          <div class="single_blogimg">

          <?php
            $post_id = $post->ID;
            $v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
            $ytube_code = get_ytube_video_code($v_test_post_meta->vt_url);
            $ytube_url = 'https://www.youtube.com/embed/'.$ytube_code;
            //print_r($v_text_post_meta);
            ?>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" id="ytplayer" type="text/html" src="<?php echo $ytube_url; ?>?autoplay=0" frameborder="0"/></iframe>
            </div>
         </div>
            <div class="blog_meta_data row">
                    <div class="col-sm-6">by <?php the_author();?></div>
                    <div class="col-sm-6 text-right">
                   <?php 
                   
            //query_posts( 'posts_per_page=6&orderby=rand' );
               $post_type = 'video-testimonials';

 
// Get all the taxonomies for this post type
                $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
                 //print_r($taxonomies);
                //$obj = get_queried_object();
                 //print_r($obj);
                   //$selcomname = $obj->name;
            foreach( $taxonomies as $taxonomy ) :
                $terms = get_terms( $taxonomy );
                 //$terms = get_terms( $taxonomy );
            if($terms):
                  ?>
                <?php 
                $i= 0;
                    foreach( $terms as $term) {
                       $term1 = $term->name;
                      //  print_r($term);
                                      
             if($i>0){
                echo '|';
             }
             echo '<a href="' . get_tag_link ($term->term_id) . '" rel="tag">' . $term1. '  </a>';  $i++;
                 }  ?> |
            <?php endif; ?>  <?php the_date('M j, Y'); ?></div>
        <?php endforeach;?>
                </div>

            
           <?php the_content(); ?>
           <hr />
      
            <div class="post_social_share text-right">
                <?php dynamic_sidebar('share');?>
                 <span> Share the article </span> 
                
            </div>
         
    <?php endif; 
  //wp_reset_postdata();?>
      <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_sharing_toolbox"></div>
        <div class="post-tags">
            
        </div>
                <div class="single-post-comments row">
            <div class="col-sm-12">
            <?php
                wp_reset_query();
                if ( comments_open()) :
            ?>
            <?php 
                    comments_template( 'comments.php', true ); 
            ?>
            <?php 
                endif; 
            ?>
            </div>
        </div>
        <div class="clearfix"></div>
        
      </div>
      
    </div>
    <div class="col-sm-3 ind-blog-4">
        <div class="blog_sidebar">
          <?php dynamic_sidebar('search');?>

          <div class="clearfix"></div>
          <div class="sidebar_header">
              <h4> Video Categories </h4>
              <hr />
          </div>
                 <ul id="tags-list">
                <?php

                $post_type = 'video-testimonials';
                 
                // Get all the taxonomies for this post type
                $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
                 
                foreach( $taxonomies as $taxonomy ) :
                 
                    // Gets every "category" (term) in this taxonomy to get the respective posts
                    $terms = get_terms( $taxonomy );

                 
                    foreach( $terms as $term ) :
                         echo '<li><a href="' . get_tag_link ($term->term_id) . '" rel="tag">' . $term->name . '  </a></li>';
                     ?>
                   
                <?php endforeach;
                endforeach;?>
                </ul>
                           
           <div class="clearfix"></div>

            <div class="blog_video_gallery">
        <div class="sidebar_header">
            <h4>Video Gallery</h4>
            <hr />
        </div>

        
    <?php
$queryObject = new WP_Query( 'post_type=video-testimonials&posts_per_page=3' );
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul>
    <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        $post_id = $post->ID;
        
$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
if($v_test_post_meta->vt_url != '') {
                    
                    $y_img = get_ytube_video_code($v_test_post_meta->vt_url);
                    $te = $v_test_post_meta->vt_url;
                    $start = strpos($te,"v=")+2;
                    $end = strpos($te,"&");
                    if( $end == "" || $end == 0 ){
                        $fstr = substr($te,$start);
                    } else {
                        $length = $end-$start;
                        $fstr = substr($te,$start,$length);
                    }
                    //print_r($v_test_post_meta);
                     ?>
                        <li>
                     <?php if($v_test_post_meta->vt_url != ''):?>
                            <div class="video_img">
                            <img  alt="<?php the_title() ?> - Testimonial for NJ Web Design company" class="img-responsive" src="https://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" >
                             <!-- Play Button -->
                            <a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/<?php echo trim($fstr); ?>" class="vt_play2" href="javascript:void();">
                                <span class="flaticon-play-button4"></span>
                            </a>
                            </div>
                        <?php endif; ?>
                            <!-- <a href="<?php //the_permalink(); ?>"> --><?php the_title(); ?><!-- </a> -->
                         </li>
    <?php
    }
}
    ?>
    </ul>
    
    <?php
}
?>
<div class="clearfix"></div>
        <a href="/video-testimonials/" class="btn btn-default">View All</a>
    </div>
            <div class="clearfix"></div>
            
            <div class="sidebar_header">
                <h4>Instagram</h4>
                <hr />
            </div>

            <div class="clearfix"></div>
                <div class="instagram_gallery">
                    <?php do_shortcode('[wp-instagram-gallery]');?>
                </div>

            <div class="clearfix"></div>
            <br />

            <div class="clearfix"></div>

            <div class="related_posts">
                <div class="sidebar_header">
                    <h4>Blog</h4>
                    <hr />
                </div>

                  <?php 
                    $args = array( 'numberposts' => 3, 'post_status'=>"publish",'post_type'=>"post",'orderby'=>"post_date");
                    $postslist = get_posts( $args );
                     $title1 = get_the_title();
                    echo '<ul>';
                   
                     foreach ($postslist as $post) :  setup_postdata($post); ?>
                     <li> 
                     
                        <?php
                            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            if(!empty($feat_image)):
                        ?>
                            <div class="video_img">
                                 <img src="<?php echo $feat_image; ?>" class="img-responsive" />
                            </div>
                        <?php
                            endif;
                        ?>
                         <?php echo strlen($title1); ?>
                        <?php if(strlen($title1)>20){ //503?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"> <?php echo trim(substr(get_the_title(), 0, 100)).'...'; ?></a>
                        <?php } else{ ?>
                         <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"> <?php echo get_the_title(); ?></a>
                         <?php }?>
                         
                </li>
                <?php endforeach; ?>
                 </ul>

                <div class="clearfix"></div>
                <a href="/blog/" class="btn btn-default">View All</a>
            </div>

            <div class="clearfix"></div>
            <br />

            <div class="sidebar_social">
                <div class="sidebar_header">
                    <h4>Social</h4>
                    <hr />
                </div>

                <ul class="social_icons">
                    <li> <a href="https://www.facebook.com/dotcomweavers" target="_blank" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                    <li> <a href="http://twitter.com/Dotcomweavers" target="_blank" class="twitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                    <li> <a href="https://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="linkedin"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                    <!-- <li> <a href="https://www.pinterest.com/dotcomweaversnj/" class="pinterest"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li> -->
                    <li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="google-plus"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                </ul>
            </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div>
<div>
<div class="clearfix"> </div>
<div class="portfolio_inner_footer text-center">
  <div class="container">
    <div class="col-sm-4">
      <?php previous_custom_post(); ?>
    </div>
    <div class="col-sm-4"><a href="/video-testimonials/">  BACK TO OUR VIDEO GALLERY</a></div>
    <div class="col-sm-4">
          <?php next_custom_post(); ?>
    </div>
  </div>
</div>
    	<?php get_footer();
?>
<style>
header {display: none;}


</style>

