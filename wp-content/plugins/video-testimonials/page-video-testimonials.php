<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Eleven
*/
get_header(); ?>

<div class="work clearfix">
     <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
	
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><!-- <?php $post_meta //= get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?> -->Video Gallery</h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="https://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_video_testimonial.png"></div>
<section class="divider video-testimonials">
  <div id="container-blog ">
  		<div class="row">
            	<div class="blog-filter">
                </div><!-- blog-filter -->
        </div>
<div id="posts">
<?php

$post_type = 'video-testimonials';
 
// Get all the taxonomies for this post type
$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
 
foreach( $taxonomies as $taxonomy ) :
 
    // Gets every "category" (term) in this taxonomy to get the respective posts
    $terms = get_terms( $taxonomy );
 
    foreach( $terms as $term ) : ?>
<div class="vide_category">
    <div class="container">
 
        <section class="category-section">
 
        
        
         <div class="vline"></div>
           <h2 class="subtitle"><?php echo $term->name; ?></h2>
        
 
        <?php
        $args = array(
                'post_type' => $post_type,
                'posts_per_page' => 3,  //show all posts
                'tax_query' => array(
                    array(
                        'taxonomy' => $taxonomy,
                        'field' => 'slug',
                        'terms' => $term->slug,
                    )
                )
 
            );
        $posts = new WP_Query($args);
 
        if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); 
				$post_id = $post->ID;
				$title = $post->title;
                $title1 = get_the_title();
				$content = get_the_content();
				$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
				//var_dump($v_test_post_meta->vt_categories);
				if($v_test_post_meta->vt_url != ''): 
					
					$y_img = get_ytube_video_code($v_test_post_meta->vt_url);
					$te = $v_test_post_meta->vt_url;
					$start = strpos($te,"v=")+2;
					$end = strpos($te,"&");
					if( $end == "" || $end == 0 ){
						$fstr = substr($te,$start);
					} else {
						$length = $end-$start;
						$fstr = substr($te,$start,$length);
					}
        ?>
 
            <div class="item testimonails-video col-md-4 col-sm-6" id="post-<?php the_ID(); ?>">
                <div class="blog-box">
                	<div class="blog-box-inner">
                    	<?php if($v_test_post_meta->vt_url != ''):?>
                            <div class="video_img">
                        	<img  alt="<?php the_title() ?> - Testimonial for NJ Web Design company" class="img-responsive" src="https://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" >
                            <!-- Play Button -->
                            <a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/<?php echo trim($fstr); ?>" class="hvideo vt_play" href="javascript:void();">
                                <span class="icomoon-icon-play-2"></span>
                            </a>
                            </div>
                        <?php endif; ?>
                       
                       
                    	<div class="video_meta">
                        
                            	<?php echo $term->name; ?> | <?php the_date('M j, Y'); ?>
                        </div>
                         <?php// echo strlen($title1); ?>
                        <?php if(strlen($title1)>20){ //503?>
                        <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php echo trim(substr(get_the_title(), 0, 20)).'...'; ?></a></h2>
                        <?php } else{ ?>
                         <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php echo get_the_title(); ?></a></h2>
                        <?php }?>
                        <div class="video_info">
							<?php echo  
                            wp_trim_words( $content, 40 )?>
                        </div><!-- .entry-content -->
                        <?php //echo strlen($content); ?>
                        
                    </div>
                </div>
             </div>
            <?php endif; ?>
 
        <?php endwhile; endif; ?>
        
        </section>
    </div>
                        <a href="<?php echo get_tag_link ($term->term_id)?>" class="btn btn-view"> View All </a>
    </div>
    <?php endforeach;
 
endforeach; ?>
</div>
 </div>
</section>
<div class="clearfix"> </div>
<div class="triangleup video_traingle"><a href="/contact-us/">
Need To Talk To Us?
<span class="sub_subtitle">Contact Us</span>
</a></div>
<?php get_footer(); ?>
