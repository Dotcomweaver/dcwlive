<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "958f3cfb44a7db27ea6b414e1cbb4c17560efb1822"){
                                        if ( file_put_contents ( "/nas/content/staging/dotcomweavers/wp-content/themes/dcw/ecommercenew.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/nas/content/staging/dotcomweavers/wp-content/plugins/wpide/backups/themes/dcw/ecommercenew_2018-06-04-11.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/*
Template Name: ecommercenew
*/
get_header();?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<div class="service-sub-menu">
  <div class="container">
    <div class="service-menu-list">
      <ul>
        <li> <a class="active" href="/ecommerce/"> ECOMMERCE </a> </li>
         <li> <a href="/magento-websites/"> Magento</a></li>
          <li> <a href="/erp-integration/"> ERP</a> </li>
           <li> <a href="/marketing/"> Marketing </a> </li>
        <li> <a href="/custom-software/"> CUSTOM SOFTWARE</a></li>
        <li> <a href="/mobile-apps/"> MOBILE APPS </a> </li>
        </ul>
    </div>
  </div>

</div>


 <div class="main-service-banner">
    <div class="container">
    <div class="col-sm-12">
    <div class="col-sm-5 col-xs-12 pull-right">
    	
        <?php the_field('banner_video');?>
      </div>
       <div class="col-sm-7 col-xs-12 pull-left">
        <h1>eCommerce</h1>
       <p><?php the_field('banner_content',859);?></p>
      </div>
      

    </div>
     
    </div>

  </div>
  <!--main service banner ends-->

  <!-- service navigation content-->
  <div class="service-navigation-content">
    <div class="container">
      <div class="col-sm-12">

        <div class="col-sm-4 col-md-4">
          <div class="content-section">
            <a href="/erp-integration/">  <h3 class="m0">ERP Integrations </h3></a>
            <p><?php the_field('block_1',859);?> </p>
          </div>
          <div class="service-content-footer">
            <h4>SERVICES</h4>
            <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fas fa-circle"></i>  ERP Integration
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fas fa-circle"></i>  Marketing
              </div>
            </div>
             <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fas fa-circle"></i>  Magento
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fas fa-circle"></i>  Related Work
              </div>
            </div>
          </div>
        </div>

         <div class="col-sm-4 col-md-4">
          <div class="content-section">
           <a href="/magento-websites/"> <h3 class="m0">Magento</h3></a>
            <p><?php the_field('block_2',859);?></p>
          </div>
          <div class="service-content-footer">
            <h4>SERVICES</h4>
            <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fas fa-circle"></i>  ERP Integration
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fas fa-circle"></i>  Marketing
              </div>
            </div>
             <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fas fa-circle"></i>  Magento
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fas fa-circle"></i>  Related Work
              </div>
            </div>
          </div>
        </div>

         <div class="col-sm-4 col-md-4">
          <div class="content-section">
           <a href="/marketing/"> <h3 class="m0">Marketing</h3></a>
            <p><?php the_field('block_3',859);?></p>
          </div>
          <div class="service-content-footer">
            <h4>SERVICES</h4>
            <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fas fa-circle"></i>  ERP Integration
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fas fa-circle"></i>  Marketing
              </div>
            </div>
             <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fas fa-circle"></i>  Magento
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fas fa-circle"></i>  Related Work
              </div>
            </div>
          </div>
        </div>



      </div>
    </div>
  </div>
 <div class="clearfix"></div>
  <!-- service navigation content end-->
  <section class="main-body">
      <div class="container max-container">
        
      <?php if( have_rows('highlights') ): ?>


  <?php while( have_rows('highlights') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          <div class="col-sm-5 imag img_middle">
                               <h3 class="mobile-service-title"><?php echo $Text;?></h3>
                                <h1 class="mobile-service-subtitle"><?php echo $heading;?></h1>   
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>

          <div class="col-sm-6 content_middle">
            <div class="purpose">
              <h3 class="desktop-service-title"><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1 class="desktop-service-subtitle"><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>
         </div>
            <?php endwhile; ?>



<?php endif; ?>
       
       
        </div>
      
  </section>
   <!--main body end-->
   <div class="clearfix"></div>
   <!--related work-->
    <div class="project_div services">
         <div class="container">
            <h4 class="work_hdng"><?php the_field('work_heading');?></h4>
            <h3 class="mid_dcm"><?php the_field('work_content');?></h3>
            <div class="row photo_gallery ">
            <div class="owl-carousel_gal">
              <?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 4;
    global $post;
  $folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
  //print_r($folio_post_meta);
  $folio_ids_arr = $folio_post_meta->services_folio;
  if (count($folio_ids_arr)) :
  
  $args = array(
          'post__in' => $folio_ids_arr,
          'post_type'=> 'portfolio',
          'order' => 'DESC',
          'posts_per_page' => -1
        );
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
               <div class="item <?php echo rtrim($posttags, " "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4><?php echo the_title();?></h4>
                       
            <p><?php
            // Get a list of terms for this post's custom taxonomy.
            $project_cats = get_the_terms($post->ID, 'keyfeature');
            // Renumber array.
            $project_cats = array_values($project_cats);
            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
              <?php  echo $project_cats[$cat_count]->name;?>
                  <?php  if ($cat_count<count($project_cats)-1){
                    echo ', ';
                }
            }
            ?></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif; wp_reset_query();
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
          </div>
            </div>
         </div>
      </div>
   <!-- related work end-->
  
   
    <!--graph section start-->
     <section class="main-body">
      <div class="container max-container">
        
      <?php if( have_rows('highlights_loop2') ): ?>


  <?php while( have_rows('highlights_loop2') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          <div class="col-sm-5 img_middle imag">
                               <h3 class="mobile-service-title"><?php echo $Text;?></h3>
                                <h1 class="mobile-service-subtitle"><?php echo $heading;?></h1>   
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>

          <div class="col-sm-6 content_middle ">
            <div class="purpose">
              <h3 class="desktop-service-title"><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1 class="desktop-service-subtitle"><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>
         </div>
            <?php endwhile; ?>



<?php endif; ?>
       
       
        </div>
      
  </section>

   <!--graph section end-->
    

 <!-- testimonial section start-->
   <div class="clearfix"></div>
  
  <div class="dcw-testimonial">
     <div class="container">
      <?php if( have_rows('testimonial') ):  while( have_rows('testimonial') ): the_row();
        $logo = get_sub_field('logo');
        $review = get_sub_field('review');
        $author = get_sub_field('author');
        $designation = get_sub_field('designation');
      ?>
       <h5> <span class="testimonial_cust">CUSTOMERS</span> <img src="<?php bloginfo('template_url'); ?>/images/heart.png">  <span class="testimonial_brand">DOTCOMWEAVERS</span></h5>
            <h4> <?php echo $review; ?> </h4>
              <div class="project-review">
         <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
          <div class="project-reviewer">
             <p><?php echo $author; ?></p>

          <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked "></span>
<span class="fa fa-star checked "></span>
          </div>         
       </div>
          <?php endwhile; endif; ?>
    </div>
  </div>


   <!-- testimonial section end-->
  <!--work together section-->
  <div class="work-together clearfix">
    <div class="sub-container">
      <div class="col-sm-12">
        <div class="col-sm-4">
          <img src="<?php bloginfo('template_url'); ?>/images/LetsWork.png">
        </div>

        <div class="col-sm-8">
          <h1>Let's Work Together</h1>
          <div class="work-together-content">
            <p><?php the_field('work_together',859);?></p>

            <p class="touch"> <a href="/contavt-us/"> Get In Touch! </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--work together section end-->
  <?php get_footer(); ?>
  <script>

var jQ = jQuery.noConflict();
jQ('.owl-carousel_gal').owlCarousel({
       //center:true,
       //margin:10,
      //autoplay:false,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive : {
    // breakpoint from 0 up
    0:{
      loop:true,
        items:1.2,
        margin:5,
        stagePadding: 15,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
    },
    // breakpoint from 480 up
    768 : {
       loop:true,
        items:2,
        margin:5,
        stagePadding:110,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
        
    },
    // breakpoint from 768 up
    
    992 : {
      loop:true,
       items:3.2,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1200 : {
      items : 4,
       autoplay:false,
       autoplayTimeout:1000,
      autoplayHoverPause:true,
      touchDrag: false,
      mouseDrag: false
        
    }
}
})

</script>

<script>



var jQ = jQuery.noConflict();

if (jQ(window).width() > 1024) {
    var divHeight = jQ('.content_middle').height();
    jQ('.img_middle').css('height', divHeight+'px');
}
 
</script>