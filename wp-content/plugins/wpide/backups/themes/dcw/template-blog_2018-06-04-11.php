<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "958f3cfb44a7db27ea6b414e1cbb4c17560efb1822"){
                                        if ( file_put_contents ( "/nas/content/staging/dotcomweavers/wp-content/themes/dcw/template-blog.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/nas/content/staging/dotcomweavers/wp-content/plugins/wpide/backups/themes/dcw/template-blog_2018-06-04-11.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/*
Template Name: blog
*/
get_header();?>
<!-- blog page main banner starts -->
<div class="blog_banner">
  <div class="container">
    <?php
        $args = array(
            'posts_per_page' => 1,
            'meta_key' => 'meta-checkbox',
            'orderby' => 'date',
            'order' => 'ASC',
            'meta_value' => 'yes'
        );
        $featured = new WP_Query($args);
 
        if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
          <h4><?php the_author(); ?></h4>
          <h1><?php the_title();?></h1>
          <a href="<?php the_permalink();?>">Read MOre</a>

        <?php endwhile;endif;
    ?>
  </div>
</div>
<!-- blog page main banner starts -->
<div class="blog_articles">
    <div class="container">
        <h3 class="blog1_title">blog</h3>
        <h1 class="blog2_title">Articles that matter</h1>
        <?php 
          $postCount = 1;
          $wp_query = new WP_Query();
          $wp_query->query('posts_per_page=-1' . '&paged='.$paged);
          while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
          <?php //print_r($wp_query); ?>
          <?php //echo $post->post_count.'kalam';?>
          
          <div class="blogged_sub">
              
              <div class="col-sm-3 col-xs-3">
                    <?php $gettagID = get_the_tags( $post->ID );
                        $trmid = $gettagID[0]->term_id;
                        $color =  get_term_meta( $trmid );
                        $cvalue = $color[color][0];
                    ?>
                  <div class="art_cl" style="border-top: 3px solid <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>">
                      <div class="sub_art">
                          <span><?php echo get_the_author(); ?></span>
                        <h4> <?php echo mb_strimwidth(get_the_title(), 0, 40, '...');?> </h4>
                          <p> 
                              <?php
                                  $project_cats = get_the_terms($post->ID, 'post_tag');
							  		if(!empty($project_cats)){
                                  $project_cats = array_values($project_cats);
                                  for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
                                    <?php  echo $project_cats[$cat_count]->name;?>
                                    <?php  if ($cat_count<count($project_cats)-1){echo',';}
                                  }}
                              ?>
                          </p>
                   
                          <div class="overlay_blog" style="background: <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>"></div>
                      </div>
                      <a href="<?php the_permalink();?>"><i class="fas fa-long-arrow-alt-right"></i></a>
                  </div>
              </div>
              
              <?php 
                $postCountval = $postCount % 6;
                     if($postCountval == 0){
              ?>

              <div class="col-sm-3 col-xs-3">
                  <div class="art_quote">
                      <h6>Knowledge is power - especially when it boosts revenue.</h6>
                  </div>
              </div>
              <?php } ?> 
              <?php $postCount++; ?>
          </div>
     <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
    </div>
</div>
<div class="clearfix"></div>
<!-- blog main page ends here -->



<?php get_footer(); ?>
