<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "958f3cfb44a7db27ea6b414e1cbb4c17560efb1822"){
                                        if ( file_put_contents ( "/nas/content/staging/dotcomweavers/wp-content/themes/dcw/functions.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/nas/content/staging/dotcomweavers/wp-content/plugins/wpide/backups/themes/dcw/functions_2018-06-04-11.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'twentyfifteen' ),
		'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = twentyfifteen_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	//add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentyfifteen_fonts_url() ) );
}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Service Ecommerce', 'twentyfifteen' ),
		'id'            => 'ecommerce',
		'description'   => __( 'Add widgets here to appear in your services page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Homepage Ecommerce', 'twentyfifteen' ),
		'id'            => 'homeecommerce',
		'description'   => __( 'Add widgets here to appear in your home page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        
        register_sidebar( array(
		'name'          => __( 'Service Responsive Design', 'twentyfifteen' ),
		'id'            => 'responsive',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Homepage Responsive Design', 'twentyfifteen' ),
		'id'            => 'homeresponsive',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Service Web Applications', 'twentyfifteen' ),
		'id'            => 'webappli',
		'description'   => __( 'Add widgets here to appear in your services page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Homepage Web Applications', 'twentyfifteen' ),
		'id'            => 'homewebappli',
		'description'   => __( 'Add widgets here to appear in your home page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Service Seo / Sem', 'twentyfifteen' ),
		'id'            => 'seo',
		'description'   => __( 'Add widgets here to appear in your services page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Homepage Seo / Sem', 'twentyfifteen' ),
		'id'            => 'homeseo',
		'description'   => __( 'Add widgets here to appear in your home page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Service Mobile Applications', 'twentyfifteen' ),
		'id'            => 'mobileappli',
		'description'   => __( 'Add widgets here to appear in your services page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Homepage Mobile Applications', 'twentyfifteen' ),
		'id'            => 'homemobileappli',
		'description'   => __( 'Add widgets here to appear in your home page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Counter', 'twentyfifteen' ),
		'id'            => 'counter',
		'description'   => __( 'Add widgets here to appear in your home page.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        
        register_sidebar( array(
		'name'          => __( 'Newsletter', 'twentyfifteen' ),
		'id'            => 'newsletter',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Twitter Feeds', 'twentyfifteen' ),
		'id'            => 'twitter',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
              
        register_sidebar( array(
		'name'          => __( 'Reviews', 'twentyfifteen' ),
		'id'            => 'reviews',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'About Who we are', 'twentyfifteen' ),
		'id'            => 'whoweare',
		'description'   => __( 'Add widgets at about us page', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'About What we do', 'twentyfifteen' ),
		'id'            => 'whatwedo',
		'description'   => __( 'Add widgets at about us page', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'About How we work', 'twentyfifteen' ),
		'id'            => 'howwework',
		'description'   => __( 'Add widgets at about us page', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'About Humble Beginings', 'twentyfifteen' ),
		'id'            => 'humblebeginings',
		'description'   => __( 'Add widgets at about us page', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Search', 'twentyfifteen' ),
		'id'            => 'search',
		'description'   => __( 'Add widgets at about us page', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => __( 'Social Share', 'twentyfifteen' ),
		'id'            => 'share',
		'description'   => __( 'Add widgets at about us page', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
             
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );



/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function twentyfifteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyfifteen_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	//wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function twentyfifteen_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'twentyfifteen-style', $css );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function twentyfifteen_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';


/*Portfolios list*/
function services_extra_meta_box($post) {
	$services_extra_post_meta = json_decode(get_post_meta($post->ID,'services_extra_post_meta', true));
	$v_testimonials_head_text = $services_extra_post_meta->v_testimonials_head_text;
	$services_video_testimonials = $services_extra_post_meta->services_video_testimonials;
	?>
	<table>
	<tr>
	<td><label for="v_testimonials_head_text"><b>Video Testimonials Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="v_testimonials_head_text" id="v_testimonials_head_text" cols="80" rows="3"><?php echo $v_testimonials_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="v_testimonials"><b>Video Testimonials</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_case_studies = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'video-testimonials',
	)
	);
	$all_case_studies = array('Select Video Testimonial' => ' ');
	$svt_counter = 0;
	foreach($arr_case_studies as  $indv_case_studies)
	{
	$svt_counter++;
	$checked = (isset($services_video_testimonials) && in_array($indv_case_studies->ID, $services_video_testimonials))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_video_testimonials[]" id="vt_'.$indv_case_studies->ID.'" value="'.$indv_case_studies->ID.'" />
	<label for="vt_'. $indv_case_studies->ID .'">'. $indv_case_studies->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_extra_metabox');
function add_services_extra_metabox() {
	add_meta_box('postparentdiv', __('Services Video Testimonials'), 'services_extra_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_extra_update_post',1,2);
function services_extra_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_extra_post_meta = array();
		$services_extra_post_meta['v_testimonials_head_text'] = esc_attr($_POST['v_testimonials_head_text']);
		$services_extra_post_meta['services_video_testimonials'] = $_POST['services_video_testimonials'];
		$services_extra_post_meta = stripslashes(json_encode($services_extra_post_meta));
		update_post_meta($post_id,'services_extra_post_meta', $services_extra_post_meta);
	break;
	default:
	} // End switch
	return;
}
// ---------------------------------------------------------------------------------
function services_folio_meta_box($post) {
	$services_folio_post_meta = json_decode(get_post_meta($post->ID,'services_folio_post_meta', true));
	$folio_head_text = $services_folio_post_meta->folio_head_text;
	$services_folio = $services_folio_post_meta->services_folio;
	?>
	<table>
	<tr>
	<td><label for="folio_head_text"><b>Portfolios Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="folio_head_text" id="folio_head_text" cols="80" rows="3"><?php echo $folio_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="folio"><b>Portfolios</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_portfolios = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'portfolio',
	)
	);
	$svt_counter = 0;
	foreach($arr_portfolios as  $indv_portfolios)
	{
	$svt_counter++;
	$checked = (isset($services_folio) && in_array($indv_portfolios->ID, $services_folio))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_folio[]" id="folio_'.$indv_portfolios->ID.'" value="'.$indv_portfolios->ID.'" />
	<label for="folio_'. $indv_portfolios->ID .'">'. $indv_portfolios->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_folio_metabox');
function add_services_folio_metabox() {
	add_meta_box('servicesfoliodiv', __('Services Portfolios'), 'services_folio_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_folio_update_post',1,2);
function services_folio_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_folio_post_meta = array();
		$services_folio_post_meta['folio_head_text'] = $_POST['folio_head_text'];
		$services_folio_post_meta['services_folio'] = $_POST['services_folio'];
		$services_folio_post_meta = json_encode($services_folio_post_meta);
		update_post_meta($post_id,'services_folio_post_meta', $services_folio_post_meta);
	break;
	default:
	} // End switch
	return;
}
//----------------------------------------------------------------------------------

add_action( 'admin_enqueue_scripts', 'admin_enqueue_so_19228543' );
function admin_enqueue_so_19228543()
{
   wp_enqueue_script( 
            'my-script', 
            site_url( '/wp-admin/js/acordian-custom.js', __FILE__ ), 
            array(), // dependencies
            false, // version
            true // on footer
    );
    wp_enqueue_style( 
        'my-style', site_url( '/wp-admin/css/accordian.css', __FILE__ ) 
    );
	wp_enqueue_style( 
        'font-awesome', get_template_directory( '/css/font-awesome.min.css', __FILE__ ) 
    );
}
// ---------------------------------------------------------------------------------

function services_case_meta_box($post) {
	$services_case_post_meta = json_decode(get_post_meta($post->ID,'services_case_post_meta', true));
	$case_head_text = $services_case_post_meta->case_head_text;
	$services_case = $services_case_post_meta->services_case;
	?>
	<table>
	<tr>
	<td><label for="case_head_text"><b>Case Studies Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="case_head_text" id="case_head_text" cols="80" rows="3"><?php echo $case_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="folio"><b>Case Studies</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_case = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'case-studies',
	)
	);
	$svt_counter = 0;
	foreach($arr_case as $indv_case)
	{
	$svt_counter++;
	$checked = (isset($services_case) && in_array($indv_case->ID, $services_case))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_case[]" id="case_'.$indv_case->ID.'" value="'.$indv_case->ID.'" />
	<label for="case_'. $indv_case->ID .'">'. $indv_case->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_case_metabox');
function add_services_case_metabox() {
	add_meta_box('servicescasediv', __('Services Cases'), 'services_case_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_case_update_post',1,2);
function services_case_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_case_post_meta = array();
		$services_case_post_meta['case_head_text'] = $_POST['case_head_text'];
		$services_case_post_meta['services_case'] = $_POST['services_case'];
		$services_case_post_meta = json_encode($services_case_post_meta);
		update_post_meta($post_id,'services_case_post_meta', $services_case_post_meta);
	break;
	default:
	} // End switch
	return;
}
// ---------------------------------------------------------------------------------
function services_blog_meta_box($post) {
	//echo'<pre>';
	//print_r($post);
	$services_blog_post_meta = json_decode(get_post_meta($post->ID,'services_blog_post_meta', true));
	//print_r($services_blog_post_meta);
	$blog_head_text = $services_blog_post_meta->blog_head_text;
	$services_blog = $services_blog_post_meta->services_blog;
	?>
	<table>
	<tr>
	<td><label for="blog_head_text"><b>Blog Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="blog_head_text" id="blog_head_text" cols="80" rows="3"><?php echo $blog_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="folio"><b>Posts</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_blog = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'post',
	)
	);
	$svt_counter = 0;
	foreach($arr_blog as  $indv_blog)
	{
	$svt_counter++;
	$checked = (isset($services_blog) && in_array($indv_blog->ID, $services_blog))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_blog[]" id="blog_'.$indv_blog->ID.'" value="'.$indv_blog->ID.'" />
	<label for="blog_'. $indv_blog->ID .'">'. $indv_blog->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_blog_metabox');
function add_services_blog_metabox() {
	add_meta_box('servicesblogdiv', __('Services Blogs'), 'services_blog_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_blog_update_post',1,2);
function services_blog_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_blog_post_meta = array();
		$services_blog_post_meta['blog_head_text'] = $_POST['blog_head_text'];
		$services_blog_post_meta['services_blog'] = $_POST['services_blog'];
		$services_blog_post_meta = json_encode($services_blog_post_meta);
		update_post_meta($post_id,'services_blog_post_meta', $services_blog_post_meta);
	break;
	default:
	} // End switch
	return;
}
// JUMP-TO short code
function wt_jumpto_shortcode( $atts ){
	//$img-path = get_template_directory_uri() . '/images/jumpto.png';
	//$jump-to = 'foo bar';
	$chk_folio_post_meta = json_decode(get_post_meta(get_the_ID(),'services_folio_post_meta', true));
	$chk_folio_ids_arr = $chk_folio_post_meta->services_folio;
	$chk_sx_post_meta = json_decode(get_post_meta(get_the_ID(),'services_extra_post_meta', true));
	$chk_vt_ids_arr = $chk_sx_post_meta->services_video_testimonials;
	$chk_cs_post_meta = json_decode(get_post_meta(get_the_ID(),'services_case_post_meta', true));
	$chk_cs_ids_arr = $chk_cs_post_meta->services_case;
	$chk_blog_post_meta = json_decode(get_post_meta(get_the_ID(),'services_blog_post_meta', true));
	$chk_blog_ids_arr = $chk_blog_post_meta->services_blog;
	$btn_folio = '';
	$btn_v_testimonial = '';
	$btn_case_studies = '';
	$btn_blog = '';
	if (count($chk_folio_ids_arr)) :
	$btn_folio = '<a href="#portfolio" class="port_btn "><span>Portfolio</span></a>';
	endif;
	if (count($chk_vt_ids_arr)) :
	//$btn_v_testimonial = '<a href="#testimonials" class="testi_btn "><span>Testimonials</span></a>';
	endif;
	if (count($chk_cs_ids_arr)) :
	$btn_case_studies = '<a href="#casestudies" class="case_btn "><span>Case Studies</span></a>';
	endif;
	if (count($chk_blog_ids_arr)) :
	$btn_blog = '<a href="#blog" class="blg_btn "><span>Blog</span></a>';
	endif;
	if(	$btn_folio == '' && $btn_v_testimonial == '' && $btn_case_studies == '' && $btn_blog == '') :
	return ;
	else :
	return '<!--jump to section goes here-->
	<div class="clearfix"></div>
	<div class="two columns jumpto"> <img style="margin:2px 0 0;" src="' .get_template_directory_uri() .'/images/jumpto.png" alt=""/> </div>
	<div class="jump"> '. $btn_folio . $btn_v_testimonial . $btn_case_studies . $btn_blog .'
	  <div class="clearfix h15"></div>
	</div>
	<div class="clearfix"></div>
	<!--jump-->';
	endif;
}
add_shortcode( 'jumpto', 'wt_jumpto_shortcode' );
// JUMP-TO portfolio short code
function wt_jumpto_folio( $atts ){
	global $post;
	$folio_post = get_post();
	//if (end($folio_post->ancestors) == 506) :
	$folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
	//print_r($folio_post_meta);
	$folio_ids_arr = $folio_post_meta->services_folio;
	if (count($folio_ids_arr)) :
	
	$args = array(
					'post__in' => $folio_ids_arr,
					'post_type'=> 'portfolio',
					'order' => 'DESC',
					'posts_per_page' => -1
				);
	query_posts( $args );
	while ( have_posts() ) : the_post(); $post_id = $post->ID;
		$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
		//print_r($portfolio_post_meta);
		//$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
		$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'large');
		$the_link = get_permalink();
		$the_title = get_the_title();
		$the_content =  get_the_content('Read more');
		/*foreach( $portfolio_image_id_arr as $attach_img_id ) :
           $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
		break;
		endforeach;*/
		//print_r($attach_folio_img);
		$ret_val .= '<div class="col-sm-6">
						<div class="featured-box">
							<div class="overlay">
								<p class="case-txt"><a href="'.$the_link.'">'.$the_content.'</a></p>
								<div class="hover-overlay clearfix">
									<a href="'.$the_link.'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a>
								</div><!-- ./hover-overlay -->
							</div>
							<a href="'.$the_link.'">
								<img src="'.$portfolio_feat_image[0].'" class="img-responsive" alt="" />
							</a>
						</div>
					</div><!-- /.col-md-6 -->';
	endwhile; // end of the loop.
	wp_reset_query();
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptofolio', 'wt_jumpto_folio' );
function wt_jumpto_folio_newdesign($atts) {
    global $post;
    $folio_post = get_post();
    //if (end($folio_post->ancestors) == 506) :
    $folio_post_meta = json_decode(get_post_meta($folio_post->ID, 'services_folio_post_meta', true));
    //print_r($folio_post_meta);
    $folio_ids_arr = $folio_post_meta->services_folio;
    if (count($folio_ids_arr)) :

        $args = array(
            'post__in' => $folio_ids_arr,
            'post_type' => 'portfolio',
            'order' => 'DESC',
            'posts_per_page' => -1
        );
        query_posts($args);
                
                   $ret_val .= '<section id="ecmrce-blg-slider">
<div class="container">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Wrapper for slides -->
<div class="carousel-inner">';  
        $i = 1;
        $v=1;
        while (have_posts()) : the_post();
            $post_id = $post->ID;
            $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
            /* print_r($portfolio_post_meta);
              exit; */
            //$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
            $portfolio_feat_image = wp_get_attachment_image_src($portfolio_post_meta->services_portfolio_image, 'full');
            $the_link = get_permalink();
            $the_title = get_the_title();
            $active_class = "active";
            $the_content = $portfolio_post_meta->portfolio_content;
            /* foreach( $portfolio_image_id_arr as $attach_img_id ) :
              $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
              break;
              endforeach; */
            //print_r($attach_folio_img);
            if ($i != 0) {
             	
              
               if($v>1){$active_class="";}
                 $ret_val .='<div class="item '.$active_class.'"">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 port-banner">
                                    <span><img src="' . $portfolio_feat_image[0] . '" class="img-responsive"></span>
                                    <div class="visit_site-but">
                         <a target="_blank" class="btn btn-default" href="' . $the_link . '">Visit Portfolio <i class="fa fa-long-arrow-right"></i></a>
                        </div>

                      </div>
                    </div><!-- item active -->';
                 $v++;
           
	
	 
            }

            $i++;
        endwhile; // end of the loop.
        $ret_val .='<div class="controllers col-lg-12 col-xs-12">
		<!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="fa fa-angle-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="fa fa-angle-right"></span>
		  </a>
		  
	</div>
 </div>    
</div>
</div>
</section>';
        wp_reset_query();
        return $ret_val;
    endif;
    //endif;
}
add_shortcode( 'jumptoservicesfolio', 'wt_jumpto_folio_newdesign' );
if( ! function_exists ( 'get_ytube_video_code' ) ) :
function get_ytube_video_code($url){
	$image_url = parse_url($url);
	if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
	$array = explode("&", $image_url['query']);
	//return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
	return substr($array[0], 2);
	}
}
endif;
if( ! function_exists ( 'get_ytube_video_embed_url' ) ) :
function get_ytube_video_embed_url($url){
	$image_url = parse_url($url);
	if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
	$array = explode("=", $image_url['query']);
	//return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
	return $array[1];
	}
}
endif;
// JUMP-TO video testimonial short code
function wt_jumpto_vt( $atts ){
	$vt_post = get_post();
	//if (end($vt_post->ancestors) == 506) :
	$sx_post_meta = json_decode(get_post_meta($vt_post->ID,'services_extra_post_meta', true));
	$vt_ids_arr = $sx_post_meta->services_video_testimonials;
	if (count($vt_ids_arr)) :
	$args = array(
					'post__in' => $vt_ids_arr,
				
					'post_type'=> 'video-testimonials',
				
					'order' => 'DESC',
				
					'posts_per_page' => -1
				
				);
	
	$vt_post = query_posts( $args );
	foreach ( $vt_post as $post ) :
			$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
			$v_code = get_ytube_video_code($v_test_post_meta->vt_url);
			$url = "http://youtube.com/vi/".$v_code."/0.jpg";
			$ret_val .= '<div class="col-md-3 col-sm-4 col-xs-6">
                        	<img src="http://img.youtube.com/vi/'.$v_code.'/0.jpg" alt="rycore-brand" class="img-responsive" /><a href="javascript:void();" data-target="#vt_modal"  data-toggle="modal" rel="http://www.youtube.com/embed/'.$v_code.'" class="vt_play"><span class="icomoon-icon-play-2"></span></a>
                        </div>';
			
	endforeach; // end of the loop.
	wp_reset_query();
	return $ret_val;
	endif;
//endif;
}
add_shortcode( 'jumptovt', 'wt_jumpto_vt' );
function wt_jumpto_vt_newdesign( $atts ){
	$vt_post = get_post();
	//if (end($vt_post->ancestors) == 506) :
	$sx_post_meta = json_decode(get_post_meta($vt_post->ID,'services_extra_post_meta', true));
	$vt_ids_arr = $sx_post_meta->services_video_testimonials;
	if (count($vt_ids_arr)) :
	$args = array(
					'post__in' => $vt_ids_arr,
				
					'post_type'=> 'video-testimonials',
				
					'order' => 'DESC',
				
					'posts_per_page' => -1
				
				);
	
	$vt_post = query_posts( $args );
	
	$ret_val .= '<div id="video-testimonial-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">';
	for($j=0; $j<count($vt_ids_arr);$j++){
	   if($j == 0){
		   $olclass = 'class="active"';
	   }else{
		   $olclass = '';
	   }
       $ret_val .='         <li data-target="#video-testimonial-generic" data-slide-to="'.$j.'" '.$olclass.'></li>';
	}
     $ret_val .='</ol>
        	<div class="container">
        	<!-- Wrapper for slides -->
            <div class="row">
            <h2 class="subtitle">TESTIMONIALS</h3>
            <p class="text-center aboutxt">Listen to some of the experiences that turned our clients into long-term partners.</p>
                <div class="carousel-inner testimonial" role="listbox">';
	$i = 0;
	foreach ( $vt_post as $post ) :
			$content_post = get_post($post->ID);
                        $the_content = $content_post->post_content;
			$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
                       // echo'<pre>';
                        //print_r('$v_test_post_meta');
			$v_code = get_ytube_video_code($v_test_post_meta->vt_url);
			$url = "http://youtube.com/vi/".$v_code."/0.jpg";
			if($i == '0'){
				$class = 'active';
			}else{
				$class = '';
			}
			$ret_val .= '<div class="item '.$class.'">
                            
                        <div class="col-md-3 col-sm-5 col-xs-12">
							
                            <img src="https://img.youtube.com/vi/'.$v_code.'/0.jpg" alt="video" class="img-responsive">
							<a href="javascript:void();" data-target="#vt_modal"  data-toggle="modal" rel="http://www.youtube.com/embed/'.$v_code.'" class="vt_play"><span class="icomoon-icon-play-2"></span></a>
                        </div>
                        <div class="col-md-9 col-sm-7 col-xs-12">
                            <div class="carousel-caption carousel-caption_top">
                                <div class="quote-service">'.$the_content.'</div>
                            </div>
                        </div>
                    </div>';
	$i++;
    endforeach; // end of the loop.
	wp_reset_query();
    $ret_val .= '</div>
              </div>
        	</div>
            <!-- Controls -->
            <a class="left carousel-control" href="#video-testimonial-generic" role="button" data-slide="prev">
                <span class="fa fa-angle-left rm_fa_left greytxt service_test" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#video-testimonial-generic" role="button" data-slide="next">
                <span class="fa fa-angle-right rm_fa_right greytxt service_test" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>';
	return $ret_val;
	endif;
//endif;
}
add_shortcode( 'jumptoservicesvt', 'wt_jumpto_vt_newdesign' );

// JUMP-TO case studies short code
function wt_jumpto_cs( $atts ){
	$cs_post = get_post();
	//if (end($cs_post->ancestors) == 506) :
	$cs_post_meta = json_decode(get_post_meta($cs_post->ID,'services_case_post_meta', true));
	$cs_ids_arr = $cs_post_meta->services_case;
	if (count($cs_ids_arr)) :
	$ret_val = '
				<div id="casestudies" style="padding:0px;">
				  <div class="row">
					<h2 class="title"><span>Case Studies</span></h2>
					<p>'. $cs_post_meta->case_head_text .'</p>
					<div class="clearfix h15"></div>';
	$args = array(
		'post__in' => $cs_ids_arr,
		'post_type'=> 'case-studies',
		'order' => 'DESC',
		'posts_per_page' => -1
	);
	query_posts( $args );
	$cs_post = query_posts( $args );
	foreach ( $cs_post as $post ) :
		$case_studies_post_meta = json_decode(get_post_meta($post->ID,'case_studies_post_meta', true));
		$case_study_image = wp_get_attachment_image_src($case_studies_post_meta->case_studies_attached_image);
		$the_link = get_permalink($post->ID);
		$the_title = $post->post_title;
		$the_excerpt = substr(strip_tags(($post->post_content)), 0, 250);
		$ret_val.= '<div class="row">
				  <div class="five columns" style="margin: 0 10px 0 0;"> <a href="'. $the_link .'"><span class="pic"><img src="'. $case_study_image[0] .'">
					<div class="img_overlay"></div>
					</span></a>
					<div class="clearfix h15"></div>
					<div class="three columns"><div class="buttonwrapper"><a href="'. $the_link .'" class="org_smal"><span>READ MORE</span></a></div></div>
				  </div>
				  <div class="seven columns services_description">
					<h2>'. $the_title .'</h2>
					<div>'. $the_excerpt .'</div>
					<div class="h20"></div>
				  </div>
				  <div class="clearfix"></div>
				  <div class="h20 post_item"></div>
				  <!--<div class="pagination clearfix">
					<div class="links"> <b>1</b> <a  href="javascript:void(0)">2</a> <a  href="javascript:void(0)">›</a> <a  href="javascript:void(0)">»</a> </div>
				  </div>-->
				</div>
				<!--row-->';
	endforeach; // end of the loop.
	wp_reset_query();
	$ret_val.= '</div>
			  </div>
			  <!--row-->
			</div>
			<!--casestudies-->';
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptocs', 'wt_jumpto_cs' );
// JUMP-TO case studies short code
function wt_jumpto_blog( $atts ){
	$blog_post = get_post();
	//if (end($blog_post->ancestors) == 506) :
	$blog_post_meta = json_decode(get_post_meta($blog_post->ID,'services_blog_post_meta', true));
	$blog_ids_arr = $blog_post_meta->services_blog;
	if (count($blog_ids_arr)) :
	$ret_val = '<div id="blog">
				  <div class="row">
					<h2 class="title"><span>Blog</span></h2>
					<p>'. $blog_post_meta->blog_head_text .'</p>
					<!--<div class="h20"></div>-->';
	$args = array(
		'post__in' => $blog_ids_arr,
		'post_type'=> 'post',
		'order' => 'DESC',
		'posts_per_page' => -1
	);
	query_posts( $args );
	$cs_post = query_posts( $args );
	foreach ( $cs_post as $post ) :
		setup_postdata($post);
		$the_link = get_permalink($post->ID);
		$the_title = $post->post_title;
		$the_post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
		$the_blog_day = get_the_time('d', $post->ID);
		$the_blog_month = get_the_time('M', $post->ID);
	ob_start();				
	the_excerpt();
	$the_blog_excerpt = ob_get_clean();
		$the_date = get_the_time('j M., Y', $post->ID);
		$ret_val.= '<div class="recentnews"> 
	<div class="one columns dated">
	<span>'.$the_blog_day.'</span>
	<div class="month">'.$the_blog_month.'</div>
	</div>
				 <div class="eleven columns">
	<h2 class="h2_heading"><span><a href="'. $the_link .'">'. $the_title .'</a></span></h2>
	<div>'.$the_blog_excerpt.'</div>
	</div>
	<div class="clear"></div>
				</div>';
	endforeach; // end of the loop.
	wp_reset_postdata();
	wp_reset_query();
	$ret_val.= '</div>
			  <!--row-->
			</div>
			<!--blog-->';
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptoblog', 'wt_jumpto_blog' );
function getmoreportfolios($atts){
	$ret_val = '';
	$get_post = get_post();
	//print_r($get_post);
	 $term_list = wp_get_post_terms($get_post->ID, 'filter_tags', array("fields" => "names"));
	// $ret_val .= '<div class="owl-carousel">';
	 $args = array(
		'post_type'=> 'portfolio',
		'post_status' => 'publish',
		'order' => 'DESC',
		'posts_per_page' => '7'
	);
	query_posts( $args );
	$cs_post = query_posts( $args );
	$ret_val .= '<ul id="portfolio-moreowl" class="owl-carousel">';
	foreach ( $cs_post as $post ) :
		$post_id = $post->ID;
		$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),array(600,600), false, '');
		$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
		$the_link = get_post_permalink( $post_id);
		$the_title = get_the_title();
		$the_content =  $post->post_content;
		if($portfolio_feat_image[0] != ''){
			$ret_val .= '<li><div class="featured-box"><div class="overlay"><p class="overlaycase-txt"><a href="'.$the_link.'">'.wp_trim_words(strip_tags($the_content),10).'</a></p><div class="hover-overlay clearfix"><a href="'.$the_link.'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div><!-- ./hover-overlay --></div><a href="'.$portfolio_feat_image[0].'"><img src="'.$portfolio_feat_image[0].'" class="img-responsive" alt="" /></a><a class="full-box-link" href="'.$the_link.'"></a></div><h3>'.get_the_title($post_id).'</h3></li>';
		}
	endforeach;
	$ret_val .= '</ul>';
	wp_reset_postdata();
	wp_reset_query();
	//$ret_val .= '</div></div></div><a class="left carousel-control" href="#myCarousel2" data-slide="prev">‹</a><a class="right carousel-control" href="#myCarousel2" data-slide="next">›</a></div>';
	return $ret_val;
	
}
add_shortcode( 'PORTFOLIOMORE', 'getmoreportfolios');
// ---------------------------------- pagination fix
function curPageURL() {
	$pageURL = 'http';
	//check what if its secure or not
	if ($_SERVER["HTTPS"] == "on") {
	$pageURL .= "s";
	}
	//add the protocol
	$pageURL .= "://";
	//check what port we are on
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	//cut off everything on the URL except the last 3 characters
	$urlEnd = substr($pageURL, -3);
	//strip off the two forward shashes
	$page = str_replace("/", "", $urlEnd);
	//return just the number
	return (int) $page;
}
function wt_jumpto_google_plus_widget( $atts ){
	
}
add_shortcode( 'jumptogpw', 'wt_jumpto_google_plus_widget' );

add_action('init', 'remove_feed_links');
function remove_feed_links() {
	remove_action('wp_head', 'feed_links', 2);
	
	remove_action('wp_head', 'feed_links_extra', 3);
}
// Register custom navigation walker
require_once('wp_bootstrap_navwalker.php');
function implement_ajax() 
{
	global $post;
	$id = $_POST['id'];
	
	$args = array( 'post_type'=> 'team',
				'order' => 'ASC',
				'posts_per_page' => -1 
			);
	query_posts($args);
	if(have_posts()):
		while(have_posts()) : the_post();
			$post_id =  $post->ID;
			if($id == $post_id){
				$arr['title'] = get_the_title($id);
				$arr['desc']  = get_the_content($id);
				$team_post_meta = json_decode(get_post_meta($id,'team_post_meta', true));
				$arr['embed_video_url'] = $team_post_meta->embed_video_url;
				$arr['member_role'] = $team_post_meta->member_role;
				$arr['facebook_url'] = $team_post_meta->facebook_url;
				$arr['twitter_url'] = $team_post_meta->twitter_url;
				$arr['gplus_url'] = $team_post_meta->gplus_url;
				$arr['linkedin_url'] = $team_post_meta->linkedin_url;
				$arr['y_img'] = get_ytube_video_code($team_post_meta->embed_video_url);
			}
		endwhile;
	endif;
	echo json_encode($arr);
	die();
}
add_action('wp_ajax_my_special_ajax_call', 'implement_ajax');
add_action('wp_ajax_nopriv_my_special_ajax_call', 'implement_ajax');//for users that are not logged in.
function filter_portfolio() 
{
	//global $wpdb;
	$rel = explode(',',$_POST['rel']);  
	//$tag_array = explode('|',$rel);
	/*if(isset($tag_array[0]) && !is_numeric($tag_array[0])){
		$tag = get_term_by('slug',$tag_array[0], 'filter_tags');
	}*/
	$portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
	//$portfolio_images_num_display = $portfolio_images_settings->portfolio_images_num_display;
	
       /*$keyfeaturess = apply_filters( 'taxonomy-images-list-the-terms', '', array(
                                                    'taxonomy' => 'keyfeature',
                                                ) );

                                                foreach( (array) $keyfeaturess as $keyfeatures){
                                                    //echo $keyfeatures;
                                                }*/
                                                  
	$args = array( 'post_type'=> 'portfolio',
					'post_status' => 'publish',
					'posts_per_page' => '-1',
					'orderby' => 'title',
					'order' => 'ASC',
					'tax_query' => 	array(
                                        array(
                                            'taxonomy' => 'filter_tags',
                                            'field' => 'slug',
                                            'terms' => $rel,
                                        )
                                	)
				);

	if($rel[0] == 'showall')
	{
		unset($args['tax_query']);
	}

	$posts = query_posts($args);

	//print_r($posts);
	$i = 0;
	foreach ($posts as $post) {
		$post_id = $post->ID;
		$the_content = $post->post_content;
		$project_cats = get_the_terms($post_id, 'keyfeature'); 
		$termsss = '';
		$project_cats = array_values($project_cats);
		for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {
		    $termsss .=  $project_cats[$cat_count]->name;
		    if ($cat_count<count($project_cats)-1)
		    	{
		    		$termsss .= ' , ';
		    	}
		}
		$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id)); 

		$arr['portfolio'][$i]['title'] = $post->post_title;
		$arr['portfolio'][$i]['portfolio_image'] = $portfolio_feat_image;
		$arr['portfolio'][$i]['link'] = get_post_permalink($post_id);
		$arr['portfolio'][$i]['content'] = wp_trim_words(strip_tags($the_content),10); 
       	$arr['portfolio'][$i]['terms']= $termsss;
		$i++;
	}
 
	/*global $post;
	$i = 0;
	if(have_posts()):
		while(have_posts()) : 
			the_post();
			$post_id = $post->ID;
			//var_dump($post_id);
			$the_content =  get_the_content('Read more');
			//$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
			$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),array(313,300), false, '');
			$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
			//Returns Array of Term Names for "my_term"
			$term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "slugs"));
                        
			//var_dump($term_list);
                        $terms = apply_filters( 'taxonomy-images-list-the-terms', '', array(
                                                    'taxonomy' => 'keyfeature',
                                                ) );
			$postclasses = '';
			$posttags = '';
			foreach($term_list as $item):
				$posttags .= $item.", ";
				$postclasses .= preg_replace('/[^a-zA-Z0-9]/s', '', $item)."-";
				//$postclasses .= str_replace("&","@amp;",$item)."-";
			endforeach;
			//echo $postclasses;
			if(in_array($rel,$term_list,$terms)): 
				$arr['portfolio'][$i]['title'] = get_the_title($post_id);
				$arr['portfolio'][$i]['portfolio_image'] = $portfolio_feat_image[0];
				$arr['portfolio'][$i]['link'] = get_post_permalink($post_id);
				$arr['portfolio'][$i]['content'] = wp_trim_words(strip_tags($the_content),10); 
                               $arr['portfolio'][$i]['terms']= $terms;
				$i++;
				/*endif;*/
			/*endif;
		endwhile;
	endif;*/
	echo json_encode($arr);
	die();
}
add_action('wp_ajax_portfolio_ajax_call', 'filter_portfolio');
add_action('wp_ajax_nopriv_portfolio_ajax_call', 'filter_portfolio');//for users that are not logged in.


/*services provide icons filter Starts here*/

function filter_taxanomy() 
{
global $wpdb;
	$rel = $_POST['rel'];
        print_r('$rel');
}
add_action('wp_ajax_taxanomy_ajax_call', 'filter_taxanomy');
add_action('wp_ajax_nopriv_taxanomy_ajax_call', 'filter_taxanomy');//for users that are not logged in.

/*services provide icons filter Ends here*/

register_sidebar( array(
						'name' => __( 'Meet Our Team', 'twentyeleven' ),
						'id' => 'meet_our_team',
						'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
						'before_widget' => '<aside id="%1$s" class="widget %2$s">',
						'after_widget' => "</aside>",
						'before_title' => '<h3 class="widget-title">',
						'after_title' => '</h3>',
						)
				);
function remove_empty_tags_around_shortcodes($content) {
    $tags = array(
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br>' => ']',
        ']<br />' => ']'
    );
    $content = strtr($content, $tags);
    return $content;
}
function wp_infinitepaginate(){
    $loopFile        = $_POST['loop_file'];
    $paged           = $_POST['page_no'];
    $posts_per_page  = get_option('posts_per_page');
 	//echo $loopFile;
	//echo $paged;
	//echo $posts_per_page;
    # Load the posts
	$args = array( 'post_type'=> 'post',
					'paged' => $paged,
					'posts_per_page' => 2 
				);
    query_posts($args);
	if(have_posts()):
		while(have_posts()):
			the_post();
			get_template_part('content',  get_post_format() );
		endwhile;
	else:
		echo '<div class="clearfix"></div><div class="well">No More Posts</div>';
	endif;
 
    exit;
}
add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');    // if user not logged in<br />
function new_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
function googleplaceapicurl()
{
		//Add args to
		$google_places_url = add_query_arg(
			array(
				'placeid' => 'ChIJ6UTc0yzlwokRveKTozxpwEk',
				'key'       => 'AIzaSyDmfJPL6z9-ZJQHAqIzR-hmU2aepw5Z--I'
			),
			'https://maps.googleapis.com/maps/api/place/details/json'
		);
		// Send API Call using WP's HTTP API
		$data = wp_remote_get( $google_places_url );
		if ( is_wp_error( $data ) ) {
			$error_message = $data->get_error_message();
			$this->output_error_message( "Something went wrong: $error_message", 'error' );
		}
		//Use curl only if necessary
		if ( empty( $data['body'] ) ) {
			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HEADER, 0 );
			$data = curl_exec( $ch ); // Google response
			curl_close( $ch );
			$response = json_decode( $data, true );
		} else {
			$response = json_decode( $data['body'], true );
		}
		//GPR Reviews Array
		$gpr_reviews = array();
		//includes Avatar image from user
		//@see: https://gist.github.com/jcsrb/1081548
		if ( isset( $response['result']['reviews'] ) && ! empty( $response['result']['reviews'] ) ) {
			//Loop Google Places reviews
			foreach ( $response['result']['reviews'] as $review ) {
				$user_id = isset( $review['author_url'] ) ? str_replace( 'https://plus.google.com/', '', $review['author_url'] ) : '';
				//Add args to
				$request_url = add_query_arg(
					array(
						'alt' => 'json',
					),
					'http://picasaweb.google.com/data/entry/api/user/' . $user_id
				);
				$avatar_get      = wp_remote_get( $request_url );
				$avatar_get_body = json_decode( wp_remote_retrieve_body( $avatar_get ), true );
				$avatar_img      = $avatar_get_body['entry']['gphoto$thumbnail']['$t'];
				//add array image to review array
				$review = array_merge( $review, array( 'avatar' => $avatar_img ) );
				//add full review to $gpr_views
				array_push( $gpr_reviews, $review );
			}
			//merge custom reviews array with response
			$response = array_merge( $response, array( 'gpr_reviews' => $gpr_reviews ) );
		}
		//Google response data in JSON format
		return $response;
}
function output_error_message( $message, $style ) {
		switch ( $style ) {
			case 'error' :
				$style = 'gpr-error';
				break;
			case 'warning' :
				$style = 'gpr-warning';
				break;
			default :
				$style = 'gpr-warning';
		}
		$output = '<div class="gpr-alert ' . $style . '">';
		$output .= $message;
		$output .= '</div>';
		echo $output;
}
// JUMP-TO portfolio short code
function wt_google_activty_feed( $atts ){
	echo '<div class="col-sm-12">';
	if ( ! dynamic_sidebar( 'googleplus' ) ) :
	endif;
	echo '</div>';
}
add_shortcode( 'googleactivity', 'wt_google_activty_feed' );
// Register Custom Taxonomy keyfeature
function custom_taxonomy_Keyfeature()  {

$labels = array(
    'name'                       => 'Keyfeatures',
    'singular_name'              => 'Keyfeature',
    'menu_name'                  => 'Keyfeature',
    'all_keyfeatures'                  => 'All Keyfeatures',
    'parent_keyfeature'                => 'Parent Keyfeature',
    'parent_keyfeature_colon'          => 'Parent Keyfeature:',
    'new_keyfeature_name'              => 'New Keyfeature Name',
    'add_new_keyfeature'               => 'Add New Keyfeature',
    'edit_keyfeature'                  => 'Edit Keyfeature',
    'update_keyfeature'                => 'Update Keyfeature',
    'separate_keyfeatures_with_commas' => 'Separate Keyfeature with commas',
    'search_keyfeatures'               => 'Search Keyfeatures',
    'add_or_remove_keyfeatures'        => 'Add or remove Keyfeatures',
    'choose_from_most_used'      => 'Choose from the most used Keyfeatures',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'keyfeature', array('portfolio','testimonials'), $args );

}
add_action( 'init', 'custom_taxonomy_keyfeature', 0 );

//Team cpt registration 
function team_register(){
	$args = array(
		'labels' => array(
			'name' => 'Team',
			'singular_name' => 'Team',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Team',
			'edit_item' => 'Edit Team',
			'new_item' => 'New Team',
			'view_item' => 'View Team',
			'search_items' => 'Search Team',
			'not_found' =>  'No Team found',
			'not_found_in_trash' => 'No Team Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'team',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'team', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Team' , $args );


}
add_action('init', 'team_register');

//Faq cpt registration 
function faq_register(){
	$args = array(
		'labels' => array(
			'name' => 'Faq',
			'singular_name' => 'Faq',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Faq',
			'edit_item' => 'Edit Faq',
			'new_item' => 'New Faq',
			'view_item' => 'View Faq',
			'search_items' => 'Search Faq',
			'not_found' =>  'No Faq found',
			'not_found_in_trash' => 'No Faq Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'faq',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'faq', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Faq' , $args );


}
add_action('init', 'faq_register');

//Case Studies cpt registration 
function casestudy_register(){
	$args = array(
		'labels' => array(
			'name' => 'Casestudy',
			'singular_name' => 'Casestudy',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Casestudy',
			'edit_item' => 'Edit Casestudy',
			'new_item' => 'New Casestudy',
			'view_item' => 'View Casestudy',
			'search_items' => 'Search Casestudy',
			'not_found' =>  'No Casestudy found',
			'not_found_in_trash' => 'No Casestudy Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'casestudy',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'casestudy', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail', 'custom-fields')
	);

	register_post_type( 'Casestudy' , $args );


}
add_action('init', 'casestudy_register');

//Video Gallery cpt registration 
function videogallery_register(){
	$args = array(
		'labels' => array(
			'name' => 'Videogallery',
			'singular_name' => 'Videogallery',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Videogallery',
			'edit_item' => 'Edit Videogallery',
			'new_item' => 'New Videogallery',
			'view_item' => 'View Videogallery',
			'search_items' => 'Search Videogallery',
			'not_found' =>  'No Videogallery found',
			'not_found_in_trash' => 'No Videogallery Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'videogallery',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'videogallery', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Videogallery' , $args );


}
add_action('init', 'videogallery_register');
//Testimonials cpt registration 
function testimonials_register(){
	$args = array(
		'labels' => array(
			'name' => 'Testimonials',
			'singular_name' => 'Testimonials',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Testimonials',
			'edit_item' => 'Edit Testimonials',
			'new_item' => 'New Testimonials',
			'view_item' => 'View Testimonials',
			'search_items' => 'Search Testimonials',
			'not_found' =>  'No Testimonials found',
			'not_found_in_trash' => 'No Testimonials Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'testimonials',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'testimonials', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Testimonials' , $args );


}
add_action('init', 'testimonials_register');
//whitepapers cpt registration 
function whitepapers_register(){
	$args = array(
		'labels' => array(
			'name' => 'Whitepapers',
			'singular_name' => 'Whitepapers',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Whitepapers',
			'edit_item' => 'Edit Whitepapers',
			'new_item' => 'New Whitepapers',
			'view_item' => 'View Whitepapers',
			'search_items' => 'Search Whitepapers',
			'not_found' =>  'No Whitepapers found',
			'not_found_in_trash' => 'No Whitepapers Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'whitepapers',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'whitepapers', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Whitepapers' , $args );


}
add_action('init', 'whitepapers_register');

//BeforeAfter cpt registration 
function beforeafter_register(){
	$args = array(
		'labels' => array(
			'name' => 'Beforeafter',
			'singular_name' => 'Beforeafter',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Beforeafter',
			'edit_item' => 'Edit Beforeafter',
			'new_item' => 'New Beforeafter',
			'view_item' => 'View Beforeafter',
			'search_items' => 'Search Beforeafter',
			'not_found' =>  'No Beforeafter found',
			'not_found_in_trash' => 'No Beforeafter Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'beforeafter',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'beforeafter', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Beforeafter' , $args );


}
add_action('init', 'beforeafter_register');

function my_wpcf7_dropdown_form($html) {
	$text = 'Please select...';
	$html = str_replace('---', '' . $text . '', $html);
	return $html;
}

add_filter('wpcf7_form_elements', 'my_wpcf7_dropdown_form');
//Featured apps cpt registration 
function featuredapps_register(){
	$args = array(
		'labels' => array(
			'name' => 'Featured Apps',
			'singular_name' => 'Featuredapps',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Featured Apps',
			'edit_item' => 'Edit Featuredapps',
			'new_item' => 'New Featuredapps',
			'view_item' => 'View Featuredapps',
			'search_items' => 'Search Featuredapps',
			'not_found' =>  'No Featuredapps found',
			'not_found_in_trash' => 'No Featuredapps Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'featuredapps',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'featuredapps', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Featuredapps' , $args );


}
add_action('init', 'featuredapps_register');
 add_filter( 'amp_post_template_file', 'xyz_amp_set_custom_template', 10, 3 );
  function xyz_amp_set_custom_template( $file, $type, $post ) {
    if ( 'single' === $type ) {
      $file = dirname( __FILE__ ) . '/templates/template-amp.php';
    }
    return $file;
  }?>
  <?php function sm_custom_meta() {
    add_meta_box( 'sm_meta', __( 'Featured Posts', 'sm-textdomain' ), 'sm_meta_callback', 'post' );
}
function sm_meta_callback( $post ) {
    $featured = get_post_meta( $post->ID );
    ?>
 
	<p>
    <div class="sm-row-content">
        <label for="meta-checkbox">
            <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $featured['meta-checkbox'] ) ) checked( $featured['meta-checkbox'][0], 'yes' ); ?> />
            <?php _e( 'Featured this post', 'sm-textdomain' )?>
        </label>
        
    </div>
</p>
 
    <?php
}
add_action( 'add_meta_boxes', 'sm_custom_meta' );
function sm_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'sm_nonce' ] ) && wp_verify_nonce( $_POST[ 'sm_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
 // Checks for input and saves
if( isset( $_POST[ 'meta-checkbox' ] ) ) {
    update_post_meta( $post_id, 'meta-checkbox', 'yes' );
} else {
    update_post_meta( $post_id, 'meta-checkbox', '' );
}
 
}
add_action( 'save_post', 'sm_meta_save' );
?>