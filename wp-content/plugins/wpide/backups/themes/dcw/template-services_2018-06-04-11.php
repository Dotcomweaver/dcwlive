<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "958f3cfb44a7db27ea6b414e1cbb4c17560efb1822"){
                                        if ( file_put_contents ( "/nas/content/staging/dotcomweavers/wp-content/themes/dcw/template-services.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/nas/content/staging/dotcomweavers/wp-content/plugins/wpide/backups/themes/dcw/template-services_2018-06-04-11.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?>

    <?php
/*
Template Name:mainservices
*/
?>


<?php get_header();?>

	<div class="service-sub-menu">
  <div class="container">
    <div class="service-menu-list">
      <ul>
        <li> <a href="/ecommerce/"> ECOMMERCE </a> </li>
        <li> <a href="/custom-software/"> CUSTOM SOFTWARE</a></li>
        <li> <a href="/mobile-apps/"> MOBILE APPS </a> </li>
      </ul>
    </div>
  </div>

</div>


 <div class="main-service-banner">
    <div class="container">
    <div class="col-sm-12">
    <div class="col-sm-5 col-xs-12 pull-right">
        <img src="<?php bloginfo('template_url'); ?>/images/Our Services Video Thumb.jpg">
      </div>
    	 <div class="col-sm-7 col-xs-12 pull-left">
        <h1>Our Services</h1>
       <p><?php the_field('banner_text',506);?></p>
        <p class="help">On that note, what can we help your business with?</p>
      </div>
      

    </div>
     
    </div>

  </div>
  <!--main service banner ends-->

  <!-- service navigation content-->
  <div class="service-navigation-content">
    <div class="container">
      <div class="col-sm-12">

        <div class="col-sm-4 col-md-4">
          <div class="content-section">
            <a href="/ecommerce/"><h3 class="m0">eCommerce</h3></a>
            <p><?php the_field('block_1',506);?></p>
          </div>
          <div class="service-content-footer">
            <h4>SERVICES</h4>
            <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>  Magento
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fa fa-circle" aria-hidden="true"></i>  ERP Integration
              </div>
            </div>
             <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>  Marketing
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fa fa-circle" aria-hidden="true"></i>  Related Work
              </div>
            </div>
          </div>
        </div>

         <div class="col-sm-4 col-md-4">
          <div class="content-section">
            <a href="/custom-software/"><h3 class="m0">Custom Software</h3></a>
            <p><?php the_field('block_2',506);?></p>
          </div>
          <div class="service-content-footer">
            <h4>SERVICES</h4>
            <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>   Scalable Architecture
              </div>
              <div class="col-sm-6 p0 ">
                <i class="fa fa-circle" aria-hidden="true"></i>   AR / AI
              </div>
            </div>
             <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>   Process Automation
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fa fa-circle" aria-hidden="true"></i>   Related Work
              </div>
            </div>
          </div>
        </div>

         <div class="col-sm-4 col-md-4">
          <div class="content-section">
            <a href="/mobile-apps/"><h3 class="m0">Mobile Apps</h3></a>
            <p><?php the_field('block_3',506);?></p>
          </div>
          <div class="service-content-footer">
            <h4>SERVICES</h4>
            <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>   ERP Integration
              </div>
              <div class="col-sm-6 p0 ">
               <i class="fa fa-circle" aria-hidden="true"></i>  Marketing
              </div>
            </div>
             <div class="col-sm-12 p0 servic_list">
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>   Magento
              </div>
              <div class="col-sm-6 p0 ">
              <i class="fa fa-circle" aria-hidden="true"></i>   Related Work
              </div>
            </div>
          </div>
        </div>



      </div>
    </div>
  </div>

  <!-- service navigation content end-->

  <!--work together section-->
  <div class="work-together service_main clearfix">
  	<div class="sub-container">
  		<div class="col-sm-12">
  			<div class="col-sm-4">
  				<img src="<?php bloginfo('template_url'); ?>/images/LetsWork.png">
  			</div>

  			<div class="col-sm-8">
  				<h1>Let's Work Together.</h1>
  				<div class="work-together-content">
  					<p><?php the_field('work_together',506);?></p>

  					<p class="touch"> <a href="/contact-us/">  Get In Touch! </a> </p>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>


  <!--work together section end-->



<?php get_footer(); ?>
