<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "958f3cfb44a7db27ea6b414e1cbb4c17560efb1822"){
                                        if ( file_put_contents ( "/nas/content/staging/dotcomweavers/wp-content/themes/dcw/homepage.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/nas/content/staging/dotcomweavers/wp-content/plugins/wpide/backups/themes/dcw/homepage_2018-06-04-11.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/*
Template Name: homepage
*/
get_header();?>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<?php if(is_page( array( 6477,479,7356) )){ ?>

<div class="work_section">

   <div class="project_div bg_wht">
         <div class="container">
            <h4 class="work_hdng"><?php the_field('work_heading');?></h4>
            <h3 class="mid_dcm"><?php the_field('work_content');?></h3>
            <div class="row photo_gallery">
              <div class="owl-carousel_work">
            	<?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 4;
    global $post;
	$folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
	//print_r($folio_post_meta);
	$folio_ids_arr = $folio_post_meta->services_folio;
	if (count($folio_ids_arr)) :
	
	$args = array(
					'post__in' => $folio_ids_arr,
					'post_type'=> 'portfolio',
					'order' => 'DESC',
					'posts_per_page' => -1
				);
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
               <div class="item <?php echo rtrim($posttags," "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4><?php echo the_title();?></h4>
                       
						<p>
							<?php

						// Get a list of terms for this post's custom taxonomy.
						$project_cats = get_the_terms($post->ID, 'keyfeature');
						// Renumber array.
						$project_cats = array_values($project_cats);
						//$x = $project_cats;
							//$x = preg_replace('/\s*,\s*/', ',', $x);

							//echo $x;
						for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
						  <?php  echo $project_cats[$cat_count]->name;?>
						  	  <?php  if ($cat_count<count($project_cats)-1){echo",";}
						}
						?></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
          </div>
          </div>  
         </div>
      </div>
<!-- Work block section close here -->

</div>
 <div class="clearfix"></div>
<!-- service html start here -->
 <div class="service_dcm">
         <div class="services_main">
            <div class="container">
               <div class="service_title">
                 
                  <h3><!-- We Provide Awesome Services  --></h3>
               </div>
            </div>
         </div>
         <div class="row_service">
            <div class="container">
            <div class="row">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 last_service_row">
                  <div class="sub_service sub_emty">
                     <div class="content_service">
                        <h4><?php the_field('services_heading',479);?></h4>
                        <p><?php the_field('services_content',479);?></p>
                     </div>
                     <div class="right_arrow">
                        <a href="javascript:void(0);"><i class="fas fa-long-arrow-alt-right"></i></a>
                     </div>
                  </div>
               </div>
               
               <?php if( have_rows('all_services',479) ): ?>

				<?php while( have_rows('all_services',479) ): the_row(); 

					$image = get_sub_field('services_image',479);
					$content = get_sub_field('services_description',479);
					$name = get_sub_field('services_name',479);
					$link = get_sub_field('services_link',479);

					?>
		       <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                  <div class="sub_service1">
                     <div class="img_service_dv">                  
                        <a href="/<?php echo $link;?>/"> <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                     </div>
                     <div class="content_service">
                        <h4><?php echo $name;?></h4>
                        <p><?php echo $content;?></p>
                     </div>
                     <div class="right_arrow">
                        <a href="/<?php echo $link;?>/"><i class="fas fa-long-arrow-alt-right"></i></a>
                     </div>
                  </div>
               </div>
					
				<?php endwhile; ?>

			<?php endif; ?>
			        </div>
            </div>
         </div>
      </div>

<!-- service html end here -->

<!-- testimonial start here -->

 <div class="testimonial_section device_div">
         <div class="container-fluid">
            <div class="row device_row">
            	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
                        <div class="carousel-inner testimonials-banner" role="listbox">
            	<?php           
                $result_query = new WP_Query(
                            array('post_type' => 'testimonials',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        ); 
                        $active_class = "active"; ?>
                        <?php 
                      while ($result_query->have_posts()) : $result_query->the_post();
                      //$title = get_the_title(); 
                      $the_content =  get_the_content();
                     $portfolio_img = get_field('portfolio_image');
                      $client_name = get_field('client_name');
                      $client_desig = get_field('client_designation');
                      $few_words = get_field('few_words');
                      $url = get_field('url');
                     ?>
                      <div class="item <?php echo $active_class; ?>">
               <div class="col-lg-6 col-md-5 col-sm-6 col-xs-6 cm_ht testimonial_home_left">
                  <div class="mobile_img">
                 
                     <img class="img-responive" src="<?php echo $portfolio_img['url']; ?>" class="img-responsive">
                  </div>
               </div>
             
               <div class="col-lg-6 col-md-7 col-sm-6 col-xs-6">
                  <div class="testimonial_dv">
                     <h5>CLIENT FEEDBACK</h5>
                     <h4><?php echo $few_words;?></h4>
                     <h3><?php echo $client_name;?>, <?php echo $client_desig;?></h3>
                     <P><?php echo $the_content; ?></P>
                     <ul class="magento_erp">
                        <li class="icon"><i class="fas fa-tag"></i></li>
                        <li><?php
						// Get a list of terms for this post's custom taxonomy.
						$project_cats = get_the_terms($post->ID, 'keyfeature');
						// Renumber array.
						$project_cats = array_values($project_cats);
						for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
						  <?php  echo $project_cats[$cat_count]->name;?>
						  	  <?php  if ($cat_count<count($project_cats)-1){
						        echo '| ';
						    }
						}
						?></li>
                        </ul>
                     <div class="button_call">
                        <a href="/<?php echo $url;?>/">VIEW PROJECT</a>
                     </div>
                  </div>
               </div>
               </div>
               <?php 
                   $active_class = ""; 
                   endwhile; ?>
            
             <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
        
      </div>
   </div>   
</div>
<!-- testimonial end here -->
<!-- Industries section start here -->
 <div class="indus_creative">
         <div class="container-fluid">
            <div class="row row_sub_indus">
            <div class="digital_design_main col-sm-6 col-sm-offset-6 mob_hide">
            <div class="design_tittle">
                        <h4>INDUSTRIES</h4>
                        <h3>Some of the industries we know best are:</h3>
                     </div>
                     </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="lappy_img">
                    <img class="media-object" src="<?php bloginfo('template_url');?>/images/industry-section.png">
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6 design_view">
                  <div class="digital_design_main">
                     <div class="design_tittle">
                        <h4>INDUSTRIES</h4>
                        <h3>Some of the industries we know best:</h3>
                     </div>
                     <div class="design_content">
                        <div class="media">
                           <div class="media-left">
                              <a href="/marketplaces/">
                              <img class="media-object" src="<?php bloginfo('template_url');?>/images/marketplace.png">
                              </a>
                           </div>
                           <div class="media-body">
                              <h4 class="media-heading">Marketplaces</h4>
                             <p>Capture your market with a simple, secure and scalable platform backed by a clear monetization strategy and a unique shopping experience.</p>
                           </div>
                        </div>
                     </div>
                     <div class="design_content">
                        <div class="media">
                           <div class="media-left">
                              <a href="/industrial-parts-machinery/">
                             <img class="media-object" src="<?php bloginfo('template_url');?>/images/parts-industry.png">
                              </a>
                           </div>
                           <div class="media-body">
                              <h4 class="media-heading">Industrial Parts & Machinery</h4>
                             <p> Update your business with an innovative online sales platform that will attract new customers, support existing ones and drive revenue. </p>
                           </div>
                        </div>
                     </div>
                     <div class="design_content">
                        <div class="media">
                           <div class="media-left">
                              <a href="/food-perishables/">
                              <img class="media-object" src="<?php bloginfo('template_url');?>/images/food.png">
                              </a>
                           </div>
                           <div class="media-body">
                              <h4 class="media-heading">Food & Perishables</h4>
                             <p>Express your brand’s unique flavor with a delicious-looking website that streamlines the shopping, sales, and shipping processes.</p>
                           </div>
                        </div>
                     </div>
                     <div class="design_content none_wala">
                        <div class="media">
                           <div class="media-left">
                              <a href="/b2b-ecommerce/">
                              <img class="media-object" src="<?php bloginfo('template_url');?>/images/b2b.png">
                              </a>
                           </div>
                           <div class="media-body">
                              <h4 class="media-heading">B2B eCommerce</h4>
                             <p>Launch a custom sales platform that optimizes your eCommerce channel for buying efficiency, customer service, and sustainable growth.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
<!-- solutions section end here -->
   





<!-- awards section start here -->


      <div class="carousel_main">
         <div class="container">
            <div class="carousel_title">
               <h4>AWARDS</h4>
               <h3>Word got out that our work supports client success.</h3>
            </div>
       <section class="slider">
 <?php if( have_rows('press_and_awards',479) ): ?>
          
        <?php while( have_rows('press_and_awards',479) ): the_row(); 

          $logo = get_sub_field('logo',479);
          ?>
                 <img src="<?php echo $logo['url']; ?>" alt="<?php echo $image['alt'] ?>"/>
                    <?php endwhile; ?>
      <?php endif; ?>    
   </section>
        </div>
      </div>

        

<!-- awards section End here -->




<!-- blog html start here -->
 <div class="blog_dcm">
         <div class="container">
            <div class="blog_title">
               <h3 class="blog"><?php the_field('blog_title',479);?></h3>
               <h2><?php the_field('blog_content',479);?></h2>
            </div>
            <div class="owl-carousel_work">
              
                 <?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 4, // Number of recent posts thumbnails to display
        'post_status' => 'publish' // Show only the published posts
    ));
 $i=1;
    foreach($recent_posts as $post) : 
         ?>
         
        <div class="item">
        <div class="inner_arr">
                  <div class="column_blog" id="homeblog<?php echo $i++; ?>">                  
                     <p><?php echo get_the_author(); ?></p>
                     <h4><?php echo $post['post_title'] ?></h4>
                     <p class="ecommence"><?php
            // Get a list of terms for this post's custom taxonomy.
            $project_cats = get_the_terms($post->ID, 'post_tag');
            // Renumber array.
            $project_cats = array_values($project_cats);
            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
              <?php  echo $project_cats[$cat_count]->name;?>
                  <?php  if ($cat_count<count($project_cats)-1){
                    echo ', ';
                }
            }
            ?></p>
            <a href="<?php echo the_permalink();?>" class="hidden-lg">Read More</a>
                 
                 
                  
                      </div>
                       <div class="right_arrow">
                         <a href="<?php echo get_permalink($post['ID']) ?>"><i class="fas fa-long-arrow-alt-right"></i></a>
                     </div>
                       </div>
               </div>
           
    <?php 
 
    endforeach;
     ?> 

            </div>
         </div>
      </div>
<!-- blog html end here -->


<!-- about_us_sections section start here-->

    <div class="clearfix"></div>
   <div class="about_us_sections row">
   <div class="containers">
   	<div class="col-md-6 col-sm-6 mob_img">
   <!-- 	<img class="media-object img-responsive" src="<?php bloginfo('template_url');?>/img/home_abt.jpg"> -->
   <div class="media-object"><?php the_field('about_us_video',479);?></div>
   	</div>
   	    	<div class="col-md-6 col-sm-6 mob_content">
   	  	<div class="abt_us_cnt">
   	  		<h2>ABOUT US</h2>
   	  		<h4>Welcome to DotcomWeavers</h4>
   	  		<p>We’re a team of passionate and experienced people focused on one goal: helping our clients succeed online. Our blend of a transparent “get stuff done” philosophy with a real love for what we do means your best interests always come first. Together, we’ll create a web solution that supports your business goals - and we’ll have fun along the way!</p>
   	  		<!-- <a href="#">SEE US IN ACTION</a> -->
          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><a href="/why-dotcomweavers/">SEE US IN ACTION</a></button>
   	  	</div>
   	</div>
   	   	</div>
   </div>



<!-- about_us_sections section End here-->


<!-- modal-->
<div id="myModal" class="modal fade" role="dialog" style="background:rgba(0,0,0,0.7)">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <video width="100%" controls>
      <source src="<?php bloginfo('template_url');?>/images/DCW_Corporate.mp4" type="video/mp4">
      <source src="<?php bloginfo('template_url');?>/images/DCW_Corporate Video.mp4" type="video/ogg">
</video>
      </div>
     
    </div>

  </div>
</div>






<?php }?>

<script>
   var jQ = jQuery.noConflict();
  jQ(".homban_carousel11").css("height", jQ(window).height()-61 + "px");
  
</script>
<script>
//   var jQ = jQuery.noConflict();
//     alert("hi");
//     jQ('.owl-carousel').owlCarousel({
//     loop:true,
//     center: true,
//      stagePadding: 200,
//     margin:100,

//     nav:false,
//     fluidSpeed:true,
//      autoplay:true,
//        autoWidth:true,
//         responsiveClass:true,
//          slideSpeed: 200
       
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:3
//         },
//         1000:{
//             items:5
//         }
//     }
// })
var jQ = jQuery.noConflict();
jQ('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    center: true,
    nav:true,
    stagePadding: 200,
    margin:100,
     nav:false,
     fluidSpeed:true,
    autoplay:true,
     autoWidth:true,
      responsiveClass:true,
      slideSpeed: 200,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});


jQ(function(){
    jQ('.slider').slick({

        speed: 6000,
        autoplay: true,
        autoplaySpeed: 0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        prevArrow: false,
        nextArrow: false,
         initialSlide: 3,
         pauseOnHover:true,
         pauseOnDotsHover:true 

    });
});



</script>
 
 <script>

var jQ = jQuery.noConflict();
jQ('.owl-carousel_work').owlCarousel({
       //center:true,
       //margin:10,
      //autoplay:false,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive : {
    // breakpoint from 0 up
    


0 : {
       loop:true,
        items:1,
        margin:5,
        stagePadding: 15,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
          
    },
    // breakpoint from 480 up
    480 : {
        loop:true,
        items:1,
        margin:5,
        stagePadding:50,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
        
    },
    768 : {
       loop:true,
        items:2,
        margin:5,
        stagePadding:5,
        center:true,
        smartSpeed:300,
        // nav:true
        autoplay:true,
        autoplayTimeout:1000
        //autoplayHoverPause:true,
        
    },
    // breakpoint from 768 up
    
    992 : {
        items:4,
        margin:5,
        stagePadding:5,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1024 : {
        items:2,
        margin:5,
        stagePadding:100,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1200 : {
      items : 4,
       autoplay:false,
       autoplayTimeout:1000,
      autoplayHoverPause:true,
      touchDrag: false,
      mouseDrag: false
        
    }
    }
})

</script>
<?php get_footer(); ?>

