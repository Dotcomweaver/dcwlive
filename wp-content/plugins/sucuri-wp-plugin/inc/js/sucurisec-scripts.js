jQuery(document).ready(function(){
    jQuery('.sucuri-alert .close').on('click', function(e){
        e.preventDefault();
        var close_btn = jQuery(this);
        close_btn.parent().remove();
    });

    if(
        sucurisec_vars.valid_api_key!=1
        && sucurisec_vars.current_page!='sucurisec'
        && sucurisec_vars.current_page!='sucurisec_settings'
    ){
        jQuery('#toplevel_page_sucurisec').pointer({
            content: "<h3>Sucuri Security</h3>"
                + "<p>You've just installed Sucuri Security plugin. Start by clicking the <strong>Activate Service</strong> button."
                + "Then you can go through all the tools available in this plugin to improve the security of your website.</p>",
            pointerWidth: 300,
            position: { edge: 'left', align: 'left' },
            buttons: function(event, t){
                buttonElem = jQuery('<a id="pointer-close" class="button-primary">Activate Service</a>');
                buttonElem.bind('click.pointer', function(){
                    window.location.href = sucurisec_vars.admin_url+'/admin.php?page=sucurisec_settings';
                });
                return buttonElem;
            }
        }).pointer('open');
    }
});
