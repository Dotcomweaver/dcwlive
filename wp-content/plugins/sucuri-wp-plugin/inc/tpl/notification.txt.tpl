Subject: %%SUCURI.Subject%%

Information:
User: %%SUCURI.User%%
Time: %%SUCURI.Time%%

Website Information:
Site: %%SUCURI.Website%%
IP Address: %%SUCURI.RemoteAddress%%

Notification Message:
%%SUCURI.Message%%
