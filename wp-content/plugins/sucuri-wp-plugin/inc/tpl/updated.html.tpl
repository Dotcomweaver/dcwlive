<div id="poststuff">
    <div class="postbox">
        <h3>Plugin updated!</h3>
        <div class="inside">
            <p>The update process was finished. Reload the page or go to the Plugins section to verify if the version of the Plugin was updated.</p>
        </div>
    </div>
</div>
