<div id="poststuff">
    <div class="postbox">
        <h3>Plugin updates available</h3>
        <div class="inside">
            <p>You have automatic updates disabled and new updates for this plugins are available. If you want to update this plugin now, click the button.</p>

            <form method="post" class="sucuri-manual-update">
                <input type="submit" name="sucuri_update_plugin_manually" value="Sucuri WP Plugin - Update now!" class="button button-primary button-hero" />
            </form>
        </div>
    </div>
</div>
