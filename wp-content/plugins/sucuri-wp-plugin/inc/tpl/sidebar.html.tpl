<div class="postbox-container" style="width:25%;min-width:200px;max-width:350px;">
    <div id="sidebar">
        <div id="sitecleanup" class="sucuriscan-sidebar">
            <h2><span class="promo">Need extra security for your site?</span></h2>
            <p><a href="https://dashboard.sucuri.net/cloudproxy/">Sign up</a> to Sucuri's CloudProxy Web Firewall
            </p>
            <p>
            <a class="button-primary" href="https://dashboard.sucuri.net/cloudproxy/">Read more &#187;</a>
            </p>
        </div>

        <div id="sucuri-latest-posts" class="sucuriscan-sidebar">
            <h2><span class="promo">Always keep a backup of your site!</span></h2>
            <p>Check out the <a href="https://dashboard.sucuri.net/backups/">Sucuri Backups</a>!
            </p>
            <p>
            <a class="button-primary" href="https://dashboard.sucuri.net/backups/">Read more &#187;</a>
            </p>
        </div>

    </div>
</div>
