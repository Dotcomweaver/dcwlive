<?php
/* Sucuri Security WordPress Plugin
 * Copyright (C) 2010-2013 Sucuri Inc. - http://sucuri.net
 * Released under the GPL - see LICENSE file for details.
 */

if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
{
    exit;
}

// Check if options exist and delete it
if ( get_option( 'sucuri_wp_re' ) != false ) {
    delete_option( 'sucuri_wp_re' );
}

if ( get_option( 'sucuri_wp_key' ) != false ) {
    delete_option( 'sucuri_wp_key' );
}
?>
