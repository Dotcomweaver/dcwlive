<?php
/*
Plugin Name: Sucuri Security
Plugin URI: http://wordpress.sucuri.net/
Description: Sucuri Security WordPress Plugin by Sucuri Inc - This plugin will monitor your WordPress installation for the latest attacks and provide visibility to what is happening inside (auditing). It will also keep track of system events, like logins, logouts, failed logins, new users, file changes, etc. When an attack is detected it will also block the IP address from accessing the site.
Author: Sucuri, INC
Network: true
Version: 4.5
Author URI: http://sucuri.net
*/


/* Sucuri Security WordPress Plugin
 * Copyright (C) 2010-2013 Sucuri Inc. - http://sucuri.net
 * Released under the GPL - see LICENSE file for details.
 */


/* No direct access. */
if(!function_exists('add_action'))
{
    exit(0);
}

@set_time_limit(0);
@ini_set('memory_limit', sucuri_memory_limit());
@ini_set('max_execution_time', 0);
@ignore_user_abort(TRUE);

/* Constants. */
define("SUCURIWPSTARTED", TRUE);
define('SUCURI','sucurisec' );
define('SUCURI_VERSION','4.5');
define('SUCURI_DEBUG', FALSE);
if( get_option('sucuri_wp_force_http')=='enabled' ){
    define('SUCURI_REMOTE_URL', 'http://wordpress.sucuri.net/');
    define('SUCURI_FORCE_HTTP', TRUE);
}else{
    define('SUCURI_REMOTE_URL', 'https://wordpress.sucuri.net/');
    define('SUCURI_FORCE_HTTP', FALSE);
}
define('SUCURI_PLUGIN_FILE', 'sucuri.php');
define('SUCURI_PLUGIN_FOLDER', 'sucuri-wp-plugin');
define('SUCURI_PLUGIN_PATH', WP_PLUGIN_DIR.'/'.SUCURI_PLUGIN_FOLDER);
define('SUCURI_PLUGIN_FILEPATH', SUCURI_PLUGIN_PATH.'/'.SUCURI_PLUGIN_FILE);
define('SUCURI_REMOTE_VERSION_URL', SUCURI_REMOTE_URL.'LATESTVERSION.txt');
define('SUCURI_REMOTE_MD5FILE_URL', SUCURI_REMOTE_URL.'LATESTMD5.txt');
define('SUCURI_REMOTE_PACKAGE_URL', SUCURI_REMOTE_URL.'sucuri-wp-plugin.zip');
define('SUCURI_PLUGIN_URL', rtrim(plugin_dir_url(SUCURI_PLUGIN_FOLDER.'/'.SUCURI_PLUGIN_FILE),'/'));
define('SUCURI_IMG', SUCURI_PLUGIN_URL.'/inc/images/');
define('SUCURI_LASTLOGINS_USERSLIMIT', 100);

add_action( 'admin_enqueue_scripts', 'sucuriwpp_admin_script_style_registration', 1 );
function sucuriwpp_admin_script_style_registration() { ?>
    <script type="text/javascript">
    var sucurisec_vars = {
        base_url: '<?php echo site_url(); ?>',
        admin_url: '<?php echo rtrim(admin_url(),"/"); ?>',
        valid_api_key: <?php echo sucuri_valid_api_key() ? 1 : 0; ?>,
        current_page: '<?php if( isset($_GET["page"]) ){ echo htmlentities($_GET["page"]); } ?>',
    }
    </script>
    <?php
    wp_enqueue_style('wp-pointer');
    wp_enqueue_script('wp-pointer');
    wp_enqueue_style(SUCURI, SUCURI_PLUGIN_URL.'/inc/css/sucuriscan-default-css.css');
    wp_enqueue_script(SUCURI, SUCURI_PLUGIN_URL.'/inc/js/sucurisec-scripts.js', array('jquery'), SUCURI_VERSION);
    ?>
<?php }

/* Requires files. */
//add_action('add_attachment', 'sucuri_add_attachment', 50);
add_action('create_category', 'sucuri_create_category', 50);
add_action('delete_post', 'sucuri_delete_post', 50);
add_action('private_to_published', 'sucuri_private_to_published', 50);
add_action('publish_page', 'sucuri_publish_post', 50);
add_action('publish_post', 'sucuri_publish_post', 50);
add_action('publish_phone', 'sucuri_publish_post', 50);
add_action('xmlrpc_publish_post', 'sucuri_publish_post', 50);
add_action('add_link', 'sucuri_add_link', 50);
add_action('switch_theme', 'sucuri_switch_theme', 50);
add_action('delete_user', 'sucuri_delete_user', 50);
add_action('retrieve_password', 'sucuri_retrieve_password', 50);
add_action('user_register', 'sucuri_user_register', 50);
add_action('wp_login', 'sucuri_wp_login', 50);
add_action('wp_login_failed', 'sucuri_wp_login_failed', 50);
add_action('login_form_resetpass', 'sucuri_reset_password', 50);
add_action('admin_head', 'sucuri_admin_notice', 50);
add_action('admin_init', 'sucuri_activate_autoupdate');
add_action('admin_head', 'sucuri_update_message');
add_action('admin_head', 'sucuri_remove_old_updates');

/* sucuri_dir_filepath:
 * Returns the system filepath to the relevant user uploads
 * directory for this site. Multisite capable.
 */
function sucuri_dir_filepath($path = '')
{
    $wp_dir_array = wp_upload_dir();
    $wp_dir_array['basedir'] = untrailingslashit($wp_dir_array['basedir']);
    return($wp_dir_array['basedir']."/sucuri/$path");
}



/* sucuri_debug_log:
 * Prints a debug message inside debug_log.php.
 */
function sucuri_debug_log($msg)
{
    if( defined('SUCURI_DEBUG') && SUCURI_DEBUG===TRUE ){
        if(!is_file(sucuri_dir_filepath('debug_log.php' )))
        {
            @file_put_contents(sucuri_dir_filepath('debug_log.php'), "<?php exit(0);\n");
        }
        @file_put_contents(sucuri_dir_filepath('debug_log.php'), date('Y-m-d h:i:s ')."$msg\n", FILE_APPEND);
    }
}



/* sucuri_plugin_activation:
 * Creates the internal files / directories used by the plugin.
 * Returns 0 on error and 1 on success.
 */
function sucuri_plugin_activation()
{
    $wp_dir_array = wp_upload_dir();
    if(!is_dir($wp_dir_array['basedir']))
    {
        @mkdir($wp_dir_array['basedir']);
        if(!is_dir($wp_dir_array['basedir']))
        {
            return(0);
        }
    }
    @mkdir(sucuri_dir_filepath());
    @touch(sucuri_dir_filepath('index.html'));
    @mkdir(sucuri_dir_filepath('blocks'));
    @mkdir(sucuri_dir_filepath('whitelist'));
    @touch(sucuri_dir_filepath('blocks/index.html'));
    @touch(sucuri_dir_filepath('whitelist/index.html'));
    @file_put_contents(sucuri_dir_filepath('.htaccess'), "\ndeny from all\n");

    if(!is_file(sucuri_dir_filepath('blocks/blocks.php')))
    {
        @file_put_contents(sucuri_dir_filepath('blocks/blocks.php'), "\n<?php exit(0);\n\n", FILE_APPEND);
    }
    return(1);
}

function sucuri_plugin_deactivation()
{
    return TRUE;
}

function sucuri_read_block_list_from_dir()
{
    $block_ips = array();
    $blockdir = sucuri_dir_filepath('blocks');
    $dh = @opendir($blockdir);
    if(!$dh)
    {
        return(0);
    }

    while(($myfile = @readdir($dh)) !== false)
    {
        if($myfile == "." || $myfile == ".." || $myfile == "index.html" || $myfile == "blocks.php")
        {
            continue;
        }
        $block_ips[] = $blockdir.'/'.$myfile;
    }
    return $block_ips;
}

function sucuri_get_wp_email(){
    $sucuri_wp_email = get_option('sucuri_wp_email');
    if( $sucuri_wp_email==FALSE || !sucuri_is_email($sucuri_wp_email) ){
        $sucuri_wp_email = get_option('admin_email');
    }
    return $sucuri_wp_email;
}

function sucuri_alerts($alert, $subject, $message, $data_set=array())
{
    $sucuri_wp_email = sucuri_get_wp_email();
    $alert_type = get_option($alert);

    if($alert_type !== FALSE && $alert_type == 'enabled')
    {
        sucuri_send_mail($sucuri_wp_email, $subject, $message, $data_set);
    }
}

function sucuri_send_mail($to='', $subject='', $message='', $data_set=array(), $debug=FALSE)
{
    $headers = array();
    $subject = ucwords(strtolower($subject));
    $wp_domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : get_option('siteurl');
    if( get_option('sucuri_wp_prettify_mails')!='disabled' ){
        $headers = array( 'Content-type: text/html' );
        $data_set['PrettifyType'] = 'html';
    }
    $message = sucuri_prettify_mail($subject, $message, $data_set);

    if($debug){
        die($message);
    }else{
        wp_mail($to, "Sucuri WP Notification: {$wp_domain}: {$subject}" , $message, $headers);
    }
}

function sucuri_is_email($address='')
{
    /* Wordpress::is_email() doesn't works very well, test it with: '...false@mailinator.com' */
    $address = htmlspecialchars(trim($address));
    return (bool) preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix', $address);
}

function sucuri_admin_notice($type='updated', $message='')
{
    $css_class = 'sucuri-alert-'.$type;
    if( !empty($message) ): ?>
        <div class="<?php echo $type; ?> sucuri-alert <?php echo $css_class; ?>">
            <a href="#" class="close">&times;</a>
            <p><?php _e($message); ?></p>
        </div>
    <?php endif;
}

function sucuri_prettify_mail($subject='', $message='', $data_set=array())
{
    $current_user = wp_get_current_user();

    $prettify_type = isset($data_set['PrettifyType']) ? $data_set['PrettifyType'] : 'txt';
    $real_ip = isset($_SERVER['SUCURI_RIP']) ? $_SERVER['SUCURI_RIP'] : $_SERVER['REMOTE_ADDR'];

    $mail_variables = array(
        'TemplateTitle'=>'Sucuri WP Notification',
        'Subject'=>$subject,
        'Website'=>get_option('siteurl'),
        'RemoteAddress'=>$real_ip,
        'Message'=>$message,
        'User'=>$current_user->display_name,
        'Time'=>current_time('mysql')
    );
    foreach($data_set as $var_key=>$var_value){
        $mail_variables[$var_key] = $var_value;
    }

    return sucuri_get_template("notification.{$prettify_type}.tpl", $mail_variables);
}

function sucuri_get_template($template='', $template_variables=array()){
    $template_content = '';
    $template_path =  WP_PLUGIN_DIR.'/'.SUCURI_PLUGIN_FOLDER."/inc/tpl/{$template}";

    if( file_exists($template_path) && is_readable($template_path) ){
        $template_content = file_get_contents($template_path);
        foreach($template_variables as $tpl_key=>$tpl_value){
            $template_content = str_replace("%%SUCURI.{$tpl_key}%%", $tpl_value, $template_content);
        }
    }
    return $template_content;
}

function sucuri_wp_sidebar_gen()
{
    return sucuri_get_template('sidebar.html.tpl');
}

function sucuri_get_new_config_keys()
{
    $request = wp_remote_get('https://api.wordpress.org/secret-key/1.1/salt/');
    if( !is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200 ){
        if( preg_match_all("/define\('([A-Z_]+)',[ ]+'(.*)'\);/", $request['body'], $match) ){
            $new_keys = array();
            foreach($match[1] as $i=>$value){
                $new_keys[$value] = $match[2][$i];
            }
            return $new_keys;
        }
    }
    return FALSE;
}

function sucuri_get_wpconfig_path(){
    $wp_config_path = ABSPATH.'wp-config.php';

    // if wp-config.php doesn't exist/not readable check one directory up
    if( !is_readable($wp_config_path)){
        $wp_config_path = ABSPATH.'/../wp-config.php';
    }
    return $wp_config_path;
}

function sucuri_get_htaccess_path(){
    $base_dirs = array(
        rtrim(ABSPATH, '/'),
        dirname(ABSPATH),
        dirname(dirname(ABSPATH))
    );

    foreach($base_dirs as $base_dir){
        $htaccess_path = sprintf('%s/.htaccess', $base_dir);
        if( file_exists($htaccess_path) ){
            return $htaccess_path;
        }
    }

    return FALSE;
}

function sucuri_set_new_config_keys()
{
    $new_wpconfig = '';
    $wp_config_path = sucuri_get_wpconfig_path();

    if( file_exists($wp_config_path) ){
        $wp_config_lines = file($wp_config_path);
        $new_keys = sucuri_get_new_config_keys();
        $old_keys = array();
        $old_keys_string = $new_keys_string = '';

        foreach($wp_config_lines as $wp_config_line){
            $wp_config_line = str_replace("\n", '', $wp_config_line);

            if( preg_match("/define\('([A-Z_]+)',([ ]+)'(.*)'\);/", $wp_config_line, $match) ){
                $key_name = $match[1];
                if( array_key_exists($key_name, $new_keys) ){
                    $white_spaces = $match[2];
                    $old_keys[$key_name] = $match[3];
                    $wp_config_line = "define('{$key_name}',{$white_spaces}'{$new_keys[$key_name]}');";

                    $old_keys_string .= "define('{$key_name}',{$white_spaces}'{$old_keys[$key_name]}');\n";
                    $new_keys_string .= "{$wp_config_line}\n";
                }
            }

            $new_wpconfig .= "{$wp_config_line}\n";
        }

        $response = array(
            'updated'=>is_writable($wp_config_path),
            'old_keys'=>$old_keys,
            'old_keys_string'=>$old_keys_string,
            'new_keys'=>$new_keys,
            'new_keys_string'=>$new_keys_string,
            'new_wpconfig'=>$new_wpconfig
        );
        if( $response['updated'] ){
            file_put_contents($wp_config_path, $new_wpconfig, LOCK_EX);
        }
        return $response;
    }
    return FALSE;
}

function sucuri_new_password($user_id=0)
{
    $user_id = intval($user_id);
    $current_user = wp_get_current_user();

    if( $user_id>0 && $user_id!=$current_user->ID ){
        $user = get_userdata($user_id);
        $new_password = wp_generate_password(15, TRUE, FALSE);

        $data_set = array( 'User'=>$user->display_name );
        $message = "The password for your user account in the website mentioned has been changed by an administrator,
            this is the new password automatically generated by the system, please update ASAP.<br>
            <div style='display:inline-block;background:#ddd;font-family:monaco,monospace,courier;
            font-size:30px;margin:0;padding:15px;border:1px solid #999'>{$new_password}</div>";
        sucuri_send_mail($user->user_email, 'Changed password', $message, $data_set);

        wp_set_password($new_password, $user_id);

        return TRUE;
    }
    return FALSE;
}

function sucuri_is_multisite(){
    if( function_exists('is_multisite') && is_multisite() ){ return TRUE; }
    return FALSE;
}

function sucuri_get_wpversion()
{
    if( $version = get_option('version') ){
        return $version;
    }else{
        $wp_version_path = ABSPATH.WPINC.'/version.php';
        include($wp_version_path);

        if( preg_match('/^(\d+)$/', $wp_version) ){
            return md5_file(ABSPATH.WPINC.'/class-wp.php');
        }

        return $wp_version;
    }
}

function sucuri_get_remoteaddr()
{
    $alternatives = array(
        'HTTP_X_REAL_IP',
        'HTTP_CLIENT_IP',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_FORWARDED_FOR',
        'HTTP_FORWARDED',
        'REMOTE_ADDR',
        'SUCURI_RIP',
    );
    foreach($alternatives as $alternative){
        if( !isset($_SERVER[$alternative]) ){ continue; }

        $remote_addr = preg_replace('/[^0-9., ]/', '', $_SERVER[$alternative]);
        if($remote_addr) break;
    }

    return $remote_addr;
}

function sucuri_is_valid_ip($ip_address=''){
    return filter_var($ip_address, FILTER_VALIDATE_IP) ? TRUE : FALSE;
}

function sucuri_is_behind_cloudproxy(){
    $http_host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
    if( preg_match('/^(.*):([0-9]+)/', $http_host, $match) ){ $http_host = $match[1]; }
    $host_by_name = gethostbyname($http_host);
    $host_by_addr = gethostbyaddr($host_by_name);

    if(
        isset($_SERVER['SUCURIREAL_REMOTE_ADDR'])
        || preg_match('/^cloudproxy([0-9]+)\.sucuri\.net$/', $host_by_addr)
    ){
        return TRUE;
    }

    return FALSE;
}

function sucuri_get_options($option_name=''){
    global $wpdb;

    $settings = array();
    $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}options WHERE option_name LIKE 'sucuri%' ORDER BY option_id ASC");
    foreach($results as $row){
        $settings[$row->option_name] = $row->option_value;
    }

    if( !isset($settings['sucuri_wp_key']) ){
        $settings['sucuri_wp_key'] = FALSE;
    }
    if( !isset($settings['sucuri_wp_email']) ){
        $settings['sucuri_wp_email'] = get_option('admin_email');
    }
    if( !isset($settings['sucuri_scan_frequency']) ){
        $settings['sucuri_scan_frequency'] = 'hourly';
    }
    if( !isset($settings['sucuri_memory_limit']) ){
        $settings['sucuri_memory_limit'] = 2048;
    }

    if( !empty($option_name) ){
        if( isset($settings[$option_name]) ){
            return $settings[$option_name];
        }
        return FALSE;
    }

    return $settings;
}

function sucuri_get_wp_options(){
    global $wpdb;

    $settings = array();
    $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}options WHERE option_name NOT LIKE '%_transient_%' ORDER BY option_id ASC");
    foreach($results as $row){
        $settings[$row->option_name] = $row->option_value;
    }

    return $settings;
}

function sucuri_what_options_were_changed($request=array()){
    $options_changed = array(
        'original' => array(),
        'changed' => array()
    );
    $wp_options = sucuri_get_wp_options();
    foreach($request as $req_name=>$req_value){
        if(
            array_key_exists($req_name, $wp_options)
            && $wp_options[$req_name]!=$req_value
        ){
            $options_changed['original'][$req_name] = $wp_options[$req_name];
            $options_changed['changed'][$req_name] = $req_value;
        }
    }
    return $options_changed;
}

function sucuri_valid_api_key(){
    $api_key = sucuri_get_options('sucuri_wp_key');
    $valid_api_key = sucuri_get_options('sucuri_valid_api_key');

    if( $api_key ){
        if( $valid_api_key==1 ){ return TRUE; }

        if( preg_match('/^[a-zA-Z0-9]+$/', $api_key) ){
            $response = sucuri_send_log($api_key, 'INFO: Validating Sucuri API Key.');
            if( $response==1 ){
                update_option('sucuri_valid_api_key', 1);
                return TRUE;
            }
        }
    }

    update_option('sucuri_valid_api_key', 0);
    return FALSE;
}

function sucuri_memory_limit($return_int=FALSE){
    $default = 1024 * 2;
    $memory_limit = sucuri_get_options('sucuri_memory_limit');

    if( !$memory_limit || !is_numeric($memory_limit) ){
        $memory_limit = $default;
        update_option('sucuri_memory_limit', 2048);
    }

    if( $return_int ){ return $memory_limit; }
    else{ return $memory_limit.'M'; }
}

function sucuri_activate_autoupdate(){
    $sucuri_autoupdate = new SucuriAutoUpdate();
    $sucuri_autoupdate->initialize();
}

function sucuri_checkfor_updates(){
    $sucuri_autoupdate = new SucuriAutoUpdate();

    // Check for updates the first time or if it has passed some hours.
    if( $sucuri_autoupdate->is_update_needed() ){
        update_option('sucuri_wp_last_update', time());
        $current = get_site_transient('update_plugins');
        $transient = $sucuri_autoupdate->force_check_update();
        $current->response = $transient->response;
        set_site_transient('update_plugins', $current);
    }

    else if( isset($_GET['sucuri_force_update']) ){
        $sucuri_autoupdate->force_update();
    }

    else if( sucuri_autoupdate_enabled()===FALSE ){
        $sucuri_autoupdate->manual_update();
    }
}

function sucuri_autoupdate_enabled(){
    return get_option('sucuri_wp_autoupdate')=='disabled' ? FALSE : TRUE;
}

function sucuri_remove_old_updates(){
    $sucuri_autoupdate = new SucuriAutoUpdate();
    $sucuri_autoupdate->remove_old_updates();
}

function sucuri_update_message(){
    if( $update_message_info = get_option('sucuri_update_info') ){
        sucuri_admin_notice('updated', $update_message_info);
        delete_option('sucuri_update_info');
    }
    if( $update_message_error = get_option('sucuri_update_error') ){
        sucuri_admin_notice('error', $update_message_error);
        delete_option('sucuri_update_error');
    }
}

class SucuriAutoUpdate{
    public $current_version;
    public $update_path_version;
    public $update_path;
    public $plugin_slug;
    public $slug;
    public $update_every;
    protected $user_agent;

    /**
     * Define initial variables used through the whole update process.
     *
     * It basically collect the necessary information to compare the current version
     * of the plugin installed in the site with the requested information in the
     * remote official server on Sucuri.net
     *
     * @return void
     */
    public function __construct(){
        $this->current_version = SUCURI_VERSION;
        $this->update_path = SUCURI_REMOTE_PACKAGE_URL;
        $this->update_path_version = SUCURI_REMOTE_VERSION_URL;
        $this->plugin_slug = SUCURI_PLUGIN_FOLDER.'/'.SUCURI_PLUGIN_FILE;
        $this->plugin_name = SUCURI_PLUGIN_FOLDER;
        $this->plugin_destination = plugin_dir_path(__FILE__);
        $this->slug = str_replace('.php', '', $this->plugin_slug);
        $this->user_agent = 'Sucuri-WP-Plugin/'.SUCURI_VERSION.'; +'.SUCURI_REMOTE_URL.'; '.get_bloginfo('url');
        $this->update_every = 12; // Hours
    }

    public function initialize(){
        // define the alternative API for updating checking
        add_filter('pre_set_site_transient_update_plugins', array(&$this, 'check_update'));
    }

    /**
     * Push the required information to the Wordpress transient functionality, that way
     * we can show that an update is available through the admin interface, in the plugins
     * page and the Wordpress core update page.
     *
     * @param  object $transient This is the current information available in the database.
     * @return object            After the checks passes, it returns the new transient information.
     */
    public function check_update($transient){
        if( empty($transient->checked) ){
            return $transient;
        }
        return $this->force_check_update();
    }

    /**
     * Check for the remote version number available of the plugin and compares it with
     * the current version installed in the site, if those two versions are different,
     * then an anonymous object is created and pushed to the global transient variable,
     * and Wordpress will use that information to show a message notifying the availability
     * of a new update for the Plugin.
     *
     * @return object After the checks passes, it returns the new transient information.
     */
    public function force_check_update(){
        $remote_version = $this->get_remote_version();
        $transient = get_site_transient('update_plugins');
        if( !is_object($transient) ){ $transient = new stdClass; }

        // If a newer version is available, add the update
        if( version_compare($this->current_version, $remote_version, '<') ){
            // Need to update, check if automatic updates are enabled.
            if( sucuri_autoupdate_enabled() ){
                $this->update_plugin();
            }else{
                $last_checked = time();

                $obj = new stdClass();
                $obj->slug = $this->slug;
                $obj->new_version = $remote_version;
                $obj->url = $this->update_path_version;
                $obj->package = $this->update_path;
                $obj->last_checked = $last_checked;

                // Update transient information.
                $transient->last_checked = $last_checked;
                $transient->response[$this->plugin_slug] = $obj;
            }
        }else{
            $this->reset_update_info();
        }

        return $transient;
    }

    /**
     * Retrieve the version number of the stable version of the Plugin.
     *
     * @return string The version number.
     */
    private function get_remote_version(){
        $request = wp_remote_get($this->update_path_version);
        if( !is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200 ){
            return trim($request['body']);
        }
        return false;
    }

    /**
     * It downloads the remote Zip file with the code of the plugin when an update is required.
     *
     * @param  string $remote_url URL of the remote file to be downloaded.
     * @return data               It returns the Zip file data.
     */
    private function get_remote_file($remote_url=''){
        $remote_content = FALSE;

        if( !empty($remote_url) ){
            $request = wp_remote_get($remote_url, array(
                'timeout' => 10,
                'httpversion' => '1.0',
                'user-agent' => $this->user_agent,
                'compress' => false,
                'decompress' => true
            ));
            if( !is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200 ){
                $remote_content = $request['body'];
            }
        }

        return $remote_content;
    }

    private function update_plugin(){
        $plugin_upload_folder = sucuri_dir_filepath();

        // Continue if the uploads/sucuri directory is writable.
        if( is_writable($plugin_upload_folder) ){
            $new_package_path = rtrim($plugin_upload_folder,'/').'/update-'.time().'.zip';
            $remote_content = $this->get_remote_file($this->update_path);

            if($remote_content){
                file_put_contents($new_package_path, $remote_content);
                if( file_exists($new_package_path) ){
                    if( $this->check_md5file($new_package_path) ){
                        // Configure the Wordpress filesystem object before the extract the Zip file downloaded.
                        if( !defined('FS_METHOD') ){ define('FS_METHOD', 'direct'); }
                        $wp_filesystem_configured = WP_Filesystem();
                        if( !$wp_filesystem_configured ){
                            if( !defined('FS_CHMOD_DIR')  ){ define('FS_CHMOD_DIR',  0755); }
                            if( !defined('FS_CHMOD_FILE') ){ define('FS_CHMOD_FILE', 0644); }
                        }

                        // Extract the Zip file in uploads/sucuri directory before replace the plugin code.
                        $unzipped = unzip_file($new_package_path, $plugin_upload_folder);
                        if( !is_wp_error($unzipped) ){
                            $new_plugin_folder = $plugin_upload_folder.SUCURI_PLUGIN_FOLDER;
                            $old_plugin_folder_bkp = SUCURI_PLUGIN_PATH.'-bkp-'.time();
                            if( @rename(SUCURI_PLUGIN_PATH, $old_plugin_folder_bkp) ){
                                if( @rename($new_plugin_folder, SUCURI_PLUGIN_PATH) ){
                                    update_option('sucuri_update_info', '<strong>Sucuri WP Plugin</strong>. The plugin was updated successfully.');
                                    $this->rmdir_recursive($old_plugin_folder_bkp);
                                    /* This method is causing an infinite loop in the plugins page. */
                                    $this->reset_update_info();
                                }else{
                                    update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong>. Error updating because Wordpress plugins folder is not writable, new Plugin can not be activated.');
                                }
                            }else{
                                update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong>. Error updating because Wordpress plugins folder is not writable, backup Plugin can not be created.');
                                $this->rmdir_recursive($new_plugin_folder);
                            }
                        }else{
                            update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong>. The zip file for the update could not be extracted and will be deleted.');
                        }
                    }else{
                        update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong>. Downloaded remote file does not match the official MD5 for the new version.');
                    }
                }else{
                    update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong>. Plugin folder is writable but was not able to hold the remote package.');
                }
                @unlink($new_package_path);
            }else{
                update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong>. The remote Zip file was not downloaded, the update will be executed again.');
            }
        }else{
            update_option('sucuri_update_error', '<strong>Sucuri WP Plugin</strong> upload folder is not writable, can not continue with the update: <code>'.$plugin_upload_folder.'</code>');
        }
    }

    /**
     * Generic function to remove recursively a folder as PHP doesn't has a native
     * function to do that if the folder that will be delete is not empty.
     *
     * @param  string $dir Directory relative path.
     * @return boolean     Returns a boolean value representing the success of the operation.
     */
    public function rmdir_recursive($dir=''){
        if (!file_exists($dir)) return true;
        if (!is_dir($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            if (!$this->rmdir_recursive($dir.DIRECTORY_SEPARATOR.$item)) return false;
        }
        return rmdir($dir);
    }

    /**
     * Function used when automatic updates are disabled and the button to check a new version
     * of the plugin is pressed by the administrator.
     *
     * @return boolean Returns a boolean value representing the success of the operation.
     */
    public function manual_update(){
        $update_plugins = get_site_transient('update_plugins');

        if( !isset( $update_plugins->response[$this->plugin_slug] )){
            return FALSE;
        }else{
            $old_version = isset($update_plugins->checked) ? $update_plugins->checked[$this->plugin_slug] : 0;
            $new_version = $update_plugins->response[$this->plugin_slug]->new_version;
            if( version_compare($old_version, $new_version, '<') ){
                $this->force_update();
            }
            return TRUE;
        }
    }

    /**
     * When automatic updates are disabled or when the button "force update" in the About page
     * is pressed, this function is executed and it shows a button in the Settings page from
     * where the administrator can initialize the update process manually.
     *
     * @return void
     */
    public function force_update(){
        // If user click the button to update, then process the request, else display the button.
        $template_path = '';
        if( isset($_POST['sucuri_update_plugin_manually']) ){
            $this->update_plugin();
            $template_path = 'updated.html.tpl';
        }else{
            $template_path = 'update.html.tpl';
        }
        echo sucuri_get_template($template_path);
    }

    /**
     * Verify if the md5sum value of the downloaded Zip file matches the official remote md5sum.
     *
     * @param  string $filepath The relative file path of the Zip file downloaded.
     * @return boolean          Returns a boolean value representing the success of the operation.
     */
    private function check_md5file($filepath=''){
        $valid_md5file = FALSE;
        if( file_exists($filepath) ){
            $downloaded_md5file = md5_file($filepath);
            $request = wp_remote_get(SUCURI_REMOTE_MD5FILE_URL);
            if( !is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200 ){
                $official_md5file = trim(str_replace("\n", '', $request['body']));
                $valid_md5file = $official_md5file==$downloaded_md5file ? TRUE : FALSE;
            }
        }
        return $valid_md5file;
    }

    /**
     * This function is supposed to clean the global transient variable removing the update
     * information once the plugin is updated. Use it at your own risk as this is not working
     * as expected.
     *
     * @return void
     */
    public function reset_update_info(){
        $current = get_site_transient('update_plugins');
        if( isset($current->response[$this->plugin_slug]) ){
            $current->last_checked = time();
            unset($current->response[$this->plugin_slug]);
            set_site_transient('update_plugins', $current);
        }
    }

    /**
     * Check every specific quantity of hours if an update is available.
     *
     * @return boolean True if the check is required.
     */
    public function is_update_needed(){
        $last_check = get_option('sucuri_wp_last_update');
        $update_every_sec = 60 * 60 * $this->update_every;
        $force_check = ( time() - $last_check ) >= $update_every_sec;

        return ( $last_check===FALSE || $force_check ) ? TRUE : FALSE;
    }

    /**
     * Remove old Zip files as those are not necessary to be in the fiel system.
     *
     * @return void
     */
    public function remove_old_updates(){
        $plugin_upload_folder = sucuri_dir_filepath();
        if( file_exists($plugin_upload_folder) ){
            $files = glob( rtrim($plugin_upload_folder,'/').'/update-*.zip');
            if( is_array($files) && !empty($files) ){
                foreach($files as $filepath){
                    @unlink($filepath);
                }
            }
        }
    }
}

class SucuriDatabase{
    public function __construct(){
    }

    /**
     * Generates a lowercase random string with an specific length.
     * @param  integer $length Length of the string that will be generated.
     * @return string          The random string generated.
     */
    public function random_char($length=4){
        $string = '';
        $chars = range('a','z');
        for( $i=0; $i<$length; $i++ ){
            $string .= $chars[ rand(0, count($chars)-1) ];
        }
        return $string;
    }

    public function human_filesize($bytes=0, $decimals=2)
    {
        /* Author: http://www.php.net/manual/en/function.filesize.php#106569 */
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    /**
     * List all database tables in a clean array of strings.
     * @return array Array of strings.
     */
    public function get_dbtables(){
        global $wpdb;

        $table_names = array();
        $tables = $wpdb->get_results('SHOW TABLES', ARRAY_N);
        foreach($tables as $table){
            $table_names[] = $table[0];
        }

        return $table_names;
    }

    /**
     * Set a new database table prefix to improve the security.
     * @return void
     */
    public function new_table_prefix(){
        $new_table_prefix = $this->random_char( rand(4,7) ).'_';
        $this->set_table_prefix($new_table_prefix);
    }

    /**
     * Reset the database table prefix with the default value 'wp_'
     * @return void
     */
    public function reset_table_prefix(){
        $this->set_table_prefix('wp_');
    }

    /**
     * Set a new table prefix and changes table names, options, configuration files, etc.
     * @param string $new_table_prefix The new table prefix.
     * @return void
     */
    private function set_table_prefix($new_table_prefix='wp_'){
        // Set the new table prefix in the configuration file.
        $wpconfig_update = $this->new_table_prefix_wpconfig($new_table_prefix);
        // Update options table with the new table prefix.
        $options_update = $this->new_table_prefix_optionstable($new_table_prefix);
        // Update usermeta table with the new table prefix.
        $usermeta_update = $this->new_table_prefix_usermetatable($new_table_prefix);
        // Rename table names with the new table prefix.
        $dbtables_renamed = $this->new_table_prefix_tablerename($new_table_prefix);

        foreach(array( $wpconfig_update, $options_update, $usermeta_update, $dbtables_renamed ) as $response){
            if( $response['process']!==TRUE ){
                sucuri_admin_notice('error', $response['message']);
            }
        }
    }

    /**
     * Using the new database table prefix, it modifies the main configuration file with that new value.
     * @param  string $new_table_prefix
     * @return array Process=Boolean, Message=String
     */
    private function new_table_prefix_wpconfig($new_table_prefix=''){
        global $wpdb;
        $response = array( 'process'=>FALSE, 'message'=>'' );

        $wp_config_path = sucuri_get_wpconfig_path();

        if( file_exists($wp_config_path) ){
            @chmod($wp_config_path, 0777);
            if( is_writable($wp_config_path) ){
                $new_wpconfig = '';
                $wpconfig_lines = file($wp_config_path);
                foreach($wpconfig_lines as $line){
                    $line = str_replace("\n", '', $line);
                    if( preg_match('/.*\$table_prefix([ ]+)?=.*/', $line, $match) ){
                        $line = str_replace($wpdb->prefix, $new_table_prefix, $match[0]);
                    }
                    $new_wpconfig .= "{$line}\n";
                }
                $handle = fopen($wp_config_path, 'w');
                @fwrite($handle, $new_wpconfig);
                @fclose($handle);
                @chmod($wp_config_path, 0644);

                $response['process'] = TRUE;
                $response['message'] = 'Main configuration file modified.';
            }else{
                $response['message'] = "Main configuration file is not writable, you will need to put the new
                    table prefix <code>{$new_table_prefix}</code> manually in <code>wp-config.php</code>.";
            }
        }else{
            $response['message'] = "Main configuration file was not located: <code>{$wp_config_path}</code>.";
        }

        return $response;
    }

    public function get_prefixed_tables($prefix=''){
        global $wpdb;

        $tables = array();
        $prefix = empty($prefix) ? $wpdb->prefix : $prefix;

        $db_tables = $this->get_dbtables();
        foreach($db_tables as $table_name){
            if( preg_match("/^{$prefix}/", $table_name) ){
                $tables[] = $table_name;
            }
        }

        return $tables;
    }

    /**
     * Using the new database table prefix, it modifies the name of all tables with the new value.
     * @param  string $new_table_prefix
     * @return array Process=Boolean, Message=String
     */
    private function new_table_prefix_tablerename($new_table_prefix=''){
        global $wpdb;
        $response = array( 'process'=>FALSE, 'message'=>'' );
        $db_tables = $this->get_prefixed_tables();

        $renamed_count = 0;
        $total_tables = count($db_tables);
        $tables_not_renamed = array();
        foreach($db_tables as $table_name){
            $table_new_name = $new_table_prefix.str_replace($wpdb->prefix, '', $table_name);
            $sql = "RENAME TABLE `%s` TO `%s`";
            if( $wpdb->query(sprintf($sql, $table_name, $table_new_name))===FALSE ){ /* Don't use WPDB->Prepare() */
                $tables_not_renamed[] = $table_name;
            }else{
                $renamed_count += 1;
            }
        }

        $response['message'] = "Database tables renamed: {$renamed_count} out of {$total_tables}.";
        if( $renamed_count>0 && $renamed_count==$total_tables ){
            $response['process'] = TRUE;
            $error = $wpdb->set_prefix($new_table_prefix);
            if( is_wp_error($error) ){
                foreach($error->errors as $error_index=>$error_data){
                    if( is_array($error_data) ){
                        foreach($error_data as $error_data_value){
                            $response['message'] .= chr(32)."{$error_data_value}.";
                        }
                    }
                }
            }
        }else{
            $response['message'] .= "<br>These tables were not renamed, you will need to do it manually:";
            $response['message'] .= chr(32).implode(','.chr(32), $table_not_renamed);
        }

        return $response;
    }

    /**
     * Using the new database table prefix, it modifies the name of all options with the new value.
     * @param  string $new_table_prefix
     * @return array Process=Boolean, Message=String
     */
    private function new_table_prefix_optionstable($new_table_prefix=''){
        global $wpdb;
        $response = array( 'process'=>TRUE, 'message'=>'' );

        $results = $wpdb->get_results("SELECT option_id, option_name FROM {$wpdb->prefix}options WHERE option_name LIKE '{$wpdb->prefix}%'");
        foreach($results as $row){
            $row->new_option_name = $new_table_prefix.str_replace($wpdb->prefix, '', $row->option_name);
            $sql = "UPDATE {$wpdb->prefix}options SET option_name=%s WHERE option_id=%s LIMIT 1";
            if( $wpdb->query($wpdb->prepare($sql, $row->new_option_name, $row->option_id))===FALSE ){
                $response['process'] = FALSE;
            }
        }
        $response['message'] = $response['process']
            ? 'Database table options updated.'
            : 'Some entries in the database table <strong>Options</strong> were not updated';

        return $response;
    }

    /**
     * Using the new database table prefix, it modifies the name of all usermeta keys with the new value.
     * @param  string $new_table_prefix
     * @return array Process=Boolean, Message=String
     */
    private function new_table_prefix_usermetatable($new_table_prefix=''){
        global $wpdb;
        $response = array( 'process'=>TRUE, 'message'=>'' );

        $results = $wpdb->get_results("SELECT umeta_id, meta_key FROM {$wpdb->prefix}usermeta WHERE meta_key LIKE '{$wpdb->prefix}%'");
        foreach($results as $row){
            $row->new_meta_key = $new_table_prefix.str_replace($wpdb->prefix, '', $row->meta_key);
            $sql = "UPDATE {$wpdb->prefix}usermeta SET meta_key=%s WHERE umeta_id=%s LIMIT 1";
            if( $wpdb->query($wpdb->prepare($sql, $row->new_meta_key, $row->umeta_id))===FALSE ){
                $response['process'] = FALSE;
            }
        }
        $response['message'] = $response['process']
            ? 'Database table usermeta updated.'
            : 'Some entries in the database table <strong>UserMeta</strong> were not updated';

        return $response;
    }
}

class SucuriBackup extends SucuriDatabase{
    public function __construct(){
        ini_set('memory_limit', '-1');
    }

    /**
     * Generate a SQL or Zip file with the current state of the database in use.
     * @return string Returns the SQL or Zip full file path created.
     */
    public function all_database(){
        $sql_output = '';
        $tables = $this->get_dbtables();
        foreach($tables as $table_name){
            $sql_output .= $this->single_table($table_name, TRUE);
            $sql_output .= PHP_EOL.PHP_EOL;
        }
        $sql_output .= PHP_EOL.PHP_EOL;

        return $this->generate_backup($sql_output);
    }

    /**
     * Generate a SQL or Zip file containing all the content inside a single table in the current database.
     * @param  string  $table_name Specify the table name to dump.
     * @param  boolean $batch_mode Specify whether if return the SQL generated or generate the SQL/Zip file.
     * @return string Returns the SQL generated or the SQL/Zip full file path created.
     */
    public function single_table($table_name='', $batch_mode=FALSE){
        global $wpdb;

        if( $wpdb->get_var("SHOW TABLES LIKE '{$table_name}'")==$table_name ){
            $sql_output = '';

            $results = $wpdb->get_results("SELECT * FROM `{$table_name}`", ARRAY_N);
            $fields = $wpdb->get_col("DESCRIBE `{$table_name}`", 0);
            $num_fields = count($fields);

            $sql_output .= "DROP TABLE IF EXISTS `{$table_name}`;";
            $table_definition = $wpdb->get_row("SHOW CREATE TABLE `{$table_name}`", ARRAY_N);
            $sql_output .= PHP_EOL.PHP_EOL . $table_definition[1].';' . PHP_EOL.PHP_EOL;

            foreach($results as $row){
                $sql_output .= "INSERT INTO `{$table_name}` VALUES(";
                for( $i=0; $i<$num_fields; $i++ ) {
                    $row[$i] = esc_sql($row[$i]);
                    $row[$i] = preg_replace('#'.PHP_EOL.'#', "\n", $row[$i]);
                    if( isset($row[$i]) ){
                        $sql_output .= "'{$row[$i]}'";
                    } else {
                        $sql_output .= "''";
                    }

                    if( $i < ($num_fields-1) ){
                        $sql_output .= ','.chr(32);
                    }
                }
                $sql_output .= ');'.PHP_EOL;
            }

            return $batch_mode===TRUE ? $sql_output : $this->generate_backup($sql_output);
        }

        return FALSE;
    }

    /**
     * Create a SQL or Zip file with the passed content.
     * @param  string $content SQL generated.
     * @return string/boolean Return FALSE or the SQL/Zip full file path created.
     */
    private function generate_backup($content=''){
        $plugin_upload_folder = sucuri_dir_filepath();

        if( is_writable($plugin_upload_folder) ){
            $filename = 'sucuri-dbbackup-'.current_time('timestamp').'.sql';
            $filepath = rtrim($plugin_upload_folder,'/').'/'.$filename;
            $handle = @fopen($filepath, 'w+');
            @fwrite($handle, $content);
            @fclose($handle);

            if( class_exists('ZipArchive') ){
                $package_path = $filepath.'.zip';

                $zip = new ZipArchive();
                $archive = $zip->open($package_path, ZipArchive::CREATE);
                $zip->addFile($filepath, $filename);
                $zip->close();

                if( file_exists($package_path) && filesize($package_path)>0 ){
                    @unlink($filepath); /* Remove the old SQL file to keep the new Zip file */
                    $filename = $filename.'.zip';
                    $filepath = $package_path;
                }
            }

            return ( file_exists($filepath) && filesize($filepath)>0 ) ? $filepath : FALSE;
        }else{
            sucuri_admin_notice('error', '<strong>Sucuri WP Plugin</strong> upload folder is
                not writable, can not continue with the backup: <code>'.$plugin_upload_folder
                .'</code>');
        }

        return FALSE;
    }

    /**
     * Get extra information of the filepath specified, including full filepath, filesize, timeatime, etc.
     * @param  string $filepath Relative path of the file.
     * @return object           Extra information of the file specified.
     */
    private function get_backup_file_info($filepath=''){
        $backup_file = FALSE;
        $plugin_upload_folder = sucuri_dir_filepath();

        if( file_exists($filepath) && is_file($filepath) && is_readable($filepath) ){
            $filesize = filesize($filepath);
            $filename_exploded = explode('.', $filepath);
            $backup_file = (object)array(
                'filepath'=>$filepath,
                'filename'=>basename($filepath),
                'filesize'=>$filesize,
                'filehumansize'=>$this->human_filesize($filesize),
                'filetime'=>fileatime($filepath),
                'fileext'=>array_pop($filename_exploded),
                'fileurl'=>site_url( str_replace(ABSPATH, '', rtrim($plugin_upload_folder,'/').'/'.basename($filepath)) )
            );
        }

        return $backup_file;
    }

    /**
     * List all database backup files generated with extra information.
     * @return array Key-value list of backup files.
     */
    public function get_backup_files(){
        $backup_files = array();
        $plugin_upload_folder = sucuri_dir_filepath();

        $files = glob( rtrim($plugin_upload_folder,'/').'/sucuri-dbbackup*.{sql,zip}', GLOB_BRACE );
        if( is_array($files) && !empty($files) ){
            $files_ordered = array_reverse($files);
            foreach($files_ordered as $filepath){
                $backup_file = $this->get_backup_file_info($filepath);
                if( $backup_file ){ $backup_files[] = $backup_file; }
            }
        }

        return $backup_files;
    }

    /**
     * Get extra information of the filename specified, including full filepath, filesize, timeatime, etc.
     * @param  string $filename Simple filename with extension.
     * @return object           Extra information of the file specified.
     */
    public function get_backup_file_from_filename($filename=''){
        $plugin_upload_folder = sucuri_dir_filepath();
        $filepath = rtrim($plugin_upload_folder,'/').'/'.$filename;
        return $this->get_backup_file_info($filepath);
    }

    public function remove_backup_file($files=array()){
        $files = is_array($files) ? $files : array($files);
        if( !empty($files) ){
            $num_files_to_remove = count($files);
            $num_removed_files = 0;
            foreach($files as $filename){
                $backup_file = $this->get_backup_file_from_filename($filename);
                if($backup_file && @unlink($backup_file->filepath) ){
                    $num_removed_files += 1;
                }
            }
            sucuri_admin_notice(
                ( $num_removed_files==$num_files_to_remove ? 'updated' : 'error' ),
                "<strong>Sucuri WP Plugin</strong>. Database backups removed: {$num_removed_files} out of {$num_files_to_remove}"
            );
        }else{
            sucuri_admin_notice('error', '<strong>Sucuri WP Plugin</strong>. You did not select any backup file to remove.');
        }
    }
}

/* sucuri_deactivate_plugin:
 * Function to run when plugin is deactivated.
 */
function sucuri_deactivate_plugin()
{
   //wp_clear_scheduled_hook('sucuri_scheduled_scan');
}



/* sucuri_verify_run:
 * Checks last time we ran to avoid running twice (or too often).
 */
function sucuri_verify_run($runtime)
{
    if(!is_readable(sucuri_dir_filepath('.firstrun')))
    {
        if(!is_dir(sucuri_dir_filepath()))
        {
            sucuri_plugin_activation();
        }
        $firstrun_created = @touch(sucuri_dir_filepath('.firstrun'));
        if(!$firstrun_created){
            sucuri_admin_notice('error', '<strong>Error.</strong> In order to use our plugin you need to
                grant write access in this location:<br><code>'.sucuri_dir_filepath().'</code>');
        }
        return(true);
    }

    if(!is_readable(sucuri_dir_filepath(".runtime")))
    {
        if(!is_dir(sucuri_dir_filepath()))
        {
            sucuri_plugin_activation();
        }
        touch(sucuri_dir_filepath(".runtime"));
        return(true);
    }

    $lastchanged = filemtime(sucuri_dir_filepath(".runtime"));
    if($lastchanged >= (time(0) - $runtime))
    {
        return(FALSE);
    }

    $runtime_created = @touch(sucuri_dir_filepath('.runtime'));
    if(!$runtime_created){
        sucuri_admin_notice('error', '<strong>Error.</strong> In order to use our plugin you need to
            grant write access in this location:<br><code>'.sucuri_dir_filepath().'</code>');
    }
    return(true);
}



/* sucuri_scanallfiles:
 * Loops through the filesystem and generates the md5 checksum of the files.
 */
function sucuri_scanallfiles($dir, $output)
{
    $dh = @opendir($dir);
    if(!$dh)
    {
        return(0);
    }
    $printdir = $dir;

    while(($myfile = readdir($dh)) !== false)
    {
        if($myfile == "." || $myfile == "..")
        {
            continue;
        }

        /* Ignoring backup files from our clean ups. */
        if(strpos($myfile, "_sucuribackup.") !== FALSE)
        {
            continue;
        }
        if(is_dir($dir."/".$myfile))
        {
            if(($myfile == "cache") && (strpos($dir, "wp-content") !== FALSE))
            {
                continue;
            }
            if(($myfile == "w3tc") && (strpos($dir."/".$myfile, "wp-content/w3tc") !== FALSE))
            {
                continue;
            }
            if($myfile == "sucuri")
            {
                continue;
            }
            $output = sucuri_scanallfiles($dir."/".$myfile, $output);
        }

        else if((strpos($myfile, ".php") !== FALSE) ||
                (strpos($myfile, ".htm") !== FALSE) ||
                (strcmp($myfile, ".htaccess") == 0) ||
                (strcmp($myfile, "php.ini") == 0) ||
                (strpos($myfile, ".js") !== FALSE))
        {
            $output = $output.md5_file($dir."/".$myfile).filesize($dir."/".$myfile)." ".$dir."/".$myfile."\n";
        }

    }
    closedir($dh);
    return($output);
}



/* sucuri_do_scan:
 * Executes the integrity / version checks.
 */
function sucuri_do_scan()
{
    $wp_version = sucuri_get_wpversion();
    $sucuri_wp_key = get_option('sucuri_wp_key');


    sucuri_debug_log("Running wp-cron (doscan).");
    if($sucuri_wp_key === FALSE)
    {
        return(NULL);
    }

    sucuri_debug_log("Running wp-cron (valid key)");
    if(sucuri_verify_run(5000) === FALSE)
    {
        return(NULL);
    }

    sucuri_debug_log("Running wp-cron (verify run passed)");
    $output = "";
    sucuri_send_log($sucuri_wp_key, "WordPress version: $wp_version", 1);
    if(strcmp($wp_version, "3.4") >= 0)
    {
        $output = sucuri_scanallfiles(ABSPATH, $output);
        sucuri_send_hashes($sucuri_wp_key, $output);
    }

    /* If IPs in block list older then sucuri_wp_bs they will be cleared */

    $block_ips = sucuri_read_block_list_from_dir();
    sucuri_debug_log("check block list");
    $sucuri_wp_bs = get_option('sucuri_wp_bs');
    if( ($sucuri_wp_bs !== FALSE) && ($sucuri_wp_bs > 1) )
    {
        if( !empty($block_ips) )
        {
            foreach ($block_ips as $block_ip) {
                $file_timestamp = @filemtime($block_ip);
                $current_timestamp = current_time( 'timestamp' );
                $older_then =  $sucuri_wp_bs * 24 * 60 * 60;

                if ( ($current_timestamp - $file_timestamp) > $older_then ) {
                    @unlink($block_ip);
                }
            }
        }
    }

    sucuri_debug_log("Running wp-cron (finished)");
}



/* sucuri_send_hashes:
 * Sends the hashes to sucuri backend.
 */
function sucuri_send_hashes($sucuri_wp_key, $final_message)
{
    $response = wp_remote_post(SUCURI_REMOTE_URL, array(
	'method' => 'POST',
	'timeout' => 30,
	'redirection' => 5,
	'httpversion' => '1.0',
	'blocking' => true,
	'body' => array( 'k' => $sucuri_wp_key, 'send-hashes' => $final_message),
    ));

    if(is_wp_error($response))
    {
        return(1);
    }

    $doresult = $response['body'];
    if(strpos($doresult,"ERROR:") === FALSE)
    {
        if(strpos($doresult, "ERROR: Invalid") !== FALSE)
        {
            delete_option('sucuri_wp_key');
        }
        return(1);
    }
    else
    {
        return(0);
    }
}



/* sucuri_block_wpadmin:
 * Blocks an IP via our plugin (wp-admin only).
 */
function sucuri_block_wpadmin()
{
    if(isset($_SERVER['SUCURI_RIP']))
    {
        @touch(sucuri_dir_filepath('blocks/').$_SERVER['SUCURI_RIP']);
    }
}



/* sucuri_send_log:
 * Sends the events (audit log) to sucuri.
 */
function sucuri_send_log($sucuri_wp_key, $final_message, $ignore_res = 0)
{
    $response = wp_remote_post(SUCURI_REMOTE_URL, array(
	'method' => 'POST',
	'timeout' => 12,
	'redirection' => 5,
	'httpversion' => '1.0',
	'blocking' => true,
	'body' => array( 'k' => $sucuri_wp_key, 'send-event' => $final_message),
    ));

    if(is_wp_error($response))
    {
        return(-1);
    }

    $doresult = $response['body'];


    /* Request to block the IP address. */
    if(strpos($doresult, "ACTION: BLOCK") !== FALSE)
    {
        if(!is_file(sucuri_dir_filepath("whitelist/".$_SERVER['SUCURI_RIP'])))
        {
            sucuri_block_wpadmin();
        }
        return(1);
    }


    if($ignore_res == 1)
    {
        return(1);
    }

    if(strpos($doresult,"ERROR:") !== FALSE)
    {
        if(strpos($doresult, "ERROR: Invalid") !== FALSE)
        {
            delete_option('sucuri_wp_key');
        }
        return(0);
    }
    else if(strpos($doresult, "OK:") !== FALSE)
    {
        return(1);
    }
    else
    {
        return(0);
    }
}



/* sucuri_event:
 * Generates an audit event log (to be sent later).
 */
function sucuri_event($severity, $location, $message)
{
    $severity = trim($severity);
    $location = trim($location);
    $message = trim($message);


    $username = NULL;
    //$user = wp_get_current_user();
    if(!empty($user->ID))
    {
        $username = $user->user_login;
    }
    $time = date('Y-m-d H:i:s', time());


    /* Fixing severity */
    $severity = (int)$severity;
    if ($severity < 0)
    {
        $severity = 1;
    }
    else if($severity > 5)
    {
        $severity = 5;
    }

    /* Setting remote ip. */
    $remote_ip = "local";
    if(isset($_SERVER['SUCURI_RIP']) && strlen($_SERVER['SUCURI_RIP']) > 6)
    {
        $remote_ip = $_SERVER['SUCURI_RIP'];
    }


    /* Setting header block */
    $header = NULL;
    if($username !== NULL)
    {
        $header = '['.$remote_ip.' '.$username.']';
    }
    else
    {
        $header = '['.$remote_ip.']';
    }


    /* Making sure we escape everything. */
    $header = htmlspecialchars($header);
    $message = htmlspecialchars($message);

    /* To avoid double lines. */
    $message = str_replace("\n", "", $message);
    $message = str_replace("\r", "", $message);


    /* Getting severity. */
    $severityname = "Info";
    if($severity == 0)
    {
        $severityname = "Debug";
    }
    else if($severity == 1)
    {
        $severityname = "Notice";
    }
    else if($severity == 2)
    {
        $severityname = "Info";
    }
    else if($severity == 3)
    {
        $severityname = "Warning";
    }
    else if($severity == 4)
    {
        $severityname = "Error";
    }
    else if($severity == 5)
    {
        $severityname = "Critical";
    }

    $sucuri_wp_key = get_option('sucuri_wp_key');
    if($sucuri_wp_key !== FALSE)
    {
        sucuri_send_log($sucuri_wp_key, "$severityname: $header: $message", 1);
    }

    return(true);
}



function sucuri_harden_error($message)
{
    return('<div id="message" class="error"><p>'.$message.'</p></div>');
}

function sucuri_harden_ok($message)
{
    return( '<div id="message" class="updated"><p>'.$message.'</p></div>');
}


function sucuri_harden_status($status, $type, $messageok, $messagewarn, $desc = NULL, $updatemsg = NULL)
{
    if($status == 1){
        echo '<h4>'.
             '<img style="position:relative;top:5px" height="22" width="22"'.
             'src="'.SUCURI_IMG.'ok.png" /> &nbsp; '.
             $messageok.'.</h4>';

        if($updatemsg != NULL){ echo $updatemsg; }

        if($type != NULL){
            echo "<input type='submit' name='{$type}_unharden' value='Revert hardening' class='button-secondary' />";
            echo '<br /><br />';
        }
    }else{
        echo '<h4>'.
             '<img style="position:relative;top:5px" height="22" width="22"'.
             'src="'.SUCURI_IMG.'warn.png" /> &nbsp; '.
             $messagewarn. '.</h4>';

        if($updatemsg != NULL){ echo $updatemsg; }

        if($type != NULL){
            echo '<input class="button-primary" type="submit" name="'.$type.'"
                         value="Harden it!" /><br /><br />';
        }
    }
    if($desc != NULL){
        echo "<i>$desc</i>";
    }

}


function sucuri_harden_version()
{
    global $wp_version;
    $cp = 0;
    $updates = get_core_updates();

    if(
           !is_array($updates)
        || empty($updates)
        || $updates[0]->response=='latest'
    ){ $cp = 1; }

    if( strcmp($wp_version, "3.6.1")<0 ){
        $cp = 0;
    }
    $wp_version = htmlspecialchars($wp_version);


    __ss_wraphardeningboxopen("Verify WordPress Version");
    sucuri_harden_status($cp, NULL,
                         "WordPress is updated", "WordPress is not updated",
                         NULL);

    if($cp == 0){
        echo "<i>Your current version ($wp_version) is not current. Please update it <a href='update-core.php'>now!</a></i>";
    }else{
        echo "<i>Your WordPress installation ($wp_version) is current.</i>";
    }
    __ss_wraphardeningboxclose();
}



function sucuri_wp_harden_removegenerator()
{
    /* Enabled by default with this plugin. */
    $cp = 1;

    __ss_wraphardeningboxopen("Remove WordPress Version");

    sucuri_harden_status($cp, NULL,
                         "WordPress version properly hidden", NULL,
                         "It checks if your WordPress version is being hidden".
                         " from being displayed in the generator tag ".
                         "(enabled by default with this plugin).");

    __ss_wraphardeningboxclose();
}



function sucuri_harden_upload()
{
    $cp = 1;
    $upmsg = NULL;
    $htaccess_upload = dirname(sucuri_dir_filepath()).'/.htaccess';

    if( !is_readable($htaccess_upload) ){
        $cp = 0;
    }else{
        $cp = 0;
        $fcontent = file($htaccess_upload);
        foreach($fcontent as $fline){
            if( strpos($fline, 'deny from all') !== FALSE ){
                $cp = 1;
                break;
            }
        }
    }

    if( isset($_POST['wpsucuri-doharden']) ){
        if( isset($_POST['sucuri_harden_upload']) && $cp==0 ){
            if( @file_put_contents($htaccess_upload, "\n<Files *.php>\ndeny from all\n</Files>")===FALSE ){
                $upmsg = sucuri_harden_error('ERROR: Unable to create <code>.htaccess</code> file, folder destination is not writable.');
            }else{
                $upmsg = sucuri_harden_ok('Completed. Upload directory successfully secured.');
                $cp = 1;
            }
        }

        elseif( isset($_POST['sucuri_harden_upload_unharden']) ){
            $htaccess_upload_writable = ( file_exists($htaccess_upload) && is_writable($htaccess_upload) ) ? TRUE : FALSE;
            $htaccess_content = $htaccess_upload_writable ? file_get_contents($htaccess_upload) : '';

            if( $htaccess_upload_writable ){
                $cp = 0;
                if( preg_match('/<Files \*\.php>\ndeny from all\n<\/Files>/', $htaccess_content, $match) ){
                    $htaccess_content = str_replace("<Files *.php>\ndeny from all\n</Files>", '', $htaccess_content);
                    file_put_contents($htaccess_upload, $htaccess_content, LOCK_EX);
                }
                sucuri_admin_notice('updated', '<strong>OK.</strong> WP-Content Uploads directory protection reverted.');
            }else{
                $harden_process = '<strong>Error.</strong> The <code>wp-content/uploads/.htaccess</code> does
                    not exists or is not writable, you will need to remove the following code manually there:
                    <code>&lt;Files *.php&gt;deny from all&lt;/Files&gt;</code>';
                sucuri_admin_notice('error', $harden_process);
            }
        }
    }

    __ss_wraphardeningboxopen("Protect uploads directory");
    sucuri_harden_status($cp, "sucuri_harden_upload",
                         "Upload directory properly protected",
                         "Upload directory not protected",
                         "It checks if your upload directory allows PHP ".
                         "execution or if it is browsable.", $upmsg);
    __ss_wraphardeningboxclose();
}



function sucuri_harden_wpcontent()
{
    $cp = 1;
    $upmsg = NULL;
    $htaccess_upload = ABSPATH.'/wp-content/.htaccess';

    if( !is_readable($htaccess_upload) ){
        $cp = 0;
    }else{
        $cp = 0;
        $fcontent = file($htaccess_upload);
        foreach($fcontent as $fline){
            if( strpos($fline, 'deny from all') !== FALSE ){
                $cp = 1;
                break;
            }
        }
    }

    if( isset($_POST['wpsucuri-doharden']) ){
        if( isset($_POST['sucuri_harden_wpcontent']) && $cp==0 ){
            if( @file_put_contents($htaccess_upload, "\n<Files *.php>\ndeny from all\n</Files>")===FALSE ){
                $upmsg = sucuri_harden_error('ERROR: Unable to create <code>.htaccess</code> file, folder destination is not writable.');
            }else{
                $upmsg = sucuri_harden_ok('Completed. wp-content directory successfully secured.');
                $cp = 1;
            }
        }

        elseif( isset($_POST['sucuri_harden_wpcontent_unharden']) ){
            $htaccess_upload_writable = ( file_exists($htaccess_upload) && is_writable($htaccess_upload) ) ? TRUE : FALSE;
            $htaccess_content = $htaccess_upload_writable ? file_get_contents($htaccess_upload) : '';

            if( $htaccess_upload_writable ){
                $cp = 0;
                if( preg_match('/<Files \*\.php>\ndeny from all\n<\/Files>/', $htaccess_content, $match) ){
                    $htaccess_content = str_replace("<Files *.php>\ndeny from all\n</Files>", '', $htaccess_content);
                    file_put_contents($htaccess_upload, $htaccess_content, LOCK_EX);
                }
                sucuri_admin_notice('updated', '<strong>OK.</strong> WP-Content directory protection reverted.');
            }else{
                $harden_process = '<strong>Error.</strong> The <code>wp-content/.htaccess</code> does
                    not exists or is not writable, you will need to remove the following code manually there:
                    <code>&lt;Files *.php&gt;deny from all&lt;/Files&gt;</code>';
                sucuri_admin_notice('error', $harden_process);
            }
        }
    }

    __ss_wraphardeningboxopen("Restrict access to wp-content");
    sucuri_harden_status($cp, "sucuri_harden_wpcontent",
                         "WP-content directory properly protected",
                         "WP-content directory not protected",
                         "This option blocks direct PHP access to any file inside wp-content. <b>Warn: Do not use it if ".
                         "your site uses timthumb or similar (insecure) scripts.</b> If something breaks, just remove the .htaccess from wp-content.", $upmsg);
    __ss_wraphardeningboxclose();
}



function sucuri_harden_wpincludes()
{
    $cp = 1;
    $upmsg = NULL;
    $htaccess_upload = ABSPATH.'/wp-includes/.htaccess';

    if( !is_readable($htaccess_upload) ){
        $cp = 0;
    }else{
        $cp = 0;
        $fcontent = file($htaccess_upload);
        foreach($fcontent as $fline){
            if( strpos($fline, 'deny from all') !== FALSE ){
                $cp = 1;
                break;
            }
        }
    }

    if( isset($_POST['wpsucuri-doharden']) ){
        if( isset($_POST['sucuri_harden_wpincludes']) && $cp==0 ){
            $htaccess_content = "\n<Files *.php>"
                ."\ndeny from all"
                ."\n</Files>"
                ."\n<Files wp-tinymce.php>"
                ."\nallow from all"
                ."\n</Files>"
                ."\n<Files ms-files.php>"
                ."\nallow from all"
                ."\n</Files>\n";
            if( @file_put_contents($htaccess_upload, $htaccess_content)===FALSE ){
                $upmsg = sucuri_harden_error('ERROR: Unable to create <code>.htaccess</code> file, folder destination is not writable.');
            }else{
                $upmsg = sucuri_harden_ok('Completed. wp-includes directory successfully secured.');
                $cp = 1;
            }
        }

        elseif( isset($_POST['sucuri_harden_wpincludes_unharden']) ){
            $htaccess_upload_writable = ( file_exists($htaccess_upload) && is_writable($htaccess_upload) ) ? TRUE : FALSE;
            $htaccess_content = $htaccess_upload_writable ? file_get_contents($htaccess_upload) : '';

            if( $htaccess_upload_writable ){
                $cp = 0;
                if( preg_match_all('/<Files (\*|wp-tinymce|ms-files)\.php>\n(deny|allow) from all\n<\/Files>/', $htaccess_content, $match) ){
                    foreach($match[0] as $restriction){
                        $htaccess_content = str_replace($restriction, '', $htaccess_content);
                    }
                    file_put_contents($htaccess_upload, $htaccess_content, LOCK_EX);
                }
                sucuri_admin_notice('updated', '<strong>OK.</strong> WP-Includes directory protection reverted.');
            }else{
                $harden_process = '<strong>Error.</strong> The <code>wp-includes/.htaccess</code> does
                    not exists or is not writable, you will need to remove the following code manually there:
                    <code>&lt;Files *.php&gt;deny from all&lt;/Files&gt;</code>';
                sucuri_admin_notice('error', $harden_process);
            }
        }
    }

    __ss_wraphardeningboxopen("Restrict access to wp-includes");
    sucuri_harden_status($cp, "sucuri_harden_wpincludes",
                         "WP-includes directory properly protected",
                         "WP-includes directory not protected",
                         "This option blocks direct PHP access to any file inside wp-includes. ", $upmsg);
    __ss_wraphardeningboxclose();
}



function sucuri_harden_keys()
{
    $upmsg = NULL;
    $cp = 1;
    $wpconf = NULL;
    if( is_readable(ABSPATH.'/wp-config.php') ){
        $wpconf = ABSPATH.'/wp-config.php';
    }else if( is_readable(ABSPATH.'/../wp-config.php') ){
        $wpconf = ABSPATH.'/../wp-config.php';
    }else{
        /* Unable to find? */
        $cp = 1;
    }

    __ss_wraphardeningboxopen('Verify proper usage of the secret keys');
    sucuri_harden_status($cp, NULL,
                         "WordPress secret keys and salts properly created",
                         "WordPress secret keys and salts not set. We recommend creating them for security reasons",
                         "It checks whether you have proper random keys/salts ".
                         "created for WordPress. They should be created when ".
                         "you first install WordPress and regenerated if you ".
                         "have been hacked recently.",
                         $upmsg);
    __ss_wraphardeningboxclose();
}


function sucuri_harden_dbtables()
{
    global $table_prefix;
    $hardened = $table_prefix=='wp_' ? 0 : 1;

    if( isset($_POST['wpsucuri-doharden']) && isset($_POST['sucuri_harden_dbtables']) ){
        $sucuri_backup = new SucuriBackup();
        $dbbackup_filepath = $sucuri_backup->all_database();

        if( $dbbackup_filepath ){
            sucuri_admin_notice('updated', "<strong>Sucuri WP Plugin</strong>. A new database table prefix
                change was initialized, if you have problems after this operation finishes you can find
                a backup of the current database here: <code>{$dbbackup_filepath}</code>");

            if( isset($_POST['sucuri_harden_dbtables']) ){
                $hardened = 1;
                $sucuri_backup->new_table_prefix();
            }elseif( isset($_POST['sucuri_harden_dbtables_unharden']) ){
                $hardened = 0;
                $sucuri_backup->reset_table_prefix();
            }
        }else{
            sucuri_admin_notice('error', '<strong>Sucuri WP Plugin</strong>. Error generating a backup for your database.');
        }
    }

    __ss_wraphardeningboxopen("Change Default Database Table Prefix");

    sucuri_harden_status($hardened, 'sucuri_harden_dbtables',
        "Database table prefix properly modified",
        "Database table set to the default value. Not recommended",
        "It checks whether your database table prefix has ".
        "been changed from the default 'wp_'.", NULL);

    __ss_wraphardeningboxclose();
}



function sucuri_harden_adminuser()
{
    global $wpdb;

    $res = $wpdb->get_results("SELECT user_login FROM {$wpdb->prefix}users WHERE user_login='admin'");
    $admin_account_removed = count($res)==0 ? 1 : 0;

    __ss_wraphardeningboxopen('Change Default Admin user name');
    sucuri_harden_status($admin_account_removed, NULL,
                          "Default admin user name (admin) not being used",
                          "Default admin user name (admin) being used. Not recommended",
                          "It checks whether you have the default 'admin' ".
                          "account enabled. Security guidelines recommend ".
                          "creating a new admin user name.", NULL);

    if($admin_account_removed == 0){
        echo '<br /><i><strong>We do not offer the option</strong> to automatically change the user name.
            Go to the <a href="users.php">user list</a> and create a new admin user name. Once created,
            log in as that user and remove the default "admin" from there (make sure to assign all the
            admin posts to the new user too!).</i>';
    }
    __ss_wraphardeningboxclose();
}



function sucuri_harden_readme()
{
    $upmsg = NULL;
    $cp = is_readable(ABSPATH.'/readme.html') ? 0 : 1;

    if( isset($_POST['wpsucuri-doharden']) ){
        if( isset($_POST['sucuri_harden_readme']) && $cp==0 ){
            if( @unlink(ABSPATH.'/readme.html') === FALSE ){
                $upmsg = sucuri_harden_error("Unable to remove readme file.");
            }else{
                $cp = 1;
                $upmsg = sucuri_harden_ok("Readme file removed.");
            }
        }

        elseif( isset($_POST['sucuri_harden_readme_unharden']) ){
            sucuri_admin_notice('error', '<strong>Error.</strong> We can not revert this
                action, you should create the <code>readme.html</code> file at your own.');
        }
    }

    __ss_wraphardeningboxopen("Remove readme.html (information leakage)");
    sucuri_harden_status($cp, "sucuri_harden_readme",
                         "Readme file properly deleted",
                         "Readme file not deleted and leaking the WordPress version",
                         "It checks whether you have the readme.html file ".
                         "available that leaks your WordPress version.", $upmsg);
    __ss_wraphardeningboxclose();
}



function sucuri_harden_phpversion()
{
    $phpv = phpversion();
    $cp = ( strncmp($phpv, "5.", 2) < 0 ) ? 0 : 1;

    __ss_wraphardeningboxopen("Verify PHP version");
    sucuri_harden_status($cp, NULL,
                         "Using an updated version of PHP (v $phpv)",
                         "The version of PHP you are using ($phpv) is not current. Not recommended and not supported",
                         "It checks if you have the latest version of PHP installed.", NULL);
    __ss_wraphardeningboxclose();
}



function sucuri_harden_fileeditor()
{
    $file_editor_disabled = defined('DISALLOW_FILE_EDIT') ? DISALLOW_FILE_EDIT : FALSE;
    __ss_wraphardeningboxopen('Wordpress Plugin and Theme editor');

    if( isset($_POST['wpsucuri-doharden']) ){
        $current_time = date('r');
        $wp_config_path = sucuri_get_wpconfig_path();

        $wp_config_writable = ( file_exists($wp_config_path) && is_writable($wp_config_path) ) ? TRUE : FALSE;
        $new_wpconfig = $wp_config_writable ? file_get_contents($wp_config_path) : '';

        if( isset($_POST['sucuri_harden_fileeditor']) ){
            if($wp_config_writable){
                if( preg_match('/(.*define\(.DB_COLLATE..*)/', $new_wpconfig, $match) ){
                    $disallow_fileedit_definition = "\n\ndefine('DISALLOW_FILE_EDIT', TRUE); // Sucuri Security: {$current_time}\n";
                    $new_wpconfig = str_replace($match[0], $match[0].$disallow_fileedit_definition, $new_wpconfig);
                }
                file_put_contents($wp_config_path, $new_wpconfig, LOCK_EX);
                $file_editor_disabled = TRUE;
                sucuri_admin_notice('updated', '<strong>OK.</strong> WP-Config file updated successfully, Plugins and Themes editor disabled.');
            }else{
                $harden_process = '<strong>Error.</strong> The <code>wp-config.php</code> file is not in the default
                    location or is not writable, you will need to put the following code manually there:
                    <code>define("DISALLOW_FILE_EDIT", TRUE);</code>';
                sucuri_admin_notice('error', $harden_process);
            }
        }

        elseif( isset($_POST['sucuri_harden_fileeditor_unharden']) ){
            if( preg_match("/(.*define\('DISALLOW_FILE_EDIT', TRUE\);.*)/", $new_wpconfig, $match) ){
                if($wp_config_writable){
                    $new_wpconfig = str_replace("\n{$match[1]}", '', $new_wpconfig);
                    file_put_contents($wp_config_path, $new_wpconfig, LOCK_EX);
                    $file_editor_disabled = FALSE;
                    sucuri_admin_notice('updated', '<strong>OK.</strong> WP-Config file updated successfully, Plugins and Themes editor enabled.');
                }else{
                    $harden_process = '<strong>Error.</strong> The <code>wp-config.php</code> file is not in the default
                        location or is not writable, you will need to remove the following code manually there:
                        <code>define("DISALLOW_FILE_EDIT", TRUE);</code>';
                    sucuri_admin_notice('error', $harden_process);
                }
            }else{
                sucuri_admin_notice('error', '<strong>Error.</strong> We did not find a definition to disallow the file editor.');
            }
        }
    }

    sucuri_harden_status($file_editor_disabled, 'sucuri_harden_fileeditor',
                         'File editor for Plugins and Themes is disabled',
                         'File editor for Plugins and Themes is enabled',
                         'Occasionally you may wish to disable the plugin or theme editor to prevent overzealous users
                         from being able to edit sensitive files and potentially crash the site. Disabling these also
                         provides an additional layer of security if a hacker gains access to a well-privileged user
                         account.', NULL);

    __ss_wraphardeningboxclose();
}

function sucuri_cloudproxy_enabled(){
    $enabled = sucuri_is_behind_cloudproxy();

    __ss_wraphardeningboxopen('Verify if your site is protected by a Web Firewall');
    sucuri_harden_status(
        $enabled, NULL,
        'Your website is protected by a Website Firewall (WAF)',
        'Your website is not protected by a Website Firewall (WAF)',
        'A Website firewall (WAF) is a security system that controls and blocks attacks and malicious requests
        against a web site. It helps to prevent compromises and most malware infections. You can find more details
        about Sucuri WAF <a href="http://cloudproxy.sucuri.net/" target="_blank">here</a>.',
        NULL
    );
    if( $enabled!==TRUE ){
        echo '<p><a href="https://login.sucuri.net/signup2/create?CloudProxy" target="_blank" class="button button-primary">Harden it!</a></p>';
    }
    __ss_wraphardeningboxclose();
}
function sucuri_add_attachment($id = NULL)
{
    if (is_numeric($id))
    {
        $postdata = get_post($id);
        $postname = $postdata->post_title;
    }
    sucuri_event(1, "core", "Attachment added to post. Id: $id, Name: $postname");
}


function sucuri_create_category($categoryid = NULL)
{
    if(is_numeric($categoryid))
    {
        $name = get_cat_name($categoryid);
    }
    sucuri_event(1, "core", "Category created. Id: $categoryid, Name: $name");
}

function sucuri_delete_post($id = NULL)
{
    //sucuri_event(3, "core", "Post deleted. Id: $id");
}

function sucuri_private_to_published($id = NULL)
{
    if (is_numeric($id))
    {
        $postdata = get_post($id);
        $postname = $postdata->post_title;
    }
    sucuri_event(2, "core", "Post state changed from private to published. Id: $id, Name: $postname");
}

function sucuri_publish_post($id = NULL)
{
    if (is_numeric($id))
    {
        $postdata = get_post($id);
        $postname = $postdata->post_title;
        $post_or_page = ucwords($postdata->post_type);

        $created_or_updated = ($postdata->post_date==$postdata->post_modified) ? 'created' : 'updated';
        sucuri_event(2, "core", "{$post_or_page} {$created_or_updated}. Id: $id, Name: $postname");
        sucuri_alerts(
            "sucuri_publish_post_alert", "{$post_or_page} {$created_or_updated}",
            "{$post_or_page} {$created_or_updated}. {$post_or_page} Id: $id, Name: $postname");
    }
}


function sucuri_add_link($id)
{
    if(is_numeric($id))
    {
        $linkdata = get_bookmark($id);
        $name = $linkdata->link_name;
        $url = $linkdata->link_url;
    }
    sucuri_event(2, "core", "New link added. Id: $id, Name: $name, URL: $url");
}


function sucuri_switch_theme($themename)
{
    sucuri_event(3, "core", "Theme modified to: $themename");
    sucuri_notify_event('theme_switched', 'Website theme modified', "The theme of your website was modified: {$themename}");
}

function sucuri_delete_user($id)
{
    sucuri_event(3, "core", "User deleted. Id: $id");
}

function sucuri_retrieve_password($name)
{
    sucuri_event(3, "core", "Password retrieval attempt by user $name");
}

function sucuri_user_register($id)
{
    if(is_numeric($id))
    {
        $userdata = get_userdata($id);
        $name = $userdata->display_name;
    }
    sucuri_event(3, "core", "New user registered: Id: $id, Name: $name");
    sucuri_alerts("sucuri_user_register_alert", "New User registered", "New user registered: Id: $id, Name: $name", array('User'=>$name) );
}


function sucuri_wp_login($name)
{
    sucuri_event(2, "core","User logged in. Name: $name");
    sucuri_alerts("sucuri_wp_login_alert", "Successful Login", "User logged in. Name: $name", array('User'=>$name) );
}

function sucuri_wp_login_failed($name)
{
    sucuri_event(3, "core","User authentication failed. User name: $name");
    sucuri_alerts("sucuri_wp_login_failed_alert", "Failed login", "User authentication failed. Name: $name", array('User'=>$name) );
}


function sucuri_reset_password($arg = NULL)
{
    if(isset($_GET['key']) )
    {
        /* Detecting wordpress 2.8.3 vulnerability - $key is array */
        if(is_array($_GET['key']))
        {
            sucuri_event(3, 'core', "IDS: Attempt to reset password by attacking wp2.8.3 bug.");
        }
    }
}



function sucuri_process_prepost()
{
    $doblock = 0;
    if($_SERVER['REQUEST_METHOD'] != "POST")
    {
        return(0);
    }

    $remediation = get_option('sucuri_wp_re');
    if($remediation !== FALSE && $remediation == 'disabled')
    {
        return(0);
    }


    /* Using the right ip address here. */
    if(isset($_SERVER['SUCURI_RIP']) && strlen($_SERVER['SUCURI_RIP']) > 5)
    {
        $myip = $_SERVER['SUCURI_RIP'];
    }
    else
    {
        return(0);
    }


    if(is_file(sucuri_dir_filepath("whitelist/$myip")))
    {
        return(0);
    }


    /* Blocking IP addresses in our block list. */
    if(is_file(sucuri_dir_filepath("blocks/$myip")))
    {
        wp_die("Denied access by <a href='http://sucuri.net'>Sucuri</a> (ip blacklisted). Please contact the site owner to get it re-opened. Your IP address: ".htmlspecialchars($myip));
    }


    /* WordPress specific ignores */
    if($doblock == 0)
    {
        $_SERVER['REQUEST_URI'] = trim($_SERVER['REQUEST_URI']);
        if(strpos($_SERVER['REQUEST_URI'],"/wp-admin/admin-ajax.php") !== FALSE)
        {
            return(0);
        }
        else if(strpos($_SERVER['REQUEST_URI'], "/ajax_search.php") !== FALSE)
        {
            return(0);
        }
        else if(strpos($_SERVER['REQUEST_URI'], "/wp-admin/post.php") !== FALSE)
        {
            return(0);
        }
        else if(strpos($_SERVER['REQUEST_URI'], "/wp-cron.php") !== FALSE)
        {
            return(0);
        }
    }


    $response = wp_remote_post("http://cc.sucuri.net", array(
        'method' => 'POST',
        'timeout' => 10,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking' => true,
        'body' => array( 'ip' => $myip,
                         'from' => $_SERVER['SERVER_NAME'],
                         'path' => $_SERVER['REQUEST_URI'],
                         'ua' => $_SERVER['HTTP_USER_AGENT'],
                         'data' => print_r($_POST,1)),
    ));

    if(is_wp_error($response))
    {
        return(1);
    }

    $doresult = $response['body'];
    if(strpos($doresult, "BLOCK") !== FALSE)
    {
        $doblock = 1;
    }
    else
    {
        $doblock = 0;
    }


    if($doblock == 1)
    {
        $sucuri_wp_key = get_option('sucuri_wp_key');
        if($sucuri_wp_key != NULL)
        {
            sucuri_event(3, 'core', "IDS: Web firewall blocked access from: ".$myip);
        }
        sucuri_block_wpadmin();
        wp_die("Denied access by <a href='http://sucuri.net'>Sucuri</a> (ip blacklisted 2). Please contact the site owner to get it re-opened. Your IP address: ".htmlspecialchars($myip));
    }
}

function sucuri_notify_event($event=NULL, $title='', $content=''){
    $notify = get_option('sucuri_notify_'.$event);

    if( $notify=='enabled' && !empty($title) ){
        $email = sucuri_get_wp_email();
        sucuri_send_mail($email, $title, $content);
    }
}

function sucuri_events_without_actions()
{
    /* Using the right ip address here. */
    if(isset($_SERVER['SUCURI_RIP']))
    {
        $myip = $_SERVER['SUCURI_RIP'];
    }
    else
    {
        return(0);
    }


    $remediation = get_option('sucuri_wp_re');
    if($remediation !== FALSE && $remediation == 'disabled')
    {
        return(0);
    }


    /* Blocking IP addresses in our block list. */
    if(is_file(sucuri_dir_filepath("blocks/$myip")))
    {
        if(!is_file(sucuri_dir_filepath("whitelist/$myip")))
        {
            wp_die("Denied access by <a href='http://sucuri.net'>Sucuri</a> (ip blacklisted 3). Please contact the site owner to get it re-opened. Your IP address: ".htmlspecialchars($myip));
        }
    }


    /* Plugin activated */
    if(isset($_GET['action']) && $_GET['action'] == "activate" && !empty($_GET['plugin']) &&
           strpos($_SERVER['REQUEST_URI'], 'plugins.php') !== false &&
           current_user_can('activate_plugins'))
    {
        $plugin = $_GET['plugin'];
        $plugin = strip_tags($plugin);
        $plugin = mysql_real_escape_string($plugin);
        sucuri_event(3, 'core', "Plugin activated: $plugin.");
        sucuri_notify_event('plugin_activated', 'Website plugin activated', "Plugin activated: {$plugin}");
    }

    /* Plugin deactivated */
    else if(isset($_GET['action']) && $_GET['action'] == "deactivate" && !empty($_GET['plugin']) &&
           strpos($_SERVER['REQUEST_URI'], 'plugins.php') !== false &&
           current_user_can('activate_plugins'))
    {
        $plugin = $_GET['plugin'];
        $plugin = strip_tags($plugin);
        $plugin = mysql_real_escape_string($plugin);
        sucuri_event(3, 'core', "Plugin deactivated: $plugin.");
        sucuri_notify_event('plugin_deactivated', 'Website plugin deactivated', "Plugin deactivated: {$plugin}");
    }

    /* Plugin updated */
    else if(isset($_GET['action']) && $_GET['action'] == "upgrade-plugin" && !empty($_GET['plugin']) &&
           strpos($_SERVER['REQUEST_URI'], 'wp-admin/update.php') !== false &&
           current_user_can('update_plugins'))
    {
        $plugin = $_GET['plugin'];
        $plugin = strip_tags($plugin);
        $plugin = mysql_real_escape_string($plugin);
        sucuri_event(3, 'core', "Plugin request to be updated: $plugin.");
        sucuri_notify_event('plugin_updated', 'Website plugin updated', "Plugin request to be updated: {$plugin}");
    }

    /* Plugin installed */
    else if(
        isset($_GET['action'])
        && preg_match('/^(install|upload)-plugin$/', $_GET['action'])
        && current_user_can('install_plugins')
    ){
        if( isset($_FILES['pluginzip']) ){
            $plugin = $_FILES['pluginzip']['name'];
        }else if( isset($_GET['plugin']) ){
            $plugin = $_GET['plugin'];
        }else{
            $plugin = 'Unknown';
        }
        $plugin = mysql_real_escape_string( strip_tags($plugin) );
        sucuri_event(3, 'core', "Plugin request to be installed: {$plugin}");
        sucuri_notify_event('plugin_installed', 'Website plugin installed', "Plugin request to be installed: {$plugin}");
    }

    /* Plugin deleted */
    else if(
        isset($_POST['action']) && $_POST['action']=='delete-selected'
        && isset($_POST['verify-delete']) && $_POST['verify-delete']==1
        && current_user_can('delete_plugins')
    ){
        $plugin = '';
        $plugins = isset($_POST['checked']) ? $_POST['checked'] : array();
        if( is_array($plugins) && !empty($plugins) ){
            $separator = ','.chr(32);
            foreach($plugins as $plugin_path){
                $plugin .= basename($plugin_path).$separator;
            }
            $plugin = rtrim($plugin, $separator);
        }
        $plugin = mysql_real_escape_string( strip_tags($plugin) );
        sucuri_event(3, 'core', "Plugin request to be deleted: {$plugin}");
        sucuri_notify_event('plugin_deleted', 'Website plugin deleted', "Plugin request to be deleted: {$plugin}");
    }

    /* WordPress updated */
    else if(isset($_POST['upgrade']) && isset($_POST['version']) &&
           strpos($_SERVER['REQUEST_URI'], 'update-core.php?action=do-core-reinstall') !== false &&
           current_user_can('update_core'))
    {
        $version = $_POST['version'];
        $version = strip_tags($version);
        $version = mysql_real_escape_string($version);
        sucuri_event(3, 'core', "WordPress updated (or re-installed) to version: $version.");
        sucuri_notify_event('website_updated', 'Website updated', "WordPress updated (or re-installed) to version: {$version}.");
    }

    /* Theme editor */
    else if(isset($_POST['action']) && $_POST['action'] == 'update' &&
            isset($_POST['file']) && isset($_POST['theme']) &&
            strpos($_SERVER['REQUEST_URI'], 'theme-editor.php') !== false)
    {
        $myfile = mysql_real_escape_string(htmlspecialchars(trim($_POST['file'])));
        $mytheme = mysql_real_escape_string(htmlspecialchars(trim($_POST['theme'])));
        sucuri_event(3, 'core', "Theme editor used to modify file: $mytheme/$myfile.");

        sucuri_alerts("sucuri_wp_theme_editor_alert", "File modified", "Theme editor used to modify file: $mytheme/$myfile.");

    }

    /* Plugin editor */
    else if(isset($_POST['action']) && $_POST['action'] == 'update' &&
            isset($_POST['file']) && isset($_POST['plugin']) &&
            strpos($_SERVER['REQUEST_URI'], 'plugin-editor.php') !== false)
    {
        $myfile = mysql_real_escape_string(htmlspecialchars(trim($_POST['file'])));
        sucuri_event(3, 'core', "Plugin editor used to modify file: $myfile.");

        sucuri_alerts("sucuri_wp_theme_editor_alert", "File modified", "Plugin editor used to modify file: $myfile.");
    }

    /* Detects any Wordpress settings updates */
    else if( isset($_POST['option_page']) ){
        // Get the settings available in the database and compare them with the submission.
        $all_options = sucuri_get_wp_options();
        $options_changed = sucuri_what_options_were_changed($_POST);

        // Generate the list of options changed.
        $options_changed_str = '';
        foreach($options_changed['original'] as $option_name=>$option_value){
            $options_changed_str .= sprintf(
                "The value of the option <strong>%s</strong> was changed from <strong>'%s'</strong> to <strong>'%s'</strong>.<br>\n",
                $option_name, $option_value, $options_changed['changed'][$option_name]);
        }

        // Notify via email that these options were modified.
        $page_referer = FALSE;
        switch($_POST['option_page']){
            case 'options':
                $page_referer = 'Global';
                break;
            case 'general':
            case 'writing':
            case 'reading':
            case 'discussion':
            case 'media':
            case 'permalink':
                $page_referer = ucwords($_POST['option_page']);
                break;
            default:
                $page_referer = 'Common';
                break;
        }
        if($page_referer){
            sucuri_event(3, 'core', "{$page_referer} settings updated.");
            sucuri_notify_event(
                'settings_updated', 'Website settings updated',
                "An administrator has updated {$page_referer} settings.<br>\n{$options_changed_str}");
        }
    }
}


/* sucuri_wpmenu:
 * Adds the proper menus on the wp-admin sidebar.
 */
function sucuri_wpmenu()
{
    /* We auto white list admins. They can disable / modify settings anyway. */
    if(current_user_can('manage_options') && isset($_SERVER['SUCURI_RIP']))
    {
        if(!is_file(sucuri_dir_filepath("whitelist/".$_SERVER['SUCURI_RIP'])))
        {
            @touch(sucuri_dir_filepath("whitelist/".$_SERVER['SUCURI_RIP']));
        }
    }

    add_menu_page('Sucuri Security', 'Sucuri Security', 'manage_options',
                  'sucurisec', 'sucuri_admin_page', SUCURI_IMG.'menu-icon.png');

    add_submenu_page('sucurisec', 'Dashboard', 'Dashboard', 'manage_options',
                     'sucurisec', 'sucuri_admin_page');

    add_submenu_page('sucurisec', 'Settings', 'Settings', 'manage_options',
                     'sucurisec_settings', 'sucuri_settings_page');

    add_submenu_page('sucurisec', 'Black & White List', 'Black & White List', 'manage_options',
                     'sucurisec_blacklist', 'sucuri_blacklist_page');

    add_submenu_page('sucurisec', 'Malware Scanning', 'Malware Scanner',
                     'manage_options',
                     'sucurisec_malwarescan', 'sucuri_malwarescan_page');

    add_submenu_page('sucurisec', '1-Click Hardening', '1-Click Hardening',
                     'manage_options',
                     'sucurisec_hardening', 'sucuri_hardening_page');

    add_submenu_page('sucurisec', 'Post-Hack', 'Post-Hack', 'manage_options',
                     'sucurisec_posthack', 'sucuri_posthack_page');

    add_submenu_page('sucurisec', 'Last Logins', 'Last Logins', 'manage_options',
                     'sucurisec_lastlogins', 'sucuri_lastlogins_page');

    add_submenu_page('sucurisec', 'Site Info', 'Site Info', 'manage_options',
                     'sucurisec_infosys', 'sucuri_infosys_page');

    add_submenu_page('sucurisec', 'About', 'About', 'manage_options',
                     'sucurisec_about', 'sucuri_about_page');
}



/* sucuri_pagestop:
 * Prints the top (header) for all internal pages.
 */
function sucuri_pagestop($sucuri_title = 'Sucuri Plugin')
{
    if(!current_user_can('manage_options'))
    {
        wp_die(__('You do not have sufficient permissions to access this page.') );
    }
    ?>
    <div class="wrap">
    <div id="icon-link-manager" class="icon32"><br /></div>
    <h2><?php echo htmlspecialchars($sucuri_title); ?></h2>
    <br class="clear"/>
    <?php
}






function sucuri_blacklist_page()
{
    $errrm = NULL;
    $pagination_perpage = 20;
    $mywhitelistips = sucuri_get_iplist('whitelist');
    $myblockedips   = sucuri_get_iplist('blocks');


    /**
     * Block listed IP Addresses
     *
     * This conditional statement verify the WP-Nonce for multiple actions in the
     * block listed ip addresses table. These are the actions processed here:
     *
     * Flush Block List
     * Search Block List
     * Unblocking IP Addresses
     */
    if(
        isset($_POST['sucuri_blocklist_nonce'])
        && wp_verify_nonce($_POST['sucuri_blocklist_nonce'], 'sucuri_blocklist_nonce')
    ){
        /* Search Block List */
        if(
            isset($_POST['wpsucuri_blocklist_search'])
            && isset($_POST['wpsucuri_blocklist_search_ip'])
        ){
            $wpsucuri_blocklist_search_ip = htmlspecialchars(trim($_POST['wpsucuri_blocklist_search_ip']));
            if( !empty($_POST['wpsucuri_blocklist_search_ip']) ){
                $myblockedips_match = array();
                if( !empty($myblockedips) )
                {
                    foreach ($myblockedips as $block_ip) {
                        if (strpos($block_ip, $wpsucuri_blocklist_search_ip) !== false) {
                            $myblockedips_match[] = $block_ip;
                        }
                    }
                    $myblockedips = $myblockedips_match;
                }
            }
        }


        /* Unblocking IP Addresses */
        else if(
            isset($_POST['wpsucuri_remove_blocklist'])
            && isset($_POST['sucuri_ipaddresses'])
            && is_array($_POST['sucuri_ipaddresses'])
            && !empty($_POST['sucuri_ipaddresses'])
        ){
            $ip_removed = $ip_failed = array();
            foreach($_POST['sucuri_ipaddresses'] as $iptorm){
                $iptorm = trim($iptorm);
                if( sucuri_is_valid_ip($iptorm) && @unlink(sucuri_dir_filepath('blocks/'.$iptorm)) ){
                    $ip_removed[] = $iptorm;
                }else{
                    $ip_failed[] = $iptorm;
                }
            }

            $myblockedips = sucuri_get_iplist('blocks');
            if( !empty($ip_removed) ){
                $ip_removed_str = '<code>'.implode(', ', $ip_removed).'</code>';
                sucuri_admin_notice('updated', 'IP Addresses removed from the block list: '.$ip_removed_str);
                sucuri_alerts('sucuri_wp_plugin_alert', 'Plugin settings changed', 'IP Addresses removed from the block list: '.$ip_removed_str);
            }
            if( !empty($ip_failed) ){
                $ip_failed_str = '<code>'.implode(', ', $ip_failed).'</code>';
                sucuri_admin_notice('error', 'These ip addresses were not unblocked whether because are invalid or because are inaccesible: '.$ip_failed_str);
            }
        }


        /* Flush Block List */
        else if( isset($_POST['wpsucuri_flush_blocklist']) ){
            $block_ips = sucuri_read_block_list_from_dir();
            if( !empty($block_ips) ){
                $block_ips_total = count($block_ips);
                foreach ($block_ips as $block_ip) {
                    @unlink($block_ip);
                }

                $myblockedips = sucuri_get_iplist('blocks');
                sucuri_admin_notice('updated', 'All the IP Addresses black listed were cleared, <code>'.$block_ips_total.'</code> addresses were unblocked to access your site.');
                sucuri_alerts("sucuri_wp_plugin_alert", "Plugin settings changed", "Sucuri Plugin black list cleared.");
            }
        }
    }


    /* White list removal */
    if( isset($_POST['sucuri_removewhitelistnonce']) ){
        if( !wp_verify_nonce($_POST['sucuri_removewhitelistnonce'], 'sucuri_removewhitelistnonce') ){
            return(NULL);
        }

        $iptorm = trim($_POST['sucuri_ipaddress']);
        if( sucuri_is_valid_ip($iptorm) ){
            @unlink(sucuri_dir_filepath('whitelist/'.$iptorm));
            $mywhitelistips = sucuri_get_iplist('whitelist');
            sucuri_admin_notice('updated', "IP Address <code>{$iptorm}</code> was removed from the white list.");
            sucuri_alerts("sucuri_wp_plugin_alert", "Plugin settings changed", "IP: {$iptorm} removed from white list");
        }else{
            sucuri_admin_notice('error', "The ip address specified <code>{$iptorm}</code> is invalid.");
        }
    }


    /* Adding to the white list. */
    if( isset($_POST['wpsucuri_whiteaddip']) ){
        if( !wp_verify_nonce($_POST['sucuri_whitelistnonce'], 'sucuri_whitelistnonce') ){
            return(NULL);
        }

        $iptorm = $_POST['wpsucuri_whiteaddip'];
        if( sucuri_is_valid_ip($iptorm) ){
            $whitelist_filepath = sucuri_dir_filepath('whitelist/'.$iptorm);
            if( file_exists($whitelist_filepath) ){
                sucuri_event(3, 'core', 'IDS: Website administrator tried to whitelist an existent ip address: '.$iptorm);
                sucuri_admin_notice('error', "The ip address specified <code>{$iptorm}</code> is already in the white list.");
            }else{
                @touch( sucuri_dir_filepath('whitelist/'.$iptorm) );
                $mywhitelistips = sucuri_get_iplist('whitelist');
                sucuri_event(3, 'core', 'IDS: Website administrator whitelisted this ip address: '.$iptorm);
                sucuri_admin_notice('updated', "The ip address specified <code>{$iptorm}</code> was added to the white list.");
                sucuri_alerts("sucuri_wp_plugin_alert", "Plugin settings changed", "IP: {$iptorm} added to the white list");
            }
        }else{
            sucuri_admin_notice('error', "The ip address specified <code>{$iptorm}</code> is invalid.");
        }
    }
    ?>
    <div class="wrap">
        <h2 id="warnings_hook"></h2>
        <div class="sucuriscan_header">
            <a href="http://sucuri.net/signup" target="_blank" title="Sucuri Security">
                <img src="<?php echo SUCURI_PLUGIN_URL; ?>/inc/images/logo.png" alt="Sucuri Security" />
            </a>
            <h2>Sucuri Security WordPress Plugin (Black &amp; White Lists)</h2>
        </div>

        <div class="postbox-container" style="width:75%">
            <div class="sucuriscan-maincontent">
                <div class="sucuri-whitelist-form">
                    <table class="wp-list-table widefat">
                        <thead>
                            <tr>
                                <th colspan="3" class="thead-with-button">
                                    <span>Whitelisted IP Addresses</span>
                                    <form method="post" class="thead-topright-action">
                                        <input type="hidden" name="sucuri_whitelistnonce" value="<?php echo wp_create_nonce('sucuri_whitelistnonce'); ?>" />
                                        <input type="text" name="wpsucuri_whiteaddip" size="15" class="input-text" />
                                        <input type="submit" value="Whitelist IP" class="button-primary" />
                                    </form>
                                </th>
                            </tr>
                            <tr>
                                <th class="manage-column">IP Address</th>
                                <th width="170" class="manage-column">Whitelisted at</th>
                                <th width="100" class="manage-column">Actions</th>
                            </tr>
                            <?php
                            $remediation = get_option('sucuri_wp_re');
                            if( $remediation!==FALSE && $remediation=='disabled' ): ?>
                                <tr>
                                    <th colspan="3">
                                        <h3>Warning: Active response is disabled. No blocking will be done. You can re-enable on the plugin settings.</h3>
                                    </th>
                                </tr>
                            <?php endif; ?>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="3" align="center">
                                    <i>* Your current IP address:</i>
                                    <code>
                                        <?php echo $_SERVER["REMOTE_ADDR"]; ?>
                                        <?php if( isset($_SERVER['SUCURI_RIP']) && $_SERVER['SUCURI_RIP']!=$_SERVER['REMOTE_ADDR'] ): ?>
                                            (<?php echo $_SERVER['SUCURI_RIP']; ?>)
                                        <?php endif; ?>
                                    </code>
                                </td>
                            </tr>

                            <?php if( !empty($mywhitelistips) ): ?>
                                <?php
                                $pages = array_chunk($mywhitelistips, $pagination_perpage);
                                $pgkey = isset($_GET['showpage_whitelist']) ? intval($_GET['showpage_whitelist']) : 1;
                                $items_in_page = $pages[$pgkey-1];
                                if( is_array($items_in_page) && !empty($items_in_page) ):
                                    ?>
                                    <?php foreach($items_in_page as $mybip): ?>
                                        <tr>
                                            <td class="monospace"><?php echo $mybip; ?></td>
                                            <td class="monospace"><?php echo sucuri_ip_creation($mybip, 'whitelist'); ?></td>
                                            <td>
                                                <form method="post">
                                                    <input type="hidden" name="sucuri_removewhitelistnonce" value="<?php echo wp_create_nonce('sucuri_removewhitelistnonce'); ?>" />
                                                    <input type="hidden" name="sucuri_ipaddress" value="<?php echo $mybip; ?>" />
                                                    <input type="submit" name="wpsucuri_whiteremoveip" value="Remove" class="button-primary" />
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3">
                                        <i>No IP Address whitelisted so far.</i>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>

                        <?php if( count($mywhitelistips) > $pagination_perpage ): ?>
                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <div class='pagination'>
                                            <?php for( $i=1; $i< count($pages)+1; $i++ ): ?>
                                                <?php $current_page = ( $pgkey==$i ) ? 'current' : ''; ?>
                                                <a href="?page=sucurisec_blacklist&showpage_whitelist=<?php echo $i; ?>" class="<?php echo $current_page; ?>"><?php echo $i; ?></a>
                                            <?php endfor; ?>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        <?php endif; ?>
                    </table>
                </div><!-- // End whitelist-form -->


                <form method="post" class="sucuri-blocklist-form">
                    <input type="hidden" name="sucuri_blocklist_nonce" value="<?php echo wp_create_nonce('sucuri_blocklist_nonce'); ?>" />

                    <table class="wp-list-table widefat">
                        <thead>
                            <tr>
                                <th colspan="3">Blacklisted IP Addresses</th>
                            </tr>
                            <tr>
                                <th colspan="3" class="thead-with-button">
                                    <span>Search IP Address</span>
                                    <div method="post" class="thead-topright-action">
                                        <?php $ip_query = ( isset($wpsucuri_blocklist_search_ip) && !empty($wpsucuri_blocklist_search_ip) ) ? $wpsucuri_blocklist_search_ip : ''; ?>
                                        <input type="text" name="wpsucuri_blocklist_search_ip" value="<?php echo $ip_query; ?>" class="input-text" />
                                        <input type="submit" name="wpsucuri_blocklist_search" value="Search" class="button-primary" />
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="3" class="thead-with-button">
                                    <span>Flush/Remove</span>
                                    <input type="submit" name="wpsucuri_flush_blocklist" value="Clear blacklist" class="button-primary thead-topright-action" style="margin-left:5px" />
                                    <input type="submit" name="wpsucuri_remove_blocklist" value="Remove selected" class="button-primary thead-topright-action" />
                                </th>
                            </tr>
                            <tr>
                                <th class="manage-column column-cb check-column">
                                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                                    <input id="cb-select-all-1" type="checkbox">
                                </th>
                                <th class="manage-column">IP Address</th>
                                <th class="manage-column">Blocked at</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if( !empty($myblockedips) ): ?>
                                <?php
                                $pages = array_chunk($myblockedips, $pagination_perpage);
                                $pgkey = isset($_GET['showpage_blocklist']) ? intval($_GET['showpage_blocklist']) : 1;
                                $items_in_page = $pages[$pgkey-1];
                                if( is_array($items_in_page) && !empty($items_in_page) ):
                                    ?>
                                    <?php foreach($pages[$pgkey-1] as $mybip): ?>
                                        <tr>
                                            <th class="check-column">
                                                <input type="checkbox" name="sucuri_ipaddresses[]" value="<?php echo $mybip; ?>" />
                                            </th>
                                            <td class="monospace"><?php echo $mybip; ?></td>
                                            <td width="170" class="monospace"><?php echo sucuri_ip_creation($mybip, 'blocks'); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3">
                                        <i>No IP Address blocked so far.</i>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>

                        <?php if( count($myblockedips) > $pagination_perpage ): ?>
                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <div class='pagination'>
                                            <?php for( $i=1; $i< count($pages)+1; $i++ ): ?>
                                                <?php $current_page = ( $pgkey==$i ) ? 'current' : ''; ?>
                                                <a href="?page=sucurisec_blacklist&showpage_blocklist=<?php echo $i; ?>" class="<?php echo $current_page; ?>"><?php echo $i; ?></a>
                                            <?php endfor; ?>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        <?php endif; ?>
                    </table>
                </form>
                <div class="sucuri-clearfix"></div>

            </div><!-- End sucuriscan-maincontent -->
        </div><!-- End postbox-container -->

        <?php echo sucuri_wp_sidebar_gen(); ?>
    </div><!-- End wrap -->
    <?php
}


function sucuri_get_iplist($ip_address_group='blocks'){
    /* List of Whitelisted and Blocklisted ip addresses */
    $allowed_groups = array('whitelist', 'blocks');
    $ip_address_list = array();

    if( in_array($ip_address_group, $allowed_groups) ){
        if( is_dir(sucuri_dir_filepath("{$ip_address_group}/")) ){
            $listofips = scandir(sucuri_dir_filepath("{$ip_address_group}/"));
            if( count($listofips>3) ){
                foreach($listofips as $uniqip){
                    if(
                           strncmp($uniqip, "<", 1) == 0
                        || strncmp($uniqip, "#", 1) == 0
                        || strncmp($uniqip, ".", 1) == 0
                        || strncmp($uniqip, "b", 1) == 0
                        || strncmp($uniqip, "i", 1) == 0
                    ){
                        continue;
                    }

                    if( sucuri_is_valid_ip($uniqip) ){
                        $ip_address_list[] = htmlspecialchars($uniqip);
                    }else{
                        /* Remove any other invalid file in these folders */
                        @unlink( sucuri_dir_filepath("{$ip_address_group}/{$uniqip}") );
                    }
                }

                /* Clear Whitelisted addresses from Block List */
                if( $ip_address_group=='whitelist' ){
                    foreach( $ip_address_list as $whitelist_ip ){
                        @unlink(sucuri_dir_filepath('blocks/'.$whitelist_ip));
                    }
                }
            }
        }
    }

    return $ip_address_list;
}

function sucuri_ip_creation($ip_address=0, $type='blocks', $in_timestamp=FALSE){
    $creation = 0;
    $ip_filepath = sucuri_dir_filepath($type.'/'.$ip_address);
    if( file_exists($ip_filepath) ){
        $creation = filemtime($ip_filepath);
        if( !$in_timestamp ){
            $creation = date('d/M/Y H:i', $creation);
        }
    }
    return $creation;
}



/* Sucuri's dashboard (main admin page) */
function sucuri_admin_page()
{
    $U_ERROR = NULL;
    if(!current_user_can('manage_options'))
    {
        wp_die( __('You do not have sufficient permissions to access this page: Sucuri Dashboard') );
    }

    $sucuri_wp_key = get_option('sucuri_wp_key');
    if( isset($_POST['wpsucuri-modify-values']) || $sucuri_wp_key === FALSE )
    {
        sucuri_settings_page();
        return(1);
    }


    /* Admin header. */
    echo '<div class="wrap">';
    ?>
    <iframe style="overflow:hidden" width="100%" height="2250px" src='https://wordpress.sucuri.net/single.php?k=<?php echo $sucuri_wp_key;?>'>
    Unable to load iframe.
    </iframe>
    <br />
    <hr />
    <h3><i>Plugin developed by <a href="http://sucuri.net">Sucuri Security</a> | <a href="https://support.sucuri.net">Support & Help</a></i></h3>
    </div>
    <?php
}




/* sucuri_posthack_page:
 * Sucuri posthack's page.
 */
function sucuri_posthack_page()
{
    if( !current_user_can('manage_options') )
    {
        wp_die(__('You do not have sufficient permissions to access this page: Sucuri Post-Hack') );
    }

    $sucuri_backup = new SucuriBackup();

    // Page pseudo-variables initialization.
    $template_variables = array(
        'SucuriURL'=>SUCURI_PLUGIN_URL,
        'PosthackNonce'=>wp_create_nonce('sucuri_posthack_nonce'),
        'SucuriWPSidebar'=>sucuri_wp_sidebar_gen(),
        'WPConfigUpdate.Display'=>'display:none',
        'WPConfigUpdate.NewConfig'=>'',
        'ResetPassword.UserList'=>'',
        'BackupList'=>''
    );

    // Process form submission
    if( isset($_POST['sucuri_posthack_action']) ){
        if( !wp_verify_nonce($_POST['sucuri_posthack_nonce'], 'sucuri_posthack_nonce') )
        {
            wp_die(__('Wordpress Nonce verification failed, try again going back and checking the form.') );
        }

        switch($_POST['sucuri_posthack_action']){
            case 'update_wpconfig':
                $update_wpconfig = ( isset($_POST['sucuri_update_wpconfig']) && $_POST['sucuri_update_wpconfig']==1 ) ? TRUE : FALSE;

                if( $update_wpconfig ){
                    $wpconfig_process = sucuri_set_new_config_keys();
                    $template_variables['WPConfigUpdate.Display'] = 'display:block';

                    if($wpconfig_process){
                        if( $wpconfig_process['updated']===TRUE ){
                            sucuri_admin_notice('updated', '<strong>OK.</strong> WP-Config keys updated successfully. In the textarea bellow you will see the old-keys and the new-keys updated.');
                            $template_variables['WPConfigUpdate.NewConfig'] .= "// Old Keys\n";
                            $template_variables['WPConfigUpdate.NewConfig'] .= $wpconfig_process['old_keys_string'];
                            $template_variables['WPConfigUpdate.NewConfig'] .= "//\n";
                            $template_variables['WPConfigUpdate.NewConfig'] .= "// New Keys\n";
                            $template_variables['WPConfigUpdate.NewConfig'] .= $wpconfig_process['new_keys_string'];
                        }else{
                            sucuri_admin_notice('error', '<strong>Error.</strong> The wp-config.php file is not writable, please copy and paste the code shown bellow in the textarea into that file manually.');
                            $template_variables['WPConfigUpdate.NewConfig'] = $wpconfig_process['new_wpconfig'];
                        }
                    }else{
                        sucuri_admin_notice('error', '<strong>Error.</strong> The wp-config.php file was not found in the default location.');
                    }
                }else{
                    sucuri_admin_notice('error', '<strong>Error.</strong> You need to confirm that you understand the risk of this operation');
                }
                break;
            case 'reset_password':
                $reset_password = ( isset($_POST['sucuri_reset_password']) && $_POST['sucuri_reset_password']==1 ) ? TRUE : FALSE;

                if( $reset_password ){
                    $user_identifiers = isset($_POST['user_ids']) ? $_POST['user_ids'] : array();
                    $pwd_changed = $pwd_not_changed = array();

                    if( is_array($user_identifiers) && !empty($user_identifiers) ){
                        arsort($user_identifiers);
                        foreach($user_identifiers as $user_id){
                            if( sucuri_new_password($user_id) ){
                                $pwd_changed[] = $user_id;
                            }else{
                                $pwd_not_changed[] = $user_id;
                            }
                        }

                        if( !empty($pwd_changed) ){
                            sucuri_admin_notice('updated', '<strong>OK.</strong> Password changed successfully for users: '.implode(', ',$pwd_changed));
                        }
                        if( !empty($pwd_not_changed) ){
                            sucuri_admin_notice('error', '<strong>Error.</strong> Password change failed for users: '.implode(', ',$pwd_not_changed));
                        }
                    }else{
                        sucuri_admin_notice('error', '<strong>Error.</strong> You did not select any user account to be reseted');
                    }
                }else{
                    sucuri_admin_notice('error', '<strong>Error.</strong> You need to confirm that you understand the risk of this operation');
                }
                break;
            case 'database_backup':
                if( isset($_POST['generate_dbbackup']) ){
                    $dbbackup_filepath = $sucuri_backup->all_database();
                    if( $dbbackup_filepath ){
                        $dbbackup_fileurl = admin_url( '?sucuri_download_dbbackup='.basename($dbbackup_filepath) );
                        sucuri_admin_notice('updated', "<strong>Sucuri WP Plugin</strong>. Database backup
                            generated and stored in the plugin upload folder, you can download the file from here:
                            <a href='{$dbbackup_fileurl}' target='_blank'><strong>Download DB Backup</strong></a>"
                        );
                    }
                }

                if( isset($_POST['remove_dbbackup']) ){
                    $backups_to_remove = isset($_POST['dbbackup_filenames']) ? $_POST['dbbackup_filenames'] : array();
                    $sucuri_backup->remove_backup_file($backups_to_remove);
                }
                break;
            default:
                wp_die(__('Sucuri WP Plugin, invalid form action, go back and try again.'));
                break;
        }
    }

    // Fill the user list for ResetPassword action.
    $user_list = get_users();
    foreach($user_list as $user){
        $user_snippet = sucuri_get_template('resetpassword.snippet.tpl', array(
            'ResetPassword.UserId'=>$user->ID,
            'ResetPassword.Username'=>$user->user_login,
            'ResetPassword.Displayname'=>$user->display_name,
            'ResetPassword.Email'=>$user->user_email,
            'ResetPassword.Registered'=>$user->user_registered
        ));
        $template_variables['ResetPassword.UserList'] .= $user_snippet;
    }

    // Fill the file list for Backups.
    $backup_files = $sucuri_backup->get_backup_files();
    foreach($backup_files as $backup_file){
        $backupfile_snippet = sucuri_get_template('posthack-backups.snippet.tpl', array(
            'BackupList.FileURL'=>admin_url("?sucuri_download_dbbackup={$backup_file->filename}"),
            'BackupList.Filename'=>$backup_file->filename,
            'BackupList.Filetype'=>strtoupper($backup_file->fileext),
            'BackupList.Filesize'=>( $backup_file->filesize>0 ? "{$backup_file->filesize} (~ {$backup_file->filehumansize})" : 0 ),
            'BackupList.Filetime'=>date('Y/M/d H:i', $backup_file->filetime),
        ));
        $template_variables['BackupList'] .= $backupfile_snippet;
    }

    echo sucuri_get_template('posthack.html.tpl', $template_variables);
}

function sucuri_download_dbbackup(){
    if(
        current_user_can('manage_options')
        AND isset($_GET['sucuri_download_dbbackup'])
    ){
        $sucuri_backup = new SucuriBackup();
        $backup_file = $sucuri_backup->get_backup_file_from_filename($_GET['sucuri_download_dbbackup']);
        if( $backup_file ){
            switch($backup_file->fileext){
                case 'sql':
                    $download_filename = sprintf('%s.sql', DB_NAME);
                    header('Content-Type: application/x-sql');
                    break;
                case 'zip':
                    $download_filename = sprintf('%s.sql.zip', DB_NAME);
                    header('Content-Type: application/zip');
                    break;
                default:
                    $download_filename = $backup_file->filename;
                    header('Content-Type: application/octet-stream');
                    break;
            }
            header('Content-Disposition: attachment; filename='.$download_filename);
            header('Content-Length: '.$backup_file->filesize);
            header('Cache-Control: private');
            if( $fd=fopen($backup_file->filepath,'r') ){
                while( !feof($fd) ){ echo fread($fd, 2048); }
            }
            fclose($fd);
        }
    }
}
add_action('admin_init', 'sucuri_download_dbbackup');

/* sucuri_lastlogins_page:
 * Sucuri lastlogins' page.
 */
function sucuri_lastlogins_page()
{
    if( !current_user_can('manage_options') )
    {
        wp_die(__('You do not have sufficient permissions to access this page: Sucuri Last-Logins') );
    }

    // Page pseudo-variables initialization.
    $template_variables = array(
        'SucuriURL'=>SUCURI_PLUGIN_URL,
        'LastLoginsNonce'=>wp_create_nonce('sucuri_lastlogins_nonce'),
        'SucuriWPSidebar'=>sucuri_wp_sidebar_gen(),
        'UserList'=>'',
        'CurrentURL'=>site_url().'/wp-admin/admin.php?page='.$_GET['page'],
    );

    if( !sucuri_lastlogins_datastore_is_writable() ){
        sucuri_admin_notice('error', '<strong>Error.</strong> The last-logins datastore
            file is not writable, gives permissions to write in this location:<br>'.
            '<code>'.sucuri_lastlogins_datastore_filepath().'</code>');
    }

    $limit = isset($_GET['limit']) ? intval($_GET['limit']) : SUCURI_LASTLOGINS_USERSLIMIT;
    $template_variables['UserList.ShowAll'] = $limit>0 ? 'visible' : 'hidden';

    $user_list = sucuri_get_logins($limit);
    foreach($user_list as $user){
        $user_snippet = sucuri_get_template('lastlogins.snippet.tpl', array(
            'UserList.UserId'=>intval($user->ID),
            'UserList.Username'=>( !is_null($user->user_login) ? $user->user_login : '<em>Unknown</em>' ),
            'UserList.Email'=>$user->user_email,
            'UserList.RemoteAddr'=>$user->user_remoteaddr,
            'UserList.Datetime'=>$user->user_lastlogin
        ));
        $template_variables['UserList'] .= $user_snippet;
    }

    echo sucuri_get_template('lastlogins.html.tpl', $template_variables);
}

function sucuri_lastlogins_datastore_filepath(){
    $plugin_upload_folder = sucuri_dir_filepath();
    $datastore_filepath = rtrim($plugin_upload_folder,'/').'/sucuri-lastlogins.php';
    return $datastore_filepath;
}

function sucuri_lastlogins_datastore_exists(){
    $datastore_filepath = sucuri_lastlogins_datastore_filepath();
    if( !file_exists($datastore_filepath) ){
        if( @file_put_contents($datastore_filepath, "<?php die('403 Forbidden'); ?>\n", LOCK_EX) ){
            @chmod($datastore_filepath, 0644);
        }
    }

    return file_exists($datastore_filepath) ? $datastore_filepath : FALSE;
}

function sucuri_lastlogins_datastore_is_writable(){
    $datastore_filepath = sucuri_lastlogins_datastore_exists();
    if($datastore_filepath){
        if( !is_writable($datastore_filepath) ){
            @chmod($datastore_filepath, 0644);
        }
        return is_writable($datastore_filepath) ? $datastore_filepath : FALSE;
    }
    return FALSE;
}

function sucuri_lastlogins_datastore_is_readable(){
    $datastore_filepath = sucuri_lastlogins_datastore_exists();
    if( $datastore_filepath && is_readable($datastore_filepath) ){
        return $datastore_filepath;
    }
    return FALSE;
}

if( !function_exists('sucuriscan_set_lastlogin') ){
    function sucuri_set_lastlogin($user_login=''){
        $datastore_filepath = sucuri_lastlogins_datastore_is_writable();

        if($datastore_filepath){
            $current_user = get_user_by('login', $user_login);
            $remote_addr = sucuri_get_remoteaddr();

            $login_info = array(
                'user_id'=>$current_user->ID,
                'user_login'=>$current_user->user_login,
                'user_remoteaddr'=>$remote_addr,
                'user_hostname'=>@gethostbyaddr($remote_addr),
                'user_lastlogin'=>current_time('mysql')
            );

            @file_put_contents($datastore_filepath, serialize($login_info)."\n", FILE_APPEND);
        }
    }
    add_action('wp_login', 'sucuri_set_lastlogin', 50);
}

function sucuri_get_logins($limit=10, $user_id=0){
    $lastlogins = array();
    $datastore_filepath = sucuri_lastlogins_datastore_is_readable();

    if($datastore_filepath){
        $parsed_lines = 0;
        $lastlogins_lines = array_reverse(file($datastore_filepath));
        foreach($lastlogins_lines as $line){
            $line = str_replace("\n", '', $line);
            if( preg_match('/^a:/', $line) ){
                $user_lastlogin = unserialize($line);

                /* Only administrators can see all login stats */
                if( !current_user_can('manage_options') ){
                    $current_user = wp_get_current_user();
                    if( $current_user->user_login!=$user_lastlogin['user_login'] ){ continue; }
                }

                /* If an User_Id was specified when this function was called, filter by that number */
                if( $user_id>0 ){
                    if( $user_lastlogin['user_id']!=$user_id ){ continue; }
                }

                /* Get the WP_User object and add extra information from the last-login data */
                $user_account = get_userdata($user_lastlogin['user_id']);
                $user_account_exists = ($user_account instanceof WP_User) ? TRUE : FALSE;
                if( !$user_account_exists ){
                    $user_account = (object)array(
                        'ID'=>0,
                        'user_login'=>NULL,
                        'user_email'=>NULL,
                        'display_name'=>'Unknown'
                    );
                }
                foreach($user_lastlogin as $user_extrainfo_key=>$user_extrainfo_value){
                    if($user_account_exists){
                        $user_account->data->{$user_extrainfo_key} = $user_extrainfo_value;
                    }else{
                        $user_account->{$user_extrainfo_key} = $user_extrainfo_value;
                    }
                }
                $lastlogins[] = $user_account;
                $parsed_lines += 1;
            }

            if( preg_match('/^([0-9]+)$/', $limit) && $limit>0 ){
                if( $parsed_lines>=$limit ){ break; }
            }
        }
    }

    return $lastlogins;
}

if( !function_exists('sucuriscan_login_redirect') ){
    function sucuri_login_redirect($redirect_to='', $request=NULL, $user=FALSE){
        global $user;
        if( $user instanceof WP_User && isset($user->roles) && is_array($user->roles) ){
            if( in_array('administrator', $user->roles) ){
                return add_query_arg( 'sucuri_lastlogin_message', 1, admin_url() );
            }
        }
        return $redirect_to;
    }
    if( get_option('sucuri_lastlogin_redirection')!='disabled' ){
        add_filter('login_redirect', 'sucuri_login_redirect', 10, 3);
    }
}

if( !function_exists('sucuriscan_get_user_lastlogin') ){
    function sucuri_get_user_lastlogin(){
        if( isset($_GET['sucuri_lastlogin_message']) && current_user_can('manage_options') ){
            $current_user = wp_get_current_user();

            // Select the penultimate entry, not the last one.
            $user_lastlogins = sucuri_get_logins(2, $current_user->ID);
            $row = isset($user_lastlogins[1]) ? $user_lastlogins[1] : FALSE;

            if($row){
                $message_tpl  = 'The last time you logged in was: %s, from %s - %s';
                $lastlogin_message = sprintf( $message_tpl, date('Y/M/d H:i'), $row->user_remoteaddr, $row->user_hostname );
                $lastlogin_message .= chr(32).'(<a href="'.site_url('wp-admin/admin.php?page='.SUCURI.'_lastlogins').'">View Last-Logins</a>)';
                sucuri_admin_notice('updated', $lastlogin_message);
            }
        }
    }
    add_action('admin_notices', 'sucuri_get_user_lastlogin');
}

/* sucuri_posthack_page:
 * Sucuri posthack's page.
 */
function sucuri_infosys_page(){
    if( !current_user_can('manage_options') )
    {
        wp_die(__('You do not have sufficient permissions to access this page: Sucuri Last-Logins') );
    }

    // Page pseudo-variables initialization.
    $template_variables = array(
        'SucuriURL'=>SUCURI_PLUGIN_URL,
        'SucuriWPSidebar'=>sucuri_wp_sidebar_gen(),
        'CurrentURL'=>site_url().'/wp-admin/admin.php?page='.$_GET['page']
    );

    $template_variables['LoggedInUsers'] = sucuri_infosys_loggedin();
    $template_variables['HTAccessIntegrity'] = sucuri_infosys_htaccess();
    $template_variables['WordpressConfig'] = sucuri_infosys_wpconfig();

    echo sucuri_get_template('infosys.html.tpl', $template_variables);
}


function sucuri_infosys_htaccess(){
    $htaccess_path = sucuri_get_htaccess_path();

    $template_variables = array(
        'HTAccess.Content' => '',
        'HTAccess.Message' => '',
        'HTAccess.MessageType' => '',
        'HTAccess.MessageVisible' => 'hidden',
        'HTAccess.TextareaVisible' => 'hidden',
    );

    if( $htaccess_path ){
        $htaccess_rules = file_get_contents($htaccess_path);

        $template_variables['HTAccess.MessageType'] = 'updated';
        $template_variables['HTAccess.MessageVisible'] = 'visible';
        $template_variables['HTAccess.TextareaVisible'] = 'visible';
        $template_variables['HTAccess.Content'] = $htaccess_rules;
        $template_variables['HTAccess.Message'] .= 'HTAccess file found in this path <code>'.$htaccess_path.'</code>';

        if( empty($htaccess_rules) ){
            $template_variables['HTAccess.TextareaVisible'] = 'hidden';
            $template_variables['HTAccess.Message'] .= '</p><p>The HTAccess file found is completely empty.';
        }
        if( sucuri_htaccess_is_standard($htaccess_rules) ){
            $template_variables['HTAccess.Message'] .= '</p><p>
                The main <code>.htaccess</code> file in your site has the standard rules for a WordPress installation. You can customize it to improve the
                performance and change the behaviour of the redirections for pages and posts in your site. To get more information visit the official documentation at
                <a href="http://codex.wordpress.org/Using_Permalinks#Creating_and_editing_.28.htaccess.29" target="_blank">Codex WordPrexx - Creating and editing (.htaccess)</a>';
        }
    }else{
        $template_variables['HTAccess.Message'] = 'Your website does not contains a <code>.htaccess</code> file or it was not found in the default location.';
        $template_variables['HTAccess.MessageType'] = 'error';
        $template_variables['HTAccess.MessageVisible'] = 'visible';
    }

    return sucuri_get_template('infosys-htaccess.html.tpl', $template_variables);
}


function sucuri_htaccess_is_standard($rules=FALSE){
    if( $rules===FALSE ){
        $htaccess_path = sucuri_get_htaccess_path();
        $rules = $htaccess_path ? file_get_contents($htaccess_path) : '';
    }

    if( !empty($rules) ){
        $standard_lines = array(
            '# BEGIN WordPress',
            '<IfModule mod_rewrite\.c>',
            'RewriteEngine On',
            'RewriteBase \/',
            'RewriteRule .index.\.php. - \[L\]',
            'RewriteCond %\{REQUEST_FILENAME\} \!-f',
            'RewriteCond %\{REQUEST_FILENAME\} \!-d',
            'RewriteRule \. \/index\.php \[L\]',
            '<\/IfModule>',
            '# END WordPress',
        );
        $pattern  = '';
        $standard_lines_total = count($standard_lines);
        foreach($standard_lines as $i=>$line){
            if( $i < ($standard_lines_total-1) ){
                $end_of_line = "\n";
            }else{
                $end_of_line = '';
            }
            $pattern .= sprintf("%s%s", $line, $end_of_line);
        }

        if( preg_match("/{$pattern}/", $rules) ){
            return TRUE;
        }
    }

    return FALSE;
}


function sucuri_infosys_wpconfig(){
    $template_variables = array(
        'WordpressConfig.Rules' => '',
        'WordpressConfig.Total' => 0,
        'WordpressConfig.Content' => '',
    );
    $ignore_wp_rules = array('DB_PASSWORD');

    $wp_config_path = sucuri_get_wpconfig_path();
    if( $wp_config_path ){
        add_thickbox();
        $wp_config_content = file($wp_config_path);
        $template_variables['WordpressConfig.Content'] = file_get_contents($wp_config_path);

        // Read WordPress main configuration file as text plain.
        $wp_config_rules = array();
        foreach( (array)$wp_config_content as $line ){
            $line = str_replace("\n", '', $line);

            // Ignore useless lines and append to the clean string the important lines.
            if( preg_match('/^define\(/', $line) ){
                $line = str_replace('define(', '', $line);
                $line = str_replace(');', '', $line);
                $line_parts = explode(',', $line, 2);
            }
            else if( preg_match('/^\$[a-zA-Z_]+/', $line) ){
                $line_parts = explode('=', $line, 2);
            }
            else{ continue; }

            // Clean and append the rule to the wp_config_rules variable.
            if( isset($line_parts) && count($line_parts)==2 ){
                $key_name = $key_value = '';
                foreach($line_parts as $i=>$line_part){
                    $line_part = trim($line_part);
                    $line_part = ltrim($line_part, '$');
                    $line_part = rtrim($line_part, ';');

                    // Remove single/double quotes at the beginning and end of the string.
                    $line_part = ltrim($line_part, "'");
                    $line_part = rtrim($line_part, "'");
                    $line_part = ltrim($line_part, '"');
                    $line_part = rtrim($line_part, '"');

                    // Assign the clean strings to specific variables.
                    if( $i==0 ){ $key_name  = $line_part; }
                    if( $i==1 ){ $key_value = $line_part; }
                }

                if( !in_array($key_name, $ignore_wp_rules) ){
                    $wp_config_rules[$key_name] = $key_value;
                }
            }
        }

        // Pass the WordPress configuration rules to the template and show them.
        foreach( $wp_config_rules as $var_name=>$var_value ){
            $template_variables['WordpressConfig.Total'] += 1;
            $template_variables['WordpressConfig.Rules'] .= sucuri_get_template('infosys-wpconfig.snippet.tpl', array(
                'WordpressConfig.VariableName' => $var_name,
                'WordpressConfig.VariableValue' => htmlentities($var_value),
            ));
        }
    }

    return sucuri_get_template('infosys-wpconfig.html.tpl', $template_variables);
}


function sucuri_infosys_loggedin(){
    // Get user logged in list.
    $template_variables = array(
        'LoggedInUsers.List' => '',
        'LoggedInUsers.Total' => 0,
    );

    $logged_in_users = sucuri_get_online_users();
    if( is_array($logged_in_users) && !empty($logged_in_users) ){
        $template_variables['LoggedInUsers.Total'] = count($logged_in_users);

        foreach( (array)$logged_in_users as $logged_in_user ){
            $logged_in_user['last_activity_datetime'] = date('d/M/Y H:i', $logged_in_user['last_activity']);
            $logged_in_user['user_registered_datetime'] = date('d/M/Y H:i', strtotime($logged_in_user['user_registered']));

            $template_variables['LoggedInUsers.List'] .= sucuri_get_template('infosys-loggedin.snippet.tpl', array(
                'LoggedInUsers.Id' => $logged_in_user['user_id'],
                'LoggedInUsers.UserURL' => admin_url('user-edit.php?user_id='.$logged_in_user['user_id']),
                'LoggedInUsers.UserLogin' => $logged_in_user['user_login'],
                'LoggedInUsers.UserEmail' => $logged_in_user['user_email'],
                'LoggedInUsers.LastActivity' => $logged_in_user['last_activity_datetime'],
                'LoggedInUsers.Registered' => $logged_in_user['user_registered_datetime'],
                'LoggedInUsers.RemoveAddr' => $logged_in_user['remote_addr'],
            ));
        }
    }

    return sucuri_get_template('infosys-loggedin.html.tpl', $template_variables);
}


function sucuri_get_online_users(){
    if( sucuri_is_multisite() ){
        return get_site_transient('online_users');
    }else{
        return get_transient('online_users');
    }
}


function sucuri_save_online_users($logged_in_users=array()){
    $expiration = 30 * 60;
    if( sucuri_is_multisite() ){
        return set_site_transient('online_users', $logged_in_users, $expiration);
    }else{
        return set_transient('online_users', $logged_in_users, $expiration);
    }
}


if( !function_exists('sucuri_unset_online_user_on_logout') ){
    function sucuri_unset_online_user_on_logout(){
        $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
        $remote_addr = sucuri_get_remoteaddr();

        sucuri_unset_online_user($user_id, $remote_addr);
    }

    add_action('wp_logout', 'sucuri_unset_online_user_on_logout');
}

function sucuri_unset_online_user($user_id=0, $remote_addr=0){
    $logged_in_users = sucuri_get_online_users();

    // Remove the specified user identifier from the list.
    if( is_array($logged_in_users) && !empty($logged_in_users) ){
        foreach($logged_in_users as $i=>$user){
            if(
                $user['user_id']==$user_id
                && strcmp($user['remote_addr'],$remote_addr)==0
            ){
                unset($logged_in_users[$i]);
                break;
            }
        }
    }

    return sucuri_save_online_users($logged_in_users);
}


if( !function_exists('sucuri_set_online_user') ){
    function sucuri_set_online_user($user_login='', $user=FALSE){
        if( $user ){
            // Get logged in user information.
            $current_user = ($user instanceof WP_User) ? $user : wp_get_current_user();
            $current_user_id = $current_user->ID;
            $remote_addr = sucuri_get_remoteaddr();
            $current_time = current_time('timestamp');
            $logged_in_users = sucuri_get_online_users();

            // Build the dataset array that will be stored in the transient variable.
            $current_user_info = array(
                'user_id' => $current_user_id,
                'user_login' => $current_user->user_login,
                'user_email' => $current_user->user_email,
                'user_registered' => $current_user->user_registered,
                'last_activity' => $current_time,
                'remote_addr' => $remote_addr
            );

            if( !is_array($logged_in_users) || empty($logged_in_users) ){
                $logged_in_users = array( $current_user_info );
                sucuri_save_online_users($logged_in_users);
            }else{
                $do_nothing = FALSE;
                $update_existing = FALSE;
                $item_index = 0;

                // Check if the user is already in the logged-in-user list and update it if is necessary.
                foreach($logged_in_users as $i=>$user){
                    if(
                        $user['user_id']==$current_user_id
                        && strcmp($user['remote_addr'],$remote_addr)==0
                    ){
                        if( $user['last_activity'] < ($current_time - (15 * 60)) ){
                            $update_existing = TRUE;
                            $item_index = $i;
                            break;
                        }else{
                            $do_nothing = TRUE;
                            break;
                        }
                    }
                }

                if($update_existing){
                    $logged_in_users[$item_index] = $current_user_info;
                    sucuri_save_online_users($logged_in_users);
                }else if($do_nothing){
                    // Do nothing.
                }else{
                    $logged_in_users[] = $current_user_info;
                    sucuri_save_online_users($logged_in_users);
                }
            }
        }
    }

    add_action('wp_login', 'sucuri_set_online_user', 10, 2);
}


function __ss_wraphardeningboxopen($msg){ ?>

    <div class="postbox hardening-box">
        <h3><?php echo $msg; ?></h3>
        <div class="inside">
<?php }

function __ss_wraphardeningboxclose(){ ?>
    </div><!-- End inside -->
    </div><!-- End postbox -->

<?php }

/* Sucuri one-click hardening page. */
function sucuri_hardening_page(){ ?>
    <div class="wrap">
        <h2 id="warnings_hook"></h2>
        <div class="sucuriscan_header">
            <a href="http://sucuri.net/signup" target="_blank" title="Sucuri Security">
                <img src="<?php echo SUCURI_PLUGIN_URL; ?>/inc/images/logo.png" alt="Sucuri Security" />
            </a>
            <h2>Sucuri Security WordPress Plugin (1-Click Hardening)</h2>
        </div>

        <div class="postbox-container" style="width:75%;">
            <div class="sucuriscan-maincontent">
                <?php

                if(isset($_POST['wpsucuri-doharden']))
                {
                    if(!wp_verify_nonce($_POST['sucuri_wphardeningnonce'], 'sucuri_wphardeningnonce'))
                    {
                        unset($_POST['wpsucuri-doharden']);
                    }
                }

                ?>

                <div id="poststuff">
                    <form action="" method="post">
                        <input type="hidden" name="sucuri_wphardeningnonce" value="<?php echo wp_create_nonce('sucuri_wphardeningnonce'); ?>" />
                        <input type="hidden" name="wpsucuri-doharden" value="wpsucuri-doharden" />

                        <?php
                        sucuri_harden_version();
                        sucuri_cloudproxy_enabled();
                        sucuri_wp_harden_removegenerator();
                        sucuri_harden_upload();
                        sucuri_harden_wpcontent();
                        sucuri_harden_wpincludes();
                        sucuri_harden_keys();
                        sucuri_harden_readme();
                        sucuri_harden_dbtables();
                        sucuri_harden_adminuser();
                        sucuri_harden_phpversion();
                        sucuri_harden_fileeditor();
                        ?>

                        <p align="center">
                            <br />
                            <strong>If you have any questions about these checks or this plugin, contact
                            us at <a href="mailto:support@sucuri.net">support@sucuri.net</a> or visit
                            <a href="http://sucuri.net">Sucuri Security</a></strong>
                        </p>
                    </form>

                </div><!-- End poststuff -->
            </div><!-- End sucuriscan-maincontent -->
        </div><!-- End postbox-container -->

        <?php echo sucuri_wp_sidebar_gen(); ?>

    </div><!-- End wrap -->

<?php }







/* Sucuri malware scan page. */
function sucuri_malwarescan_page()
{
    $U_ERROR = NULL;
    if(!current_user_can('manage_options'))
    {
        wp_die(__('You do not have sufficient permissions to access this page: Sucuri Malware Scanner') );
    }
    ?>
    <div class="wrap">
        <h2 id="warnings_hook"></h2>
        <div class="sucuriscan_header">
            <a href="http://sucuri.net/signup" target="_blank" title="Sucuri Security">
                <img src="<?php echo SUCURI_PLUGIN_URL; ?>/inc/images/logo.png" alt="Sucuri Security" />
            </a>
            <h2>Sucuri Security WordPress Plugin (Malware Scanner)</h2>
        </div>

        <div class="postbox-container" style="width:75%;">
            <div class="sucuriscan-maincontent">
                <div id="poststuff">
                    <div class="postbox">
                        <h3>Sucuri Malware Scanner</h3>
                        <div class="inside">
                            <p>
                                Execute an external malware scan on your site, using the <a href="http://sucuri.net">Sucuri</a> scanner.
                                It will alert you if your site is compromised with malware, spam, defaced or blacklisted.
                            </p>
                            <a target="_blank" href="http://sitecheck.sucuri.net/results/<?php echo home_url();?>" class="button-primary">Scan now!</a>
                        </div>
                    </div>
                </div><!-- End poststuff -->

            </div><!-- End sucuriscan-maincontent -->
        </div><!-- End postbox-container -->

        <?php echo sucuri_wp_sidebar_gen(); ?>
    </div><!-- End wrap -->
    <?php
}




/* Global setting variables */
$sucuri_notify_options = array(
    'sucuri_user_register_alert' => 'Enable new user registration alerts',
    'sucuri_wp_login_alert' => 'Enable successful logins alerts',
    'sucuri_wp_login_failed_alert' => 'Enable failed logins alerts',
    'sucuri_publish_post_alert' => 'Enable new post published alerts',
    'sucuri_wp_theme_editor_alert' => 'Enable when any file is modified via the editor alerts',
    'sucuri_wp_plugin_alert' => 'Enable plugin changes alerts',
    'sucuri_notify_website_updated' => 'Enable email notifications when your website is updated',
    'sucuri_notify_settings_updated' => 'Enable email notifications when your website settings are updated',
    'sucuri_notify_theme_switched' => 'Enable email notifications when the website theme is switched',
    'sucuri_notify_plugin_activated' => 'Enable email notifications when a plugin is activated',
    'sucuri_notify_plugin_deactivated' => 'Enable email notifications when a plugin is deactivated',
    'sucuri_notify_plugin_updated' => 'Enable email notifications when a plugin is updated',
    'sucuri_notify_plugin_installed' => 'Enable email notifications when a plugin is installed',
    'sucuri_notify_plugin_deleted' => 'Enable email notifications when a plugin is deleted',
    'sucuri_wp_prettify_mails' => 'Enable HTML notifications (uncheck if you want to receive notifications in text plain)',
    'sucuri_lastlogin_redirection' => 'Allow redirection after login to report the last-login information (uncheck if you have custom redirection rules)',
);
$sucuri_schedule_allowed = array(
    /* 'hourly' => 'Once hourly (1 hour)', */
    'twicedaily' => 'Twice daily (12 hours)',
    'daily' => 'Once daily (24 hours)',
    /* '_oneoff' => 'Never', */
);

/* sucuri_modify_settings:
 * Process Post requests from the settings page.
 */
function sucuri_modify_settings()
{
    global $sucuri_schedule_allowed;

    if(!isset($_POST['wpsucuri-modify-values']))
    {
        return(NULL);
    }

    if(!wp_verify_nonce($_POST['sucuri_wpsettingsnonce'], 'sucuri_wpsettingsnonce'))
    {
        return(NULL);
    }


    //sucuri_alerts("sucuri_wp_plugin_alert", "Plugin settings changed", "Main plugin settings changed.");

    /* Use HTTP instead of HTTPS (Just if you don't want to use SSL) */
    if( isset($_POST['sucuri_force_http']) ){
        update_option('sucuri_wp_force_http', $_POST['sucuri_force_http']==1 ? 'enabled' : 'disabled' );
    }else{
        update_option('sucuri_wp_force_http', 'disabled' );
    }

    /* Enable or disable automatic updates */
    if( isset($_POST['sucuri_autoupdate']) ){
        update_option('sucuri_wp_autoupdate', $_POST['sucuri_autoupdate']==1 ? 'enabled' : 'disabled' );
    }else{
        update_option('sucuri_wp_autoupdate', 'disabled' );
    }

    /* Modify the schedule of the filesystem scanner */
    if( isset($_POST['sucuri_scan_frequency']) ){
        $frequency = $_POST['sucuri_scan_frequency'];
        $current_frequency = get_option('sucuri_scan_frequency');
        $allowed_frequency = array_keys($sucuri_schedule_allowed);
        if( in_array($frequency, $allowed_frequency) && $current_frequency!=$frequency ){
            update_option('sucuri_scan_frequency', $frequency);
            wp_clear_scheduled_hook('sucuri_scheduled_scan');
            if( $frequency!='_oneoff' ){
                wp_schedule_event(time()+10, $frequency, 'sucuri_scheduled_scan');
            }
            sucuri_admin_notice('updated', 'Sucuri filesystem scan schedule changed to <strong>'.$frequency.'</strong>.');
        }
    }

    /* Modify the memory limit that our Plugin can consume */
    if( isset($_POST['sucuri_memory_limit']) && preg_match('/^([0-9]+)$/', $_POST['sucuri_memory_limit']) ){
        $max_memory_limit = 1024 * 10;
        $memory_limit = intval($_POST['sucuri_memory_limit']);
        if( $memory_limit<=$max_memory_limit ){
            update_option('sucuri_memory_limit', $memory_limit);
        }else{
            sucuri_admin_notice('error', 'You should reconsider your choice, <code>'.$memory_limit.'M</code> is too much memory and maybe you will not need it.');
        }
    }

    /* Make sure all our files are there. */
    sucuri_plugin_activation();
    if(!is_dir(sucuri_dir_filepath('/blocks')))
    {
        sucuri_admin_notice('error', '<strong>Error.</strong> Unable to activate without permissions to modify: <code>'.sucuri_dir_filepath('blocks').'</code>');
        return FALSE;
    }
    if(!is_file(sucuri_dir_filepath('blocks/blocks.php')))
    {
        sucuri_admin_notice('error', '<strong>Error.</strong> Unable to activate without permissions to modify: <code>'.sucuri_dir_filepath().'</code>');
        return FALSE;
    }


    /* White listing a few IP addresses by default. */
    if(isset($_SERVER['SUCURI_RIP']))
    {
        @touch(sucuri_dir_filepath('whitelist/'.$_SERVER['SUCURI_RIP']));
    }
    @touch(sucuri_dir_filepath('whitelist/'.$_SERVER["REMOTE_ADDR"]));
    @touch(sucuri_dir_filepath('whitelist/'.$_SERVER['SERVER_ADDR']));


    /* Enabling or disabling remediation. */
    if(isset($_POST['sucuri_activeresponses']))
    {
        update_option('sucuri_wp_re', 'enabled');
    }
    else
    {
        update_option('sucuri_wp_re', 'disabled');
    }

    /* Create/Update sucuri_wp_bs */
    if(isset($_POST['sucuri_wp_bs']) && (( $_POST['sucuri_wp_bs'] >= 7 ) && ( $_POST['sucuri_wp_bs'] <= 30 )) )
    {
        update_option('sucuri_wp_bs', htmlspecialchars(trim((int)$_POST['sucuri_wp_bs'])));
    }

    /* Handling unsafe (user) content */
    if(isset($_POST['wpsucuri-newkey']))
    {
        $newkey = htmlspecialchars(trim($_POST['wpsucuri-newkey']));
        if(preg_match("/^[a-zA-Z0-9]+$/", $newkey, $regs, PREG_OFFSET_CAPTURE,0))
        {
            $res = sucuri_send_log($newkey, "INFO: Authentication key added and plugin enabled.");
            /* RES = 1 , key accepted. */
            if($res == 1)
            {
                update_option('sucuri_wp_key', $newkey);
                update_option('sucuri_valid_api_key', 1);

                sucuri_debug_log("Activating key $newkey..");
                if(!wp_next_scheduled( 'sucuri_scheduled_scan'))
                {
                    sucuri_debug_log("Activating wp_schedule event..");
                    wp_schedule_event(time() + 10, 'twicedaily', 'sucuri_scheduled_scan');
                }

                wp_schedule_single_event(time()+300, 'sucuri_scheduled_scan');
                sucuri_admin_notice('updated', 'The <strong>Sucuri API Key</strong> was accepted and all the main settings were saved');
                return TRUE;
            }
            else if($res == -1)
            {
                sucuri_admin_notice('error', '<strong>Error.</strong> Unable to connect to '.SUCURI_REMOTE_URL.' (check for Curl + SSL support on PHP).');
            }
            else
            {
                sucuri_admin_notice('error', 'The <strong>Sucuri API Key</strong> provided was not accepted by our system: <code>'.$newkey.'</code>');
            }
        }
        else
        {
            sucuri_admin_notice('error', 'The <strong>Sucuri API Key</strong> provided is not valid: <code>'.$newkey.'</code>');
        }
    }

    return FALSE;
}

function sucuri_modify_alerts_settings()
{
    global $sucuri_notify_options;

    if(!isset($_POST['wpsucuri-alerts-modify-values']))
    {
        return(NULL);
    }

    if(!wp_verify_nonce($_POST['sucuri_wpemailalertsnonce'], 'sucuri_wpemailalertsnonce'))
    {
        return(NULL);
    }

    /* Create/Update sucuri_wp_email */
    if(isset($_POST['sucuri_wp_email']))
    {
        $sucuri_wp_email = $_POST['sucuri_wp_email'];
        if( sucuri_is_email($sucuri_wp_email) )
        {
            update_option('sucuri_wp_email', $sucuri_wp_email);
        }
        else
        {
            sucuri_admin_notice('error', 'Plugin changes aborted. Email address provided is invalid.');
            return FALSE;
        }
    }

    if( isset($sucuri_notify_options) ){
        foreach( (array)$sucuri_notify_options as $option_name=>$option_label ){
            if( isset($_POST[$option_name]) ){
                $option_value = $_POST[$option_name]==1 ? 'enabled' : 'disabled';
                update_option($option_name, $option_value);
            }
        }
    }

    sucuri_alerts("sucuri_wp_plugin_alert", "Plugin settings changed", "Email alerts settings changed.");
}



/* sucuri_settings_page:
 * Sucuri main setting's page.
 */
function sucuri_settings_page()
{
    global $sucuri_notify_options, $sucuri_schedule_allowed;

    if(!current_user_can('manage_options'))
    {
        wp_die(__('You do not have sufficient permissions to access this page: Sucuri Settings') );
    }

    /* If the scan was not done recently, try now. */
    if(!isset($_SESSION['sucuri_scan_just_done']))
    {
        $_SESSION['sucuri_scan_just_done'] = true;
        sucuri_do_scan();
    }

    /* Check if the scheduled task is installed */
    if( !wp_next_scheduled( 'sucuri_scheduled_scan') ){
        sucuri_admin_notice('error', '<strong>Error.</strong> A scheduled task is missing in the scan process that
            monitors your site. To install this task click the <strong>Save values</strong> button in the panel
            bellow with the title <strong>Main settings</strong>.');
    }

    // Process POST requests to update main/generic settings.
    sucuri_modify_settings();
    sucuri_modify_alerts_settings();
    $sucuri_settings = sucuri_get_options();

    /* If remediation (ip blocking is enabled). */
    $remediation = get_option('sucuri_wp_re');
    if($remediation !== FALSE && $remediation == 'disabled')
    {
        $remediation = '';
    }
    else
    {
        $remediation = ' checked="checked"';
    }

    /* Use HTTP instead of HTTPS (Just if you don't want to use SSL) */
    $sucuri_wp_force_http = get_option('sucuri_wp_force_http');
    $sucuri_wp_force_http_checked = $sucuri_wp_force_http=='enabled' ? 'checked="checked"' : '';

    /* Disable automatic updates. Check this if you want to manually update this plugin. */
    $sucuri_wp_autoupdate_checked = sucuri_autoupdate_enabled() ? '' : 'checked="checked"';

    /* Get sucuri_wp_bs (if IPs in block older then sucuri_wp_bs they will be cleared) */
    $sucuri_wp_bs = get_option('sucuri_wp_bs');
    $sucuri_wp_bs_7 = $sucuri_wp_bs_15 = $sucuri_wp_bs_30 = '';
    if($sucuri_wp_bs !== FALSE)
    {
        if($sucuri_wp_bs == 7) { $sucuri_wp_bs_7 = "selected";  }
        if($sucuri_wp_bs == 15){ $sucuri_wp_bs_15 = "selected"; }
        if($sucuri_wp_bs == 30){ $sucuri_wp_bs_30 = "selected"; }
    }
    ?>

    <div class="wrap">
        <h2 id="warnings_hook"></h2>
        <div class="sucuriscan_header">
            <a href="http://sucuri.net/signup" target="_blank" title="Sucuri Security">
                <img src="<?php echo SUCURI_PLUGIN_URL; ?>/inc/images/logo.png" alt="Sucuri Security" />
            </a>
            <h2>Sucuri Security WordPress Plugin (Settings)</h2>
        </div>

        <div class="postbox-container" style="width:75%;">
            <div class="sucuriscan-maincontent">
                <?php if( !$sucuri_settings['sucuri_wp_key'] ): ?>
                    <div id="poststuff">
                        <div class="postbox">
                            <h3>Plugin not activated</h3>
                            <div class="inside">
                                Your plugin is not configured yet. Please login to <a target="_blank" href="https://wordpress.sucuri.net">https://wordpress.sucuri.net</a>
                                to get your authentication (API) key.<br /><br />Note, this plugin is only for Sucuri users. If you do not have an account, please
                                sign up here: <a href="http://sucuri.net/signup">http://sucuri.net/signup</a>.
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php sucuri_checkfor_updates(); ?>

                <div id="poststuff">
                    <div class="postbox">
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        <h3>Main settings</h3>
                        <div class="inside sucuri-main-settings">
                            <form method="post">
                                <input type="hidden" name="sucuri_wpsettingsnonce" value="<?php echo wp_create_nonce('sucuri_wpsettingsnonce'); ?>" />
                                <input type="hidden" name="wpsucuri-modify-values" value="wpsucuri-modify-values" />

                                <table class="form-table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <span>SUCURI API KEY (<i>get it from <a target="_blank" href="https://wordpress.sucuri.net">here</a></i>):</span>
                                                    <input type="text" name="wpsucuri-newkey" value="<?php echo $sucuri_settings['sucuri_wp_key']; ?>" class="regular-text" />
                                                </label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div>
                                                    <label>
                                                        <input type="checkbox" checked="checked" disabled="disabled" />
                                                        Enable integrity monitoring for your files.
                                                    </label>
                                                </div>

                                                <div>
                                                    <label>
                                                        <input type="checkbox" checked="checked" disabled="disabled" />
                                                        Enable audit trails and internal logs.
                                                    </label>
                                                </div>

                                                <div>
                                                    <label>
                                                        <input type="checkbox" name="sucuri_activeresponses" value="1" <?php echo $remediation;?> />
                                                        Enable active response (to block suspicious IP addresses and attacks)
                                                    </label>
                                                </div>

                                                <div>
                                                    <label>
                                                        <input type="hidden" name="sucuri_force_http" value="0" />
                                                        <input type="checkbox" name="sucuri_force_http" value="1" <?php echo $sucuri_wp_force_http_checked; ?> />
                                                        Use HTTP instead of HTTPS (check if you don't want to use SSL)
                                                    </label>
                                                </div>

                                                <div>
                                                    <label>
                                                        <input type="hidden" name="sucuri_autoupdate" value="1" />
                                                        <input type="checkbox" name="sucuri_autoupdate" value="0" <?php echo $sucuri_wp_autoupdate_checked; ?> />
                                                        Disable automatic updates (you'll need to apply updates manually through this page).
                                                    </label>
                                                </div>

                                                <div class="sucuri-settings-flush-blocklist">
                                                    <select name="sucuri_wp_bs">
                                                        <option value="7" <?php echo $sucuri_wp_bs_7;?>>7 days</option>
                                                        <option value="15" <?php echo $sucuri_wp_bs_15;?>>15 days</option>
                                                        <option value="30" <?php echo $sucuri_wp_bs_30;?>>30 days</option>
                                                    </select>
                                                    How often should we clear IP Addresses in block list?
                                                </div>

                                                <div class="sucuri-settings-scan-frequency">
                                                    <label>
                                                        <select name="sucuri_scan_frequency">
                                                            <?php foreach( (array)$sucuri_schedule_allowed as $schedule=>$schedule_label ): ?>
                                                                <?php $selected = $sucuri_settings['sucuri_scan_frequency']==$schedule ? 'selected="selected"' : ''; ?>
                                                                <option <?php echo $selected; ?> value="<?php echo $schedule; ?>"><?php echo $schedule_label; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        How often should we scan your website?
                                                    </label>
                                                </div>

                                                <div class="sucuri-settings-memory-limit">
                                                    <label>
                                                        <input type="text" name="sucuri_memory_limit" value="<?php echo sucuri_memory_limit(TRUE); ?>" />
                                                        <span>Memory limit (in megabytes) for our filesystem scanner (default 2048M).</span>
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr><td><input class="button-primary" type="submit" name="wpsucuri_domodify" value="Save values"></td></tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>

                    <div class="postbox">
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        <h3>Email alerts settings</h3>
                        <div class="inside">
                            <form method="post">
                                <input type="hidden" name="sucuri_wpemailalertsnonce" value="<?php echo wp_create_nonce('sucuri_wpemailalertsnonce'); ?>" />
                                <input type="hidden" name="wpsucuri-alerts-modify-values" value="wpsucuri-alerts-modify-values" />

                                <table class="form-table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    Send alerts to this email:
                                                    <input type="text" name="sucuri_wp_email" value="<?php echo $sucuri_settings['sucuri_wp_email']; ?>" class="regular-text" placeholder="username@example.com" />
                                                </label>
                                            </td>
                                        </tr>

                                        <?php if( isset($sucuri_notify_options) ): ?>
                                            <tr>
                                                <td>
                                                    <?php foreach( (array)$sucuri_notify_options as $alert_type=>$alert_label ): ?>
                                                        <?php
                                                        if( !isset($sucuri_settings[$alert_type]) || $sucuri_settings[$alert_type]=='enabled' ){
                                                            $alert_checked = 'checked="checked"';
                                                        }else{
                                                            $alert_checked = '';
                                                        }
                                                        ?>
                                                        <div>
                                                            <label>
                                                                <input type="hidden" name="<?php echo $alert_type; ?>" value="0" />
                                                                <input type="checkbox" name="<?php echo $alert_type; ?>" value="1" <?php echo $alert_checked; ?> />
                                                                <?php echo $alert_label; ?>
                                                            </label>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <tr><td><input class="button-primary" type="submit" name="wpsucuri_email_alerts_domodify" value="Save values"></td></tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div><!-- End postbox -->
                </div><!-- End poststuff -->

            </div><!-- End sucuriscan-maincontent -->
        </div><!-- End postbox-container -->

        <?php echo sucuri_wp_sidebar_gen(); ?>

    </div><!-- End wrap -->
<?php
}



/* sucuri_about_page:
 * Sucuri about' page.
 */
function sucuri_about_page()
{
    if( !current_user_can('manage_options') )
    {
        wp_die(__('You do not have sufficient permissions to access this page: Sucuri Last-Logins') );
    }

    // Page pseudo-variables initialization.
    $template_variables = array(
        'SucuriURL'=>SUCURI_PLUGIN_URL,
        'SucuriWPSidebar'=>sucuri_wp_sidebar_gen(),
        'CurrentURL'=>site_url().'/wp-admin/admin.php?page='.$_GET['page'],
        'SettingsDisplay'=>'hidden'
    );

    $template_variables = sucuri_about_information($template_variables);
    $template_variables = sucuri_show_cronjobs($template_variables);

    echo sucuri_get_template('about.html.tpl', $template_variables);
}

function sucuri_about_information($template_variables=array())
{
    global $wpdb;

    if( current_user_can('manage_options') ){
        $memory_usage = function_exists('memory_get_usage') ? round(memory_get_usage()/1024/1024,2).' MB' : 'N/A';
        $mysql_version = $wpdb->get_var('SELECT VERSION() AS version');
        $mysql_info = $wpdb->get_results('SHOW VARIABLES LIKE "sql_mode"');
        $sql_mode = ( is_array($mysql_info) && !empty($mysql_info[0]->Value) ) ? $mysql_info[0]->Value : 'Not set';
        $plugin_runtime_filepath = sucuri_dir_filepath('.runtime');
        $plugin_runtime_datetime = file_exists($plugin_runtime_filepath) ? date('r',filemtime($plugin_runtime_filepath)) : 'N/A';

        $template_variables = array_merge($template_variables, array(
            'SettingsDisplay'=>'block',
            'PluginVersion'=>SUCURI_VERSION,
            'PluginForceUpdate'=>admin_url('admin.php?page=sucurisec_settings&sucuri_force_update=1'),
            'PluginMD5'=>md5_file(SUCURI_PLUGIN_FILEPATH),
            'PluginRuntimeDatetime'=>$plugin_runtime_datetime,
            'OperatingSystem'=>sprintf('%s (%d Bit)', PHP_OS, PHP_INT_SIZE*8),
            'Server'=>isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : 'Unknown',
            'MemoryUsage'=>$memory_usage,
            'MySQLVersion'=>$mysql_version,
            'SQLMode'=>$sql_mode,
            'PHPVersion'=>PHP_VERSION,
        ));

        foreach(array(
            'safe_mode',
            'allow_url_fopen',
            'memory_limit',
            'upload_max_filesize',
            'post_max_size',
            'max_execution_time',
            'max_input_time',
        ) as $php_flag){
            $php_flag_name = ucwords(str_replace('_', chr(32), $php_flag) );
            $tpl_varname = str_replace(chr(32), '', $php_flag_name);
            $php_flag_value = ini_get($php_flag);
            $template_variables[$tpl_varname] = $php_flag_value ? $php_flag_value : 'N/A';
        }
    }

    return $template_variables;
}

function sucuri_show_cronjobs($template_variables=array())
{
    $template_variables['Cronjobs'] = '';

    $cronjobs = _get_cron_array();
    $schedules = wp_get_schedules();
    $date_format = _x('M j, Y - H:i', 'Publish box date format', 'cron-view' );

    foreach( $cronjobs as $timestamp=>$cronhooks ){
        foreach( (array)$cronhooks as $hook=>$events ){
            foreach( (array)$events as $key=>$event ){
                $cronjob_snippet = '';
                $template_variables['Cronjobs'] .= sucuri_get_template('about-cronjobs.snippet.tpl', array(
                    'Cronjob.Task'=>ucwords(str_replace('_',chr(32),$hook)),
                    'Cronjob.Schedule'=>$event['schedule'],
                    'Cronjob.Nexttime'=>date_i18n($date_format, $timestamp),
                    'Cronjob.Hook'=>$hook,
                    'Cronjob.Arguments'=>implode(', ', $event['args'])
                ));
            }
        }
    }

    return $template_variables;
}
/* Requires files. */

/* Getting correct remote ip  */
if(isset($_SERVER['SUCURI_RIP']))
{
    unset($_SERVER['SUCURI_RIP']);
}

/* For Cloudflare. */
if(isset($_SERVER['HTTP_CF_CONNECTING_IP']))
{
    $_SERVER['SUCURI_RIP'] = trim($_SERVER['HTTP_CF_CONNECTING_IP']);
}
/* More gateway requests. */
else if(isset($_SERVER['HTTP_X_ORIG_CLIENT_IP']))
{
    $_SERVER['SUCURI_RIP'] = $_SERVER['HTTP_X_ORIG_CLIENT_IP'];
}
/* Proxy requests. */
else if(isset($_SERVER['HTTP_TRUE_CLIENT_IP']))
{
    $_SERVER['SUCURI_RIP'] = $_SERVER['HTTP_TRUE_CLIENT_IP'];
}
else if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) &&
   $_SERVER["HTTP_X_FORWARDED_FOR"] != $_SERVER['REMOTE_ADDR'] &&
   preg_match("/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/", $_SERVER["HTTP_X_FORWARDED_FOR"]))
{
    $_SERVER['SUCURI_RIP'] = trim($_SERVER["HTTP_X_FORWARDED_FOR"], "a..zA..Z%/. \t\n");
    $_SERVER['SUCURI_RIP'] = trim($_SERVER['SUCURI_RIP']);
}
else if(preg_match("/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/", $_SERVER["REMOTE_ADDR"]))
{
    $_SERVER['SUCURI_RIP'] = $_SERVER['REMOTE_ADDR'];
}
$_SERVER['SUCURI_RIP'] = basename($_SERVER['SUCURI_RIP']);

if(!preg_match("/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/", $_SERVER["SUCURI_RIP"]))
{
    unset($_SERVER['SUCURI_RIP']);
}

/* Inspect every post request */
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_SERVER['SUCURI_RIP']))
{
    add_action('init', 'sucuri_process_prepost');
}

if( function_exists('is_multisite') && is_multisite() )
{
    if(is_network_admin())
    {

        add_action('network_admin_menu', 'sucuri_wpmenu');
        add_action('admin_init', 'sucuri_events_without_actions');
        add_action('login_form', 'sucuri_events_without_actions');
    }
    else if(is_admin())
    {
        add_action('admin_init', 'sucuri_events_without_actions');
        add_action('login_form', 'sucuri_events_without_actions');
    }
}

/* Admin specific actions. */
else if(is_admin())
{


    add_action('admin_menu', 'sucuri_wpmenu');
    add_action('admin_init', 'sucuri_events_without_actions');
    add_action('login_form', 'sucuri_events_without_actions');
}

/* Activation / Deactivation actions. */
register_activation_hook(__FILE__, 'sucuri_plugin_activation');
register_deactivation_hook(__FILE__, 'sucuri_plugin_deactivation');

/* Hooks our scanner function to the hourly cron. */
add_action('sucuri_scheduled_scan', 'sucuri_do_scan');

/* Removing generator */
remove_action('wp_head', 'wp_generator');

/* EOF */
