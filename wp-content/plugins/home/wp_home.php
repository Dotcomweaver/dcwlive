<?php

/*
Plugin Name: Home
Description: custom home
Author: Devi Dash
Version: 1
Author URI: http://www.dotcomweavers.com
*/


add_action( 'init', 'home_register' );
function home_register() {
	register_post_type( 'home',
		array(
			'labels' => array(
				'name' => __( 'Home' ),
				'singular_name' => __( 'Home' ),
				'add_new' => _x('Add New', 'Home'),
				'add_new_item' => __('Add New Home'),
				'edit_item' => __('Edit Home'),
				'new_item' => __('New Home'),
				'view_item' => __('View Home'),
				'search_items' => __('Search Home'),
				'not_found' =>  __('Nothing found'),
				'not_found_in_trash' => __('Nothing found in Trash'),
				'parent_item_colon' => ''
			),
		'public' => true,
		'publicly_queryable' => true,
		'menu_icon' => plugins_url('video-testimonials') . '/images/icon-case-studies-sml.png',
		'capability_type' => 'post',
		'hierarchical' => false,
		'has_archive' => false,
		'supports' => array('title','editor', 'thumbnail')
		)
	);
}

/*
add_filter('page_template', 'home_page_template' );
function home_page_template( $page_template ){
    if (is_page('home')){
    	//enqueue_cs_styles('site');
    	//enqueue_scripts_cs('site');
    	$page_template = dirname( __FILE__ ) . '/page-home.php';
    }
    return $page_template;
}
*/

add_filter( "single_template", "home_single_template" ) ;
function home_single_template($single_template) {
     global $post;

     if ($post->post_type == 'home') {
     	//enqueue_cs_styles('site');
        //enqueue_scripts_cs('site');
     	$single_template = dirname( __FILE__ ) . '/page-home_single.php';
     }

     //add_filter('nav_menu_css_class', 'current_type_cs_nav_class', 10, 2 );

     return $single_template;
}

function home_page_code_box($post)
{
	$home_post_meta = json_decode(get_post_meta($post->ID,'home_post_meta', true));
	$home_code = $home_post_meta->home_code;
?>
  <table>
	<tr>
		<td><label for="home_code">Page Code:</label></td>
	</tr>
	<tr>
		<td><input type="text" name="home_code" id="home_code" value="<?php echo (($home_code)?$home_code:"");?>" /></td>
	</tr>

	<tr>
		<td><label for="upload_image_button">Page Image:</label><input type="button" id="upload_image_button" value="Select a File" /></td>
	</tr>
  </table>

<?php
}

add_action('admin_init','home_setup_meta_boxes');
function home_setup_meta_boxes() {

    // Add the box to a particular custom content type page
    add_meta_box('home_code_box', 'Home Attributes', 'home_page_code_box', 'home', 'normal', 'high');

}

add_action('save_post','home_update_post',1,2);
function home_update_post($post_id, $post) {

    // Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
    // It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
    // post type in the passed object isn't "revision"
    $post_type = $post->post_type;

    // Make sure our flag is in there, otherwise it's an autosave and we should bail.
    if($post_id) {

        // Logic to handle specific post types
        switch($post_type) {

            // If this is a post. You can change this case to reflect your custom post slug
            case 'home':
//print_r($_POST);exit(0);
				$home_post_meta = array();

				$home_code = strip_tags($_POST['home_code']);
				$home_post_meta['home_code'] = $home_code;

				$home_post_meta = json_encode($home_post_meta);
				update_post_meta($post_id,'home_post_meta', $home_post_meta);

            break;
            default:


        } // End switch

    return;

	} // End if manual save flag
    return;

}
?>