<?php

    // calling the header.php
    get_header();

?>
<div class="container">
	<div class="sixteen columns wide_wrapper clearfix">
<!-- Start the Loop. -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

	$post_id = $post->ID;
	$home_post_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));

?>

  <h2 style="margin-top:15px;color:#F79521;"><?php the_title(); ?></h2>
  <hr />

  <div style="float:right;"><img src="<?php echo $home_post_feat_image; ?>" alt="" /></div>
  <div class="heading_descp"><?php the_content(); ?></div>
  <div class="clear"></div>

  <!-- Stop The Loop (but note the "else:" - see next line). -->
  <?php endwhile; else: ?>

    <!-- The very first "if" tested to see if there were any Posts to -->
    <!-- display.  This "else" part tells what do if there weren't any. -->
    <p>Sorry, no posts matched your criteria.</p>

<!-- REALLY stop The Loop. -->
<?php endif; ?>

	</div>
</div><!-- container -->

<?php
    	// calling footer.php
    	get_footer();
?>

