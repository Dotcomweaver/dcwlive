<?php
    // calling the header.php
    get_header();
?>
<div class="container">
	<div class="sixteen columns wide_wrapper clearfix">

	            <?php

		            query_posts('post_type=dcw-home');

					//Start the Loop.
					if ( have_posts() ) : while ( have_posts() ) : the_post();

						$post_id = $post->ID;

				?>

					  <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>


				 <!-- Stop The Loop (but note the "else:" - see next line). -->
				 <?php endwhile; else: ?>

				 <!-- The very first "if" tested to see if there were any Posts to -->
				 <!-- display.  This "else" part tells what do if there weren't any. -->
				 <p>Sorry, no posts matched your criteria.</p>

				 <!-- REALLY stop The Loop. -->
				 <?php endif; ?>

  </div>
</div>
<div class="clear" style="clear:both"></div>
<?php

	// calling the sidebar.php
	//get_sidebar();

    // calling footer.php
    get_footer();

?>
