<?php

    // calling the header.php
    get_header();

?>
<div class="row">	
<div class="container">   
<div class="wide_wrapper pad_aply">	
<div class="sixteen columns">	
<!-- Start the Loop. -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

$post_id = $post->ID;
$home_post_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));

?>
<div class="row inner_heading">	
<!--<h2 style="margin-top:15px;color:#F79521;"><?php //the_title(); ?></h2>-->
<h1 style="font-size: 28px; line-height: 30px; display: inline-block;"><?php the_title(); ?></h1>
</div>
<!--<hr />-->
<!--<div style="float:right;"><img src="<?php //echo $home_post_feat_image; ?>" alt="" /></div>-->
<div class="clearfix h15"></div>
<div class="row">
<div class="four columns" style="margin: 0 0 0 10px;">
<span class="pic"><img src="<?php echo $home_post_feat_image; ?>"  alt="" />
<div class="img_overlay"></div>
</span>	<!--pic-->
<div class="clearfix h15"></div>
</div>	<!--four columns-->
<div class="twelve columns">
<div class="heading_descp" style="padding:0 0 0 10px; text-align: justify;"><?php the_content(); ?></div>
</div>	<!--twelve columns-->
</div>	<!--row-->
<div class="clear"></div>

<!-- Stop The Loop (but note the "else:" - see next line). -->
<?php endwhile; else: ?>

<!-- The very first "if" tested to see if there were any Posts to -->
<!-- display.  This "else" part tells what do if there weren't any. -->
<p>Sorry, no posts matched your criteria.</p>

<!-- REALLY stop The Loop. -->
<?php endif; ?>
</div>	<!--sixteen columns-->
<div class="clearfix"></div>
</div>	<!--wide_wrapper pad_aply-->
</div>	<!--container-->
</div>	<!--row-->

<?php
    	// calling footer.php
    	get_footer();
?>

