<?php
/*
Plugin Name: ACF for AMP
Plugin URI: https://ampforwp.com/acf-amp/
Description: Advanced Custom Feilds Support for AMP
Version: 2.5
Author: Mohammed Kaludi
Author URI: https://ampforwp.com/
Donate link: https://www.paypal.me/Kaludi/25
License: GPL2
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'ACF_FOR_AMP_VERSION' ) ) {
    define( 'ACF_FOR_AMP_VERSION', '2.5' );
}
// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'ACF_FOR_AMP_STORE_URL', 'https://accounts.ampforwp.com/' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'ACF_FOR_AMP_ITEM_NAME', 'ACF for AMP' );

// the download ID. This is the ID of your product in EDD and should match the download ID visible in your Downloads list (see example below)
//define( 'AMPFORWP_ITEM_ID', 2502 );
// the name of the settings page for the license input to be displayed
define( 'ACF_FOR_AMP_LICENSE_PAGE', 'acf-for-amp-license' );

if(! defined('AMP_ACF_ITEM_FOLDER_NAME')){
    $folderName = basename(__DIR__);
    define( 'AMP_ACF_ITEM_FOLDER_NAME', $folderName );
}


// Include Meta Select metabox file 
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/metabox.php' );



add_action( 'init', 'amp_acf_create_post_type' );
function amp_acf_create_post_type() {
  register_post_type( 'amp_acf',
    array(
      'labels' => array(
          'name'          => esc_attr__( 'AMP ACF', 'amp-acf' ),
          'singular_name' => esc_attr__( 'AMP ACF', 'amp-acf' )
      ),
        'public'                => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'supports'              => array('title')
    )
  );
}

function remove_post_custom_fields() {
  remove_meta_box( 'review_overall_rating' , 'amp_acf' , 'side' ); 
  remove_meta_box( 'ampforwp_title_meta' , 'amp_acf' , 'side' ); 
  remove_meta_box( 'wpseo_meta' , 'amp_acf' , 'normal' ); 
}
add_action( 'do_meta_boxes' , 'remove_post_custom_fields' );



add_action( 'admin_enqueue_scripts', 'amp_acf_style_script_include' );
 
function amp_acf_style_script_include( $hook ) {
    // Admin Style
      wp_enqueue_style('amp-acf-admin', plugin_dir_url(__FILE__) . 'assets/css/admin-style.css');
 
      wp_enqueue_script( 'ace_code_highlighter_js', plugin_dir_url(__FILE__) . 'editor/ace.js', '', '1.0.0', true );
      wp_enqueue_script( 'ace_mode_js', plugin_dir_url(__FILE__) . 'editor/worker-php.js', array( 'ace_code_highlighter_js' ), '1.0.0', true );
      wp_enqueue_script( 'custom_css_js', plugin_dir_url(__FILE__) . 'editor/custom-css.js', array( 'jquery', 'ace_code_highlighter_js' ), '1.0.0', true );
      wp_enqueue_script( 'field-creator', plugin_dir_url(__FILE__) . 'assets/js/field-creator.js', array( 'jquery'), '1.0.0', true );

    // Register the script
    wp_register_script( 'amp_acf_field', plugin_dir_url(__FILE__) . 'assets/js/field-creator.js');

    // Localize the script with new data
    $data_array = array(
        'ajax_url'    =>  admin_url( 'admin-ajax.php' ) 
    );
    wp_localize_script( 'amp_acf_field', 'amp_acf_field_data', $data_array );

    // Enqueued script with localized data.
    wp_enqueue_script( 'amp_acf_field' );
}


// Including files
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/ajax-select-generator.php' ); 
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/amp-logic.php' ); 
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/debug.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/editor-meta.php' );


// Generate Proper post types for select and to add data.
add_action('wp_loaded', 'amp_acf_post_type_generator');
 
function amp_acf_post_type_generator(){

    $post_types = '';
    $post_types = get_post_types( array( 'public' => true ), 'names' );

    // Remove Unsupported Post types
    unset($post_types['attachment'], $post_types['amp_acf']);

    return $post_types;
}



// Generate Proper Post Taxonomy for select and to add data.
function amp_acf_post_taxonomy_generator(){
    $taxonomies = '';  
    $choices    = '';
      $taxonomies = get_taxonomies( array('public' => true), 'objects' );

      foreach($taxonomies as $taxonomy) {
        $choices[ $taxonomy->name ] = $taxonomy->labels->name;
      }

      // unset post_format (why is this a public taxonomy?)
      if( isset($choices['post_format']) ) {
        unset( $choices['post_format']) ;
      }

    return $choices;
}


// Code inserter in proper postition
add_action('pre_amp_render_post','amp_acf_code_position_handler');
function amp_acf_code_position_handler(){
    //Get All POSTs id by post_type
    $post_idArray = amp_acf_get_all_post_id();
    if(count($post_idArray)>0){
      foreach ($post_idArray as $key => $post_id) {
        $position_selector  = get_post_meta( $post_id, 'position_selector', true);  
        $position_hook      = get_post_meta( $post_id, 'position_hook', true);  
        if ( $position_selector === 'single') {
            $position = is_singular();
        }
        if ( $position_selector === 'global') {
            $position = true;
        }
        if ( $position_selector === 'homepage') {
            $position = is_home() || is_front_page();
        }
        if ( $position_selector === 'archive') {
            $position = is_archive();
        }

         if(ampforwp_is_front_page()){
          $position = ampforwp_is_front_page();
        }

          if($position){
        add_action('amp_post_template_head',function() use ($post_id){
          $amp_scripts = get_post_meta($post_id, 'amp_scripts', true);
           echo $amp_scripts;
         });
      }

        if ( $position && $position_hook) {
              
              add_action( $position_hook ,function() use ($post_id){
                run_the_code_V2($post_id);

              });
           
        }
      }
    }
        
  // position_selector
}
//Advance Custom Field Pro Compatibility using acf/validate_post_id filter
add_filter('acf/validate_post_id','ampforwp_modify_post_id');
function ampforwp_modify_post_id($post_id){
  if ( ampforwp_is_front_page()){
    global $redux_builder_amp;
    $post_id =$redux_builder_amp['amp-frontpage-select-option-pages'];
  }
    return $post_id;
}



// Version 2 code to insert ACF data
function run_the_code_V2($post_id){
      $data           = '';
      $unique_checker = '';

      $data = generate_the_field_data($post_id);

      $number_of_fields = count($data);

        // if there is only 1 Checker set, then get the value and directly send to the final checker
        // and process that data
        if ( $number_of_fields == 1 ) {
          $unique_checker = $data[0];
        }
        // Check if we have more then 1 fields.
        if ( $number_of_fields > 1 ) {
          // Check if all the arrays have TRUE setup, then send the value 1, if all the 
          // values are same.
          $unique_checker = count( array_unique($data) );
          // Check and make sure only all TRUE values only passed on, if all values are FALSE,
          // then making sure all FALSE are converting to 0, and returing false.
          // Code will not run.
          $array_is_false =  in_array(false, $data);
          if (  $array_is_false ) {
            $unique_checker = 0;
          }
        }
        if ( $unique_checker === 1 || $unique_checker === true) {
          echo get_php_editor_data($post_id);
        }
  }


function generate_the_field_data($post_id){
    $conditions = get_post_meta( $post_id, 'data_in_array', true);  
    $output = array();

    $output = amp_acf_field_checker($conditions);
    return $output;
}

/* 
 * amp_acf_field_checker function takes all logics selected in dropdown in Field Generator,
 * and checks it with amp_acf_comparison_logic_checker function and generates
 * proper true or false value, depending on the user and post.
 * 
*/
function amp_acf_field_checker($conditions){
    $output = array();
    if ( $conditions ) { 
      $output = array_map('amp_acf_comparison_logic_checker', $conditions); 
      /*foreach ($conditions as $key => $value) {
         if ( amp_acf_comparison_logic_checker($value) ) {
            $output[] = true;
         } else {
            $output[] = false;
         }
      }*/
    }
    return $output;
}

function amp_acf_get_all_post_id() {
  $query = new WP_Query(array(
        'post_type' => 'amp_acf',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    ));
    while ($query->have_posts()) {
        $query->the_post();
        $post_id[] = get_the_ID();
    }
    wp_reset_query();
    wp_reset_postdata();
    return $post_id;
}
require_once dirname( __FILE__ ) . '/updater/EDD_SL_Plugin_Updater.php';

// Check for updates
function amp_acf_plugin_updater() {

    // retrieve our license key from the DB
    //$license_key = trim( get_option( 'amp_ads_license_key' ) );
    $selectedOption = get_option('redux_builder_amp',true);
    $license_key = '';//trim( get_option( 'amp_ads_license_key' ) );
    $pluginItemName = '';
    $pluginItemStoreUrl = '';
    $pluginstatus = '';
    if( isset($selectedOption['amp-license']) && "" != $selectedOption['amp-license'] && isset($selectedOption['amp-license'][AMP_ACF_ITEM_FOLDER_NAME])){

       $pluginsDetail = $selectedOption['amp-license'][AMP_ACF_ITEM_FOLDER_NAME];
       $license_key = $pluginsDetail['license'];
       $pluginItemName = $pluginsDetail['item_name'];
       $pluginItemStoreUrl = $pluginsDetail['store_url'];
       $pluginstatus = $pluginsDetail['status'];
    }
    
    // setup the updater
    $edd_updater = new ACF_FOR_AMP_EDD_SL_Plugin_Updater( ACF_FOR_AMP_STORE_URL, __FILE__, array(
            'version'   => ACF_FOR_AMP_VERSION,                // current version number
            'license'   => $license_key,                        // license key (used get_option above to retrieve from DB)
            'license_status'=>$pluginstatus,
            'item_name' => ACF_FOR_AMP_ITEM_NAME,          // name of this plugin
            'author'    => 'Mohammed Kaludi',                   // author of this plugin
            'beta'      => false,
        )
    );
}
add_action( 'admin_init', 'amp_acf_plugin_updater', 0 );

// Notice to enter license key once activate the plugin

$path = plugin_basename( __FILE__ );
    add_action("after_plugin_row_{$path}", function( $plugin_file, $plugin_data, $status ) {
        global $redux_builder_amp;
         if(! defined('AMP_ACF_ITEM_FOLDER_NAME')){
        
           $folderName = basename(__DIR__);
           define( 'AMP_ACF_ITEM_FOLDER_NAME', $folderName );
        }
        $pluginsDetail = $redux_builder_amp['amp-license'][AMP_ACF_ITEM_FOLDER_NAME];
        $pluginstatus = $pluginsDetail['status'];

        if(empty($redux_builder_amp['amp-license'][AMP_ACF_ITEM_FOLDER_NAME]['license'])){
            echo "<tr class='active'><td>&nbsp;</td><td colspan='2'><a href='".esc_url(  self_admin_url( 'admin.php?page=amp_options&tabid=opt-go-premium' )  )."'>Please enter the license key</a> to get the <strong>latest features</strong> and <strong>stable updates</strong></td></tr>";
                 }elseif($pluginstatus=="valid"){
                $update_cache = get_site_transient( 'update_plugins' );
            $update_cache = is_object( $update_cache ) ? $update_cache : new stdClass();
            if(isset($update_cache->response[ AMP_ACF_ITEM_FOLDER_NAME ]) 
                && empty($update_cache->response[ AMP_ACF_ITEM_FOLDER_NAME ]->download_link) 
              ){
               unset($update_cache->response[ AMP_ACF_ITEM_FOLDER_NAME ]);
            }
            set_site_transient( 'update_plugins', $update_cache );
            
        }
    }, 10, 3 );