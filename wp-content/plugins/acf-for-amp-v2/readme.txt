=== ACF for AMP ===
Contributors: Mohammed Kaludi
Tags: acf, amp, advanced custom field, plugin
Requires at least: 3.0
Tested up to: 4.9.5
Stable tag: 2.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

ACF for AMP enables you to display Advanced Custom fields data into the AMP version of your website.

== Description ==
ACF for AMP enables you to display Advanced Custom fields data into the AMP version of your website.

== Changelog ==

= 2.5 (23 June 2018) =
*  Console error fixed.

= 2.4 (20 April 2018) =
*  Upgrade Constants

= 2.3 (18 April 2018) =
*  Change some 'Update Proces' Code for better enhancement.

= 2.2 (16 April 2018) =
* Activation improvements

= 2.1 (06 April 2018) =
* Frontpage was not working with post id, Fixed.
* External Javascript issue Fixed
* Better Update Process Integrated
