<?php
/*
 *	Select Meta Boxes:
 *	- This file contains all the select meta boxes in the AMP-ACF post type
 *	- 1. Show Data on?
 *	- 2. Hook Selector
 *	- 3. Repeater metaboxes.
 *	All the data is in this file from registration to save.
*/

add_action( 'add_meta_boxes', 'amp_acf_create_meta_box_select' );

function amp_acf_create_meta_box_select() {
	
	// First Section of Meta Box
	add_meta_box( 'amp_acf_show_data', __( 'Location','amp-acf' ), 'amp_acf_show_data_callback', 'amp_acf','normal', 'high' );

	// Repeater Comparison Field
	add_meta_box( 'amp_acf_select', __( 'Placement','amp-acf' ), 'amp_acf_select_callback', 'amp_acf','normal', 'high' );
}

function amp_acf_show_data_callback($post){
	$pt_type 			= esc_attr( get_post_meta($post->ID, 'post_types_setting', true) );
	$current_location 	= esc_attr( get_post_meta($post->ID, 'position_selector', true) );
	$hook_to_insert 	= esc_attr( get_post_meta($post->ID, 'position_hook', true) );?>

	<div id="current_post_type_id" class="screen-reader-text"> <?php echo $pt_type; ?></div>
	<?php

		//security check
		wp_nonce_field( 'amp_acf_show_data_action_nonce', 'amp_acf_show_data_name_nonce' );

		/* Positioning */
		$amp_acf_location = array(
			'single' 	=> esc_attr__( 'Show Only on Post Pages' ,'amp-acf'),
			'global'	=> esc_attr__( 'Show Globaly ','amp-acf'),
			'homepage'	=> esc_attr__( 'Show Only on Homepage','amp-acf'),
			'archive'	=> esc_attr__( 'Show only on Archive Pages','amp-acf') 
			);?>
		<div style="display: inline-block; float: left; width:49%">
			<label for="position-selector" class="widefat"> <b> <?php esc_attr_e( 'Show Data on' ,'amp-acf');?> </b> </label>
			<select name="position_selector" id="position-selector" class="widefat">
				<?php foreach ($amp_acf_location as $key => $value) { ?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $current_location, $key );?> ><?php echo esc_attr( $value ); ?></option>
				<?php 
				} ?>			
			</select>
		</div>  
	<?php
		/* Hook */
		$hook_name = array(
			'ampforwp_after_header' 				=> 'After the Header (Global)' ,
			'ampforwp_post_before_design_elements'	=> 'Before Post (Single)',


			'ampforwp_post_before_design_elements'	=> 'Before Featured Image (Single)',
			'ampforwp_after_featured_image_hook'	=> 'After Featured Image (Single)',
			'ampforwp_after_meta_info_hook'			=> 'After Post Meta (Single)',
			'ampforwp_before_meta_taxonomy_hook'	=> 'Before Post Meta (Single) ',
			'ampforwp_before_post_content'			=> 'Before Post Content (Single) ',
			'ampforwp_after_post_content'			=> 'After Post Content (Single)',


			'ampforwp_post_after_design_elements'	=> 'Below the Post (Single)',
			'amp_post_template_above_footer'		=> 'Above the Footer (Sitewide)',
			'amp_post_template_footer'				=> 'In the Footer (Sitewide)'

			);?>
		<div style="display: inline-block;  float: right; width:49%">
			<label for="position-hook" class="widefat"> <b> <?php esc_attr_e( 'Where you want to show the content?' ,'amp-acf');?> </b> </label>
			<select name="position_hook" id="position-hook" class="widefat">
				<?php foreach ($hook_name as $key => $value) { ?>
					<option value="<?php echo $key ?>" <?php selected( $hook_to_insert, $key );?> ><?php echo $value ?></option>
				<?php 
				} ?>				
			</select> 
		</div>
		<div class="clear"></div>
	<?php 
}

function amp_acf_select_callback($post) {
	$data_in_array 		= esc_sql ( get_post_meta($post->ID, 'data_in_array', true)  );

	if ( empty( $data_in_array ) ) {
		$data_in_array = array(
			array(
			'key_1' => 'post_type',
			'key_2' => 'not_equal',
			'key_3' => 'none',
			)
		);
	}
	//security check
	wp_nonce_field( 'amp_acf_select_action_nonce', 'amp_acf_select_name_nonce' );?>

	<?php 
	// Type Select		
		$choices = array(
			__("Basic",'amp-acf') => array(
			//	'none'			=>	__(" -- Select --",'amp-acf'),
				'post_type'		=>	__("Post Type",'amp-acf'),
				'user_type'		=>	__("Logged in User Type",'amp-acf'),
			),
			__("Post",'amp-acf') => array(
				'post'			=>	__("Post",'amp-acf'),
				'post_category'	=>	__("Post Category",'amp-acf'),
				'post_format'	=>	__("Post Format",'amp-acf'), 
			),
			__("Page",'amp-acf') => array(
				'page'			=>	__("Page",'amp-acf'), 
				'page_template'	=>	__("Page Template",'amp-acf'),
			),
			__("Other",'amp-acf') => array( 
				'ef_taxonomy'	=>	__("Taxonomy Term",'amp-acf'), 
			)
		); 

		$comparison = array(
			'equal'		=>	esc_attr__( 'Equal to', 'amp-acf'), 
			'not_equal'	=>	esc_attr__( 'Not Equal to', 'amp-acf'),			
		);

		$total_fields = count( $data_in_array ); ?>

		<table class="widefat">
			<tbody id="repeater-tbody" class="fields-wrapper-1">
			<?php  for ($i=0; $i < $total_fields; $i++) {  
				$selected_val_key_1 = $data_in_array[$i]['key_1']; 
				$selected_val_key_2 = $data_in_array[$i]['key_2']; 
				$selected_val_key_3 = $data_in_array[$i]['key_3'];
				?>
				<tr class="toclone">
					<td style="width:31%" class="post_types"> 
						<select class="widefat <?php echo esc_attr( $i );?>" name="data_in_array[<?php echo $i?>][key_1]">		
							<?php 
							foreach ($choices as $choice_key => $choice_value) { ?>					
								<option disabled class="pt-heading" value="<?php echo $choice_key;?>"> <?php echo esc_attr( $choice_key );?> </option>
								<?php
								foreach ($choice_value as $sub_key => $sub_value) { ?> 
									<option class="pt-child" value="<?php echo esc_attr( $sub_key );?>" <?php selected( $selected_val_key_1, $sub_key );?> > <?php echo esc_attr( $sub_value );?> </option>
									<?php
								}
							} ?>
						</select>
					</td>
					<td style="width:31%">
						<select class="widefat comparison" name="data_in_array[<?php echo $i?>][key_2]"> <?php
							foreach ($comparison as $key => $value) { ?>
								<option class="pt-child" value="<?php echo esc_attr( $key );?>" <?php selected( $selected_val_key_2, 1 );?> > <?php echo esc_attr( $value );?> </option>
								<?php
							} ?>
						</select>
					</td>
					<td style="width:31%">
						<div class="insert-ajax-select">							
							<?php amp_acf_ajax_select_creator($selected_val_key_1, $selected_val_key_3, $i );?>
							<div class="spinner"></div>
						</div>
					</td>

					<td class="widefat clone" style="width:3.5%">
					<span> <button> <?php esc_attr_e( 'Add' ,'amp-acf');?> </button> </span> </td>
					
					<td class="widefat delete" style="width:3.5%">
					<span> <button> <?php esc_attr_e( 'Remove' ,'amp-acf');?> </button> </span> </td>					
				</tr>
				<?php 
			} ?>
			</tbody>
		</table>
	<?php
}

//save value entered into the custom metabox
add_action('save_post', 'amp_acf_select_save_data');
function amp_acf_select_save_data($id) {

  	// Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['amp_acf_select_name_nonce'] ) || !wp_verify_nonce( $_POST['amp_acf_select_name_nonce'], 'amp_acf_select_action_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

	$post_data_array = $_POST['data_in_array'];

	// Data
	if(isset($_POST['data_in_array'])){
		update_post_meta(
			$id, 
			'data_in_array', 
			$post_data_array 
		);
	}
}


add_action('save_post', 'amp_acf_show_data_save_data');
function amp_acf_show_data_save_data($id) {

  // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['amp_acf_show_data_name_nonce'] ) || !wp_verify_nonce( $_POST['amp_acf_show_data_name_nonce'], 'amp_acf_show_data_action_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

	// Position selector
	if(isset($_POST['position_selector'])){
		update_post_meta( 
			$id, 
			'position_selector', 
			strip_tags($_POST['position_selector']) 
		);
	}

	// Position Hook
	if(isset($_POST['position_hook'])){
		update_post_meta( 
			$id, 
			'position_hook', 
			strip_tags($_POST['position_hook']) 
		);
	}

	// Current Post Type
	if(isset($_POST['post_types_setting'])){
		update_post_meta( 
			$id, 
			'post_types_setting', 
			strip_tags($_POST['post_types_setting']) 
		);
	}

}