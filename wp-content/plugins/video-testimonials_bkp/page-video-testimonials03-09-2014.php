<?php
    // calling the header.php
    get_header();
	// add wp thickbox
	add_thickbox();
?>
<!--Testimonails carousel Scripts Starts here-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.vtflexisel.js"></script>
<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
//$("#flexiselDemo1").flexisel();
$("#eCommerce").flexisel({
	enableResponsiveBreakpoints: true,
	responsiveBreakpoints: { 
		portrait: { 
			changePoint:480,
			visibleItems: 1
		}, 
		landscape: { 
			changePoint:640,
			visibleItems: 2
		},
		tablet: { 
			changePoint:768,
			visibleItems: 3
		}
	}
});
$("#Web-Design").flexisel({
	enableResponsiveBreakpoints: true,
	responsiveBreakpoints: { 
		portrait: { 
			changePoint:480,
			visibleItems: 1
		}, 
		landscape: { 
			changePoint:640,
			visibleItems: 2
		},
		tablet: { 
			changePoint:768,
			visibleItems: 3
		}
	}
});
$("#Web-Applications").flexisel({
	enableResponsiveBreakpoints: true,
	responsiveBreakpoints: { 
		portrait: { 
			changePoint:480,
			visibleItems: 1
		}, 
		landscape: { 
			changePoint:640,
			visibleItems: 2
		},
		tablet: { 
			changePoint:768,
			visibleItems: 3
		}
	}
});
});
</script>
<style type="text/css">
.popup{position:fixed; background: url("../images/fancybox_overlay.png") repeat scroll 0 0 transparent; width:100%; height:100%; z-index:9999999; left:0;}
.youtube_ifram{background:#F9F9F9; width:40%; height:60%; margin:auto; position:relative; top:18%; padding:10px; border-radius:5px; bottom:0;}
.popup_close{background:url("../images/fancybox_sprite.png") no-repeat; cursor: pointer;height: 36px;position: absolute;right: -18px;top: -18px;width: 36px;}
</style>
<!--Testimonails carousel Scripts Ends here-->
<div class="row" id="innerpage_bg">
<div id="clrbg">
  <div class="container">
    <?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>
    <?php //get_sidebar(); ?>
    <div class="wide_wrapper pad_aply fullwrapper">
      <div class="row inner_heading">
        <section id="primary" class="vt-pad">
          <h1 class="page-title">Video Testimonials
          <div class="fr wid_135"> <a class="org_smal" href="http://www.dotcomweavers.com/view-video-testimonials"><span style="line-height: 22px;">View All</span></a> </div>
          </h1>
          <div id="content">
            <div>
              <?php while ( have_posts() ) : the_post(); ?>
              <?php echo the_content(); ?>
              <?php endwhile; // end of the loop. ?>
            </div>
            <div class="clearfix h15"></div>  </div> <!-- #content --> 
            </section><!-- #primary --> 
        
<?php
// To get the video category tags names from dcw_taxanomy
	$taxonomies = 'category_tags';
	$args = array(
		'orderby'       => 'slug', 
		'order'         => 'ASC',
		'hide_empty'    => true, 
		'exclude'       => array(), 
		'exclude_tree'  => array(), 
		'include'       => array(),
		'fields'        => 'all', 
		'hierarchical'  => true, 
		'child_of'      => 0, 
		'pad_counts'    => false, 
		'cache_domain'  => 'core'
	); 
	$terms = get_terms($taxonomies,$args);  
	//print_r($terms);
	// to get the video testimonials from dcw-posts table
	if(!empty($terms)): 
	foreach($terms as $category_name)
	{
	?>
    <div class="row clearfix">	<!---- Row Starts from here ---->
    <div class="row vt_bg">
        <h2><?php echo str_replace('-',' ',$category_name->name); ?></h2>
        <div class="vtbtm-bg"></div>
     </div>  <!--testimonails-->
     <div class="container max-width">
        <div class="sixteen columns">	
        <ul id="<?php echo $category_name->name; ?>"> 
    	<?php 
		query_posts( array( 'post_type' => 'video-testimonials', 'posts_per_page' => -1 ) );
		if ( have_posts() ) : while ( have_posts() ) : the_post();
		$post_id = $post->ID;
		$title = $post->title;
		$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
		if($v_test_post_meta->vt_categories!=''): $i = 0; $cat_id=''; 
		 foreach($v_test_post_meta->vt_categories as $category_id){   $cat_id[$i] = $category_id; $i++; }
		 if(in_array($category_name->term_id,$cat_id)): 
		 $y_img = get_ytube_video_code($v_test_post_meta->vt_url);
		$te = $v_test_post_meta->vt_url;
		$start = strpos($te,"v=")+2;
		$end = strpos($te,"&");
		if( $end == "" || $end == 0 ){
			$fstr = substr($te,$start);
		} else {
			$length = $end-$start;
			$fstr = substr($te,$start,$length);
		}
		?>
        <!--ecommerce testimoanils starts here-->
        <li><a href="javascript:void(0)" rel="http://www.youtube.com/embed/<?php echo $fstr; ?>" class="show_video"><img src="/wp-content/themes/twentyeleven/images/play120x90.png" style="background:url('http://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg') no-repeat scroll center center; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" alt="<?php the_title() ?> - Testimonial for NJ Web Design company" /></a><div class="vt-shadow"></div></li>   
          <?php endif; endif; endwhile; // end of the loop. ?>
             </ul>
        </div>
        </div>	<!--testmonials_bg-->
        </div>	<!--row clearfix-->
        <!--ecommerce testimoanils ends here-->
            <!-- Stop The Loop (but note the "else:" - see next line). -->
            <?php else: ?>
            <p>Sorry, no posts matched your criteria.</p>
             <?php endif; } endif; ?>
            <?php
	wp_reset_query();
?>
            <!--<div class="clearfix h20" style="height:100px;"></div>-->
         
         
          
      </div>
      <!--.row inner_heading--> 
      <div class="fr wid_135" style="margin-right:20px;"> <a href="http://www.dotcomweavers.com/view-video-testimonials" class="org_smal"><span style="line-height: 22px;">View All</span></a> </div>
    <div class="clear"></div>
    </div> <!--.wide_wrapper-->
      
    <div id="toTop"></div>
  </div>
  <!--.container--> 
  </div>
  <!-- clrbg -->
</div>
<!--.row-->
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>
<script type="text/javascript">
$('a.show_video').click(function(){
var yvideo = $(this).attr('rel');
$("#inner_video_div").html("<a href='javascript:;' class='popup_close' id='close_btn' title='Close'></a><iframe src="+yvideo+" width='100%' height='100%' autoplay='true'></iframe>");
$("#video_div").show();
init();
});
$('#video_div').click(function(){
  $('#video_div').hide();
  });
function init(){
$('#close_btn').click(function(){
$("#inner_video_div").html('');
$("#video_div").hide()
});
}
</script>