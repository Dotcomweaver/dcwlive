<?php

/*
Plugin Name: Video Testimonials
Description: Display custom case studies items in a list with an interactive filter menu
Author: Devi Dash
Version: 1.2.1
Author URI: http://www.dotcomweavers.com
*/

add_action('init', 'v_testimonials_register');
//register case studies on admin menu
function v_testimonials_register() {

	$labels = array(
		'name' => _x('Video Testimonials', 'post type general name'),
		'singular_name' => _x('Video Testimonials', 'post type singular name'),
		'add_new' => _x('Add New', 'Video Testimonials'),
		'add_new_item' => __('Add New Video Testimonials'),
		'edit_item' => __('Edit Video Testimonials'),
		'new_item' => __('New Video Testimonials'),
		'view_item' => __('View Video Testimonials'),
		'search_items' => __('Search Video Testimonials'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => plugins_url('video-testimonials') . '/images/icon-case-studies-sml.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		/*'taxonomies' => array('post_tag','category'),*/
		'supports' => array('title','editor','thumbnail')
	  );

	register_post_type( 'video-testimonials' , $args );
}
/**
 * Add custom taxonomies*/
function add_custom_vtestimonials_taxonomies() {
	// Add new "Locations" taxonomy to Posts
	register_taxonomy('category_tags', 'video-testimonials', array(
		// Hierarchical taxonomy (like categories)
		'hierarchical' => true,
		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Category Tags', 'taxonomy general name' ),
			'singular_name' => _x( 'Category Tags', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Category Tags' ),
			'all_items' => __( 'All Category Tags' ),
			'parent_item' => __( 'Parent Category Tag' ),
			'parent_item_colon' => __( 'Parent Category Tag:' ),
			'edit_item' => __( 'Edit Category Tag' ),
			'update_item' => __( 'Update Category Tag' ),
			'add_new_item' => __( 'Add New Category Tag' ),
			'new_item_name' => __( 'New Category Tag' ),
			'menu_name' => __( 'Category Tags' ),
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'category-tags', // This controls the base slug that will display before each term
			'with_front' => false, // Don't display the category base before "/locations/"
			'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
		),
	));
}
add_action( 'init', 'add_custom_vtestimonials_taxonomies', 0 );


add_action('post_edit_form_tag', 'v_testimonials_add_edit_form_multipart_encoding');
//open form multipart for file upload
function v_testimonials_add_edit_form_multipart_encoding() {
    echo ' enctype="multipart/form-data"';
}

function v_testimonials_render_image_attachment_box($post) {

    // See if there's an existing image. (We're associating images with posts by saving the image's 'attachment id' as a post meta value)
    // Incidentally, this is also how you'd find any uploaded files for display on the frontend.

    $v_testimonials_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
    $vt_url = $v_testimonials_post_meta->vt_url;
	$client_name = $v_testimonials_post_meta->client_name;
	$text_testimonial = $v_testimonials_post_meta->text_testimonial;
    /*$vt_is_home = $v_testimonials_post_meta->vt_is_home;*/
    ?>
    <table>
    	<tr>
    		<td><label for="vt_url">Youtube URL:</label></td>
    	</tr>
    	<tr>
    		<td><input type="text" name="vt_url" size="68" id="vt_url" value="<?php echo (($vt_url)?$vt_url:"");?>"/></td>
    	</tr>
        <tr>
    		<td><label for="vt_url">Clinet Name:</label></td>
    	</tr>
    	<tr>
    		<td><input type="text" name="client_name" size="68" id="client_name" value="<?php echo (($client_name)?$client_name:"");?>"/></td>
    	</tr>
        <tr>
    		<td><label for="vt_url">Text Description:</label></td>
    	</tr>
    	<tr>
    		<td><textarea name="text_testimonial" id="text_testimonial" rows="7" cols="65"><?php echo (($text_testimonial)?$text_testimonial:"");?></textarea></td>
    	</tr>
    </table>

   <?php
    // Put in a hidden flag. This helps differentiate between manual saves and auto-saves (in auto-saves, the file wouldn't be passed).
    echo '<input type="hidden" name="manual_save_flag" value="true" />';
}

add_action('admin_init','vtestimonials_setup_meta_boxes');
function vtestimonials_setup_meta_boxes() {

    // Add the box to a particular custom content type page
    add_meta_box('v_testimonials_image_box', 'Video Testimonials Attributes', 'v_testimonials_render_image_attachment_box', 'video-testimonials', 'normal', 'high');

}

add_action('save_post','v_testimonials_update_post',1,2);
function v_testimonials_update_post($post_id, $post) {

    // Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
    // It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
    // post type in the passed object isn't "revision"
    $post_type = $post->post_type;
	
    // Make sure our flag is in there, otherwise it's an autosave and we should bail.
    if($post_id && isset($_POST['manual_save_flag'])) {

        // Logic to handle specific post types
        switch($post_type) {

            // If this is a post. You can change this case to reflect your custom post slug
            case 'video-testimonials':

					$v_testimonials_post_meta = array();

					$vt_url = strip_tags($_POST['vt_url']);
					$v_testimonials_post_meta['vt_url'] = $vt_url;
					
					$client_name = strip_tags($_POST['client_name']);
					$v_testimonials_post_meta['client_name'] = $client_name;
					
					$text_testimonial = strip_tags($_POST['text_testimonial']);
					$v_testimonials_post_meta['text_testimonial'] = $text_testimonial;
					
					/*$vt_is_home = $_POST['vt_is_home'];
					$v_testimonials_post_meta['vt_is_home'] = $vt_is_home;*/
					
					$vt_categories = $_POST['tax_input']['category_tags'];
					$v_testimonials_post_meta['vt_categories'] = $vt_categories;
					
                	$v_testimonials_post_meta = json_encode($v_testimonials_post_meta);
                	update_post_meta($post_id,'v_testimonials_post_meta', $v_testimonials_post_meta);

            break;
            default:


        } // End switch

    return;

} // End if manual save flag

    return;

}

add_action('admin_print_scripts', 'upload_vtestimonials_admin_scripts');
add_action('admin_print_styles', 'upload_vtestimonials_admin_styles');
function upload_vtestimonials_admin_scripts() {
	enqueue_scripts_vtestimonials('admin');
}

function upload_vtestimonials_admin_styles() {
	enqueue_vtestimonials_styles('admin');
}

//add_action("manage_posts_custom_column",  "case_studies_custom_columns");
add_filter("manage_edit-v_testimonials_columns", "case_studies_edit_columns");
function vtestimonials_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "thumb" => "Video Testimonials Image",
    "title" => "Name",
    "published" => "Active",
    "description" => "Description",
    "url" => "URL"
  );

  return $columns;
}

function vtestimonials_custom_columns($column){
  global $post;

  switch ($column) {
    case "description":
      the_excerpt();
      break;
    case "thumb":

		echo '<div>';
		the_post_thumbnail('thumbnail');
		echo '</div>';

    break;
    case "url":
      	$v_testimonials_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta',true));
      	echo "<a href='".$v_testimonials_meta->v_testimonials_url."' target='_blank'>".$v_testimonials_meta->v_testimonials_url."</a>";
    break;
    case "published":
      	if($post->post_status == 'publish'){
      		echo "Yes";
      	}else{
      		echo "No";
      	}
    break;
  }
}

function generate_vtestimonials_Select($name = '', $options = array(), $selected = '') {
	$html = '<select name="'.$name.'">';
	$select = str_replace("_"," ",$selected);
	foreach ($options as $option => $value) {
		$html .= '<option value='.$value.' '.(($selected == $value)?'selected="selected"':'').'>'.$option.'</option>';
	}
	$html .= '</select>';
	return $html;
}

function get_attachment_id_from_src_vtestimonials($image_src){
	global $wpdb;

	$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
	$id = $wpdb->get_var($query);
	return $id;
}

add_filter('sanitize_file_name', 'case_studies_filename_hash', 10);
function vtestimonials_filename_hash($filename) {
    $v_testimonials_encrypt_images = get_option('case_studies_encrypt_images');
    if($v_testimonials_encrypt_images){
    	$info = pathinfo($filename);
    	$ext  = empty($info['extension']) ? '' : '.' . $info['extension'];
    	$name = basename($filename, $ext);
    	return md5($name) . $ext;
    }else{
    	return $filename;
    }
}

function vtestimonials_admin(){
	global $pagenow;
	enqueue_vtestimonials_styles('admin');
}

add_action('admin_menu', 'case_studies_admin_actions');
function vtestimonials_admin_actions() {
	//add_submenu_page("edit.php?post_type=video-testimonials", "Video Testimonials Settings", "Settings", 1, "video-testimonials-settings","video-testimonials_admin");
}

add_filter('page_template', 'vtestimonials_page_template' );
function vtestimonials_page_template( $page_template ){
    if (is_page('video-testimonials')){
    	enqueue_vtestimonials_styles('site');
    	enqueue_scripts_vtestimonials('site');
    	$page_template = dirname( __FILE__ ) . '/page-video-testimonials.php';
    }
    return $page_template;
}

add_filter( "single_template", "vtestimonials_single_template" ) ;
function vtestimonials_single_template($single_template) {
     global $post;

     if ($post->post_type == 'video-testimonials') {
     	enqueue_vtestimonials_styles('site');
        enqueue_scripts_vtestimonials('site');
     	$single_template = dirname( __FILE__ ) . '/page-video-testimonials_single.php';
     }

     add_filter('nav_menu_css_class', 'current_type_vtestimonials_nav_class', 10, 2 );

     return $single_template;
}

function enqueue_scripts_vtestimonials($type=''){

		/*
	if($type == 'admin'){
		// enqueue the script
		wp_enqueue_script('jquery-ui-core');

		wp_enqueue_script('jquery-ui-datepicker');

		// enqueue the script
		wp_enqueue_script('media-upload');
		// enqueue the script
		wp_enqueue_script('thickbox');

		wp_register_script('my-upload', plugins_url('video-testimonials/').'/js/media_upload.js', array('jquery','jquery-ui-core','media-upload','thickbox'));
		// enqueue the script
		wp_enqueue_script('my-upload');

	}elseif($type == 'site'){
		wp_enqueue_script('jquery-ui-core');

		// register your script location, dependencies and version
		wp_register_script('custom-ajax',plugins_url('video-testimonials/') . '/js/ajax.js',array('jquery','jquery-ui-core'));
	   // enqueue the script
	   wp_enqueue_script('custom-ajax');

	   wp_register_script('nivo-slider',plugins_url('video-testimonials/') . '/js/jquery.nivo.slider.pack.js',array('jquery'));
	   // enqueue the script
	   wp_enqueue_script('nivo-slider');

	   wp_register_script('easing',plugins_url('video-testimonials/') . '/js/jquery.easing.js',array('jquery','jquery-ui-core'));
	   // enqueue the script
	   wp_enqueue_script('easing');

	   wp_register_script('quicksand',plugins_url('video-testimonials/') . '/js/jquery.quicksand.js',array('jquery','jquery-ui-core'));
	   // enqueue the script
	   wp_enqueue_script('quicksand');
   	}
   */
}

function enqueue_vtestimonials_styles($type = ''){

/*
	if($type == 'admin'){

		$jquery_datpicker_css = plugins_url('', __FILE__).'/css/jquery.ui.datepicker.css'; // Respects SSL, Style.css is relative to the current file
		$jquery_datpicker_file = dirname(__FILE__) . '/css/jquery.ui.datepicker.css';
		if ( file_exists($jquery_datpicker_file) ) {
			wp_register_style('jquery_datpicker', $jquery_datpicker_css);
			wp_enqueue_style( 'jquery_datpicker');
		}

		$jquery_base_css = plugins_url('', __FILE__).'/css/jquery.ui.all.css'; // Respects SSL, Style.css is relative to the current file
		$jquery_base_file = dirname(__FILE__) . '/css/jquery.ui.all.css';
		if ( file_exists($jquery_base_file) ) {
			wp_register_style('jquery-base-css', $jquery_base_css);
			wp_enqueue_style( 'jquery-base-css');
		}

		wp_enqueue_style('thickbox');

	}elseif($type == 'site'){

		$nivo_style_css = plugins_url('', __FILE__).'/themes/nivo/nivo.css'; // Respects SSL, Style.css is relative to the current file
		$nivo_style_file = dirname(__FILE__) . '/themes/nivo/nivo.css';
		if ( file_exists($nivo_style_file) ) {
			wp_register_style('nivo-theme', $nivo_style_css);
			wp_enqueue_style( 'nivo-theme');
		}

	}

	$portfolio_style_css = plugins_url('', __FILE__).'/css/portfolio_style.css'; // Respects SSL, Style.css is relative to the current file
	$portfolio_style_file = dirname(__FILE__) . '/css/portfolio_style.css';
	if ( file_exists($portfolio_style_file) ) {
		wp_register_style('portfolioStyle', $portfolio_style_css);
		wp_enqueue_style( 'portfolioStyle');
	}
*/
}

function current_type_vtestimonials_nav_class($classes, $item) {
    //$post_type = get_post_type();
    //print_r($classes);
    //print_r($item);
    if ($item->title != '' && $item->title == 'Video Testimonials') {
        //array_push($classes, 'current_page_item');
    };
    return $classes;
}

/*
function vtestimonials_admin_tabs( $current = 'homepage' ) {
    $tabs = array( 'homepage' => 'Home', 'image_settings' => 'Image Settings', 'layout' => 'Layout','filter' => 'Filter'); //'tags' => 'Tags', removed custom tags Tab in settings
    $links = array();
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?post_type=video-testimonials&page=portfolio-settings&tab=$tab'>$name</a>";
    }
    echo '</h2>';
}
*/