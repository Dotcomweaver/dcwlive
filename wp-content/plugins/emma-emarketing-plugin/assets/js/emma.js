jQuery(document).ready(function($) {
    $('#emma-form input#emma-form-submit').on("click", function(e) {
    	

        e.preventDefault();
        e.stopPropagation();
        var thisForm = $(e.target).closest('#emma-subscription-form');
        var thisWrap = $(e.target).closest('.emma-wrap');
        var thisFormUnique = thisForm.attr('data-form-unique');
        thisForm.addClass('activeForm');
        if ($('.emma-status').length > 0) {
            $('.emma-status').fadeOut({
                duration: 300,
                queue: false,
                complete: function() {
                    $('.emma-status').remove();
                    
                }
            });
        }
        thisForm.fadeOut({
            duration: 300,
            queue: false,
            complete: function() {
            var spin = $('.spinner').length;
    		//console.log("spinner is" + spin);
    	  	if (spin > 0) {
              $('<div class="spinner"></div>').prependTo(thisWrap).hide();
        		}else{
        	 		$('<div class="spinner"></div>').prependTo(thisWrap).show();
        		}
                var data = {
                    'action': 'emma_ajax_form_submit',
                    'emma_email': $('#emma-subscription-form[data-form-unique="' + thisFormUnique + '"] input[name="emma_email"]').val(),
                    'emma_firstname': $('#emma-subscription-form[data-form-unique="' + thisFormUnique + '"] input[name="emma_firstname"]').val(),
                    'emma_lastname': $('#emma-subscription-form[data-form-unique="' + thisFormUnique + '"] input[name="emma_lastname"]').val(),
                    'emma_signup_form_id': $('#emma-subscription-form[data-form-unique="' + thisFormUnique + '"] input[name="emma_signup_form_id"]').val(),
                    'emma_send_confirmation': $('#emma-subscription-form[data-form-unique="' + thisFormUnique + '"] input[name="emma_send_confirmation"]').val(),
                };
                jQuery.post(ajax_object.ajax_url, data, function(response) {
                    var errorClass = '';
                    var hasError = false;
                    response = $.parseJSON(response);
                    if (response.code > 800) {
                        errorClass = 'emma-alert';
                        hasError = true;
                        response.tracking_pixel = '<p style="display:none !important;">Error occured. No tracking pixel placed.</p>';
			            if ($('.emma-status').length > 0) {
			            $('.emma-status').fadeOut({
			                duration: 300,
			                queue: false,
			                complete: function() {
			                    $('.emma-status').remove();
			                    //$("#emma-subscription-form").removeAttr("style");
			                    $("#emma-subscription-form").css("opacity", "");
			                }
			            });
			        }
                    } else {
                        errorClass = '';
                        if ($('.emma-status').length > 0) {
			            $('.emma-status').fadeOut({
			                duration: 300,
			                queue: false,
			                complete: function() {
			                    $('.emma-status').remove();
			                    $("#emma-subscription-form").css("opacity", "");
			                }
			            });
			        }
                    }
                    thisWrap.prepend('<div class="emma-status ' + errorClass + '" style="display:none;">' + response.status_txt + '</div>' + response.tracking_pixel);
                    $('.spinner').delay(800).fadeOut(300, function() {			             			           			       
                        $('.spinner').remove();
                        $('.emma-status').fadeIn(300);
                        if (hasError == true) {
                            thisForm.fadeIn(300);
                        }
                    });
                });
            }
        });
    });
});