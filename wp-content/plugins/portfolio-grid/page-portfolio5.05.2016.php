<?php
/*
  Template Name:work
 */
?>

<?php get_header();
?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1> <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
                                <p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_work.png"></div>
<section id="portfolio-dis">
    <div class="container1">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Wrapper for slides -->
		<div class="carousel-inner">  
          	<?php 
			   $query = "SELECT $wpdb->posts.*,$wpdb->postmeta.* 
					FROM $wpdb->posts, $wpdb->postmeta
					WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
					AND $wpdb->postmeta.meta_key = 'portfolio_post_meta' 
					AND $wpdb->posts.post_status = 'publish' 
					AND $wpdb->posts.post_type = 'portfolio'
					ORDER BY $wpdb->posts.post_date DESC";
					
				$pageposts = $wpdb->get_results($query, OBJECT);
				$query = new WP_Query( array( 'post_type' => 'portfolio','posts_per_page' => 1  ) ); 
				
				if ( $pageposts ) 
				{ 
					$i = 1;
					foreach ( $pageposts as $post )
					{ 
						
						setup_postdata($post);
						$post_id = $post->ID;
						$active_id = $post->ID;
						$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
						
						//echo $post_id;
						$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
						//print_r($portfolio_post_meta);
						if($portfolio_post_meta->ourwork_is_home == 'Y')
						{
			  				if($i == '1'){
								$class = 'active';
							}else{
								$class = "";
							}
			  ?>
        
		                 
        <div class="item <?php echo $class; ?>">
                               
                                    <div class="col-lg-6 col-sm-6 col-xs-12 port-banner">
                                      
                                            <?php 
                                                $portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_home_image));
                                                foreach( $portfolio_image_id_arr as $attach_img_id ) :
                                                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
                                            ?>
                                        <span><a href="<?php the_permalink();?>"><img src="<?php echo $attach_folio_img[0]; ?>" class="img-responsive" alt=""></a></span>
                                            
                                            <?php break; ?>
                                            <?php endforeach; ?>
                                       
                                    </div>
                                     <div class="col-lg-5 col-sm-6 col-xs-12">
                                         
                                         <h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
                                         <p><?php //the_content();?></p>
                                         <p><?php echo wp_trim_words(($post->post_content), 250);?> .....</p>
                                        <p class="more"> <a href="<?php the_permalink();?>">Want to Learn More?</a></p>
                                       
                                    </div>
                                   
                              
                           </div><!-- item active -->
                           <div class="controllers col-lg-12 col-xs-12">
	<!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="fa fa-angle-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="fa fa-angle-right"></span>
	  </a>
	  
	</div>
			  <?php 
			  			$i++;
			  			}
					
					}
				}
			   wp_reset_postdata();
			?>
    </div>
        
</div>
          </div>
	
	
</section>
<div class="clearfix"></div>
<section id="portfolio-view" class="clearfix ">


    <div class="portfolio-filter-view">
        <?php
        while (have_posts()): the_post();
            $post_id = $post->ID;
            the_content();
        endwhile;
        ?>
        <!-- Filter tagss -->

        <?php
        $taxonomies = 'filter_tags';
        $parent_terms = get_terms($taxonomies, array('parent' => 0));
        //print_r($parent_terms);
        foreach ($parent_terms as $key => $object):
            if ($object->name == 'Category'):
                $args = array(
                    'orderby' => 'term_id',
                    'order' => 'ASC',
                    'hide_empty' => true,
                    'exclude' => array(),
                    'exclude_tree' => array(),
                    'include' => array(),
                    'fields' => 'all',
                    'hierarchical' => true,
                    'child_of' => $object->term_id,
                    'pad_counts' => false,
                    'cache_domain' => 'core'
                );
                $terms = get_terms($taxonomies, $args);
                //$a = get_option('portfolio_tags_settings'); print_r($a);
                $portfolio_tags_settings = json_decode(get_option('portfolio_tags_settings'));
                //print_r($portfolio_tags_settings);
                $portfolio_use_portfolio_filter = $portfolio_tags_settings->portfolio_use_portfolio_filter;
                //print_r($portfolio_use_portfolio_filter);
                if (!empty($portfolio_use_portfolio_filter)):
                    if (!empty($terms)):
                        ?>
                        <div class="container">
                            <ul class="nav navbar-nav filter-nav">
                             <li class=""><a href="javascript:void(0)">Filter by:</a></li>
                            <li class="dropdown fil-by"><a class="dropdown-toggle" data-toggle="dropdown">Service
							
					 <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu fliter-drp">
                                        <?php
                                        $i = 0;
                                        $temp = array();
                                        foreach ($terms as $item):
                                            if ($item->slug == 'ecommerce'):
                                                $temp[0] = $item;
                                            elseif ($item->slug == 'webapplications'):
                                                $temp[3] = $item;
                                            elseif ($item->slug == 'webdevelopment'):
                                                $temp[2] = $item;
                                            elseif ($item->slug == 'responsivedesign'):
                                                $temp[1] = $item;
                                            elseif ($item->slug == 'mobileapplications'):
                                                $temp[4] = $item;
                                            endif;
                                        endforeach;
                                        ksort($temp);
                                        foreach ($temp as $item):
                                            //var_dump($item);
                                            $classname = $item->slug;
                                            ?>
                                            <li><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="categorytermlist <?php echo $item->term_id; ?>" rel="<?php echo $classname; ?>" id="<?php echo $item->term_id; ?>"><?php echo str_replace('-', ' ', $item->name); ?></a></li>

                <?php endforeach; ?>
                                    </ul>
                                     <?php
                                endif;
                            endif;
                        else:
                            ?>
                                </li>
                                <li class="dropdown devide text-left filtertags">

                                    <a class="dropdown-toggle sortby" id="dropdownMenu1" data-toggle="dropdown" href="javascript:void(0)">Industry <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
<!--                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="termslist" rel="showall" id='all'>All</a></li>-->
                                        <?php
                                        $args = array(
                                            'orderby' => 'slug',
                                            'order' => 'ASC',
                                            'hide_empty' => true,
                                            'exclude' => array(),
                                            'exclude_tree' => array(),
                                            'include' => array(),
                                            'fields' => 'all',
                                            'hierarchical' => true,
                                            'child_of' => $object->term_id,
                                            'pad_counts' => false,
                                            'cache_domain' => 'core'
                                        );
                                        $terms = get_terms($taxonomies, $args);
                                        //$a = get_option('portfolio_tags_settings'); print_r($a);
                                        $portfolio_tags_settings = json_decode(get_option('portfolio_tags_settings'));
                                        //print_r($portfolio_tags_settings);
                                        $portfolio_use_portfolio_filter = $portfolio_tags_settings->portfolio_use_portfolio_filter;
                                        //print_r($portfolio_use_portfolio_filter);
                                        if (!empty($portfolio_use_portfolio_filter)):
                                            if (!empty($terms)):
                                                foreach ($terms as $item):
                                                    $classname = $item->slug;
                                                    ?>

                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="termslist <?php echo $item->term_id; ?>" rel="<?php echo $classname; ?>" id="<?php echo $item->term_id; ?>"><?php echo str_replace('-', ' ', $item->name); ?></a></li>
                                                    <?php
                                        endforeach;
                                    endif;
                                endif;
                                ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <?php
        endif;
    endforeach;
    ?>


        <!-- end of Filter Tags -->
       
            <div class="show_cat portfolio portfolio-projects">
             <ul id="mylist">
                <?php
                $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
                //print_r($portfolio_images_settings);
                //$portfolio_images_num_display = $portfolio_images_settings->portfolio_images_num_display;
                $portfolio_images_num_display = -1;
                //echo $portfolio_images_num_display;
                //query_posts('orderby=post_date&order=DESC&post_type=portfolio&posts_per_page='.intval($portfolio_images_num_display)); 
                query_posts('orderby=post_date&order=DESC&post_type=portfolio&post_status=publish&posts_per_page=-1');
                
                if (have_posts()) : $i = 0;
                    while (have_posts()) :
                        the_post();
                        $post_id = $post->ID;
                        //echo $post_id;
                        //$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id,'medium'));
                        //print_r($portfolio_feat_image);
                        $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                        $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                        //print_r($portfolio_post_meta);
                        //Returns Array of Term Names for "my_term"
                        $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                        $the_link = get_permalink();
                        $the_title = get_the_title();
                        $the_content = get_the_content('Read more');
                        $postclasses = '';
                        $posttags = '';
                        $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                        //print_r($portfolio_image_id_arr);

                        foreach ($portfolio_image_id_arr as $attach_img_id) :
                            $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                        endforeach;
                        //print_r($attach_folio_img);
                        foreach ($term_list as $item):
                            $posttags .= $item . " ";
                            //$postclasses .= str_replace(" ","_",$item)."-";
                        endforeach;
                        //$image = !empty($portfolio_feat_image[0])?$portfolio_feat_image[0]:$attach_folio_img[0];
                        if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                            ?>
              <li>  <div class="col-lg-3 col-sm-6">
                            <div class="portfolio-box <?php echo rtrim($posttags, " "); ?>">
                                <a href="<?php echo $portfolio_feat_image[0] ?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
										 <div class="line"></div>
                                        <div class="project-category text-faded">
                                            <h2><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h2>
                                            <a href="<?php the_permalink(); ?>"><?php echo wp_trim_words(strip_tags($the_content), 10); ?></a>
                                        </div>
                                        <div class="project-name">
                                        <a href="<?php the_permalink(); ?>"><?php
                                                  $terms = apply_filters( 'taxonomy-images-list-the-terms', '', array(
                                                    'taxonomy' => 'keyfeature',
                                                ) );

                                                foreach( (array) $terms as $term){
                                                    echo $term;
                                                }
                                                  ?></a>
                                            
                                        
					<br>
						 <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                                           <!-- <a href="<?php //the_permalink(); ?>">DIG <br>DEEP</a>-->
                                        </div>
                                    </div>
                                </div>

                            </div><!-- col-sm-4 col-xs-6 -->
            </div></li>
                
                            <?php
                            $i++;
                        endif;
                        if ($i == $portfolio_images_num_display): break;
                        endif;
                    endwhile;
                else:
                    ?>
                    <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
</ul>
   <input type="hidden" id="rowcount" value="<?php if($wp_query->found_posts > 0){echo $wp_query->found_posts;}else{echo "0";}?>" />          
         
</div>
            <div class="filter-content portfolio portfolio-projects"></div>   
            <div class="text-center seemore col-xs-12"><p class=" btn btn-default" id="loadMore">Wanna See More ?</p></div>
       
    </div>



</section>
</div>


<?php get_footer(); ?>
