<?php
/*
Template Name: Portfolio template
*/
    // calling the header.php
    get_header();
?>
<!-- Header for inner pages 
	================================================== -->
    <section class="inner-header">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                <?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
							} ?>
               	  <!--<h1><?php the_title(); ?></h1>-->
                  <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta;  ?>
              </div>
          </div>
        </div>
    </section>
<!-- Secondary Nav for each inner page
	================================================== -->
    <section id="secondnav">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">                    
                    <div class="navbar secondary-nav navbar-default" role="navigation">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".second-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="#">Navigation</a>
                        </div>
                        <div class="navbar-collapse second-nav collapse">
                          <?php
							wp_nav_menu( array(
												  'menu' => 'work',
												  'depth' => 2,
												  'container' => false,
												  'menu_class' => 'nav navbar-nav',
												  //Process nav menu using our custom nav walker
												  'walker' => new wp_bootstrap_navwalker())
												);
						 
						?>
                       </div><!--/.nav-collapse -->
                    </div>
                                
                </div>
            </div>
        </div>
    </section>
<!-- Body content
	================================================== -->    
    <section class="light-gray-wraper">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                     
                    <?php while(have_posts()): the_post(); $post_id = $post->ID; the_content(); endwhile; ?>
                    <div class="text-left row portfolio">
                    <?php
						$args = array(
							'post_type'=> 'portfolio',
							'order' => 'DESC',
							'posts_per_page' => -1
						);
						query_posts( $args );
					?>
                    <?php
						
						//Start the Loop.
						if ( have_posts() ) : while ( have_posts() ) : the_post();
						$post_id = $post->ID;
						//$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
						
						$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
						if($portfolio_post_meta->folio_is_home && $portfolio_post_meta->folio_is_home == 'Y')
						{
							$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'large');
							$portfolio_post_meta->portfolio_url;
							//Returns Array of Term Names for "my_term"
							$term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
							$postclasses = '';
							$posttags = '';
							$the_link = get_permalink();
							$the_title = get_the_title();
							$the_content =  get_the_content('Read more');
							foreach($term_list as $item):
								$posttags .= $item.", ";
								$postclasses .= str_replace(" ","_",$item)."-";
							endforeach;
					?>
                            <div class="col-sm-6">
                                <div class="featured-box">
                                    <div class="overlay">
                                        <p class="case-txt"><a href="<?php the_permalink();?>"><?php echo wp_trim_words(strip_tags($the_content),30); ?></a></p>
                                        <div class="hover-overlay clearfix"><a href="<?php the_permalink();?>"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div><!-- ./hover-overlay -->
                                    </div>
                                    <a href="<?php echo $portfolio_feat_image[0]?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                                </div>
                            </div><!-- /.col-md-6 -->
                    <?php 
					}
					 endwhile; ?>
                     	<div class="col-sm-12 text-center">
                        	<a href="<?php echo site_url('portfolio'); ?>" class="btn btn-warning">Looking for more? </a>
                        </div>
                     <?php else: ?>
                    <!-- The very first "if" tested to see if there were any Posts to --> 
                    <!-- display.  This "else" part tells what do if there weren't any. -->
                    <p>Sorry, no posts matched your criteria.</p>
                    <!-- REALLY stop The Loop. -->
                    <?php endif; ?>
                        <!--<div class="col-sm-6">
                        	<div class="white-box">
                            	<div class="white-box-top">
                                    <div class="row">
                                        <div class="col-md-9"><h3>PUMP PRODUCTS</h3></div>
                                        <div class="col-md-3 text-right"><a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></strong></a></div>
                                    </div>
                                    <p class="port-desc">This is description for Pump Products Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                </div>
                                <img src="../wp-content/themes/twentytwelve/images/pump.jpg" class="img-responsive" alt="">
                            </div>
                        </div>--><!-- /.col-md-6 -->
                        
                        <!--<div class="col-sm-6">
                        	<div class="white-box">
                            	<div class="white-box-top">
                                    <div class="row">
                                        <div class="col-md-9"><h3>RYCORE BRAND SHOES</h3></div>
                                        <div class="col-md-3 text-right"><a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></a></strong></div>
                                    </div>
                                    <p class="port-desc">This is description for Rycore Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                                </div>
                                <img src="../wp-content/themes/twentytwelve/images/rycore-brand.jpg" class="img-responsive" alt="">
                            </div>
                        </div>--><!-- /.col-md-6 -->
                        
                        <!--<div class="col-sm-6">
                        	<div class="white-box">
                            	<div class="white-box-top">
                                    <div class="row">
                                        <div class="col-md-9"><h3>PUMP PRODUCTS</h3></div>
                                        <div class="col-md-3 text-right"><a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></strong></a></div>
                                    </div>
                                    <p class="port-desc">This is description for Pump Products Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                </div>
                                <img src="../wp-content/themes/twentytwelve/images/pump.jpg" class="img-responsive" alt="">
                            </div>
                        </div>--><!-- /.col-md-6 -->
                        
                        <!--<div class="col-sm-6">
                        	<div class="white-box">
                            	<div class="white-box-top">
                                    <div class="row">
                                        <div class="col-md-9"><h3>RYCORE BRAND SHOES</h3></div>
                                        <div class="col-md-3 text-right"><a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></a></strong></div>
                                    </div>
                                    <p class="port-desc">This is description for Rycore Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                                </div>
                                <img src="../wp-content/themes/twentytwelve/images/rycore-brand.jpg" class="img-responsive" alt="">
                            </div>
                        </div>--><!-- /.col-md-6 -->
                        
                        <!--<div class="col-sm-6">
                        	<div class="white-box">
                            	<div class="white-box-top">
                                    <div class="row">
                                        <div class="col-md-9"><h3>PUMP PRODUCTS</h3></div>
                                        <div class="col-md-3 text-right"><a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></strong></a></div>
                                    </div>
                                    <p class="port-desc">This is description for Pump Products Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                </div>
                                <img src="../wp-content/themes/twentytwelve/images/pump.jpg" class="img-responsive" alt="">
                            </div>
                        </div>--><!-- /.col-md-6 -->
                        
                        <!--<div class="col-sm-6">
                        	<div class="white-box">
                            	<div class="white-box-top">
                                    <div class="row">
                                        <div class="col-md-9"><h3>RYCORE BRAND SHOES</h3></div>
                                        <div class="col-md-3 text-right"><a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></a></strong></div>
                                    </div>
                                    <p class="port-desc">This is description for Rycore Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                                </div>
                                <img src="../wp-content/themes/twentytwelve/images/rycore-brand.jpg" class="img-responsive" alt="">
                            </div>
                        </div>--><!-- /.col-md-6 -->
                        
                        
                    </div><!-- /.portfolio -->
                    
                </div>
            </div><!-- /.row -->
            
        </div>
    </section>
<div class="clear" style="clear:both"></div>
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>