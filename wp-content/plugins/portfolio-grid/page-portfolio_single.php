<?php

// calling the header.php

get_header();
?>
<style>
.email_templates{display: none;}
.sitefooter{display: none;}

</style>
<link rel="stylesheet" href="/wp-content/themes/dcw/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/wp-content/themes/dcw/css/services.css" type="text/css">
<link rel="stylesheet" type="text/css" href="https://alvarotrigo.com/multiScroll/jquery.multiscroll.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
<div class="portfolio-submenu">
  <div class="container">
     <a href="/work/"> <i class="fa fa-angle-left"></i> ALL PROJECTS</a>
     <?php $f_url = get_field('url');?>
     
      <ul class="portfolio-list" >
        
          <!-- <i class="fa fa-tag" aria-hidden="true"></i>  --> 
          <?php
            $project_cats = get_the_terms($post->ID, 'keyfeature');
            if(!empty($project_cats)){
            // Renumber array.
            $project_cats = array_values($project_cats);
            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
             <li> <?php  echo $project_cats[$cat_count]->name;?></li>
                  <?php  if ($cat_count<count($project_cats)-1){echo'  |  ';}
            }}
          ?>
        </li>
      </ul>
    </div>
</div>

<div class="container">
 <p class="project"><a href="<?php 'https://'.the_field('url') ?>" target="_blank" ><?php the_title();?></a></p>
  <!-- <div class="social_prt">
    <ul>
      <li>share</li>
      <li><a href="http://www.facebook.com/dotcomweavers" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
      <li><a href="http://twitter.com/Dotcomweavers" target="_blank"><i class="fab fa-twitter"></i></a></li>
      <li><a href="https://www.instagram.com/dotcomweavers/" target="_blank"><i class="fab fa-instagram"></i></a></li>                  
    </ul> 
  </div> -->
  <ul id="menu">
  	<?php
  		$count=0;
		if (have_rows('scroll')) {
		  while (have_rows('scroll')) {
		    the_row();
		    // nested repeater
		   $count += count(get_sub_field('highlight_text'));
		  }
		}
		//echo $count;
  	?>
    <?php if( have_rows('scroll') ): $cnt =1; while( have_rows('scroll') ): the_row(); ?>
          <?php if($cnt <= $count){ ?>
          <li data-menuanchor="first<?php echo $cnt; ?>" class="<?php echo ($cnt==1) ? 'active' : '';?>">
              <a href="#first<?php echo $cnt; ?>">
                <?php if(!empty(get_sub_field('tab_title'))) {?>
                <?php echo get_sub_field('tab_title'); ?>
                <?php } else { echo 'Highlight'.$cnt; } ?>
              </a>
          </li>
    <?php $cnt++; } endwhile; endif; ?> 
  </ul>
</div>
<div class="clearfix"></div>
<div class="multiscroll_main">
  <div class="container">
    <div id="multiscroll">
      <div class="ms-left">
        <?php if( have_rows('scroll') ): while( have_rows('scroll') ): the_row(); ?>
          <div class="ms-section" style="background-image:url('<?php the_sub_field('image_scroll'); ?>');">     
                    <!-- <img class="img-responsive" src="<?php// the_sub_field('image_scroll'); ?>"> -->
          </div>
        <?php endwhile; endif; ?>  
      </div>

      
      <div class="ms-right">
      <?php if( have_rows('scroll') ): while( have_rows('scroll') ): the_row(); ?>
        <h1 class="inner-port-mob-highlight"><?php the_sub_field('highlight_text');?></h1>
         <img class="img-responsive" src="<?php the_sub_field('image_scroll'); ?>">
        <div class="ms-section">
          <div style="background-color:<?php the_sub_field('color');?>" class="projects_pfl_dv">
            <h1><?php the_sub_field('highlight_text');?></h1>
              <?php $desc = get_sub_field('content');
              echo mb_strimwidth($desc, 0, 802, ''); ?>
            <a href="/contact-us/" class="action_button"><button type="button" class="btn">Contact Us</button></a>

           <!-- <h6>Category: 
              <?php
                $project_cats = get_the_terms($post->ID, 'keyfeature');
                $project_cats = array_values($project_cats);
                for($cat_count=0; $cat_count<count($project_cats); $cat_count++) { ?>
                <?php  echo $project_cats[$cat_count]->name;?>
                  <?php  if ($cat_count<count($project_cats)-1){echo',';}
                }
              ?>
            </h6>-->

          </div>
        </div>
        <?php endwhile; endif; ?>
      </div>      

    </div>
  </div>
</div>
  <script src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.easings.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/multiscroll.scrollOverflow.limited.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.multiscroll.min.js"></script>

<?php get_footer(); ?>
