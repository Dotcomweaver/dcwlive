<?php
/*
Template Name: Portfolio template
*/
// calling the header.php
get_header();
?>
<!-- Header for inner pages 
================================================== -->
<section class="inner-header-work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php 
			if ( function_exists('yoast_breadcrumb'))
			{
                yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
            } 
			?>
            <!--<h1><?php the_title(); ?></h1>-->
            <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta;  ?>
          </div>
      </div>
    </div>
</section>
<!-- Secondary Nav for each inner page
	================================================== -->
<section id="secondnav" class="work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="navbar secondary-nav navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".second-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="#">Navigation</a>
                    </div>
                    <div class="navbar-collapse second-nav collapse">
                      <?php
                        wp_nav_menu( array(
                                              'menu' => 'work',
                                              'depth' => 2,
                                              'container' => false,
                                              'menu_class' => 'nav navbar-nav',
                                              //Process nav menu using our custom nav walker
                                              'walker' => new wp_bootstrap_navwalker()
											)
                                     );
                     
                    ?>
                   </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Body content
	================================================== -->    
<section class="light-gray-wraper divider">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                   <?php while(have_posts()): the_post(); $post_id = $post->ID; the_content(); endwhile; ?>
                    <!-- Filter tagss -->
                    <div class="row">
					<?php
						$taxonomies = 'filter_tags';
						$parent_terms = get_terms($taxonomies,array('parent'=>0));
						//print_r($parent_terms);
						foreach($parent_terms as $key=>$object):
							if($object->name == 'Category'):
								$args = array(
													'orderby'       => 'term_id', 
													'order'         => 'ASC',
													'hide_empty'    => true, 
													'exclude'       => array(), 
													'exclude_tree'  => array(), 
													'include'       => array(),
													'fields'        => 'all', 
													'hierarchical'  => true, 
													'child_of'      => $object->term_id, 
													'pad_counts'    => false, 
													'cache_domain'  => 'core'
											); 
								$terms = get_terms($taxonomies,$args);
								//$a = get_option('portfolio_tags_settings'); print_r($a);
								$portfolio_tags_settings = json_decode(get_option('portfolio_tags_settings'));
								//print_r($portfolio_tags_settings);
								$portfolio_use_portfolio_filter = $portfolio_tags_settings->portfolio_use_portfolio_filter;
								//print_r($portfolio_use_portfolio_filter);
								if(!empty($portfolio_use_portfolio_filter)):
									if(!empty($terms)): ?>
                                	<div class="testimonial-category-filter col-md-9 col-sm-9 text-left">
                                    <ul class="nav nav-pills nav-justified">
							<?php
									 $i = 0;
									 $temp = array();
									 foreach($terms as $item):
									 	if($item->slug == 'ecommerce'):
											$temp[0] = $item;
										elseif($item->slug == 'webapplications'):
											$temp[3] = $item;
										elseif($item->slug == 'webdevelopment'):
											$temp[2] = $item;
										elseif($item->slug == 'responsivedesign'):
											$temp[1] = $item;
										elseif($item->slug == 'mobileapplications'):
											$temp[4] = $item;
										endif;
									 endforeach;
									 ksort($temp);
									 foreach($temp as $item):
									 	//var_dump($item);
										$classname = $item->slug; ?>
                                        	<li><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="btn btn-warning categorytermlist <?php echo  $item->term_id; ?>" rel="<?php echo $classname;?>" id="<?php echo $item->term_id; ?>"><?php echo str_replace('-', ' ', $item->name);?></a></li>
                                            
                        <?php
									endforeach; ?>
                                    </ul>
                                    </div>
                        <?php		endif;
								endif;
							else:
						?>
								 <div class="col-md-3 col-sm-3">
                                 	<div class="p10">
                                 	<div class="dropdown devide text-left filtertags">
                                      <button class="btn btn-warning dropdown-toggle sortby" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                        Filter By Industry
                                        <span class="glyphicon glyphicon-th"></span>
                                      </button>
                                      <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                                      <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="termslist" rel="showall" id='all'>All</a></li>
                                      	<?php
											$args = array(
												'orderby'       => 'slug', 
												'order'         => 'ASC',
												'hide_empty'    => true, 
												'exclude'       => array(), 
												'exclude_tree'  => array(), 
												'include'       => array(),
												'fields'        => 'all', 
												'hierarchical'  => true, 
												'child_of'      => $object->term_id, 
												'pad_counts'    => false, 
												'cache_domain'  => 'core'
										); 
										$terms = get_terms($taxonomies,$args);
										//$a = get_option('portfolio_tags_settings'); print_r($a);
										$portfolio_tags_settings = json_decode(get_option('portfolio_tags_settings'));
										//print_r($portfolio_tags_settings);
										$portfolio_use_portfolio_filter = $portfolio_tags_settings->portfolio_use_portfolio_filter;
										//print_r($portfolio_use_portfolio_filter);
										if(!empty($portfolio_use_portfolio_filter)):
											if(!empty($terms)):
											 	foreach($terms as $item):
													$classname = $item->slug;
											?>
 
    												<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="termslist <?php echo $item->term_id;?>" rel="<?php echo $classname;?>" id="<?php echo $item->term_id; ?>"><?php echo str_replace('-', ' ', $item->name);?></a></li>
    							<?php 			endforeach;
											endif;
										endif;
										?>
                                      </ul>
                                      </div>
                                      </div>
                                 </div>
						<?php
							endif;
						endforeach;
					?>
					</div>		
                    
                    <!-- end of Filter Tags -->
                    <div class="row">
                   	<div class="show_cat portfolio">
						<?php
                            $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
                            //print_r($portfolio_images_settings);
                            //$portfolio_images_num_display = $portfolio_images_settings->portfolio_images_num_display;
							$portfolio_images_num_display = 30;
							//echo $portfolio_images_num_display;
                            //query_posts('orderby=post_date&order=DESC&post_type=portfolio&posts_per_page='.intval($portfolio_images_num_display)); 
							query_posts('orderby=post_date&order=DESC&post_type=portfolio&post_status=publish&posts_per_page=-1'); 
                            if ( have_posts() ) : $i = 0;
                                while ( have_posts() ) : 
                                    the_post();
                                    $post_id = $post->ID;
									//echo $post_id;
                                    //$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id,'medium'));
                                    //print_r($portfolio_feat_image);
                                    $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),array(600,600), false, '');
                                    $portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
									//print_r($portfolio_post_meta);
                                    //Returns Array of Term Names for "my_term"
                                    $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                                    $the_link = get_permalink();
									$the_title = get_the_title();
									$the_content =  get_the_content('Read more');
									$postclasses = '';
                                    $posttags = '';
									$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
									//print_r($portfolio_image_id_arr);
									
									foreach( $portfolio_image_id_arr as $attach_img_id ) :
											$attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
									endforeach;
									//print_r($attach_folio_img);
                                    foreach($term_list as $item):
                                        $posttags .= $item." ";
                                        //$postclasses .= str_replace(" ","_",$item)."-";
                                    endforeach;
									//$image = !empty($portfolio_feat_image[0])?$portfolio_feat_image[0]:$attach_folio_img[0];
									if($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                        ?>
                                        <div class="col-sm-4 col-xs-6 <?php echo rtrim($posttags," "); ?>">
                                            <div class="featured-box">
                                                <div class="overlay">
                                                	
                                                    <p class="overlaycase-txt">
                                                        <a href="<?php the_permalink();?>"><?php echo wp_trim_words(strip_tags($the_content),10); ?></a></p>
                                                    <div class="hover-overlay clearfix"><a href="<?php the_permalink();?>"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div><!-- ./hover-overlay -->
                                                </div>
                                                <a href="<?php echo $portfolio_feat_image[0]?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                                                <a class="full-box-link" href="<?php the_permalink();?>"></a>
                                            </div>
                                            
                                        </div><!-- col-sm-4 col-xs-6 -->
                        <?php $i++; endif; 
						if($i == $portfolio_images_num_display): break; endif;
						endwhile; else: ?>
                        		<p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
                     	</div>
                     <div class="filter-content portfolio"></div>   
                   </div>
                </div>
            </div><!-- /.row -->
            
        </div>
    </section>
<div class="clear" style="clear:both"></div>
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>