<?php
    // calling the header.php
    get_header();
?>
<?php
$portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
$portfolio_layout_settings = json_decode(get_option('portfolio_layout_settings'));
$portfolio_use_full_width = $portfolio_layout_settings->portfolio_use_full_width;
$portfolio_images_width = $portfolio_images_settings->portfolio_images_width;
$portfolio_images_height = $portfolio_images_settings->portfolio_images_height;
?>
<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post();
$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
?>
<?php
$post_id = $post->ID;
$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
$imgcount = 1;
?>
<section class="portfolios">
	<?php 
		//foreach($portfolio_image_id_arr as $key=>$value):
			$attach_folio_img = wp_get_attachment_image_src($portfolio_image_id_arr[0], 'full');
	?>
    		<div class="folio-banner"><img class="img-responsive" src="<?php echo $attach_folio_img[0]; ?>"></div>
    <?php
		//endforeach;
	?>
    <div class="casestudy-testimonials">
    	<?php
        if($portfolio_post_meta->portfolio_linked_video_testimonials != ''){
            wp_reset_postdata();
            $testimonial_object = get_post( $portfolio_post_meta->portfolio_linked_video_testimonials );
            add_filter($testimonial_object->post_content, 'remove_empty_tags_around_shorcodes');
            $meta_object = get_post_meta($portfolio_post_meta->portfolio_linked_video_testimonials);
            $meta = json_decode($meta_object['v_testimonials_post_meta'][0]);
            
        ?>
        	<div class="container">
              <div class="col-md-2 col-sm-2 col-xs-12">
                    <?php $img_src = get_ytube_video_code($meta->vt_url); ?>
                    <img class="img-responsive" src="https://img.youtube.com/vi/<?php echo $img_src; ?>/0.jpg" />
                    <a data-target="#vt_modal" data-toggle="modal" rel="<?php echo $meta->vt_url; ?>" class="hvideo vt_play" href="javascript:void();"><span class="icomoon-icon-play-2"></span></a>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-12">
                    <h2>"<?php echo $testimonial_object->post_content; ?>"<span>- <?php echo $meta->client_name; ?></span></h2>
              </div>
            </div>
       <?php
		 }else if($portfolio_post_meta->portfolio_client_testimonials != ''){ 
		?>
         	<div class="container"><?php echo $portfolio_post_meta->portfolio_client_testimonials; ?></div>
       <?php } ?>
    </div>
</section>
<?php the_content(); ?>
<?php endwhile; else: ?>
<!-- The very first "if" tested to see if there were any Posts to --> 
<!-- display.  This "else" part tells what do if there weren't any. -->
<p>Sorry, no posts matched your criteria.</p>
<!-- REALLY stop The Loop. -->
<?php endif; ?>
<?php get_footer(); ?>