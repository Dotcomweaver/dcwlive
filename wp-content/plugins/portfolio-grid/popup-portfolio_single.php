<?php

/**

* Template Name: HomePageOld

* The template for displaying all pages.

*

* This is the template that displays all pages by default.

* Please note that this is the WordPress construct of pages

* and that other 'pages' on your WordPress site will use a

* different template.

*

* @package WordPress

* @subpackage Twenty_Eleven

* @since Twenty Eleven 1.0

*/



get_header(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/flexslider.css" />

<!--Testimonails carousel Scripts Starts here-->
<script src="<?php echo get_template_directory_uri(); ?>/js/dw_event.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/dw_scroll.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scroll_controls.js" type="text/javascript"></script>
<script type="text/javascript">
function init_dw_Scroll() {
var wndo = new dw_scrollObj('wn', 'lyr1', 't1');
wndo.setUpScrollControls('scrollLinks');
}
// if code supported, link in the style sheet and call the init function onload
if ( dw_scrollObj.isSupported() ) {
//dw_writeStyleSheet('css/scroll.css');
dw_Event.add( window, 'load', init_dw_Scroll);
}
</script>
<style type="text/css">
div#wn {
float: left;
height: 115px;
margin: 0 10px 0 20px;
overflow: hidden;
position: relative;
width: 550px;
}
div#lyr1 {
top:0px !important;
}
.popup{position:fixed; background: url("../images/fancybox_overlay.png") repeat scroll 0 0 transparent; width:100%; height:100%; z-index:9999999; left:0;}
.youtube_ifram{background:#F9F9F9; width:40%; height:60%; margin:auto; position:relative; top:18%; padding:10px; border-radius:5px; bottom:0;}
.popup_close{background:url("../images/fancybox_sprite.png") no-repeat; cursor: pointer;
height: 36px;
position: absolute;
right: -18px;
top: -18px;
width: 36px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("a#show_video").click(function(){
			var yvideo = $(this).attr('rel');
			$("#inner_video_div").html("<a href='javascript:;' class='popup_close' id='close_btn' title='Close'></a><iframe src="+yvideo+" width='100%' height='100%' autoplay='true'></iframe>");
			$("#video_div").show();
			init();
		});
	$('#video_div').click(function(){
  		$('#video_div').hide();
  	});
});
function init(){
$('#close_btn').click(function(){
$("#inner_video_div").html('');
$("#video_div").hide()
});
}
</script>
<!--Testimonails carousel Scripts Ends here-->

<div class="row clearfix">
<div class="row bgclr">
<div class="container main_section">	

<div class="row">

<div class="eight columns slidecontent">
<h2>OVER 300 SUCCESSFUL PROJECTS AND COUNTING...</h2>
<p>We design, build and implement web and ecommerce solutions for businesses, serving a wide variety of clients in different industries from Paper to Pump.
</p>
<p>We specialize in ecommerce, web applications, third party integrations, shipping, payment gateways, CRM and ERP.</p>

<div class="btnwrp five columns">
<a class="org_btn" href="javascript:void(0)"><span>We have proof</span></a>
</div>

</div>	<!--eight slider slidecontent-->

<div class="nine columns fr">
<div class="flexslider">
<?php
$args = array(
'post_type'=> 'portfolio',
'order' => 'DESC',
'posts_per_page' => -1
);
query_posts( $args );
?>
<ul class="slides">
<?php while ( have_posts() ) : the_post();
$folio_post_meta = json_decode(get_post_meta($post->ID,'portfolio_post_meta', true));
if($folio_post_meta->folio_is_home && $folio_post_meta->folio_is_home == 'Y')
{
$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'large');
?>
		<li><img src="<?php echo $portfolio_feat_image[0] ?>" alt="" style="" /></li>
<?php
}
endwhile; ?>
</ul>
</div>
<!--flexslider--> 
</div>
<!--thirteen columns fr--> 
</div>

</div>	<!--main_section-->
</div>	<!--row bgclr-->

<div class="row testimonails">
<div class="container testmonials_bg">
<div class="five columns">
<h3>TESTIMONIALS</h3>
<p>We treat our clients like partners with 
the understanding that the success of
your project is our success.</p>
<div class="btnwrp five columns">
<a class="org_smal" href="javascript:void(0)"><span>VIEW ALL</span></a>
</div>
</div>	<!--five columns testimonails content wrap-->

<div class="twelve columns fr">	
<div id="scrollLinks"> <a href="#" class="mouseover_left"></a>
<div id="wn">
<div id="lyr1">
<table width="100%" border="0" id="t1">
<tr> 
<?php
$args = array(
'post_type'=> 'video-testimonials',
'order' => 'DESC',
'posts_per_page' => -1
);
query_posts( $args );
?>
<?php while ( have_posts() ) : the_post();
$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
	
	
if($v_test_post_meta->vt_is_home && $v_test_post_meta->vt_is_home == 'Y')
{
$y_img = get_ytube_video_code($v_test_post_meta->vt_url);
$te = $v_test_post_meta->vt_url;
	
	$start = strpos($te,"v=")+2;
	
	$end = strpos($te,"&");
	
	if( $end == "" || $end == 0 ){
		$fstr = substr($te,$start);
	} else {
		$length = $end-$start;
		$fstr = substr($te,$start,$length);
	}
?>
<td><a href="javascript:void(0)" rel="http://www.youtube.com/embed/<?php echo $fstr; ?>" id="show_video"><img src="http://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" alt="<?php the_title() ?> - Testimonial for NJ Web Design company" />
</a></td>

<?php
}
endwhile; ?>
<!--<td><a href="#" class="arrow-left"></a></td>-->
<!--<td><a href="#" rel="example_group"><img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" alt="" /></a></td>
<td><a href="#" rel="example_group"><img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" alt="" /></a></td>
<td><a href="#" rel="example_group"><img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" alt="" /></a></td>
<td><a href="#" rel="example_group"><img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" alt="" /></a></td>
<td><a href="#" rel="example_group"><img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" alt="" /></a></td>-->
<!--<td><a href="#" class="arrow-right"></a></td>--> 

<!--<a  href="javascript:void(0)" rel="http://www.youtube.com/embed/<?php echo $fstr; ?>" class="thickbox" id="show_video">

<span class="pic">

<img src="http://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" alt="<?php the_title() ?> - Testimonial for NJ Web Design company" style="width:300px;" />

<div class="img_overlay"></div>

</span>

</a>-->

</tr>
<!--<tr><td>
<div class="thirteen columns fr">

<div id="amazingslider-2" style="display:block;position:relative;margin:15px auto 30px;">



<ul class="amazingslider-slides" style="display:none;">



<?php while ( have_posts() ) : the_post();



$post_id = $post->ID;



$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));



print_r($v_test_post_meta);







if($v_test_post_meta->vt_is_home && $v_test_post_meta->vt_is_home == 'Y')



{



$y_img = '<img src="https://img.youtube.com/vi/'.get_ytube_video_code($v_test_post_meta->vt_url).'/0.jpg" alt="" style="width:200px;" />';



?>



		<li><?php echo $y_img; ?>



			<video preload="none" src="<?php echo $v_test_post_meta->vt_url; ?>"></video>



		</li>



<?php



}



endwhile; ?>



</ul>



<ul class="amazingslider-thumbnails" style="display:none;">







<?php while ( have_posts() ) : the_post();



$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));



if($v_test_post_meta->vt_is_home && $v_test_post_meta->vt_is_home == 'Y')



{



$y_img_thumb = '<img src="https://img.youtube.com/vi/'.get_ytube_video_code($v_test_post_meta->vt_url).'/1.jpg" alt="" />';



?>



<li><?php echo $y_img_thumb; ?></li>







<?php



}



endwhile; ?>







</ul>



</div>



<div class="clearfix" style="height:80px;"></div>



<?php wp_reset_query(); ?>







</div>
</td></tr>-->
</table>
</div>
</div>
<a href="#" class="mouseover_right"></a> </div>
</div>

</div>	<!--testmonials_bg-->
</div>  <!--testimonails-->

<div class="clearfix"></div>  

<div class="row producticons">	
<div class="container icons_wrapper">
Icons Comes here
</div>	<!--icons_wrapper-->
</div> <!--product_icons-->

<div class="row boxwrapper">		
<div class="container icons_wrapper">
<div class="six columns custwid">
<div class="service-box">
<div class="row clearfix hrline">
<figure><img src="images/onlineicon.png" alt=""></figure>
<h2>Get Online</h2>
</div>
<p>
Ecommerce revenue world-wide currently is a whopping $1.2 trillion, if you had any doubts of about ecommerce, then that is cleared now. So Get Online, and if your business is already online, let us show you how to dominate it. We specialize in unique yet practical website designs that highlight your company's personality, tailored to your target audience, and your core values.
</p>
</div>	<!--service-box-->

<div class="botm_img"></div>

</div>	<!--six columns-->

<div class="six columns custwid">
<div class="service-box">
<div class="row clearfix hrline">
<figure><img src="images/noticed_ico.png" alt=""></figure>
<h2>Get NOTICED</h2>
</div>
<p>
Whether you are a small business trying to build an online presence from scratch or an established businesses looking to make significant improvements to your existing ecommerce website, the experienced web designers and developers at Dotcomweavers can provide you with everything to make your online store a success from initial concept to completion.
</p>
</div>	<!--service-box-->

<div class="botm_img"></div>

</div>	<!--six columns-->

<div class="six columns custwid">
<div class="service-box">
<div class="row clearfix hrline">
<figure><img src="images/results_ico.png" alt=""></figure>
<h2>Get RESULTS</h2>
</div>
<p>
Whether your target customers are local, regional, or national, the online marketing experts at Dotcomweavers can effectively help you find and engage with your target audience. We utilize strategies such as search engine optimization, pay-per click advertising, social media marketing, online ad placements, and much more to ensure that you maximize your web presence. 
</p>
</div>	<!--service-box-->
<div class="botm_img"></div>
</div>	<!--six columns-->

</div>	<!--icons_wrapper-->
</div>	<!--boxwrapper-->


<div class="row newswrapper">
<div class="container">
<div class="clearfix"></div>
<h1>Recent News</h1>
</div>

<div class="container clearfix" id="gotoBlog">

<!--<div class="one columns dated">
<span>17</span>
<div class="month">APR</div>
</div>	<!--dated

<div class="fifteen columns">	
Your News Item Title Right Here
</div>-->
<!--end_row-->
<div class="thirteen columns">
<?php query_posts('showposts=3'); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="recentnews">
<h2 class="h2_heading"><span><?php the_title(); ?></span></h2>
<div style="padding-bottom:18px;"><?php the_excerpt(); ?></div>
</div>
<?php endwhile; // end of the loop. ?>
<?php wp_reset_query(); ?>
<div class="clear" style="height:80px;"></div>
</div>
 <!--container_1280x960-->



<!-- END OF BLOG -->




</div>	<!--newspost-->
<div class="botm_imglarge"></div>	<!--botm_imglarge-->

</div>  <!--recentnews-->

<!--container clearfix--> 
<!--body_section ends--> 
</div><!--row clearfix-->

<?php get_footer(); ?>