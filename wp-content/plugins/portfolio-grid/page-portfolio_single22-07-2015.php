<?php
    // calling the header.php
    get_header();
?>
<?php
$portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
$portfolio_layout_settings = json_decode(get_option('portfolio_layout_settings'));
$portfolio_use_full_width = $portfolio_layout_settings->portfolio_use_full_width;
$portfolio_images_width = $portfolio_images_settings->portfolio_images_width;
$portfolio_images_height = $portfolio_images_settings->portfolio_images_height;
?>
<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post();
$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
?>
<?php
$post_id = $post->ID;
$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
//print_r($portfolio_post_meta);
$imgcount = 1;
?>
<section class="portfolios">
		<div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
           <?php
		   		foreach($portfolio_image_id_arr as $key=>$value){
			?> 
            		<li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){ ?>class="active" <?php } ?>></li>
            <?php } ?>
        </ol>
        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <?php 
			foreach($portfolio_image_id_arr as $key=>$value){
				 $attach_folio_img = wp_get_attachment_image_src($value, 'full');
			?>
                <div class="item <?php if($key == 0){ ?>active <?php } ?>">
                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill"><img class="img-responsive" src="<?php echo $attach_folio_img[0]; ?>"></div>
                    <!--div class="carousel-caption">
                        <h2>Caption 1</h2>
                    </div>-->
                </div>
            <?php
			}
			?>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>

    </div>
    </section>
<section class="shdw">
    <div class="box-shdw">
    </div>
</section>
<?php the_content(); ?>
<?php endwhile; else: ?>
<!-- The very first "if" tested to see if there were any Posts to --> 
<!-- display.  This "else" part tells what do if there weren't any. -->
<p>Sorry, no posts matched your criteria.</p>
<!-- REALLY stop The Loop. -->
<?php endif; ?>
<?php get_footer(); ?>