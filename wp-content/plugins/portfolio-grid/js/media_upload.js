jQuery(document).ready(function() {

	jQuery("#portfolio_completion_date").datepicker({dateFormat: 'yy-mm-dd' });

	var $default=window.send_to_editor;
	jQuery('#upload_image_button1, #upload_image_button2, #upload_image_button3').click(function() {
		formfield = jQuery('#upload_image').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');

		window.send_to_editor = function(html) {

			// WT Fix
			html = '<span>'+ html +'</span>';

			$classes = jQuery('img', html).attr('class'); // old code
			$id = $classes.replace(/(.*?)wp-image-/, '');
			$hidden_div = jQuery('.upload_image_id_hidden');
			$hidden_div.val($hidden_div.val()+','+$id);
			$imgurl = jQuery('img',html).attr('src');
			jQuery('#upload_image').after("<li class='portfolio_uploaded_img_"+$id+"'><img src='"+$imgurl+"' alt=''/></li><li class='portfolio_uploaded_img_"+$id+"' onclick=\"portfolio_remove_image('"+$id+"');\">X_REMOVE_IMG</li>");
			tb_remove();
			window.send_to_editor=$default;
		}

		return false;
	});
	jQuery('#upload_result_button').click(function() {
		formfield = jQuery('#result_image').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');

		window.send_to_editor = function(html) {

			// WT Fix
			html = '<span>'+ html +'</span>';

			$classes = jQuery('img', html).attr('class'); // old code
			$id = $classes.replace(/(.*?)wp-image-/, '');
			$hidden_div = jQuery('#result_image_id_hidden');
			$hidden_div.val($id);
			$imgurl = jQuery('img',html).attr('src');
			jQuery('#result_image').html("<li class='portfolio_uploaded_img_"+$id+"'><img src='"+$imgurl+"' alt=''/></li><li class='portfolio_uploaded_img_"+$id+"' onclick=\"portfolio_remove_image('"+$id+"');\">X_REMOVE_IMG</li>");
			tb_remove();
			window.send_to_editor=$default;
		}

		return false;
	});
	
	jQuery(document).on('click','.uploadslide',function() {
		//formfield = jQuery('#upload_image').attr('name');
		$tdid = jQuery(this).attr('id');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');

		window.send_to_editor = function(html) {

			// WT Fix
			html = '<span>'+ html +'</span>';
			
			$classes = jQuery('img', html).attr('class'); // old code
			$id = $classes.replace(/(.*?)wp-image-/, '');
			//$hidden_div = jQuery('#upload_image_id_hidden');
			//$hidden_div.val($hidden_div.val()+','+$id);
			$imgurl = jQuery('img',html).attr('src');
			jQuery('.image_container_'+$tdid).html('<li><img src="'+$imgurl+'"><input type="hidden" name="slides['+$tdid+'][image]" value="'+$id+'" /></li>');
			tb_remove();
			window.send_to_editor=$default;
		}

		return false;
	});
	jQuery(document).on('click','.uploadgraph',function() {
		//formfield = jQuery('#upload_image').attr('name');
		$tdid = jQuery(this).attr('id');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');

		window.send_to_editor = function(html) {

			// WT Fix
			html = '<span>'+ html +'</span>';
			
			$classes = jQuery('img', html).attr('class'); // old code
			$id = $classes.replace(/(.*?)wp-image-/, '');
			//$hidden_div = jQuery('#upload_image_id_hidden');
			//$hidden_div.val($hidden_div.val()+','+$id);
			$imgurl = jQuery('img',html).attr('src');
			jQuery('.graph_container_'+$tdid).html('<li><img src="'+$imgurl+'"><input type="hidden" name="graphs['+$tdid+'][image]" value="'+$id+'" /></li>');
			tb_remove();
			window.send_to_editor=$default;
		}

		return false;
	});
	jQuery(document).on('click','.removeslider',function(){
			var id = jQuery(this).attr('id');
			jQuery('.row_'+id).remove();
	});
	jQuery(document).on('click','.removegraph',function(){
			var id = jQuery(this).attr('id');
			jQuery('.row_'+id).remove();
	});
	jQuery('#add_more_slides').click(function() {
			var count = jQuery(this).attr('class');
			var value = ++count;
			var html = '<tr class="row_'+value+'"><td width="40%" style="vertical-align:top"><ul class="image_container_'+value+'"></ul><input type="button" id="'+value+'" value="Select a File" class="uploadslide"/></td><td width="40%" style="vertical-align:top"><textarea name="slides['+value+']["content"]" id="slides['+value+']["content"]" cols="30" rows="4"></textarea></td><td width="20%" style="vertical-align:top"><input type="button" id="'+value+'" class="removeslider" value="Remove Slider"></td></tr>';
			jQuery('.carouselslides').append(html);
			jQuery('#add_more_slides').attr('class',value);
	});
	jQuery('#add_more_graphs').click(function() {
			var count = jQuery(this).attr('class');
			var value = ++count;
			var html = '<tr class="row_'+value+'"><td width="40%" style="vertical-align:top"><ul class="graph_container_'+value+'"></ul><input type="button" id="'+value+'" value="Select a File" class="uploadgraph"/></td><td width="40%" style="vertical-align:top"><textarea name="graphs['+value+']["content"]" id="graphs['+value+']["content"]" cols="30" rows="4"></textarea></td><td width="20%" style="vertical-align:top"><input type="button" id="'+value+'" class="removegraph" value="Remove Grpah"></td></tr>';
			jQuery('.graphgraphics').append(html);
			jQuery('#add_more_graphs').attr('class',value);
	});
	var $default=window.send_to_editor;
	jQuery('#home_image_button').click(function() {
		formfield = jQuery('#home_image').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');

		window.send_to_editor = function(html) {

			// WT Fix
			html = '<span>'+ html +'</span>';
			$classes = jQuery('img', html).attr('class'); // old code
			$id = $classes.replace(/(.*?)wp-image-/, ''); 
			$hidden_div = jQuery('#portfolio_home_image_hidden');
			if($hidden_div.val() != ''){
				$hidden_div.val($hidden_div.val()+','+$id);
			}else{
				$hidden_div.val($id);	
			}
			$imgurl = jQuery('img',html).attr('src');
			jQuery('#home_image').after("<li class='portfolio_home_image_"+$id+"'><img src='"+$imgurl+"' alt=''/></li><li class='portfolio_home_image_"+$id+"' onclick=\"home_image_remove_image('"+$id+"');\">X_REMOVE_IMG</li>");
			tb_remove();
			window.send_to_editor=$default;
		}

		return false;
	});
	jQuery('#services_image_button').click(function() {
		formfield = jQuery('#services_image').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');

		window.send_to_editor = function(html) {

			// WT Fix
			html = '<span>'+ html +'</span>';
			$classes = jQuery('img', html).attr('class'); // old code
			$id = $classes.replace(/(.*?)wp-image-/, ''); 
			$hidden_div = jQuery('#services_portfolio_image_hidden');
			/*if($hidden_div.val() != ''){*/
			$hidden_div.val($id);
			/*}else{
				$hidden_div.val($id);	
			}*/
			$imgurl = jQuery('img',html).attr('src');
			jQuery('#services_image').html("<li class='services_portfolio_image_"+$id+"'><img src='"+$imgurl+"' alt=''/></li><li class='services_portfolio_image_"+$id+"' onclick=\"services_image_remove('"+$id+"');\">X_REMOVE_IMG</li>");
			tb_remove();
			window.send_to_editor=$default;
		}

		return false;
	});
});

//Remove image from custom field box below editor if uploaded
function portfolio_remove_image($id){
	jQuery('.portfolio_uploaded_img_'+$id).remove();

	$hidden_val = jQuery('.upload_image_id_hidden');
	$value = $hidden_val.val();

	$new_value = $value.replace($id,'');
	$hidden_val.val($new_value);
}
function home_image_remove_image($id){
	jQuery('.portfolio_home_image_'+$id).remove();

	$hidden_val = jQuery('#portfolio_home_image_hidden');
	$value = $hidden_val.val();

	$new_value = $value.replace($id,'');
	$hidden_val.val($new_value);
}
function services_image_remove($id){
	jQuery('.services_portfolio_image_'+$id).remove();

	$hidden_val = jQuery('#services_portfolio_image_hidden');
	$value = $hidden_val.val();

	$new_value = $value.replace($id,'');
	$hidden_val.val($new_value);
}
//add custom filter tags to portfolio **DEPRICATED** using custom taxonomies
function portfolio_project_add_tags(){
	$this = jQuery('#portfolio_project_tag');
	$hidden = jQuery('#portfolio_project_tag_hidden');
	$hidden_val = $hidden.val();
	$this_val = $this.val().replace(/[^a-z0-9.]/gi,' ').replace(/ /g,'_');
	jQuery('#portfolio_project_tags_list').append("<li id='portfolio_tag_"+$this_val+"'><a href='#' onclick=\"portfolio_remove_tags('portfolio_tag_"+$this_val+"');\">X</a> "+$this_val+"</li>");

	$hidden.val($hidden_val+""+$this_val+"-");
	$this.val('');
}

//remove custom filter tags to portfolio **DEPRICATED** using custom taxonomies
function portfolio_remove_tags($id){
	$this = jQuery("#"+$id);

	$hidden = jQuery('#portfolio_project_tag_hidden');
	$value = $hidden.val();

	$remove_text = $this.text().split(' ');

	$new_value = $value.replace($remove_text[1],'');
	$hidden.val($new_value);

	$this.remove();
}

function casestudies_remove_image($id){ alert('Hai');
	jQuery('.casestudies_uploaded_img_'+$id).remove();

	$hidden_val = jQuery('#upload_image_id_hidden');
	$value = $hidden_val.val();

	$new_value = $value.replace($id,'');
	$hidden_val.val($new_value);
}