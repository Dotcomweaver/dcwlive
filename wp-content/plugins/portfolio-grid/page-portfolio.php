
  <?php
/*
Template Name: portflio
*/
get_header();?>

<!-- <div class="service-sub-menu text-navigation">
 <div class="container">
   <div class="service-menu-list">
   		<h1 class="port_texxt">A Few reasons why we might be the right fit</h1>
   </div>
 </div>

</div> -->

 
<section class="devide">
	
	<section id="portfolio-view" class="clearfix">
		<div class="container">
			<div class="col-sm-3 text-center mobiel_portf">
	           	<?php while(have_posts()): the_post(); $post_id = $post->ID; the_content(); endwhile; ?>
	            <!-- Filter tagss -->
	            <div class="portfolio-filter-view clearfix">
					<?php
						$taxonomies = 'filter_tags';
						$parent_terms = get_terms($taxonomies,array('parent'=>0));
						//echo "<pre>";
						//print_r($parent_terms);
						//echo "<pre>";

						foreach($parent_terms as $key=>$object):
							if($object->name == 'Category'):
								$args = array(
											'orderby'       => 'term_id', 
											'order'         => 'ASC',
											'hide_empty'    => true, 
											'exclude'       => array(), 
											'exclude_tree'  => array(), 
											'include'       => array(),
											'fields'        => 'all', 
											'hierarchical'  => true, 
											'child_of'      => $object->term_id, 
											'pad_counts'    => false, 
											'cache_domain'  => 'core'
										); 
								$terms = get_terms($taxonomies,$args);
								//print_r($terms);
								//$a = get_option('portfolio_tags_settings'); print_r($a);
								$portfolio_tags_settings = json_decode(get_option('portfolio_tags_settings'));
								//print_r($portfolio_tags_settings);
								$portfolio_use_portfolio_filter = $portfolio_tags_settings->portfolio_use_portfolio_filter;
								//print_r($portfolio_use_portfolio_filter);
								if(!empty($portfolio_use_portfolio_filter)):
									if(!empty($terms)): ?>
	                                	<div class="testimonial-category-filter filtertags">
	                                		<a href="/work/" class="termslist active" id='all'>SHOW ALL WORK</a>
	                                    	<ul class="nav nav-pills nav-justified work-nav">
												
												
												<li>SERVICE
												<ul class="dropdown-menu dropdown-menu-right testing">
												<?php
												 	$i = 0;
												 	$temp = array();
												 	foreach($terms as $item):
													 //echo '<pre>';
													//print_r($item);
												 	if($item->slug == 'ecommerce'):
														$temp[0] = $item;
													//elseif($item->slug == 'webapplications'):
														//$temp[3] = $item;
													elseif($item->slug == 'webdevelopment'):
														$temp[2] = $item;
													elseif($item->slug == 'responsivedesign'):
														$temp[1] = $item;
													//elseif($item->slug == 'mobileapplications'):
														//$temp[4] = $item;
														elseif($item->slug == 'seo-sem'):
														$temp[5] = $item;
													endif;
												 endforeach;
												 ksort($temp);
											 	foreach($temp as $item):
												 	//var_dump($item);
													$classname = $item->slug; ?>
												
	                                        		<li>
	                                        			<label>
	                                		 				<input onclick="javascript:getPortfilo('<?php echo $classname;?>')"  type="radio" name="service"> 
	                                        			</label>
		                                        		<a href="javascript:void(0)"
		                                        		 onclick="javascript:getPortfilo('<?php echo $classname;?>')">
	                                        		 		<?php echo str_replace('-', ' ', $item->name);?> 
	                                        			</a> 
	                                        		</li>
	                                            
	                        					<?php endforeach; ?>
	                                    	</ul>
	                                    	</li>
	                                    	</ul>
	                                    </div>
	                        		<?php endif; endif;
								else: ?>
									<div class="clearfix"></div>
					 				<div class="">
	                                 	<div class="p10">
	                                 		<div class="dropdown devide text-left filtertags">
	                                      		<!-- <button class="btn btn-warning" type="button">
		                                        Filter By Industry
	                                        		<span class="glyphicon glyphicon-th"></span>
	                                      		</button> -->
	                                      		<ul>
	                                      		<li> INDUSTRY
	                                      		<ul class="dropdown-menu dropdown-menu-right testing" role="menu" aria-labelledby="dropdownMenu1">
	                                      			<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" class="termslist" rel="showall" id='all'>All</a></li> -->
			                                      	<?php
														$args = array(
															'orderby'       => 'slug', 
															'order'         => 'ASC',
															'hide_empty'    => true, 
															'exclude'       => array(), 
															'exclude_tree'  => array(), 
															'include'       => array(),
															'fields'        => 'all', 
															'hierarchical'  => true, 
															'child_of'      => $object->term_id, 
															'pad_counts'    => false, 
															'cache_domain'  => 'core'
													); 
													$terms = get_terms($taxonomies,$args);
													//$a = get_option('portfolio_tags_settings'); print_r($a);
													$portfolio_tags_settings = json_decode(get_option('portfolio_tags_settings'));
													//print_r($portfolio_tags_settings);
													$portfolio_use_portfolio_filter = $portfolio_tags_settings->portfolio_use_portfolio_filter;
													//print_r($portfolio_use_portfolio_filter);
													if(!empty($portfolio_use_portfolio_filter)):
														if(!empty($terms)):
														 	foreach($terms as $item):
																$classname = $item->slug;
														?>
	 
	    												<li role="presentation">
	    													<label>
			    												<input onclick="javascript:getPortfilo('<?php echo $classname;?>')"  type="radio" name="service">
	    													</label>	
		    												<a 	id="<?php echo $item->term_id; ?>"
		    													href="javascript:void(0)"
	                                        		 			onclick="javascript:getPortfilo('<?php echo $classname;?>')" >
                                        		 			<?php echo str_replace('-', ' ', $item->name);?>
	    													</a>
	    												</li>
	    											<?php endforeach; endif; endif; ?>
	                                      		</ul>
	                                      		</li>
	                                      		</ul>
	                                      	</div>
	                                  	</div>
	                             	</div>
					<?php endif; endforeach; ?>
					<input type="hidden" name="existingRel" id="existingRel" value="" />
				</div>		
	        </div>
	        <!-- end of Filter Tags -->
	        <div class="col-md-9 col-sm-12 port_right">
	    	   <div class="show_cat portfolio portfolio-projects da-thumbs" id="da-thumbs">
				   <ul class="filters_anchors clearfix">
					   <li><a href="/work/">SHOW ALL WORK</a></li>
					   <li class="filter_bt"><a href="javascript:void(0);">FILTER</a></li>
				   </ul>
				   <ul id="mylist">
				<!-- 	<div class="row photo_gallery">
              <div class="owl-carousel_work"> -->
            	<?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 50;
   // global $post;
	//$folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
	//print_r($folio_post_meta);
	//$folio_ids_arr = $folio_post_meta->services_folio;
	//if (count($folio_ids_arr)) :
	
	$args = array(
					//'post__in' => $folio_ids_arr,
					'post_type'=> 'portfolio',
					'order' => 'DESC',
					'post_status' => 'publish',
					'posts_per_page' => -1
				);
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
                    <li>
               <div class="item <?php echo rtrim($posttags," "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h4>
                       
						<p><?php
						// Get a list of terms for this post's custom taxonomy.
						$project_cats = get_the_terms($post->ID, 'keyfeature');
						// Renumber array.
						$project_cats = array_values($project_cats);
						for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
						  <?php  echo $project_cats[$cat_count]->name;if ($cat_count<count($project_cats)-1){echo',';}
						}
						?></p>
                           
                        </div>
                     </div>
                  </div>
               </div></li>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
             wp_reset_postdata();

           // endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?></ul>
          <!-- </div>
          </div> -->
	         	</div>
	         	<div class="filter-content portfolio portfolio-projects"></div>
	        </div>
			<div class="clearfix"></div>
			<div class="work_morelessecbtn visible-xs">
		  		<a href="javaScript:Void(0);" class="btn btn-default dcworng_btn work_moreless">View All</a>
		  		<a href="javaScript:Void(0);" class="btn btn-default dcworng_btn filter_moreless">View All</a>
		  	</div>
	 	</div>   
	</div>  
</div>   
</section>
</section>
<div class="clearfix"></div>
 <style type="text/css">
 	.filtertags .dropdown-menu{display: block;}
 	.port_texxt{text-align: center;text-transform: uppercase;margin: 10px 0;}
 </style>

<script>
function getPortfilo(relation)
{  
	 var existingRel = document.getElementById('existingRel').value;
	 
 	document.getElementById('existingRel').value = relation;
	 
	 var finalRel = document.getElementById('existingRel').value;
	 if(finalRel != ''){
	 	if(finalRel == existingRel){
	 		finalRel = "showall";
	 		//existingRel = '';
	 	}
	 }
	 if(finalRel == '')
	 {
	 	finalRel = "showall";
	 }

	jQuery.post('/wp-admin/admin-ajax.php', { action: "portfolio_ajax_call", rel :finalRel }, 
    function(returnedData){
		returnedData = jQuery.parseJSON(returnedData); 
		var divId = '';
				if(returnedData != '' || returnedData != 'null' || returnedData != NULL)
				{ 
					//console.log(returnedData);
					var count = Object.keys(returnedData.portfolio).length;    
					for (x in returnedData.portfolio) {
						 if( !isNaN(parseInt(x)+1) )
						 	{
					   		var porttitle = returnedData.portfolio[x].title;
					   		var portImage = returnedData.portfolio[x].portfolio_image;
					   		var portlink = returnedData.portfolio[x].link;
					   		var portterms = returnedData.portfolio[x].terms;
					   		divId = divId+'<li>\n'+
							               		'<div class="item">\n'+
							                  		'<div class="pg_dcm_main">\n'+
							                     		'<div class="img_div">\n'+
							                        		'<a href="'+portlink+'">\n'+
							                        			'<img class="img-responive" src="'+portImage+'"/>\n'+
						                        			'</a>\n'+
							                        		'<div class="plus_button">\n'+
							                        			'<a href="'+portlink+'">\n'+
							                        				'<i class="fas fa-plus"></i>\n'+
							                        			'</a>\n'+
							                        		'</div>\n'+
						                        			'<div class="photo_des">\n'+
						                           				'<h4>'+porttitle+'</h4>\n'+
																'<p>'+portterms+'</p>\n'+			                           
						                        			'</div>\n'+
						                     			'</div>\n'+
						                  			'</div>\n'+
						               			'</div>\n'+
											'</li>\n';
								}
						} 
			} 
			// wp_reset_postdata();

			else
			{
				divId = 'NO POSTS';
			}
		document.getElementById('mylist').innerHTML = divId;
	});
}

Array.prototype.remove= function(){
    var what, a= arguments, L= a.length, ax;
    while(L && this.length){
        what= a[--L];
        while((ax= this.indexOf(what))!= -1){
            this.splice(ax, 1);
        }
    }
    return this;
}

</script>

<?php get_footer(); ?>


