<?php

// calling the header.php

get_header();
?>
<style>
.email_templates{display: none;}
.sitefooter{display: none;}

</style>
<link rel="stylesheet" href="/wp-content/themes/dcw/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/wp-content/themes/dcw/css/services.css" type="text/css">
<link rel="stylesheet" type="text/css" href="https://alvarotrigo.com/multiScroll/jquery.multiscroll.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
<div class="portfolio-submenu">
  <div class="container">
     <i class="fa fa-angle-left"></i>
     <?php previous_custom_post(); ?>
      <p class="project"><?php the_field('url');?></p>
      <ui class="portfolio-list" >
        
          <i class="fa fa-tag" aria-hidden="true"></i>  
          <?php
            $project_cats = get_the_terms($post->ID, 'keyfeature');
            // Renumber array.
            $project_cats = array_values($project_cats);
            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
             <li> <?php  echo $project_cats[$cat_count]->name;?></li>
                  <?php  if ($cat_count<count($project_cats)-1){echo'  |  ';}
            }
          ?>
        </li>
      </ui>
    </div>
</div>

<div class="container">
  <div class="social_prt">
    <ul>
      <li>share</li>
      <li><a href="http://www.facebook.com/dotcomweavers" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
      <li><a href="http://twitter.com/Dotcomweavers" target="_blank"><i class="fab fa-twitter"></i></a></li>
      <li><a href="https://www.instagram.com/dotcomweavers/" target="_blank"><i class="fab fa-instagram"></i></a></li>                  
    </ul> 
  </div>
  <ul id="menu">
    <li data-menuanchor="first" class="active"><a href="#first">HIGHLIGHT 1</a></li>
    <li data-menuanchor="second" class=""><a href="#second">HIGHLIGHT 2</a></li>
    <li data-menuanchor="third" class=""><a href="#third">HIGHLIGHT 3</a></li>
     <li data-menuanchor="four" class=""><a href="#four">HIGHLIGHT 4</a></li>
    <li data-menuanchor="five" class=""><a href="#five">Before & After</a></li>
    <li data-menuanchor="six" class=""><a href="#six">Results</a></li>
    <li data-menuanchor="seven" class=""><a href="#seven">Work With Us</a></li>
  </ul>
</div>
<div class="clearfix"></div>
<div class="multiscroll_main">
  <div class="container">
    <div id="multiscroll">
      <div class="ms-left">
        <?php if( have_rows('scroll') ): while( have_rows('scroll') ): the_row(); ?>
          <div class="ms-section">
            <img class="img-responsive" src="<?php the_sub_field('image_scroll'); ?>">
          </div>
        <?php endwhile; endif; ?>  
      </div>

      
      <div class="ms-right">
      <?php if( have_rows('scroll') ): while( have_rows('scroll') ): the_row(); ?>
        <h1 class="inner-port-mob-highlight"><?php the_sub_field('highlight_text');?></h1>
         <img class="img-responsive" src="<?php the_sub_field('image_scroll'); ?>">
        <div class="ms-section">
          <div style="background-color:<?php the_field('color_code');?>" class="projects_pfl_dv">
            <h1><?php the_field('title');?></h1>
           <p> <?php the_sub_field('content'); ?></p>
            <a href="/contact-us/"><button type="button" class="btn">Call To Action</button></a>
            <h6>Category: 
              <?php
                $project_cats = get_the_terms($post->ID, 'keyfeature');
                $project_cats = array_values($project_cats);
                for($cat_count=0; $cat_count<count($project_cats); $cat_count++) { ?>
                <?php  echo $project_cats[$cat_count]->name;?>
                  <?php  if ($cat_count<count($project_cats)-1){echo',';}
                }
              ?>
            </h6>
          </div>
        </div>
        <?php endwhile; endif; ?>
      </div>      

    </div>
  </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
  <script src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
 <script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/vendors/jquery.easings.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/multiscroll.scrollOverflow.limited.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/jquery.multiscroll.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<script type="text/javascript">
       var jq = $.noConflict();
    jq(document).ready(function() {
      
      
      if(jq(window).width()>1024){
        jq('#myContainer').multiscroll({
          //sectionsColor: ['#5ad0ff', '#000', '#ffdd00'],
            anchors: ['first', 'second', 'third', 'four' , 'five'],
            menu: '#menu',
            navigation: true,
            
            normalScrollElements: '.normal, .spacer',
            responsiveWidth: 1025,
            css3: true,
            scrollOverflow: true,
                scrollOverflowKey: 'YWx2YXJvdHJpZ28uY29tXzBqTWMyTnliMnhzVDNabGNtWnNiM2M9QVU3',
                afterResponsive: function(){
                    console.log("after responsive...");
                },
                   navigationTooltips: ['First', 'Second', 'Third' , 'four' , 'five'],

            afterLoad: function(anchor, index){
              if(index==2 || index == 3){
                jq('#infoMenu li a').css('color', '#f2f2f2');
              }
          if(index==1){
            jq('#infoMenu li a').css('color', '#333');
          }
            }
        });
  }
  

      });
    </script>

<?php
get_footer(); ?>
