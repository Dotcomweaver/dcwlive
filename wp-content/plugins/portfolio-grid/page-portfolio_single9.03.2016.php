<?php
    // calling the header.php
    get_header('portfoliowork');
?>
<style>
.affixed-top {
    margin-top: 100px;
}
.footer-contact{margin-top: 40px;}
.body-locked{margin-bottom:0px;}
.inner-left-side{padding-top:0px; }
header{display:none;}
.navbar-default {
    border:none !important;
}
ul.nav.navbar-nav.navbar-right li .glyphicon.glyphicon-menu-hamburger {
    border: 2px solid #434343 !important;
    border-radius: 50%;
    color: #434343!important;
    margin-left: 15px;
    padding: 11px;
    z-index: 9999;
}
.navbar-default .nav > li > a, .navbar-default .nav > li > a:focus {
    color: inherit !important;
}
.total-footer {
    position: static;}
</style>
<?php
$portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
$portfolio_layout_settings = json_decode(get_option('portfolio_layout_settings'));
$portfolio_use_full_width = $portfolio_layout_settings->portfolio_use_full_width;
$portfolio_images_width = $portfolio_images_settings->portfolio_images_width;
$portfolio_images_height = $portfolio_images_settings->portfolio_images_height;
?>
<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post();
$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
?>
<?php
$post_id = $post->ID;
$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
//print_r($portfolio_post_meta);
$imgcount = 1;
if(count($portfolio_image_id_arr) < 3):
?>
<section class="port-inner-header">
    <?php 

foreach( $portfolio_image_id_arr as $attach_img_id ) :
	$attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
	$img_src[] = $attach_folio_img[0];

endforeach;
	
?>
            <div class="portfolio_inner">
             
            	<div class="col-lg-5 col-sm-6 col-xs-12 inner-left-side sidebar">
                	<h3><?php echo the_title(); ?></h3>
                    <p><?php $content = get_the_content($post->ID); echo $content; ?></p>
                    
                   <p class="more">Services provided:</p>
                   <div class="row left_side_icons">
			
				
                                 <?php
$terms = get_the_terms( $post->ID, 'keyfeature' );
if ($terms) {
foreach ($terms as $term) : 

    $colors = apply_filters( 'taxonomy-images-get-terms', '', array(
        'taxonomy' => 'keyfeature',
            'term_args' => array(
                'slug' => $term->slug,
                )
        ) 
    );
    foreach( (array) $colors as $color) :?>
                       <div class=" col-xs-4 col-sm-4 col-lg-4">
                            <p><a href="#"><?php  echo wp_get_attachment_image( $color->image_id, 'full');?></a></p>
                                <span><?php echo $term->name;?></span>
                                </div>
    <?php endforeach;

endforeach;
} else { ?>
 <p>There are no Services</p>
<?php }
?>      
				
			
			
                    
                                                    <?php/*
							$key_features = $portfolio_post_meta->portfolio_key_features;
							//echo nl2br($portfolio_post_meta->portfolio_key_features);
							if( $key_features != "" ){
							$key_features_arr = explode(',',$key_features);
							?>
							<ul class="mbt30">
							<?php
								foreach($key_features_arr as $kfk=>$kfv){
							?>
							<li><?php echo $kfv; ?></li>
							<?php
								}
							?>
							</ul>
							<?php
							}
							*/?>
                    <?php
						if($portfolio_post_meta->portfolio_url != ''):
						$prefix = 'http://';
						$pattern = '^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$^';
						if(preg_match($pattern,$portfolio_post_meta->portfolio_url)){
							$url = $portfolio_post_meta->portfolio_url;
							$display_url = str_replace($prefix,'',$portfolio_post_meta->portfolio_url);
							//$display_url = str_replace('https://','',$portfolio_post_meta->portfolio_url);
						}else{
							$url = $prefix.$portfolio_post_meta->portfolio_url;
							$display_url = $portfolio_post_meta->portfolio_url;
						}
					?>
                    <div class="col-lg-12 col-xs-10 col-sm-10  visit_site-but">
                    <a href="<?php echo $url; ?>" class="btn btn-default" target="_blank">Visit Site <i class="fa fa-long-arrow-right"></i></a>
                </div>
                </div>
                   <div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="port-line-brder">
				<hr>
			</div>
				<div class="row port-lft-page">
					<div class="col-lg-6 col-sm-6 col-xs-6"><?php previous_custom_post(); ?></div>
					<div class="col-lg-6 col-sm-6 col-xs-6 text-right"><?php next_custom_post(); ?></div>
				</div>
		 </div>
                    <?php endif; ?>
                </div>
                    <div class="col-lg-7 col-sm-6 col-xs-12 inner-right-side">
                        <?php if($post->ID == 5248){?>
                        <div class="row">
                          <iframe width="600" height="350" src="https://www.youtube.com/embed/So2cN2ru4RY/?rel=0" frameborder="0" allowfullscreen></iframe>  
                        </div>
                        <?php }else{?>
                        <div class="row">
                                   <div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side">
                                       <img class="img-responsive" alt="" src="<?php echo $img_src[1]; ?>">
                                   </div>
                              
                                <div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side mobile-res">
                                   <img class="img-responsive" alt="" src="<?php echo $img_src[2]; ?>">
                               </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side mobile-res">
                                       <img class="img-responsive" alt="" src="<?php echo $img_src[0]; ?>">
                             </div>
                              
                             
                        </div>
                        <?php }?>

                </div>
           
      
    </div>
    <div class="clearfix">
</div>
</section>
<?php 
else:
foreach( $portfolio_image_id_arr as $attach_img_id ) :
	$attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
	$img_src[] = $attach_folio_img[0];

endforeach;
	
?>
            <div class="portfolio_inner">
             
            	<div class="col-lg-5 col-sm-6 col-xs-12 inner-left-side sidebar">
                	<h3><?php echo the_title(); ?></h3>
                    <p><?php $content = get_the_content($post->ID); echo $content; ?></p>
                    
                   <p class="more">Services provided:</p>
                   <div class="row left_side_icons">
			
                                 <?php
$terms = get_the_terms( $post->ID, 'keyfeature' );
if ($terms) {
foreach ($terms as $term) : 

    $colors = apply_filters( 'taxonomy-images-get-terms', '', array(
        'taxonomy' => 'keyfeature',
            'term_args' => array(
                'slug' => $term->slug,
                )
        ) 
    );
    foreach( (array) $colors as $color) :?>
                       <div class=" col-xs-4 col-sm-4 col-lg-4">
                            <p><a href="#"><?php  echo wp_get_attachment_image( $color->image_id, 'full');?></a></p>
                                <span><?php echo $term->name;?></span>
                                </div>
    <?php endforeach;

endforeach;
} else { ?>
<p>There are no Services </p>
<?php }
?>  
			
                    
                                                    <?php/*
							$key_features = $portfolio_post_meta->portfolio_key_features;
							//echo nl2br($portfolio_post_meta->portfolio_key_features);
							if( $key_features != "" ){
							$key_features_arr = explode(',',$key_features);
							?>
							<ul class="mbt30">
							<?php
								foreach($key_features_arr as $kfk=>$kfv){
							?>
							<li><?php echo $kfv; ?></li>
							<?php
								}
							?>
							</ul>
							<?php
							}
							*/?>
                    <?php
						if($portfolio_post_meta->portfolio_url != ''):
						$prefix = 'http://';
						$pattern = '^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$^';
						if(preg_match($pattern,$portfolio_post_meta->portfolio_url)){
							$url = $portfolio_post_meta->portfolio_url;
							$display_url = str_replace($prefix,'',$portfolio_post_meta->portfolio_url);
							//$display_url = str_replace('https://','',$portfolio_post_meta->portfolio_url);
						}else{
							$url = $prefix.$portfolio_post_meta->portfolio_url;
							$display_url = $portfolio_post_meta->portfolio_url;
						}
					?>
                    <div class="col-lg-6 col-xs-10 col-sm-10  visit_site-but">
                    <a href="<?php echo $url; ?>" class="btn btn-default" target="_blank">Visit Site <i class="fa fa-long-arrow-right"></i></a>
                </div>
                </div>
                   <div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="port-line-brder">
				<hr>
			</div>
				<div class="row port-lft-page">
					<div class="col-lg-6 col-sm-6 col-xs-6"><?php previous_custom_post(); ?><i class="fa fa-angle-left"></i> </div>
					<div class="col-lg-6 col-sm-6 col-xs-6 text-right"><?php next_custom_post(); ?><i class="fa fa-angle-right"></i></div>
				</div>
		 </div>
                    <?php endif; ?>
                </div>
                    <div class="col-lg-7 col-sm-6 col-xs-12 inner-right-side">
                        <?php if($post->ID == 5248){?>
                        <div class="row">
                          <iframe width="600" height="350" src="https://www.youtube.com/embed/So2cN2ru4RY/?rel=0" frameborder="0" allowfullscreen></iframe>  
                        </div>
                        <?php }else{?>
                        <div class="row">
                                   <div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side">
                                       <img class="img-responsive" alt="" src="<?php echo $img_src[1]; ?>">
                                   </div>
                              
                                <div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side mobile-res">
                                   <img class="img-responsive" alt="" src="<?php echo $img_src[2]; ?>">
                               </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side mobile-res">
                                       <img class="img-responsive" alt="" src="<?php echo $img_src[0]; ?>">
                             </div>
                              
                             
                        </div>
                        <?php }?>

                </div>
           
      
    </div>
<div class="clearfix">
</div>
<?php 
endif; 
?>

<!--<section class="light-gray-wraper">
        <div class="container">
            <div class="row divide">
                <div class="col-md-12 text-center">
                    <h1 class="port-title"><?php echo the_title(); ?></h1>
                    <p class="sub-desc">
						<?php $content = get_the_content($post->ID); echo $content; ?>
                     </p>
                </div>
                <div class="clearfix"></div>
                <div class="divide port-inr-hdgs">
                    <div class="col-sm-4 col-sm-6 col-xs-6">
                        <div class="panel panel-default">
                          <div class="panel-heading"><h3>about</h3></div>
                          	<div class="panel-body">
                            	<p><?php echo $portfolio_post_meta->portfolio_about_client;?></p>
                        	</div>
                          </div>
                    </div>
                    
                    <div class="col-sm-4 col-sm-6 col-xs-6">
                        <div class="panel panel-default">
                          <div class="panel-heading"><h3>KEY FEATURES</h3></div>
                          <div class="panel-body">
                            <?php
							$key_features = $portfolio_post_meta->portfolio_key_features;
							//echo nl2br($portfolio_post_meta->portfolio_key_features);
							if( $key_features != "" ){
							$key_features_arr = explode(',',$key_features);
							?>
							<ul>
							<?php
								foreach($key_features_arr as $kfk=>$kfv){
							?>
							<li><?php echo $kfv; ?></li>
							<?php
								}
							?>
							</ul>
							<?php
							}
							?>
                          
                          </div>
                        </div>
                            
                    </div>
                    
                    <div class="col-sm-4 col-sm-6 col-xs-6">
                        <div class="panel panel-default">
                          <div class="panel-heading"><h3>PLATFORMS</h3></div>
                          	<div class="panel-body">
                            <?php
							$port_folio = $portfolio_post_meta->portfolio_technologies;
							//echo $portfolio_post_meta->portfolio_technologies;
							if( $port_folio != "" )
							{
							$port_folio_arr = explode(',',$port_folio);
							?>
							<ul>
							<?php
							foreach($port_folio_arr as $pfk=>$pfv){
							?>
							<li><?php echo $pfv; ?></li>
							<?php
							}
							?>
							</ul>
							<?php
							}
						?>
                         </div>
                        </div>
                    </div>
                </div>
                <?php 
					if($portfolio_post_meta->portfolio_url != ''):
				?>
                <div class="col-md-12 text-center mtop40">
                	<?php
						$prefix = 'http://';
						$pattern = '^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$^';
						if(preg_match($pattern,$portfolio_post_meta->portfolio_url)){
							$url = $portfolio_post_meta->portfolio_url;
							$display_url = str_replace($prefix,'',$portfolio_post_meta->portfolio_url);
							//$display_url = str_replace('https://','',$portfolio_post_meta->portfolio_url);
						}else{
							$url = $prefix.$portfolio_post_meta->portfolio_url;
							$display_url = $portfolio_post_meta->portfolio_url;
						}
					?>
                    <p>Visit Website : <a href="<?php echo $url; ?>" target="_blank"><?php echo $display_url; ?></a></p>
                </div>
                <?php
					endif;
				?>
            </div>
        </div>
</section>-->

<?php
if($portfolio_post_meta->portfolio_linked_case_studies != '')
{ 
	//global $the_query;
	wp_reset_postdata();
	//var_dump($portfolio_post_meta->portfolio_linked_case_studies);
	/*$args = array(
					  'post_type' => 'case-studies', 
					  'ID'	=> $portfolio_post_meta->portfolio_linked_case_studies,
					  'posts_per_page' => 1
				  ); 
	query_posts($args);*/
	$page_object = get_post( $portfolio_post_meta->portfolio_linked_case_studies );
	add_filter($page_object->post_content, 'remove_empty_tags_around_shorcodes');
	/* Removed after changing the Case study page Design */
	//echo $page_object->post_content;
	
?>
<?php } ?>
<?php

if($portfolio_post_meta->portfolio_client_testimonials != '')
{ 
//echo nl2br($portfolio_post_meta->portfolio_key_features);
wp_reset_postdata();
$testimonial_object = get_post( $portfolio_post_meta->portfolio_client_testimonials );
add_filter($testimonial_object->post_content, 'remove_empty_tags_around_shorcodes');
$meta_object = get_post_meta($portfolio_post_meta->portfolio_client_testimonials);
?>
<section class="blue-testi">
    <div class="container text-center">
        <p class="h3">"<?php echo $testimonial_object->post_content; ?>"</p>
        <p>– <?php if(!empty($meta_object['testimonial_client_name'])): echo $meta_object['testimonial_client_name'][0]; endif; ?>, President of <?php if(!empty($meta_object['testimonial_client_company_name'])): echo $meta_object['testimonial_client_company_name'][0]; endif; ?></p>
    </div>
</section>
<?php } ?>
<?php endwhile; else: ?>
        
        <!-- The very first "if" tested to see if there were any Posts to --> 
        <!-- display.  This "else" part tells what do if there weren't any. -->
        <p>Sorry, no posts matched your criteria.</p>
        
        <!-- REALLY stop The Loop. -->
        <?php endif; ?>
       
<?php get_footer(); ?>
