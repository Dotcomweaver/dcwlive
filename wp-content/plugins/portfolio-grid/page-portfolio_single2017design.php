<?php

// calling the header.php

get_header();
?>
<?php
  $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
  $portfolio_layout_settings = json_decode(get_option('portfolio_layout_settings'));
  $portfolio_use_full_width = $portfolio_layout_settings->portfolio_use_full_width;
  $portfolio_images_width = $portfolio_images_settings->portfolio_images_width;
  $portfolio_images_height = $portfolio_images_settings->portfolio_images_height;
?>
<?php

if (have_posts()):
  while (have_posts()):
    the_post();
    $portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
?>

<?php
    $post_id = $post->ID;
    $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
    $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));

    $imgcount = 1;
   
?>

<?php
      if ($portfolio_post_meta->portfolio_url != ''):
        $prefix = 'http://';
        $pattern = '^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$^';
        if (preg_match($pattern, $portfolio_post_meta->portfolio_url)) {
          $url = $portfolio_post_meta->portfolio_url;
          $display_url = str_replace($prefix, '', $portfolio_post_meta->portfolio_url);

          // $display_url = str_replace('https://','',$portfolio_post_meta->portfolio_url);

        }
        else {
          $url = $prefix . $portfolio_post_meta->portfolio_url;
          $display_url = $portfolio_post_meta->portfolio_url;
        }

?>
        
            
  <?php
      endif; ?>               

    <div class="portfolio_inner olddesign">
    <div class="pfi_header">
      <div class="pfi_left">
        <div id="port-carousel-left" data-interval="20000" data-pause="false" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
              <?php 

                $image = get_post_meta($post->ID,'logo_image',true);wp_get_attachment_url( $id );
                $image = wp_get_attachment_url($image);
                if( !empty($image) ): ?>
                             
                <div class="left_img" style="background-image: url('<?php echo $image; ?>');">
                      <div class="slider_content"> <h1 class="more">INTRODUCTION</h1>
                        <?php echo get_post_meta($post->ID,'introduction',true); ?> 
                      </div>
                </div>
                <?php endif; ?>
           
            
          </div>
        </div>  
      </div>
      <div class="pfi_right">
        <?php
          if ($post->ID == 5248) { ?>
            <iframe width="600" height="350" src="https://www.youtube.com/embed/So2cN2ru4RY/?rel=0" frameborder="0" allowfullscreen></iframe>  
            <?php
          }
          else { ?>

          <div id="port-carousel-right" data-interval="4000" data-pause="false" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
             <?php
            
      foreach($portfolio_image_id_arr as $attach_img_id):
        $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
        $img_src[] = $attach_folio_img[0];
      endforeach;
       $i=1;
      foreach($img_src as $img){ ?>
              <div class="item <?php if($i==1){echo 'active';}?>">
                <div class="item-img">
                  <img class="img-responsive" alt="" src="<?php echo $img; ?>">
                </div>
              </div>
     <?php $i++; } ?>  
            </div>


                <?php $active_class1 = "active";?>
                  <ol class="carousel-indicators">
                 
                    <?php  $i=0;
                        foreach($img_src as $img){?>
                        <li data-target="#port-carousel-right" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0){echo 'active';} ?>"></li>
                        <?php $i++;
                        }?>
                 
                  </ol>
                       
          </div>                      
          <?php
        } ?>           
      </div>
    </div>
    </div>

<div class="clearfix"></div>


<div class="container">
<div class="row">
<div class="portfolio_inner_content">
  <div class="col-md-9 col-sm-8">
<?php
    if ($portfolio_post_meta->portfolio_linked_case_studies != '') {
      wp_reset_postdata();
      $page_object = get_post($portfolio_post_meta->portfolio_linked_case_studies);
      add_filter($page_object->post_content, 'remove_empty_tags_around_shorcodes');
?>
<?php
    } ?>
<?php
    if ($portfolio_post_meta->portfolio_client_testimonials != '') {
      wp_reset_postdata();
      $testimonial_object = get_post($portfolio_post_meta->portfolio_client_testimonials);
      add_filter($testimonial_object->post_content, 'remove_empty_tags_around_shorcodes');
      $meta_object = get_post_meta($portfolio_post_meta->portfolio_client_testimonials);
?>
<section class="blue-testi">
    <div class="container text-center">
        <?php echo $testimonial_object->post_content; ?>
        <p><?php if (!empty($meta_object['testimonial_client_name'])):
          echo $meta_object['testimonial_client_name'][0];
        endif; ?>, President of <?php
        if (!empty($meta_object['testimonial_client_company_name'])):
          echo $meta_object['testimonial_client_company_name'][0];
        endif; ?></p>
    </div>
</section>
<?php
    } ?>
<?php
  endwhile; 
else: ?>
        <p>Sorry, no posts matched your criteria.</p>        
        <?php
endif;  ?>
        <!-- <div class="breadcrumb" style="margin-bottom: 5px;">  
          <h1><?php //if (function_exists('yoast_breadcrumb')) {
           // yoast_breadcrumb('<ol class="breadcrumb"><a href="/work">Portfolio</a> : ', '</ol>');
        //  } ?></h1>
        </div> -->
                 <!--  <h3><?php// echo the_title(); ?></h3> -->
                <?php if(have_posts()) : ?>
                <?php while(have_posts()) : the_post(); ?>
                  <h4 class="title-head text-uppercase">THEIR CHALLENGE</h4>
                  <p class="default-text"><?php $content = get_the_content($post->ID); echo $content; ?></p>
                  <br class="visible-lg" /><br class="visible-lg" />
                  <h4 class="title-head text-uppercase"> <strong>OUR SOLUTION</strong></h4>
                  <p  class="default-text 123">
                    <strong>
                    <?php echo get_post_meta($post->ID,'our_solution',true); ?>
                    </strong>
                  </p>
                <?php endwhile; endif; ?>

    </div>
    <div class="col-md-3 col-sm-4">

                  <h4 class="title-head text-right text-uppercase">HOW WE HELPED</h4>
      
                
                  <?php
                    $terms = get_the_terms($post->ID, 'keyfeature');
                    ?>
                      <ul class="what-we-did">
                    <?php if ($terms) {
                      foreach($terms as $term):
                        $colors = apply_filters('taxonomy-images-get-terms', '', array(
                          'taxonomy' => 'keyfeature',
                          'term_args' => array(
                            'slug' => $term->slug,
                          )
                        ));
                        foreach((array)$colors as $color): ?>
                        <li><?php echo $term->name; ?></li>
                      <?php
                        endforeach;
                  endforeach;
                  ?>
                  </ul>


             <?php }
              else { ?>
                


<p>There are no Services </p>
<?php
      }

?>  
      
<?php
      if ($portfolio_post_meta->portfolio_url != ''):
        $prefix = 'http://';
        $pattern = '^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$^';
        if (preg_match($pattern, $portfolio_post_meta->portfolio_url)) {
          $url = $portfolio_post_meta->portfolio_url;
          $display_url = str_replace($prefix, '', $portfolio_post_meta->portfolio_url);
        }
        else {
          $url = $prefix . $portfolio_post_meta->portfolio_url;
          $display_url = $portfolio_post_meta->portfolio_url;
        }

?>

          <a href="<?php echo $url; ?>" class="btn btn-visit" target="_blank">Visit Site </a>

        
        

     
  <?php
      endif; ?>
    </div>
    </div>
    </div>
 </div> 
<div class="portfolio_inner_footer text-center">
  <div class="container">
    <div class="col-sm-4">
      <?php previous_custom_post(); ?>
    </div>
    <div class="col-sm-4"><a href="/work/">  BACK TO OUR WORK</a></div>
    <div class="col-sm-4">
          <?php next_custom_post(); ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.carousel').carousel({
    
  })
</script>
<?php
get_footer(); ?>
