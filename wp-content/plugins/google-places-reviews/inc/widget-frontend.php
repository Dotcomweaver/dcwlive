<?php
/**
 *  Widget Frontend Display
 *
 * @description: Responsible for the frontend display of the Google Places Reviews
 * @since      : 1.0
 */

//Handle website link
$website = isset( $response['result']['website'] ) ? $response['result']['website'] : '';
if ( ! isset( $response['result']['website'] ) || empty( $response['result']['website'] ) ) {
	//user g+ page since they have no website
	$website = isset( $response['result']['url'] ) ? $response['result']['url'] : '';
}
$place_avatar = isset( $response['place_avatar'] ) ? $response['place_avatar'] : GPR_PLUGIN_URL . '/assets/images/default-img.png';
?>

	<div class="gpr-<?php echo sanitize_title( $widget_style ); ?>">

		<?php

		//Business Information
		if ( $hide_header !== '1' ) {
			?>

			<div class="gpr-business-header gpr-clearfix">

				<div class="gpr-business-avatar" style="background-image: url(<?php echo $place_avatar; ?>)"></div>

				<div class="gpr-header-content-wrap gpr-clearfix">
					<span class="gpr-business-name"><a href="<?php echo $website; ?>" title="<?php echo $response['result']['name']; ?>" <?php echo $target_blank . $no_follow; ?>><span><?php echo $response['result']['name']; ?></span></a></span>

					<?php
					//Overall rating for biz display:
					$overall_rating = isset( $response['result']['rating'] ) ? $response['result']['rating'] : '';
					if ( $overall_rating ) {
						echo $this->get_star_rating( $overall_rating, null, $hide_out_of_rating, $hide_google_image );
					} //No rating for this biz yet:
					else {
						?>

						<span class="no-reviews-header"><?php
							$googleplus_page = isset( $response['result']['url'] ) ? $response['result']['url'] : '';
							echo sprintf( __( '<a href="%1$s" class="leave-review" target="_blank" class="new-window">Write a review</a>', 'gpr' ), esc_url( $googleplus_page ) ); ?></span>

					<?php } ?>


				</div>
			</div>

		<?php } ?>


		<?php
		//Google Places Reviews
		if ( isset( $response['gpr_reviews'] ) && ! empty( $response['gpr_reviews'] ) ) {
			?>
            <div class="header-group">
            <?php if(ICL_LANGUAGE_CODE=='en'){?>
           <h4>Exceeding<span>Expectations</span></h4>
          <p>Our team keeps pace with changing technology so they can deliver outstanding solutions for our clients. Here’s what some clients said about us:</p>
         <?php }else{?>
          <h4><span>  تجاوز التوقعات  </span></h4>
          <p>يقوم فريقنا بمواكبة التكنولوجيا المتغيرة لذلك فإنهم يمكنهم تقديم خيارات رائعة لعملائنا. وهذا هو بعض ما قاله عنا بعض عملائنا:</p>
      		<?php }?>
      	</div>
        <div class="clearfix"></div>
           

            <div class="row hidden-xs">
				<?php
				$counter = 0;
				$review_limit = isset( $review_limit ) ? $review_limit : 5;
				//Loop Google Places reviews
				foreach ( $response['gpr_reviews'] as $review ) {

					//Set review vars
					$author_name    = $review['author_name'];
					$author_url     = isset( $review['author_url'] ) ? $review['author_url'] : '';
					$overall_rating = $review['rating'];
					$review_text    = $review['text'];
					$time           = $review['time'];
					$avatar         = isset( $review['avatar'] ) ? $review['avatar'] : GPR_PLUGIN_URL . '/assets/images/mystery-man.png';
					$review_filter  = isset( $review_filter ) ? $review_filter : '';
					

	
					//Review filter set OR count limit reached?
					if ( $overall_rating >= $review_filter && $overall_rating >=4 && $counter < $review_limit ) {
						?>
                       <div class="col-sm-4">
            				<a href="https://www.google.com/#q=dotcomweavers&lrd=0x89c2e52cd3dc44e9:0x49c0693ca393e2bd,1," target="_blank"> <div class="box">						
                           	
                               <p class="text-center"><img src="<?php bloginfo('template_url');?>/images/Review-Stars.png" class="img-responsive"> <span> - <?php echo $author_name; ?> </span></p>
                               <p>"<?php echo wp_trim_words( wpautop( $review_text ), 20,  null ); ?>"</p>
					   </div></a>
                    	</div>

					<?php $counter ++; } //endif review filter ?>

				<?php } //end review loop	?>
				
			</div><!--/.gpr-reviews -->
			 <div class="row visible-xs">
			 	<div class="review_carousel">
    						<div class="owl-carousel owl-theme" id="review_carousel">
				<?php
				$counter = 0;
				$review_limit = isset( $review_limit ) ? $review_limit : 5;
				//Loop Google Places reviews
				foreach ( $response['gpr_reviews'] as $review ) {?>
				

					<?php //Set review vars
					$author_name    = $review['author_name'];
					$author_url     = isset( $review['author_url'] ) ? $review['author_url'] : '';
					$overall_rating = $review['rating'];
					$review_text    = $review['text'];
					$time           = $review['time'];
					$avatar         = isset( $review['avatar'] ) ? $review['avatar'] : GPR_PLUGIN_URL . '/assets/images/mystery-man.png';
					$review_filter  = isset( $review_filter ) ? $review_filter : '';?>
					
					<?php //Review filter set OR count limit reached?
					if ( $overall_rating >= $review_filter && $overall_rating >=4 && $counter < $review_limit ) {
						?>
						
                      <div class="col-sm-4">
            				<a href="https://www.google.com/#q=dotcomweavers&lrd=0x89c2e52cd3dc44e9:0x49c0693ca393e2bd,1," target="_blank"> <div class="box">						
                           	<?php// echo $this->get_star_rating( $overall_rating, $time, $hide_out_of_rating, false ); ?>
                               <p class="text-center"><img src="<?php bloginfo('template_url');?>/images/Review-Stars.png" class="img-responsive"> <span> - <?php echo $author_name; ?> </span></p>
                               <p>"<?php echo wp_trim_words( wpautop( $review_text ), 20,  null ); ?>"</p>
					   </div></a>
                    	</div>
				
					<?php $counter ++; } //endif review filter ?>
					

				<?php } //end review loop	?>
			
				</div></div>
			</div><!--/.gpr-reviews -->
			
            <div class="clearfix"></div>
		<?php
		} //end review if
		else {
			//No Reviews for this location
			?>

			<div class="gpr-no-reviews-wrap">
				<p class="no-reviews"><?php
					$googleplus_page = isset( $response['result']['url'] ) ? $response['result']['url'] : '';
					echo sprintf( __( 'There are no reviews yet for this business. <a href="%1$s" class="leave-review" target="_blank">Be the first to review</a>', 'gpr' ), esc_url( $googleplus_page ) ); ?></p>

			</div>

		<?php } ?>


	</div>


<?php
//after widget
echo ! empty( $after_widget ) ? $after_widget : '</div>';
?>
<script type="text/javascript">
  var jQ = jQuery.noConflict();
  jQ(document).ready(function() {
  jQ('#review_carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
     })
 </script>
