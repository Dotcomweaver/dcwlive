<?php
/**
* The main template file.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Eleven
*/
echo "Suneel";
get_header(); ?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					} ?>
                  <span class="fa fa-cog"></span>
               	  <h1><?php the_title(); ?></h1>
                  <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta;  ?>
              </div>
          </div>
        </div>
</section>
<section class="light-gray-wraper divider">
  <div class="container" id="container-blog">
		<?php 
		if ( have_posts() ) : ?>
        	<div id="posts" class="row">
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
						
					<?php the_content(); ?>
			<?php 	
				endwhile; ?>

			<?php //twentytwelve_content_nav( 'nav-below' ); ?>
		</div><!-- #content -->
        <?php else : ?>
			<div class="row" id="posts">
			<article id="post-0" class="post no-results not-found">

			<?php if ( current_user_can( 'edit_posts' ) ) :
				// Show a different message to a logged-in user who can add posts.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'No posts to display', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'twentytwelve' ), admin_url( 'post-new.php' ) ); ?></p>
				</div><!-- .entry-content -->

			<?php else :
				// Show the default message to everyone else.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'twentytwelve' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			<?php endif; // end current_user_can() check ?>

			</article><!-- #post-0 -->
			</div>
		<?php endif; // end have_posts() check ?>
	</div><!-- #primary -->
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>