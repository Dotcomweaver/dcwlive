<?php 

add_shortcode('qa', 'qahome_shortcode');

/* Define the shortcode function */

function qahome_shortcode( $atts ) {
	
	global $qaplus_options, $catname;
	$catname = (get_query_var('category_name'));
	
	STATIC $i = 0;

	$qaplus_shortcode_output = '';
	
	extract(shortcode_atts(array(
		'id' => '',
		'cat' => '',
		'limit' => $qaplus_options['limit'],
		'search' => $qaplus_options['search'],
		'searchpos' => $qaplus_options['searchpos'],
		'catlink' => $qaplus_options['catlink'],
		'postnumber' => $qaplus_options['postnumber'],
		'excerpts' => $qaplus_options['excerpts'],
		'permalinks' => $qaplus_options['permalinks'],
		'accordion'	=> $qaplus_options['accordion'],
		'collapsible'	=> $qaplus_options['collapsible'],
		'sort' => 'menu_order',
		'animation' => $qaplus_options['animation'],
		'exclude' => '',
		'orderby' => 'date',
	), $atts));
	
	$args = array(
		'orderby' => $orderby,
		'exclude' => $exclude,
		'taxonomy' => 'faq_category',
	);
	
	if ( $id ) { // Show a single entry //

		$qaplus_shortcode_output .= '<div class="qa-faqs qa-single cf';
			
			switch( $animation ) {
				
				case 'none' :
				$qaplus_shortcode_output .= ' animation-none';
				break;
				
				case 'slide' :
				$qaplus_shortcode_output .= ' animation-slide';
				break;
				
				default :
				$qaplus_shortcode_output .= ' animation-fade';
				break;
			}	

		if ( $accordion == "true" )	$qaplus_shortcode_output .= ' accordion';

		if ( $collapsible == "true" ) $qaplus_shortcode_output .= ' collapsible';
		 						
		$qaplus_shortcode_output .= '">
		';
		
		$args = array(
			'p'	=> $id,
			'post_type'     => 'qa_faqs',
			'post_status'   => 'publish',
			'posts_per_page' => 1,
		);
				
		$qa_faqs = new WP_Query( $args );
		
		while( $qa_faqs->have_posts() ): $qa_faqs->the_post();
			
			global $post;
			$qaplus_shortcode_output .= '<div id="qa-faq' . $i . '" class="qa-faq">
			';
			$qaplus_shortcode_output .= '<h3 class="qa-faq-title"><a class="qa-faq-anchor" href="' . get_permalink() . '">'. get_the_title().'</a></h3>
			';

			$qaplus_shortcode_output .= '<div class="qa-faq-answer">' . apply_filters( 'the_content', get_the_content() );

			if ( $permalinks == "true" ) { 
				$qaplus_shortcode_output .= '<p class="qa-faq-meta qa-post-like">';
			}

			if ( $permalinks == "true" ) $qaplus_shortcode_output .= '<a class="qa-permalink" href="' . get_permalink() . '">' . __( 'Permalink' , 'qa-plus') . '.</a>';

			if ( $permalinks == "true" ) $qaplus_shortcode_output .= '</p>';

			$qaplus_shortcode_output .= '</div><!--.qa-faq-answer-->
			</div><!--.qa-faq-->
			';

		$i++;	

		endwhile; // end loop

		$qaplus_shortcode_output .= '</div><!--.qa-faqs -->';

		wp_reset_postdata();

	} elseif ( $catname || $cat ) { // Show a single category //

		if ( $cat ) {
			$catname = $cat;
		}

		/*if ( $searchpos == "top" ) { 
			if ( $search == "category" || $search == "both" ) $qaplus_shortcode_output .= qa_search();
		}*/

		$category = get_term_by( 'slug', $catname, 'faq_category' );

		$qaplus_shortcode_output .= '<div id="posts"  class="qa-faqs divide qa-category cf';
			
			switch( $animation ) {
				
				case 'none' :
				$qaplus_shortcode_output .= ' animation-none';
				break;
				
				case 'slide' :
				$qaplus_shortcode_output .= ' animation-slide';
				break;
				
				default :
				$qaplus_shortcode_output .= ' animation-fade';
				break;
			}	

		if ( $accordion == "true" )	$qaplus_shortcode_output .= ' accordion';

		if ( $collapsible == "true" ) $qaplus_shortcode_output .= ' collapsible';
		 						
		$qaplus_shortcode_output .= '">';

		/*$qaplus_shortcode_output .=  '<div class="qa-category four columns">
			<h2 class="faq-catname">' . $category->name . '</h2>
			';	*/

		$args = array(
			'order'         => 'DESC',
			'orderby' 		=> 'date',
			'post_type'     => 'qa_faqs',
			'post_status'   => 'publish',
			'posts_per_page' => 6,
			//'posts_per_page=6',
			'faq_category'	=> $category->slug
		);
		$qa_faqs = new WP_Query( $args );
		while( $qa_faqs->have_posts() ): $qa_faqs->the_post();
			global $post;
			$post_date = get_the_date('M j, Y');
			$author = get_the_author();
			$qaplus_shortcode_output .= '<div class="item col-md-4" id="post-"'.$i.'">
													<div class="blog-box">
														<div class="qa-blog-box-inner">
															<div class="row">
																<div class="col-xs-6 col-md-12 col-lg-6"><span class="icomoon-icon-clock "></span>'.$post_date.'</div>
																<div class="col-xs-6 col-md-12 col-lg-6 text-right"><span class="icomoon-icon-user"></span>'.$author.'</div>
																</div><hr>';
			/*$qaplus_shortcode_output .= '<h3 class="qa-faq-title"><a class="qa-faq-anchor" href="' . get_permalink() . '">'. get_the_title().'</a></h3>
			';*/
			$title = get_the_title();
			$qaplus_shortcode_output .= '<h2><a class="qa-faq-anchor" href="' . get_permalink() . '" rel="bookmark">'. $title.'</a></h2>';
			$content =  wp_trim_words(get_the_content(), 35); 
			$qaplus_shortcode_output .= '<div class="info">'.$content.'</div>';
			
			/*if ( $permalinks == "true" ) $qaplus_shortcode_output .= '<p class="qa-faq-meta qa-post-like">';

			if ( $permalinks == "true" ) $qaplus_shortcode_output .= '<a class="qa-permalink" href="' . get_permalink() . '">' . __( 'Permalink' , 'qa-plus') . '.</a>';

			if ( $permalinks == "true" ) $qaplus_shortcode_output .= '</p>';*/

			$qaplus_shortcode_output .= '<a class="blog-but" href="'.get_permalink().'" rel="bookmark" title="Permanent Link to '.the_title_attribute('echo=0').'">Continue Reading »</a></div><!--.bloginner --></div><!-- blogbox --></div> <!-- col-4 div -->';

		$i++;	
		endwhile; // end loop
		$qaplus_shortcode_output .= '</div><!-- posts-->';
		if(function_exists('wp_paginate')) 
		{
				  $qaplus_shortcode_output .= wp_paginate();
		 }
		wp_reset_postdata();
		if ( $searchpos == "bottom" ) { 
			if ( $search == "category" || $search == "both" ) $qaplus_shortcode_output .= qa_search();
		}

	}
	
	else {
			$args = array(
							'orderby'	=> 'date',
							'order'	=> 'DESC',
							'taxonomy'	=> 'faq_category'
						);

		$categories = get_categories( $args );	
		if ( $categories ) {
			$qaplus_shortcode_output .= '<div id="posts" class="qa-faqs divide qa-home cf';
			
			switch( $animation ) {
				
				case 'none' :
				$qaplus_shortcode_output .= ' animation-none';
				break;
				
				case 'slide' :
				$qaplus_shortcode_output .= ' animation-slide';
				break;
				
				default :
				$qaplus_shortcode_output .= ' animation-fade';
				break;
			}	

			if ( $accordion == "true" )	$qaplus_shortcode_output .= ' accordion';

			if ( $collapsible == "true" ) $qaplus_shortcode_output .= ' collapsible';
		 						
			$qaplus_shortcode_output .= '">';
			
			foreach ( $categories as $category ) {
					
				if ( $sort == 'menu_order' ) {
					$args = array(
						'order'         => 'DESC',
						'orderby' 		=> 'date',
						'post_type'     => 'qa_faqs',
						'post_status'   => 'publish',
						'posts_per_page' => $limit,
						'faq_category'	=> $category->slug
					);
				} else {
					$args = array(
						'meta_key'	=> 'votes_count',
						'order'         => 'DESC',
						'orderby' 		=> 'date',
						'post_type'     => 'qa_faqs',
						'post_status'   => 'publish',
						'posts_per_page' => $limit,
						'faq_category'	=> $category->slug
						
					);
				}
				$qa_faqs = new WP_Query( $args );
				//print_r($qa_faqs);
				
				while( $qa_faqs->have_posts() ): $qa_faqs->the_post();
					global $post;
					$post_date = get_the_date('M j, Y');
					$author = get_the_author();
					$qaplus_shortcode_output .= '<div class="item" id="post-"'.$i.'">
													<div class="blog-box">
														<div class="qa-blog-box-inner">
															<div class="row">
															</div>
																<div class="row">
																<div class="col-sm-4">
																<div class="kb-img">
															<a href="'.get_permalink().'"><img src="/wp-content/themes/dcw/images/logo_slogan.jpg" class="img-responsive"></a>
														</div>
																</div> <div class="col-sm-8"><div class="post-meta"> ';
					
														
					$title = get_the_title();
					$post_type = 'qa_faqs';
                    $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
                    


                     foreach( $taxonomies as $taxonomy ) :
                   // Gets every "category" (term) in this taxonomy to get the respective posts
                    $terms = get_terms( $taxonomy );
                	 foreach( $terms as $term ) :

                     $qaplus_shortcode_output .=    ' ' . $term->name .' | ' ;
                                       
                endforeach;
                endforeach;
               
		     	

					$qaplus_shortcode_output .= '' . $post_date .'</div>';
					$qaplus_shortcode_output .='<h2><a class="qa-faq-anchor" href="' . get_permalink() . '" rel="bookmark">'. $title.'</a></h2>';
					if ( $excerpts == "true" ) {
						$content =  wp_trim_words(get_the_content(), 35); 
						$qaplus_shortcode_output .= '<div class="info">' . $content .'</div>';
					} else { 
						$content = wp_trim_words(get_the_content(), 35);
						$qaplus_shortcode_output .= '<div class="info">' .$content.'</div>';
					}
					$qaplus_shortcode_output .= '<a class="blog-but" href="'.get_permalink().'" rel="bookmark" title="Permanent Link to '.the_title_attribute('echo=0').'">Continue Reading »</a>
					</div>
					</div>
					</div><!--.bloginner -->
					</div><!-- blogbox -->
					</div> <!-- col-4 div -->';
					$i++;
				endwhile;
			}
			$qaplus_shortcode_output .= '</div><!-- posts-->';
			if(function_exists('wp_paginate')) 
			{
                      $qaplus_shortcode_output .= wp_paginate();
             }
		}else { // no categories, just home
		
			$qaplus_shortcode_output .= '<div class="qa-faqs qa-home cf';
			
			switch( $animation ) {
				
				case 'none' :
				$qaplus_shortcode_output .= ' animation-none';
				break;
				
				case 'slide' :
				$qaplus_shortcode_output .= ' animation-slide';
				break;
				
				default :
				$qaplus_shortcode_output .= ' animation-fade';
				break;
			}	

			if ( $accordion == "true" )	$qaplus_shortcode_output .= ' accordion';

			if ( $collapsible == "true" ) $qaplus_shortcode_output .= ' collapsible';
		 					
			$qaplus_shortcode_output .= '">
			';

			if ( $searchpos == "top" ) { 
				if ( $search == "home" || $search == "both" ) $qaplus_shortcode_output .= qa_search();
			}

			$catheader = $category->name;	
			if ( $postnumber == "true" ) {
				$catheader .= ' (' . $category->count . ')';
			} 


			$args = array(
				'order'         => 'DESC',
				'orderby' 		=> 'date',
				'post_type'     => 'qa_faqs',
				'post_status'   => 'publish',
				'posts_per_page' => 6,
			);

			$qa_faqs = new WP_Query( $args );
			
			while( $qa_faqs->have_posts() ): $qa_faqs->the_post();
				
				global $post;
				$qaplus_shortcode_output .= '<div id="qa-faq' . $i . '" class="qa-faq cf">
				';
				$qaplus_shortcode_output .= '<h3 class="qa-faq-title"><a class="qa-faq-anchor" href="' . get_permalink() . '">'. get_the_title().'</a></h3>
				';
				
				if ( $excerpts == "true" ) {
					$qaplus_shortcode_output .= '<div class="qa-faq-answer">' . apply_filters( 'the_content', get_the_excerpt() );
				} else { 
					$qaplus_shortcode_output .= '<div class="qa-faq-answer">' . apply_filters( 'the_content', get_the_content() );
				}

				if ( $permalinks == "true" ) $qaplus_shortcode_output .= '<p class="qa-faq-meta qa-post-like">';

				if ( $permalinks == "true" ) $qaplus_shortcode_output .= '<a class="qa-permalink" href="' . get_permalink() . '">' . __( 'Permalink' , 'qa-plus') . '.</a>';

				if ( $permalinks == "true" ) $qaplus_shortcode_output .= '</p>';

				$qaplus_shortcode_output .= '</div><!--.qa-faq-answer-->
				</div><!--.qa-faq-->
				';

				$i++;
	
			endwhile; // end loop
				
		$qaplus_shortcode_output .= '</div><!--.qa-faqs -->';

			if ( $searchpos == "bottom" ) { 
				if ( $search == "home" || $search == "both" ) $qaplus_shortcode_output .= qa_search();
			}

		}
	}	

	wp_reset_postdata();

	return $qaplus_shortcode_output;	
}

// functions and hooks go here 

function qa_search() { 
	global $qaplus_options;

    $searchform = '<form role="search" method="get" id="qaplus_searchform" action="' . home_url() . '">
		<input type="text" value="" placeholder="' . __('Search', 'qa-plus') . '" name="s" id="qasearch" class="qaplus_search" />
		<input type="hidden" name="search_link" id="qa_search_link" value="' . home_url() . '/' . $qaplus_options['faq_slug'] . '/search/"/>
		<input type="submit" id="qaplus_searchsubmit" value="Search" />
		</form>';

	return $searchform;
}