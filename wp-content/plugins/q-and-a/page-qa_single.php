<?php
/**

 * The Template for displaying all single posts.

 *

 * @package WordPress

 * @subpackage Twenty_Eleven

 * @since Twenty Eleven 1.0

 */
get_header();
?>
<div class="ecommerce clearfix">
			<div class="header-content">
				<div class="header-content-inner">
<!--				 <h1>About Us</h1>-->
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="https://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_blog.png"></div>
<section class="light-gray-wraper divider kb-single">
    <div class="container" id="container-blog">
        <div class="row">
            <div class="col-md-12">
                <!-- blog-filter -->
                <div class="row">
                    <div class="col-md-9">
                        <?php if (have_posts()):while (have_posts()) : the_post(); ?>
                                <div class="blog-box">                
                                    <div class="blog-box-inner ind-blog-page">
                                        <div class="row">
                                            <div class="col-sm-6"><span class="icomoon-icon-clock "></span><?php the_date('M j, Y'); ?></div>
                                            <div class="col-sm-6 text-right"><span class="icomoon-icon-user"></span><?php the_author(); ?></div>
                                        </div>
                                        <hr>
                                        <div class="info">
                                         <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                    <div class="col-md-4 ind-blog-4">
                        <div class="blog_sidebar">
  <?php dynamic_sidebar('search');?>

  <div class="clearfix"></div>
  <div class="sidebar_header">
      <h4> Categories </h4>
      <hr />
  </div>
     <ul id="tags-list">
                <?php

                $post_type = 'qa_faqs';
                 
                // Get all the taxonomies for this post type
                $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
                 
                foreach( $taxonomies as $taxonomy ) :
                 
                    // Gets every "category" (term) in this taxonomy to get the respective posts
                    $terms = get_terms( $taxonomy );

                 
                    foreach( $terms as $term ) :
                         echo '<li><a href="' . get_tag_link ($term->name) . '" rel="tag">' . $term->name . '  </a></li>';
                     ?>
                   
                <?php endforeach;
                endforeach;?>
                </ul>

    <div class="clearfix"></div>
    
    <div class="sidebar_header">
        <h4>Instagram</h4>
        <hr />
    </div>
        <div class="clearfix"></div>
        <div class="instagram_gallery">
            <?php do_shortcode('[wp-instagram-gallery]');?>
        </div>

    <div class="clearfix"></div>
    <br />
    <div class="blog_video_gallery">
        <div class="sidebar_header">
            <h4>Video Gallery</h4>
            <hr />
        </div>

        
    <?php
$queryObject = new WP_Query( 'post_type=video-testimonials&posts_per_page=3' );
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul>
    <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        $post_id = $post->ID;
        
$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
if($v_test_post_meta->vt_url != '') {
                    
                    $y_img = get_ytube_video_code($v_test_post_meta->vt_url);
                    $te = $v_test_post_meta->vt_url;
                    $start = strpos($te,"v=")+2;
                    $end = strpos($te,"&");
                    if( $end == "" || $end == 0 ){
                        $fstr = substr($te,$start);
                    } else {
                        $length = $end-$start;
                        $fstr = substr($te,$start,$length);
                    }
                    //print_r($v_test_post_meta);
                     ?>
                        <li>
                     <?php if($v_test_post_meta->vt_url != ''):?>
                            <div class="video_img">
                            <img  alt="<?php the_title() ?> - Testimonial for NJ Web Design company" class="img-responsive" src="https://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" >
                             <!-- Play Button -->
                            <a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/<?php echo trim($fstr); ?>" class="vt_play2" href="javascript:void();">
                                <span class="flaticon-play-button4"></span>
                            </a>
                            </div>
                        <?php endif; ?>
                            <!-- <a href="<?php //the_permalink(); ?>"> --><?php the_title(); ?><!-- </a> -->
                         </li>
    <?php
    }
}
    ?>
    </ul>
    
    <?php
}
?>
<div class="clearfix"></div>
        <a href="/video-testimonials/" class="btn btn-default">View All</a>
    </div>


    <div class="clearfix"></div>
    <br />

    <div class="sidebar_social">
        <div class="sidebar_header">
            <h4>Social</h4>
            <hr />
        </div>

        <ul class="social_icons">
            <li> <a href="https://www.facebook.com/dotcomweavers" target="_blank" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
            <li> <a href="http://twitter.com/Dotcomweavers" target="_blank" class="twitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
            <li> <a href="https://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="linkedin"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
            <!-- <li> <a href="https://www.pinterest.com/dotcomweaversnj/" target="_blank" class="pinterest"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li> -->
            <li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="google-plus"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
        </ul>
    </div>

    </div>
  
  </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--.row-->
<?php get_footer(); ?>