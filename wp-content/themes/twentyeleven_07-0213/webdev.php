<?php
/**
 * Template Name: Webdevelopment
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
    
    <?php /*?><div class="four columns sidebar">	
    <?php if ( ! dynamic_sidebar( 'leftside' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
    </div><?php */?>	<!--four columns sidebar-->
   
    
    <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'content', 'page' ); ?>
    
    <?php //comments_template( '', true ); ?>
    
    <?php endwhile; // end of the loop. ?>
    
<!--slide scripts starts--> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/myjs.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.scrollTo.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.nav.min.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery_005.js"></script> 

<script type="text/javascript">
$(document).ready(function() {
$('.jump').onePageNav();
//$("#header").sticky({ topSpacing: 0, className: 'sticky', wrapperClassName: 'my-wrapper'});
});

/*$('.jump_ref').live('click', function(){
$('.jump_target').removeClass('port_padding');
ref = $(this).attr('href');
$(ref).addClass('port_padding');
});
*/
$(".port_btn ").live('click', function(){
$("#portfolio").addClass("port_padding");
});
</script>

<?php get_footer(); ?>