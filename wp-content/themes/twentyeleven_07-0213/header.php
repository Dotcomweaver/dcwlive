<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/skeleton.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/flexslider.css" />

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.sticky.js"></script> <!--jquery_sticky-->
<script type="text/javascript">
$(document).ready(function() {
//$('.navigation').onePageNav();
$("#header").sticky({ topSpacing: 0, className: 'sticky', wrapperClassName: 'my-wrapper'});
});
</script> 

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>

<!-- header wrapper starts here--> 
<div class="wrapper" id="header">
<header  role="banner">
<div class="container">
<div class="header clearfix row">
<div class="eight columns header_left"> 
<div><a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div> 
</div>
<!--hometop contact widget starts-->
<div class="eight columns">
<div class="header_right">
<div class="header_contacts clearfix">
<?php if ( ! dynamic_sidebar( 'contact_info' ) ) : ?>
<?php endif; // custom widget ?>
</div>	<!--header_contacts clearfix-->
</div><!--header_right-->
</div>	<!--line_circ-->
<!--hometop contact widget ends-->

</div>
<!--header clearfix row--> 
</div>

<!-- Main Navigation -->
<div class="row no_bm">
<div class="dark_menu sixteen columns">
<div class="container">

<nav id="access" role="navigation">
<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
<?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'div',
'container_class' => 'menu','menu' => 'topmenu','menu_class'      => 'menu' ) ); ?>
</nav>


</div><!-- .container -->
</div>	<!--dark_menu sixteen columns-->
</div>	<!--row no_bm-->

<select id="select_menu" onChange="location = this.value">
<option value="">Navigation</option>
<option value="">About Us</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Home Version 1</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Home Version 2</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Home Version 3</option>
<option value="">Services</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Services</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;About Us</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Left Sidebar Page</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Right Sidebar Page</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Search Results</option>
<option value="">&nbsp;&nbsp;&nbsp;&nbsp;Contact Us</option>
<option value="">Portfolio</option>
<option value="">Portfolio</option>
<option value="">Contact Us</option>
</select>

<!-- Main Navigation::END --> 
</header>	<!-- #header -->
</div>	<!--sticky_header-->
<!-- header wrapper ends here--> 

<!--  Fonts  --> 
<script type="text/javascript">
WebFontConfig = {
google: { families: ["Open Sans","Oxygen"] }
};
(function() {
var wf = document.createElement('script');
wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
wf.type = 'text/javascript';
wf.async = 'true';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(wf, s);
})();
</script> 