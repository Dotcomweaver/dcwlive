<?php
/**
 * Template Name: HomePage
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

    <div class="container clearfix" id="main_section">
    <div class="line_circ">
    <?php if ( ! dynamic_sidebar( 'intro_side' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
    </div>	<!--line_circ-->
    
    <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'content', 'page' ); ?>
    
    <?php //comments_template( '', true ); ?>
    
    <?php endwhile; // end of the loop. ?>
    
    </div>	<!--main_section-->
        
<!--slide scripts starts--> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.scrollTo.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.nav.min.js"></script> 
<script>
$(document).ready(function() {
$('.navigation').onePageNav();
//$("#header").sticky({ topSpacing: 0, className: 'sticky', wrapperClassName: 'my-wrapper'});
});
</script>     
<script type="text/javascript">
$('#introduction').removeAttr('class');
$('#introduction').addClass('default');
/*1280x800
1280x960
1280x1024
1366x768
1440x900
1600x1200
1920x1080
1920x1200*/
$('.navigate_scroll').live('click',function(){
id = $(this).attr('href');
width 	= screen.width;
height 	= screen.height;
/*alert(width+" "+height);*/
if( (width>=1024) && (height>=768) && (width<1280) ) {
$(id).removeAttr('class');
$(id).attr('class','container_1024x768');
} else if( (width>=1280) && (height>=800) && (width<1366) && (height<960) ) {
$(id).removeAttr('class');
$(id).attr('class','container_1280x800');
} else if( (width>=1280) && (height>=960) && (width<1366) && (height<1024) ) {
$(id).removeAttr('class');
$(id).attr('class','container_1280x960');
} else if( (width>=1280) && (height>=1024) && (width<1366) ) {
$(id).removeAttr('class');
$(id).attr('class','container_1280x1024');
} else if( (width>=1366) && (height>=768) && (width<1440) ){
$(id).removeAttr('class');
$(id).attr('class','container_1366x768');
} else if( (width>=1440) && (height>=900) && (width<1600) ){
$(id).removeAttr('class');
$(id).attr('class','container_1440x900');
} else if( (width>=1600) && (height>=1200) && (width<1920) ){
$(id).removeAttr('class');
$(id).attr('class','container_1600x1200');
} else if( (width>=1920) && (height>=1080) && (height<1200) ){
$(id).removeAttr('class');
$(id).attr('class','container_1920x1080');
} else if( (width>=1920) && (height>=1200) ){
$(id).removeAttr('class');
$(id).attr('class','container_1920x1200');
}

});
</script>

<?php get_footer(); ?>