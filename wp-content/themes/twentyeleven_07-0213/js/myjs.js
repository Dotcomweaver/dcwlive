$(function() {
	$.localScroll({
		stop: true
	});
});

$(window).load(function() {
$('.flexslider').flexslider({
animation: "slide",              //String: Select your animation type, "fade" or "slide"
animationLoop: true,             //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
slideshow: true,                //Boolean: Animate slider automatically
slideshowSpeed: 8000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
animationSpeed: 800,            //Integer: Set the speed of animations, in milliseconds

// Usability features
pauseOnHover: true,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices

// Primary Controls
controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
directionNav: false,             //Boolean: Create navigation for previous/next navigation? (true/false)

// Secondary Navigation
keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys



});
});


var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i) ? true : false;
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};


	if( isMobile.any() ) {
  /* the view port is at least 400 pixels wide */
  
 
} else {

	// Grayscale w canvas method
	function grayscale(src){
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		var imgObj = new Image();
		imgObj.src = src;
		canvas.width = imgObj.width;
		canvas.height = imgObj.height; 
		ctx.drawImage(imgObj, 0, 0); 
		var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
		for(var y = 0; y < imgPixels.height; y++){
			for(var x = 0; x < imgPixels.width; x++){
				var i = (y * 4) * imgPixels.width + x * 4;
				var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
				imgPixels.data[i] = avg; 
				imgPixels.data[i + 1] = avg; 
				imgPixels.data[i + 2] = avg;
			}
		}
		ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
		return canvas.toDataURL();
    }}

// JavaScript Document