// Bind the possible Add to Cart btns with event to position top links
$(document).ready(function(){
	// toTop
	jQuery(window).scroll(function() {
	if(jQuery(this).scrollTop() != 0) {
	jQuery('#toTop').fadeIn();	
	} else {
	$("#portfolio").removeClass("port_padding");
	jQuery('#toTop').fadeOut();
	}
	});
	jQuery('#toTop').click(function() {
	jQuery('body,html').animate({scrollTop:0},300);
	});
	
	// Menu Animation
    $('.menu ul li').hover(
        function() {
            $(this).addClass("active");
            $(this).find('div').not('.subsub_menu, .subsubsub_menu').stop(false, true).slideDown({
            	duration:300,
            	easing:"easeOutExpo"});
        },
        function() {
            $(this).removeClass("active");        
            $(this).find('div').not('.subsub_menu, .subsubsub_menu').stop(false, true).slideUp({
            	duration:100,
            	easing:"easeOutExpo"});
        }
    );

	// Sub Menu Animation
    $('.menu ul li li').hover(
        function() {
            $(this).find('.subsub_menu').stop(false, true).slideDown({
            	duration:300,
            	easing:"easeOutExpo"});
        },
        function() {        
            $(this).find('.subsub_menu').stop(false, true).hide();
        }
    );	
    
    // Subsub Menu Animation
    $('.menu ul li li li').hover(
    		function() {
    			$(this).find('.subsubsub_menu').stop(false, true).slideDown({
    				duration:300,
    				easing:"easeOutExpo"});
    		},
    		function() {        
    			$(this).find('.subsubsub_menu').stop(false, true).hide();
    		}
    );	

	
});