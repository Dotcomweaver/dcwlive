<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->
    
    
<footer id="colophon" role="contentinfo">
<?php
/* A sidebar in the footer? Yep. You can can customize
* your footer with three columns of widgets.
*/
if ( ! is_404() )
get_sidebar( 'footer' );
?>

<?php /*?><div id="site-generator">
<?php do_action( 'twentyeleven_credits' ); ?>
<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentyeleven' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentyeleven' ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyeleven' ), 'WordPress' ); ?></a>
</div><?php */?>
</footer><!-- #colophon -->

    
    <!-- Main Footer start -->
<footer id="foot_wrapper">
  <div id="footer" class="container">
    <div class="row footer_inside">
		 <div class="four columns">
        <h3 class="margined_left">About Us</h3>
        	<?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul', 'container_class' => 'four columns','menu' => 'aboutus','menu_class'      => 'margined_left', ) ); ?>
		</div>
        
       <div class="four columns">
      <h3 class="margined_left">Services</h3>
	<?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => 'four columns','menu' => 'services','menu_class'      => 'margined_left', ) ); ?>
      </div>
      
    <div class="four columns">
    <h3 class="margined_left">Genral</h3>
    <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => 'four columns','menu' => 'general','menu_class'      => 'margined_left', ) ); ?>
    
    </div>
     <!--address block widget--> 
    <div class="four columns">
    <?php if ( ! dynamic_sidebar( 'footer_address' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
    </div>	<!--four columns-->
    
    </div>
    <div class="clear"></div>
  </div>
</footer>
<!-- Main Footer::END --> 

<!-- FADING Footer starts -->
<div class="footer_btm" <?php if ( is_front_page())  echo 'id="fade"'; else echo ''; ?>>
<div class="footer_btm_inner container">
<div class="row">
<div class="six columns">
<div id="powered">
<div class="connect_us"> <span>CONNECT WITH US</span> <a href="javascript:void(0)" class="header_soc_rss" id="soc_rss">RSS</a> <a href="javascript:void(0)" class="header_soc_fb" id="soc_fb">Facebook</a> <a href="javascript:void(0)" class="header_soc_twitter" id="soc_twitter">Twitter</a> </div>
<!--<div class="connect_us">COMPNAY</div>--> 
</div>
</div>
<div class="seven columns footer_btm_inner">
<?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'div',
'container_class' => 'seven columns footer_btm_inner','menu' => 'stickyfooter','menu_class'      => '', ) ); ?>

</div>
<!--footer_btn_inner-->
<div class="three columns"> <a href="javascript:void(0)" class="btn btn-small btn-success">Request A Quote</a> </div>
</div>
<!--row--> 
</div>
<!--footer_btm_inner--> 
</div>
<!--footer_btm--> 

<!-- FADING Footer ends --> 
    
</div>	<!-- #page -->

<?php wp_footer(); ?>

</body>
</html>