<?php
/**
* Template Name: Viewall-VT
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>

<style type="text/css">
.popup {position:fixed;	background: url("../images/fancybox_overlay.png") repeat scroll 0 0 transparent;	width:100%;	height:100%;	z-index:9999999;	left:0;}
.youtube_ifram {background:#F9F9F9;	width:40%;	height:60%;	margin:auto;	position:relative;	top:18%;	padding:10px;	border-radius:5px;	bottom:0;}
.popup_close {background:url("../images/fancybox_sprite.png") no-repeat;	cursor: pointer;	height: 36px;	position: absolute;	right: -18px;	top: -18px;	width: 36px;}
</style>

<!--Testimonails carousel Scripts Ends here-->

<div class="row" id="innerpage_bg">
    <div id="clrbg">	
        <div class="container">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');

} ?>
        <div class="wide_wrapper pad_aply fullwrapper">	
        <div class="row inner_heading">
        <section id="primary" class="vt-pad">
        <h1 class="page-title">View all Video Testimonials</h1>
        <div id="content">
        <div>
        <?php while ( have_posts() ) : the_post(); ?>
        <?php echo the_content(); ?>
        <?php endwhile; // end of the loop. ?>
        </div>
        <div class="clearfix h15"></div>  </div> <!-- #content --> 
        </section><!-- #primary --> 
        </div>	<!--inner_heading-->
        
        <div class="row shadow-3">	</div>	<!--vtbtm-bg-->
        
        <div class="row align-center">	
        <?php
                $args = array(
                'post_type'=> 'video-testimonials',
                'order' => 'DESC',
                'posts_per_page' => -1
                );
                query_posts( $args );
                ?>
                <?php while ( have_posts() ) : the_post();
                $v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
                
                $y_img = get_ytube_video_code($v_test_post_meta->vt_url);
                $te = $v_test_post_meta->vt_url;
                
                $start = strpos($te,"v=")+2;
                
                $end = strpos($te,"&");
                
                if( $end == "" || $end == 0 ){
                $fstr = substr($te,$start);
                } else {
                $length = $end-$start;
                $fstr = substr($te,$start,$length);
                }
                ?>
            <div class="four columns">		
                   <a href="javascript:void(0)" rel="http://www.youtube.com/embed/<?php echo $fstr; ?>" class="show_video"><img src="/wp-content/themes/twentyeleven/images/play120x90.png" style="background:url('http://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg') no-repeat scroll center center; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" alt="<?php the_title() ?> - Testimonial for NJ Web Design company" /></a>
                   <div class="shadow-2"></div>
                </div>	   
            
                 <?php endwhile; ?> 
               
        </div>	<!--align-center-->
        
        </div>	<!--fullwrapper-->
        
        </div>	<!--container-->
    </div>	<!--clrbg-->
</div>	<!--#innerpage_bg-->
<?php get_footer(); ?>