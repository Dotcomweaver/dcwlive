<?php
/**
* The Header for our theme.
*
* Displays all of the <head> section and everything up till <div id="main">
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<!-- Mobile Specific Metas
================================================== -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="verify-v1" content="tjXUxSEr2SbBrHbVx9yXPM8XO89pl01dzSnxjBpv91o=" />

<title><?php
/*
* Print the <title> tag based on what is being viewed.
*/
global $page, $paged;
wp_title( '|', true, 'right' );
// Add the blog name.
//bloginfo( 'name' );
// Add the blog description for the home/front page.
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
echo " | $site_description";
// Add a page number if necessary:
if ( $paged >= 2 || $page >= 2 )
echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!-- all js and css -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>	<!--Droid Sans-->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/skeleton.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/responsive.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/flexslider.css" />
<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/protfolio.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script> 
<!--Testimonails carousel Scripts Starts here--> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexisel.js"></script> 
<!--homepage scripts ends here-->
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<style type="text/css">
/*div#wn {float: left;height: 115px;margin: 0 10px 0 20px;overflow: hidden;position: relative;width: 550px;}div#lyr1 {top:0px !important;}
*/
.popup {position:fixed;background: url("../images/fancybox_overlay.png") repeat scroll 0 0 transparent;width:100%;height:100%;z-index:9999999;left:0;}.youtube_ifram {background:#F9F9F9;width:40%;height:60%;margin:auto;position:relative;top:18%;padding:10px;border-radius:5px;bottom:0;}.popup_close {background:url("../images/fancybox_sprite.png") no-repeat;cursor: pointer;height: 36px;position: absolute;right: -18px;top: -18px;width: 36px;}
</style>

<?php
/* We add some JavaScript to pages with the comment form
* to support sites with threaded comments (when in use).
*/
if ( is_singular() && get_option( 'thread_comments' ) )
wp_enqueue_script( 'comment-reply' );
/* Always have wp_head() just before the closing </head>
* tag of your theme, or you will break many plugins, which
* generally use this hook to add elements to <head> such
* as styles, scripts, and meta tags.
*/
wp_head();
?>
</head>
<body <?php body_class(); ?>>
<div itemscope itemtype="http://schema.org/LocalBusiness">
<!--<div class="popup" id="video_div" style="display:none">
<div class="youtube_ifram" id="inner_video_div">
</div>
</div>-->

<div class="popup" id="video_div" style="display:none">
<div class="youtube_ifram" id="inner_video_div">
</div>
</div>
<?php 
$page = $_SERVER['REQUEST_URI'];
$explore = explode('/',$page);
if($explore[1]!='landing-page')
{
?>
<!-- header wrapper starts here-->
<div class="wrapper">
<header  role="banner">
<div class="container">
<div class="header clearfix row">
<div class="eight columns header_left">
<div id="logo">
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php /*?><?php bloginfo( 'name' ); ?><?php */?>
<img itemprop="image" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"/>
<span class="logo-slogan"><img  src="<?php echo get_template_directory_uri(); ?>/images/logo-slogan.png" alt="slogan"/></span>
</a>
</div>	<!--#logo-->
</div>
<!--hometop contact widget starts-->
<div class="nine columns wid_360">
<div class="header_right">
<div class="header_contacts clearfix">
<?php if ( ! dynamic_sidebar( 'contact_info' ) ) : ?>
<?php endif; // custom widget ?>
</div>	<!--header_contacts clearfix-->
</div><!--header_right-->
</div>	<!--line_circ-->
<!--hometop contact widget ends-->
</div>
<!--header clearfix row-->

</div>
<!-- Main Navigation -->
<div class="row no_bm">
<div class="dark_menu sixteen columns">
<div class="container">
<nav id="access" role="navigation">
<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
<?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'div', 'container_class' => 'menu','menu' => 'topmenu','menu_class'      => 'menu', ) ); ?>
<?php /*?><?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?><?php */?>
</nav>
</div><!-- .container -->
</div>	<!--dark_menu sixteen columns-->
</div>	<!--row no_bm-->
<select id="select_menu" onChange="location = this.value">
<option value="/">Home</option>
<?php
$menu = wp_get_nav_menu_object( 'topmenu' );
$menu_items = wp_get_nav_menu_items($menu->term_id);
foreach ( (array) $menu_items as $key => $menu_item ) {
?>
<option value="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></option>
<?php
}
?>
</select>
<!-- Main Navigation::END -->
</header>	<!-- #header -->
</div>	<!--sticky_header-->
<?php }?>
<!-- header wrapper ends here-->