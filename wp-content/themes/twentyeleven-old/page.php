<?php
/**
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>
<div class="row" id="innerpage_bg">
<div  id="clrbg">
<div class="container">
<?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>
<?php get_sidebar(); ?>
<div class="twelve columns wide_wrapper">
<div class="row inner_heading">
<section id="primary">
<div id="content" role="main" class="innerpage">
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'content', 'page' ); ?>
<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_button_linkedin_counter"></a>
<a class="addthis_button_pinterest_pinit"></a>
<a class="addthis_button_tweet"></a>
</div>
<?php //comments_template( '', true ); ?>
<?php endwhile; // end of the loop. ?>
</div>	<!-- #content -->
</section><!-- #primary -->
</div>	<!--.row inner_heading-->
<div class="clearfix h10"></div>
</div>	<!--.wide_wrapper-->
<div id="toTop"> </div>
</div>	<!--.container-->
</div>	<!-- #clrbg-->
</div>	<!--.row-->
<?php get_footer(); ?>