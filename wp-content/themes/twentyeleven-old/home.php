<?php



/**



* The main template file.



*



* This is the most generic template file in a WordPress theme



* and one of the two required files for a theme (the other being style.css).



* It is used to display a page when nothing more specific matches a query.



* E.g., it puts together the home page when no home.php file exists.



* Learn more: http://codex.wordpress.org/Template_Hierarchy



*



* @package WordPress



* @subpackage Twenty_Eleven



*/







get_header(); ?>



<div id="innerpage_bg" class="row">

  <div id="clrbg">

    <div class="container">

      <?php if ( function_exists('yoast_breadcrumb') ) {

yoast_breadcrumb('<p id="breadcrumbs">','</p>');

} ?>

      <?php get_sidebar(); ?>

      <div class="twelve columns wide_wrapper">

        <div class="row inner_heading">

          <div class="row" style="margin: 0px 0px 0px 15px;">

            <?php if ( ! dynamic_sidebar( 'blogheads' ) ) : ?>

            <?php endif; // end sidebar widget area ?>

          </div>

          <!--row-->

          

          <div id="primary">

            <div id="content" role="main" class="test">

              <?php if ( have_posts() ) : ?>

              <?php //twentyeleven_content_nav( 'nav-above' ); ?>

              <?php /* Start the Loop */ ?>

              <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'content', get_post_format() ); ?>

              <?php endwhile; ?>

              <?php //twentyeleven_content_nav( 'nav-below' ); ?>

              <?php if(function_exists('wp_paginate')) {



wp_paginate();



} ?>

              <?php else : ?>

              <article id="post-0" class="post no-results not-found">

                <header class="entry-header">

                  <h1 class="entry-title">

                    <?php _e( 'Nothing Found', 'twentyeleven' ); ?>

                  </h1>

                </header>

                <!-- .entry-header -->

                

                <div class="entry-content">

                  <p>

                    <?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?>

                  </p>

                  <?php get_search_form(); ?>

                </div>

                <!-- .entry-content --> 

                

              </article>

              <!-- #post-0 -->

              

              <?php endif; ?>

            </div>

            <!-- #content --> 

            

          </div>

          <!-- #primary --> 

          

        </div>

        <!--.row inner_heading--> 

        

      </div>

      <!--.wide_wrapper-->

      

      <div id="toTop"> </div>

    </div>

    <!--.container--> 

  </div>

  <!-- clrbg --> 

</div>

<!--.row-->



<?php get_footer(); ?>

