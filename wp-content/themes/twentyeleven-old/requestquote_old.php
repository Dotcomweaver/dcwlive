	<?php
	/*
	Template Name: Tpl Quote
	*/

	if(isset($_POST['submitted'])) {

		print_r($_POST);
		exit(0);

		$emailTo = get_option('admin_email');
		//$emailTo = 'ranjan@webtenet.com'; // uncomment to test
		$subject = 'Request a Quote from '.$name;// Subject
		$body = "
				<p><em>Note: This is an automatically generated email message.</em></p>
				<p>A visitor has requested a quote on <a ><u>The dotcom weavers</u></a> website.</p>

				<p>The details are as below:</p>
				<p><b>Company:</b> ".$company."</p>
				<p><b>Name:</b> ".$name."</p>
				<p><b>Title:</b> ".$title."</p>
				<p><b>Email Address:</b> ".$email."</p>
				<p><b>Phone Number:</b> ".$phone."</p>
				<p><b>Website:</b> ".$website."</p>
				<p><b>Brief Project Description:</b> ".$description."</p>
				<p><b>Budget:</b> ".$budget."</p>
				<p><b>How did you find us?:</b> ".$howfind."</p>

				<p>Thanks.<br />
				Webmaster</p>
				";

				echo $body;exit(0);

		$logo_path = get_template_directory_uri();// for path

		$file = get_template_directory_uri() . '/inc/template.txt';
		$msg = file_get_contents($file);

		$msg = str_replace("#EMAIL_CONTENT#", $body, $msg);
		$msg = str_replace("#LOGO#", $logo_path, $msg);

		$headers = array("From: ".$name." <".$email.">", "Content-Type: text/html");

		$h = implode("\r\n",$headers) . "\r\n";
		wp_mail($emailTo, $subject, $msg, $h);

		$redirect_link = get_permalink($post->ID).'/?msg='.urlencode('We have received your quote. We will be in touch soon.');
		header('Location: '.$redirect_link);
		exit(0);

	}

	get_header();
	?>

	<script src="<?php echo get_template_directory_uri(); ?>/js/req_a_quote.js"></script>

	<div class="row">
	<div class="container">
    <div class="sixteen columns wide_wrapper clearfix">
    <div class="row inner_heading">

    <div id="primary">
    <div id="content" role="main" class="innerpage">

    <?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'content', 'page' ); ?>

    <?php comments_template( '', true ); ?>

    <?php endwhile; // end of the loop. ?>


    <!--form-->
    <?php if(isset($_GET['msg'])) { ?><p style="color:#008108"><?php echo $_GET['msg']?></p><?php } ?>


		<form action="<?php the_permalink(); ?>" id="reqQuoteForm" name="reqQuoteForm" method="post" onsubmit="javascript:return validateQuote(this);">
		  <table width="100%" cellspacing="0" cellpadding="0" border="0" id="contact_table" style="font-style:normal;font-weight:normal;color:#2e3132;">
            <tr>
                <td width="200">Company:</td>
                <td colspan="2"><input type="text" name="company" size="30" class="ftbox"></td>
            </tr>
            <tr>
            	<td>Name:<span style="color:#FF6603">*</span></td>
                <td colspan="2"><input type="text" name="name" size="30" class="ftbox"></td>
            </tr>
             <tr>
            	<td>Title:</td>
                <td colspan="2"><input type="text" name="title" size="30" class="ftbox"></td>
            </tr>
            <tr>
                <td>Email Address:<span style="color:#FF6603">*</span></td>
                <td colspan="2"><input type="text" name="email" size="30" class="ftbox"></td>
            </tr>
            <tr>
            	 <td>Phone Number:</td>
                <td colspan="2"><input type="text" name="phone" size="30" class="ftbox"></td>
            </tr>
            <tr>
            	 <td>Website:</td>
                <td colspan="2"><input type="text" name="website" size="30" class="ftbox"></td>
            </tr>
            <tr>
                <td style="vertical-align:top;">Brief Project Description:</td>
                <td colspan="2"><textarea id="description" name="description" rows="8" cols="40" style="width:340px;height:52px;"></textarea></td>
            </tr>
             <tr>
                <td nowrap="nowrap" height="25">Budget:<!--<span style="color:#FF6603">*</span>--></td>
                <td width="207"><select id="budget" name="budget" style="width:200px;">
                <option value="" selected="selected">-- select one --</option>
                <option value="$3,000 - $10,000">$3,000 - $10,000</option>
                <option value="$10,000 - $20,000">$10,000 - $20,000</option>
                <option value="$20,000 - $30,000">$20,000 - $30,000</option>
                <option value="$30,000 - $50,000">$30,000 - $50,000</option>
                <option value="$50,000 - $100,000">$50,000 - $100,000</option>
                <option value="more than $100,000">more than $100,000</option>
                </select></td>
                <td>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" height="25">How did you find us?</td>
                <td colspan="2"><select id="howfind" name="howfind" style="width:200px;">
                <option value="" selected="selected">-- select one --</option>
                <option value="Conference/Trade Show">Conference/Trade Show</option>
                <option value="Google">Google</option>
                <option value="Yahoo">Yahoo</option>
                <option value="Bing">Bing</option>
                <option value="MSN">MSN</option>
                <option value="Press Release">Press Release</option>
                <option value="Other">Other</option>
                </select></td>
            </tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>

		<input type="hidden" name="refid" id="refid" value="<?php echo $_GET['refid']; ?>">
		<input type="hidden" name="submitted" id="submitted" value="true" /><!--Submit the form here-->
	  </form>
    <!--form-->


    </div>	<!-- #content -->
    </div>	<!-- #primary -->	<!--.innerpage-->
    <div id="toTop"> </div>
   	</div>	<!--inner_heading-->
    </div>	<!--sixteen columns clearfix-->

	</div>	<!--.container-->
	</div>	<!--.row-->
	<?php get_footer(); ?>