// homepagepage slider & testimonails code starts here
jQuery.noConflict();
jQuery(document).ready(function($) {
	$('.flexslider').flexslider();
    $("#flexiselDemo2").flexisel({
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3
            }
        }
		
    });
	
	$("a.show_video").click(function(){
	var yvideo = $(this).attr('rel'); 
	$("#inner_video_div").html("<a href='javascript:;' class='popup_close' id='close_btn' title='Close'></a><iframe src='"+yvideo+"' width='100%' height='100%' autoplay='true'></iframe>");
	$("#video_div").show();
	init();
	});
	$('#video_div').click(function(){
	$('#video_div').hide();
	});
	});
	function init(){
	$('#close_btn').click(function(){
	$("#inner_video_div").html('');
	$("#video_div").hide()
	});
	}

// homepagepage slider & testimonails code ends here

// conatct us validation code starts here
function checkContactform()
{
   frm=document.contactForm; 
   	if(frm.first_name.value == "")
	{
	alert("Please enter First Name.");
	frm.first_name.focus();
	return false;
	}
	if(frm.last_name.value == "")
	{
	alert("Please enter Last Name.");
	frm.last_name.focus();
	return false;
	}
	
	if(frm.email.value == "")
	{
	alert("Please enter your email address.");
	frm.email.focus();
	return false;
	}
	
	var x=frm.email.value;
	var atpos=x.indexOf("@");
	var dotpos=x.lastIndexOf(".");
	
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	  {
	  alert("Not a valid e-mail address");
	   frm.email.focus();
	 return false;
	  } 
	
	return true;
}
// conatct us validation code ends here

// scroll top starts starts here
jQuery.noConflict();
jQuery(document).ready(function($) {
// toTop
$(window).scroll(function() {
if(jQuery(this).scrollTop() != 0) {
$('#toTop').fadeIn();	
} else {
$('#toTop').fadeOut();
}
});
$('#toTop').click(function() {
$('body,html').animate({scrollTop:0},300);
});
});
// scroll top starts ends here