jQuery.noConflict();
jQuery(document).ready(function($) {
//$("#flexiselDemo1").flexisel();
$("#flexiselDemo2").flexisel({
enableResponsiveBreakpoints: true,
responsiveBreakpoints: { 
portrait: { 
changePoint:480,
visibleItems: 1
}, 
landscape: { 
changePoint:640,
visibleItems: 2
},
tablet: { 
changePoint:768,
visibleItems: 3
}
}
});

});