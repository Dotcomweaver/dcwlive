<?php
/**
 * Template Name: Webdevelopment
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
    
    <?php /*?><div class="four columns sidebar">	
    <?php if ( ! dynamic_sidebar( 'leftside' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
    </div><?php */?>	<!--four columns sidebar-->
   
    
    <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'content', 'page' ); ?>
    
    <?php //comments_template( '', true ); ?>
    
    <?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>