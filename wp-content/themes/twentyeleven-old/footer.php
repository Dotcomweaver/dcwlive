<?php
/*** The template for displaying the footer.** Contains the closing of the id=main div and all content after** @package WordPress* @subpackage Twenty_Eleven* @since Twenty Eleven 1.0*/
?>

<!--</div> #main -->

<!-- Main Footer::END --><!-- LIVE CHAT CODE STARTS -->

<!-- LIVE CHAT CODE ENDS -->

<!-- Main Footer start -->

<footer id="foot_wrapper">
  <div id="footer" class="container">
    <div class="row footer_inside">
      <?php
$page = $_SERVER['REQUEST_URI'];
$page_arr = explode('/',$page);
if( ($page_arr[1]=='blog' && !is_single()) || ($page_arr[1]=='category' && !is_single()) || ($page_arr[1]=='author' && !is_single()) || ($page_arr[1]=='tag' && !is_single()) || ($page_arr[2]=='' && is_single()) || stristr($page_arr[1],"?s=") )
{
?>
      <div class="five columns" style="width:500px;">
        <h3 class="margined_left">About DotcomWeavers</h3>
        <p></p>
        <p class="margined_left">DotcomWeavers is a full-service web design and development agency with over a decade of successful projects. Our customers come to us because we listen to them, do the research, and create the solutions they need at rates they can afford. </p>
      </div>
      <?php
}
else
{
?>
      <div class="five columns">
        <h3 class="margined_left">Services</h3>
        <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => 'four columns','menu' => 'Footer_Services','menu_class'      => 'margined_left', ) ); ?>
      </div>
      <div class="four columns">
        <h3 class="margined_left">Clients</h3>
        <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => 'four columns','menu' => 'general','menu_class'      => 'margined_left', ) ); ?>
      </div>
      <?php
}
?>
      <!--address block widget-->
      <div class="seven columns dcw_addrs">
        <?php if ( ! dynamic_sidebar( 'footer_address' ) ) : ?>
        <?php endif; // end sidebar widget area ?>
      </div>
      <!--four columns-->
      <div class="clear"></div>
      <?php
$url = get_template_directory_uri();
$url = str_replace('themes/twentyeleven','',$url);
?>
      <div class="clear"></div>
      <!--newsletter starts here-->
      <?php /*?><?php
if(is_page('home'))
{
?><?php */?>
      <div class="row" style="min-height:80px;">
        <div class="newsltr_wrp"> </div>
        <!--newsltr_wrp-->
        <div class="ten columns">
          <label class="news_label">Join our Newsletter for Exclusive Tips/Updates on how to grow your business online</label>
        </div>
        <div class="six columns newslt_input">
          <div id="display_newletter"></div>
          <form name="subnews_news_letter" id="subnews_news_letter" class="validateform" method="post">
            <input type="text"  class="corner-input" id="news_email" name="news_email" placeholder="Enter Email Address" />
            <input type="button" class="subscribe" id="subscribe_news" name="subscribe" value="Subscribe" />
          </form>
        </div>
        <!--newslt_input-->
        <div class="clear"></div>
		<script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function($) {
        $("#subscribe_news").click(function(){
        $("#display_newletter").html("");
        var post_data = $('#subnews_news_letter').serialize();
        $.ajax({
        type: "POST",
        url: "http://www.dotcomweavers.com/newsletter/newsletter.php",
        data: post_data,
        dataType: "html",
        success: function(msg){
        $("#display_newletter").html(msg);
        },
        error: function(){
        $("#display_newletter").html("Failed to Send Your Request. Try again!");
        }
        });
        });
        });
        </script> 
      </div>
      <div class="clear"></div>
      <!--row--> 
      <!--newsletter ends here--> 
    </div>
    <div class="clearfix" style="height:30px" ></div>
  </div>
  <!-- FADING Footer starts -->
  <div class="footer_btm" <?php if ( is_front_page())  echo 'id="fade"'; else echo 'id="fade"'; ?>>
    <div class="footer_btm_inner container">
      <div class="row">
        <div class="six columns">
          <div id="powered">
            <div class="connect_us"> <span>CONNECT WITH US</span> <a href="http://www.facebook.com/pages/Dotcomweavers/124236440928166" class="header_soc_fb" title="facebook" target="_blank">Facebook</a> <a href="http://twitter.com/dotcomweavers" class="header_soc_twitter" title="Twitter" target="_blank">Twitter</a> <a href="https://plus.google.com/107414909360503446764" rel="publisher" title="Google+" class="footer_google_plus" target="_blank">Google+</a> <a href="http://www.dotcomweavers.com/feed" class="header_soc_rss" title="RSS" target="_blank">RSS</a> </div>
            
            <!--<div class="connect_us">COMPNAY</div>--> 
            
          </div>
        </div>
        <div class="nine columns footer_btm_inner">
          <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'div',

'container_class' => 'footer_btm_inner','menu' => 'stickyfooter','menu_class'      => '', ) ); ?>
        </div>
        
        <!--footer_btn_inner-->
        
        <div class="fl" style="margin-left: 3px;"> <a href="<?php echo get_permalink( get_page_by_title( 'Contact Us' ) ); ?>?refid=<?php echo $post->ID; ?>" class="org_smal"><span>Contact Us</span></a> </div>
      </div>
      
      <!--row--> 
      
    </div>
    <!--footer_btm_inner--> 
    
  </div>
  <!-- FADING Footer ends --> 
</footer>
<!-- Main Footer::END --> 
<!-- </div>	 #page -->

<script type="text/javascript">
WebFontConfig = {
google: { families: ["Open Sans","Oxygen"] }
};
(function() {
var wf = document.createElement('script');
wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
wf.type = 'text/javascript';
wf.async = 'true';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(wf, s);
})();
</script>

<?php /*?><?php wp_footer(); ?><?php */?>
<script type="text/javascript">var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-16800069-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52232976284229a1"></script>
<!-- AddThis Button END -->
</div>
</body></html>
