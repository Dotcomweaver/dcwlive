<?php

/**

* The Template for displaying all single posts.

*

* @package WordPress

* @subpackage Twenty_Eleven

* @since Twenty Eleven 1.0

*/



get_header(); ?>



<div id="innerpage_bg" class="row">

  <div id="clrbg">

    <div class="container">

      <?php if ( function_exists('yoast_breadcrumb') ) {

yoast_breadcrumb('<p id="breadcrumbs">','</p>');

} ?>

      <?php get_sidebar(); ?>

      <div class="twelve columns wide_wrapper">

        <div class="row inner_heading">

          <?php /*?><div class="row">

<?php if ( ! dynamic_sidebar( 'blogheads' ) ) : ?>

<?php endif; // end sideb	ar widget area ?>

</div><?php */?>

          <!--row-->

          <div id="primary">

            <div id="content" role="main" class="heading">

              <?php while ( have_posts() ) : the_post(); ?>

              <?php /*?><nav id="nav-single">

<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentyeleven' ); ?></h3>

<span class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous', 'twentyeleven' ) ); ?></span>

<span class="nav-next"><?php next_post_link( '%link', __( 'Next <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></span>

</nav>	<?php */?>

              

              <!-- #nav-single -->

              

              <?php get_template_part( 'content-single', get_post_format() ); ?>

              <?php //comments_template( '', true ); ?>

              <?php endwhile; // end of the loop. ?>

            </div>

            <!-- #content --> 

          </div>

          <!-- #primary --> 

        </div>

        <!--.row inner_heading--> 

      </div>

      <!--.wide_wrapper-->

      <div id="toTop"> </div>

    </div>

    <!--.container--> 

  </div>

  <!-- clrbg --> 

</div>

<!--.row-->

<?php get_footer(); ?>

<style type="text/css">
#content article h1{  color: #0C3953 !important;
/* display: block; font-family: 'Droid Sans', sans-serif; font-weight: 400; font-size: 28px; line-height: 28px; background: #fff; margin:0; text-transform:none; letter-spacing: normal; padding-right: 10px;*/
border-bottom: 1px solid #FFFFFF;
border-left: 4px solid #FD5000;
color: #0C3953;
display: block !important;
font-family: 'Droid Sans',sans-serif;
font-weight: 400 !important;
letter-spacing: normal;
line-height: normal;
padding:10px 10px 10px 6px;
text-transform: none; 
margin: 0px 0 10px;
/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(top, #FDFDFD 0%, #EAEAEA 100%) !important;

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(top, #FDFDFD 0%, #EAEAEA 100%)!important;

/* Opera */ 
background-image: -o-linear-gradient(top, #FDFDFD 0%, #EAEAEA 100%)!important;

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #FDFDFD), color-stop(1, #EAEAEA))!important;

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(top, #FDFDFD 0%, #EAEAEA 100%)!important;

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to bottom, #FDFDFD 0%, #EAEAEA 100%)!important;

border-bottom: 5px solid #d6d6d6;
}
</style>