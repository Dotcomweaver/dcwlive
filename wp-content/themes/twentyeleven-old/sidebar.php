<?php

/**

 * The Sidebar containing the main widget area.

 *

 * @package WordPress

 * @subpackage Twenty_Eleven

 * @since Twenty Eleven 1.0

 */



$options = twentyeleven_get_theme_options();

$current_layout = $options['theme_layout'];



if ( 'content' != $current_layout ) :

?>

		<div id="secondary" class="widget-area" role="complementary">

        <div class="four columns sidebar">



<?php

echo get_option('current_page_template');

	// check for any sidebar association

	$menu_name_post_meta = json_decode(get_post_meta($post->ID,'menu_name_post_meta', true));

	$page_sidebar_id = $menu_name_post_meta->page_sidebar_id;

	if (is_home() || 'post'==get_post_type())

		$page_sidebar_id = 'sidebar-2';

	

?>



			<?php if ( ! dynamic_sidebar( $page_sidebar_id ) ) : ?>



				<aside id="archives" class="widget">

					<h3 class="widget-title"><?php _e( 'Archives', 'twentyeleven' ); ?></h3>

					<ul>

						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>

					</ul>

				</aside>



				<aside id="meta" class="widget">

					<h3 class="widget-title"><?php _e( 'Meta', 'twentyeleven' ); ?></h3>

					<ul>

						<?php wp_register(); ?>

						<li><?php wp_loginout(); ?></li>

						<?php wp_meta(); ?>

					</ul>

				</aside>



			<?php endif; // end sidebar widget area ?>

            </div>	<!--four columns sidebar-->

		</div><!-- #secondary .widget-area -->

<?php endif; ?>