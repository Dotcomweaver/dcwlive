<?php

/**

* The Template for displaying all single posts.

*

* @package WordPress

* @subpackage Twenty_Eleven

* @since Twenty Eleven 1.0

*/
get_header(); ?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					} ?>
               	  <h1><?php the_title(); ?></h1>
              </div>
          </div>
        </div>
</section>

<section class="light-gray-wraper divider">
        <div class="container" id="container-blog">
            <div class="row">
                <div class="col-md-12">
                	<div class="blog-filter">
                    	<a href="<?php echo site_url('blog'); ?>" class="btn btn-warning active">VIEW ALL</a>
                        <?php
						$posttags = get_tags();
						$tmp = $posttags[4];
						$posttags[4] = $posttags[5];
						$posttags[5] = $tmp;
						foreach($posttags as $tag){
						?>
                        	<a href="<?php echo get_tag_link($tag->term_id); ?>" class="btn btn-warning"><?php echo $tag->name; ?></a>
                        <?php } ?>
                    </div><!-- blog-filter -->
                    <?php while ( have_posts() ) : the_post();
				$category = get_the_category( $post->ID);
						if($category != 'Home Slider'): ?>
                 <?php get_template_part( 'content-single', get_post_format() ); ?>
                  
                <?php 	endif;
                
				endwhile; ?>
                	
                </div>
            </div>
		</div>
    </section>



<!--.row-->

<?php get_footer(); ?>
