<?php
/**
* Template Name: Services Inner
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); 
?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12"><span class="fa fa-cog"></span><h1>Services</h1>
                  <p>This is a subtext that can be replaced with real text once decided.This is a subtext that can be replaced with real text once decided. This is a subtext that can be replaced with real text once decided.This is a subtext that can be replaced with real text once decided.<?php $post_meta = get_post_meta($post->ID,'wpcf-bannersubtext',true); echo $post_meta;  ?></p>
              </div>
          </div>
        </div>
</section>
<section id="sub-nav">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="navbar secondary-nav navbar-default" role="navigation">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".second-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="#">Navigation</a>
                        </div>
                        <div class="navbar-collapse second-nav collapse">
                          <?php
							wp_nav_menu( array(
												  'menu' => 'services',
												  'depth' => 2,
												  'container' => false,
												  'menu_class' => 'nav navbar-nav',
												  //Process nav menu using our custom nav walker
												  'walker' => new wp_bootstrap_navwalker())
												);
						 
						?>
                       </div><!--/.nav-collapse -->
                    </div>
                                
                </div>
            </div>
        </div>
</section>
<section class="light-gray-wraper">
<div class="container">
<div class="row divide">
<div class="col-md-12 text-center">
<h2>WHAT IS RESPONSIVE WEB DESIGN</h2>
<p class="sub-desc">We design, build and implement web and ecommerce solutions for businesses, serving a wide variety of clients in different industries from Paper to Pump.</p>

<div class="divide"><img class="img-responsive" src="http://localhost/dcw/wp-content/uploads/2014/05/responsive-iimg.jpg" alt="responsive-iimg" />
<div class="h1">Get to where your audience is.. <span class="orange-txt">Be responsive</span></div>
</div>
</div>
</div>
</div>
</section><section id="slide-1" class="sevicesSlide">
<div class="bcg" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#slide-1">
<div class="hsContainer">
<div class="hsContent" data-center="opacity: 1" data-106-top="opacity: 0.8" data-anchor-target="#slide-1 h2">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>WHY RESPONSIVE DESIGN</h2>
<p class="sub-desc">We design, build and implement web and ecommerce solutions for businesses, serving a wide variety of clients in different industries from Paper to Pump.</p>

<div class="row divide">
<div class="col-md-6 text-left">

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
<div class="col-md-6 text-left">

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
<div class="col-md-12 text-center mtop20"><a class="btn btn-warning" href="#">READ MORE BELOW</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section><section class="light-gray-wraper">
<div class="container">
<div class="row mtop40">
<div class="col-md-12 text-center">
<h2>RESPONSIVE WORKS</h2>
<p class="sub-desc">We design, build and implement web and ecommerce solutions for businesses, serving a wide variety of clients in different industries from Paper to Pump.</p>
<div class="text-left row portfolio">
<?php
$folio_post = get_post();

//if (end($folio_post->ancestors) == 506) :

$folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
//print_r($folio_post_meta);
$folio_ids_arr = $folio_post_meta->services_folio;

if (count($folio_ids_arr)) :
				$args = array(

				'post__in' => $folio_ids_arr,

				'post_type'=> 'portfolio',

				'order' => 'DESC',

				'posts_per_page' => -1

				);

				query_posts( $args );

				while ( have_posts() ) : the_post();

				$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

				$the_link = get_permalink();

				$the_title = get_the_title();
				$the_content =  get_the_content('Read more');
				$ret_val.= '<div class="col-sm-6">
									<div class="white-box">
										<div class="white-box-top">
											<div class="row">
												<div class="col-md-9">
													<h3>'.$the_title.'</h3>
												</div>
												<div class="col-md-3 text-right">
													<a href="#"><strong>Dig Deeper <i class="fa fa-angle-double-right"></i></strong></a>
												</div>
											</div>
											<p class="port-desc">'.$the_content.'</p>
									</div>
									<a href="http://localhost/dcw/wp-content/uploads/2014/05/pump.jpg">
										<img src="'.$portfolio_feat_image.'" alt="pump" class="img-responsive" />
									</a>
									</div>
								</div>';

				endwhile; // end of the loop.
				wp_reset_query();

echo $ret_val;

endif;
?>
</div>
<div class="row testimonial">
<?php 
wp_reset_query();
$ret_val = '';
$vt_post = get_post();
$sx_post_meta = json_decode(get_post_meta($vt_post->ID,'services_extra_post_meta', true));
$vt_ids_arr = $sx_post_meta->services_video_testimonials;
if (count($vt_ids_arr)) :
				$args = array(

				'post__in' => $vt_ids_arr,

				'post_type'=> 'video-testimonials',

				'order' => 'DESC',

				'posts_per_page' => -1

				);

				$vt_post = query_posts( $args );

				

				foreach ( $vt_post as $post ) :

				$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));

				$v_code = get_ytube_video_code($v_test_post_meta->vt_url);

				$url = "http://youtube.com/vi/".$v_code."/0.jpg";

				$ret_val .= '<div class="col-md-3 col-sm-4 col-xs-6">
                        	<img src="http://img.youtube.com/vi/'.$v_code.'/0.jpg" alt="rycore-brand" class="img-responsive" /><a href="javascript:void();"><span class="icomoon-icon-play-2"></span></a>
                        </div>';
				endforeach; // end of the loop.
echo $ret_val;

endif;
?>
</div>
<!-- /.testimonial --></div>
</div>
<!-- /.row .divide -->
<div class="row">
<div class="col-md-12">
<h2 class="text-center">More on Responsive Design</h2>
<div>
    <p>Most business relationships start with an online research of a company's history and background offering the consumer vital information on whether to work with them. Therefore establishing an effective web presence that properly represents your brand and products can be the difference between gaining a new customer or just having a visitor to your site. Each business represents a unique value proposition for their product or service. Our goal with each web design client is to take that unique vision and transfer it to the digital world by creating a practical yet effective web design that helps better engage your target audience.<br>
<strong>Web Design NJ</strong></p>

<p>In today's socially active world we realize that starting a new business venture, especially a web design and development project, has more to it than just designing a website. It's about designing a responsive website. With all the different strategies, solutions and cost for responsive design, it's no wonder why small businesses enter these relationships with so much trepidation. We have years of experience building small business websites and helping local New Jersey businesses get their website built right the first time. We enter into each business relationship as a partnership that shares success through mutual goals.</p>

<p>Most business relationships start with an online research of a company's history and background offering the consumer vital information on whether to work with them. Therefore establishing an effective web presence that properly represents your brand and products can be the difference between gaining a new customer or just having a visitor to your site. Each business represents a unique value proposition for their product or service. Our goal with each web design client is to take that unique vision and transfer it to the digital world by creating a practical yet effective web design that helps better engage your target audience.<br/>
Web Design NJ</p>

<p>In today's socially active world we realize that starting a new business venture, especially a web design and development project, has more to it than just designing a website. It's about designing a responsive website. With all the different strategies, solutions and cost for responsive design, it's no wonder why small businesses enter these relationships with so much trepidation. We have years of experience building small business websites and helping local New Jersey businesses get their website built right the first time. We enter into each business relationship as a partnership that shares success through mutual goals.</p>
</div>
</div>
</div>
</div>
</section><?php get_footer(); ?>