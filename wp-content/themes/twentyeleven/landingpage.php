<?php
/**
* Template Name: Landingpage
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>
<?php
//Check for post data
if(isset($_POST['submitcontact'])) {
$phone1 = trim($_POST['phone1']);
$phone2 = trim($_POST['phone2']);
$phone3 = trim($_POST['phone3']);
$phone = $phone1."-".$phone2."-".$phone3;
$website = trim($_POST['website']);
$budget = trim($_POST['budget']);
$company = trim($_POST['company']);
// validate the first name field
if(trim($_POST['first_name']) === '') {
$fnameError = 'Please enter First Name.';
$hasError = true;
} else {
$first_name = trim($_POST['first_name']);
}
// validate the last name field
if(trim($_POST['last_name']) === '') {
$lnameError = 'Please enter Last Name.';
$hasError = true;
} else {
$last_name = trim($_POST['last_name']);
}
// validate the email field
if(trim($_POST['email']) === '')  {
$emailError = 'Please enter your email address.';
$hasError = true;
} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {// validate the correct email field
$emailError = 'You entered an invalid email address.';
$hasError = true;
} else {
$email = trim($_POST['email']);
}
if(function_exists('stripslashes')) {
$comments = stripslashes(trim($_POST['commentsText']));
} else {
$comments = trim($_POST['commentsText']);
}
//if( (!isset($hasError)) && ($resp->is_valid) ) {
if(!isset($hasError)) {
$name = $first_name.' '.$last_name;
$emailTo = get_option('admin_email');
$emailTo = 'info@dotcomweavers.com,fhaywood@dotcomweavers.com,drapoport@dotcomweavers.com'; // uncomment to test
//$emailTo = 'saaluri@dotcomweavers.com'; // uncomment to test
$subject = 'Contact Request from '.$name;// Subject
$body = "
<p><em>Note: This is an automatically generated email message.</em></p>
<p>A visitor has filled out the contact form on Dotcomweavers website.</p>
<p>The details are as below:</p>
<p><b>Message:</b> ".$comments."</p>
<p><b>Visitor&rsquo;s Information:</b><br /><br />
Name: ".$first_name."<br />
Email: ".$email."<br />
Phone: ".$phone."<br />
Company: ".$company."<br />
Budget: ".$budget."<br />
Website: ".$website."<br />
<p>Thanks.<br />
Webmaster<br />
";
$logo_path = get_template_directory_uri();// for path
$file = get_template_directory_uri() . '/inc/template.txt';
$msg = file_get_contents($file);
$msg = str_replace("#EMAIL_CONTENT#", $body, $msg);
$msg = str_replace("#LOGO#", $logo_path, $msg);
$headers = array("From: ".$name." <info@dotcomweavers.com>", "Reply-To:".$name." <".$email.">", "Content-Type: text/html");
$h = implode("\r\n",$headers) . "\r\n";
wp_mail($emailTo, $subject, $msg, $h);
//End Admin Email
//Sending Email to Customer
$body  ='
<table width="70%" border="0" align="left" cellpadding="5" cellspacing="5" bgcolor="#F1F2EB">
<tr>
<td colspan="2"><a href="http://www.dotcomweavers.com/" ><img src="http://www.dotcomweavers.com/images/new-logo.png" border="0"></a></td>
</tr>
<tr>
<td colspan="2" style="color:#666666; font-family:Arial, Helvetica, sans-serif;">Thank You for contacting us. We will get back to you very shortly.</td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2" style="color:#cccccc; font-family:Arial, Helvetica, sans-serif;">Thank You!<br />
Dotcomweavers<br />
<a href="http://www.dotcomweavers.com" style="color:#cccccc; font-family:Arial, Helvetica, sans-serif;">http://www.dotcomweavers.com</a></td>
</tr>
</table>
';
$headers = array("From: Dotcomweavers <info@dotcomweavers.com>", "Content-Type: text/html");
$subject = "Thank You for contacting Dotcomweavers";
$h = implode("\r\n",$headers) . "\r\n";
wp_mail($email, $subject, $body, $h);
//End Customer Email
$pg = get_page_by_title( 'Thank You' );
$redirect_link = get_permalink($pg->ID);
header('Location: '.$redirect_link);
exit(0);
}
}
if(isset($_POST['submitloading'])) {
$phone1 = trim($_POST['phone1_one']);
$phone2 = trim($_POST['phone2_one']);
$phone3 = trim($_POST['phone3_one']);
$phone = $phone1."-".$phone2."-".$phone3;
// validate the first name field
if(trim($_POST['first_name_one']) === '') {
$fnameError1 = 'Please enter First Name.';
$hasError = true;
} else {
$first_name = trim($_POST['first_name_one']);
}
// validate the last name field
if(trim($_POST['last_name_one']) === '') {
$lnameError1 = 'Please enter Last Name.';
$hasError = true;
} else {
$last_name = trim($_POST['last_name_one']);
}
// validate the email field
if(trim($_POST['email_one']) === '')  {
$emailError1 = 'Please enter your email address.';
$hasError = true;
} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email_one']))) {// validate the correct email field
$emailError1 = 'You entered an invalid email address.';
$hasError = true;
} else {
$email = trim($_POST['email_one']);
}
if(function_exists('stripslashes')) {
$comments = stripslashes(trim($_POST['commentsText_one']));
} else {
$comments = trim($_POST['commentsText_one']);
}
//if( (!isset($hasError)) && ($resp->is_valid) ) {
if(!isset($hasError)) {
$name = $first_name.' '.$last_name;
$emailTo = get_option('admin_email');
$emailTo = 'info@dotcomweavers.com,fhaywood@dotcomweavers.com,drapoport@dotcomweavers.com'; // uncomment to test
//$emailTo = 'saaluri@dotcomweavers.com,sureshaaluri@gmail.com'; // uncomment to test
$subject = 'Contact Request from '.$name;// Subject
$body = "
<p><em>Note: This is an automatically generated email message.</em></p>
<p>A visitor has filled out the contact form on Dotcomweavers website.</p>
<p>The details are as below:</p>
<p><b>Message:</b> ".$comments."</p>
<p><b>Visitor&rsquo;s Information:</b><br /><br />
Name: ".$first_name."<br />
Email: ".$email."<br />
Phone: ".$phone."<br />
<p>Thanks.<br />
Webmaster<br />
";
$logo_path = get_template_directory_uri();// for path
$file = get_template_directory_uri() . '/inc/template.txt';
$msg = file_get_contents($file);
$msg = str_replace("#EMAIL_CONTENT#", $body, $msg);
$msg = str_replace("#LOGO#", $logo_path, $msg);
$headers = array("From: ".$name." <info@dotcomweavers.com>", "Reply-To:".$name." <".$email.">", "Content-Type: text/html");
$h = implode("\r\n",$headers) . "\r\n";
wp_mail($emailTo, $subject, $msg, $h);
//End Admin Email
//Sending Email to Customer
$body  ='
<table width="70%" border="0" align="left" cellpadding="5" cellspacing="5" bgcolor="#F1F2EB">
<tr>
<td colspan="2"><a href="http://www.dotcomweavers.com/" ><img src="http://www.dotcomweavers.com/images/new-logo.png" border="0"></a></td>
</tr>
<tr>
<td colspan="2" style="color:#666666; font-family:Arial, Helvetica, sans-serif;">Thank You for contacting us. We will get back to you very shortly.</td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2" style="color:#cccccc; font-family:Arial, Helvetica, sans-serif;">Thank You!<br />
Dotcomweavers<br />
<a href="http://www.dotcomweavers.com" style="color:#cccccc; font-family:Arial, Helvetica, sans-serif;">http://www.dotcomweavers.com</a></td>
</tr>
</table>
';
$headers = array("From: Dotcomweavers <info@dotcomweavers.com>", "Content-Type: text/html");
$subject = "Thank You for contacting Dotcomweavers";
$h = implode("\r\n",$headers) . "\r\n";
wp_mail($email, $subject, $body, $h);
//End Customer Email
$pg = get_page_by_title( 'Thank You' );
$redirect_link = get_permalink($pg->ID);
header('Location: '.$redirect_link);
exit(0);
}
}
?>
<!--<div class="popup" id="video_div" style="display:none">
<div class="youtube_ifram" id="inner_video_div">
</div>
</div>-->
<style type="text/css">
html,body{ margin:0 !important; padding:0; background:#fff !important;}
/*.my-wrapper {display: none;}*/
.container > p {padding: 0 0 13px; font-size: 17px;}
.list_menu a {font-size: 15px;}
.landing_form {background-color: #FFFFFF; color: #FFFFFF; padding: 0 0 15px; position: absolute;top: 180px;width:325px;z-index: 1000 !important;    right: 1%;
-webkit-border-radius: 0px 0px 3px 3px;
border-radius: 0px 0px 3px 3px;
-moz-background-clip: padding;
-webkit-background-clip: padding-box;
background-clip: padding-box;
border: 1px solid #d8e0e4;
-webkit-box-shadow: 0 0 5px #d8e0e4;
box-shadow: 0 0 5px #d8e0e4;}
.blue_bg{background: none repeat scroll 0 0 #5DA0E1;border: 1px solid #4F89C1; -webkit-border-radius: 3px 3px 0 0; border-radius: 3px 3px 0 0; height: 38px;margin: 0 0 10px;width: 100%;}
.blue_bg h3 {color: #FFFFFF; line-height: 38px; padding: 0 0 0 12px;}
#cont_form{margin: 0 0 0 13px; }
#cont_form .text-input{margin-bottom: 8px !important; 	padding: 6px 5px !important;}
#contactform .selectbox{margin-bottom: 10px !important; padding: 6px 5px !important;}
#contactform .text-input{margin-bottom: 10px !important; padding: 6px 5px !important;}
.footer_btm{padding: 7px 0;}
.content_list_ul li{ font-size: 16px;   margin-bottom: 15px;}
.list_menu ul {margin: 0 !important;}
.pic img{max-width: 93%;}
.popup{position:fixed; background: url("../images/fancybox_overlay.png") repeat scroll 0 0 transparent; width:100%; height:100%; z-index:9999999; left:0;}
.youtube_ifram{background:#F9F9F9; width:40%; height:60%; margin:auto; position:relative; top:18%; padding:10px; border-radius:5px; bottom:0;}
.popup_close{background:url("../images/fancybox_sprite.png") no-repeat; cursor: pointer;height: 36px;position: absolute;right: -18px;top: -18px;width: 36px;}
</style>
<div class="popup" id="video_div" style="display:none">
<div class="youtube_ifram" id="inner_video_div">
</div>
</div>
<div class="topBG" id="header">
<div class="container">
<div class="header clearfix row">
<div class="eight columns header_left">
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php /*?><?php bloginfo( 'name' ); ?><?php */?>
<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" style="padding: 13px 0 0;"/>
</a>
</div>	<!--logo-->
<div class="nine columns">
<ul class="header-information">
<li class="phone-info">
<p>Questions?</p>
<p><strong>Call 1-888-315-6518</strong></p>
</li>
<li class="email-info">
<p>Email Us:</p>
<p><a href="mailto:info@dotcomweavers.com">info@dotcomweavers.com</a></p>
</li>
</ul>	<!--header-information-->
</div>	<!--eight columns-->
</div>	<!--header clearfix row-->
</div>	<!--container-->
<div class="clear"></div>
</div>	<!-- header wrapper-->
<?php while ( have_posts() ) : the_post(); ?>
<?php //get_template_part( 'content', 'page' ); ?>
<?php //comments_template( '', true ); ?>
<?php endwhile; // end of the loop. ?>
<!--content starts here-->
<div class="row blue_clr">
<div class="container">
<h1>Web Design Experts, Delivering <span>Innovative Web Solutions</span> for Your Business</h1>
</div>
<!--container ends here-->
</div>
<!--row-->
<div class="container">
<p>DotcomWeavers utilizes the latest technology to help your company excel and succeed online. We are totally dedicated to delivering cutting edge and cost-effective
solutions for website design and development, eCommerce, SEO/SEM, and more.</p>
<div class="clearfix h5"></div>
<div class="list_menu">
<ul class="content_list_ul">
<li>Web Design</li>
<li>eCommerce</li>
<li>Content Management Systems</li>
<li>Web Development</li>
<li>Web Applications</li>
<li>Mobile websites & Apps</li>
</ul>
</div>
<!--<div class="thirteen columns">
<a href="http://www.dotcomweavers.com" class="magicmore" target="_blank">View Details on our full Website</a>
</div>-->
</div>	<!--container-->
<div class="row blue_clr">
<div class="container">
<h2>Our <span>Work</span></h2>
</div>
<!--container ends here-->
</div>
<div class="container">
<link type="text/css" href="<?php echo plugins_url( ); ?>/portfolio-grid/css/portfolio_style.css" rel="stylesheet" />
<?php
$folio_post_meta = json_decode(get_post_meta($post->ID,'services_folio_post_meta', true));
$folio_ids_arr = $folio_post_meta->services_folio;
$portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
$portfolio_images_num_display = $portfolio_images_settings->portfolio_images_num_display;
$args = array(
'post__in' => $folio_ids_arr,
'post_type'=> 'portfolio',
'order' => 'DESC',
'posts_per_page' => intval($portfolio_images_num_display)
);
query_posts( $args );
//Start the Loop.
if ( have_posts() ) : while ( have_posts() ) : the_post();
$post_id = $post->ID;
//$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
$portfolio_feat_image = get_the_post_thumbnail($post_id, 'medium');
//Returns Array of Term Names for "my_term"
$term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
$postclasses = '';
$posttags = '';
foreach($term_list as $item):
$posttags .= $item.", ";
$postclasses .= str_replace(" ","_",$item)."-";
endforeach;
// twick to popup
$folio_permalink = get_permalink();
$folio_permalink = add_query_arg('popup','open',$folio_permalink);
?>
<div id="post_<?php echo $post_id ?>" class="row_portfolio <?php echo implode(' ', $term_list)?>" data-id="<?php echo rtrim($postclasses,'-');?>">
<?php /*?><a href="<?php the_permalink();?>"><img src="<?php echo $portfolio_feat_image;?>" alt="<?php the_title();?>"/></a><?php */?>
<span class="pic">
<a href="<?php echo $folio_permalink ;?>" class="folio_popup"><?php echo $portfolio_feat_image;?></a>
<div class="img_overlay"></div>
</span>	<!--pic-->
<div class="row_title"><a href="<?php echo $folio_permalink ;?>" class="folio_popup"><?php the_title(); ?></a></div>
</div> <!-- closes the first div box -->
<!-- Stop The Loop (but note the "else:" - see next line). -->
<?php endwhile; else: ?>
<!-- The very first "if" tested to see if there were any Posts to -->
<!-- display.  This "else" part tells what do if there weren't any. -->
<p>Sorry, no posts matched your criteria.</p>
<!-- REALLY stop The Loop. -->
<?php endif; ?>
<?php wp_reset_query(); ?>
<div class="clear"></div>
</div>
&nbsp;
<div class="row blue_clr">
<div class="container">
<h2>What our <span>clients are saying</span></h2>
</div>
<!--container ends here-->
</div>
<!--row-->
<div class="container">
<?php
$sx_post_meta = json_decode(get_post_meta($post->ID,'services_extra_post_meta', true));
$vt_ids_arr = $sx_post_meta->services_video_testimonials;
$args = array(
'post__in' => $vt_ids_arr,
'post_type'=> 'video-testimonials',
'order' => 'ASC',
'posts_per_page' => -1
);
$vt_post = query_posts( $args );
//Start the Loop.
foreach ( $vt_post as $post ) :
$post_id = $post->ID;
$title = $post->title;
$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
//print_r($case_studies_post_meta);
$y_img = get_ytube_video_code($v_test_post_meta->vt_url);
$te = $v_test_post_meta->vt_url;
$start = strpos($te,"v=")+2;
$end = strpos($te,"&");
if( $end == "" || $end == 0 ){
$fstr = substr($te,$start);
} else {
$length = $end-$start;
$fstr = substr($te,$start,$length);
}
?>
<div class="four columns" style="margin: 0 100px 0 0;float:left;">
<a  href="javascript:void(0)" rel="http://www.youtube.com/embed/<?php echo $fstr; ?>" class="show_video">
<span class="pic">
<img src="http://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" alt="<?php the_title() ?> - Testimonial for NJ Web Design company" style="width:300px;" />
<div class="img_overlay"></div>
</span>
</a>
</div>
<?php endforeach; // end of the loop. ?>
<?php wp_reset_query(); ?>
</div>	<!--vt container-->
<div class="clear"></div>
&nbsp;
<div class="row blue_clr">
<div class="container">
<h2>Contact <span>Us</span></h2>
</div>
<!--container ends here-->
</div>
<!--row-->


<div class="landing_form">
<div class="blue_bg">
<h3><b>DotcomWeavers.</b> <span>can help you</span></h3>
</div>
<div id="cont_form">
<!--<form action="<?php //the_permalink(); ?>" id="contactForm" method="post" class="landingform_void">-->
<form action="<?php the_permalink(); ?>" id="contactForm" method="post" class="landingform_void">
<fieldset>
<div class="row">
<div class="fl">
<label for="first_name">First Name:&nbsp;<em>(Required)</em></label>
<input type="text" name="first_name_one" id="first_name" value="<?php if(isset($_POST['first_name_one'])) echo $_POST['first_name_one'];?>" class="text-input" /><br>
<?php if($fnameError1 != '') { ?><!--Show error message-->
<span style="color:#FF0000;"><?=$fnameError1;?></span>
<?php } ?>
</div>	<!--fl-->
<div class="fl">
<label for="lName">Last Name:&nbsp;<em>(Required)</em></label>
<input type="text" name="last_name_one" id="last_name_one" value="<?php if(isset($_POST['last_name_one'])) echo $_POST['last_name_one'];?>" class="text-input" /><br>
<?php if($lnameError1 != '') { ?><!--Show error message-->
<span style="color:#FF0000;"><?=$lnameError1;?></span>
<?php } ?>
</div>	<!--fl-->
</div>	<!--row-->
<div class="row">
<div class="fl">
<!--Contact Email-->  <label for="email">Email:&nbsp;<em>(Required)</em></label>
<input type="text" name="email_one" id="email" value="<?php if(isset($_POST['email_one']))  echo $_POST['email_one'];?>" class="text-input" /> <br>
<?php if($emailError1 != '')	{ ?><!--Show error message-->
<span style="color:#FF0000;"><?=$emailError1;?></span>
<?php } ?>
</div>	<!--fl-->
<div class="fl" style="color:#555555;">
<label for="phone">Phone:&nbsp;</label>
<input type="text" maxlength="3" name="phone1_one" id="phone1" value="" class="text-input valid" style="text-align: center; max-width: 40px ! important;"> -
<input type="text" maxlength="3" name="phone2_one" id="phone2" value="" class="text-input valid" style="text-align: center; max-width: 40px ! important;"> -
<input type="text" maxlength="4" name="phone3_one" id="phone3" value="" class="text-input valid" style="text-align: center; max-width: 60px;">
</div>
</div>	<!--row-->
<label for="commentsText">Comments/Questions</label>
<!--Comment-->
<textarea name="commentsText_one" id="commentsText" rows="5" cols="30"><?php if(isset($_POST['commentsText_one'])) { if(function_exists('commentsText_one')) { echo stripslashes($_POST['commentsText_one']); } else { echo $_POST['commentsText_one']; } } ?></textarea><br />
<?php if($commentError != '') { ?><!--Show error message-->
<span style="color:#FF0000;"><?=$commentError;?></span>
<?php } ?>
<input type="submit" value="Submit" class="frm_submit" name="submitloading"/>
</fieldset>
</form>
</div>
</div>	<!--landing_form-->
<div class="clearfix"></div>
<div class="container">
<div class="twelve columns">
<div id="contactform">
<form action="<?php the_permalink(); ?>" id="contactForm" method="post" class="contactform_void">
<fieldset>
<div class="row">
<div class="fl mr_13">
<label for="first_name">First Name:&nbsp;<em>(Required)</em></label>
<input type="text" name="first_name" id="first_name" value="<?php if(isset($_POST['first_name'])) echo $_POST['first_name'];?>" class="text-input" /><br>
<?php if($fnameError != '') { ?><!--Show error message-->
<span style="color:#FF0000;"><?=$fnameError;?></span>
<?php } ?>
</div>	<!--fl-->
<div class="fl">
<label for="lName">Last Name:&nbsp;<em>(Required)</em></label>
<input type="text" name="last_name" id="last_name" value="<?php if(isset($_POST['last_name'])) echo $_POST['last_name'];?>" class="text-input" /><br>
<?php if($lnameError != '') { ?><!--Show error message-->
<span style="color:#FF0000;"><?=$lnameError;?></span>
<?php } ?>
</div>	<!--fl-->
</div>	<!--row-->
<div class="row">
<div class="fl mr_13" style="width: 313px ! important;">
<label for="phone">Phone:&nbsp;</label>
<?php /*?><input type="text" name="phone" id="phone" value="<?php //if(isset($_POST['email']))  echo $_POST['email'];?>" class="text-input" /><?php */?>
<input type="text" maxlength="3" name="phone1" id="phone1" value="" class="text-input valid" style="text-align: center; max-width: 40px ! important;"> -
<input type="text" maxlength="3" name="phone2" id="phone2" value="" class="text-input valid" style="text-align: center; max-width: 40px ! important;"> -
<input type="text" maxlength="4" name="phone3" id="phone3" value="" class="text-input valid" style="text-align: center; max-width: 60px;">
</div>
<div class="fl">
<!--Contact Email-->  <label for="email">Email:&nbsp;<em>(Required)</em></label>
<input type="text" name="email" id="email" value="<?php if(isset($_POST['email']))  echo $_POST['email'];?>" class="text-input" /> <br>
<?php if($emailError != '')	{ ?><!--Show error message-->
<span style="color:#FF0000;"><?=$emailError;?></span>
<?php } ?>
</div>	<!--fl-->
</div>	<!--row-->
<div class="row">
<div class="fl mr_13">
<label for="company">Company:&nbsp;</label>
<input type="text" name="company" id="company" value="<?php //if(isset($_POST['email']))  echo $_POST['email'];?>" class="text-input" />
</div>	<!--fl-->
<div class="fl">
<label for="website">Website:&nbsp;</label>
<input type="text" name="website" id="website" value="<?php //if(isset($_POST['email']))  echo $_POST['email'];?>" class="text-input" />
</div><!--fl-->
</div>	<!--row-->
<div class="row">
<div class="fl mr_13">
<label for="budget">Estimated Budget:&nbsp;</label>
<?php /*?><input type="text" name="phone" id="phone" value="<?php if(isset($_POST['email']))  echo $_POST['email'];?>" class="text-input" /><?php */?>
<select class="selectbox" name="budget" id="budget">
<option value="Select a budget" selected="selected"> Select a budget </option>
<option value="$0-$10,000">$0-$10,000</option>
<option value="$10,000-$25,000">$10,000-$25,000</option>
<option value="$25,000-$50,000">$25,000-$50,000</option>
</select>
</div>	<!--fl-->
<!--<div class="fl">
<label for="source">How did you hear about us?:&nbsp;</label>
<input type="text" name="source" id="source" value="<?php //if(isset($_POST['email']))  echo $_POST['email'];?>" class="text-input" />
</div>--><!--fl-->
</div>	<!--row-->
<label for="commentsText">Comments/Questions</label>
<!--Comment-->
<textarea name="commentsText" id="commentsText" rows="5" cols="30"><?php if(isset($_POST['commentsText'])) { if(function_exists('stripslashes')) { echo stripslashes($_POST['commentsText']); } else { echo $_POST['commentsText']; } } ?></textarea><br />
<?php if($commentError != '') { ?><!--Show error message-->
<span style="color:#FF0000;"><?=$commentError;?></span>
<?php } ?>
<!-- recaptch -->
<?php /*?><?php
// load recaptcha file
require_once('captcha/recaptchalib.php');
// enter your public key
//$publickey = "6Lf6JeASAAAAAMkZN1lnv_-1nSnh54ZHSA_q-2hn";
$publickey = "6Lc8JuASAAAAALB64z4ZavlHYM7G2NTxtICXcdnS";
// display recaptcha test fields
echo recaptcha_get_html($publickey, $resp->error);
?><?php */?>
<input type="submit" name="submitcontact" class="frm_submit" id="contact_frm_submit" value="Submit" />
<input type="hidden" name="submitted" id="submitted" value="true" /><!--Submit the form here-->
</fieldset>
</form>
</div><!-- end #contactform -->
</div>	<!--six columns-->
<!--form ends here-->
</div>	<!--container-->
<div class="clearfix heig_10"></div>
<?php //get_footer(); ?>
<!-- FADING Footer starts -->
<div class="footer_btm" <?php //if ( is_front_page())  echo 'id="fade"'; else echo 'id="fade"'; ?>>
<div class="footer_btm_inner container">
<div class="row">
<div class="five columns">
<div id="powered">
<div class="connect_us"> <span>CONNECT WITH US</span>
<a href="http://www.facebook.com/pages/Dotcomweavers/124236440928166" class="header_soc_fb" title="facebook">Facebook</a>
<a href="http://twitter.com/dotcomweavers" class="header_soc_twitter" title="Twitter">Twitter</a>
<a href="https://plus.google.com/107414909360503446764" rel="publisher" title="Google+" class="footer_google_plus">Google+</a>
<a href="http://www.dotcomweavers.com/feed" class="header_soc_rss" title="RSS">RSS</a>
</div>
<!--<div class="connect_us">COMPNAY</div>-->
</div>
</div>
<?php /*?><div class="seven columns footer_btm_inner">
<?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'div',
'container_class' => 'ten columns footer_btm_inner','menu' => 'stickyfooter','menu_class'      => '', ) ); ?>
</div><?php */?>
<!--footer_btn_inner-->
<?php /*?><div class="three columns fr"> <a href="<?php echo get_permalink( get_page_by_title( 'Contact Us' ) ); ?>?refid=<?php echo $post->ID; ?>" class="btn btn-small btn-success">Contact Us</a> </div><?php */?>
</div>
<!--row-->
</div>
<!--footer_btm_inner-->
</div>
<!-- FADING Footer ends -->