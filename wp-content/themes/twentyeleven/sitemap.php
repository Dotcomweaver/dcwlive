<?php
/*
 Template Name: Sitemap Page
*/
get_header(); 
?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
							} ?>
                	<h1><?php the_title(); ?></h1>
                	<?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?>
              </div>
          </div>
        </div>
</section>

<section class="light-gray-wraper">
        <div class="container">
        	<div class="row mtop40">
            	<div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Main Navigation</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'topmenu' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Work</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'work' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Company</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'company' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                <div class="clearfix"></div>
                
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Resources</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'Resources' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                <div class="col-sm-8">
                	<div class="panel panel-default">
                      <div class="panel-heading">Services</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'services-sitemap' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
            </div><!-- /row -->
            <?php /*?><div class="row mtop40">
                 
                <ul>
                <?php
                // Add pages you'd like to exclude in the exclude here
                wp_list_pages(
                  array(
                    'exclude' => '',
                    'title_li' => '',
                  )
                );
                ?>
                </ul>
                 
                
                <?php
                foreach( get_post_types( array('public' => true, 'post_type' => 'page') ) as $post_type ) {
                  if ( in_array( $post_type, array('post','page','attachment') ) )
                    continue;
                 
                  $pt = get_post_type_object( $post_type );
                 
                  echo '<h2>'.$pt->labels->name.'</h2>';
                  echo '<ul>';
                 
                  query_posts('post_type='.$post_type.'&posts_per_page=-1');
                  while( have_posts() ) {
                    the_post();
                    echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
                  }
                 
                  echo '</ul>';
                }
                ?> 
		</div><?php */?>
    </div>
</section>
       
<?php //comments_template( '', true ); ?>
<?php //if ( ! dynamic_sidebar( 'service_parallax' ) ) : ?>
<?php //endif; // custom widget ?>
<?php get_footer(); ?>