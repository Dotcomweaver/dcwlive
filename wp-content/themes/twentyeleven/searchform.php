<?php
/**
 * The template for displaying search forms in Twenty Eleven
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
<div class="search_wrap">
<div class="sear_m">	
<label for="s" class="assistive-text"><?php _e( 'Search', 'twentyeleven' ); ?></label>
<input type="text" class="inputfield_b2" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'twentyeleven' ); ?>" />
<input type="submit" class="go_btn" name="submit" id="searchsubmit" value="<?php esc_attr_e( '', 'twentyeleven' ); ?>" />
</div>	<!--sear_m-->
</div>	<!--search_wrap-->
</form>


