<?php
/**
* Template Name: design-form
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					} ?>
                  <span class="fa fa-cog"></span>
               	  <h1><?php the_title()?></h1>
                  <p>We are a full-service web design and development agency with over a decade of successful projects. Our customers come to us because we listen to them, do the research, and create the solutions they need at rates they can afford.</p>
              </div>
          </div>
        </div>
</section>
<section class="light-gray-wraper">
<div class="container">
<div class="row">
<div class="col-md-12">
	<?php while ( have_posts() ) : the_post(); ?>
	<?php echo the_content(); ?>
    <?php endwhile; // end of the loop. ?>
</div>
</div>
</div>
</section>
<!--#innerpage_bg-->
<?php get_footer(); ?>