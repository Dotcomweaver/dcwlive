<?php
/**
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>

<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12"><h1 class="port-title"><?php echo the_title(); ?></h1>
                  <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?>
              </div>
          </div>
        </div>
</section>
<?php /*?><?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>
<?php get_sidebar(); ?><?php */?>

<?php while ( have_posts() ) : the_post(); ?>
<?php the_content();//get_template_part( 'content', 'page' ); ?>
<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>