<?php
/**
* Template Name: Press Releases
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); 
?>
<section class="inner-header-company">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
							} ?>
                	<h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
                	
              </div>
          </div>
        </div>
</section>
<section id="secondnav" class="company">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="navbar secondary-nav navbar-default" role="navigation">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".second-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="#">Navigation</a>
                        </div>
                        <div class="navbar-collapse second-nav collapse">
                          <?php
							wp_nav_menu( array(
												  'menu' => 'company',
												  'depth' => 2,
												  'container' => false,
												  'menu_class' => 'nav navbar-nav',
												  //Process nav menu using our custom nav walker
												  'walker' => new wp_bootstrap_navwalker())
												);
						 
						?>
                       </div><!--/.nav-collapse -->
                    </div>
                                
                </div>
            </div>
        </div>
</section>
<section class="light-gray-wraper divider">
  <div class="container" id="container-blog">

<?php 
$args = array('post_type'=>'post','tag'=>'press-awards','posts_per_page'=>'10');
query_posts($args);
if(have_posts()): ?>
     <div id="posts" class="row press">
			<?php
				while ( have_posts() ) : the_post(); 
						get_template_part( 'presscontent', get_post_format() );
				
				endwhile; // end of the loop. 
             ?>
      </div>
<?php if(function_exists('wp_paginate')) {
                                            wp_paginate();
                                            } 
        ?>
<?php endif; ?>
</div>
</section>
<?php get_footer(); ?>