<?php
function mysession(){
	if(session_id() == ''){
		session_start();
		$_SESSION['sess_id'] = session_id();
	}
}
add_action( 'init', 'mysession' );
/**
* Twenty Eleven functions and definitions
*
* Sets up the theme and provides some helper functions. Some helper functions
* are used in the theme as custom template tags. Others are attached to action and
* filter hooks in WordPress to change core functionality.
*
* The first function, twentyeleven_setup(), sets up the theme by registering support
* for various features in WordPress, such as post thumbnails, navigation menus, and the like.
*
* When using a child theme (see http://codex.wordpress.org/Theme_Development and
* http://codex.wordpress.org/Child_Themes), you can override certain functions
* (those wrapped in a function_exists() call) by defining them first in your child theme's
* functions.php file. The child theme's functions.php file is included before the parent
* theme's file, so the child theme functions would be used.
*
* Functions that are not pluggable (not wrapped in function_exists()) are instead attached
* to a filter or action hook. The hook can be removed by using remove_action() or
* remove_filter() and you can attach your own function to the hook.
*
* We can remove the parent theme's hook only after it is attached, which means we need to
* wait until setting up the child theme:
*
* <code>
* add_action( 'after_setup_theme', 'my_child_theme_setup' );
* function my_child_theme_setup() {
*     // We are providing our own filter for excerpt_length (or using the unfiltered value)
*     remove_filter( 'excerpt_length', 'twentyeleven_excerpt_length' );
*     ...
* }
* </code>
*
* For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
/**
* Set the content width based on the theme's design and stylesheet.
*/
if ( ! isset( $content_width ) )
	$content_width = 584;
/**
* Tell WordPress to run twentyeleven_setup() when the 'after_setup_theme' hook is run.
*/
add_action( 'after_setup_theme', 'twentyeleven_setup' );
if ( ! function_exists( 'twentyeleven_setup' ) ):
/**
* Sets up theme defaults and registers support for various WordPress features.
*
* Note that this function is hooked into the after_setup_theme hook, which runs
* before the init hook. The init hook is too late for some features, such as indicating
* support post thumbnails.
*
* To override twentyeleven_setup() in a child theme, add your own twentyeleven_setup to your child theme's
* functions.php file.
*
* @uses load_theme_textdomain() For translation/localization support.
* @uses add_editor_style() To style the visual editor.
* @uses add_theme_support() To add support for post thumbnails, automatic feed links, custom headers
* 	and backgrounds, and post formats.
* @uses register_nav_menus() To add support for navigation menus.
* @uses register_default_headers() To register the default custom header images provided with the theme.
* @uses set_post_thumbnail_size() To set a custom post thumbnail size.
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_content_nav( $nav_id ) {
	global $wp_query;
	if ( function_exists( 'wp_paginate' ) ) {
		wp_paginate();
	}
	else {
		if ( $wp_query->max_num_pages > 1 ) : ?>
			<nav id="<?php echo $nav_id; ?>">
			<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentyeleven' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyeleven' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></div>
			</nav><!-- #nav-above -->
		<?php endif;
	}
}
function twentyeleven_setup() {
	/* Make Twenty Eleven available for translation.
	* Translations can be added to the /languages/ directory.
	* If you're building a theme based on Twenty Eleven, use a find and replace
	* to change 'twentyeleven' to the name of your theme in all the template files.
	*/
	load_theme_textdomain( 'twentyeleven', get_template_directory() . '/languages' );
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	// Load up our theme options page and related code.
	require( get_template_directory() . '/inc/theme-options.php' );
	// Grab Twenty Eleven's Ephemera widget.
	require( get_template_directory() . '/inc/widgets.php' );
	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'twentyeleven' ) );
	// Add support for a variety of post formats
	add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'status', 'quote', 'image','audio' ) );
	$theme_options = twentyeleven_get_theme_options();
	if ( 'dark' == $theme_options['color_scheme'] )
	$default_background_color = '1d1d1d';
	else
	$default_background_color = 'e2e2e2';
	// Add support for custom backgrounds.
	add_theme_support( 'custom-background', array(
	// Let WordPress know what our default background color is.
	// This is dependent on our current color scheme.
	'default-color' => $default_background_color,
	) );
	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );
	// Add support for custom headers.
	$custom_header_support = array(
	// The default header text color.
	'default-text-color' => '000',
	// The height and width of our custom header.
	'width' => apply_filters( 'twentyeleven_header_image_width', 1000 ),
	'height' => apply_filters( 'twentyeleven_header_image_height', 288 ),
	// Support flexible heights.
	'flex-height' => true,
	// Random image rotation by default.
	'random-default' => true,
	// Callback for styling the header.
	'wp-head-callback' => 'twentyeleven_header_style',
	// Callback for styling the header preview in the admin.
	'admin-head-callback' => 'twentyeleven_admin_header_style',
	// Callback used to display the header preview in the admin.
	'admin-preview-callback' => 'twentyeleven_admin_header_image',
	);
	add_theme_support( 'custom-header', $custom_header_support );
	if ( ! function_exists( 'get_custom_header' ) ) {
	// This is all for compatibility with versions of WordPress prior to 3.4.
	define( 'HEADER_TEXTCOLOR', $custom_header_support['default-text-color'] );
	define( 'HEADER_IMAGE', '' );
	define( 'HEADER_IMAGE_WIDTH', $custom_header_support['width'] );
	define( 'HEADER_IMAGE_HEIGHT', $custom_header_support['height'] );
	add_custom_image_header( $custom_header_support['wp-head-callback'], $custom_header_support['admin-head-callback'], $custom_header_support['admin-preview-callback'] );
	add_custom_background();
	}
	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be the size of the header image that we just defined
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( $custom_header_support['width'], $custom_header_support['height'], true );
	// Add Twenty Eleven's custom image sizes.
	// Used for large feature (header) images.
	add_image_size( 'large-feature', $custom_header_support['width'], $custom_header_support['height'], true );
	// Used for featured posts if a large-feature doesn't exist.
	add_image_size( 'small-feature', 500, 300 );
	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
	'wheel' => array(
	'url' => '%s/images/headers/wheel.jpg',
	'thumbnail_url' => '%s/images/headers/wheel-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Wheel', 'twentyeleven' )
	),
	'shore' => array(
	'url' => '%s/images/headers/shore.jpg',
	'thumbnail_url' => '%s/images/headers/shore-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Shore', 'twentyeleven' )
	),
	'trolley' => array(
	'url' => '%s/images/headers/trolley.jpg',
	'thumbnail_url' => '%s/images/headers/trolley-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Trolley', 'twentyeleven' )
	),
	'pine-cone' => array(
	'url' => '%s/images/headers/pine-cone.jpg',
	'thumbnail_url' => '%s/images/headers/pine-cone-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Pine Cone', 'twentyeleven' )
	),
	'chessboard' => array(
	'url' => '%s/images/headers/chessboard.jpg',
	'thumbnail_url' => '%s/images/headers/chessboard-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Chessboard', 'twentyeleven' )
	),
	'lanterns' => array(
	'url' => '%s/images/headers/lanterns.jpg',
	'thumbnail_url' => '%s/images/headers/lanterns-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Lanterns', 'twentyeleven' )
	),
	'willow' => array(
	'url' => '%s/images/headers/willow.jpg',
	'thumbnail_url' => '%s/images/headers/willow-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Willow', 'twentyeleven' )
	),
	'hanoi' => array(
	'url' => '%s/images/headers/hanoi.jpg',
	'thumbnail_url' => '%s/images/headers/hanoi-thumbnail.jpg',
	/* translators: header image description */
	'description' => __( 'Hanoi Plant', 'twentyeleven' )
	)
	) );
}
endif; // twentyeleven_setup
if ( ! function_exists( 'twentyeleven_header_style' ) ) :
/**
* Styles the header image and text displayed on the blog
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_header_style() {
	$text_color = get_header_textcolor();
	// If no custom options for text are set, let's bail.
	if ( $text_color == HEADER_TEXTCOLOR )
	return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
	// Has the text been hidden?
	if ( 'blank' == $text_color ) :
	?>
	#site-title,
	#site-description {
	position: absolute !important;
	clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
	clip: rect(1px, 1px, 1px, 1px);
	}
	<?php
	// If the user has set a custom color for the text use that
	else :
	?>
	#site-title a,
	#site-description {
	color: #<?php echo $text_color; ?> !important;
	}
	<?php endif; ?>
	</style>
	<?php
}
endif; // twentyeleven_header_style
if ( ! function_exists( 'twentyeleven_admin_header_style' ) ) :
/**
* Styles the header image displayed on the Appearance > Header admin panel.
*
* Referenced via add_theme_support('custom-header') in twentyeleven_setup().
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_admin_header_style() {
	?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
	border: none;
	}
	#headimg h1,
	#desc {
	font-family: "Helvetica Neue", Arial, Helvetica, "Nimbus Sans L", sans-serif;
	}
	#headimg h1 {
	margin: 0;
	}
	#headimg h1 a {
	font-size: 32px;
	line-height: 36px;
	text-decoration: none;
	}
	#desc {
	font-size: 14px;
	line-height: 23px;
	padding: 0 0 3em;
	}
	<?php
	// If the user has set a custom color for the text use that
	if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
	#site-title a,
	#site-description {
	color: #<?php echo get_header_textcolor(); ?>;
	}
	<?php endif; ?>
	#headimg img {
	max-width: 1000px;
	height: auto;
	width: 100%;
	}
	</style>
	<?php
}
endif; // twentyeleven_admin_header_style
if ( ! function_exists( 'twentyeleven_admin_header_image' ) ) :
/**
* Custom header image markup displayed on the Appearance > Header admin panel.
*
* Referenced via add_theme_support('custom-header') in twentyeleven_setup().
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_admin_header_image() { ?>
    <div id="headimg">
    <?php
    $color = get_header_textcolor();
    $image = get_header_image();
    if ( $color && $color != 'blank' )
    $style = ' style="color:#' . $color . '"';
    else
    $style = ' style="display:none"';
    ?>
    <h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
    <div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
    <?php if ( $image ) : ?>
    <img src="<?php echo esc_url( $image ); ?>" alt="" />
    <?php endif; ?>
    </div>
<?php }
endif; // twentyeleven_admin_header_image
/**
* Sets the post excerpt length to 40 words.
*
* To override this length in a child theme, remove the filter and add your own
* function tied to the excerpt_length filter hook.
*/
function twentyeleven_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'twentyeleven_excerpt_length' );
if ( ! function_exists( 'twentyeleven_continue_reading_link' ) ) :
/**
* Returns a "Continue Reading" link for excerpts
*/
function twentyeleven_continue_reading_link() {
	return ' <a href="'. esc_url( get_permalink() ) . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) . '</a>';
}
endif; // twentyeleven_continue_reading_link
/**
* Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyeleven_continue_reading_link().
*
* To override this in a child theme, remove the filter and add your own
* function tied to the excerpt_more filter hook.
*/
function twentyeleven_auto_excerpt_more( $more ) {
	return ' &hellip;' . twentyeleven_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyeleven_auto_excerpt_more' );
/**
* Adds a pretty "Continue Reading" link to custom post excerpts.
*
* To override this link in a child theme, remove the filter and add your own
* function tied to the get_the_excerpt filter hook.
*/
function twentyeleven_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= twentyeleven_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'twentyeleven_custom_excerpt_more' );
/**
* Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
*/
function twentyeleven_page_menu_args( $args ) {
if ( ! isset( $args['show_home'] ) )
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'twentyeleven_page_menu_args' );
/**
* Register our sidebars and widgetized areas. Also register the default Epherma widget.
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_widgets_init() {
	register_widget( 'Twenty_Eleven_Ephemera_Widget' );
	register_sidebar( array(
	'name' => __( 'Main Sidebar', 'twentyeleven' ),
	'id' => 'sidebar-1',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'About Us', 'twentyeleven' ),
	'id' => 'sidebar-about-us',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Blog Sidebar', 'twentyeleven' ),
	'id' => 'sidebar-2',
	'description' => __( 'The sidebar for the Blog Pages', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Footer Area One', 'twentyeleven' ),
	'id' => 'sidebar-3',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title hide">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Contact Form Text', 'twentyeleven' ),
	'id' => 'sidebar-4',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Footer Area Three', 'twentyeleven' ),
	'id' => 'sidebar-5',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Footer Area Four', 'twentyeleven' ),
	'id' => 'sidebar-6',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget col-md-3 col-sm-12 %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Homecontact', 'twentyeleven' ),
	'id' => 'contact_info',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Contact Address', 'twentyeleven' ),
	'id' => 'contactaddress',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'SlideContent', 'twentyeleven' ),
	'id' => 'slidecontent',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Box Content', 'twentyeleven' ),
	'id' => 'boxwrapper',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title hide">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Latest Tweets', 'twentyeleven' ),
	'id' => 'latest_tweets',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Sidebar', 'twentyeleven' ),
	'id' => 'leftside',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Footer Address', 'twentyeleven' ),
	'id' => 'footer_address',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Blog Title', 'twentyeleven' ),
	'id' => 'blogheads',
	'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'Services', 'twentyeleven' ),
	'id' => 'services',
	'description' => __( 'An optional widget area for all services pages', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => __( 'googleplus', 'twentyeleven' ),
	'id' => 'googleplus',
	'description' => __( 'An optional widget area for all services pages', 'twentyeleven' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => "</aside>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'twentyeleven_widgets_init' );
if ( ! function_exists( 'twentyeleven_content_nav' ) ) :
/**
* Display navigation to next/previous pages when applicable
*/
function twentyeleven_content_nav( $html_id ) {
	global $wp_query;
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<nav id="<?php echo esc_attr( $html_id ); ?>">
	<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentyeleven' ); ?></h3>
	<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyeleven' ) ); ?></div>
	<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></div>
	</nav><!-- #nav-above -->
	<?php endif;
}
endif; // twentyeleven_content_nav
/**
* Return the URL for the first link found in the post content.
*
* @since Twenty Eleven 1.0
* @return string|bool URL or false when no link is present.
*/
function twentyeleven_url_grabber() {
	if ( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches ) )
	return false;
	return esc_url_raw( $matches[1] );
}
/**
* Count the number of footer sidebars to enable dynamic classes for the footer
*/
function twentyeleven_footer_sidebar_class() {
	$count = 0;
	if ( is_active_sidebar( 'sidebar-3' ) )
	$count++;
	if ( is_active_sidebar( 'sidebar-4' ) )
	$count++;
	if ( is_active_sidebar( 'sidebar-5' ) )
	$count++;
	$class = '';
	switch ( $count ) {
	case '1':
	$class = 'one';
	break;
	case '2':
	$class = 'two';
	break;
	case '3':
	$class = 'three';
	break;
	}
	if ( $class )
	echo 'class="' . $class . '"';
}
if ( ! function_exists( 'twentyeleven_comment' ) ) :
/**
* Template for comments and pingbacks.
*
* To override this walker in a child theme without modifying the comments template
* simply create your own twentyeleven_comment(), and that function will be used instead.
*
* Used as a callback by wp_list_comments() for displaying the comments.
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
	case 'pingback' :
	case 'trackback' :
	?>
	<li class="post pingback">
	<p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
	break;
	default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
	<article id="comment-<?php comment_ID(); ?>" class="comment">
	<footer class="comment-meta">
		<div class="comment-author vcard">
			<?php
				$avatar_size = 68;
				if ( '0' != $comment->comment_parent )
					$avatar_size = 39;
				echo get_avatar( $comment, $avatar_size );
				/* translators: 1: comment author, 2: date and time */
				printf( __( '%1$s on %2$s <span class="says">said:</span>', 'twentyeleven' ),
					sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
					sprintf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'twentyeleven' ), get_comment_date(), get_comment_time() )
					)
				);
			?>
			<?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentyeleven' ); ?></em>
			<br />
		<?php endif; ?>
	</footer>
	<div class="comment-content"><?php comment_text(); ?></div>
	<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'twentyeleven' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
	</div><!-- .reply -->
	</article><!-- #comment-## -->
	<?php
	break;
	endswitch;
}
endif; // ends check for twentyeleven_comment()
if ( ! function_exists( 'twentyeleven_posted_on' ) ) :
/**
* Prints HTML with meta information for the current post-date/time and author.
* Create your own twentyeleven_posted_on to override in a child theme
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_posted_on() {
	printf( __( '<span class="sep"></span><a href="%1$s" title="%2$s" rel="bookmark" style="float: right;  text-transform: uppercase;"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="by-author"> <span class="sep"></span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'twentyeleven' ),
	esc_url( get_permalink() ),
	esc_attr( get_the_time() ),
	esc_attr( get_the_date( 'c' ) ),
	esc_html( get_the_date() ),
	esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
	esc_attr( sprintf( __( 'View all posts by %s', 'twentyeleven' ), get_the_author() ) ),
	get_the_author()
	);
}
endif;
/**
* Adds two classes to the array of body classes.
* The first is if the site has only had one author with published posts.
* The second is if a singular post being displayed
*
* @since Twenty Eleven 1.0
*/
function twentyeleven_body_classes( $classes ) {
	if ( function_exists( 'is_multi_author' ) && ! is_multi_author() )
	$classes[] = 'single-author';
	if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
	$classes[] = 'singular';
	return $classes;
}
add_filter( 'body_class', 'twentyeleven_body_classes' );
if( ! function_exists ( 'get_ytube_video_code' ) ) :
function get_ytube_video_code($url){
	$image_url = parse_url($url);
	if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
	$array = explode("&", $image_url['query']);
	//return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
	return substr($array[0], 2);
	}
}
endif;
if( ! function_exists ( 'get_ytube_video_embed_url' ) ) :
function get_ytube_video_embed_url($url){
	$image_url = parse_url($url);
	if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
	$array = explode("=", $image_url['query']);
	//return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
	return $array[1];
	}
}
endif;
// --------------------------------------------------------------------------------- WT hooks
function menu_name_meta_box($post)
{
	$menu_name_post_meta = json_decode(get_post_meta($post->ID,'menu_name_post_meta', true));
	$page_sidebar_id = $menu_name_post_meta->page_sidebar_id;
	?>
	<table>
	<tr>
	<td><label for="page_sidebar_id">Select Sidebar:</label>
	<select name="page_sidebar_id" id="page_sidebar_id">
	<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { ?>
		 <option value="<?php echo $sidebar['id'] ; ?>"<?php echo ($page_sidebar_id == $sidebar['id'] )?' selected="selected"':'' ?>>
				  <?php echo ucwords( $sidebar['name'] ); ?>
		 </option>
	<?php } ?>
	</select>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_menu_name_metabox');
function add_menu_name_metabox() {
	add_meta_box('menunamediv', __('Page Sidebar Name'), 'menu_name_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','menu_name_update_post',1,2);
function menu_name_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Make sure our flag is in there, otherwise it's an autosave and we should bail.
	if($post_id) {
	// Logic to handle specific post types
		switch($post_type) {
			// If this is a post. You can change this case to reflect your custom post slug
			case 'page':
				//print_r($_POST);exit(0); UNCOMMENT TO CHECK :)
				$menu_name_post_meta = array();
				$page_sidebar_id = strip_tags($_POST['page_sidebar_id']);
				$menu_name_post_meta['page_sidebar_id'] = $page_sidebar_id;
				$menu_name_post_meta = json_encode($menu_name_post_meta);
				update_post_meta($post_id,'menu_name_post_meta', $menu_name_post_meta);
				break;
			default:
		} // End switch
	return;
	} // End if manual save flag
	return;
}
function services_extra_meta_box($post) {
	$services_extra_post_meta = json_decode(get_post_meta($post->ID,'services_extra_post_meta', true));
	$v_testimonials_head_text = $services_extra_post_meta->v_testimonials_head_text;
	$services_video_testimonials = $services_extra_post_meta->services_video_testimonials;
	?>
	<table>
	<tr>
	<td><label for="v_testimonials_head_text"><b>Video Testimonials Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="v_testimonials_head_text" id="v_testimonials_head_text" cols="80" rows="3"><?php echo $v_testimonials_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="v_testimonials"><b>Video Testimonials</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_case_studies = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'video-testimonials',
	)
	);
	$all_case_studies = array('Select Video Testimonial' => ' ');
	$svt_counter = 0;
	foreach($arr_case_studies as  $indv_case_studies)
	{
	$svt_counter++;
	$checked = (isset($services_video_testimonials) && in_array($indv_case_studies->ID, $services_video_testimonials))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_video_testimonials[]" id="vt_'.$indv_case_studies->ID.'" value="'.$indv_case_studies->ID.'" />
	<label for="vt_'. $indv_case_studies->ID .'">'. $indv_case_studies->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_extra_metabox');
function add_services_extra_metabox() {
	add_meta_box('postparentdiv', __('Services Video Testimonials'), 'services_extra_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_extra_update_post',1,2);
function services_extra_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_extra_post_meta = array();
		$services_extra_post_meta['v_testimonials_head_text'] = esc_attr($_POST['v_testimonials_head_text']);
		$services_extra_post_meta['services_video_testimonials'] = $_POST['services_video_testimonials'];
		$services_extra_post_meta = stripslashes(json_encode($services_extra_post_meta));
		update_post_meta($post_id,'services_extra_post_meta', $services_extra_post_meta);
	break;
	default:
	} // End switch
	return;
}
// ---------------------------------------------------------------------------------
function services_folio_meta_box($post) {
	$services_folio_post_meta = json_decode(get_post_meta($post->ID,'services_folio_post_meta', true));
	$folio_head_text = $services_folio_post_meta->folio_head_text;
	$services_folio = $services_folio_post_meta->services_folio;
	?>
	<table>
	<tr>
	<td><label for="folio_head_text"><b>Portfolios Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="folio_head_text" id="folio_head_text" cols="80" rows="3"><?php echo $folio_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="folio"><b>Portfolios</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_portfolios = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'portfolio',
	)
	);
	$svt_counter = 0;
	foreach($arr_portfolios as  $indv_portfolios)
	{
	$svt_counter++;
	$checked = (isset($services_folio) && in_array($indv_portfolios->ID, $services_folio))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_folio[]" id="folio_'.$indv_portfolios->ID.'" value="'.$indv_portfolios->ID.'" />
	<label for="folio_'. $indv_portfolios->ID .'">'. $indv_portfolios->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_folio_metabox');
function add_services_folio_metabox() {
	add_meta_box('servicesfoliodiv', __('Services Portfolios'), 'services_folio_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_folio_update_post',1,2);
function services_folio_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_folio_post_meta = array();
		$services_folio_post_meta['folio_head_text'] = $_POST['folio_head_text'];
		$services_folio_post_meta['services_folio'] = $_POST['services_folio'];
		$services_folio_post_meta = json_encode($services_folio_post_meta);
		update_post_meta($post_id,'services_folio_post_meta', $services_folio_post_meta);
	break;
	default:
	} // End switch
	return;
}
//----------------------------------------------------------------------------------
function services_faq_meta_box($post) {
	$faq_post_meta = get_post_meta($post->ID,'services_faq_post_meta',true);
	//var_dump($faq_post_meta);
	$services_faq_post_meta = unserialize(base64_decode($faq_post_meta));
	//$services_faq_post_meta = json_decode(get_post_meta($post->ID,'services_faq_post_meta', true));
	//var_dump($services_faq_post_meta);
	$faq_head_text = $services_faq_post_meta['faq_head_text'];
	$services_faq = $services_faq_post_meta['services_faqs'];
	//echo count($services_faq);
	?>
	<table>
	<tr>
	<td><label for="faq_head_text"><b>FAQS Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="faq_head_text" id="faq_head_text" cols="80" rows="3"><?php echo $faq_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="faq"><b>FAQS</b>:</label></td>
	</tr>
	<tr>
	<td>
	<table colspan='0' border="0" width="100%">
	  <tr>
		<td>
		  <div class="listmetafaqs">
			  <?php 
			
			  if(isset($services_faq) && count($services_faq)>0){
				  $count = count($services_faq)-1;
				  foreach($services_faq as $fkey=>$fitem){ ?>
						<div class="faq<?php echo $fkey; ?>">
						<h3 class="listfaqs" id="faqhead_<?php echo $fkey; ?>"><span class="closed"><?php echo stripslashes($fitem['title']); ?></span><span>( + )</span></h3>
						<div class="faq_content" style="display:none;">
						<div class="faq_title">
								  <label for="faq title"><strong>Title :</strong></label>
									<input type="text" size="40" value="<?php if(isset($fitem['title'])){ echo stripslashes($fitem['title']); }?>" id="faqtitle_<?php echo $fkey; ?>" name="faqs[<?php echo $fkey; ?>][title]">
								</div>
						  <div class="faq_title">
								  <label description"="" for"faq="">Description : </label>
								  <textarea rows="8" cols="50" id="faqdesc_<?php echo $fkey; ?>" name="faqs[<?php echo $fkey; ?>][description]"><?php if(isset($fitem['description'])){ echo stripslashes($fitem['description']); }?></textarea>
								</div>
						  <div class="faq_title">
						  <button name="removeaccordian_<?php echo $fkey; ?>" id="removeaccordian_<?php echo $fkey; ?>" onclick="removeaccordian(<?php echo $fkey; ?>)">Remove</button>
						</div>
						<div class="clearfix"></div>
						</div>
					 </div>
			<?php  }
			  }else{
				   $count = 0;
			  ?>
			  <div class="faq0">
					<h3 class="listfaqs" id="faqhead_0"><span class="opened">Add New</span><span>( + )</span></h3>
					<div class="faq_content">
					<div class="faq_title">
							  <label for="faq title"><strong>Title :</strong></label>
								<input type="text" size="40" value="" id="faqtitle_0" name="faqs[0][title]">
							</div>
					  <div class="faq_title">
							  <label description"="" for"faq="">Description : </label>
							  <textarea rows="8" cols="50" id="faqdesc_0" name="faqs[0][description]"></textarea>
							</div>
					  <div class="faq_title">
					  <button name="removeaccordian_0" id="removeaccordian_0" onclick="removeaccordian(0)">Remove</button>
					</div>
					<div class="clearfix"></div>
					</div>
			 </div>
		 </div>
		 <?php } ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div class="addmorefaqcs">
				<a href="javascript:void(0)" name="csfaqsaddmore" rel="<?php echo $count; ?>" class="csfaqsaddmore">Add More</a>
			</div>
		</td>
	  </tr>
	  </table>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_faq_metabox');
function add_services_faq_metabox() {
	add_meta_box('servicesfaqdiv', __('Services Faqs'), 'services_faq_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_faq_update_post',1,2);
function services_faq_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		
		$services_faq_post_meta = array();
		$services_faq_post_meta['faq_head_text'] = $_POST['faq_head_text'];
		$services_faq_post_meta['services_faqs'] = $_POST['faqs'];
		
		//echo count($services_faq_post_meta['services_faqs']);
		$services_faq_post_meta = base64_encode(serialize($services_faq_post_meta));
		
		//exit;
		update_post_meta($post_id,'services_faq_post_meta', $services_faq_post_meta);
	break;
	default:
	} // End switch
	return;
}
add_action( 'admin_enqueue_scripts', 'admin_enqueue_so_19228543' );
function admin_enqueue_so_19228543()
{
   wp_enqueue_script( 
            'my-script', 
            site_url( '/wp-admin/js/acordian-custom.js', __FILE__ ), 
            array(), // dependencies
            false, // version
            true // on footer
    );
    wp_enqueue_style( 
        'my-style', site_url( '/wp-admin/css/accordian.css', __FILE__ ) 
    );
	wp_enqueue_style( 
        'font-awesome', get_template_directory( '/css/font-awesome.min.css', __FILE__ ) 
    );
}
// ---------------------------------------------------------------------------------
function services_case_meta_box($post) {
	$services_case_post_meta = json_decode(get_post_meta($post->ID,'services_case_post_meta', true));
	$case_head_text = $services_case_post_meta->case_head_text;
	$services_case = $services_case_post_meta->services_case;
	?>
	<table>
	<tr>
	<td><label for="case_head_text"><b>Case Studies Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="case_head_text" id="case_head_text" cols="80" rows="3"><?php echo $case_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="folio"><b>Case Studies</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_case = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'case-studies',
	)
	);
	$svt_counter = 0;
	foreach($arr_case as $indv_case)
	{
	$svt_counter++;
	$checked = (isset($services_case) && in_array($indv_case->ID, $services_case))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_case[]" id="case_'.$indv_case->ID.'" value="'.$indv_case->ID.'" />
	<label for="case_'. $indv_case->ID .'">'. $indv_case->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_case_metabox');
function add_services_case_metabox() {
	add_meta_box('servicescasediv', __('Services Cases'), 'services_case_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_case_update_post',1,2);
function services_case_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_case_post_meta = array();
		$services_case_post_meta['case_head_text'] = $_POST['case_head_text'];
		$services_case_post_meta['services_case'] = $_POST['services_case'];
		$services_case_post_meta = json_encode($services_case_post_meta);
		update_post_meta($post_id,'services_case_post_meta', $services_case_post_meta);
	break;
	default:
	} // End switch
	return;
}
// ---------------------------------------------------------------------------------
function services_blog_meta_box($post) {
	$services_blog_post_meta = json_decode(get_post_meta($post->ID,'services_blog_post_meta', true));
	$blog_head_text = $services_blog_post_meta->blog_head_text;
	$services_blog = $services_blog_post_meta->services_blog;
	?>
	<table>
	<tr>
	<td><label for="blog_head_text"><b>Blog Header Text</b>:</label></td>
	</tr>
	<tr>
	<td><textarea name="blog_head_text" id="blog_head_text" cols="80" rows="3"><?php echo $blog_head_text ?></textarea></td>
	</tr>
	<tr>
	<td><label for="folio"><b>Posts</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_blog = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'post',
	)
	);
	$svt_counter = 0;
	foreach($arr_blog as  $indv_blog)
	{
	$svt_counter++;
	$checked = (isset($services_blog) && in_array($indv_blog->ID, $services_blog))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_blog[]" id="blog_'.$indv_blog->ID.'" value="'.$indv_blog->ID.'" />
	<label for="blog_'. $indv_blog->ID .'">'. $indv_blog->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_blog_metabox');
function add_services_blog_metabox() {
	add_meta_box('servicesblogdiv', __('Services Blogs'), 'services_blog_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_blog_update_post',1,2);
function services_blog_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_blog_post_meta = array();
		$services_blog_post_meta['blog_head_text'] = $_POST['blog_head_text'];
		$services_blog_post_meta['services_blog'] = $_POST['services_blog'];
		$services_blog_post_meta = json_encode($services_blog_post_meta);
		update_post_meta($post_id,'services_blog_post_meta', $services_blog_post_meta);
	break;
	default:
	} // End switch
	return;
}
// JUMP-TO short code
function wt_jumpto_shortcode( $atts ){
	//$img-path = get_template_directory_uri() . '/images/jumpto.png';
	//$jump-to = 'foo bar';
	$chk_folio_post_meta = json_decode(get_post_meta(get_the_ID(),'services_folio_post_meta', true));
	$chk_folio_ids_arr = $chk_folio_post_meta->services_folio;
	$chk_sx_post_meta = json_decode(get_post_meta(get_the_ID(),'services_extra_post_meta', true));
	$chk_vt_ids_arr = $chk_sx_post_meta->services_video_testimonials;
	$chk_cs_post_meta = json_decode(get_post_meta(get_the_ID(),'services_case_post_meta', true));
	$chk_cs_ids_arr = $chk_cs_post_meta->services_case;
	$chk_blog_post_meta = json_decode(get_post_meta(get_the_ID(),'services_blog_post_meta', true));
	$chk_blog_ids_arr = $chk_blog_post_meta->services_blog;
	$btn_folio = '';
	$btn_v_testimonial = '';
	$btn_case_studies = '';
	$btn_blog = '';
	if (count($chk_folio_ids_arr)) :
	$btn_folio = '<a href="#portfolio" class="port_btn "><span>Portfolio</span></a>';
	endif;
	if (count($chk_vt_ids_arr)) :
	//$btn_v_testimonial = '<a href="#testimonials" class="testi_btn "><span>Testimonials</span></a>';
	endif;
	if (count($chk_cs_ids_arr)) :
	$btn_case_studies = '<a href="#casestudies" class="case_btn "><span>Case Studies</span></a>';
	endif;
	if (count($chk_blog_ids_arr)) :
	$btn_blog = '<a href="#blog" class="blg_btn "><span>Blog</span></a>';
	endif;
	if(	$btn_folio == '' && $btn_v_testimonial == '' && $btn_case_studies == '' && $btn_blog == '') :
	return ;
	else :
	return '<!--jump to section goes here-->
	<div class="clearfix"></div>
	<div class="two columns jumpto"> <img style="margin:2px 0 0;" src="' .get_template_directory_uri() .'/images/jumpto.png" alt=""/> </div>
	<div class="jump"> '. $btn_folio . $btn_v_testimonial . $btn_case_studies . $btn_blog .'
	  <div class="clearfix h15"></div>
	</div>
	<div class="clearfix"></div>
	<!--jump-->';
	endif;
}
add_shortcode( 'jumpto', 'wt_jumpto_shortcode' );
// JUMP-TO portfolio short code
function wt_jumpto_folio( $atts ){
	global $post;
	$folio_post = get_post();
	//if (end($folio_post->ancestors) == 506) :
	$folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
	//print_r($folio_post_meta);
	$folio_ids_arr = $folio_post_meta->services_folio;
	if (count($folio_ids_arr)) :
	
	$args = array(
					'post__in' => $folio_ids_arr,
					'post_type'=> 'portfolio',
					'order' => 'DESC',
					'posts_per_page' => -1
				);
	query_posts( $args );
	while ( have_posts() ) : the_post(); $post_id = $post->ID;
		$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
		//print_r($portfolio_post_meta);
		//$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
		$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'large');
		$the_link = get_permalink();
		$the_title = get_the_title();
		$the_content =  get_the_content('Read more');
		/*foreach( $portfolio_image_id_arr as $attach_img_id ) :
           $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
		break;
		endforeach;*/
		//print_r($attach_folio_img);
		$ret_val .= '<div class="col-sm-6">
						<div class="featured-box">
							<div class="overlay">
								<p class="case-txt"><a href="'.$the_link.'">'.$the_content.'</a></p>
								<div class="hover-overlay clearfix">
									<a href="'.$the_link.'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a>
								</div><!-- ./hover-overlay -->
							</div>
							<a href="'.$the_link.'">
								<img src="'.$portfolio_feat_image[0].'" class="img-responsive" alt="" />
							</a>
						</div>
					</div><!-- /.col-md-6 -->';
	endwhile; // end of the loop.
	wp_reset_query();
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptofolio', 'wt_jumpto_folio' );
function wt_jumpto_folio_newdesign( $atts ){
	global $post;
	$folio_post = get_post();
	//if (end($folio_post->ancestors) == 506) :
	$folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
	//print_r($folio_post_meta);
	$folio_ids_arr = $folio_post_meta->services_folio;
	if (count($folio_ids_arr)) :
	
	$args = array(
					'post__in' => $folio_ids_arr,
					'post_type'=> 'portfolio',
					'order' => 'DESC',
					'posts_per_page' => -1
				);
	query_posts( $args );
	$i = 1;
	while ( have_posts() ) : the_post(); $post_id = $post->ID;
		$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
		/*print_r($portfolio_post_meta);
		exit;*/
		//$portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_attached_image));
		$portfolio_feat_image = wp_get_attachment_image_src($portfolio_post_meta->services_portfolio_image,'full');
		$the_link = get_permalink();
		$the_title = get_the_title();
		$the_content =  $portfolio_post_meta->portfolio_content;
		/*foreach( $portfolio_image_id_arr as $attach_img_id ) :
           $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
		break;
		endforeach;*/
		//print_r($attach_folio_img);
		if($i%2 != 0){
			$ret_val .=  '<section class="light-gray-wraper">
							<div class="container">
								<div class="row folio-wraper">
									<div class="col-md-12 text-center">
										<div class="row ecm-client text-left mtop30">
											<div class="col-md-9 col-sm-6">
												<img src="'.$portfolio_feat_image[0].'" class="img-responsive">
											</div>
											<div class="col-md-6 col-sm-6 pull-right">
												<div class="main_data">
													<h3>'.$the_title.'</h3>
													<p>'.stripslashes(base64_decode($the_content)).'</p>
													<a href="'.$the_link.'" class="btn btn-warning">VIEW PROJECT</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>';
		}else{
			$ret_val .= '<section class="light-gray-wraper2 servicesfolio">
							<div class="container">
								<div class="row ecm-client text-left mtop50 mbt30">
									<div class="col-sm-6 fright">
										<img src="'.$portfolio_feat_image[0].'" class="img-responsive">
									</div>
									<div class="col-sm-6">
										<h3>'.$the_title.'</h3>
										<p>'.stripslashes(base64_decode($the_content)).'</p>
										<a href="'.$the_link.'" class="btn btn-warning">VIEW PROJECT</a>
									</div>
									
								</div>
							</div>
						</section>';
		}
		/*$ret_val .= '<div class="col-sm-6">
						<div class="featured-box">
							<div class="overlay">
								<p class="case-txt"><a href="'.$the_link.'">'.$the_content.'</a></p>
								<div class="hover-overlay clearfix">
									<a href="'.$the_link.'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a>
								</div><!-- ./hover-overlay -->
							</div>
							<a href="'.$the_link.'">
								<img src="'.$portfolio_feat_image[0].'" class="img-responsive" alt="" />
							</a>
						</div>
					</div><!-- /.col-md-6 -->';*/
	$i++;
	endwhile; // end of the loop.
	wp_reset_query();
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptoservicesfolio', 'wt_jumpto_folio_newdesign' );
// JUMP-TO video testimonial short code
function wt_jumpto_vt( $atts ){
	$vt_post = get_post();
	//if (end($vt_post->ancestors) == 506) :
	$sx_post_meta = json_decode(get_post_meta($vt_post->ID,'services_extra_post_meta', true));
	$vt_ids_arr = $sx_post_meta->services_video_testimonials;
	if (count($vt_ids_arr)) :
	$args = array(
					'post__in' => $vt_ids_arr,
				
					'post_type'=> 'video-testimonials',
				
					'order' => 'DESC',
				
					'posts_per_page' => -1
				
				);
	
	$vt_post = query_posts( $args );
	foreach ( $vt_post as $post ) :
			$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
			$v_code = get_ytube_video_code($v_test_post_meta->vt_url);
			$url = "http://youtube.com/vi/".$v_code."/0.jpg";
			$ret_val .= '<div class="col-md-3 col-sm-4 col-xs-6">
                        	<img src="http://img.youtube.com/vi/'.$v_code.'/0.jpg" alt="rycore-brand" class="img-responsive" /><a href="javascript:void();" data-target="#vt_modal"  data-toggle="modal" rel="http://www.youtube.com/embed/'.$v_code.'" class="vt_play"><span class="icomoon-icon-play-2"></span></a>
                        </div>';
			
	endforeach; // end of the loop.
	wp_reset_query();
	return $ret_val;
	endif;
//endif;
}
add_shortcode( 'jumptovt', 'wt_jumpto_vt' );
function wt_jumpto_vt_newdesign( $atts ){
	$vt_post = get_post();
	//if (end($vt_post->ancestors) == 506) :
	$sx_post_meta = json_decode(get_post_meta($vt_post->ID,'services_extra_post_meta', true));
	$vt_ids_arr = $sx_post_meta->services_video_testimonials;
	if (count($vt_ids_arr)) :
	$args = array(
					'post__in' => $vt_ids_arr,
				
					'post_type'=> 'video-testimonials',
				
					'order' => 'DESC',
				
					'posts_per_page' => -1
				
				);
	
	$vt_post = query_posts( $args );
	
	$ret_val .= '<div id="video-testimonial-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">';
	for($j=0; $j<count($vt_ids_arr);$j++){
	   if($j == 0){
		   $olclass = 'class="active"';
	   }else{
		   $olclass = '';
	   }
       $ret_val .='         <li data-target="#video-testimonial-generic" data-slide-to="'.$j.'" '.$olclass.'></li>';
	}
     $ret_val .='</ol>
        	<div class="container">
        	<!-- Wrapper for slides -->
            <div class="row">
                <div class="carousel-inner testimonial" role="listbox">';
	$i = 0;
	foreach ( $vt_post as $post ) :
			$content_post = get_post($post->ID);
			$the_content = $content_post->post_content;
			$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
			$v_code = get_ytube_video_code($v_test_post_meta->vt_url);
			$url = "http://youtube.com/vi/".$v_code."/0.jpg";
			if($i == '0'){
				$class = 'active';
			}else{
				$class = '';
			}
			$ret_val .= '<div class="item '.$class.'">
                        <div class="col-md-3">
                            <img src="http://img.youtube.com/vi/'.$v_code.'/0.jpg" alt="video" class="img-responsive">
							<a href="javascript:void();" data-target="#vt_modal"  data-toggle="modal" rel="http://www.youtube.com/embed/'.$v_code.'" class="vt_play"><span class="icomoon-icon-play-2"></span></a>
                        </div>
                        <div class="col-md-9">
                            <div class="carousel-caption">
                                <div class="quote">'.$the_content.'</div>
                            </div>
                        </div>
                    </div>';
	$i++;
    endforeach; // end of the loop.
	wp_reset_query();
    $ret_val .= '</div>
              </div>
        	</div>
            <!-- Controls -->
            <a class="left carousel-control" href="#video-testimonial-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#video-testimonial-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>';
	return $ret_val;
	endif;
//endif;
}
add_shortcode( 'jumptoservicesvt', 'wt_jumpto_vt_newdesign' );
// Jump to Faqs
function wt_jumpto_faqs( $atts ){
	
	global $post;
	$faq_post = get_post();
	//var_dump($faq_post);
	//if (end($folio_post->ancestors) == 506) :
	$faq_post_meta = unserialize(base64_decode(get_post_meta($faq_post->ID,'services_faq_post_meta', true)));
	
	$faq_list = $faq_post_meta['services_faqs'];
	if (count($faq_list)) :
	$ret_val .= '<div class="panel-group" id="accordion-ecom">';
	$i = 0;
	foreach($faq_list as $flkey=>$flitem):
		if($flkey == 0){
			$flclass = 'in';
			$plus_minus = 'fa-arrow-down';
		}else{
			$flclass = '';
			$plus_minus = 'fa-arrow-right';
		}
		$ret_val .= '<div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-ecom" href="#collapse'.$flkey.'">
                             <i class="fa '.$plus_minus.'"></i>
                              <span class="ttl">'.stripslashes($flitem['title']).'</span>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse'.$flkey.'" class="panel-collapse collapse '.$flclass.'">
                          <div class="panel-body">'.stripslashes($flitem['description']).'</div>
                        </div>
					</div>'	;
	$i++;
	endforeach;
	$ret_val .= '</div>';
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptoservicesfaqs', 'wt_jumpto_faqs' );
// JUMP-TO case studies short code
function wt_jumpto_cs( $atts ){
	$cs_post = get_post();
	//if (end($cs_post->ancestors) == 506) :
	$cs_post_meta = json_decode(get_post_meta($cs_post->ID,'services_case_post_meta', true));
	$cs_ids_arr = $cs_post_meta->services_case;
	if (count($cs_ids_arr)) :
	$ret_val = '
				<div id="casestudies" style="padding:0px;">
				  <div class="row">
					<h2 class="title"><span>Case Studies</span></h2>
					<p>'. $cs_post_meta->case_head_text .'</p>
					<div class="clearfix h15"></div>';
	$args = array(
		'post__in' => $cs_ids_arr,
		'post_type'=> 'case-studies',
		'order' => 'DESC',
		'posts_per_page' => -1
	);
	query_posts( $args );
	$cs_post = query_posts( $args );
	foreach ( $cs_post as $post ) :
		$case_studies_post_meta = json_decode(get_post_meta($post->ID,'case_studies_post_meta', true));
		$case_study_image = wp_get_attachment_image_src($case_studies_post_meta->case_studies_attached_image);
		$the_link = get_permalink($post->ID);
		$the_title = $post->post_title;
		$the_excerpt = substr(strip_tags(($post->post_content)), 0, 250);
		$ret_val.= '<div class="row">
				  <div class="five columns" style="margin: 0 10px 0 0;"> <a href="'. $the_link .'"><span class="pic"><img src="'. $case_study_image[0] .'">
					<div class="img_overlay"></div>
					</span></a>
					<div class="clearfix h15"></div>
					<div class="three columns"><div class="buttonwrapper"><a href="'. $the_link .'" class="org_smal"><span>READ MORE</span></a></div></div>
				  </div>
				  <div class="seven columns services_description">
					<h2>'. $the_title .'</h2>
					<div>'. $the_excerpt .'</div>
					<div class="h20"></div>
				  </div>
				  <div class="clearfix"></div>
				  <div class="h20 post_item"></div>
				  <!--<div class="pagination clearfix">
					<div class="links"> <b>1</b> <a  href="javascript:void(0)">2</a> <a  href="javascript:void(0)">›</a> <a  href="javascript:void(0)">»</a> </div>
				  </div>-->
				</div>
				<!--row-->';
	endforeach; // end of the loop.
	wp_reset_query();
	$ret_val.= '</div>
			  </div>
			  <!--row-->
			</div>
			<!--casestudies-->';
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptocs', 'wt_jumpto_cs' );
// JUMP-TO case studies short code
function wt_jumpto_blog( $atts ){
	$blog_post = get_post();
	//if (end($blog_post->ancestors) == 506) :
	$blog_post_meta = json_decode(get_post_meta($blog_post->ID,'services_blog_post_meta', true));
	$blog_ids_arr = $blog_post_meta->services_blog;
	if (count($blog_ids_arr)) :
	$ret_val = '<div id="blog">
				  <div class="row">
					<h2 class="title"><span>Blog</span></h2>
					<p>'. $blog_post_meta->blog_head_text .'</p>
					<!--<div class="h20"></div>-->';
	$args = array(
		'post__in' => $blog_ids_arr,
		'post_type'=> 'post',
		'order' => 'DESC',
		'posts_per_page' => -1
	);
	query_posts( $args );
	$cs_post = query_posts( $args );
	foreach ( $cs_post as $post ) :
		setup_postdata($post);
		$the_link = get_permalink($post->ID);
		$the_title = $post->post_title;
		$the_post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
		$the_blog_day = get_the_time('d', $post->ID);
		$the_blog_month = get_the_time('M', $post->ID);
	ob_start();				
	the_excerpt();
	$the_blog_excerpt = ob_get_clean();
		$the_date = get_the_time('j M., Y', $post->ID);
		$ret_val.= '<div class="recentnews"> 
	<div class="one columns dated">
	<span>'.$the_blog_day.'</span>
	<div class="month">'.$the_blog_month.'</div>
	</div>
				 <div class="eleven columns">
	<h2 class="h2_heading"><span><a href="'. $the_link .'">'. $the_title .'</a></span></h2>
	<div>'.$the_blog_excerpt.'</div>
	</div>
	<div class="clear"></div>
				</div>';
	endforeach; // end of the loop.
	wp_reset_postdata();
	wp_reset_query();
	$ret_val.= '</div>
			  <!--row-->
			</div>
			<!--blog-->';
	return $ret_val;
	endif;
	//endif;
}
add_shortcode( 'jumptoblog', 'wt_jumpto_blog' );
function getmoreportfolios($atts){
	$ret_val = '';
	$get_post = get_post();
	//print_r($get_post);
	 $term_list = wp_get_post_terms($get_post->ID, 'filter_tags', array("fields" => "names"));
	// $ret_val .= '<div class="owl-carousel">';
	 $args = array(
		'post_type'=> 'portfolio',
		'post_status' => 'publish',
		'order' => 'DESC',
		'posts_per_page' => '7'
	);
	query_posts( $args );
	$cs_post = query_posts( $args );
	$ret_val .= '<div id="myCarousel2" class="carousel slide"><div class="carousel-inner">';
	
	$i =1;
	foreach ( $cs_post as $post ) :
		if($i == 1){
			$cls = 'active';
		}else{ $cls = ''; }
		if($i == 1){
			$ret_val .= '<div class="item '.$cls.'"><div class="row">';
		}
		$post_id = $post->ID;
		$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),array(600,600), false, '');
		$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
		$the_link = get_post_permalink( $post_id);
		$the_title = get_the_title();
		$the_content =  $post->post_content;
		
		if($portfolio_feat_image[0] != ''){
			$ret_val .= '<div class="col-sm-4 col-xs-4"><div class="featured-box"><div class="overlay"><p class="overlaycase-txt"><a href="'.$the_link.'">'.wp_trim_words(strip_tags($the_content),10).'</a></p><div class="hover-overlay clearfix"><a href="'.$the_link.'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div><!-- ./hover-overlay --></div><a href="'.$portfolio_feat_image[0].'"><img src="'.$portfolio_feat_image[0].'" class="img-responsive" alt="" /></a><a class="full-box-link" href="'.$the_link.'"></a></div></div>';
			if($i == 6){
				break;
			}
			if($i %3 == 0){
				$ret_val .= '</div></div><div class="item"><div class="row">';
			}
			$i++;
		}
	endforeach;
	wp_reset_postdata();
	wp_reset_query();
	$ret_val .= '</div></div></div><a class="left carousel-control" href="#myCarousel2" data-slide="prev">‹</a><a class="right carousel-control" href="#myCarousel2" data-slide="next">›</a></div>';
	return $ret_val;
	
}
add_shortcode( 'PORTFOLIOMORE', 'getmoreportfolios');
// ---------------------------------- pagination fix
function curPageURL() {
	$pageURL = 'http';
	//check what if its secure or not
	if ($_SERVER["HTTPS"] == "on") {
	$pageURL .= "s";
	}
	//add the protocol
	$pageURL .= "://";
	//check what port we are on
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	//cut off everything on the URL except the last 3 characters
	$urlEnd = substr($pageURL, -3);
	//strip off the two forward shashes
	$page = str_replace("/", "", $urlEnd);
	//return just the number
	return (int) $page;
}
function wt_jumpto_google_plus_widget( $atts ){
	
}
add_shortcode( 'jumptogpw', 'wt_jumpto_google_plus_widget' );

add_action('init', 'remove_feed_links');
function remove_feed_links() {
	remove_action('wp_head', 'feed_links', 2);
	
	remove_action('wp_head', 'feed_links_extra', 3);
}
// Register custom navigation walker
require_once('wp_bootstrap_navwalker.php');
function implement_ajax() 
{
	global $post;
	$id = $_POST['id'];
	
	$args = array( 'post_type'=> 'team',
				'order' => 'ASC',
				'posts_per_page' => -1 
			);
	query_posts($args);
	if(have_posts()):
		while(have_posts()) : the_post();
			$post_id =  $post->ID;
			if($id == $post_id){
				$arr['title'] = get_the_title($id);
				$arr['desc']  = get_the_content($id);
				$team_post_meta = json_decode(get_post_meta($id,'team_post_meta', true));
				$arr['embed_video_url'] = $team_post_meta->embed_video_url;
				$arr['member_role'] = $team_post_meta->member_role;
				$arr['facebook_url'] = $team_post_meta->facebook_url;
				$arr['twitter_url'] = $team_post_meta->twitter_url;
				$arr['gplus_url'] = $team_post_meta->gplus_url;
				$arr['linkedin_url'] = $team_post_meta->linkedin_url;
				$arr['y_img'] = get_ytube_video_code($team_post_meta->embed_video_url);
			}
		endwhile;
	endif;
	echo json_encode($arr);
	die();
}
add_action('wp_ajax_my_special_ajax_call', 'implement_ajax');
add_action('wp_ajax_nopriv_my_special_ajax_call', 'implement_ajax');//for users that are not logged in.
function filter_portfolio() 
{
	//global $wpdb;
	$rel = $_POST['rel'];
	//$tag_array = explode('|',$rel);
	/*if(isset($tag_array[0]) && !is_numeric($tag_array[0])){
		$tag = get_term_by('slug',$tag_array[0], 'filter_tags');
	}*/
	$portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
	//$portfolio_images_num_display = $portfolio_images_settings->portfolio_images_num_display;
	
	$args = array( 'post_type'=> 'portfolio',
					'post_status' => 'publish',
					'posts_per_page' => '-1',
					'tax_query' => array(
											array(
												'taxonomy' => 'filter_tags',
												'field' => 'slug',
												'terms' => array($rel)
											)
										)
				);
	query_posts($args);
	global $post;
	$i = 0;
	if(have_posts()):
		while(have_posts()) : 
			the_post();
			$post_id = $post->ID;
			//var_dump($post_id);
			$the_content =  get_the_content('Read more');
			//$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
			$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),array(313,300), false, '');
			$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
			//Returns Array of Term Names for "my_term"
			$term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "slugs"));
			//var_dump($term_list);
			$postclasses = '';
			$posttags = '';
			foreach($term_list as $item):
				$posttags .= $item.", ";
				$postclasses .= preg_replace('/[^a-zA-Z0-9]/s', '', $item)."-";
				//$postclasses .= str_replace("&","@amp;",$item)."-";
			endforeach;
			//echo $postclasses;
			if(in_array($rel,$term_list)):
				/*if(isset($tag_array[1])):
					if(in_array($tag_array[1],$term_list)):
						$arr['portfolio'][$i]['title'] = get_the_title($post_id);
						$arr['portfolio'][$i]['portfolio_image'] = $portfolio_feat_image[0];
						$arr['portfolio'][$i]['link'] = get_post_permalink($post_id);
						$arr['portfolio'][$i]['content'] = wp_trim_words(strip_tags($the_content),10);
						$arr['portfolio'][$i]['term'] = $tag_array[1];
					endif;
				else:*/
				$arr['portfolio'][$i]['title'] = get_the_title($post_id);
				$arr['portfolio'][$i]['portfolio_image'] = $portfolio_feat_image[0];
				$arr['portfolio'][$i]['link'] = get_post_permalink($post_id);
				$arr['portfolio'][$i]['content'] = wp_trim_words(strip_tags($the_content),10); 
				$i++;
				/*endif;*/
			endif;
		endwhile;
	endif;
	echo json_encode($arr);
	die();
}
add_action('wp_ajax_portfolio_ajax_call', 'filter_portfolio');
add_action('wp_ajax_nopriv_portfolio_ajax_call', 'filter_portfolio');//for users that are not logged in.
register_sidebar( array(
						'name' => __( 'Meet Our Team', 'twentyeleven' ),
						'id' => 'meet_our_team',
						'description' => __( 'An optional widget area for your site footer', 'twentyeleven' ),
						'before_widget' => '<aside id="%1$s" class="widget %2$s">',
						'after_widget' => "</aside>",
						'before_title' => '<h3 class="widget-title">',
						'after_title' => '</h3>',
						)
				);
function remove_empty_tags_around_shortcodes($content) {
    $tags = array(
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br>' => ']',
        ']<br />' => ']'
    );
    $content = strtr($content, $tags);
    return $content;
}
function wp_infinitepaginate(){
    $loopFile        = $_POST['loop_file'];
    $paged           = $_POST['page_no'];
    $posts_per_page  = get_option('posts_per_page');
 	//echo $loopFile;
	//echo $paged;
	//echo $posts_per_page;
    # Load the posts
	$args = array( 'post_type'=> 'post',
					'paged' => $paged,
					'posts_per_page' => 2 
				);
    query_posts($args);
	if(have_posts()):
		while(have_posts()):
			the_post();
			get_template_part('content',  get_post_format() );
		endwhile;
	else:
		echo '<div class="clearfix"></div><div class="well">No More Posts</div>';
	endif;
 
    exit;
}
add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');    // if user not logged in<br />
function new_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
function googleplaceapicurl()
{
		//Add args to
		$google_places_url = add_query_arg(
			array(
				'placeid' => 'ChIJ6UTc0yzlwokRveKTozxpwEk',
				'key'       => 'AIzaSyDmfJPL6z9-ZJQHAqIzR-hmU2aepw5Z--I'
			),
			'https://maps.googleapis.com/maps/api/place/details/json'
		);
		// Send API Call using WP's HTTP API
		$data = wp_remote_get( $google_places_url );
		if ( is_wp_error( $data ) ) {
			$error_message = $data->get_error_message();
			$this->output_error_message( "Something went wrong: $error_message", 'error' );
		}
		//Use curl only if necessary
		if ( empty( $data['body'] ) ) {
			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HEADER, 0 );
			$data = curl_exec( $ch ); // Google response
			curl_close( $ch );
			$response = json_decode( $data, true );
		} else {
			$response = json_decode( $data['body'], true );
		}
		//GPR Reviews Array
		$gpr_reviews = array();
		//includes Avatar image from user
		//@see: https://gist.github.com/jcsrb/1081548
		if ( isset( $response['result']['reviews'] ) && ! empty( $response['result']['reviews'] ) ) {
			//Loop Google Places reviews
			foreach ( $response['result']['reviews'] as $review ) {
				$user_id = isset( $review['author_url'] ) ? str_replace( 'https://plus.google.com/', '', $review['author_url'] ) : '';
				//Add args to
				$request_url = add_query_arg(
					array(
						'alt' => 'json',
					),
					'http://picasaweb.google.com/data/entry/api/user/' . $user_id
				);
				$avatar_get      = wp_remote_get( $request_url );
				$avatar_get_body = json_decode( wp_remote_retrieve_body( $avatar_get ), true );
				$avatar_img      = $avatar_get_body['entry']['gphoto$thumbnail']['$t'];
				//add array image to review array
				$review = array_merge( $review, array( 'avatar' => $avatar_img ) );
				//add full review to $gpr_views
				array_push( $gpr_reviews, $review );
			}
			//merge custom reviews array with response
			$response = array_merge( $response, array( 'gpr_reviews' => $gpr_reviews ) );
		}
		//Google response data in JSON format
		return $response;
}
function output_error_message( $message, $style ) {
		switch ( $style ) {
			case 'error' :
				$style = 'gpr-error';
				break;
			case 'warning' :
				$style = 'gpr-warning';
				break;
			default :
				$style = 'gpr-warning';
		}
		$output = '<div class="gpr-alert ' . $style . '">';
		$output .= $message;
		$output .= '</div>';
		echo $output;
}
// JUMP-TO portfolio short code
function wt_google_activty_feed( $atts ){
	echo '<div class="col-sm-12">';
	if ( ! dynamic_sidebar( 'googleplus' ) ) :
	endif;
	echo '</div>';
}
add_shortcode( 'googleactivity', 'wt_google_activty_feed' );