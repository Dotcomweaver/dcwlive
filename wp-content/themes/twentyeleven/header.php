<?php
/**
* The Header for our theme.
*
* Displays all of the <head> section and everything up till <div id="main">
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<!-- Mobile Specific Metas
================================================== -->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<!--<title><?php wp_title( '|', true, 'right' ); ?></title>-->
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<title>
<?php
/*
* Print the <title> tag based on what is being viewed.
*/
global $page, $paged;
wp_title( '|', true, 'right' );
// Add the blog name.
//bloginfo( 'name' );
// Add the blog description for the home/front page.
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
echo " | $site_description";
// Add a page number if necessary:
if ( $paged >= 2 || $page >= 2 )
echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
?>
</title>
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!-- all js and css -->
<link  async rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
<link async href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet" type="text/css" />
<?php
/* We add some JavaScript to pages with the comment form
* to support sites with threaded comments (when in use).
*/
if ( get_option( 'thread_comments' ) )
/* wp_enqueue_script( 'comment-reply' ); */
/* Always have wp_head() just before the closing </head>
* tag of your theme, or you will break many plugins, which
* generally use this hook to add elements to <head> such
* as styles, scripts, and meta tags.
*/
wp_head();
$url =$_SERVER['REQUEST_URI'];
$urlarr = explode('/',$url);
if ($urlarr[1] == 'case-studies') { ?>
    <link async href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css" />
    <link async href="<?php echo get_template_directory_uri(); ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link async href="<?php echo get_template_directory_uri(); ?>/css/charts.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<?php } 
?>
</head>
<?php
$dataspy = '';
if(is_page('contact-us')){ 
	$dataspy = 'data-spy="scroll" data-target="#navbar-example"';
}
?>
<body <?php body_class(); ?> <?php //echo $dataspy; ?>>
<div itemscope itemtype="http://schema.org/LocalBusiness">
<?php 
$page = $_SERVER['REQUEST_URI'];
$explore = explode('/',$page);
if($explore[1]!='landing-page')
{
	if(is_page('home')){
		$class = 'top-bg-modified';
	}else{
		$class = 'top-bg';
	}
?>
<!-- header wrapper starts here-->
<header class="top-bg affix-top" id="nav">
    <div class="main">
    	<div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-4" id="logo">
            	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="logo">
            		<img src="<?php echo get_template_directory_uri(); ?>/images/dcw-header-logo.png" alt="logo" class="img-responsive"/>
                </a>
            </div>
            <div class="col-md-7 col-sm-8">
                <div class="row">
                	<?php if ( ! dynamic_sidebar( 'contact_info' ) ) : ?>
                                <!--<div class="col-md-12 top-link">
                                    <span class="blue-txt">CALL 888-315-6518</span> <a href="#">REQUEST QUOTE »</a>
                                </div>-->
                    <?php endif; // custom widget ?>
                    <div class="col-md-12 navscroll">
                        <div class="navbar main-nav navbar-default" role="navigation">
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                              <a href="#" class="navbar-brand">Menu</a>
                            </div>
                            <div class="navbar-collapse collapse">
                            <?php
							wp_nav_menu( array(
                                              'menu' => 'topmenu',
                                              'depth' => 2,
                                              'container' => false,
                                              'menu_class' => 'nav navbar-nav navbar-right',
                                              //Process nav menu using our custom nav walker
                                              'walker' => new wp_bootstrap_navwalker())
                                            );
                             
							?>
                            
                            </div><!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    
</header>
<!--sticky_header-->
<?php }?>
<!-- header wrapper ends here-->
