<?php
/**
* Template Name: HomePageOld
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>
<!--Testimonails carousel Scripts Ends here-->
<div class="row clearfix">
  <div class="row bgclr">
    <div class="container main_section">
      <div class="row">
        <div class="eight columns slidecontent">
          <?php if ( ! dynamic_sidebar( 'slidecontent' ) ) : ?>
          <?php endif; // custom widget ?>
        </div>
        <!--eight slider slidecontent-->
        
        <div class="nine columns slideright">
          <div class="flexslider">
            <?php
			$args = array(
				'post_type'=> 'portfolio',
				'order' => 'DESC',
				'posts_per_page' => -1
			);
			query_posts( $args );
			?>
            <ul class="slides">
              <?php while ( have_posts() ) : the_post();
$folio_post_meta = json_decode(get_post_meta($post->ID,'portfolio_post_meta', true));
if($folio_post_meta->folio_is_home && $folio_post_meta->folio_is_home == 'Y')
{
$portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'large');
$portfolio_post_meta->portfolio_url;
?>
              <li><a href="<?php the_permalink();?>"><img src="<?php echo $portfolio_feat_image[0] ?>" alt="" style=""/></a></li>
              <?php
}
endwhile; ?>
            </ul>
          </div>
          
          <!--flexslider--> 
          
        </div>
        
        <!--thirteen columns fr--> 
        
      </div>
    </div>
    <!--main_section--> 
    
  </div>
  <!--row bgclr-->
  
  <div class="row testimonails">
    <div class="container testmonials_bg">
      <div class="five columns">
        <h3>TESTIMONIALS</h3>
        <p>We treat our clients like partners with the understanding that the success of your project is our success.</p>
        <div class="btnwrp five columns"> <a class="org_smal" href="http://www.dotcomweavers.com/video-testimonials"><span>VIEW ALL</span></a> </div>
      </div>
      <!--five columns testimonails content wrap-->
      <div class="eleven columns testmoright"> 
        <!--testimonails carasoule  starts  here-->
        <ul id="flexiselDemo2">
          <?php
$args = array(
'post_type'=> 'video-testimonials',
'order' => 'DESC',
'posts_per_page' => -1
);
query_posts( $args );
?>
          <?php while ( have_posts() ) : the_post();
$v_test_post_meta = json_decode(get_post_meta($post->ID,'v_testimonials_post_meta', true));
	
	
if($v_test_post_meta->vt_is_home && $v_test_post_meta->vt_is_home == 'Y')
{
$y_img = get_ytube_video_code($v_test_post_meta->vt_url);
$te = $v_test_post_meta->vt_url;
	
	$start = strpos($te,"v=")+2;
	
	$end = strpos($te,"&");
	
	if( $end == "" || $end == 0 ){
		$fstr = substr($te,$start);
	} else {
		$length = $end-$start;
		$fstr = substr($te,$start,$length);
	}
?>
          <li><a href="javascript:void(0)" rel="http://www.youtube.com/embed/<?php echo $fstr; ?>" class="show_video"> <img src="/wp-content/themes/twentyeleven/images/play120x90.png" style="background:url('http://img.youtube.com/vi/<?php echo $y_img; ?>/1.jpg') no-repeat scroll center center;" alt="<?php the_title() ?> - Testimonial for NJ Web Design company" /> </a></li>
          <?php
}
endwhile; ?>
        </ul>
        
        <!--testimonails carasoule  ends  here--> 
        
      </div>
    </div>
    <!--testmonials_bg--> 
    
  </div>
  <!--testimonails-->
  
  <div class="clearfix"></div>
  <div class="row producticons">
    <div class="container icons_wrapper">
      <div class="imagerry">
        <ul>
          <li> <a title="Dotcomweavers | webaward" href="http://www.webaward.org/winner.asp?eid=20742" target="_blank"> 
          <img  alt="" src="<?php echo get_template_directory_uri(); ?>/icons/wma_logo.png" style="max-height:50px; margin-top:-8px;"> </a> </li>
          <li><a  href="http://dotcomweavers.topseoscompanies.com" target="_blank"> <img alt="" src="<?php echo get_template_directory_uri(); ?>/icons/bestseo.jpg" style="max-height:50px; margin-top:-10px;"></a></li>
          <li> <a title="Dotcomweavers | bestwebdesignagencies" href="http://dotcomweavers-inc.bestwebdesignagencies.com" target="_blank"> <img  alt="" src="<?php echo get_template_directory_uri(); ?>/icons/bestwebdesign.jpg"> </a> </li>
          <li> <a title="Click for the Business Review of Dotcomweavers, a Web Design in Paramus NJ" href="http://www.bbb.org/new-jersey/business-reviews/web-design/dotcomweavers-in-paramus-nj-90086990/#sealclick"  target="_blank"> <img  alt="" src="<?php echo get_template_directory_uri(); ?>/icons/bcc-logo-aplus.png"></a></li>
         <?php /*?> <li><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/icons/aplus.jpg"></li><?php */?>
          <li> <a  href="http://www.njtech.me" target="_blank"> <img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/icons/njtech.jpg"> </a> </li>
          <li><a href="http://experts.shopify.com/dotcomweavers?rank=1"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/icons/shopify.jpg"></a></li>
          <li><a title="Miva Merchant Certified Developer" href="http://www.mivamerchant.com/partners/developers" target="_blank"><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/icons/miva-logo.jpg"></a></li>
          <li><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/icons/oscomrce.jpg" style="max-height: 30px;  margin-top: 5px;"></li>
          <li><img title="" alt="" src="<?php echo get_template_directory_uri(); ?>/icons/upsznet.gif" style="max-height: 30px; margin-top: 5px;"></li>
        </ul>
      </div>
      <!--imagerry--> 
      
    </div>
    <!--icons_wrapper--> 
    
  </div>
  <!--product_icons-->
  
  <div class="row boxwrapper">
    <div class="container icons_wrapper">
      <?php if ( ! dynamic_sidebar( 'boxwrapper' ) ) : ?>
      <?php endif; // custom widget ?>
    </div>
    <!--icons_wrapper-->
    <div class="clearfix"></div>
  </div>
  <!--row boxwrapper-->
  
  <div class="row newswrapper">
    <div class="container">
      <div class="clearfix"></div>
      <h2>Recent News</h2>
    </div>
    <div class="container clearfix" id="gotoBlog"> 
      
      <!--end_row-->
      
      <div class="columns mp0">
        <?php query_posts('showposts=3'); ?>
        <?php while ( have_posts() ) : the_post(); ?>
        <div class="recentnews">
          <div class="one columns dated"> <span>
            <?php the_time('d') ?>
            </span>
            <div class="month">
              <?php the_time('M') ?>
            </div>
          </div>
          <div class="fifteen columns wid95">
            <h2 class="h2_heading"><span>
              <?php the_title(); ?>
              </span></h2>
            <div style="padding-bottom:18px;">
              <?php the_excerpt(); ?>
            </div>
          </div>
          <!--fifteen columns wid95-->
          
          <div class="clear"></div>
        </div>
        <?php endwhile; // end of the loop. ?>
        <?php wp_reset_query(); ?>
      </div>
      
      <!--container_1280x960--> 
      
      <!-- END OF BLOG --> 
      
    </div>
    <!--newspost-->
    
    <div class="botm_imglarge"></div>
    <!--botm_imglarge--> 
    
  </div>
  <!--recentnews--> 
  
  <!--container clearfix--> 
  
  <!--body_section ends--> 
  
</div>
<!--row clearfix--> 
	
<?php get_footer(); ?>