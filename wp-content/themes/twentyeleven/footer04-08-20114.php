<?php
/*** The template for displaying the footer.** Contains the closing of the id=main div and all content after** @package WordPress* @subpackage Twenty_Eleven* @since Twenty Eleven 1.0*/
?>
<!--</div> #main -->
<!-- Main Footer::END --><!-- LIVE CHAT CODE STARTS -->
<!-- LIVE CHAT CODE ENDS -->
<!-- Main Footer start -->
<?php 
if(!is_page('Contact Us')): ?>
<section class="light-gray-wraper">
	<div class="container">
		<div class="row divide">
            <div class="col-md-6 recent-news">
                <h2 class="h2-lines"><span>Recent News</span></h2>
                <a href="<?php echo site_url('blog'); ?>" class="view">VIEW ALL</a>
                <div class="row">
                     <?php query_posts('showposts=2'); ?>
                    <?php while ( have_posts() ) : the_post();  ?>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="date"><?php the_time('M'); ?><span><?php the_time('d'); ?></span></div>
                            </div>
                            <div class="col-xs-10">
                                <p><strong><?php the_title(); ?></strong></p>
                                <p><?php $excerpt = get_the_excerpt( $deprecated); echo substr($excerpt,0,100).'...'; ?></p>
                                <a href="<?php echo get_permalink( $post->postID ); ?>"><strong>Read More <i class="fa fa-angle-double-right"></i></strong></a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; // end of the loop. ?>
                     <?php wp_reset_query(); ?>
                </div>
            </div>
            <div class="col-md-6 recent-news">
                <h2 class="h2-lines"><span>Latest Tweet</span></h2>
                <a href="https://twitter.com/Dotcomweavers" class="view" target="_blank">VIEW ALL</a>
                <div class="row">
                    <?php if ( ! dynamic_sidebar( 'latest_tweets' ) ) : ?>
                    <?php endif; // end sidebar widget area ?>
                   </div>
            </div>
        </div>
	</div>
</section>
<?php endif; ?>
<footer id="footer">
  <div class="container">
     <div class="row">
      <?php
		/*$page = $_SERVER['REQUEST_URI'];
		$page_arr = explode('/',$page);
		if( ($page_arr[1]=='blog' && !is_single()) || ($page_arr[1]=='category' && !is_single()) || ($page_arr[1]=='author' && !is_single()) || ($page_arr[1]=='tag' && !is_single()) || ($page_arr[2]=='' && is_single()) || stristr($page_arr[1],"?s=") )
		{
		
              <div class="col-sm-9">
							<h3>About DotcomWeavers</h3>
							<p>DotcomWeavers is a full-service web design and development agency with over a decade of successful projects. Our customers come to us because we listen to them, do the research, and create the solutions they need at rates they can afford. </p>
						  </div>
     
		}
		else
		{*/
		?>
         <aside class="col-sm-6 col-md-3">
            <h5 class="h2-lines"><span>Services</span></h5>
            <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => '','menu' => 'Footer_Services','menu_class'      => '', ) ); ?>
        </aside>
        <aside class="col-sm-6 col-md-3">
            <h5 class="h2-lines"><span>Work</span></h5>
            <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => '','menu' => 'general','menu_class'      => '', ) ); ?>
        </aside>
        <div class="visible-sm clearfix"></div>
        <aside class="col-sm-6 col-md-3">
            <h5 class="h2-lines"><span>Resources</span></h5>
            <?php wp_nav_menu( array( 'theme_location' => 'primary','container'       => 'ul',    'container_class' => '','menu' => 'resources','menu_class'      => '', ) ); ?>
        </aside>
      <?php
/*}*/
?>
      <!--address block widget-->
          <aside class="col-sm-6 col-md-3">
            <h5 class="h2-lines"><span>Contact Us</span></h5>
            <ul class="contact" itemscope itemtype="http://schema.org/Organization"> 
				<meta itemprop="name" content="Dotcomweavers" >
                <li itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><i class="fa fa-home"></i><span itemprop="streetAddress">140 E Ridgewood Ave</span>, <br />
				Suite 415 South <br />
				<span itemprop="addressLocality">Paramus</span>, <span itemprop="addressRegion">NJ</span> <span itemprop="postalCode">07652</span></li>
                <li><i class="fa fa-phone"></i><span itemprop="telephone">(888) 315-6518</span><br />
				(201) 940-7377</li>
				<meta itemprop="url" content="www.dotcomweavers.com" >
                <li><i class="fa fa-envelope"></i>info@dotcomweavers.com</li>
            </ul>
        </aside>
      <!--four columns-->
      
      <?php
		$url = get_template_directory_uri();
		$url = str_replace('themes/twentyeleven','',$url);
		?>
      
      <!--newsletter starts here-->
      <?php /*?><?php
if(is_page('home'))
{
?><?php */?>
     
     
     <aside class="col-sm-12 mtop20 text-center">
        <small><a href="<?php echo site_url('privacy-policy')?>" target="ext" rel="nofollow">Privacy Policy</a> | &copy; 2014 DotcomWeavers. All rights reserved</small>
    </aside>
     
      <!--row--> 
      <!--newsletter ends here--> 
    </div>
  </div>
  <!-- FADING Footer starts -->
<!--  <div class="row" <?php if ( is_front_page())  echo 'id="fade"'; else echo 'id="fade"'; ?>>
-->    <section class="footer_btm">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="social-links row"> 
                            <div class="col-sm-3 col-xs-6"><a href="http://www.facebook.com/dotcomweavers" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a> Like </div>
                            <div class="col-sm-3 col-xs-6"><a href="http://twitter.com/Dotcomweavers" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a> FOLLOW </div>
                            <div class="col-sm-3 col-xs-6"><a href="https://plus.google.com/+Dotcomweavers" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a> ADD </div>
                            <div class="col-sm-3 col-xs-6"><a href="http://www.linkedin.com/company/dotcomweavers-inc" title="Linked In" target="_blank"><i class="fa fa-linkedin"></i></a> CONNECT</div>
                      </div>
                    </div><!--col-sm-4-->
                    <div class="col-md-5">
                    	<div class="row">
                        	<div class="col-xs-6 text-right">
                                <a href="<?php echo site_url('contact-us'); ?>" class="btn btn-warning">REQUEST A consultation</a> 
                            </div><!--col-sm-4 -->
                        	<div class="col-xs-6 text-right"> 
                                <a href="#" class="btn btn-warning">Call 888-315-6518</a>
                            </div><!--col-sm-4 -->
                        </div>
                    </div>
                </div>
              </div><!--container-->
      </section>
    <!--footer_btm_inner--> 
    
   
  <!-- FADING Footer ends --> 
</footer>
<!-- Google API -->
<link async rel="stylesheet" type="text/css" media="all"  href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
<link async href="<?php echo get_template_directory_uri(); ?>/css/icons.css" rel="stylesheet" type="text/css" />

<link async href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link async href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
<link async href='http://fonts.googleapis.com/css?family=Unica+One' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>



<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.1.11.0.js"></script>
<script type="text/javascript" async src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<script type="text/javascript" async src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';</script>
<?php 
if(is_single() || is_home() || is_tag() || is_page('Knowledge Base') || is_page('Press Releases') ): ?>
<script async src="<?php echo get_template_directory_uri(); ?>/js/isotope.pkgd.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
	$container = $("#posts");
	  /* activate jquery isotope */
	  $container.isotope({
        itemSelector: '.item',
    });
	
});
</script>
<script async type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a81c67660fddc6"></script>
<?php endif; ?>
<?php
if(is_page('contact-us')){ ?>
<script type="text/javascript" async src="<?php echo get_template_directory_uri(); ?>/js/validatecontactus.js"></script>
<script type="text/javascript" async src="<?php echo get_template_directory_uri(); ?>/js/bootstrapValidator.js"></script>
<?php 
}
if(!is_page('home') && !is_home() && !is_single() && !is_tag() && !is_page('Knowledge Base') && !is_page('Press Releases') ){
?>
<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($){
	  $('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			  $taroffset = target.offset().top;
			  $header = $("header").height();
			  $inner = $(".inner-header").height();
			$('html,body').animate({
			  scrollTop : ($taroffset-($inner-90))
			}, 1000);
			return false;
		  }
		}
	  });
});
</script>
<?php		
}
?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- AddThis Button END -->
<a title="Web Statistics" href="http://clicky.com/100706936"><img alt="Web Statistics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script async src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100706936); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100706936ns.gif" /></p></noscript>
</body>
</html>