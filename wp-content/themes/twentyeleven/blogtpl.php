<?php
/**
* Template Name: Blog
* The template for displaying posts pages.
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Eleven
*/
get_header(); 
?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					} ?>
               	  <h1>We are a full-service web design and development agency with over a decade of successful projects. Our customers come to us because we listen to them, do the research, and create the solutions they need at rates they can afford.</h1>
                  
              </div>
          </div>
        </div>
</section>
<section class="light-gray-wraper divider">
  <div class="container" id="container-blog">
		<?php 
		if ( have_posts() ) : ?>
        	<div class="row">
            	<div class="blog-filter">
                        <a href="<?php echo site_url('blog'); ?>" class="btn btn-warning active">VIEW ALL</a>
                        <?php
                        $posttags = get_tags();
						//print_r($posttags);
						$tmp = $posttags[4];
						$posttags[4] = $posttags[5];
						$posttags[5] = $tmp;
						
                        foreach($posttags as $tag){
                        ?>
                            <a href="<?php echo get_tag_link($tag->term_id); ?>" class="btn btn-warning"><?php echo $tag->name; ?></a>
                        <?php } ?>
                        
                    </div><!-- blog-filter -->
            </div>
			<div id="posts" class="row blog">
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); 
						$category = get_the_category( $post->ID);
						if($category != 'Home Slider'): ?>
							<?php get_template_part( 'content', get_post_format() ); ?>
			<?php 		endif;
				endwhile; ?>

			<?php //twentytwelve_content_nav( 'nav-below' ); ?>
		</div><!-- #content -->
        <?php else : ?>
			<div class="row" id="posts">
			<article id="post-0" class="post no-results not-found">

			<?php if ( current_user_can( 'edit_posts' ) ) :
				// Show a different message to a logged-in user who can add posts.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'No posts to display', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'twentytwelve' ), admin_url( 'post-new.php' ) ); ?></p>
				</div><!-- .entry-content -->

			<?php else :
				// Show the default message to everyone else.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'twentytwelve' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			<?php endif; // end current_user_can() check ?>

			</article><!-- #post-0 -->
			</div>
		<?php endif; // end have_posts() check ?>
        
        
			<?php if(function_exists('wp_paginate')) {
                                                wp_paginate();
                                                } 
            ?>
        
	</div><!-- #primary -->
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>