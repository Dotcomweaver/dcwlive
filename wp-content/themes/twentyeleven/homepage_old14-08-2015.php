<?php
/**
* Template Name: HomePageOld
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); 
?>
<!--Testimonails carousel Scripts Ends here-->
<!---- Carousel Starts at here ---->
<!--<div class="holiday-ad1"></div>-->
<!-- IRCE Banne Carousel -->
<!--<section class="main-banner">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="/wp-content/themes/twentyeleven/images/ircebanner.jpg" alt="banner1" class="img-responsive">
      <div class="carousel-caption">
      </div>
    </div>
    <div class="item">
      <img src="/wp-content/themes/twentyeleven/images/homepagebanner.jpg" alt="banner2" class="img-responsive">
      <div class="carousel-caption">
        <div class="row">
            <div class="col-sm-6">
                <p class="head">OVER 300 SUCCESSFUL PROJECTS & COUNTING...</p>
                <hr>
                <div class="clearfix"></div>
                <h1>Web Design and Development Company focusing on Custom Web Solutions & eCommerce Websites.</h1>
            </div>
            <div class="col-sm-6">
              <div class="embed-responsive mtop20 embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/108521016" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
         </div>
         <div class="row mtop20">
          <div class="col-lg-2 col-sm-3 col-xs-4 text-center"><a href="/ecommerce" class="btn btn-default">eCommerce</a></div>
            <div class="col-lg-2 col-sm-3 col-xs-4 text-center"><a href="/web-development" class="btn btn-default">Web Dev</a></div>
            <div class="col-lg-2 col-sm-3 col-xs-4 text-center"><a href="/web-applications" class="btn btn-default">Web Apps</a></div>
            <div class="col-lg-2 col-sm-3 col-xs-4 text-center"><a href="/responsive-web-design" class="btn btn-default">Responsive</a></div>
            <div class="col-lg-2 col-sm-3 col-xs-4 text-center"><a href="/mobile-application-mobile-website-development" class="btn btn-default">Mobile Apps</a></div>
            <div class="col-lg-2 col-sm-3 col-xs-4 text-center"><a href="/seo-sem" class="btn btn-default">SEO/SEM</a></div>
         </div>
      </div>
    </div>
  </div>

  
</div>
</section>-->
<!-- EOF IRCE banner Carousel -->
<section class="main-banner">
	<div class="container">
         <div class="row">
            <div class="col-sm-6">
                <p class="lead">OVER 300 SUCCESSFUL PROJECTS & COUNTING...</p>
                <hr>
                <div class="clearfix"></div>
                <h1>Web Design and Development Company focusing on Custom Web Solutions & eCommerce Websites.</h1>
            </div>
            <div class="col-sm-6">
            	<div class="embed-responsive mtop20 embed-responsive-16by9">
	                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/108521016" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
         </div>
         <div class="row mtop40">
         	<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 text-center"><a href="/ecommerce" class="btn btn-default">eCommerce</a></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 text-center"><a href="/web-development" class="btn btn-default">Web Dev</a></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 text-center"><a href="/web-applications" class="btn btn-default">Web Apps</a></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 text-center"><a href="/responsive-web-design" class="btn btn-default">Responsive</a></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 text-center"><a href="/mobile-application-mobile-website-development" class="btn btn-default">Mobile Apps</a></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 text-center"><a href="/seo-sem" class="btn btn-default">SEO/SEM</a></div>
         </div>
    </div>
</section>
<?php if ( ! dynamic_sidebar( 'boxwrapper' ) ) : ?>
<?php endif; // custom widget ?>

<section class="our-work">
    <div class="container">
       <div class="row">
        <div class="col-md-12">
            <h2>our work</h2>
            <p class="sub-desc">With over 300 projects and counting, we have empowered each of our clients to propel their business vision forward and become even more successful. Every website we undertake demonstrates our knowledge of WEB 2.0.</p>
         </div>
        <!-- Carousel
        ================================================== -->
        <div class="col-md-12">
        	<div id="myWork" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          
          <div class="carousel-inner">
          	<?php 
			   $query = "SELECT $wpdb->posts.*,$wpdb->postmeta.* 
					FROM $wpdb->posts, $wpdb->postmeta
					WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
					AND $wpdb->postmeta.meta_key = 'portfolio_post_meta' 
					AND $wpdb->posts.post_status = 'publish' 
					AND $wpdb->posts.post_type = 'portfolio'
					ORDER BY $wpdb->posts.post_date DESC";
					
				$pageposts = $wpdb->get_results($query, OBJECT);
				$query = new WP_Query( array( 'post_type' => 'portfolio','posts_per_page' => 1  ) ); 
				
				if ( $pageposts ) 
				{ 
					$i = 1;
					foreach ( $pageposts as $post )
					{ 
						
						setup_postdata($post);
						$post_id = $post->ID;
						$active_id = $post->ID;
						$portfolio_feat_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
						
						//echo $post_id;
						$portfolio_post_meta = json_decode(get_post_meta($post_id,'portfolio_post_meta', true));
						//print_r($portfolio_post_meta);
						if($portfolio_post_meta->ourwork_is_home == 'Y')
						{
			  				if($i == '1'){
								$class = 'active';
							}else{
								$class = "";
							}
			  ?>
                           <div class="item <?php echo $class; ?>">
                               <div class="row">
                                    <div class="col-md-7 col-sm-7">
                                        <div>
                                            <?php 
                                                $portfolio_image_id_arr = array_filter(explode(',',$portfolio_post_meta->portfolio_home_image));
                                                foreach( $portfolio_image_id_arr as $attach_img_id ) :
                                                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'full');
                                            ?>
                                                    <span><img src="<?php echo $attach_folio_img[0]; ?>" class="img-responsive" alt=""></span>
                                            
                                            <?php break; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                     <div class="col-md-5 col-sm-5">
                                         <h3><?php echo $portfolio_post_meta->portfolio_client_heading; ?></h3>
                                         <span class="h4 txt-shdow"><a href="<?php echo the_permalink();?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></span>
                                         <hr>
                                        <?php
                                            $key_features = $portfolio_post_meta->portfolio_key_features;
                                            //echo nl2br($portfolio_post_meta->portfolio_key_features);
                                            if( $key_features != "" ){
                                            $key_features_arr = explode(',',$key_features);
                                        ?>
                                        <h5 class="txt-shadow">Key Features</h5>
                                        <ul class="key-points">
                                            <?php
                                                $i = 1;
                                                foreach($key_features_arr as $kfk=>$kfv){
                                            ?>
                                                    <li><i><?php echo $i; ?></i> <?php echo $kfv; ?> </li>
                                            <?php 
                                                    $i++; 
                                                }
                                            ?>
                                            <!--<li><i>2</i> User Account Registrations </li>
                                            <li><i>3</i> Claim Listings for businesses</li>
                                            <li><i>4</i> Customized CMS </li>
                                            <li><i>5</i> Spreadsheet based data management </li>-->
                                        </ul>
                                        <?php 
									   }
									   $title = get_the_title();
									   
									   ?>
                                       <a href="<?php echo the_permalink();?>" class="btn btn-warning">Dig Deeper</a>
                                    </div>
                                   
                                </div>
                           </div><!-- item active -->
			  <?php 
			  			$i++;
			  			}
					
					}
				}
			   wp_reset_postdata();
			?>
          </div>
          <a class="left carousel-control" href="#myWork" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#myWork" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
        </div><!-- /.carousel -->
     <!---- Carousel Ends here ---------->
	 <!---- Recents News Start here ----------> 
     </div>
    </div>
</section>
<?php if ( ! dynamic_sidebar( 'sidebar-3' ) ) : ?>
<?php endif; // custom widget ?>
<!--row clearfix--> 
<?php get_footer(); ?>