<?php
/**
 * The template used to display Tag Archive pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
<section class="inner-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					} ?>
              <h1><?php
                    printf( __( 'Blog: %s', 'twentyeleven' ), '<span>' . single_tag_title( '', false ) . '</span>' );
                ?></h1>
               <p><?php
                    $tag_description = tag_description();
                    if ( ! empty( $tag_description ) )
                        echo apply_filters( 'tag_archive_meta', '<div class="tag-archive-meta">' . $tag_description . '</div>' );
                ?></p>
          </div>
      </div>
    </div>
</section>
<section class="light-gray-wraper divider">
    <div class="container" id="container-blog">
        <div class="row">
            <?php 
            if(have_posts()): ?>
                <div class="col-md-12">
                    <div class="blog-filter">
                        <a href="<?php echo site_url('blog'); ?>" class="btn btn-warning active">VIEW ALL</a>
                        <?php
                        $posttags = get_tags();
						$tmp = $posttags[4];
						$posttags[4] = $posttags[5];
						$posttags[5] = $tmp;
                        foreach($posttags as $tag){
                        ?>
                            <a href="<?php echo get_tag_link($tag->term_id); ?>" class="btn btn-warning"><?php echo $tag->name; ?></a>
                        <?php } ?>
                       
                    </div><!-- blog-filter -->
                    <div id="posts" class="row">
                    <?php 
                    while ( have_posts() ) : the_post();
                        $category = get_the_category( $post->ID);
                        if($category != 'Home Slider'): ?>
					 <?php get_template_part( 'content', get_post_format() ); ?>
                    <?php 	endif;	
                    endwhile; ?>
                    </div>
                </div>
            <?php else: ?>
                    <div class="col-md-12">
                        <article id="post-0" class="post no-results not-found">
                        <header class="entry-header">
                            <h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
                        </header><!-- .entry-header -->
        
                        <div class="entry-content">
                            <p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
                            <?php get_search_form(); ?>
                        </div><!-- .entry-content -->
                    </article><!-- #post-0 -->
                    </div>
          	<?php endif; ?>
        </div>
        <?php if(function_exists('wp_paginate')) {
                                                wp_paginate();
                                                } 
            ?>
    </div>
</section>
<!--.row-->        
<?php get_footer(); ?>