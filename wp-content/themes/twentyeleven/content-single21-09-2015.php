<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
	$post_id = get_the_ID();
	$tags = wp_get_post_tags($post_id);
?>
<div class="row">
    <div class="col-md-8 col-sm-12">
    <div class="blog-box">                
	<div class="blog-box-inner ind-blog-page">
        <div class="row">
            
        <?php 
            if ( comments_open() && ! post_password_required() ) :
            if ( 'post' == get_post_type() ) :?>
                <div class="col-sm-6"><span class="icomoon-icon-clock "></span><?php the_date('M j, Y'); ?></div>
                <div class="col-sm-6 text-right"><span class="icomoon-icon-user"></span><?php the_author();?></div>
            <?php
            endif;
            
            endif; ?>
        </div>
        <hr>
    <?php if ( is_search() ) :?>
        <div class="info">
        	<?php the_excerpt(); ?>
         </div>
    <?php 
	else: ?>
    	<div class="info">
        	<?php the_content(); ?>
         </div>
    <?php endif; 
	//wp_reset_postdata();?>
       
    	<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<div class="addthis_sharing_toolbox"></div>
        <div class="post-tags">
            <?php 
            $sposttags = get_the_tags($post_id);
            if($sposttags): ?>
                 Posted in : <span class="icomoon-icon-bookmark-2"></span>
            <?php
                foreach($sposttags as $stags){
            ?>
                 <span class="label label-default"><a href="<?php echo get_tag_link($stags->term_id); ?>"><?php echo $stags->name; ?></a></span>
            <?php } 
            endif;
            ?>
        </div>
        <div class="clearfix"></div>
        </div>
      </div>
      
    </div>
    <div class="col-md-4 col-sm-12 ind-blog-4">
    		<?php 
				if($tags):
				$first_tag = $tags[0]->term_id;
				$args=array(
				'tag__in' => array($first_tag),
				'post__not_in' => array($post->ID),
				'posts_per_page'=>3,
				);
				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) :
				$i=1;
				while ($my_query->have_posts()) : $my_query->the_post(); ?>

                        <div class="col-md-12 col-sm-4 feature-posts <?php if($i != 1){ ?> mtop40 <?php } ?>">
                                        <div class="blog-box">
										<?php
											$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
											if(!empty($feat_image)):
										?>
												 <img src="<?php echo $feat_image; ?>" class="img-responsive" />
										<?php
											endif;
										?>
                                        <div class="blog-box-inner">
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6"><span class="icomoon-icon-clock "></span><?php the_date('M j, Y')?></div>
                                                <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 text-right"><p class="author"><span class="icomoon-icon-user"><?php the_author();?></span></p></div>
                                                <div class="col-sm-12">
														<?php 
                                                        $sposttags = get_the_tags($post_id);
                                                        if($sposttags): ?>
                                                             <span class="icomoon-icon-bookmark-2"></span>
                                                        <?php
                                                            foreach($sposttags as $stags){
                                                        ?>
                                                             <span class="label label-default"><?php echo $stags->name; ?></span>
                                                        <?php } 
                                                        endif;
                                                        ?>
                                                    </div>
                                            </div>
                                            <hr>
                                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                            <div class="info">
                                                <p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?></p>
                                                <a class="blog-but" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute('echo=0'); ?>">Continue Reading »</a>
                                            </div>
                                         </div>
                                         </div>
                                    </div>
                   <?php
				   	$i++;
				   	endwhile;
				endif;
				endif;
				wp_reset_query();
			?>
       
            
    </div>
</div>