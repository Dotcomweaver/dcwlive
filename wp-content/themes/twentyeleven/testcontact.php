<?php
/*
Template Name: Test Contact
*/
get_header();
?>
<!-- Header for inner pages 
================================================== -->
<section class="light-gray-wraper tlbg">
<div class="container">
    <div class="col-md-5 col-sm-6 cnt-hdng sm-bg1">
		<div class="sidebar position-cnt" id="navbar-example">
        <h2>Contact us</h2>
        <span>We are here to help your business grow.</span>
		<p><i class="fa fa-envelope"></i>info@dotcomweavers.com  <span></span>  <i class="fa fa-phone"></i>888.315.6518  <!--|  201.880.6656--></p>
 <form class="cnt-frm " method="post" action="https://www.pipelinedeals.com/web_lead">

    <input type="hidden" name="w2lid" value="212f7ab56299" />
    <input type="hidden" name="thank_you_page" value="http://www.dotcomweavers.com/test-thank-you/" />

    <!-- Basic Demographic Fields -->
 
	
	
	
	
	<div class="form-group has-feedback">
	   <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
	</div>
		<div class="form-group">
    <input type="text" name="lead[company_name]"class="form-control"  placeholder="Company*" required>
		</div>
				<div class="form-group has-feedback">
    <input type="text" name="lead[phone]"class="form-control" placeholder="Phone*" required>
	</div>
	<div class="form-group">
    <input type="email" name="lead[email]"class="form-control"  placeholder="Email*" required>	</div>
    
    <div class="form-group">
    <input type="hidden" name="lead[work_country]"class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >	</div>
   	<div class="form-group">
    <input type="text" name="lead[summary]" class="form-control cmnts" placeholder="Comments">	</div>
    
<div class="form-group snd">
    <input type="submit" value="send"class="btn btn-warning col-sm-12 cpl-xs-12" /></div>
                    <?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?>
            <?php endif; // end sidebar widget area ?>
                </form>
        
		</div>	
    </div>	
    <div class="col-md-7 col-sm-6 sm-bg">
		<?php while ( have_posts() ) : the_post(); ?>
		<?php 
        remove_filter ('the_content', 'wpautop');
        the_content();//get_template_part( 'content', 'page' ); ?>
        <?php //comments_template( '', true ); ?>
    	<?php endwhile; // end of the loop. ?>
    </div>
</div>	
</section>
<?php get_footer(); ?>