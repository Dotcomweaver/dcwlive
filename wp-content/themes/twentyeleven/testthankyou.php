<?php
/*
Template Name: Test Thank you
*/

get_header();
?>
<section class="light-gray-wraper tlbg thank-you">
    <div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
        <?php 
        remove_filter ('the_content', 'wpautop');
        the_content();//get_template_part( 'content', 'page' ); ?>
        <?php //comments_template( '', true ); ?>
        <?php endwhile; // end of the loop. ?>
    </div>	
</section>
<?php get_footer(); ?>