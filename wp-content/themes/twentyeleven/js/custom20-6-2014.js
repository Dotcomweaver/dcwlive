(function($){
	$("#nav").slideDown().affix({
			offset: {
      				top:10
			}
		}).slideDown('100')
	$('#nav').on('affix.bs.affix', function () {
				$("#logo").addClass('col-sm-5').removeClass('col-md-5');
				$(".row #text-2 , .row #text-4").hide();
				$(".navscroll").parent().parent().addClass('col-sm-7').removeClass('col-md-7');
				$('#nav').next().css({'margin-top':'0'});
				//var topnavh = $('#nav').height();
				//$('#nav').next().css({'margin-top':topnavh});
				//alert('working');
			});
			$('#nav').on('affixed.bs.affix', function(){
				var topnavh = $('#nav').height();
				$('#nav').next().css({'margin-top':'69px'});
			});
			
			$('#nav').on('affixed-top.bs.affix', function () {
				$("#logo").addClass('col-md-5').removeClass('col-sm-5');
				$(".row #text-2, .row #text-4").show();
				$(".navscroll").parent().parent().removeClass('col-sm-7').addClass('col-md-7');
				$('#nav').next().css({'margin-top':0});
				//$('#nav').next().css({'margin-top':'102px'});
			});
	$('.team-breif').on('click', function(){
		$(".team-info-parent").remove();
		$(".team-membs .col-xs-4 > span").hide();
		
		var insertafter = $(this).parent().parent().parent();
		var d=document.createElement('div');
		
		var classname = "team-info-parent show";
		var id = $(this).attr('id');
		var data = '';
		$.ajax({
					url:"/wp-admin/admin-ajax.php",
					type:'POST',
					data:'action=my_special_ajax_call&id=' + id,
					 success:function(data)
							 {
									//console.log(data);
									var json_data = JSON.parse(data); 
									data="";
									j=1;
									posts_len = json_data.length;
									data += '<div class="team-info"><a class="close" href="javascript:void(0);"><span class="icomoon-icon-close"></span></a><div class="row">';
									
									if(json_data['embed_video_url'] != ''){
										data += '<div class="col-md-5 logos-video"><img src="http://img.youtube.com/vi/'+json_data['y_img']+'/0.jpg" class="img-responsive mrg-auto mtop20" alt=""><a href="javascript:void();"><span class="icomoon-icon-play-2"></span></a></div><div class="col-md-7 text-center"><h5>'+json_data['title']+', '+json_data['member_role']+'</h5><p>'+json_data['desc'].replace(/\'/gi, "\\\'")+'</p><div class="social-links">';
									}else{
										data += '<div class="col-md-12 text-center"><h5>'+json_data['title']+', '+json_data['member_role']+'</h5><p>'+json_data['desc'].replace(/\'/gi, "\\\'")+'</p><div class="social-links">';
									}
									
									if(json_data['facebook_url'] != ''){
										data += '<a href="'+json_data['facebook_url']+'" title="Facebook"><i class="fa fa-facebook"></i></a>';
									}
									
									if(json_data['twitter_url'] != ''){
										data += '<a href="'+json_data['twitter_url']+'" title="Twitter" ><i class="fa fa-twitter"></i></a>';
							 		}
									
									if(json_data['gplus_url'] != ''){
										data +='<a href="'+json_data['gplus_url']+'" title="Google Plus"><i class="fa fa-google-plus"></i></a>';
									}
									
									if(json_data['linkedin_url'] != ''){
										data += '<a href="'+json_data['linkedin_url']+'" title="RSS"><i class="fa fa-linkedin"></i></a>';
									}
									data += '</div></div></div></div>';
									$cls = $(d).addClass(classname).html(data).insertAfter(insertafter).find('a.close');
									
									$cls.on('click',function(){
																		$(".team-membs .col-xs-4 > span").hide();
																		$cls.parent().parent().remove();
																});
								},
						error:function(){
									var data = '';
								}
	                });
		$(this).parent().next().delay(500).fadeIn();
		
	});
	
	$(".vt_play").on('click',function(){
									  		var yvideo = $(this).attr('rel');
											
											$(".videoWrapper").html('<iframe width="560" height="349" src='+yvideo+' frameborder="0" allowfullscreen autoplay="true"></iframe>');
									  });
	$(".filter-content").hide();		  
	$(".termslist").on('click',function(){
									   		 var term = $(this);
											 var rel=$(this).attr('rel');	
									   		 if(rel == 'showall'){
												 $(".filter-content").hide();
												 $(".show_cat").show().animate(600);
											 }else{
													$(".show_cat").hide();
													$(".filter-content").html('<i class="fa fa-spinner fa-2x"></i>').show();
													$.ajax({
																url:"/wp-admin/admin-ajax.php",
																type:'POST',
																data:'action=portfolio_ajax_call&rel=' + rel,
																success:function(data)
																		 {
																			 	
																				var json_data = JSON.parse(data); 
																				//console.log(json_data['portfolio']);
																				data="";
																				j=1;
																				//alert(json_data['portfolio'].length);
																				
																				for( var i=0; i < json_data['portfolio'].length ; i++ ){
																					
																					data += '<div class="col-sm-4 col-xs-6"><div class="clients-box"><span>&bull;&bull;&bull;</span><div class="clients-in-box"><div class="overlay"><div class="hover-overlay clearfix"><a href="'+json_data['portfolio'][i]['link']+'"><i class="fa fa-eye fa-2x"></i><span>Dig <br>Deeper</span></a></div><h6>'+json_data['portfolio'][i]['title']+'</h6></div><img src="'+json_data['portfolio'][i]['portfolio_image']+'" alt=""></div></div></div>';
																				}
																				//alert(data);
																				$(".termslist").removeClass('active');
																				$(".filter-content").html(data);
																				term.addClass('active');
																				$(".filter-content").show().flip({
																												direction:'tb',
																												content:'this is my new content'
																											});
																				
																			},
																	error:function(){
																				alert("Something went wiered");
																				return false;
																			}
																
															});
											 }
									   });
		  
})(jQuery);

( function( $ ) {
    // Init Skrollr
    var s = skrollr.init({
        render: function(data) {
            //Debugging - Log the current scroll position.
            //console.log(data.curTop);
        }
    });
} )( jQuery );
jQuery(function($){	
	var header = $("div.main");
	var header_slide = $("div.fixed");
	var headerTop = header.offset().top  + header.height();
	var view = $( window );
	var share = $('.g-custom-share');
	view.bind(
		"scroll resize",
		function(){
			var viewTop = view.scrollTop();
			if ((viewTop > headerTop) && !header_slide.is( ".sticky" )){
				header_slide.addClass( "sticky" );
				//share.animate({marginTop: 70},1000)
			} else if ((viewTop <= (headerTop)) && header_slide.is( ".sticky" )) {
				header_slide.removeClass("sticky");
				share.css('margin-top','0px')
			}
			
		}
	);
	
	
	//mouser menu for second navigation in each page
	/*$('.second-nav').on('mouseover', '.dropdown-toggle', function(e){
    	$(e.currentTarget).trigger('click')
		//alert('working');
	})*/
	jQuery('.menu-item-has-children').hover(function() {
	  jQuery('.second-nav > ul > li').find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
	}, function() {
	  jQuery('.second-nav > ul > li').find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
	});
	jQuery(function($){
		$selector = $("#secondnav");
		$selector.scroll_navi();
	});
});
