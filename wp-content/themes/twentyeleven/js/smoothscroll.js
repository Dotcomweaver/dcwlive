// JavaScript Document
(function($){
$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			  $taroffset = target.offset().top;
			  $header = $("header").height();
			  $inner = $(".inner-header").height();
			$('html,body').animate({
			  scrollTop : ($taroffset-($inner-90))
			}, 1000);
			return false;
		  }
		}
	  });
});