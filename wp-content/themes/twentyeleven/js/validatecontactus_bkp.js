// JavaScript Document
var jQuery = jQuery.noConflict();
jQuery(function($){
$('#contactForm').bootstrapValidator({
       // message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			first_name: {
                message: 'Please input your name',
                validators: {
                    notEmpty: {
                        message: 'Please input your name'
                    },
                }
            },
			email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter email'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    },
				}
            },
        }
    });
});