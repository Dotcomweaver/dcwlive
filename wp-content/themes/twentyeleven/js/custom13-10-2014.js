var jQuer = jQuery.noConflict();
jQuery(function($){
	$("#nav").affix({
			offset: {
      				top:10
			}
		}).slideDown('100');
	$('#nav').on('affix.bs.affix', function () {
				$("#logo").addClass('col-sm-5').removeClass('col-md-5');
				$(".row #text-2 , .row #text-4").hide();
				$(".navscroll").parent().parent().addClass('col-sm-7').removeClass('col-md-7');
				//$('#nav').next().css({'margin-top':'0'});
				//var topnavh = $('#nav').height();
				//$('#nav').next().css({'margin-top':topnavh});
				//alert('working');
			});
			$('#nav').on('affixed.bs.affix', function(){
				var topnavh = $('header').height();
				//$('#nav').next().css({'margin-top':topnavh});
				$('body').css({'margin-top':topnavh});
				//console.log(topnavh);
			});
			
			$('#nav').on('affixed-top.bs.affix', function () {
				$("#logo").addClass('col-md-5').removeClass('col-sm-5');
				$(".row #text-2, .row #text-4").show();
				$(".navscroll").parent().parent().removeClass('col-sm-7').addClass('col-md-7');
				//$('#nav').next().css({'margin-top':0});
				//$('#nav').next().css({'margin-top':'102px'});
				$('body').css({'margin-top':'0px'});
			});
	var innerheadht = $('.inner-header').height();
	$("#secondnav").affix({
			offset: {
      				top:innerheadht
			}
		}).slideDown('100');
	$('#secondnav').on('affix.bs.affix', function () {
	});
	$('#secondnav').on('affixed-top.bs.affix', function () {
	});
	$('.team-breif').on('click', function(){
		$(".team-info-parent").remove();
		$(".team-membs .col-xs-6 > span").hide();
		
		var insertafter = $(this).parent().parent().parent();
		var d=document.createElement('div');
		
		var classname = "team-info-parent show";
		var id = $(this).attr('id');
		var data = '';
		$.ajax({
					url:"/wp-admin/admin-ajax.php",
					type:'POST',
					data:'action=my_special_ajax_call&id=' + id,
					 success:function(data)
							 {
									//console.log(data);
									var json_data = JSON.parse(data); 
									data="";
									j=1;
									posts_len = json_data.length;
									data += '<div class="team-info"><a class="close" href="javascript:void(0);"><span class="icomoon-icon-close"></span></a><div class="row">';
									//alert(json_data['desc']);
									if(json_data['embed_video_url'] != ''){
										data += '<div class="col-md-5 logos-video"><img src="http://img.youtube.com/vi/'+json_data['y_img']+'/0.jpg" class="img-responsive mrg-auto mtop20" alt=""><a href="javascript:void();" class="hvideo vt_play" data-target="#vt_modal" data-toggle="modal" rel="http://www.youtube.com/embed/'+json_data['y_img']+'"><span class="icomoon-icon-play-2"></span></a></div><div class="col-md-7 text-left"><h5 class="text-center">'+json_data['title']+', '+json_data['member_role']+'</h5><p>'+json_data['desc']+'</p><div class="social-links">';
									}else{
										data += '<div class="col-md-12 text-left"><h5 class="text-center">'+json_data['title']+', '+json_data['member_role']+'</h5><p>'+json_data['desc']+'</p><div class="social-links">';
									}
									
									if(json_data['facebook_url'] != ''){
										data += '<a href="'+json_data['facebook_url']+'" title="Facebook"><i class="fa fa-facebook"></i></a>';
									}
									
									if(json_data['twitter_url'] != ''){
										data += '<a href="'+json_data['twitter_url']+'" title="Twitter" ><i class="fa fa-twitter"></i></a>';
							 		}
									
									if(json_data['gplus_url'] != ''){
										data +='<a href="'+json_data['gplus_url']+'" title="Google Plus"><i class="fa fa-google-plus"></i></a>';
									}
									
									if(json_data['linkedin_url'] != ''){
										data += '<a href="'+json_data['linkedin_url']+'" title="RSS"><i class="fa fa-linkedin"></i></a>';
									}
									data += '</div></div>';
									if(json_data['embed_video_url'] != ''){
										data += '';
									}
									data += '</div></div>';
									
									$cls = $(d).addClass(classname).html(data).insertAfter(insertafter).find('a.close');
									
									$play = $(d).find('a.vt_play');
									$cls.on('click',function(){
																		$(".team-membs .col-xs-6 > span").hide();
																		$cls.parent().parent().remove();
																});
									$play.on('click',function(){
									  		var yvideo = $play.attr('rel');
											//alert(yvideo);
											$(".videoWrapper").html('<iframe width="560" height="349" src='+yvideo+' frameborder="0" allowfullscreen autoplay="true"></iframe>');
									  });
									
								},
						error:function(){
									var data = '';
								}
	                });
		$(this).parent().next().delay(500).fadeIn();
		
	});
	
	$(".vt_play").on('click',function(){
									  		var yvideo = $(this).attr('rel');
											//alert(yvideo);
											$(".videoWrapper").html('<iframe width="560" height="349" src='+yvideo+'?autoplay=1 frameborder="0" allowfullscreen autoplay="1"></iframe>');
									  });
	$("#vt_modal .close").on('click',function(){
			$(".videoWrapper").html('');
	});
	$(".video-testimonials").on('click',function(){
		
									  		var yvideo = $(this).attr('rel');
											//alert(yvideo);
											$(".videoWrapper").html('<iframe width="560" height="349" src='+yvideo+'?autoplay=1 frameborder="0" allowfullscreen autoplay="1"></iframe>');
									  });
	$(".filter-content").hide();		  
	$(".termslist").on('click',function(){
											//e.preventDefault;
									   		 var term = $(this);
											 var rel=$(this).attr('rel'); 
											 var id=$(this).attr('id');
											 var hash = window.location.hash;
											 var category_id = $('.categoryselected').html();
											 var category_rel = $('.categoryrelselected').html();
											// alert(category_term);
											 if(category_rel != '' && rel != 'showall'){
												alert('if');
												var hashvalue =  document.URL.substr(document.URL.indexOf('#')+1);
												if(hashvalue.indexOf('|')+1 != ''){
													//var splitted = hashvalue.split('|');
													var newhash = category_rel+'|'+rel;
													var newrel = category_rel+'|'+rel;
												}else{
														if(category_rel == hashvalue){
															var newhash = hashvalue+'|'+rel;
															var newrel = category_rel+'|'+rel;
														}else{
															var newrel = rel;
														}
												}
											 }else if(category_rel != '' && rel == 'showall'){
												alert('else if');
													var newhash = category_rel;
													var newrel = newhash;
											}else{
												alert('else');
												var newhash = rel;
												var newrel = rel;
											 }
											 
											window.location.hash = newhash;
											 var termname = $(this).html();
											 //alert(termname);
									   		/* if(rel == 'showall'){
												 $(".filter-content").hide();
												 $(".show_cat").show().animate(600);
												 $("#dropdownMenu1").html('Filter By<span class="glyphicon glyphicon-th-large"></span>');
											 }else{*/
													$(".show_cat").hide();
													$(".filter-content").html('<i class="fa fa-spinner fa-2x"></i>').show();
													$.ajax({
																url:"/wp-admin/admin-ajax.php",
																type:'POST',
																data:'action=portfolio_ajax_call&rel=' + newrel,
																success:function(data)
																		 {
																			// alert(data);
																			 if(data != 'null'){	
																			 	//alert('if condition');
																				var json_data = JSON.parse(data); 
																				//console.log(json_data['portfolio']);
																				data1 = "";
																				j=1;
																				//alert(json_data['portfolio'].length);
																				
																				for( var i=0; i < json_data['portfolio'].length ; i++ ){
																					
																					/*data += '<div class="col-sm-4 col-xs-6"><div class="clients-box"><span>&bull;&bull;&bull;</span><div class="clients-in-box"><div class="overlay"><div class="hover-overlay clearfix"><a href="'+json_data['portfolio'][i]['link']+'"><i class="fa fa-eye fa-2x"></i><span>Dig <br>Deeper</span></a></div><h6>'+json_data['portfolio'][i]['title']+'</h6></div><img src="'+json_data['portfolio'][i]['portfolio_image']+'" alt=""></div></div></div>';*/
																					//alert(json_data['portfolio'][i]['portfolio_image']);
																					if(json_data['portfolio'][i]['portfolio_image'] != null){
																					data1 += '<div class="col-sm-4 col-xs-6"><div class="featured-box"><div class="overlay"><p class="overlaycase-txt"><a href="'+json_data['portfolio'][i]['link']+'">'+json_data['portfolio'][i]['content']+'</a></p><div class="hover-overlay clearfix"><a href="'+json_data['portfolio'][i]['link']+'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div></div><a href="'+json_data['portfolio'][i]['portfolio_image']+'"><img src="'+json_data['portfolio'][i]['portfolio_image']+'" class="img-responsive" alt="" /></a><a class="full-box-link" href="'+json_data['portfolio'][i]['link']+'"></a></div></div>';
																					}
																				}
																				//alert(data);
																				$(".termslist").removeClass('active');
																				$(".filter-content").html(data1);
																				term.addClass('active');
																				$("#dropdownMenu1").html(termname+'<span class="glyphicon glyphicon-th-large"></span>');
																			 }else{
																				//alert('else condition');
																				$(".termslist").removeClass('active');
																				$(".filter-content").html('<div class="well well-sm">No items found</div>');
																				term.addClass('active');
																				$("#dropdownMenu1").html(termname+'<span class="glyphicon glyphicon-th-large"></span>');
																			 }
																			},
																	error:function(){
																				alert("Something went wiered");
																				return false;
																			}
																
															});
											/* }*/
									   });
$('.categorytermlist').on('click',function(){
											//e.preventDefault;
									   		 var term = $(this);
											 var rel=$(this).attr('rel');
											 var id=$(this).attr('id');
											 window.location.hash = rel;
											 var termname = $(this).html();
											 //alert(termname);
									   		 if(rel == 'showall'){
												 $(".filter-content").hide();
												 $(".show_cat").show().animate(600);
												 $("#dropdownMenu1").html('Filter By<span class="glyphicon glyphicon-th-large"></span>');
											 }else{
													$(".show_cat").hide();
													$(".filter-content").html('<i class="fa fa-spinner fa-2x"></i>').show();
													$("#dropdownMenu1").html('Filter By<span class="glyphicon glyphicon-th-large"></span>');
													$.ajax({
																url:"/wp-admin/admin-ajax.php",
																type:'POST',
																data:'action=portfolio_ajax_call&rel=' + rel,
																success:function(data)
																		 {
																			 	//alert(data);
																				//return false;
																				var json_data = JSON.parse(data); 
																				//console.log(json_data['portfolio']);
																				data="";
																				j=1;
																				//alert(json_data['portfolio'].length);
																				
																				for( var i=0; i < json_data['portfolio'].length ; i++ ){
																					
																					/*data += '<div class="col-sm-4 col-xs-6"><div class="clients-box"><span>&bull;&bull;&bull;</span><div class="clients-in-box"><div class="overlay"><div class="hover-overlay clearfix"><a href="'+json_data['portfolio'][i]['link']+'"><i class="fa fa-eye fa-2x"></i><span>Dig <br>Deeper</span></a></div><h6>'+json_data['portfolio'][i]['title']+'</h6></div><img src="'+json_data['portfolio'][i]['portfolio_image']+'" alt=""></div></div></div>';*/
																					//alert(json_data['portfolio'][i]['portfolio_image']);
																					if(json_data['portfolio'][i]['portfolio_image'] != null){
																					data += '<div class="col-sm-4 col-xs-6"><div class="featured-box"><div class="overlay"><p class="overlaycase-txt"><a href="'+json_data['portfolio'][i]['link']+'">'+json_data['portfolio'][i]['content']+'</a></p><div class="hover-overlay clearfix"><a href="'+json_data['portfolio'][i]['link']+'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div></div><a href="'+json_data['portfolio'][i]['portfolio_image']+'"><img src="'+json_data['portfolio'][i]['portfolio_image']+'" class="img-responsive" alt="" /></a><a class="full-box-link" href="'+json_data['portfolio'][i]['link']+'"></a></div></div>';
																					}
																				}
																				//alert(data);
																				$(".termslist").removeClass('active');
																				$(".filter-content").html(data);
																				term.addClass('active');
																				$('.categoryselected').html(id);
																				$('.categoryrelselected').html(rel);
																				//$("#dropdownMenu1").html(termname+'<span class="glyphicon glyphicon-th-large"></span>');
																			},
																	error:function(){
																				alert("Something went wiered");
																				return false;
																			}
																
															});
											 }
									   
	});
	//mouser menu for second navigation in each page
	/*$('.second-nav').on('mouseover', '.dropdown-toggle', function(e){
    	$(e.currentTarget).trigger('click')
		//alert('working');
	})*/
	$('.sortby , .dropdown-menu-right').on('mouseenter',function(e){ //alert('Hai');
				e.preventDefault;
				$('.dropdown').addClass('open');
		});
	$('.sortby , .dropdown-menu-right').on('mouseleave',function(){ //alert('Hai');
			$('.dropdown').removeClass('open');
	});
	$('.menu-item-has-children').hover(function() {
	  $('.second-nav > ul > li').find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
	}, function() {
	  $('.second-nav > ul > li').find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
	});
	$('#testimonials-rotate').carousel({
            interval: 12000
    });
	$(document).on('click','.morebutton:hidden,.morebutton:visible',function(){
			$(".testimonials blockquote").addClass('fullreview').removeClass('shortreview');
			$(".readmore .text-info").html('<a class="lessbutton" href="javascript:void(0)">Read less <i class="fa fa-angle-double-up fa-lg"></i></a>')
		});
	
	$(document).on('click','.lessbutton:hidden,.lessbutton:visible',function(){ 
			$(".testimonials blockquote").addClass('shortreview').removeClass('fullreview');
			$(".readmore .text-info").html('<a class="morebutton" href="javascript:void(0)">Read more <i class="fa fa-angle-double-down fa-lg"></i></a>')
		});
	
	$(document).on('click','.readmorevt:hidden,.readmorevt:visible',function(){
			$(this).prev().addClass('hghtauto').removeClass('hght190');
			$(this).html('Read less »').removeClass('readmorevt').addClass('readlessvt');
			$(this).parent().toggleClass('large');
			$("#posts").isotope('reLayout');
		 
		});
	
	$(document).on('click','.readlessvt:hidden,.readlessvt:visible',function(){
			$(this).prev().removeClass('hghtauto').addClass('hght190');
			$(this).html('Read More »').addClass('readmorevt').removeClass('readlessvt');
			$(this).parent().toggleClass('large');
			$("#posts").isotope('reLayout');
		  
		});
	
});