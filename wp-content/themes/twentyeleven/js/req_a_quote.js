function validateQuote(thisForm)
{
	//$("#quoteForm").attr("action", url_action);

	if( $.trim( $('input:text[name=visitor_name]').val() ) == '' )
	{
		alert('Please enter Name');
		$('input:text[name=visitor_name]').focus();
		return false;
	}

	if( $.trim( $('input:text[name=email]').val() ) == '' )
	{
		alert('Please enter Email');
		$('input:text[name=email]').focus();
		return false;
	}

	if( !chkEmail( $('input:text[name=email]').val() ) )
	{
		alert('Please enter a valid Email');
		$('input:text[name=email]').focus();
		$('input:text[name=email]').select();
		return false;
	}

	//thisForm.submit();
	return true;
}

//Function to check valid Email
function chkEmail(tmpStr)
{
	var email_pat = /^[a-z][a-z0-9_\.\-]*[a-z0-9]@[a-z0-9]+[a-z0-9\.\-_]*\.[a-z]+$/i;
	return(email_pat.test(tmpStr));
}