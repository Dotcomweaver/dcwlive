/* 
    Name: ScrollNavi.js
    Author: senthilkumar
    Website: http://senthildesigner.co.nr/
    mail: senthil2rajan@gmail.com
*/

(function ( $ ) {

    $.fn.scroll_navi = function(options) {
        
        var defaults = {
            speed        : 300
        };
        
        var settings = $.extend( {}, defaults, options );
        
        return this.each( function() {
            var win = $( window );
            var elem = $( this );
           // var elem_a = "#"+elem.attr("id")+" a";
            var wh = win.height();
			var header = ($("header").height()-40);
			var banner = $(".inner-header").height();
			wh=wh-(banner+1);
			//alert(wh);
           // elem.css("top",wh+"px");
		   /*win.resize(function() {
					if(banner<=topvalue)
					{
					   console.log('Working window resize');
						var h = $('header').height();
						$(elem).css({"position":"fixed","top":(h+1)+"px","width":"100%","background-color":"#f3f3f3","z-index":"999", "padding-bottom":"10px"});
					}
					else{
						$(elem).removeAttr('style');
					}
		   });*/
		   
            win.scroll(function() {
                var topvalue = win.scrollTop();
				
                if(banner<=topvalue)
                {
                   //alert(topvalue);
				    var h = $('header').height();
					$(elem).css({"position":"fixed","top":(h+1)+"px","width":"100%","background-color":"#f3f3f3","z-index":"999", "padding-bottom":"10px"});
                }
                else{
                    $(elem).removeAttr('style');
                }
            });
            
        });
    };
 
}( jQuery ));
