<?php
/**
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package WordPress
* @subpackage Twenty_Eleven
* @since Twenty Eleven 1.0
*/
get_header(); ?>
<?php if(is_page('social-hub')){?>
<section class="inner-header-socialhub">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
							} ?>
                	<h1><?php// the_title(); ?></h1>
                	<?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?>
              </div>
          </div>
        </div>
</section>
<?php }else{?>
<section class="inner-header">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
							} ?>
                	<h1><?php the_title(); ?></h1>
                	<?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?>
              </div>
          </div>
        </div>
</section>
<?php }?>
<?php 
if(is_page('Our Process') || is_page('Why select us as your Web Design and Development Partner?')): ?>
<section id="sub-nav">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="navbar secondary-nav navbar-default" role="navigation">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".second-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="#">Navigation</a>
                        </div>
                        <div class="navbar-collapse second-nav collapse">
                          <?php
							wp_nav_menu( array(
												  'menu' => 'company',
												  'depth' => 2,
												  'container' => false,
												  'menu_class' => 'nav navbar-nav',
												  //Process nav menu using our custom nav walker
												  'walker' => new wp_bootstrap_navwalker())
												);
						 
						?>
                       </div><!--/.nav-collapse -->
                    </div>
                                
                </div>
            </div>
        </div>
</section>
<?php endif; ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php 
		remove_filter ('the_content', 'wpautop');
		the_content();//get_template_part( 'content', 'page' ); 
?>
        
<?php //comments_template( '', true ); ?>
<?php endwhile; // end of the loop. ?>
<?php //if ( ! dynamic_sidebar( 'service_parallax' ) ) : ?>
<?php //endif; // custom widget ?>
<?php get_footer(); ?>