<?php
/*
Template Name: Tpl Thank you
*/
if(isset($_POST['hidsubmit'])) {
$phone = trim($_POST['phone']);
$company = trim($_POST['company']);
$first_name =  $_SESSION['userfirstname'] = trim($_POST['first_name']);
$email = trim($_POST['email']);
if(function_exists('stripslashes')) {
	$comments = stripslashes(trim($_POST['comments']));
} else {
	$comments = trim($_POST['comments']);
}
$hasError = false;
if(trim($first_name) == '') {
	$firstnameError = 'Please enter your first name.';
	$hasError = true;
	$first_nameerror = 'Please Enter Your Name';
} 
if(trim($email) == '') {
	$emailError = 'Please enter your email.';
	$hasError = true;
	$first_nameerror = 'Please Enter Your Email';
} 
if(!$hasError) {
$name = $first_name;
$emailTo = get_option('admin_email');
$emailTo = 'info@dotcomweavers.com,wrangel@dotcomweavers.com'; // uncomment to Live
//$emailTo = 'suneel@dotcomweavers.com'; // uncomment to test
$subject = 'Contact Request from '.$name;// Subject
$body = "<p><em>Note: This is an automatically generated email message.</em></p>
			<p>A visitor has filled out the contact form on Dotcomweavers website.</p>
			<p>The details are as below:</p>
			<p><b>Message:</b> ".$comments."</p>
			<p><b>Visitor&rsquo;s Information:</b><br /><br />
			Name: ".$first_name."<br />
			Email: ".$email."<br />
			Phone: ".$phone."<br />
			Company: ".$company."<br />
			IP Address: ".$_SERVER["HTTP_CF_CONNECTING_IP"]."<br />
			Country: ".$_SERVER["HTTP_CF_IPCOUNTRY"]."<br />
			<p>Thanks.<br />
			Webmaster<br />
			";
$logo_path = get_template_directory_uri();// for path
$file = get_template_directory_uri() . '/inc/template.txt';
$msg = file_get_contents($file);
$msg = str_replace("#EMAIL_CONTENT#", $body, $msg);
$msg = str_replace("#LOGO#", $logo_path, $msg);
$headers = array("From: ".$name." <info@dotcomweavers.com>", "Reply-To:".$name." <".$email.">", "Content-Type: text/html");
$h = implode("\r\n",$headers) . "\r\n";
if (mail($emailTo, $subject, $msg, $h)){
	$mail_flag = 'Success';
}else{
	$mail_flag = 'false';
}
//End Admin Email
//Sending Email to Customer
$body  ='
    <table width="70%" border="0" align="left" cellpadding="5" cellspacing="5" bgcolor="#F1F2EB">
 <tr>
 	<td colspan="2"><a href="http://www.dotcomweavers.com/" ><img src="http://www.dotcomweavers.com/wp-content/themes/twentyeleven/images/logo.png" border="0"></a></td>
 </tr>
  <tr>
    <td colspan="2" style="color:#666666; font-family:Arial, Helvetica, sans-serif;"><p>Automated reply:</p><p>Thank you for your inquiry about our services. We are working hard to respond to all of our customers within 2 business days. Until then, please feel free to check out some of our most exciting work.</p><p stle="margin-top:40px;">To Check our portfolio <a href="http://www.dotcomweavers.com/work">Click Here</a></p></td>
  </tr>  
  
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" style="color:#cccccc; font-family:Arial, Helvetica, sans-serif;">Thank You!<br />
Dotcomweavers<br />
<a href="http://www.dotcomweavers.com" style="color:#cccccc; font-family:Arial, Helvetica, sans-serif;">http://www.dotcomweavers.com</a></td>
  </tr>
  
</table>
';
$headers = array("From: Dotcomweavers <info@dotcomweavers.com>", "Content-Type: text/html");
$subject = "Thank You for contacting Dotcomweavers";
$h = implode("\r\n",$headers) . "\r\n";
if (mail($email, $subject, $body, $h)){
	$mail_flag = 'Success';
}else{
	$mail_flag = 'false';
}
			define('sugarEntry', TRUE);
			require_once('nusoap.php');
			$soapclient = new nusoapclient('http://sugarcrm.dotcomweavers.com/service/v2/soap.php?wsdl',true);
			
			$user_auth = array(
			'user_auth' => array(
			'user_name' => 'admin',
			'password' => md5('dcwcrm123')
			));
			  
			$result_array = $soapclient->call('login',$user_auth);
			$session_id = $result_array['id'];
			$user_guid = $soapclient->call('get_user_id',$session_id);
			
			$set_entry_params = array(
			'session' => $session_id,
			'module_name' => 'Leads',			
			'name_value_list'=>array(
									array('name' => 'first_name', 'value' => $first_name),
									array('name' => 'last_name', 'value' => $last_name),
									array('name' => 'phone_work', 'value' => $phone),
									array('name' => 'account_name', 'value' => $company),
									array('name' => 'description', 'value' => $comments),
									array('name' => 'email1', 'value' => $email),
									array('name' => 'website', 'value' => $website),
									array('name' => 'status', 'value' => 'New'),
									array('name' => 'assigned_user_name', 'value' => $user_guid))
									);
			
			$result = $soapclient->call('set_entry',$set_entry_params);
			//var_dump ($result);
	//End
	$_POST = array();
/*$pg = get_page_by_title( 'Thank You' );
$redirect_link = get_permalink($pg->ID);
header('Location: '.$redirect_link);
exit(0);
*/}
}
get_header();
?>

<section class="contact-inner-header">
    	<img src="/wp-content/themes/twentyeleven/images/contactus-banner.jpg" class="img-responsive">
        <div class="contact-banner-content">
        	<div class="container">
                <div class="row">
                    <div class="col-md-7 pull-right">
                        <h1 class="thank-you">Thank you<span>We will respond within 2 business days</span></h1>
                    </div>
              </div>
        	</div>
        </div>
</section>

<!-- Header for inner pages 
================================================== -->
<!-- Inner page first title and description starts here -->

<?php while ( have_posts() ) : the_post(); ?>
		<?php 
		//remove_filter ('the_content', 'wpautop');
		the_content();//get_template_part( 'content', 'page' ); ?>
        <?php //comments_template( '', true ); ?>
    <?php endwhile; // end of the loop. ?>
    <!-- EOF Inner page first title and description here -->
<!--<section id="map-container" class="divide">
        <div id="map_canvas"></div>
</section>-->
<?php get_footer(); ?>