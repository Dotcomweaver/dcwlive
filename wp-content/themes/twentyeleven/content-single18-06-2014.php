<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<div class="col-md-10 col-md-push-1">
<?php
if( ! post_password_required() && ! is_attachment()):
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
if(!empty($feat_image)):
?>
	<img src="<?php echo $feat_image; ?>" class="img-responsive" />
<?php
endif;
endif;
?>
<div class="blog-box-inner ind-blog-page">
<div class="row">
<?php 
if ( comments_open() && ! post_password_required() ) :
	if ( 'post' == get_post_type() ) :?>
    <div class="col-xs-4"><span class="icomoon-icon-clock "></span><?php the_date('M j, Y'); ?></div>
    <div class="col-xs-5"><span class="icomoon-icon-user"></span><?php the_author();?></div>
    <div class="col-xs-3 blog-social-links text-right">
        <a href="#" class="share"><span class="fa fa-share-alt"></span></a>
        <a href="#" class="fb"><span class="fa fa-facebook"></span></a>
        <a href="#" class="tw"><span class="fa fa-twitter"></span></a>
        <a href="#" class="pint"><span class="fa fa-pinterest"></span></a>
    </div>
</div>
<?php endif; 
endif; ?>
<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="info">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
<?php else : ?>
		<div class="info">
			<?php the_content(); ?>
			<?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
<?php endif; ?>
</div>
</div>