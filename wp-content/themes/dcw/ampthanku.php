<?php
/*
	Template Name: ampthanku
*/
get_header();
$siteUrl = get_site_url();
?>
<?php 

	$fullName = $_POST["full_name"];
	$eEmail = $_POST["email"];
	$pPhone = $_POST["phone"];
	$cCompany = $_POST["company"];
	$sSummary = $_POST["summary"];
	if($siteUrl == 'https://dotcomweavers.staging.wpengine.com'){
		$sSummary .= 'Lead from staging AMP';
	}else{
		$sSummary .= 'Lead from AMP';
	}
	$workCountry = $_SERVER["HTTP_CF_IPCOUNTRY"];
	$thankYouPage = get_bloginfo('url').'/thank-you/';
	echo $thankYouPage.'kalam';
	$thankRedirect = 'Location:'.$thankYouPage;

	if (!empty($fullName) && !empty($eEmail) && !empty($pPhone)) {
  	$ch = curl_init();
	$curlConfig = array(
	   CURLOPT_URL            => "https://www.pipelinedeals.com/web_lead",
	   CURLOPT_POST           => true,
	   CURLOPT_RETURNTRANSFER => true,
	   CURLOPT_POSTFIELDS     => array(
	       'w2lid' => '212f7ab56299',
	       'thank_you_page' => $thankYouPage,
	       'lead[full_name]' => $fullName,
	       'lead[email]' => $eEmail,
	       'lead[phone]' => $pPhone,
	       'lead[work_country]' => $workCountry,
	       'lead[company_name]' => $cCompany,
	       'lead[summary]' => $sSummary,
	   )
	);
	curl_setopt_array($ch, $curlConfig);
	$result = curl_exec($ch);
	curl_close($ch);
	http_response_code(200);
	header($thankRedirect);
}

?>


<?php get_footer(); ?>

