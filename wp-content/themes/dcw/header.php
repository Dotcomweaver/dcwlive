<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta property="og:image" content="<?php bloginfo('template_url');?>/img/brand-scroll.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<meta name="viewport" content="width=device-width">
<!-- <title><?php //wp_title(''); ?></title> -->
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/dcw/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/dcw2018.css?ver=1.2.39" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/style.min.css?ver=1.0.0" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/creative.css?ver=1.0.0" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/services.css?ver=1.0.0" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/progress.css?ver=1.0.0" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/aos.css" type="text/css">

<script defer src="<?php bloginfo('template_url'); ?>/js/all.js"></script>
<?php wp_head(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16800069-1', 'auto', {'name':'gaTracker'});
  ga('gaTracker.send', 'pageview');
  gtag('config', 'AW-987851546');

</script>
<?php if(is_page(11280) ){?>
 <!-- Event snippet for Magento Form Submission conversion page -->
 <script> gtag('event', 'conversion', {'send_to': 'AW-987851546/c34CCM2krpABEJrWhdcD'}); </script>
 <?php }?>
<?php dynamic_sidebar('structured_data'); ?>
<style type="text/css">.structured_dta p{margin: 0 !important;}</style>
<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
</head>

<body>

<?php if(is_page( array(479,7356) ) ){?>

<div class="home baner_withgooglereview homban_caroselsec">
<header>
      <div class="home_orangenewbaner1" style="cursor:url(<?php bloginfo('template_url'); ?>/images/Arrows.png),auto">
        <div class="homban_carousel11">
          <!-- <div class="item hombanr1" style="background:url(<?php bloginfo('template_url'); ?>/images/Home_Banner.jpg) no-repeat;background-size:cover;"> -->
          <div class="item hombanr1" style="background-image: url(<?php bloginfo('template_url'); ?>/images/DCW_New_Banner.jpg)">
            <!-- <div  class="snow_fall"></div> -->
            <div class="container">
                <div class="suprchrge_head">
                  <div class="header-group">
                  <!-- <img src="<?php bloginfo('template_url'); ?>/images/Arrows.png" class="down-arrow" style="cursor:pointer"> -->
                   <!-- <div class="banner-list-item">
                  <ul>
                    <li><a href="<?php echo get_site_url(); ?>/ecommerce/">eCommerce</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/custom-software/">Custom Software</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/mobile-apps/" style="border-right: 0px solid transparent">Mobile Apps</a></li>
                  </ul>
                </div> -->     
            
                <h1 class="clickable-div"> <?php the_field('home_banner_title' ); ?></h1>
                <h3>   <?php the_field('home_banner_content_'); ?>  </h3>

              <a href="<?php the_field('home_banner_btn_link'); ?>" class="home_banner_button"><?php the_field('home_banner_button_text'); ?></a>

                      
        
        </div>
                  
                
                </div>       
            </div>
          </div>
       
        </div>
       </div> 
        <div class="clearfix"></div>
          
       </header>
       
     </div>
<div class="clearfix"></div>



   
<?php } elseif (is_page( 6477 )){ ?>
 
<?php
get_header(); ?>
<header>
  <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
    <div class="google-adwords">
    <h1> SUPERCHARGE <small> Your Online Business </small> </h1>
    <form class="cnt-frm " method="post" id="register-form" action="https://www.pipelinedeals.com/web_lead">

        <input type="hidden" name="w2lid" value="212f7ab56299" />
        <input type="hidden" name="thank_you_page" value="http://www.dotcomweavers.com/thank-you/" />
        <div class="col-sm-6">
         <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
        </div>
        
        <div class="col-sm-6">
          <input type="text" name="lead[company_name]" class="form-control"  placeholder="Company">
        </div>
        
        <div class="col-sm-6">
          <input type="text" name="lead[phone]" class="form-control" placeholder="Phone*" required>
        </div>

        <div class="col-sm-6">
          <input type="email" name="lead[email]" class="form-control"  placeholder="Email*" required>  
        </div>

        <input type="hidden" name="lead[work_country]" class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >  
        
        <div class="col-sm-12">
          <textarea type="text" name="lead[summary]" class="form-control cmnts" placeholder="How can we help you achieve your eCommerce goals? " /></textarea>
        </div>
        
        <div class="clearfix"></div> 
        <div class="snd">
          <div class="start_btn"><input type="submit" value="Submit" class="btn btn-warning" /></div>
          <div class="call_btn"> <a href="tel:8553156518"><input type="button" value="Call Now: 855 315 6518" class="btn btn-warning" /></a></div>
          <div class="clearfix"></div> 
        </div>
    
      </form>
      </div>
  </div> 
  <div class="clearfix"> </div>
</div>
         <div class="clearfix"></div>
              <div class="h_services adword-services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce">
                 <a href="<?php echo get_site_url(); ?>/ecommerce/"><h4>eCommerce Development</h4></a>
                  </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive">
                   <a href="<?php echo get_site_url(); ?>/responsive-web-design/"><h4>Responsive Design</h4></a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication">
                  <a href="<?php echo get_site_url(); ?>/web-applications/"><h4>Web Applications</h4></a>
                 </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication">
                  <a href="<?php echo get_site_url(); ?>/mobile-application-mobile-website-development/"><h4>Mobile Applications</h4></a>
                 </div> 
              </div>
            </div>
          </div>
        </div>
 </header>
 <div class="clearfix"></div>

      <div class="o_line"></div>

    <div class="clearfix"></div>


    <script type="text/javascript">
      if(jQuery('header .google-adwords label').hasClass('error')){
        alert("has Class");
      };
    </script>

 <?php }else if(is_page('10152')) {?>
 <div class="home baner_withgooglereview homban_caroselsec">
<header>
      <div class="home_orangenewbaner1">
        <div class="homban_carousel11 arabic_home_baner">
          <div class="item hombanr1" style="background:url(<?php bloginfo('template_url'); ?>/images/Home_Banner.jpg) no-repeat;background-size:cover;">
            <!-- <div  class="snow_fall"></div> -->
            <div class="container-fluid">

            <div class="col-sm-12">
              <div class="col-sm-5 desktop-img">
                <img src="<?php bloginfo('template_url'); ?>/images/SBEMockup.png">
              </div>
              <div class="col-sm-7">
              <div class="banner-right">    
                 <div class="suprchrge_head">
                  <div class="header-group">
           <h1 class="top-space">تصاميم ويب <span class="highlight">عالية التأثير</span>
تطوير استراتيجيات التسويق الرقمي</h1>
        </div>
                  
                
                </div>
            
                 <div class="banner-list-item">
                  <ul>
                   <li><a href="<?php echo get_site_url(); ?>/ecommerce/">eCommerce</a></li>
                   <li><a href="<?php echo get_site_url(); ?>/custom-software/">Custom Software</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/mobile-apps/" style="border-right: 0px solid transparent" >Mobile Apps</a></li>
                  </ul>
                </div>
               
                </div>
              </div>
            </div>

            </div>
          </div>
        
        </div>
        
        
       </div> 
        <div class="clearfix"></div>
           <div class="h_services project_div">
          <div class="container">
          
          <div class="row services_ar">
              <?php 
               if( have_rows('homepage_services') ): ?>
              <?php while( have_rows('homepage_services') ): the_row(); 
               $title = get_sub_field('text');
                $content = get_sub_field('content');
                 $link = get_sub_field('link');
              //print_r($banner);
               ?>
            <div class="col-sm-6 col-md-3">
                <div class="box">
                  <h4><?php echo $title;?></h4>
                  <p><?php echo $content;?></p>
                 <?php echo $link;?>
                </div> 
              </div>
             <?php endwhile; ?>
            <?php endif; ?>
            </div>
           
          </div>
        </div>
       </header>
     </div>
<!-- Head home banner with carousel sliders and google tringle review thing END-->

 <?php }?>
 <?php if(is_front_page()){?>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top home-nav">
  <?php } else {?>
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
  <?php }?>
<div class="container">
<div class="topcallreq_sec">
  <div class="top_call">
      <a href="tel:8883156518" class="top_phone">201.880.6656</a>
  </div>
  <div class="top_rq">
    <span class="blue-txt-btn"><a href="<?php echo get_site_url(); ?>/contact-us/"> CONTACT </a> </span>
  </div>
  <div class="clearfix"> </div>
</div>

<div class="navbar-header col-md-3 col-sm-3 col-xs-7">
  <button type="button" class="navbar-toggle collapsed bs-1-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1 ">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>

  <a class="navbar-brand page-scroll scrl-main dsk-logo" href="<?php echo get_site_url(); ?>/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/DCW_new_with_R.png"></a>
  <a class="navbar-brand page-scroll mbl-logo" href="<?php echo get_site_url(); ?>/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/DCW_new_with_R.png"></a>
</div>
<div class="flip-container col-md-9 col-sm-6 hm-pg-flp">
  <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
    <!-- <div class="flipper">
        <ul>
          <li>201.880.6656</li>
         <li id="orange"> <a href="<?php echo get_site_url(); ?>/contact-us/">CONTACT</a></li>
        </ul>
    
    </div> -->
    <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav main_nav_bar">%3$s</ul>' )); ?>
  </div>
</div>
<button type="button" class="menu-rt  col-xs-5 button">

<span class="line_1"></span><span class="line_2"></span> <!-- <span class="fa fa-bars " aria-hidden="true"></span> --></button>


<button type="button" class="mobile-port col-xs-5 button">

<span class="line_1"></span><span class="line_2"></span> <!-- <span class="fa fa-bars " aria-hidden="true"></span> --></button>
</div>
</nav> 
<div class="over_lay"></div>
<!-- Mobile tablet slide navigation Start -->
  <div class="nav_multi_level2">
<ul class="nav navbar-nav main_nav_bar">
<div class="slide_menubtn"><button type="button" class="menu-rt  col-xs-5 button"><span class="fa fa-bars " aria-hidden="true"></span></button>
</div>
<li id="menu-item-6026" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6026"><a href="<?php echo get_site_url(); ?>/work/">Portfolio</a></li>
<li id="menu-item-5999" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5999"> <span class="mobile_caret">
  
</span> <a href="<?php echo get_site_url(); ?>/services/">Services</a>
  <ul class="mobile_service_navigation">
    <div class="mobile_dropdown_content">
      <div class="mobile_ecommerce_list">
        <p class="megamenu-title border"><a href="<?php echo get_site_url(); ?>/ecommerce/">ECOMMERCE</a></p>
          <div class="eCommerce_mobile_list_item">
            <ul>
              <li> <a href="<?php echo get_site_url(); ?>/magento/">Magento Development</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/marketplaces/">Marketplaces</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/re-platforming/">Website Re-Platforming</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/strategy-consultation/">Ecommerce Strategy</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/integrations/">eCommerce Integrations</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/industrial-parts-machinery/">Industrial Parts &amp; Machinery</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/b2b-ecommerce/">B2B Ecommerce</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/food-perishables/">Food &amp; Perishables</a></li>
            </ul>
          </div>
      </div>
      <div class="custom_dev_mobile_list">
        <p class="megamenu-title border"><a href="<?php echo get_site_url(); ?>/web-development/">WEB DEVELOPMENT</a></p>
          <div class="customdev_mobile_list_item">
            <ul>
              <li> <a href="<?php echo get_site_url(); ?>/magento/">Marketing Websites</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/marketplaces/">Custom Software</a></li>
              <li> <a href="<?php echo get_site_url(); ?>/mobile-apps/">Mobile Apps</a></li>
            </ul>
          </div>
      </div>
      <div class="mobile_support_section" style="background-image:url(<?php bloginfo('template_url'); ?>/images/Mega_menu_BG.jpg)">
        <?php dynamic_sidebar('header_managed_support_mobile'); ?>
      </div>
    </div>
  </ul>
</li>
<!-- <li id="menu-item-9350" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9350"><a href="<?php echo get_site_url(); ?>/industries/">Industries</a></li> -->
<li id="menu-item-6002" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6002"><a href="<?php echo get_site_url(); ?>/why-dotcomweavers/">ABOUT</a></li>
<li id="menu-item-7294" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7294"><a href="<?php echo get_site_url(); ?>/blog/">Blog</a></li>
<!-- <li id="menu-item-6012" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6012"><a href="<?php echo get_site_url(); ?>/white-papers/">Case Studies</a></li> -->
</ul>
</div>
</div>

<!-- Mobile tablet slide navigation End -->
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" id="play-button" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>

<!-- google Reviews plugin customization -->


<!-- service sub menu navigation start-->
<ul class="service-navigation">
  <div class="dropdown-content">
    <div class="col-sm-12 p0">
      <div class="col-sm-8 p0 left-side-list">
      <div class="col-sm-8 p0 eCommerce-nav">
        <p class="megamenu-title border"> <a href="<?php echo get_site_url(); ?>/ecommerce/">ECOMMERCE</a></p>  
        <?php wp_nav_menu( array('menu' =>'services submenu ecommerce' , 'menu_class' => 'service_ecommerce' ) ); ?>            
      </div>
        <div class="col-sm-4 customdev-nav">
          <p class="megamenu-title border"> <a href="<?php echo get_site_url(); ?>/web-development/">WEB DEVELOPMENT</a></p>
          <div class="customdev-nav-list">
          <?php wp_nav_menu( array('menu' =>'service-submenu-custom-development' , 'menu_class' => 'service_customdevelopment' ) ); ?>
          </div>                        
        </div>
      </div>
      <div class="col-sm-4 manage-support" style="background-image:url(<?php bloginfo('template_url'); ?>/images/Mega_menu_BG.jpg)">
        <?php dynamic_sidebar('header_managed_support_desktop'); ?>
      </div>
    </div>
  </div>
</ul>
<!-- service sub menu navigation end-->

<div class="sub-menu-overlay">
  
</div>


<?php dynamic_sidebar('reviews');?>


<style>
@media only screen and (min-width:768px) and (max-width: 1024px){
.pattern-overlay iframe{top:0px !important;}  
header{height:auto;}
 }
@media(max-width:767px) {
.header-content-inner.hm_pg_cnt{top:30%;}header{height:auto;}
}

</style>