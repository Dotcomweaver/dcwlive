<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="container">
			<section class="error-404 not-found">
				
					<h1 class="page-title"><?php _e( '<span>Oops!</span> That page can&rsquo;t be found.', 'twentyfifteen' ); ?></h1>
				
					<img src="/wp-content/themes/dcw/images/404new.png" class="img-responsive img404">
					<p> <a href="/"> Click here </a>to land back on the home page! Unless you were looking for...</p>
					<div class="container other_links404">
						<div class="row">
							<div class="col-sm-3 col-xs-6">
								<a href="/about-us/" ><img src="/wp-content/themes/dcw/images/er_company.png" class="icons"></a>
								<a href="/about-us/" >
									Information about <span>our company</span>
								</a>
							</div>
							<div class="col-sm-3 col-xs-6">
								<a href="/services/" ><img src="/wp-content/themes/dcw/images/err_services.png" class="icons"></a>
								<a href="/services/" >
									An overview of <span>our services</span> 
								</a>
							</div>
							<div class="col-sm-3 col-xs-6">
								<a href="/work/" ><img src="/wp-content/themes/dcw/images/err_work.png" class="icons"></a>
								<a href="/work/" >
									Examples of <span>our work</span>
								</a>
							</div>
							<div class="col-sm-3 col-xs-6">
								<a href="/contact-us/" ><img src="/wp-content/themes/dcw/images/err_contact.png" class="icons"></a>
								<a href="/contact-us/" >
									A way to <span>contact us</span>
								</a>
							</div>
						</div>
						
					</div>

				<div class="page-content">
					<p><?php _e( 'Still not what you are looking for? Search for it here:', 'twentyfifteen' ); ?></p>

					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

	
</div>

<?php get_footer(); ?>
