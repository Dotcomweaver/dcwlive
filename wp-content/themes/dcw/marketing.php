<?php
/*
Template Name: marketing
*/
get_header();?>
<!-- service submenu-->
<div class="service-sub-menu">
  <div class="container">
    <div class="service-menu-list">
        <ul>
        <li> <a class="active" href="../ecommerce/"> ECOMMERCE </a> </li>
         <li> <a  href="../magento/"> Magento</a></li>
          <li> <a href="../erp-integration/"> ERP</a> </li>
           <li> <a class="active"  href="../ecommerce-marketing/"> Marketing </a> </li>
        <li> <a href="../custom-software/"> CUSTOM SOFTWARE</a></li>
        <li> <a href="../mobile-apps/"> MOBILE APPS </a> </li>
        </ul>
    </div>
  </div>

</div>



<!-- service sub menu end-->


<!--banner start-->
   <div class="banner sticky_subnav">
    <div class="container">
      <div class="col-sm-12 banner_badge_center">
      <div class="col-sm-5 col-xs-12 pull-right">
        <div class="banner-right">
            <?php 
                $countt = get_field('count_number');
                $image = get_field('image');
                if(!empty($countt)){ ?>
                  <div class="magento-project-counter">
                    <div class="site_lunched_box">
                        <div class="project_counter">
                            <h1><?php the_field('count_number'); ?></h1>
                      </div>
                      <div class="site_lunched_description">
                          <p><?php the_field('banner_box_content'); ?></p>
                      </div>
                      <div class="get_magento_site_btn">
                          <a href="<?php bloginfo('url');?><?php the_field('banner_box_link_url'); ?>"><?php the_field('banner_box_link_text'); ?></a>
                      </div>
                    </div>
                  </div>
                <?php } elseif( !empty($image) ) { ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php } else {
                    $banvideo = the_field('video');
                    if($banvideo){
                        echo $banvideo; 
                    }
                }
            ?>
        </div>
        </div>
        <div class="col-sm-7 col-xs-12 pull-left">
          <div class="banner-left">
            <h1> <?php the_field('ecommerce_heading');?></h1>
            <?php the_field('banner_content');?>
          </div>
        </div>
        
      </div>
    </div>     
   </div>
   <!--banner end-->

  <!--related work-->
    <div class="project_div services">
         <div class="container">
            <h4 class="work_hdng"><?php the_field('work_heading');?></h4>
            <h3 class="mid_dcm"><?php the_field('work_content');?></h3>
            <div class="row photo_gallery ">
            <div class="owl-carousel_gal">
              <?php
                $portfolio_two = get_field('portfolio_items');
                $port_value = array();
                foreach ($portfolio_two as $portfoli) {
                  $port_value[] = $portfoli->ID;
                }
            $folio_ids_arr =  $port_value;
  if (count($folio_ids_arr)) :
  
  $args = array(
          'post__in' => $folio_ids_arr,
          'post_type'=> 'portfolio',
          'order' => 'DESC',
          'posts_per_page' => -1
        );
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
               <div class="item <?php echo rtrim($posttags, " "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4> <a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h4>
                       
            <p><?php
            // Get a list of terms for this post's custom taxonomy.
            $project_cats = get_the_terms($post->ID, 'keyfeature');
            // Renumber array.
            $project_cats = array_values($project_cats);
            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
              <?php  echo $project_cats[$cat_count]->name; if ($cat_count<count($project_cats)-1){ echo ', '; }
            }
            ?></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif; wp_reset_query();
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
          </div>
            </div>
         </div>
      </div>
   <!-- related work end-->



   <!--main body start-->
    <section class="main-body">
      <div class="container max-container">
        
      <?php if( have_rows('highlights') ): ?>


  <?php while( have_rows('highlights') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          <div class="col-sm-5 img_middle imag">
                               <h3 class="mobile-service-title"><?php echo $Text;?></h3>
                                <h1 class="mobile-service-subtitle"><?php echo $heading;?></h1>   
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>

          <div class="col-sm-7 content_middle ">
            <div class="purpose">
              <h3 class="desktop-service-title"><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1 class="desktop-service-subtitle"><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>
         </div>
            <?php endwhile; ?>



<?php endif; ?>
       
       
        </div>
      
  </section>
   <!--main body end-->
<div class="clearfix"></div>

   <!--testimonial block section start-->  
   <div class="dcw-testimonial">
     <div class="container">
      <?php if( have_rows('testimonial') ):  while( have_rows('testimonial') ): the_row();
        $logo = get_sub_field('logo');
        $review = get_sub_field('review');
        $author = get_sub_field('author');
        $designation = get_sub_field('designation');
        $portfolio_name = get_sub_field('portfolio_name');
        $portfolio_link = get_sub_field('portfolio_link');
      ?>
       <h5> <img src="<?php bloginfo('template_url'); ?>/images/heart.png">  <span class="testimonial_brand">CLIENTS LOVE US</span></h5>
            <h4> <?php echo $review; ?> </h4>
              <div class="project-review">
         <!-- <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" /> -->
          <div class="project-reviewer">
            <p><?php echo $author; ?>, <a href="<?php echo get_site_url(); ?>/<?php echo $portfolio_link; ?>"><?php echo $portfolio_name; ?></a></p>

         <!--  <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked "></span>
<span class="fa fa-star checked "></span> -->
          </div>         
       </div>
          <?php endwhile; endif; ?>
    </div>
  </div>
 <!--testimonial block section end--> 
  
  

   <!-- testimonial section end-->
    <!--graph section start-->
    <section class="main-body testi_second">
      <div class="container max-container">
        
      <?php if( have_rows('highlights_loop2') ): ?>


  <?php while( have_rows('highlights_loop2') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          <div class="col-sm-5 img_middle imag ecom-marekting">
                               <h3 class="mobile-service-title"><?php echo $Text;?></h3>
                                <h1 class="mobile-service-subtitle"><?php echo $heading;?></h1>   
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>

          <div class="col-sm-7 content_middle ">
            <div class="purpose">
              <h3 class="desktop-service-title"><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1 class="desktop-service-subtitle"><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>
         </div>
            <?php endwhile; ?>



<?php endif; ?>
       
       
        </div>
      
  </section>

   <!--graph section end-->

  

   <!--work together section-->
  <div class="work-together clearfix">
    <div class="sub-container">
      <div class="col-sm-12">
        <div class="col-sm-4">
         <?php 
          $image = get_field('call_to_action');

          if( !empty($image) ): ?>

            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

          <?php endif; ?>
        </div>

        <div class="col-sm-8">
          <h1><?php the_field('work_together_title');?></h1>
          <div class="work-together-content">
              <p><?php the_field('work_together');?></p>
              <p class="touch"> <a href="<?php the_field('work_together_link');?>"> <?php the_field('work_together_link_content');?> </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--work together section end-->
 
<?php get_footer(); ?>
