<?php
/*
Template Name: Responsive
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_responsive.png"></div>

<div class="container">
<div class="ecmmorce-iner clearfix">
                     
    <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
    <img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/Responsive.png">
    </div>
	<div class="col-lg-6 col-sm-6 col-xs-12">
     <h3>RESPONSIVE WEB DESIGN FOR ECOMMERCE</h3>
     <p>The need for user convenience makes usage of multi-screens a way of life. Responsive website design accommodates user behavior today and anticipates the device delivery innovations that will impact consumer behavior in the future. Consolidating all your content into one site, responsive website design delivers seamless interaction and a flawless user experience device-to-device. We’ll leverage our responsive design expertise and help you engage your customers with fluid design, flexible images and overall simplicity. Reach your customers on multiple touch points to increase your conversions and grow your business today, tomorrow and in the years to come.</p>

     </div>
</div>
 </div>
<?php echo do_shortcode('[jumptoservicesfolio]');?>

<section id="ecmrce-blg-slider clearfix">
	<div class="container content-headings">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="emcre-cnt">
			<h3>BE CONSISTENT. SAVE TIME.</h3>
			<p>Responsive web design is reliant on Cascading Style Sheets, more commonly known as CSS, a form of website communication that governs features like fonts, layouts, spacing and colors. CSS takes the content you already have and makes it more attractive; what's more, it also ensures consistent styling on web pages throughout your site. And, it saves time, Make a change once; it appears on every page. Encounter a glitch. With responsive design, one fix repairs all your user access systems. That's crisis management at its best.</p>
			</div>
			<div class="emcre-cnt">
			<h3>ACCOMMODATE USERS ANYTIME ANYWHERE</h3>
			<p>Responsive website design offers your users a seamless experience regardless of device. They can browse, transact or grab information on-the-run. Layouts shift seamlessly device to device. And, because devices dictate behaviors, users can swipe, hover or touch. With responsive website design, we vary navigation by device. What's more, on small screens, we make sure touch regions are big enough for easy clicking.</p>
			</div>
			<div class="emcre-cnt">
			<h3>FASTER AND EASIER FRONT-END DEVELOPMENT</h3>
			<p>We leverage Bootstrap, an increasingly popular, flexible open-source framework, to create your site. Front-end design and site execution is made easy with Bootstrap. Your users will have an enjoyable experience. You'll have the benefit of continuous metric capabilities to see whether it's the prominent display of compelling content or a call-to-action that's center stage in turning browsers to buyers when they're on your responsive website.</p>
			</div>
			<div class="emcre-cnt">
			<h3>SIMPLIFIED USER EXPERIENCE AND NAVIGATION</h3>
			<p>Design constraints created by the use of smaller screens have led developers and designers to find common ground in developing for all devices. This means design, images, content presentation and functionality have been simplified for multiple device use. This simplicity helps with navigation and interactivity allowing for customers to experience your site more efficiently and quickly. The payoff: more conversions for you.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="place-holder">
			<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="https://player.vimeo.com/video/109278606" class="vt_play">
			<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
    <img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/1.jpg" alt="video" class="img-responsive">
				</a>
			</div>
			       <h2>Related Blog</h2>
                      <?php
                    $args=array(
                      'tag' => 'web-design',
                      'showposts'=>2,
                      'orderby' => 'date',
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                    
                      while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="place-holder">
			<p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?>
			<span><a href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-long-arrow-right"></i></a></span>
			</p>
			</div>
                       <?php
                      endwhile;
                    } //if ($my_query)
                  wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
		
		</div>
	</div>


</section>
<section class="video-testmonilas">
	<div class="container">
		<div class="row">
<!--			<div class="col-md-3 col-sm-3 col-xs-12">
				<p><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/ecmrc_video.jpg"></p>
			</div>-->
			<div class="col-md-9 col-sm-9 col-xs-12">
                            <?php echo do_shortcode('[jumptoservicesvt]');?>
                <!--	<span class="cma-left"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-left.png"></span>
				<p>his section of text is up to you. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
				<span>- First Name Last Name, President of Battlefield Collection</span>
				</p>
				<span class="cma-right"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-right.png"></span>-->
			</div>
		</div>
	</div>
</section>

<section class="ecmrce-accordians">

	<div class = "container">

  <h3>More Questions about Responsive Design</h3>
	<?php echo do_shortcode('[jumptoservicesfaqs]');?>

  
	</div>
</section>


 
<?php get_footer(); ?>
