<?php
/*
Template Name: beforeafter
*/
get_header();?>
<div class="work clearfix beforeafter_bann">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1> <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?>
				 	<!-- <span>Get an inside look at the real success stories, and how we got them there.</span> --></h1>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/beforeafter_icon.png"></div>
<div class="clearfix"></div>
<section class="devide beafter_container">
				<?php           
                		$bf_query = new WP_Query(
                            array('post_type' => 'beforeafter',                               
                                  'orderby' => 'post_date',
                                  'posts_per_page' => '-1',

                            )
                        );  
				?>
				<?php //$content = get_the_content();
				 	$j = 0;
				    while ($bf_query->have_posts()) : $bf_query->the_post();
					$counter = 1;
				    $bflink = get_permalink( $id );
				    $the_link = get_permalink();
				    $title = get_the_title(); 
				    $banner = get_field('banner');
				    $the_content =  get_the_content();
				    $url = get_field('url');
				 ?>
<div class="beafter_section">
	<div class="container">
		<div class="vline"></div>
		<div class="row">
			<div class="col-md-10 col-md-push-1">
				<h2 class="subtitle subt_txttrm"><?php echo $title; ?></h2>
				<p class="aboutxt text-center"><?php echo $the_content;?></p>
			</div>
		</div>
	</div>

	<div class="ba_bg" style="background: url(<?php echo $banner['url']; ?>) no-repeat center;">

	<div id="beafter_sliderid<?php echo $j;?>" class="carousel slide beafter_slider" data-ride="carousel">
	<ol class="carousel-indicators">
          <li data-target="#beafter_sliderid<?php echo $j;?>" data-slide-to="0" class="active"></li>
          <li data-target="#beafter_sliderid<?php echo $j;?>" data-slide-to="1" class=""></li>
          <li data-target="#beafter_sliderid<?php echo $j;?>" data-slide-to="2" class=""></li>
      
 	 </ol>

		<div class="container">
			<div class="carousel-inner" role="listbox">
				<?php if( have_rows('scroll') ): ?>



  <?php while( have_rows('scroll') ): the_row(); 
  // vars
    $before = get_sub_field('before');
    $after = get_sub_field('after');
    $listtxt = get_sub_field('productlisttxt');
   ?>
			<div class="item <?php if($counter <= 1){echo " active"; } ?>">
				<div class="row">
					<div class="col-sm-6">
						<div class="batxt">
							Before
						</div>
						<div class="ba_img"><a href="JavaScript:Void(0)" data-toggle="modal" data-viewp="<?php echo $url;?>" data-mdlc="<?php echo $listtxt; ?>" data-whatever="<?php echo $before['url']; ?>" data-target="#myModal"><img class="img-responsive csthmimg" src="<?php echo $before['url']; ?>" alt="<?php echo $before['alt'] ?>"></a></div>
											</div>
					<div class="col-sm-6">
						<div class="batxt">
							After
						</div>
						<div class="ba_img"><a href="JavaScript:Void(0)"  data-viewp="<?php echo $url;?>" data-mdlc="<?php echo $listtxt; ?>" data-whatever="<?php echo $after['url']; ?>" data-toggle="modal" data-target="#myModal"><img class="img-responsive csthmimg" src="<?php echo $after['url']; ?>" alt="<?php echo $after['alt'] ?>"></a></div>
						</div>
					<div class="prolisttxt"><?php echo $listtxt; ?></div>
				</div>
			</div>
			<?php $counter++;
      endwhile; ?>
<?php endif; ?>
<?php  wp_reset_query();?>
			
			</div>
			 	<a class="left carousel-control" href="#beafter_sliderid<?php echo $j;?>" role="button" data-slide="prev">
				<span class="fa fa-angle-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>

				<a class="right carousel-control" href="#beafter_sliderid<?php echo $j;?>" role="button" data-slide="next">
				<span class="fa fa-angle-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>
		</div>
		</div>
	</div>
	<?php if  (!empty( $url )) { ?>
		<div class="container beaft_btn">
	<a href="<?php echo $url;?>" class="ba_btn ba_btn_main">VIEW PROJECT</a>
	</div>

	<?php }
      else {

     } ?>
	
 </div>
<?php 
wp_reset_query();
$j++;
endwhile; ?>
<br> <br> <br> 
<div class="triangleup"><a href="/press-awards/">
We Make Headlines.
<span class="sub_subtitle">Read More</span>
</a></div>  

<!-- Modal -->
<div id="myModal" class="modal fade beafter_pop" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    
	
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
     
      <div class="modal-body">
      	<img class="modal_img img-responsive" />
        <!-- <img class="img-responsive csthmimg" width="1000" src="<?php echo $popup['url']; ?>" alt="<?php echo $popup['alt'] ?>"> -->
      </div>
     
				
		<div class="bapopbtn">
      <a href="#" class="ba_btn ba_btn_pop">VIEW PROJECT</a>
      </div>
	
         
     
    </div>

  </div>
</div>


<script type="text/javascript">
//alert();


jQuery('#myModal').on('show.bs.modal', function (event) {
  var button = jQuery(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever');
  var mdlc = button.data('mdlc');
  var viewp = button.data('viewp'); // Extract info from data-* attributes
  var modal = jQuery(this);
  //modal.find('.modal-title').text('New message to ' + recipient)

  modal.find('.modal_img').attr('src', recipient );
  modal.find('.modal-title').text(mdlc);
  modal.find('.ba_btn_pop').attr('href', viewp );
  
})


	
</script>


 
<?php get_footer(); ?>
