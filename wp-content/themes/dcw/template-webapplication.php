<?php
/*
Template Name: Web Application
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_web_appli.png"></div>

<div class="container">
<div class="ecmmorce-iner clearfix">
                     
    <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
    <img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/applicatin.png">
    </div>
	<div class="col-lg-6 col-sm-6 col-xs-12">
     <h3>THE FOUNDATION TO HELP YOU GROW YOUR BUSINESS</h3>
     <p>Understanding the financial and operational objectives for your online business is important to building a successful web application for your business. We apply our extensive knowledge in web service standards of UDDI, SOAP, XML and WSDL to your existing databases and web-based systems to map out the most efficient processes to achieve your business goals. This will provide you with a firm foundation for current and future integrations with your customers, suppliers, other third parties and their computer systems and ensure the continued competitiveness of your business.</p>

     </div>
</div>
 </div>
<?php echo do_shortcode('[jumptoservicesfolio]');?>

<section id="ecmrce-blg-slider clearfix">
	<div class="container content-headings">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="emcre-cnt">
			<h3>CLOUD-BASED APPLICATIONS</h3>
			<p>We leverage SugarCRM and Salesforce to bring the cloud to the customized solutions we provide to your employees and your customers. This enables you to archive all customer transactions and have that data at your fingertips. You'll be able to develop social profiles and the actionable insights that facilitate targeted marketing campaigns and fulfill the needs of your customers.</p>
			</div>
			<div class="emcre-cnt">
			<h3>REDUCED PAGE LOAD TIME TO INCREASE CONVERSIONS</h3>
			<p>We leverage integrated service applications to help customize your website development. This facilitates faster page loads and a natural flow on the user's path to a desired objective. Be it a product purchase, subscription service, or gleaning information, page-load speed can make the difference between website abandonment or the website stickiness that enables intuitive cross-channel experiences and ramped up conversions.</p>
			</div>
			<div class="emcre-cnt">
			<h3>SEAMLESS INTEGRATION OF BUSINESS APPLICATIONS</h3>
			<p>When a customer places a new order for a product or a service as many, as five or six internal and external services may be required, but the customer experiences a single transaction. The base of our custom development relies on the latest service apps that efficiently allow departments and companies to easily exchange information to facilitate seamless transactions for customers.</p>
			</div>
			<div class="emcre-cnt">
			<h3>DASHBOARDS THAT GET IT DONE</h3>
			<p>We leverage tools that will help you boost your revenue. Best-of-breed applications like CRMSugar, Salesforce and CodeIgniter come equipped with dashboards that consolidate important in-house and customer information in one-place. Your employees can manage their workflow more efficiently and interact more effectively with customers, improving business and transactional process-and your bottom line.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="place-holder">
			<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="https://player.vimeo.com/video/109983975" class="vt_play">
			<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
        <img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/3c.jpg" alt="video" class="img-responsive">
				</a>
			</div>
			     <h2>Related Blog</h2>
                      <?php
                    $args=array(
                      'tag' => 'web-development',
                      'showposts'=>2,
                      'orderby' => 'date',
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                    
                      while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="place-holder">
			<p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?>
			<span><a href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-long-arrow-right"></i></a></span>
			</p>
			</div>
                       <?php
                      endwhile;
                    } //if ($my_query)
                  wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
		
		
		</div>
	</div>


</section>
<section class="video-testmonilas">
	<div class="container">
		<div class="row">
<!--			<div class="col-md-3 col-sm-3 col-xs-12">
				<p><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/ecmrc_video.jpg"></p>
			</div>-->
			<div class="col-md-9 col-sm-9 col-xs-12">
                            <?php echo do_shortcode('[jumptoservicesvt]');?>
                <!--	<span class="cma-left"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-left.png"></span>
				<p>his section of text is up to you. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
				<span>- First Name Last Name, President of Battlefield Collection</span>
				</p>
				<span class="cma-right"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-right.png"></span>-->
			</div>
		</div>
	</div>
</section>

<section class="ecmrce-accordians">

	<div class = "container">

  <h3>More Questions about Web Applications</h3>
	<?php echo do_shortcode('[jumptoservicesfaqs]');?>

  
	</div>
</section>


 
<?php get_footer(); ?>
<script>
/* var $jq= jQuery.noConflict();
$jq(window).load(function() {
	$jq('#myCarousel').carousel({
  		interval: 3000
 		})
   	}); */

</script>