<?php
/*
Template Name: about
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url')?>/img/heading_icon_dcw.png"></div>

<!-- Inner page first title and description starts here -->

<section class="light-gray-wraper achievements-block">
<div class="container">
<div class="row text-center divide">
<div class="col-md-12 col-sm-12 col-xs-12">

<!-- Edit Inner page first title and content here -->
<h2>OUR VISION</h2>
<p>With top-notch technical and creative talent, DotcomWeavers embraces the challenges that every project presents. Our team is continuously learning in order to keep pace with the ever-advancing technical landscape. This allows our company to innovate the next-level designs that exceed the expectations of our clients. Along the way, we're proud to have received fine recognition and several awards. But when it comes down to it, a good reputation and a true definition of success, can only be built on a foundation of strong, long-standing partnerships. Therefore, as a DCW client, your goals and your customers will always be our first priority.</p>
<!-- EOF Edit Inner page first title and content here -->

</div>
<!-- Awards, Best desgins, Top Seo Columns starts here -->
<div class="col-sm-4 col-xs-4 col-md-4">
<div class="achieve-box">
<a href="http://www.webaward.org/winner.asp?eid=20742#.U54WTl7o1cM" target="_blank"><img class="img-responsive" src="/wp-content/uploads/webaward.png" alt="" /></a>
<h3>WEBAWARDS</h3>

<hr />

<a href="http://www.webaward.org/winner.asp?eid=20742" target="_blank">Raise Big</a>
Advocacy Standard of Excellence

</div>
</div>
<div class="col-sm-4 col-xs-4 col-md-4">
<div class="achieve-box">

<a class="big" href="http://www.10bestdesign.com/firms/" target="_blank"><img class="img-responsive" src="/wp-content/themes/twentyeleven/images/10bd.png" alt="" /></a>
<h3>10 BEST DESIGNS</h3>

<hr />

<a href="http://www.10bestdesign.com/firms/2014/june/dotcomweavers/" target="_blank">Top 10 Firm</a>
eCommerce, Custom Web Design and Overall Best Firms

</div>
</div>
<div class="col-sm-4 col-xs-4 col-md-4">
<div class="achieve-box">

<a href="https://clutch.co/directory/ecommerce-web-developers" target="_blank"><img class="img-responsive" src="/wp-content/themes/twentyeleven/images/clutchtagline.png" alt="" /></a>
<h3>CLUTCH</h3>

<hr />

<a href="https://clutch.co/directory/ecommerce-web-developers" target="_blank">eCommerce</a>
Top 10 eCommerce and Wordpress Development Company

</div>
</div>
<!-- EOF columns here -->

</div>
</div>
</section><!-- EOF Inner page first title and description here -->
<!-- Parallax Section starts here -->

<section id="slide-1" class="companySlide">
<div class="bcg" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#slide-1">
<div class="hsContainer">
<div class="hsContent" data-center="opacity: 1" data-106-top="opacity: 0.8" data-anchor-target="#slide-1 h2">
<div class="container">
<div class="row">
<div class="col-md-12">

<!-- Edit parallax content starts -->
<h2>OUR COMPANY</h2>
<p class="sub-desc-full">DotcomWeavers is a web development and design company specializing in eCommerce and custom web applications. We're fully committed to our clients and their projects, and our level of commitment is what truly sets us apart. Communication is key; listening to your needs and learning about your passion is critical to us when formulating an effective strategy. We pride ourselves on fanatical attention to detail and on staying totally engaged from initiation to launch, and beyond. We consistently monitor and test each project as it progresses, making sure that we've done our job, and that our solution is helping your business grow to new heights.Like so many of our partners, DotcomWeavers started with the vision of two entrepreneurs with limited resources, big ideas and a firm commitment to excellence.</p>
<p>
 Our story began in 2006, when co-founders Amit Bhaiya and Mayank Agrawal were motivated to offer big city design services at New Jersey prices. Their primary focus has always been on delivering quality performance and real value for our clients and their customers, within a budget that makes business-sense. Within a few short years, they knew they were on the right track; to this day our clients praise us for our fair pricing and clear transparency. It's that small-business mentality that continues to drive our growth and success. 
</p>

<div class="row divide abt-text-algn">
<div class="col-md-6 col-sm-6 text-left">Our story started seven years ago when cofounders Amit Bhaiya and Mayank Agrawal were motivated to offer big city design services at New Jersey prices. We were convinced establishing a New Jersey corporate base in close proximity to New York had advantages that would translate into cost savings, yes, for us, but most importantly for our clients. Three years out, and we know our thinking was right. Our clients consistently tell us our pricing is fair and totally transparent.</div>
<div class="col-md-6 col-sm-6 text-left">Like many of our clients, we started out as entrepreneurs with limited resources and big ideas and belief in the integrity of our services. We know what it's like to be in the trenches. We know your customers demand a great user experience from you, and we're here to make sure you deliver on that expectation.</div>
<!-- EOF Edit parallax content -->

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section><!-- EOF Parallax Section here -->
<section class="light-gray-wraper">
  <div class="container">
    <div class="row divide">
      <div class="col-md-12 text-center hw-wrks">
        <!-- Edit Inner page first title and content here -->
        <h2>HOW WE WORK: BEST PRACTICES</h2>
        <p class="sub-desc-full">Our expert team of designers, developers and project managers leverage best practices in executing and delivering an efficient methodology we call SMART.</p>
        <div class="divide"><img class="img-responsive center-block" src="../../wp-content/themes/twentyeleven/images/smart.png" alt="responsive-iimg" /></div>
        <div class="row text-left f-letter abt-pgs">
          <div class="col-sm-4">
            <h3>Specific</h3>
            <p>Our team will sit down with you to fully understand your objectives, brainstorm together, and prepare a detailed written plan of action, which will serve as a technical roadmap for the project. This scope will clearly identify all of the critical details and resources, while establishing a firm time line for staying on track.</p>
          </div>
          <div class="col-sm-4">
            <h3>Measurable</h3>
            <p>Keeping your project on the right path is just as important to us as it is to you. By segmenting each project into manageable sprints, we establish clear measurements for each milestone along the way, which allows us to identify and immediately address any obstacles that would otherwise cause delays. </p>
          </div>
          <div class="col-sm-4">
            <h3>Agreed Upon</h3>
            <p>We understand that projects like these are an important investment of time and resources. Therefore, once everything is according to expectations, we get right to work! Throughout the project, we maintain an open line of communication, taking steps in accord with the plan's terms and outlined time-frame.</p>
          </div>
          <div class="clearfix"></div>
          <div class="col-sm-4">
            <h3>Realistic</h3>
            <p>When it comes to a project's time and cost, no one likes surprises. For that reason, our entire team is comitted to ensuring a plan that is thorough, and a solution that incorporates the right resources to deliver on schedule and within budget.</p>
          </div>
          <div class="col-sm-4">
            <h3>Time-Framed</h3>
            <p>The scoping exercise establishes a journey map that takes you from ideas to implementation. Therefore, honoring the mutually agreed upong schedule outlined in the plan, is paramount to us. Along each milestone, we're monitoring for progress, and making sure to smooth out speed bumps before they become road blocks, and this keeps each project on track to a succesful launch!</p>
          </div>
          <!-- EOF Edit Inner page first title and content here -->
        </div>
      </div>
    </div>
  </div>
</section>

 
<?php get_footer(); ?>
