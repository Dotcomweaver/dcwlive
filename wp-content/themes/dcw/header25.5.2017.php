<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta property="og:image" content="<?php bloginfo('template_url');?>/img/brand-scroll.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<meta name="viewport" content="width=device-width">
<title><?php wp_title(''); ?></title>
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css">
<!-- Custom Fonts -->
<!-- <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro|Roboto|Open+Sans|Josefin+Slab:400,100,600,300|Josefin+Sans|Raleway|Roboto+Condensed' rel='stylesheet' type='text/css'> Commented on 27-04-2017-->

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
<!--<link href="<?php// bloginfo('template_url'); ?>/css/owl.carousel.css" rel="stylesheet">-->
<link href="<?php bloginfo('template_url'); ?>/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/fontsawesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/style.min.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Poppins:300,400,500,600,700" rel="stylesheet">
<!--<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">-->
<!--[if lt IE 9]>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

</head>

<body>
<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-38465042-2', 'auto'); ga('send', 'pageview'); </script>

<?php if(is_page( 479 ) ){?>

<header>
		
        <!-- main header content start-->
        <div class="container">
          <!-- <h1> 'Tis The Season to Supercharge Your eCommerce Site </h1> -->
          <h1>Supercharge Your Online Business</h1>
        </div>
        <!-- main header content ends -->

        <div class="clearfix"></div>

        <!-- h_services start -->
        <div class="h_services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce">
                  <h4>eCommerce</h4>
                  <p>From shopping cart upgrades to third party integrations, we consider your business requirements and develop solutions for each of them.</p>
                  <a href="/ecommerce" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive">
                  <h4>Responsive</h4>
                  <p>We build high-quality responsive websites that scale to different devices for optimal user experience and higher conversions.</p>
                  <a href="/responsive-web-design" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication">
                  <h4>Web Applications</h4>
                  <p>From concept to completion, we create engaging Web applications that will streamline your business.</p>
                  <a href="/web-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication">
                  <h4>Mobile Apps</h4>
                  <p>Enhance user experience and encourage customer relationships as we take your website to the next level with mobile applications.</p>
                  <a href="/mobile-application-mobile-website-development" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
            </div>
          </div>
        </div>
        <!-- h_services Ends -->
</header>
<div class="clearfix"></div>

      <div class="o_line"></div>

    <div class="clearfix"></div>
<?php } elseif (is_page( 6477 )){ ?>
 
<?php get_header(); ?>
<!-- main header content start-->
<header>
  <div class="container">
    <!-- <h1> 'Tis The Season to Supercharge Your eCommerce Site </h1> -->
    <div class="col-sm-10 col-sm-offset-1">
    <div class="google-adwords">
    <h1> SUPERCHARGE <small> Your Online Business </small> </h1>
    <form class="cnt-frm " method="post" id="register-form" action="https://www.pipelinedeals.com/web_lead">

        <input type="hidden" name="w2lid" value="212f7ab56299" />
        <input type="hidden" name="thank_you_page" value="http://www.dotcomweavers.com/thank-you/" />
        <div class="col-sm-6">
         <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
        </div>
        
        <div class="col-sm-6">
          <input type="text" name="lead[company_name]" class="form-control"  placeholder="Company">
        </div>
        
        <div class="col-sm-6">
          <input type="text" name="lead[phone]" class="form-control" placeholder="Phone*" required>
        </div>

        <div class="col-sm-6">
          <input type="email" name="lead[email]" class="form-control"  placeholder="Email*" required>  
        </div>

        <input type="hidden" name="lead[work_country]" class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >  
        
        <div class="col-sm-12">
          <textarea type="text" name="lead[summary]" class="form-control cmnts" placeholder="How can we help you achieve your eCommerce goals? " /></textarea>
        </div>
        
        <div class="clearfix"></div> 
        <div class="snd">
          <div class="start_btn"><input type="submit" value="Submit" class="btn btn-warning" /></div>
          <div class="call_btn"> <a href="tel:8883156518"><input type="button" value="Call Now: 888.315.6518" class="btn btn-warning" /></a></div>
          <div class="clearfix"></div> 
        </div>

        
      </form>
      </div>
  </div> 
  <div class="clearfix"> </div>
</div>
        <!-- main header content ends -->

        <div class="clearfix"></div>
         <!-- h_services start -->
        <div class="h_services adword-services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce">
                 <a href="/ecommerce"><h4>eCommerce Development</h4></a>
                  </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive">
                   <a href="/responsive-web-design"><h4>Responsive Design</h4></a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication">
                  <a href="/web-applications"><h4>Web Applications</h4></a>
                 </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication">
                  <a href="/mobile-application-mobile-website-development"><h4>Mobile Applications</h4></a>
                 </div> 
              </div>
            </div>
          </div>
        </div>
        <!-- h_services Ends -->
 </header>
 <div class="clearfix"></div>

      <div class="o_line"></div>

    <div class="clearfix"></div>


    <script type="text/javascript">
      if(jQuery('header .google-adwords label').hasClass('error')){
        alert("has Class");
      };
    </script>

 <?php }?>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">

<div class="topcallreq_sec">
  <div class="top_call">
  
    <a href="tel:8883156518" class="top_phone"><!-- <i class="fa fa-phone"> </i> -->CALL NOW</a>
 </div>
 <div class="top_rq">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
<div class="clearfix"> </div>
</div>

<div class="navbar-header col-md-4 col-sm-4 col-xs7">
<button type="button" class="navbar-toggle collapsed bs-1-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1 ">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
<!-- <a class="navbar-brand page-scroll scrl dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php //bloginfo('template_url'); ?>/img/brand-scroll.png"></a> -->
<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-8 col-sm-6 hm-pg-flp">
<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <div class="top_nav">
    <a href="/contact-us/" class="top_phone"><i class="fa fa-phone"> </i>888.315.6518</a>
    <?php wp_nav_menu( array('menu' => 'socialmenu' )); ?>

  
 </div>
 <div class="flipper">
<!-- <div class="front">
<span class="blue-txt "><a href="/contact-us/"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div> -->
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
</div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>
  
</div>
<div class="flipper">
<!-- <div class="front">
<span class="blue-txt "><a href="/contact-us"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div> -->
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
</div>
</div>
<button type="button" class="menu-rt  col-xs-5 " data-toggle="modal" data-target="#menuModal">Menu <span class="fa fa-bars " aria-hidden="true"></span></button>
</nav>
<!-- FULLSCREEN MODAL CODE (.fullscreen) -->

<div class="modal fade fullscreen" id="menuModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content" style="color:#fff;">
<div class="modal-header" style="border:0;">
<button type="button" class="close btn btn-link" data-dismiss="modal" aria-hidden="true" title="close">
<span class="cltext">  Close </span><i class="clsee" style="color:#fff;">x</i></button> 
<h4 class="modal-title text-center"><span class="sr-only">main navigation</span></h4>
</div>
<div class="menu masthead__menu mbl-main-navigation">
<div class="row m-main-menu">
<div class="click-video col-md-6 col-sm-6 col-xs-3 text-left">
<a href="/" title="DCW" class="pp-up-logo" style="padding:0px; border:none;">
<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a></div>
<div class="click-video col-md-4 col-sm-5 col-xs-6 text-right menu-flips hm-pg-flp mdl-view-menu">
<div class="flip-container">
<div class="flipper">
<div class="front">
<span class="blue-txt "><i class="fa fa-phone"></i> 888.315.6518</span>
</div>
<!-- <div class="back">
<span class="blue-txt"><a href="/contact-us">Get Started </a></span>
</div> -->
</div>
</div>
</div>	
</div>
<a title="DCW" href="/" class="menu__title hidden-md hidden-sm hme-cion-mobile"><i class="fa fa-home"></i></a>
<ul class="menu__list">
<hr class="lines2">  

<li class="menu__item">

<a href="/about-us" class="menu__title">Company</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/about-us" class="menu__link" id="Why us-menu">About Us</a></li>
<li><a href="/our-process" class="menu__link" id="Our-Process-menu">Our Process</a></li>
<li><a href="/partners" class="menu__link" id="Partners-menu">Partners</a></li>
<li><a href="/team" class="menu__link" id="Team-menu">Team</a></li>
<li><a href="/contact-us" class="menu__link" id="Contact-us">Contact Us</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/services" class="menu__title">Services</a>
<ul class="menu__submenu" style="display: block;">
<li>
  <a href="/ecommerce" class="menu__link" id="eCommerce-menu">eCommerce</a>
  <span class="accordin_sbmenu fa fa-angle-down"></span>
    <ul class="sub-menu">
      <li id="menu-item-6597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6597"><a href="/b2b-solutions/">B2B Solutions</a></li>
      <li id="menu-item-6596" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6596"><a href="/magento-development/">Magento Development</a></li>
      <li id="menu-item-6595" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6595"><a href="/strategy-consultation/">Strategy & Consultation</a></li>
      <li id="menu-item-6594" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6594"><a href="/ecommerce-seo/">eCommerce SEO</a></li>
      <li id="menu-item-6593" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6593"><a href="/data-management/">Data Management</a></li>
    </ul>
</li>
<li><a href="/web-applications" class="menu__link" id="Web-Application-menu">Web Applications</a></li>
<li><a href="/responsive-web-design" class="menu__link" id="Responsive-Design-menu">Responsive Design</a></li>
<li><a href="/seo-sem" class="menu__link" id="SEO/SEM-menu">SEO/SEM</a></li>
<li><a href="/mobile-application-mobile-website-development" class="menu__link" id="Mobile-Application-menu">Mobile Applications</a></li>
<li><a href="/additional-services/" class="menu__link" id="additional-services-menu">Additional Services</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/work" class="menu__title">Work</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/work" class="menu__link" id="Portfolio-menu">Portfolio</a></li>
<li><a href="/case-studies" class="menu__link" id="Case-Study-menu">Case Studies</a></li>
<li><a href="/journey-map" class="menu__link" id="Journey-Map-menu">Journey Map</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/social-hub" class="menu__title">DCW Social</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/social-hub" class="menu__link" id="Social-Hub-menu">Social Hub</a></li>
<li><a href="/blog" class="menu__link" id="Blog-menu">Blog</a></li>
<li><a href="/tag/press-awards/" class="menu__link" id="PR-menu">Press Releases</a></li>
<li><a href="/video-testimonials" class="menu__link" id="Video-Testimonials-menu">Video Testimonials</a></li>
</ul>
</li>

<li class="menu__item lst-li">
<a href="/faq" class="menu__title">Resources</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/faq" class="menu__link" id="Social-Hub-menu">FAQs</a></li>
<li><a href="/kb" class="menu__link" id="PR-menu">Knowledge Base</a></li>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod == "IN")
{
?>
<li><a href="/careers" class="menu__link" id="Blog-menu">Careers</a></li>
<li><a href="/events" class="menu__link" id="Video-Testimonials-menu">Events</a></li>
<?php }?>
</ul>
</li>

<div class="row video-links-menu clearfix">
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983974" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/menu_1-1-img.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play2 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983975" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/3c.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play3 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983005" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div> 
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/menu_2img.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
</div>	
</ul>

</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.fullscreen -->
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" id="play-button" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>
<style>
@media only screen and (min-width:768px) and (max-width: 1024px){
.pattern-overlay iframe{top:0px !important;}	
header{height:auto;}
 }
@media(max-width:767px) {
.header-content-inner.hm_pg_cnt{top:30%;}header{height:auto;}
}

</style>

<script type="text/javascript">
  var jQ = jQuery.noConflict();
     jQ(document).ready(function(){
      if(jQ(window).width() <= 1024){
        
      jQ(".accordin_sbmenu").click(function(){

        jQ(".sub-menu").toggle("fast");
        jQ(".accordin_sbmenu").toggleClass("fa-angle-down fa-angle-up");

      });

      jQ(document).click(function (e) {
        if (!jQ(e.target).hasClass("accordin_sbmenu") 
         && jQ(e.target).parents(".sub-menu").length === 0) {
          jQ(".sub-menu").hide();
          }
        });
      }   
});

</script>