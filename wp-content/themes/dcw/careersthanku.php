<?php
/*
Template Name: careersthanku
*/

get_header();

?>

<section class="light-gray-wraper thank-you2 mtop40">
    <div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
        <?php 
        remove_filter ('the_content', 'wpautop');
         ?>
    <div class="thank2_content">
        <div class="weaverimg_tnk2">
        <a href="/about-beaver/"><img src="../wp-content/themes/dcw/images/thumbs-up.png" alt="" width="300" height="317" class="aligncenter size-full wp-image-6731" /></a>
        </div>
        <h2>Thank You for Reaching Out!</h2>
        <p>

           We will review your professional information and be in touch within 3 business days.

        </p>
        <p>For more information about DotcomWeavers and how we work, please check out <a href="/our-process/">Our Process</a></p>

    </div>
    <div class="webview_thanku">
            <div class="row pageother_links">
                <div class="col-sm-4">
                <a href="/team/"><img src="../wp-content/themes/dcw/images/id-card.png" class="img-responsive icons"></a>
                <p>The <a href="/why-dotcomweavers/">team</a> at DotcomWeavers</p>
                </div>
                <div class="col-sm-4">
                <a href="/blog/"><img src="../wp-content/themes/dcw/images/list.png" class="img-responsive icons"></a>
                <p>The <a href="/blog/">Beaver Blog</a> with <br> valuable business insights</p>
                </div>
                <div class="col-sm-4">
                <a href="/work/"><img src="../wp-content/themes/dcw/images/responsive.png" class="img-responsive icons"></a>
                <p>Our <a href="/work/">portfolio</a> of exciting projects</p>
                </div>
            </div>
        </div>
        
        <?php //comments_template( '', true ); ?>
        <?php endwhile; // end of the loop. ?>
    </div>	
</section>

<?php get_footer(); ?>



