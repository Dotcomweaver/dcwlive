<?php
/*
Template Name: homepage
*/ 
get_header();?>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<?php if(is_page( array( 6477,7356) )){ ?>

<div class="work_section">
<!-- this is a dummy test I have added for testing -->
   <div class="project_div bg_wht">
         <div class="container">
            <!-- <h4 class="work_hdng"><?php //the_field('work_heading');?></h4> -->
            <h3 class="mid_dcm"><?php the_field('work_content');?></h3>
            <div class="row photo_gallery">
              <div class="owl-carousel_work">
            	<?php
              $portfolio_two = get_field('portfolio_items');
              $port_value = array();
              foreach ($portfolio_two as $portfoli) {
                $port_value[] = $portfoli->ID;
              }
            $folio_ids_arr =  $port_value;
            if (count($folio_ids_arr)) :
	
          	$args = array(
          					'post__in' => $folio_ids_arr,
          					'post_type'=> 'portfolio',
          					'order' => 'DESC',
          					'posts_per_page' => -1
          				);
            query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
               <div class="item <?php echo rtrim($posttags," "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4> <a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h4>
                       
						<p>
							<?php

						// Get a list of terms for this post's custom taxonomy.
						$project_cats = get_the_terms($post->ID, 'keyfeature');
						// Renumber array.
						$project_cats = array_values($project_cats);
						//$x = $project_cats;
							//$x = preg_replace('/\s*,\s*/', ',', $x);

							//echo $x;
						for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
						  <?php  echo $project_cats[$cat_count]->name;if ($cat_count<count($project_cats)-1){echo",";}
						}
						?></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
          </div>
          </div>  
         </div>
      </div>
<!-- Work block section close here -->

</div>
 <div class="clearfix"></div>
<!-- service html start here -->
 <div class="service_dcm">
         <div class="services_main">
            <div class="container">
               <div class="service_title">
                 
                  <h3><!-- We Provide Awesome Services  --></h3>
               </div>
            </div>
         </div>
         <div class="row_service">
            <div class="container">
            <div class="row">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 last_service_row">
                  <div class="sub_service sub_emty">
                     <div class="content_service">
                        <!-- <h4><?php //the_field('services_heading',7356);?></h4> -->
                        <p><?php the_field('services_content',7356);?></p>
                     </div>
                     <div class="right_arrow">
                        <a href="javascript:void(0);"><i class="fas fa-long-arrow-alt-right"></i></a>
                     </div>
                  </div>
               </div>
               
               <?php if( have_rows('all_services',7356) ): ?>

				<?php while( have_rows('all_services',7356) ): the_row(); 

					$image = get_sub_field('services_image',7356);
					$content = get_sub_field('services_description',7356);
					$name = get_sub_field('services_name',7356);
					$link = get_sub_field('services_link',7356);

					?>
		       <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                  <div class="sub_service1">
                     <div class="img_service_dv">                  
                        <a href="/<?php echo $link;?>/"> <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></a>
                     </div>
                     <div class="content_service">
                        <h4><a href="/<?php echo $link;?>/"><?php echo $name;?></a></h4>
                        <p><?php echo $content;?></p>
                     </div>
                     <div class="right_arrow">
                        <a href="/<?php echo $link;?>/"><i class="fas fa-long-arrow-alt-right"></i></a>
                     </div>
                  </div>
               </div>
					
				<?php endwhile; ?>

			<?php endif; ?>
			        </div>
            </div>
         </div>
      </div>
<!-- service html end here -->

<!--ecommerce partner testimonial start-->
<?php $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 1, 'p' => '9364', );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<section class="ecommerce-partner" style="background-image: url(<?php the_field('section_background_image');?>)">
  <div class="container">
    <div class="ecmrc-content">
      <div class="col-sm-12">
        <div class="col-sm-6">
          <div class="ecommerce-testi-left-img">
            <figure>
              <img src="<?php the_field('portfolio_image_main');?>">
            </figure>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="ecommerce-testi-left-content">
            <h1 class="ecommerce-testi-title"><?php the_field('few_words'); ?></h1>
            <p class="ecommerce-testi-description">
                <?php echo get_the_content(); ?>
            </p>
            <p class="ecommerce-testi-reviewer-name"><?php the_field('name_of_the_client'); ?>, <?php the_field('client_designation'); ?>, <a href="<?php the_field('url'); ?>"><?php the_title(); ?></a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endwhile; wp_reset_query(); ?>
<!--ecommerce partner testimonial end-->


<!--home page our technology section start-->
<section class="homepage-technolgy">
  <div class="container">
    <div class="sub-container">
      <div class="technology-we-deal">
        <div class="col-sm-12">
          <?php if( have_rows('new_service_section') ): while( have_rows('new_service_section') ): the_row(); ?>
          <div class="col-sm-4">
            <div class="technology-box">
              <div class="technology-img">
                <img src="<?php the_sub_field('section_image'); ?>">
              </div>
              <div class="technology-title">
                <h4><?php the_sub_field('section_title'); ?></h4>
              </div>
              <div class="technology-description">
               <p><?php the_sub_field('section_content'); ?></p>
              </div>
            </div>
          </div>
          <?php endwhile; endif; ?>          
        </div>
      </div>
    </div>
  </div>
</section>
<!--home page our technology section end--> 

<!--ecommerce partner testimonial start-->
 <?php 
  $baseUrl = get_bloginfo('url');
  if($baseUrl == 'https://www.dotcomweavers.com'){
    $testiId = '11141';
  }else{
    $testiId = '11136';
  }
?> 
<?php $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 1, 'p' => $testiId, );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<section class="ecommerce-partner home-second-testimonial" style="background-image: url(<?php the_field('section_background_image');?>)">
  <div class="container">
    <div class="ecmrc-content">
      <div class="col-sm-12">
        <div class="col-sm-6">
          <div class="ecommerce-testi-left-img second-testimonial">
            <figure>
              <img src="<?php the_field('portfolio_image_main');?>">
            </figure>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="ecommerce-testi-left-content">
            <h1 class="ecommerce-testi-title"><?php the_field('few_words'); ?></h1>
            <p class="ecommerce-testi-description"><?php echo get_the_content(); ?></p>
            <p class="ecommerce-testi-reviewer-name"><?php the_field('name_of_the_client'); ?>, <?php the_field('client_designation'); ?>, <a href="<?php the_field('url'); ?>"><?php the_title(); ?></a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endwhile; wp_reset_query(); ?>
<!--ecommerce partner testimonial end-->  

<!-- blog html start here -->
<div class="blog_dcm">
    <div class="container">
        <div class="blog_title">
          <!-- <h3 class="blog"><?php the_field('blog_title',7356);?></h3> -->
           <h2><?php the_field('blog_content',7356);?></h2>
        </div>
        
        
        <div class="owl-carousel_work">
                                      
<?php 

$posts = get_field('posts_items',7356);

if( $posts ): ?>
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
         <div class="item">
                        <div class="home_over_lay">
                            <div class="inner_arr">
                                <a href="<?php the_permalink(); ?>">
                                <?php 
                                    $gettagID = get_the_tags( $post );
                                     // print_r($gettagID);
                                    $trmid = $gettagID[0]->term_id;
                                     // print_r($post);
                                    $color =  get_term_meta( $trmid );
                                     // print_r($color);
                                    $cvalue = $color[color][0];

                                ?>
                                <div class="column_blog" id="homeblog" style="border-top: 3px solid <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>"> 
                                    <?php if( get_field('author_name') ){ ?>                 
                                    <p><?php the_field('author_name'); ?></p>
                                    <?php }else{?>
                                    <p>Dotcomweavers</p>
                                    <?php }?>
                                    <h4><?php the_title(); ?></h4>
                                    <!-- <h4> <?php echo mb_strimwidth(get_the_title(), 0, 40, '...');?> </h4> -->
                                      <!-- <p class="ecommence">
                                        <?php
                                            // Get a list of terms for this post's custom taxonomy.
                                            $project_cats = get_the_terms($post->ID, 'post_tag');
                                           //print_r($project_cats);
                                            // Renumber array.
                                            if(!empty($project_cats)){
                                            $project_cats = array_values($project_cats);
                                            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
                                              <?php  echo $project_cats[$cat_count]->name;?>
                                                  <?php  if ($cat_count<count($project_cats)-1){
                                                    echo ', ';
                                                }
                                            } }
                                        ?>
                                    </p> -->

                                    <div class="overlay_blog" style="background: <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>">
                                      
                                      
                                    </div>  
                                    <a href="<?php echo the_permalink();?>" class="hidden-lg">Read More</a>
                                </div>
                               </a>
                            </div>
                             <div class="right_arrow">
                                    <a href="<?php echo the_permalink(); ?>"><i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                        </div>
                    </div>
                       
    <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

</div>
    </div>
</div>
<!-- blog html end here -->


<!-- about_us_sections section start here-->

    <div class="clearfix"></div>
   <div class="about_us_sections row">
   <div class="containers">
    <div class="col-md-6 col-sm-6 mob_img">
        <?php 
          $image = get_field('about_us_image', 7356);
          if( !empty($image) ): ?>
            <img class="media-object img-responsive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
   <!-- <div class="media-object">


    <div class="iframe_middle" id="yout">
    
    <div id="ytplayer"></div>

  </div>

   <?php /*
   <?php the_field('about_us_video',7356);?>
   */ ?>
     
   </div> -->
    </div>
          <div class="col-md-6 col-sm-6 mob_content">
        <div class="abt_us_cnt">
          <!-- <h2>A PERSONAL APPROACH</h2> -->
          <!-- <h4>The DotcomWeavers Difference</h4> -->
          
          <h4><?php the_field('about_us_title', 7356); ?></h4>
          <p><?php the_field('about_us_content', 7356); ?></p>
          <!-- <a href="#">SEE US IN ACTION</a> -->
           <button type="button" class="btn btn-info btn-lg"><a href="<?php the_field('about_us_button_link', 7356); ?>"><?php the_field('about_us_button_text', 7356); ?></a></button> 
        </div>
    </div>
        </div>
   </div>



<!-- about_us_sections section End here-->

<?php }?>

<?php get_footer(); ?>

