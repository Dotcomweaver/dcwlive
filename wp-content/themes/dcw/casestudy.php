<?php
/*
Template Name: casestudy
*/
get_header();?>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
 
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_case_studies.png"></div>

<section class="light-gray-wraper case-stus ptop60">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                     <?php 
					 while(have_posts()):
					 	the_post();
						the_content();
					 endwhile;
					 ?>
					<?php           
                $cs_query = new WP_Query(
                            array('post_type' => 'casestudy',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        );  
				?>

   <?php //$content = get_the_content();
    while ($cs_query->have_posts()) : $cs_query->the_post();
    $cslink = get_permalink( $id );
    $the_link = get_permalink();
    $title = get_the_title(); 
    $thumb = get_field('thumbnail');
	$the_content =  get_the_content();
   ?>

    <div class="col-sm-6">
           <div class="featured-box">
            <div class="overlay">
                <p class="case-txt"><a href="<?php echo $the_link; ?>"><?php echo wp_trim_words($the_content, 15 ); ?></a></p>
                <div class="hover-overlay clearfix"><a href="<?php echo $the_link; ?>"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div><!-- ./hover-overlay -->
            </div>
             <a href="<?php echo $thumb['url']; ?>"><img src="<?php echo $thumb['url']; ?>" class="img-responsive" alt=""></a>
            <a href="<?php echo $the_link; ?>" class="full-box-link"></a>
           </div>
    </div>
	
	 <?php endwhile; ?>



				</div>
			</div>
	</div>
</section>

<!--.row-->
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>
<style>
header {display: none;}
</style>
