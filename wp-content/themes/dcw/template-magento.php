<?php
/*
Template Name: magento
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_Magento.png"></div>

<!-- Inner page first title and description starts here -->
<section class="light-gray-wraper tmp-mgnto">
<div class="container">
<div class="row divide">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 video-iframe">
<div class="embed-responsive embed-responsive-16by9">
<a class="vt_play2 btn btn-default dropdown-toggle " href="javascript:void();" rel="https://player.vimeo.com/video/109833320" data-target="#vt_modal" data-toggle="modal">	<i class="flaticon-play-button4"></i><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/2c.jpg"></a>
</div>
</div>
<div class="visible-xs clearfix"></div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 magento">
<h2>ABOUT MAGENTO</h2>
<p>eCommerce platform Magento enables retailers of all sizes to deliver consistently rich multichannel experiences across all retail touch points. Platform flexibility means you meet your unique needs with back-end workflows tailored to your business. Performance and scalability buttress your growth. Magento's many tools and features help you attract new customers, and its developer community enables you to continuously bring quality, efficiency and speed to your customers.</p>
</div>
</div>
</div>
</section><section id="slide-1" class="magentoSlide">
<div class="bcg clearfix" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#slide-1">
<div class="hsContainer">
<div class="hsContent" data-center="opacity: 1" data-106-top="opacity: 0" data-anchor-target="#slide-1 h2">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>WHY MAGENTO?</h2>
<p class="sub-desc-full">Because Magento takes care of business! Its array of tools and resources helps you engage consumers with an enjoyable, simple user experience. Migration is easy. Plus, Magento's many rich, flexible options help optimize your marketing, and your upsell and cross-sell capabilities. We've leveraged the Magento platform for many of our eCommerce clients, and they give it rave reviews. No matter what the size of your business, here's why we'd like to add you to our Magento client list:</p>
<div class="row text-left divide why-mgnt">
<div class="col-md-6">
<h3>CONTINUOUS INNOVATION</h3>
Corporate parent, eBay Enterprise, vigorously supports solutions for eCommerce and arms Magento with the resources and experience to optimize your store. Faster search indexing, speedier checkout, payment gateway and financing options are just some examples of Magento's continuous innovation in providing the feature-rich, flexible options that fuel the growth of your online store.
</div>
<div class="col-md-6">
<h3>THIRD PARTY INTEGRATION</h3>
Hundreds of extensions and open architecture increase functionality and integrate with other best-of-breed enterprise systems to tailor back-end workflows and increase the service options you offer your customers. Are you seeking accounting/payroll functionality, an expanded selection of payment gateways, personalization in display ads, shipping and fulfillment solutions, shopping website integration or social and marketing applications? Magento provides it all. The platform's broad usage means most third-party applications having modules which support the Magento shopping cart and streamline checkout.
</div>
<div class="clearfix"></div>
<div class="col-md-6">
<h3>COMMUNITY OF SUPPORT</h3>
Magento boasts a global network of partners and certified developers. This network can help expedite launch and provide the ongoing support and consulting to help you grow into an eCommerce business that's high-performing end-to-end. Whether you need hosting, CRM solution support, or help with digital strategy for B2B or B2C marketing, Magento's partner and developer community can help you produce better results across the digital channel.
</div>
<div class="col-md-6">
<h3>COMPLETELY SCALABLE</h3>
The platform is just the beginning. Go from one to many stores. Modify your objectives. Magento's continuous updates and plug-and-play features shore up your growth goals. Merchants using Magento as their eCommerce platform use extensions to make their online stores expand functionality to offer more services. Plus, Magento's enhanced search capabilities help you promote your online store through automatic update of new products and services.
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="light-gray-wraper jumpto">
<div class="container">
<div class="row">
<div class="col-md-12 text-center">
<h2>MAGENTO CLIENT WORKS</h2>
</div>
</div>
</div>
</section><!-- portfolio starts -->
<?php echo do_shortcode('[jumptoservicesfolio]');?>
<!-- portfolio Ends here -->

 
<?php get_footer(); ?>
