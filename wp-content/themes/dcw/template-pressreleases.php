<?php
/*
Template Name: Press Releases
*/
get_header();?>
<div class="work clearfix">
     <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
    
</div>
<!-- Inner page first title and description starts here -->
<div class="client-icon press-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_press.png"></div>
<section class="light-gray-wraper divider gry-clr ptop60 pressreleases-dcw ">
  <div class="container" id="container-blog">

<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1 ;
$args = array('post_type'=>'post','tag'=>'press-awards','paged'=>$paged,'posts_per_page'=>'9');
query_posts($args);
if(have_posts()): ?>
     <div id="posts" class="row press">
		<?php
            while ( have_posts() ) : the_post(); 
                    get_template_part( 'presscontent', get_post_format() );
            endwhile; // end of the loop. 
         ?>
      </div>
<?php 
if(function_exists('wp_paginate')) {
            wp_paginate();
       } 
?>
<?php endif; ?>
</div>

      <div class="triangleup blog_traingle"><a href="/video-testimonials/">
We Also Make Videos
<span class="sub_subtitle">View Now</span></a></div>
</section>

 
<?php get_footer(); ?>
