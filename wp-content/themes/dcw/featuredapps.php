<?php
/*
Template Name: featuredapps
*/
get_header();?>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
 
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_mobile_appli.png"></div>
<div class="clearfix"></div>
<section class="devide featured_appspage">
<div class="container">
<div class="vline"></div>

<h2 class="subtitle">We Design and Develop <span>Mobile Apps</span></h2>
<p class="aboutxt text-center">Design and development are done, we've put the app through our rigorous testing process, and it's finally ready to introduce to the public! As your app is published, we'll also review the marketing goals and opportunities for your app to help it get picked up and rank well on the App Store and Google play. This post-launch support is invaluable in helping your app take off. We also offer maintainance packages for app updates and additions you want to implement in the future.</p>

<div class="featured_appssec">
	<?php           
		$fa_query = new WP_Query(
            array('post_type' => 'featuredapps',                               
                  'orderby' => 'DESC',
                  'posts_per_page' => '-1',

            )
        );  
		?>
		<?php //$content = get_the_content();
				 	$j = 0;
				    while ($fa_query->have_posts()) : $fa_query->the_post();
					$counter = 1;
				    $falink = get_permalink( $id );
				    $the_link = get_permalink();
				    $title = get_the_title(); 
				    $the_content =  get_the_content();
				    
				 ?>
	<div class="row featured_app">
		<div class="vline"></div>
		<?php echo $title; ?>
		<p class="aboutxt text-center"><?php echo $the_content; ?></p>
		<div id="featured_appcaro<?php echo $j;?>" class="carousel slide" data-ride="carousel" data-interval="false">

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		  	<?php if( have_rows('featured_apps') ): ?>
	<?php while( have_rows('featured_apps') ): the_row(); 
  // vars
     $image = get_sub_field('image');
     $content = get_sub_field('content');
     $icon = get_sub_field('icon');
     $icon_text = get_sub_field('icon_text');

   ?>
		    <div class="item <?php if($counter <= 1){echo " active"; } ?>">
		    	<div class="col-sm-4">
            
		    		<div class="fetapp_img"><img src="<?php echo $image['url']; ?>" class="img-responsive 123" alt="<?php echo $image['alt'] ?>">
		    		</div>
		    	</div>
		    	<div class="col-sm-8">
		    		<?php echo $content; ?>
              <div class="beach-download">
              <?php the_sub_field('google_and_app_store'); ?>
            </div>
		    	</div>
		    	
		    </div>
		    <?php $counter++;
      endwhile; ?>
<?php endif; ?>
<?php  wp_reset_query();?>
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#featured_appcaro<?php echo $j;?>" role="button" data-slide="prev">
		  </a>
		  <a class="right carousel-control" href="#featured_appcaro<?php echo $j;?>" role="button" data-slide="next">
		  </a>

		  <!-- Indicators -->

		  <ol class="carousel-indicators">
		    <li data-target="#featured_appcaro<?php echo $j;?>" data-slide-to="0" class="active">
		    	<img src="/wp-content/themes/dcw/images/Icons/home.png" class="img-responsive">
		    	<span>Overview</span>
		    </li>
		    <li data-target="#featured_appcaro<?php echo $j;?>" data-slide-to="1">
		    	<img src="/wp-content/themes/dcw/images/Icons/list.png" class="img-responsive">
		    	<span>Functionality</span>
		    </li>
		    <li data-target="#featured_appcaro<?php echo $j;?>" data-slide-to="2">
		    	<img src="/wp-content/themes/dcw/images/Icons/search.png" class="img-responsive">
		    	<span>How it works</span>
		    </li>
		    <li data-target="#featured_appcaro<?php echo $j;?>" data-slide-to="3" class="active">
		    	<img src="/wp-content/themes/dcw/images/Icons/windows.png" class="img-responsive">
		    	<span>UX / UI</span>
		    </li>
		    <li data-target="#featured_appcaro<?php echo $j;?>" data-slide-to="4">
		    	<img src="/wp-content/themes/dcw/images/Icons/catg.png" class="img-responsive">
		    	<span>Compatibility</span>
		    </li>
		    
		  </ol>
	
		</div>

	</div>
<?php

$j++;
 $counter++;
      endwhile; ?>
</div>



<div class="row benfit_megentosec">
	<div class="sub_headsec">
		 <h3 class="subhead_title">Our Mobile <span>App Strategy</span></h3>
		 <p class="aboutxt text-center">Designing and developing mobile apps requires creative problem-solving skills and deep technical expertise. Of course, the process should also be fun. We believe that the way an app is built translates directly into the user experience – and you want your users to enjoy themselves!</p>
	 </div>
	 <?php if( have_rows('featured') ): ?>

				<?php while( have_rows('featured') ): the_row(); 
				$icon = get_sub_field('icon');
				$title = get_sub_field('title');
				$content = get_sub_field('content');
				$image = get_sub_field('image');
				?>
<div class="row benfit_mssec">

	<div class="col-sm-6">
					
				<div class="ms_image">
					<img src="<?php echo $image['url']; ?>" class="img-responsive" alt="<?php echo $image['alt'] ?>" />
				</div>	
		
	</div>
	<div class="col-sm-6">
		<img src="<?php echo $icon['url']; ?>" class="icons" alt="<?php echo $icon['alt'] ?>" >
		<h2 class="sub_subtitle orngtxt"><?php echo $title; ?></h2>
		<p class="text-center innovative txt"><?php echo $content; ?></p>
	</div>
	
	
</div>
	<?php endwhile; ?>

		<?php endif; ?>

</div>

</div>
<div class="clearfix"></div>

<div class="vline"></div>
<section class="res-testmonilas">
	<div class="container">
		<div class="row">

			<div class="col-md-10 col-md-offset-1 ">
                            <div id="video-testimonial-generic" class="carousel slide" data-ride="carousel">
            
           <?php $evideos = new WP_Query( array( 'page_id' => 2878, 'showposts' => -1 ));
              
              while ( $evideos->have_posts() ) : $evideos->the_post(); 
            
              ?>
    <?php if( have_rows('ecommerce') ): $i = 0; ?>
  <ol class="carousel-indicators">
    <?php while ( have_rows('ecommerce') ): the_row(); ?>
      <li data-target="#video-testimonial-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0) echo 'active'; ?>"></li>
    <?php $i++; endwhile; ?>  
  </ol>
<?php endif; ?>
<?php endwhile; 
        wp_reset_query();?>
    
        	
            <div class="row ">
              <h3 class="subtitle ecom_title">Mobile Applications <span>Videos</span></h3>
                  <div class="videotesti_container">           
                <div class="carousel-inner testimonial" role="listbox">
              <?php $evideos = new WP_Query( array( 'page_id' => 2878, 'showposts' => -1 ));
              
              while ( $evideos->have_posts() ) : $evideos->the_post(); 
             $counter = 1;

              ?>
                  <?php if( have_rows('ecommerce') ): ?>



  <?php while( have_rows('ecommerce') ): the_row(); 
  // vars
    $video = get_sub_field('video');
    $image = get_sub_field('image');
    $description = get_sub_field('description');
    $title = get_sub_field('title');

    ?>

  <div class="item <?php if($counter <= 1){echo " active"; } ?>">
                            
<div class="col-md-4 col-sm-5 col-xs-12 video-testimonials">
  <div class="video_img">
<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive">
<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="<?php echo $video; ?>" class="vt_play"><span class="flaticon-play-button4"></span></a>
</div></div>

<div class="col-md-8 col-sm-7 col-xs-12 ecomm_videos">
  <h2 class="ecomm_videos_title"><?php echo $title; ?></h2>
<div class="carousel-caption carousel-caption_top">
  <div class="quote-service">

    <?php echo  
                            wp_trim_words( $description, 40 )?> </div>
</div>
</div>
</div>
<?php $counter++;
   

   endwhile; ?>


<?php endif; ?>
<?php endwhile; 
        wp_reset_query();?>
       </div>
          <a class="left carousel-control" href="#video-testimonial-generic" role="button" data-slide="prev">
                <span class="fa fa-angle-left rm_fa_left greytxt service_test" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#video-testimonial-generic" role="button" data-slide="next">
                <span class="fa fa-angle-right rm_fa_right greytxt service_test" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
  </div>
      </div>                
			</div>
		</div>
  </div>
	</div>
</section> 
<div class="clearfix"></div>

<div class="vline"></div>
<section class="blog_block">
	<div class = "container">
    <?php
 /* $arr_blog = get_posts(
  array(
  'numberposts' => -1,
  'post_status' => 'publish',
  'post_type' => 'post',
  )
  );*/
  $services_blog_post_meta = json_decode(get_post_meta(2878,'services_blog_post_meta', true));
  //print_r($services_blog_post_meta);
 $blog_head_text = $services_blog_post_meta->blog_head_text;
  $services_blog = $services_blog_post_meta->services_blog;

  $arr_blog = get_posts(
  array(
  'numberposts' => -1,
  'post_status' => 'publish',
  'post_type' => 'post',
  )
  );
  $svt_counter = 0;?>
<div class="col-md-10 col-md-offset-1">
    <h2 class="subtitle">Related  <span>Blogs</span></h2>
 <?php foreach($arr_blog as  $indv_blog)
  {

  $svt_counter++;
  if(isset($services_blog) && in_array($indv_blog->ID, $services_blog)){
//echo '<pre>';
  //  print_r($indv_blog);
    //exit;
   $post_thumbnail_id = get_post_thumbnail_id( $indv_blog->ID );
 $thumb = wp_get_attachment_image_src( $post_thumbnail_id, 'post'); 
$yrdata= strtotime($indv_blog->post_date); ?>

                <div class="col-md-2 col-sm-3">
                    <div class="innovativetxt">
                    <?php if(!empty($thumb)){ ?>
                      <img src="<?php echo $thumb[0];?>"/>  
                    <?php }else{?>
                      <img src="/wp-content/themes/dcw/images/logo_slogan.jpg" class="img-responsive">
                    <?php }?>      
                    </div>
                     <p class="orngtxt blog_date text-center"><?php echo date('F d, Y', $yrdata); ?></p>
                </div>

                <div class="col-md-10 col-sm-9">
                <a href="<?php echo get_permalink($indv_blog);?>"><h2 class="blog_title orngtxt"><?php echo $indv_blog->post_title;?></h2></a>
                 <p>  <?php $content =  wp_trim_words($indv_blog->post_content, 35); echo $content; ?>
                <b><a href="<?php echo get_permalink($indv_blog);?>">Read More <i class="fa fa-long-arrow-right"></i></a></b></p>
                </div>
               <div class="clearfix"></div>


 <?php  }
  }



  //print_r($arr_blog);

  ?>
     </div>

	</div>
</section>
<div class="clearfix"></div>

<div class="vline"></div>
<section class="ecmrce-accordians">
	<div class = "container">
  <h2 class="subtitle resfaq">More Questions about <span>Mobile Applications</span></h2>
	<?php// echo do_shortcode('[jumptoservicesfaqs]');?>
        <?php
$faq_query = new WP_Query(
        array('post_type' => 'faq',
    'order' => 'ASC',
    'posts_per_page' => '-1',
           'cat'=>226
        )
);

//print_r($faq_query);
$flkey =0;?>
  <div class="panel-group" id="accordion-ecom">
 <?php while ($faq_query->have_posts()) : $faq_query->the_post();
   
    $title = get_the_title(); 
    $content = get_the_content();
    
              //$i = 0;
	
		if($flkey == 0){
			$flclass = '';
			$plus_minus = 'fa-caret-right';
		}else{
			$flclass = '';
			$plus_minus = 'fa-caret-right';
		}
        ?>
    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-ecom" href="#collapse<?php echo $flkey;?>">
                             <i class="fa <?php echo $plus_minus ?>"></i>
                              <span class="ttl"><?php echo $title;?></span>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse<?php echo $flkey;?>"  class="panel-collapse collapse<?php echo $flkey;?>">
                          <div class="panel-body"><?php echo $content?></div>
                        </div>
					</div>
        <?php $flkey++;
  
	?>
 
 <?php  endwhile;
?>
    </div> 
           <div class="ecom_morelessecbtn">
          <a href="javaScript:Void(0);" class="btn btn-default dcworng_btn ecom_moreless">View All</a>
       </div>
	</div>
</section>


	<div class="triangleup blog_traingle">
  		<a href="/contact-us/">
			Get an indepth look.
			<span class="sub_subtitle">Contact <b>Us</b></span>
  		</a>
	</div>

</section>

<!--.row-->
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>

