<?php
/**
 * The template for displaying search forms 
 *
  */
?>

<form method="get" role="search" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
	<input type="search" class="search-field" name="s" placeholder="<?php esc_attr_e( 'Search...');?>" /></label>
	<input type="submit" class="search-submit screen-reader-text" name="submit" value="<?php esc_attr_e( 'Search'); ?>" />
</form>

