<?php
/**
 * The template used to display Tag Archive pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
<!-- blog page main banner starts -->
<div class="blog_banner" style="background : url(<?php bloginfo('template_url'); ?>/images/BlogBanner-blog.jpg) center no-repeat;background-size: cover;">
    <div class="container">
        <?php
            $args = array(
                'posts_per_page' => 1,
                'meta_key' => 'meta-checkbox',
                'orderby' => 'date',
                'order' => 'ASC',
                'meta_value' => 'yes'
            );
            $featured = new WP_Query($args);
   
            if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
                <h4><?php the_author(); ?></h4>
                <h1><?php the_title();?></h1>
                <a href="<?php the_permalink();?>">Read MOre</a>
            <?php endwhile;endif;
        ?>
    </div>
</div>

<section class="divider ptop60 blog_articles">
    <div class="container tmp-blog" id="container-blog">
    <!-- <div class="vline"></div> -->
        <h3 class="blog1_title">blog</h3>
        <h1 class="blog2_title">Practical articles for success online</h1>
        <?php if( $terms = get_tags() ) : ?>
            <form>
                <select onchange="location = this.options[this.selectedIndex].value;">
                    <option>Sort by</option>
                    <option class="view_all" value="<?php bloginfo('url');?>/blog/">VIEW ALL</option>
                    <?php foreach ( $terms as $term ) :?>
                    <option value="<?php bloginfo('url');?>/tag/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></option>
                  <?php endforeach; ?>
                </select>​
            </form>
        <?php endif; ?>
        <br />
        <div class="row">
            <?php 
            if(have_posts()): ?>
                <div class="col-sm-12 blogged_sub">
                    <div id="posts">
                        <?php 
                            $i = 1; 
                            $postCount = 1;
                            $quoteCountval = 1;
                            while ( have_posts() && $i<15 ) : the_post();
                                $category = get_the_category( $post->ID);
                                if($category != 'Home Slider'): ?>
                                    <div class="col-sm-3 col-xs-3">
                                        <?php 
                                            $gettagID = get_the_tags( $post->ID );
                                            $trmid = $gettagID[0]->term_id;
                                            $color =  get_term_meta( $trmid );
                                            $cvalue = $color[color][0];
                                        ?>
                                        <div class="art_cl" style="border-top: 3px solid <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>">
                                            <div class="sub_art">
                                                <a class="main-anchor" href="<?php the_permalink();?>">
                                                    <?php if( get_field('author_name') ){ ?>
                                                        <span><?php the_field('author_name'); ?></span>
                                                    <?php }else{ ?>
                                                        <span>Dotcomweavers</span>
                                                    <?php } ?>
                                                    <h4> <?php echo mb_strimwidth(get_the_title(), 0, 60, '...');?> </h4>
                                                    <p> 
                                                        <?php
                                                            $project_cats = get_the_terms($post->ID, 'post_tag');
                                                            if(!empty($project_cats)){
                                                                $project_cats = array_values($project_cats);
                                                                for($cat_count=0; $cat_count<count($project_cats); $cat_count++) { ?>
                                                                    <?php echo $project_cats[$cat_count]->name;if ($cat_count<count($project_cats)-1){echo',';}
                                                                }
                                                            }
                                                        ?>
                                                    </p>
                                           
                                                    <div class="overlay_blog" style="background: <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>"></div>
                                                </a>

                                            </div>
                                            <a href="<?php the_permalink();?>" class="sub-anchor"><i class="fas fa-long-arrow-alt-right"></i></a>
                                        </div>
                                    </div>
                                    <?php 
                                        $postCountval = $postCount % 6;
                                        if($postCountval == 0){
                                            $quoteCount = $quoteCountval % 10;
                                            $quoteCountval++;
                                            if($quoteCount == 0){$quoteCount = 10;}
                                            $fieldNum = 'quote_text'.$quoteCount; ?>
                                            <div class="col-sm-3 col-xs-3">
                                                <div class="art_quote">
                                                    <h6><?php the_field($fieldNum,518);?></h6>
                                                </div>
                                            </div>

                                        <?php } ?> 
                                        <?php  $postCount++;  ?>
                                        <?php endif; $i++;  endwhile; 
                                    ?>
                    </div>
                </div>
                <?php if(function_exists('wp_paginate')) {
                    wp_paginate();
                    } 
                ?>
            <?php else: ?>
                <div class="col-sm-12">
                    <article id="post-0" class="post no-results not-found">
                        <header class="entry-header">
                            <h1 class="entry-title"><?php _e( 'Nothing Found12', 'twentyeleven' ); ?></h1>
                        </header><!-- .entry-header -->
        
                        <div class="entry-content">
                            <p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
                            <?php get_search_form(); ?>
                        </div><!-- .entry-content -->
                    </article><!-- #post-0 -->
                </div>
          	<?php endif; ?>
        </div>
    </div>
</section>
<!--.row-->        
<?php get_footer(); ?>