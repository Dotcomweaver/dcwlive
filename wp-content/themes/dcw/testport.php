
<!DOCTYPE html>
<?php
get_header();
?>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="http://dcw.local.dotcomweavers.net/wp-content/themes/dcw/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="http://dcw.local.dotcomweavers.net/wp-content/themes/dcw/css/services.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="https://alvarotrigo.com/multiScroll/jquery.multiscroll.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
</head>
<html>
<body>
<div class="portfolio-submenu">
	<div class="container">
		<a href="javascript:;"> <i class="fa fa-angle-left"></i> BACK</a>
		<p class="project">SAMSUNGPARTS.COM</p>
		<ui class="portfolio-list" >
			<li> <a href="javascript:;"> <i class="fa fa-tag" aria-hidden="true"></i>  ERP </a></li>
			<li> <a href="javascript:;"> MAGENTO </a></li>
			<li> <a href="javascript:;"> RE-PLATFORM </a></li>
		</ui>
	</div>
</div>

	<div class="container">
		  <div class="social_prt">
	   <ul>
	      <li>share</li>
	      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
	      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
	      <li><a href="#"><i class="fab fa-instagram"></i></a></li>                  
	   </ul> 
	</div>
	<ul id="menu">
	<li data-menuanchor="first" class=""><a href="#first">HIGHLIGHT</a></li>
	<li data-menuanchor="second" class="active"><a href="#second">HIGHLIGHT 2</a></li>
	<li data-menuanchor="third" class=""><a href="#third">HIGHLIGHT 3</a></li>
	<li data-menuanchor="four" class=""><a href="#four">Before & After</a></li>
	<li data-menuanchor="five" class=""><a href="#five">Results</a></li>
	<li data-menuanchor="six" class=""><a href="#six">Work With Us</a></li>
	
</ul>
	
</div>
<div class="clearfix"></div>
<div class="multiscroll_main">
	<div class="container">
	<div id="multiscroll">
    <div class="ms-left">
        <div class="ms-section"><img class="img-responsive" src="http://www.homecrux.com/wp-content/uploads/2014/01/Samsung-Food-Showcase-Refrigerator.jpg"></div>
        <div class="ms-section"><img class="img-responsive" src="http://www.homecrux.com/wp-content/uploads/2014/01/Samsung-Food-Showcase-Refrigerator.jpg"></div>
        <div class="ms-section"><img class="img-responsive" src="https://i.ebayimg.com/00/s/NTMyWDYwMA==/z/AlQAAOSwi7RZJ~V1/$_86.JPG"></div>
        <div class="ms-section"><img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg"></div>
    </div>
    
    <div class="ms-right">
    	
        <div class="ms-section">
        	<h2>HIGHLIGHT</h2>
    	<img class="img-responsive" src="http://www.homecrux.com/wp-content/uploads/2014/01/Samsung-Food-Showcase-Refrigerator.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
        <div class="ms-section">
    	<h2>HIGHLIGHT 2</h2>
        	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
        <div class="ms-section">
        	<h2>HIGHLIGHT 3</h2>
        	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
            <div class="ms-section">
            	<h2>Before & After</h2>
        	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
    </div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://dcw.local.dotcomweavers.net/wp-content/themes/dcw/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/vendors/jquery.easings.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/multiscroll.scrollOverflow.limited.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/jquery.multiscroll.min.js"></script>



<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

	<script type="text/javascript">
		/*$(document).ready(function(){


			 $(window).scroll(function() {
			    entering = true;
			    scroll_loc = $(window).scrollTop();
			    top_loc = $(".spacer").height(); 
			    
			    if(scroll_loc < top_loc) {
			      entering = true;
			    }
			    if(scroll_loc >= top_loc - 300
			      && scroll_loc < top_loc - 100 ) {
			      console.log("scroll correction engaged");
			      $('html, body').animate({
			          scrollTop: $(".spacer.one").height()
			      }, 600, function() {
			        $('html').css("overflow","hidden");
			        $('body').css("overflow","hidden");  
			        
			      });
			      
			    }
			  })
			});*/
			var jq = $.noConflict();
		jq(document).ready(function() {
			
			
			if(jq(window).width()>1024){
	    	jq('#myContainer').multiscroll({
	    		//sectionsColor: ['#5ad0ff', '#000', '#ffdd00'],
	        	anchors: ['first', 'second', 'third', 'four'],
	        	menu: '#menu',
	        	navigation: true,
	        	
	        	normalScrollElements: '.normal, .spacer',
	        	responsiveWidth: 1025,
				css3: true,
	        	scrollOverflow: true,
                scrollOverflowKey: 'YWx2YXJvdHJpZ28uY29tXzBqTWMyTnliMnhzVDNabGNtWnNiM2M9QVU3',
                afterResponsive: function(){
                    console.log("after responsive...");
                },
	        /*	css3: true,
            	paddingTop: '70px',
            	paddingBottom: '70px',*/
	        	navigationTooltips: ['First', 'Second', 'Third'],

	        	afterLoad: function(anchor, index){
	        		if(index==2 || index == 3){
	        			jq('#infoMenu li a').css('color', '#f2f2f2');
	        		}
					if(index==1){
						jq('#infoMenu li a').css('color', '#333');
					}
	        	}
	  	 	});
	}
	

	    });
    </script>


    <script type="text/javascript">
    	
    </script>
</body> 
</script>
<?php
get_footer(); ?>
</html>


