<?php
/*
Template Name:innerportfolio
*/
?>
<style>
.home-header{display:none;}
.navbar-default {
    border:none !important;
}
ul.nav.navbar-nav.navbar-right li .glyphicon.glyphicon-menu-hamburger {
    border: 2px solid #434343 !important;
    border-radius: 50%;
    color: #434343!important;
    margin-left: 15px;
    padding: 11px;
    z-index: 9999;
}
.navbar-default .nav > li > a, .navbar-default .nav > li > a:focus {
    color: inherit !important;
}
</style>
<?php get_header();?>


<div class="portfolio_inner">
	<div class="col-lg-5 col-sm-6 col-xs-12 inner-left-side">
		<h3>Jamali Garden</h3>
		<p>You are a savvy online retailer. Your goal: to meet the needs of your customer. Creating a user experience that makes consumer lives easier is the key to converting them from casual. It is also the key to foster your connection with your customer and result in the success of your online store.</p>
		<p class="more">Services provided:</p>
		<div class="row left_side_icons">
			<div class=" col-xs-4 col-sm-4 col-lg-2">
				<p><a href="#"><span class="port-ecom"></span></a></p>
				<span>eCommerce</span>
			</div>
			<div class="col-xs-4 col-sm-4 col-lg-2">
					<p><a href="#"><span class="port-res"></span></a></p>
					<span>Responsive</span>
			</div>
			<div class="col-xs-4 col-sm-4 col-lg-2">
				<p><a href="#"><span class="port-seo"></span></a></p>
					<span>Seo/sem</span>
			</div>
				<div class="col-lg-5 col-xs-10 col-sm-10  visit_site-but">
					<a class="btn btn-default">Visit Site</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="port-line-brder">
				<hr>
			</div>
				<div class="row port-lft-page">
					<div class="col-lg-6 col-sm-6 col-xs-6"><p><a href="#"><i class="fa fa-angle-left"></i>  Global Sugar Art</a></p></div>
					<div class="col-lg-6 col-sm-6 col-xs-6 text-right"><p><a href="#">Raji K <i class="fa fa-angle-right"></i></a> </p></div>
				</div>
		 </div>
		 
	</div>
	<div class="col-lg-7 col-sm-6 col-xs-12 inner-right-side">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side">
				<img class="img-responsive" src="<?php bloginfo('template_url');?>/img/portfolioinner_pump.png">
			</div>
			
			<div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side mobile-res">
				<img class="img-responsive" src="<?php bloginfo('template_url');?>/img/portfolioinner_pump-mobile.png">
			</div>
			
			<div class="col-lg-12 col-sm-12 col-xs-12 inner-right-side mobile-res">
				<img class="img-responsive" src="<?php bloginfo('template_url');?>/img/portfolioinner-table.png">
			</div>
		</div>
	</div>
</div>
<div class="clearfix">
</div>
<?php get_footer(); ?>
