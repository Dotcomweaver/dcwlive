<?php
/*
Template Name: Web Developement
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_web_development.png"></div>

<div class="container">
<div class="ecmmorce-iner clearfix">
                     
    <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
    <img class="img-responsive" src="/wp-content/themes/dcw/img/web-dev.png">
    </div>
	<div class="col-lg-6 col-sm-6 col-xs-12">
     <h3>ONGOING WEBSITE EXPERIENCE</h3>
     <p>To buy, subscribe and interact. When it comes to attracting customers, your website has a job to do. At first glance, a visually appealing site will attract consumers, but if your site doesn’t work fluidly, you won’t be able to keep them there—much less have them return. We listen to your goals, then combine best-of- breed platforms, open-source development and customized solutions to build a website that does just what you need it to do.</p>

     </div>
</div>
 </div>
<?php echo do_shortcode('[jumptoservicesfolio]');?>

<section id="ecmrce-blg-slider clearfix">
	<div class="container content-headings">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="emcre-cnt">
                            <h3>DASHBOARDS AT A GLANCE</h3>
                            <p>Monitor everything. A comprehensive dashboard helps you gauge your key performance indicators to determine how well you're meeting your operational and strategic goals. Pull data from Salesforce or Google or securely display data from your company's databases using custom widgets or Push API. We can help you connect every detail of your business, and we'll help you do it in realtime.</p>
			</div>
			<div class="emcre-cnt">
			<h3>ROBUST REALTIME FUNCTIONALITY THROUGH CENTRAL INTERFACE</h3>
			<p>We know time is money and that good time management will help you do your job more efficiently. Our websites are developed to accommodate a central interface from leading industry providers CRMSugar, Salesforce and FileMaker to keep you up-to-date with your projects and colleagues and integrate your business apps seamlessly with customer data. Sticky notes are out and replaced with workflow. This is business process management at its best-which means less stress for you and more satisfaction for your customer.</p>
			</div>
			<div class="emcre-cnt">
			<h3>CONTINUOUS MEASUREMENT</h3>
			<p>We leverage various third-party integration tools (eg. Salesforce and QuickBooks) that work behind the scenes to help you gauge the effectiveness of your entire website as well as the effectiveness of its individual pages. Evaluate the popularity of different pages, how long visitors stay on the website or a page and even the path with which they navigate. This iterative ability keeps your website fresh and allows you to continuously improve it.</p>
			</div>
			<div class="emcre-cnt">
			<h3>PAGE PERFORMANCE SPEED</h3>
			<p>The speed of your website can definitely affect your sales. Experts say that almost half of web users expect a site to load in two seconds or less. In fact, they abandon a website that won't load in three seconds and won't return to the site to buy again. Even worse, they spread the bad word to friends. We code carefully and with quality to provide the fast load time you need. We also leverage Google Analytics to help you get on top of page speed, even across different browsers. The result: Our custom development will save you important seconds and prevent sales from going down the drain.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="place-holder">

                 <img src="https://img.youtube.com/vi/G7OULxmpoRA/0.jpg" alt="video" class="img-responsive">
				<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="http://www.youtube.com/embed/G7OULxmpoRA" class="vt_play"><span class="icomoon-icon-play-2"></span></a>
			</div>
			     <h2>Related Blog</h2>
                      <?php
                    $args=array(
                      'tag' => 'web-development',
                      'showposts'=>2,
                      'orderby' => 'date',
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                    
                      while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="place-holder">
			<p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?>
			<span><a href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-long-arrow-right"></i></a></span>
			</p>
			</div>
                       <?php
                      endwhile;
                    } //if ($my_query)
                  wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
		
		
		</div>
	</div>


</section>
<section class="video-testmonilas">
	<div class="container">
		<div class="row">
<!--			<div class="col-md-3 col-sm-3 col-xs-12">
				<p><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/ecmrc_video.jpg"></p>
			</div>-->
			<div class="col-md-9 col-sm-9 col-xs-12">
                            <?php echo do_shortcode('[jumptoservicesvt]');?>
                <!--	<span class="cma-left"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-left.png"></span>
				<p>his section of text is up to you. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
				<span>- First Name Last Name, President of Battlefield Collection</span>
				</p>
				<span class="cma-right"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-right.png"></span>-->
			</div>
		</div>
	</div>
</section>

<section class="ecmrce-accordians">

	<div class = "container">

  <h3>More Questions about Web Applications</h3>
	<?php echo do_shortcode('[jumptoservicesfaqs]');?>

  
	</div>
</section>


 
<?php get_footer(); ?>
<script>
var $jq= jQuery.noConflict();
$jq(document).ready(function(){
	
//ecommerce page accordion	
	$jq('#accordion-ecom').on('shown.bs.collapse', function(){
		alert('hi');
		$jq(this).parent().find(".fa-arrow-right").removeClass("fa-arrow-right").addClass("fa-arrow-down");
	}).on('hidden.bs.collapse', function(){
	
		$jq(this).parent().find(".fa-arrow-down").removeClass("fa-arrow-down").addClass("fa-arrow-right");
	});
	
	
});
</script>