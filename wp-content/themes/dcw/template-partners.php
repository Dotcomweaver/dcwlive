<?php
/*
Template Name: partners
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner partner-head">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url')?>/img/heading_icon_partners.png"></div>
<?php 
if ( have_posts() ) {
    while ( have_posts() ) {
        the_post(); 
        the_content();
    } // end while
} // end if
?>



<?php get_footer(); ?>
<style>
header {display: none;}
</style>