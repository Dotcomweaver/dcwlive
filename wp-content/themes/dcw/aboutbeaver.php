<?php
/*
Template Name: aboutbeaver
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
    <?php// if ( function_exists('yoast_breadcrumb') ) {
								//yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					//} ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url')?>/img/heading_icon_faq.png"></div>

<section class="devide aboutbeaver_page">
<div class="container">
<div class="vline"></div>

	<?php 
if ( have_posts() ) {
    while ( have_posts() ) {
        the_post(); 
        the_content();
    } // end while
} // end if
?>

	

	<div class="styles_beaver">
		<div class="vline"></div>
	<h2 class="subtitle">The Many  <span>Styles of Weaver</span></h2>
	<p class="aboutxt text-center">Check out his different outfits for holidays and events on our social media and in our newsletter.</p>

	<div class="weaver_carousel">
		<div class="owl-carousel owl-theme" id="weaver_carousel">
		<?php if( have_rows('slider') ): ?>

	
	<?php while( have_rows('slider') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$text = get_sub_field('title');
		?>
		<div class="item">
		    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"  class="img-responsive">
		    <p> <?php echo $text; ?></p>
		    </div>
		
	<?php endwhile; ?>

<?php endif; ?>
		

		</div>
	</div>
	<p class="aboutxt text-center">And that’s the Dotcom Beaver story! Our team is here to work hard and make your vision of a successful online business a reality. Weaver is here to keep us on point. In his words, “We build the best DAM web solutions!”</p>

	</div>


</div>

<div class="triangleup blog_traingle"><a href="/journey-map/">
Ready For A Journey?
<span class="sub_subtitle">See Our Map</span>
</a></div>
</section>

 <script>
  var jQ = jQuery.noConflict();
  jQ(document).ready(function() {
  jQ('#weaver_carousel').owlCarousel({
    loop:true,
    margin:40,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
     })
 </script>

<?php get_footer(); ?>
