<?php
/**
 * The Template for displaying all Single casestudy posts.
 */
// calling the header.php
get_header();
 header('Content-Type: text/html; charset=ISO-8859-1');
?>
<!-- <style>
    header{display:none;}
</style> -->

<div class="portfolio_inner casestudy-singpagewraper olddesign">
    <div class="pfi_header">
      <div class="pfi_left">
        <div id="port-carousel-left" data-interval="20000" data-pause="false" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
                                           
              <?php 

                $image = get_field('logo_image');

                if( !empty($image) ): ?>
                             
                <div class="left_img" style="background-image: url('<?php echo $image['url']; ?>');">
                      <div class="slider_content"> <p class="more">INTRODUCTION</p><?php echo the_field('introduction');?></div>
                </div>
                <?php endif; ?>     
            
          </div>
        </div>  
      </div>
      <div class="pfi_right">
        
          <div id="port-carousel-right" data-interval="4000" data-pause="false" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
            <?php 
               $i=0;
            if( have_rows('right_banner') ): ?>
              <?php while( have_rows('right_banner') ): the_row(); 
             // $thumb = wp_get_attachment_image_src(get_sub_field('banner_image'));
              $banner = get_sub_field('banner_image');
              //print_r($banner);
               ?>
            <div class="item <?php if($i==0){echo 'active';}?>">
              <div class="item-img">
                  <img class="img-responsive" alt="" src="<?php echo $banner['url']; ?>">
              </div>
              </div>
             <?php $i++; ?>  
               <?php endwhile; ?>
            <?php endif; ?>
            </div>
                  <?php $active_class1 = "active";?>
                  <ol class="carousel-indicators">
                 
                    <?php  $k=0;
                    //echo count($banner);
                       foreach($banner as $img){?>
                        <li data-target="#port-carousel-right" data-slide-to="<?php echo $k; ?>" class="<?php if($k==0){echo 'active';} ?>"></li>
                        <?php $k++;
                        if($k == $i){
                          break;
                        }
                        }?>
                 
                  </ol>
                       
          </div>                      
                     
      </div>
    </div>
    </div>

   <div class="clearfix"></div>
   <div class="container cssingle_content">
   <?php 
if ( have_posts() ) {
  while ( have_posts() ) {
    the_post(); 
    the_content();
  } // end while
} // end if
?>

   </div>
<div class="clearfix"></div>
   
<div class="dcw_counts">
<div class="container">
<div class="row">

<?php 
 if( have_rows('counter') ): ?>
<?php while( have_rows('counter') ): the_row();
$img = get_sub_field('image');
$digit = get_sub_field('digit');
$description = get_sub_field('description');
$symb = get_sub_field('symbol');
   ?>
<div class="col-md-3 col-sm-3 col-xs-6">
<div class="count">
<h4><!--<i class="fa fa-coffee"></i>--><img src="<?php echo $img['url']; ?>" class="img-responsive"> <span class="numb"> <?php echo  htmlspecialchars($digit);
 ?></span><?php echo $symb; ?></h4>
<h5><?php echo $description; ?></h5>
</div>
</div>
<?php endwhile; ?>
 <?php endif; ?>
</div>
</div>
</div>
    <?php 
  $banner_img = get_field('banner_image');
  $main_img = get_field('main_image');
  $info = get_field('detail_info'); 
  $results = get_field('results');
    ?>

  <div class="cssinglepage_thmbaner" style="background: url(<?php echo $banner_img['url']; ?>) no-repeat center;">
     <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img class="img-responsive csthmimg" src="<?php echo $main_img['url']; ?>">
          
        </div>

        <div class="col-md-6">
          <?php echo $results; ?>
        </div>
        
      </div>
     </div>
   </div>

   <div class="portfolio_inner_footer text-center cssinglepaination">
    <div class="container">
      <div class="col-sm-4">
        <?php previous_custom_post(); ?>
      </div>
      <div class="col-sm-4"><a href="/case-studies/">  BACK TO CASE STUDIES</a></div>
      <div class="col-sm-4">
           <?php next_custom_post(); ?>
    </div>
      </div>
  </div>


<?php
// calling footer.php
get_footer();
?>
