<?php
/*
Template Name: cst
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1>Highlighting solutions that change the way you do business online </h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_dcw.png"></div>
<section class="devide cspage">
<?php 
$title = get_the_title(); 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} // end while
} // end if
?>

<div class="cs-tophead">
	<div class="vline"></div>
	<h2 class="subtitle subt_txttrm">Case <span>Studies</span></h2>
	<p class="aboutxt text-center">Get an inside look at the real success stories and how we got them there.</p>
</div>

<div class="case_studywrapper">
			<?php           
                $cs_query = new WP_Query(
                            array('post_type' => 'casestudy',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        );  
				?>
				<?php //$content = get_the_content();
    while ($cs_query->have_posts()) : $cs_query->the_post();
    $cslink = get_permalink( $id );
    $the_link = get_permalink();
    $title = get_the_title(); 
    //$thumb = get_field('thumbnail');
	$the_content =  get_the_content();
	$banner_img = get_field('banner_image');
	$main_img = get_field('main_image');
	$info = get_field('detail_info'); 
	$what_we_did = get_field('what_we_did'); 
	$intro = get_field('introduction');
	$overviewintro = get_field('overview_introduction');
   ?>
	<div class="case_study">
	 <div class="csban_thmbaner cs_global_sugart" style="background: url(<?php echo $banner_img['url']; ?>) no-repeat center;">
		 <div class="container">
		 	<div class="row">
		 		<div class="col-md-7">
		 			<img class="img-responsive csthmimg" src="<?php echo $main_img['url']; ?>">
		 			
		 		</div>

		 		<div class="col-md-5">
		 			<h2 class="subtitle"><?php echo $title; ?></h2>
		 			<p class="cstxtinfo"><?php echo $overviewintro; ?></p>
		 			<a href="<?php echo  $cslink ;?>" class="vcs_link">View Case Study</a>
		 		</div>
		 		
		 	</div>
		 </div>
	 </div>
		 <div class="container csdetailinfo_contain">
			 	<div class="row">
			 		<div class="col-sm-7">
				 		<?php echo $info; ?>
			 		</div>

			 		<div class="col-sm-5">
			 			<?php echo $what_we_did; ?>
			 		</div>
			 		
			 	</div>
		</div>
	</div>
	 <?php endwhile; ?>

</div>
<!-- <div class="clearfix"></div>
<div class="triangleup"><a href="/before-after/">
See What We Do
<span class="sub_subtitle">View Before & After</span>
</a></div> -->


</section>






<?php get_footer(); ?>
