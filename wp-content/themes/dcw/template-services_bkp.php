<?php
/*
Template Name:services
*/
?>


<?php get_header();?>
<div class="services_page_new">
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_services.png"></div>

<section class="service_view_page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 left-bar services_head">
				<div class="services_capabilities">
					<div class="header-group">
					    <h4>Services &amp; <span> Capabilities </span></h4>
					</div>
					<p>At DotcomWeavers, we create engaging digital experiences. <br /> By leveraging our full suite of skulls and services, we can connect your brand with customers in unique ways, open new business opportunities and foster growth through scalable solutions.</p>

					<div class="clearfix"></div>
					<div class="video-thumb">
	                   <img src="http://dotcomweavers.staging.wpengine.com/wp-content/uploads/2016/11/-10" class="img-responsive">
	                    <a class="video_overlay vt_play2 dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109278606" data-target="#vt_modal" data-toggle="modal">
	                    <div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
	                      <div class="overlay-text">
	                        <h5>Web Builder vs Web Developer</h5>
	                       </div>
	                    </a>
                  	</div>	
				</div>
			</div>
			<div class="col-sm-6 right-bar services_head">
				<div class="services_list">
					<div>
							<h5>
							<img src="<?php bloginfo('template_url');?>/images/new-icons/eCommerce.png" class="img-responsive">
							Ecommerce Development &amp; Strategy</h5>
							<ul>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
							</ul>
						
							<h5>
							<img src="<?php bloginfo('template_url');?>/images/new-icons/WebResponsive.png" class="img-responsive">
							Ecommerce Development &amp; Strategy</h5>
							<ul>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
							</ul>
						
							<h5>
							<img src="<?php bloginfo('template_url');?>/images/new-icons/WebApplication.png" class="img-responsive">
							Ecommerce Development &amp; Strategy</h5>
							<ul>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
							</ul>
						
					</div>
					<div>
						
							<h5>
							<img src="<?php bloginfo('template_url');?>/images/new-icons/MobileApplication.png" class="img-responsive">
							Ecommerce Development &amp; Strategy</h5>
							<ul>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
							</ul>
						
							<h5>
							<img src="<?php bloginfo('template_url');?>/images/new-icons/search.png" class="img-responsive">
							Ecommerce Development &amp; Strategy</h5>
							<ul>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
								<li>Engaging Shopping Experiences</li>
							</ul>
						
					</div>
						
				</div>
			</div>
		</div>
	</div>
</section>
<section id="slide-1" class="sevicesSlide">
<div class="bcg" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#slide-1">
<div class="hsContainer">
<div class="hsContent" data-center="opacity: 0.7" data-106-top="opacity: 1" data-anchor-target="#slide-1 h2">
<div class="container">
<div class="row"><!-- Edit parallax content starts -->
<div class="col-md-12">
<h2>KEY PARTNERSHIPS</h2>
<p class="sub-desc">Our relationships with many premiere technology companies and services enable us to leverage the right combination of resources to develop the optimal website solution for your business.</p>

<div class="row divide">
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/oscomerce-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/magento-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/wordpress-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/shopify-logo.png" alt="" /></div>
</div>
<div class="row divide cols5 text-center">
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/loaded-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/sage-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/monsoon-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/authorize-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/sencha-logo.png" alt="" /></div>
</div>
</div>
<!-- EOF Parallax Content here -->

</div>
</div>
</div>
</div>
</div>
</section>
<section class="light-gray-wraper">
<div class="container">
<div class="row">
<div class="col-md-12 our_aprch">
<h3 class="text-center">Our Approach</h3>
<div class="row">
<div class="col-md-6 our-approach">We promise to listen carefully to your business needs, goals, and expectations. By understanding specifics, from your target customer to specific service requirements, we can create an actionable plan, execute it, and deliver a solution. Open communication and transparency throughout the life of a project are essential aspects of our working process, as are progress monitoring and product testing. We employ best practice methodologies at each stage of the process and pride ourselves on reaching project milestones on time. Above all, we understand that every business is unique, so we endeavor to add value to yours by customizing our solutions to your individual needs.
</div>
<div class="col-md-6 col-sm-12 col-xs-12 thank-you text-center">
<!--<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/109983974" width="300" height="150" allowfullscreen="allowfullscreen"></iframe></div>
<div class="embed-responsive embed-responsive-16by9">
<!--<iframe allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" src="https://player.vimeo.com/video/109983974" class="embed-responsive-item"></iframe>-->
<a data-toggle="modal" data-target="#vt_modal" rel="https://player.vimeo.com/video/109983974" href="javascript:void();" class="vt_play btn btn-default dropdown-toggle">
<i class="flaticon-play-button4"></i>
<img style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/menu_1-1-img.jpg" alt="Ideas to Implementation Webinar" class="img-responsive"></a>
	</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<?php get_footer(); ?>
