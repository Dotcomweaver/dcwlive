<?php
/*
Template Name: Tpl Thank you
*/

get_header();
/*session_start();
if($_SESSION['token']='' || !isset($_SESSION['token']))
{
  $_SESSION['token']='1';*/
$url = "https://api.pipelinedeals.com/api/v3/people.json?api_key=yq4sqwP2rUn10H8sc0Gz";
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url);
$content = curl_exec($ch);
$curl_jason = json_decode($content, true);
$emailTo = $curl_jason['entries'][0]['email'];
$from = "info@dotcomweavers.com";
$message = '<html>
<head>
</head>
<body style="margin:0">';
      $message .= '<table border="0" cellpadding="0" cellspacing="0" width="700" align="center">
    <tbody>
    <tr>
        <td align="center" valign="center" width="100%" style="padding:10px 0 0 0;border-color: rgba(34, 34, 34, .05); background-color: #19272f; border-bottom: 3px solid #e8663f;">            
                   <a href="https://www.dotcomweavers.com/"><img align="center" src="https://www.dotcomweavers.com/wp-content/themes/dcw/images/brand.png"></a>

        <table width="100%">
            <tr>
                <td align="center" height="50"><a href="https://www.dotcomweavers.com/why-dotcomweavers/" style="color:#fff; font-family: sans-serif; font-size: 14px; text-decoration: none;">COMPANY</a></td>
                <td align="center" height="50"><a href="https://www.dotcomweavers.com/services/" style="color:#fff; font-family: sans-serif; font-size: 14px; text-decoration: none;">SERVICES</a></td>
                <td align="center" height="50"><a href="https://www.dotcomweavers.com/work/" style="color:#fff; font-family: sans-serif; font-size: 14px; text-decoration: none;">PORTFOLIO</a></td>
                <td align="center" height="50"><a href="https://www.dotcomweavers.com/industries/" style="color:#fff; font-family: sans-serif; font-size: 14px; text-decoration: none;">INDUSTRIES</a></td>
                <td align="center" height="50" width="180"><a href="https://www.dotcomweavers.com/contact-us/" style="color:#fff; font-family: sans-serif; font-size: 14px; text-decoration: none;background-color: #e8663f; border-radius: 3px; padding: 4px 10px;">REQUEST QUOTE</a></td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="center" width="100%" style="padding:20px 20px 60px 50px;">            
                <a href="https://www.dotcomweavers.com/"><img style="width:250px;padding: 40px 0 10px;" src="https://www.dotcomweavers.com/wp-content/themes/dcw/images/thumbs-up.png"></a>
                    <p style=" font-family: sans-serif;font-size: 20px; line-height: 40px;color: #333333;">
                        <span style="color:#E7663F;font-weight:bold;">Thank you</span> for inquiring about our services!<br>
                         We will get back to you within one business day.
                    </p>
                    <p style="font-family: sans-serif;font-size: 18px; line-height: 32px;color: #333333;">
                        While we review your message, feel free to check out:
                    </p>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top"> 
            <table width="100%">           
                <td align="center" >
                    <a href="https://www.dotcomweavers.com/why-dotcomweavers/"><img style="width: 50px; height: 50px" src="https://www.dotcomweavers.com/wp-content/themes/dcw/images/id-card.png"></a>
                    <p style="font-family: sans-serif;font-size: 14px; height: 48px;">The <a href="https://www.dotcomweavers.com/why-dotcomweavers/" style="color:#E7663F;">team</a> at DotcomWeavers</p>
                </td>
                 <td align="center">
                    <a href="https://www.dotcomweavers.com/blog/"><img style="width: 50px;height: 50px;" src="https://www.dotcomweavers.com/wp-content/themes/dcw/images/list.png"></a>
                    <p style="font-family: sans-serif;font-size: 14px; height: 48px;     line-height: 24px;">The <a href="https://www.dotcomweavers.com/blog/" style="color:#E7663F;">Beaver Blog</a> with <br> valuable business insights</p>
                </td>
                <td align="center">
                    <a href="https://www.dotcomweavers.com/work/"><img style="width: 50px;height: 50px;" src="https://www.dotcomweavers.com/wp-content/themes/dcw/images/responsive.png"></a>
                    <p style="font-family: sans-serif;font-size: 14px; height: 48px;">Our <a href="https://www.dotcomweavers.com/work/" style="color:#E7663F;">portfolio</a> of exciting projects
                    </p>
                </td>
            </table>  
            <br>
            <br>
            <br> 
        </td>
    </tr>
    <tr>
        <td align="center" valign="center" style="font-family: sans-serif; background: #E7663F;color: #fff;">
            <p style="padding: 0px 30px; font-size: 16px;">
               We appreciate your interest in DotcomWeavers and look forward to showing you what sets us apart from the competition!
               </p>            
        </td>
    </tr>
</tbody>
</table>';
     
      $message .= "</body></html>";
$headers = array("From: Dotcomweavers <info@dotcomweavers.com>", "Content-Type: text/html");
 $headers = "From: " . $from . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$subject = "Thank You for contacting DotcomWeavers";
$success = mail($emailTo,$subject,$message,$headers);

$ref = wp_get_referer();
$postid = url_to_postid( $ref );
if($postid = 7447){
   $thankYouPageMail = get_bloginfo('url').'/mail-thank-you/';
}
elseif ($postid = 10546){
   $thankYouPageMail = get_bloginfo('url').'/magento-mail-thankyou/';
}


$thankYouPageMail = 'Location:'.$thankYouPageMail;
 if ($success) {
 header($thankYouPageMail);
 //exit;
}
/* mail($emailTo,$subject,$message,$headers);

 }
else
{
  $_SESSION['token']=''; 
  unset($_SESSION['token']);
  header('Location:https://www.dotcomweavers.com/');
}*/
?>
<section class="newthanku">
    <div class="container">
    <?php while ( have_posts() ) : the_post(); ?>
        <?php 
        remove_filter ('the_content', 'wpautop');
        the_content();//get_template_part( 'content', 'page' ); ?>
        <?php //comments_template( '', true ); ?>
        <?php endwhile; // end of the loop. ?>
    </div>  
</section>
<?php get_footer(); ?>
