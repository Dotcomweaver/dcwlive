<?php
/*
Template Name: aboutnew
*/
get_header(); ?>
<div class="service-sub-menu text-navigation">
  <div class="container">
    <div class="service-menu-list why_submenu">
     
      <p class="text-center">Get to know DotcomWeavers</p>
    </div>
  </div>
</div>



<!-- banner start-->
<div class="banner">
    <div class="container">
      <div class="col-sm-12">
      <div class="col-sm-5 col-xs-12 pull-right">
        <div class="banner-right about_video">
                   <?php the_field('banner_video');?>
        </div>
        </div>
        <div class="col-sm-7 col-xs-12 pull-left">
          <div class="banner-left">
            <h1><?php the_field('banner_heading');?></h1>
            <p><?php the_field('banner_content');?>
      </p>
      </div>
        </div>
      </div>
    </div>     
   </div>
<!-- banner end-->

<!-- highlight main body start here-->
<section class="main-body about">
      <div class="container max-container">
        
      <?php if( have_rows('highlights') ): ?>


  <?php while( have_rows('highlights') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          <div class="col-sm-5 imag">
                               <h3 class="mobile-service-title"><?php echo $Text;?></h3>
                                <h1 class="mobile-service-subtitle"><?php echo $heading;?></h1>   
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>

          <div class="col-sm-6">
            <div class="purpose">
              <h3 class="desktop-service-title"><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1 class="desktop-service-subtitle"><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>
         </div>
            <?php endwhile; ?>

<?php endif; ?>
       <?php wp_reset_postdata(); ?>
       
        </div>
      
  </section>
  
<!--highlight main body end here-->


<!--about us discovery new section start here-->
<div class="clearfix"></div>
<div class="discovery_new">
  <div class="discovery_new_content">
    <div class="container max-container">
      <div class="col-md-5 col-sm-6">
        <!--slider left-->
      <div class="collab_carousel_one">
      <div class="about_slider_left_content mobile_none">
          <h1>Discovery</h1>
          <p>You tell us to your ideas, technical needs and goals. We research your market, customers and competitors. Together, we’ll brainstorm the perfect solution for your business.</p>
      </div>
      
      <div class="about_slider_left_content mobile_none">
          <h1>Definition</h1>
          <p>Here, we align our expertise with your requirements and make plan that documents everything from the scope and costs to your project team and launch timeline.</p>
      </div>
     
      <div class="about_slider_left_content mobile_none">
          <h1>Design</h1>
          <p>This is where we build your website UI and UX. Our team will ensure the layout is simple, scalable, engaging and, of course, approved by you every step of the way.</p>
      </div>
      <div class="about_slider_left_content mobile_none">
          <h1>Development</h1>
          <p>Next, we turn your project into code. We’ll implement all scripting, integrations, and customizations and our team will audit the project to ensure seamless functionality.</p>
      </div>
      <div class="about_slider_left_content mobile_none">
          <h1>Deployment</h1>
          <p>After we test the site and ensure your needs are met, it’s go time! We promise a smooth launch and will be on hand for support and maintenance as you grow.</p>
      </div>
      </div>
      <!--slider left end-->
      </div>

      <div class="col-md-7 col-sm-6">
        <!-- slider right-->
       <div class="collab_carousel_two">
      <div class="about_slider_right_content">
        <div class="slider_card">
        <div class="slider_card_number">
          1
        </div>
        <div class="slider_card_box">
          <div class="img_circle">
                <img src="<?php bloginfo('template_url'); ?>/images/About_us_Discovery.png">
              </div>
           <div class="slider_card_box_text">
                <p>Discovery</p>
              </div>   
        </div>

          
        </div>

        <div class="about_slider_left_content mobile_view">
          <h1>Discovery</h1>
          <p>You tell us to your ideas, technical needs and goals. We research your market, customers and competitors. Together, we’ll brainstorm the perfect solution for your business.</p>
      </div>

      </div>


       <div class="about_slider_right_content">
        <div class="slider_card">
        <div class="slider_card_number">
          2
        </div>
        <div class="slider_card_box">
          <div class="img_circle">
                <img src="<?php bloginfo('template_url'); ?>/images/About_us_Definition.png">
              </div>
           <div class="slider_card_box_text">
                <p>Definition</p>
              </div>   
        </div>
          
        </div>
        <div class="about_slider_left_content mobile_view">
          <h1>Definition</h1>
          <p>Here, we align our expertise with your requirements and make plan that documents everything from the scope and costs to your project team and launch timeline.</p>
      </div>

      </div>
     

       <div class="about_slider_right_content">
        <div class="slider_card">
        <div class="slider_card_number">
          3
        </div>
        <div class="slider_card_box">
          <div class="img_circle">
                <img src="<?php bloginfo('template_url'); ?>/images/Design2.png">
              </div>
           <div class="slider_card_box_text">
                <p>Design</p>
              </div>   
        </div>

          
        </div>
        <div class="about_slider_left_content mobile_view">
         <h1>Design</h1>
          <p>This is where we build your website UI and UX. Our team will ensure the layout is simple, scalable, engaging and, of course, approved by you every step of the way.</p>
      </div>
      </div>

      
      <div class="about_slider_right_content">
        <div class="slider_card">
        <div class="slider_card_number">
          4
        </div>
        <div class="slider_card_box">
          <div class="img_circle">
                <img src="<?php bloginfo('template_url'); ?>/images/Devlopment_Icon.png">
              </div>
           <div class="slider_card_box_text">
                <p>Development</p>
              </div>   
        </div>    
        </div>
        <div class="about_slider_left_content mobile_view">
          <h1>Development</h1>
          <p>Next, we turn your project into code. We’ll implement all scripting, integrations, and customizations and our team will audit the project to ensure seamless functionality.</p>
      </div>
      </div>


       <div class="about_slider_right_content">
        <div class="slider_card">
        <div class="slider_card_number">
          5
        </div>
        <div class="slider_card_box">
          <div class="img_circle">
                <img src="<?php bloginfo('template_url'); ?>/images/Deployment_Icon.png">
              </div>
           <div class="slider_card_box_text">
                <p>Deployment</p>
              </div>   
        </div>

          
        </div>
        <div class="about_slider_left_content mobile_view">
          <h1>Deployment</h1>
          <p>After we test the site and ensure your needs are met, it’s go time! We promise a smooth launch and will be on hand for support and maintenance as you grow.</p>
      </div>
      </div>
      </div>   


        <!-- slider right end-->

      </div>
    </div>
  </div>
</div>

<!--about us discovery new section end here-->

<!---Team block starts here -->
<div class="team_member">
 <section class="team_blogs mtop60">
<div class="container">
 <h4>OUR TEAM</h4>
 <h1>We’re proud of our people and the dedication they give every day</h1>   
     <?php           
                $team_query = new WP_Query(
                            array('post_type' => 'team',                               
                                  /*'order' => 'ASC',*/
                                  'posts_per_page' => '-1',
                                  
                            )
                        );  
        ?>
     
    <div class="row">
            
    <div class="images-row">
                     <?php //$content = get_the_content();
    while ($team_query->have_posts()) : $team_query->the_post();
    $teamlink = get_permalink( $id );
    $title = get_the_title(); 
    $prof_pic = get_field('professional_pic');
   $funky_pic = get_field('funky_pic');
    $designation = get_field('designation');
    $about = get_field('about');?>
    <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="thumbnail">
      <?php if( !empty($prof_pic) ): ?>
                            <img src="<?php echo $prof_pic['url']; ?>" class="img-responsive profile-box" rel="<?php echo $funky_pic['url']; ?>" />
                            <img src="<?php echo $funky_pic['url']; ?>" class="img-responsive profile-box-mobiletab" rel="<?php echo $funky_pic['url']; ?>" />                            
                        <?php endif; ?>
          <div class="caption">
          <h4><span class="team_name" ><?php echo the_title();?></span>,<br/>  <span> <?php echo $designation; ?></span></h4>
          <!-- <p><?php echo $about; ?></p> -->

        </div>
      </div>  
                     
    </div>
                    <?php endwhile; ?>
                     <?php wp_reset_postdata(); ?>
      
    </div>
           <!--  <div class="profile clearfix mbtm20">
      <div class="profile-image col-md-4  col-sm-6 col-xs-12"></div>
      <div class="profile-dis col-md-8 col-sm-6"></div>
    </div>
  -->
    
  </div>
     
  </div>
</section> 

</div>

<!--- Team Block ends here -->

<!-- about us culture-->
 <section class="main-body about-culture">
      <div class="container max-container">
    <div class="col-sm-12" id="position">
          <div class="col-sm-5 imag">                       
                         <div class="collab_carousel">
                          <?php if( have_rows('highlights_loop2') ): ?>

              <?php while( have_rows('highlights_loop2') ): the_row(); 

                // vars
                $image = get_sub_field('image');
                
                ?>

                <div class="img_dv">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

                </div>    
                  
                <?php endwhile; ?>
            <?php endif; ?>
                
                </div>
          </div>

          <div class="col-sm-6">
            <div class="purpose">
              <h3><?php the_field('highlights_loop2_text');?></h3>
              <div class="purpose-content">
                <h1><?php the_field('highlights_loop2_heading');?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                    <?php 
            $image = get_field('highlights_loop2_logo');

            if( !empty($image) ): ?>

              <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

            <?php endif; ?>
                  </div>
                 <p><?php the_field('highlights_loop2_content');?></p>
                </div>
                
              </div>
            </div>
          </div>
         </div>

     </div>
   </section> 
<!-- about us culture end-->

<!-- about us core start-->
<div class="core_value">
  <div class="container">
    <div class="core_value_content">
      <h3><?php the_field('core_value_text');?></h3>
      <h1><?php the_field('core_value_content');?></h1>

      <div class="core_value_img">
        <?php 
        $image = get_field('core_value_image');

        if( !empty($image) ): ?>

          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<!-- about us core end-->


<!-- testimonial section start-->
   <div class="clearfix"></div>
  
  <div class="dcw-testimonial">
     <div class="container">
      <?php if( have_rows('testimonial') ):  while( have_rows('testimonial') ): the_row();
        $logo = get_sub_field('logo');
        $review = get_sub_field('review');
        $author = get_sub_field('author');
        $designation = get_sub_field('designation');
      ?>
       <h5> <img src="<?php bloginfo('template_url'); ?>/images/heart.png">  <span>CLIENTS LOVE US</span></h5>
            <h4> <?php echo $review; ?> </h4>
              <div class="project-review">
         <!-- <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" /> -->
          <div class="project-reviewer">
             <p><?php echo $author; ?></p>

          <!-- <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked "></span>
<span class="fa fa-star checked "></span> -->
          </div>         
       </div>
          <?php endwhile; endif; ?>
    </div>
  </div>


   <!-- testimonial section end-->

   <!--work together section-->
  <div class="work-together clearfix">
    <div class="sub-container">
      <div class="col-sm-12">
        <div class="col-sm-4">
            <?php 
          $image = get_field('call_to_action',503);

          if( !empty($image) ): ?>

            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

          <?php endif; ?>
        </div>

        <div class="col-sm-8">
          <h1>What’s your next project?</h1>
          <div class="work-together-content">
            <p><?php the_field('work_together');?></p>

            <p class="touch"> <a href="/contact-us/"> Get In Touch! </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--work together section end-->

<script src="<?php bloginfo('template_url');?>/js/slick.min.js"></script>

<?php get_footer(); ?>