
<!DOCTYPE html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="https://alvarotrigo.com/multiScroll/jquery.multiscroll.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
</head>
<html>
<body>
	<style type="text/css">
	header ul{list-style: none; height: 50px; background: #000}
		header ul li{float: left;padding: 0 15px;}
		header ul li a{color: #fff; font-size: 14px;line-height: 50px;text-decoration: none}
		.multiscroll_main {margin: 20px 0; height: 100%; position: relative; overflow: hidden; }
		.multiscroll_main #multiscroll{max-width: 1336px; margin:0px auto; position: relative;}
        .portf_header{margin: 5px 0px; }
/*---Social Icons-------*/
.social_prt ul{display: inline-block; margin-bottom: 0px; float: left; padding: 4px 0px; }
.social_prt ul li{display: inline-block; list-style: none; margin-right: 11px;font-family: 'Work Sans', sans-serif;
font-size: 18px; color: #4E4E4E; text-transform: uppercase } 
.social_prt ul li a{text-decoration: none !important; color: #fff !important; height: 28px; width: 28px; border: 1px solid #EBEBEB; background-color: #EBEBEB; display: block; text-align: center; border-radius: 100%; line-height: 28px; }
/*---menu-------*/
#menu{margin: 20px auto;float: right; text-align: center; list-style: none}
#menu li{font-family: 'Work Sans', sans-serif;
font-size: 18px; color: #4E4E4E; text-transform: uppercase;float: left;} 
#menu li a{border: none; text-decoration: none !important; color: #4E4E4E; font-family: 'Work Sans', sans-serif; font-size: 18px;padding: 4px 15px;} 
#menu li.active a{color: red;}

/*---Section Inner-------*/
.ms-left img{width: 100%;}
.ms-right img{display: none;}
.projects_pfl_dv{border: 1px solid #EBEBEB; background-color: #30457A; padding: 80px 30px 99px; }
.projects_pfl_dv img{display: none}
.projects_pfl_dv h1{color: #fff; font-family: 'Work Sans', sans-serif; text-transform: capitalize; font-weight: 400; margin-top: 0px; margin-bottom: 40px; font-size: 36px; }
.projects_pfl_dv h1 span{font-weight: 600; }
.projects_pfl_dv p {color: #FFFFFF; font-weight: 300; font-family: 'Work Sans', sans-serif; font-size: 18px; line-height: 34px; margin-bottom: 40px; }
.projects_pfl_dv h5{color: #EBEBEB; font-size: 14px; line-height: 24px; font-family: 'Varela Round', sans-serif; margin-top: 0px; margin-bottom: 40px; } 
.projects_pfl_dv button.btn{color: #FFFFFF; font-family: 'Work Sans', sans-serif; font-size: 14px; height: 40px; width: 173px; background: transparent; border: 2px solid #FFFFFF; border-radius: 0px; text-transform: uppercase; margin-bottom: 40px; }
.projects_pfl_dv button.btn:active, .projects_pfl_dv button.btn:focus{outline: none; box-shadow: none; }
.projects_pfl_dv h6{color: #FFFFFF; font-size: 14px; font-family: 'Work Sans', sans-serif; text-transform: uppercase; margin: 0px; }
@media only screen and (max-width : 1024px){
	html, body{overflow: scroll;}
	.ms-right img{display: block;}
	.ms-left img{display: none;}
	

}

	
	/*-----remove lines----*/
		.portf_header{max-width: 1336px;margin: 0 auto}
		.clearfix{clear: both;height: 0;}
	</style>
	<div class="portf_header clearfix">
	  <div class="social_prt">
	   <ul>
	      <li>share</li>
	      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
	      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
	      <li><a href="#"><i class="fab fa-instagram"></i></a></li>                  
	   </ul> 
	</div>
	<ul id="menu">
	<li data-menuanchor="first" class=""><a href="#first">First section</a></li>
	<li data-menuanchor="second" class="active"><a href="#second">Second section</a></li>
	<li data-menuanchor="third" class=""><a href="#third">Third section</a></li>
	<li data-menuanchor="four" class=""><a href="#four">Before & After</a></li>
	<li data-menuanchor="five" class=""><a href="#five">Results</a></li>
	<li data-menuanchor="six" class=""><a href="#six">Work With Us</a></li>
	
</ul>
</div>
<div class="clearfix"></div>
<div class="multiscroll_main">
	<div id="multiscroll">
    <div class="ms-left">
        <div class="ms-section"><img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg"></div>
        <div class="ms-section"><img class="img-responsive" src="http://www.homecrux.com/wp-content/uploads/2014/01/Samsung-Food-Showcase-Refrigerator.jpg"></div>
        <div class="ms-section"><img class="img-responsive" src="https://i.ebayimg.com/00/s/NTMyWDYwMA==/z/AlQAAOSwi7RZJ~V1/$_86.JPG"></div>
        <div class="ms-section"><img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg"></div>
    </div>
    
    <div class="ms-right">
    	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        <div class="ms-section">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
        <div class="ms-section">
        	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
        <div class="ms-section">
        	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
            <div class="ms-section">
        	<img class="img-responsive" src="https://www.rlrdistribution.co.uk/product_pics/14072-large.jpg">
        	<div class="projects_pfl_dv">
        		
                  <h1>samsung<span>Parts</span></h1>
                  <p>To manage 500,000+ SKUs, SamsungParts required a powerful new eCommerce platform. They also asked for enhanced search features and an ERP that could support high order volumes.</p>
                  <h5>We migrated the existing SamsungParts website from an ASP framework to the powerful Magento 2.0 platform. 
                   To drive the core website functionality, we built a robust Parts Finder. This lets users to search by a variety of parameters, refine their results, and discover related parts. When using the parts finder, visitors can explore 3D interactive parts diagrams.</h5>
                  <button type="button" class="btn">Call To Action</button>
                  <h6>Category: ERP, Magento, Re-Platform</h6>
                </div>
        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/vendors/jquery.easings.min.js"></script>
<script type="text/javascript" src="https://alvarotrigo.com/multiScroll/jquery.multiscroll.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	<script type="text/javascript">
		/*$(document).ready(function(){


			 $(window).scroll(function() {
			    entering = true;
			    scroll_loc = $(window).scrollTop();
			    top_loc = $(".spacer").height(); 
			    
			    if(scroll_loc < top_loc) {
			      entering = true;
			    }
			    if(scroll_loc >= top_loc - 300
			      && scroll_loc < top_loc - 100 ) {
			      console.log("scroll correction engaged");
			      $('html, body').animate({
			          scrollTop: $(".spacer.one").height()
			      }, 600, function() {
			        $('html').css("overflow","hidden");
			        $('body').css("overflow","hidden");  
			        
			      });
			      
			    }
			  })
			});*/
			var jq = $.noConflict();
		jq(document).ready(function() {
			
			
			if(jq(window).width()>=1024){
	    	jq('#myContainer').multiscroll({
	    		//sectionsColor: ['#5ad0ff', '#000', '#ffdd00'],
	        	anchors: ['first', 'second', 'third', 'four'],
	        	menu: '#menu',
	        	navigation: true,
	        	'loopTop': false,
	        	normalScrollElements: '.normal, .spacer',
	        	responsiveWidth: 1024,
                responsiveExpand: true,
                responsiveExpandKey: '',
                afterResponsive: function(){
                    console.log("after responsive...");
                },
	        /*	css3: true,
            	paddingTop: '70px',
            	paddingBottom: '70px',*/
	        	navigationTooltips: ['First', 'Second', 'Third'],

	        	afterLoad: function(anchor, index){
	        		if(index==2 || index == 3){
	        			jq('#infoMenu li a').css('color', '#f2f2f2');
	        		}
					if(index==1){
						jq('#infoMenu li a').css('color', '#333');
					}
	        	}
	  	 	});
	}
	

	    });
    </script>


    <script type="text/javascript">
    	
    </script>
</body> 
</html>


