<?php
/*
Template Name: addservices
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1>CONTENT DEVELOPMENT</h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/heading_icon_dcw.png"></div>
<div class="clearfix"></div>
<section class="devide">
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} // end while
} // end if
?>
</section>


<?php get_footer(); ?>
