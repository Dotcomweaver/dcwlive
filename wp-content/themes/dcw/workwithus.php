<?php
/*
Template Name: workwithus
*/
get_header();?>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
 
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/Careers_hand-shake.png"></div>
<div class="clearfix"></div>
<section class="devide carrers_workwitus">
<div class="container">
<div class="vline"></div>

<h2 class="subtitle">Have Fun <span>At Work</span></h2>
<p class="aboutxt text-center">At DotcomWeavers we are thinkers, creators, dreamers, and doers. We’re also friends. Each person has a unique set of skills, from design and project management to copywriting and development. They those skills with the rest of the team and, together, solve problems. It’s an environment of fun challenges where innovative solutions drive client success. There’s also room to grow. We’ve built a culture of creative freedom, where professional exploration and the willingness to take on new challenges are encouraged and embraced. Ready to join our team? We’d love to talk.</p>

<div class="row carer_carosec">
	<div class="col-sm-6 col-sm-push-6">
		<div class="culture_carousel">
			<div class="owl-carousel owl-theme" id="carrer_culture">
				<?php if( have_rows('culture') ): ?>

				<?php while( have_rows('culture') ): the_row(); 
				$image = get_sub_field('culture_image');
				?>
				<div class="item">
					<img src="<?php echo $image['url']; ?>" class="img-responsive" alt="<?php echo $image['alt'] ?>" />
				</div>

			<?php endwhile; ?>

		<?php endif; ?>
			
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-sm-pull-6">
		<img src="/wp-content/themes/dcw/images/Icons/shirt_iocn.png" class="icons">
		<h2 class="sub_subtitle orngtxt">Culture</h2>
		<p class="text-center innovative txt">We do amazing things at DotcomWeavers, and we do them as a team. In our open, collaborative environment we create best-in- class digital experiences that challenge us and thrill our clients. If we aren’t pushing our limits and learning new skills, we aren’t happy.</p>
	</div>
	
</div>

<div class="row carer_carosec">
	<div class="col-sm-6">
		<div class="carercreativity_carousel">
			<div class="owl-carousel owl-theme" id="carrer_creativity">
				<?php if( have_rows('creativity') ): ?>

				<?php while( have_rows('creativity') ): the_row(); 
				$crimage = get_sub_field('creativity_image');
				?>
				<div class="item">
					<img src="<?php echo $crimage['url']; ?>" class="img-responsive" alt="<?php echo $crimage['alt'] ?>" />
				</div>

			<?php endwhile; ?>

		<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<img src="/wp-content/themes/dcw/images/Icons/graphic-designer-tools.png" class="icons">
		<h2 class="sub_subtitle orngtxt">Creativity</h2>
		<p class="text-center innovative txt">At DotcomWeavers, we’re obsessed with performance. Our designers, developers and project managers constantly ask, “Is this the best way? Will it produce the best results?”. We love industry trends, but if we find a more effective way to do something, we’re going to do it.</p>
	</div>
</div>

<div class="row carer_carosec">
	<div class="col-sm-6 col-sm-push-6">
		<div class="carerlearn_carousel">
			<div class="owl-carousel owl-theme" id="carrer_learning">
				<?php if( have_rows('learning') ): ?>

				<?php while( have_rows('learning') ): the_row(); 
				$lrimage = get_sub_field('learning_image');
				?>
				<div class="item">
					<img src="<?php echo $lrimage['url']; ?>" class="img-responsive" alt="<?php echo $lrimage['alt'] ?>" />
				</div>

			<?php endwhile; ?>

		<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-sm-pull-6">
		<img src="/wp-content/themes/dcw/images/Icons/mortarboard.png" class="icons">
		<h2 class="sub_subtitle orngtxt">Learning</h2>
		<p class="text-center innovative txt">Professional and personal growth are paramount at DotcomWeavers. The more we understand about our work and how we do it, the more value we can offer our clients and see in our own lives. Our team is constantly involved in training programs, professional events, peer to peer discussions, individual study and, especially, learning by doing.</p>
	</div>
	
</div>

<div class="row carer_carosec">
	<div class="col-sm-6">
		<div class="carerfun_carousel">
			<div class="owl-carousel owl-theme" id="carrer_fun">
				<?php if( have_rows('fun') ): ?>

				<?php while( have_rows('fun') ): the_row(); 
				$fnimage = get_sub_field('fun_image');
				?>
				<div class="item">
					<img src="<?php echo $fnimage['url']; ?>" class="img-responsive" alt="<?php echo $fnimage['alt'] ?>" />
				</div>

			<?php endwhile; ?>

		<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<img src="/wp-content/themes/dcw/images/Icons/confetti.png" class="icons">
		<h2 class="sub_subtitle orngtxt">Fun</h2>
		<p class="text-center innovative txt">Fun and productivity go hand in hand at DotcomWeavers. In fact, we’ve found that the more fun we have, the more we get done. We love waking up excited to get to work (even on work from home days), and clients love our energy and enthusiasm.</p>
	</div>
</div>

<div class="career_insidelooksec row">
		<div class="vline"></div>
	<h2 class="subtitle">An <span>Inside Look</span></h2>
	<p class="aboutxt text-center">Check out some of our recent events to get a feel for the way we live and work at DotcomWeavers.</p>

	<div class="carerinsidelook_carousel col-sm-12">
			<div class="owl-carousel owl-theme" id="carrer_insidelook">
				<?php if( have_rows('inside_look') ): ?>

				<?php while( have_rows('inside_look') ): the_row(); 
				$ilimage = get_sub_field('inside_look_image');
				?>
				<div class="item">
					<img src="<?php echo $ilimage['url']; ?>" class="img-responsive" alt="<?php echo $ilimage['alt'] ?>" />
				</div>

			<?php endwhile; ?>

		<?php endif; ?>
			</div>
		</div>
</div>

<div class="wantto_workwitussec">
	<div class="carerform_txt">
		 <h3 class="subhead_title">Want To <span>Work With Us?</span></h3>
		 <img src="/wp-content/themes/dcw/images/thumbs-up.png" class="img-responsive carer_thumsupimg">
		 <p class="aboutxt text-center">Fill out the form below with the following information, and we should be in touch soon!</p>
	 </div>
	 <?php echo do_shortcode('[contact-form-7 id="6976" title="work with us"]');?>
</div>




</div>
	<div class="clearfix"></div>
		<div class="triangleup blog_traingle">
  		<a href="/about-us/">
			Interested in DotcomWeavers?
			<span class="sub_subtitle"><b>Learn About Us</b></span>
  		</a>
	</div>
</section>

<script type="text/javascript">
 var jQ = jQuery.noConflict();
 jQ(document).ready(function() {
	jQ('#carrer_culture').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    autoplayTimeout:3000,
	    autoplay:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	jQ('#carrer_creativity').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    autoplayTimeout:3000,
	    autoplay:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	jQ('#carrer_learning').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    autoplayTimeout:3000,
	    autoplay:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	jQ('#carrer_fun').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    autoplayTimeout:3000,
	    autoplay:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	jQ('#carrer_insidelook').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    autoplayTimeout:3000,
	    autoplay:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:3
	        }
	    }
	});

	
});
</script>
<!-- custom input file script START-->
<script type="text/javascript">
document.getElementById('resum_file').onchange = uploadOnChange;
    
function uploadOnChange() {
    var filename = this.value;
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    document.getElementById('resmlbl_file').innerHTML = filename;
}

</script>
<!-- custom input file script END-->



<!--.row-->
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>

