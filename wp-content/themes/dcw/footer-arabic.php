<?php
/**
* The template for displaying the footer
*
* Contains the closing of the "site-content" div and all content after.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?>
    <footer class="sitefooter">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
         
          <h4>الشركة</h4>
             
             <address>DotcomWeavers هي شركة من أعلى تصنيف وحاصلة على جوائز في تقديم حلول للإنترنت وهي متخصصة في تطوير التجارة الإلكترونية وتصميم مواقع الإنترنت التفاعلية وتطبيقات الإنترنت والهاتف المخصصة واستراتيجية المحتوى وخدمات تحسين محركات البحث وغيرها من الخدمات.
            </address>
                
            <div class="clearfix"></div>
            
            <address><b>
             15 شارع فارفيو
<br/> باراموس, نيوجيرسي
 <br /> رقم الهاتف: 6656 880 201 

            </b></address>
                         
            <div class="clearfix"></div>

            <div class="social-icons">
            <a href="http://www.facebook.com/dotcomweavers" target="_blank" class="s_facebook"><i class="fa fa-facebook"></i></a>
            <a href="http://twitter.com/Dotcomweavers" target="_blank" class="s_twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="s_google-plus"><i class="fa fa-google-plus"></i></a>
            <a href="http://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="s_linkedin"><i class="fa fa-linkedin"></i></a>
           <!-- <a href="https://www.youtube.com/user/dotcomweavers" target="_blank" class="s_youtube"><i class="fa fa-youtube-square"></i></a> -->
            <a href="https://www.instagram.com/dotcomweavers/" target="_blank" class="s_instagram"><i class="fa fa-instagram"></i></a>
            </div>

          </div>

            <div class="col-md-6 col-sm-6">
              <h4>أحدث المقالات</h4>
             
                     <?php
                                $args = array( 'posts_per_page' => 6, 'orderby' => 'ASC', );
                                $lastposts = get_posts( $args );
                                foreach ( $lastposts as $post ) :
                                  setup_postdata( $post ); ?>
                                     <ul class="recent-blogs">
                                        <li>
                                          <a href="<?php the_permalink();?>">
                                            <img src="<?php the_post_thumbnail_url(); ?>" width="70" height="50">
                                            <?php echo wp_trim_words( get_the_title(), 10, '..');?><br />
                                            <strong><?php echo get_the_date(); ?></strong>
                                          </a>
                                        </li>
                                      </ul>
                                                                       
                                  <?php 
                                  endforeach; 
                                wp_reset_postdata(); ?>
          
          </div>
                 <div class="col-md-3 col-sm-6">
              <h4>الأخبار</h4>
                 <div class="newsletter arabic">
            <div class="lettercontent">
                 <p>انضم إلى 3.700 شخص يتلقون تحديثات شهرية ونصائح للتجارة الإلكترونية</p>
                    <?php dynamic_sidebar('newsletter');?>
              </div>
            </div>
            <div class="link-inline">
           
               <a href="/sitemap/"> خريطة الموقع </a> | <a href="/privacy-policy/"> سياسة الخصوصية </a>
             </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>

               <p class="reserve-policy">جميع الحقوق محفوظة 2018©</p>
     </footer>
   <?php wp_footer(); ?>


<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/owl.theme.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/canvasdots.animation.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/fontsawesome.min.css" rel="stylesheet" type="text/css"/>

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Poppins:300,400,500,600,700" rel="stylesheet">


<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>

<!-- <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script> -->
<script src="<?php bloginfo('template_url');?>/js/froogaloop2.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/custom.js"></script>
<script src="<?php bloginfo('template_url');?>/js/dotcom.js"></script>

<script src="<?php bloginfo('template_url');?>/js/modernizr.custom.97074.js"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.hoverdir.js"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.fittext.js"></script>
<script src="<?php bloginfo('template_url');?>/js/wow.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/creative.js"></script>



<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.flexisel.js"></script>


<script>
  
var jq = jQuery.noConflict();

jq( document ).ready(function() {
   
    
   jq( ".service_icons" ).hover(function() {
       ///alert('test');
        var replaceval = jq(this).children('img').attr("rel");
        var pfimage = jq(this).children('img').attr("src");
        jq(this).children('img').attr("rel",pfimage);
        jq(this).children('img').attr("src",replaceval);
   });

}); 



jq(window).load(function() {
    
    jq("#flexiselDemo3").flexisel({
        visibleItems: 2,
        itemsToScroll: 1,         
        autoPlay: {
            enable: true,
            interval: 4000,
            pauseOnHover: true
        }        
    });
});

var jq = jQuery.noConflict();
 jq(document).ready(function($){
 
      jq(window).load(function() { 
       jq(".test1").css("display","block");
         jq(".test2").css("display","none");
        })
     
      jq(window).scroll(function() { // check if scroll event happened
         if ((jq(document).scrollTop() > 1300)  &&  (jq(document).scrollTop() < 1700))  {
          jq(".test2").css("display","block"); 
          jq(".test1").css("display","none");
               
        }
     else {
        jq(".test2").css("display","none");
       jq(".test1").css("display","block");
        }
      });
    });
 
</script>


<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function () {
size_li = j("#mylist>li").size();
x=24;
j('#mylist>li:lt('+x+')').show();
j('#loadMore').click(function () {
    j(this).addClass('hidden');
// x= (x+8 <= size_li) ? x+8 : size_li;
x=j('#rowcount').val();
j('#mylist>li:lt('+x+')').show();
});

j('#showLess').click(function () {
x=(x-8<0) ? 8 : x-8;
j('#mylist>li').not(':lt('+x+')').hide();
});
 
});




</script>
 

<script>
var j = jQuery.noConflict();
j(document).ready(function() {
 
});
</script>
<!-- <script src="<?php //bloginfo('template_url');?>/js/canvasdots.custom.js"></script>
<script src="<?php //bloginfo('template_url');?>/js/canvasdots.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3-beta1/jquery.min.js"></script>


<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>


<!-- <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script> -->
<script src="<?php bloginfo('template_url');?>/js/additional-methods.min.js"></script>

<script type="text/javascript">

              jQuery.noConflict();
              jQuery('document').ready(function()
             {  
 jQuery.validator.addMethod(
  
"regex",
function(value, element, regexp) 
{
if (regexp.constructor != RegExp)
regexp = new RegExp(regexp);
else if (regexp.global)
regexp.lastIndex = 0;
return this.optional(element) || regexp.test(value);
},
"Please check your input."
);
 
 
                     jQuery("#request-form,#request-form1").validate({
                         rules: {
                             "lead[full_name]": "required", 
                              "lead[phone]": { 
                                  required:true, 
                                       },
                             "lead[email]": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                                              }, 
                         },
                         messages: {
                             "lead[full_name]": "Please enter your name*", 
                              "lead[phone]": "Please enter a valid Phone number*", 
                              "lead[email]": "Please enter a valid email address*"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     });  
                     jQuery("#register-form").validate({
                         rules: {
                             "lead[full_name]": "required", 
                             "lead[phone]": { 
                                  required:true, 
                                         },
                             "lead[email]": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                             }, 
                         },
                         messages: {
                             "lead[full_name]": "Please enter your name*", 
                             "lead[phone]": "Please enter a valid phone number*", 
                              "lead[email]": "Please enter a valid email address*"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     }); 

         });
</script>
<script type="text/javascript">
  /*jQuery(function(){
    document.getElementById("btn-click-download").click();
  });*/
</script>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod != "IN")
{
?>
<a title="Real Time Web Analytics" href="http://clicky.com/100706936"><img alt="Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100706936); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100706936ns.gif" /></p></noscript>
<?php }?>
<script type="text/javascript">
    adroll_adv_id = "CLTHEUMRDJEQVKSWZOBGQU";
    adroll_pix_id = "W7ZSUWIXBREQRG4DAKUW43";
        (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
 <?php if(ICL_LANGUAGE_CODE=='en'){?>
           <div class="sticky_footermsg">
        Book your <a href="/contact-us/">FREE consultation</a> with us @<strong>Booth </strong>#540 @<strong>IRCE </strong>in Chicago
        </div>
         <?php }else{?>
          <div class="sticky_footermsg">
      احجز<a href="/contact-us/">استشارتك المجانية معنا</a>@<strong>Booth </strong>#540 @<strong>IRCE </strong>في شيكاغو
      </div>
          <?php }?>

<!-- begin olark code -->
<script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
olark.identify('8908-923-10-4123');</script>
<!-- end olark code -->
<script>
jQuery(document).ready(function(){
jQuery('.top_phone').click(function(){
_gaq.push(['_trackEvent','link','click','tel'])
})
});
</script>

<!-- <script src="<?php bloginfo('template_url');?>/js/jquery.progressScroll.min.js"></script> -->
<!-- <script type="text/javascript">
 var jQ = jQuery.noConflict();
  jQ.fn.progressScroll=function(e){var r=jQ.extend({width:100,height:100,borderSize:10,mainBgColor:"#E6F4F7",lightBorderColor:"#A2ECFB",darkBorderColor:"#39B4CC",fontSize:"30px"},e);var t,i,n,o=this,a=this.selector,s="progressScroll-border",d="progressScroll-circle",l="progressScroll-text";this.getHeight=function(){t=window.innerHeight;i=document.body.offsetHeight;n=i-t};this.addEvent=function(){var e=document.createEvent("Event");e.initEvent("scroll",false,false);window.dispatchEvent(e)};this.updateProgress=function(e){var e=Math.round(100*e);var r=e*360/100;if(r<=180){jQ("."+s,a).css("background-image","linear-gradient("+(90+r)+"deg, transparent 50%, #A2ECFB 50%),linear-gradient(90deg, #A2ECFB 50%, transparent 50%)")}else{jQ("."+s,a).css("background-image","linear-gradient("+(r-90)+"deg, transparent 50%, #39B4CC 50%),linear-gradient(90deg, #A2ECFB 50%, transparent 50%)")}jQ("."+l,a).text(e+"%")};this.prepare=function(){jQ(a).addClass("progressScroll");jQ(a).html("<div class='"+s+"'><div class='"+d+"'><span class='"+l+"'></span></div></div>");jQ(".progressScroll").css({width:r.width,height:r.height,position:"fixed",top:"20px",right:"20px"});jQ("."+s,a).css({position:"relative","text-align":"center",width:"100%",height:"100%","border-radius":"50%","background-color":r.darkBorderColor,"background-image":"linear-gradient(91deg, transparent 50%,"+r.lightBorderColor+"50%), linear-gradient(90deg,"+r.lightBorderColor+"50%, transparent 50%"});jQ("."+d,a).css({position:"relative",top:"50%",left:"50%",transform:"translate(-50%, -50%)","text-align":"center",width:r.width-r.borderSize,height:r.height-r.borderSize,"border-radius":"50%","background-color":r.mainBgColor});jQ("."+l,a).css({top:"50%",left:"50%",transform:"translate(-50%, -50%)",position:"absolute","font-size":r.fontSize})};this.init=function(){o.prepare();jQ(window).bind("scroll",function(){var e=window.pageYOffset||document.documentElement.scrollTop,r=Math.max(0,Math.min(1,e/n));o.updateProgress(r)});jQ(window).bind("resize",function(){o.getHeight();o.addEvent()});jQ(window).bind("load",function(){o.getHeight();o.addEvent()})};o.init()};
</script>
<script type="text/javascript">
  jQ(".progressCounter").progressScroll();
</script> -->




<script type="text/javascript">
var jQ = jQuery.noConflict();
  jQ(document).ready(function() {
      var docHeight = jQ(document).height(),
      windowHeight = jQ(window).height(),
      scrollPercent;

var test = docHeight / 4;


      jQ(window).scroll(function() {
        
        var scrollPercent = parseInt(jQ(window).scrollTop() / (docHeight - windowHeight) * 100);
        jQ('.progressCounter').html(scrollPercent + '%');


        });
        });

 jQ(document).scroll(function() {
  var y = jQ(this).scrollTop();
  if (y >= 2000) {
     //alert("hello");
    jQ('.progressCounter').css("display", "block");
  } else {
    jQ('.progressCounter').css("display", "none");
  }
});

 jQ(document).ready(function(){
  jQ('.progressCounter').css("display", "none");
 });


</script> 

<script type="text/javascript">
var jQ = jQuery.noConflict();
  (function(){
    var $w = jQ(window);
  var $circ = jQ('.animated-circle');
  var $progCount = jQ('.progress-count');

  var wh, h, sHeight;

  function setSizes(){
    wh = $w.height();
    h = jQ('body').height();
    sHeight = h - wh;
  }

  setSizes();

  $w.on('scroll', function(){
    var perc = Math.max(0, Math.min(1, $w.scrollTop()/sHeight));
    updateProgress(perc);
  }).on('resize', function(){
    setSizes();
    $w.trigger('scroll');
  });


  function updateProgress(perc){
    var circle_offset = 126 * perc;
    $circ.css({
      "stroke-dashoffset" : 126 - circle_offset
    });
    $progCount.html(Math.round(perc * 100) + "%");

  }

}());

</script>

</body>
</html>
