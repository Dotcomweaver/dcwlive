<?php
/*
Template Name: SEO
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_seo.png"></div>

<div class="container">
<div class="ecmmorce-iner clearfix">
                     
    <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
    <img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/seo.png">
    </div>
	<div class="col-lg-6 col-sm-6 col-xs-12">
     <h3>THE FIRST STEP ON THE ROAD TO CONVERSIONS</h3>
     <p>Done right, organic search will quite naturally propel you to the top of a search result page. The goal: to be among the top three results. High ranking is crucial for driving traffic to your site. And, it’s the first step in a continuum that requires strong internal linking and the integration of social media to keep users on and returning to your website. Our knowledge of SEO and our experience in building eCommerce websites position us to help you get the rankings you want, the customers you’re after and the conversions you need to drive the success of your business.</p>

     </div>
</div>
 </div>
<?php echo do_shortcode('[jumptoservicesfolio]');?>

<section id="ecmrce-blg-slider clearfix">
	<div class="container content-headings">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="emcre-cnt">
			<h3>GOOGLE ALGORITHMS</h3>
			<p>We know Penguin, Panda, and Hummingbird, but we're not trying to beat Google's organic search algorithms. Forget link wheels, content networks and content spinning. Our SEO relies on the best practices that make product pages rank-worthy. We want your brand to earn authority status on search engines and your website to gain search engine and consumer trust.</p>
			</div>
			<div class="emcre-cnt">
			<h3>SEARCH AND SOCIAL: HAND-IN-HAND</h3>
			<p>We integrate SEO into the links on your website. Quality social media is engaging, entertaining and fresh. It creates a "share factor" that fosters SEO endeavors. Consumer product reviews are also an important part of the social media information exchange. More than 50% of buyers are looking to see reviews of products before purchasing. It's easier for review pages to reach the top of search results than it is for content pages that are strictly selling product.</p>
			</div>
			<div class="emcre-cnt">
			<h3>TURN ON TRACKING</h3>
			<p>SEO is not static. You must continuously test and present with two versions of the same website pages to see which attracts more customers. We leverage GoogleAnalytics and bingAds. These give you the tools to test and analyze your site pages to see what needs improvement. We also implement iContact. iContact supports email marketing campaigns. Imbedded dashboards provide actionable information to help you evaluate the success of your email marketing campaigns.</p>
			</div>
			<div class="emcre-cnt">
			<h3>SEO (SEARCH ENGINE OPTIMIZATION) AND SEM (SEARCH ENGING MARKETING): A DUO</h3>
			<p>We encourage clients to use a complementary approach to search, using both paid search (SEM) and organic search (SEO) marketing campaigns. Each campaign relies on different strategies. SEM uses the search engines to advertise your business website to Internet customers. SEM sends more targeted traffic to your website. You pay per click (PPC) for that exposure. SEO supports your website being naturally accessible to a search engine when consumers do a search. To conduct business on the Internet you need to be visible in both organic and advertised links and need both SEO and SEM. DotcomWeavers offers both services.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="place-holder">
			<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/n2AfAcz4Tuo" class="vt_play">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
                         <img src="/wp-content/themes/dcw/img/p-cost.jpg" alt="video" class="img-responsive">
				</a>
			</div>
			     <h2>Related Blog</h2>
                      <?php
                    $args=array(
                      'tag' => 'seo-2',
                      'showposts'=>2,
                      'orderby' => 'date',
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                    
                      while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="place-holder">
			<p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?>
			<span><a href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-long-arrow-right"></i></a></span>
			</p>
			</div>
                       <?php
                      endwhile;
                    } //if ($my_query)
                  wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
		
		
		</div>
	</div>


</section>
<section class="video-testmonilas">
	<div class="container">
		<div class="row">
<!--			<div class="col-md-3 col-sm-3 col-xs-12">
				<p><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/ecmrc_video.jpg"></p>
			</div>-->
			<div class="col-md-9 col-sm-9 col-xs-12">
                            <?php echo do_shortcode('[jumptoservicesvt]');?>
                <!--	<span class="cma-left"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-left.png"></span>
				<p>his section of text is up to you. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
				<span>- First Name Last Name, President of Battlefield Collection</span>
				</p>
				<span class="cma-right"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-right.png"></span>-->
			</div>
		</div>
	</div>
</section>

<section class="ecmrce-accordians">

	<div class = "container">

  <h3>More Questions about SEO / SEM</h3>
	<?php echo do_shortcode('[jumptoservicesfaqs]');?>

  
	</div>
</section>


 
<?php get_footer(); ?>
