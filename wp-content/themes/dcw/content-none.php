<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header('portfoliowork');
?>

	
	<div class="ecommerce clearfix">
			<div class="header-content">
				<div class="header-content-inner">
        			<h1><?php _e( 'Nothing Found', 'twentyfifteen' ); ?></h1>
					<p></p>
				</div>
			</div>
				
			</div>
<div class="client-icon"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/heading_icon_blog.png"></div>
<div class="clearfix"></div>
</br></br></br>
<div class="container">
<div class="tmp-blog" id="container-blog">
	<div class="entry-summary">

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'twentyfifteen' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyfifteen' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyfifteen' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>

	</div><!-- .page-content -->
	</div>
	</div>
	<div class="clearfix"></div>

