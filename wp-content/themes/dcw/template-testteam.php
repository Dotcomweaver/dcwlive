<?php
/*
Template Name: testteam
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/heading_icon_team.png"></div>


<section class="team_page mtop60">
	<div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="2"></li>
  </ol>
 
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
	  <img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_img_1.jpg" class="img-responsive" alt="team">
      <div class="carousel-caption">
   
      </div>
    </div>
    <div class="item">
 <img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_img_2.jpg" class="img-responsive" alt="team">
      <div class="carousel-caption">
  
      </div>
    </div>
    <div class="item">
 <img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_img_1.jpg" class="img-responsive" alt="team">
      <div class="carousel-caption">
        
      </div>
    </div>
  </div>
 
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div> <!-- Carousel -->
</section>

<section class="team_blogs mtop60">
<div class="container">
	<div class="row">
		<div class="images-row">
		<div class="col-md-4 col-sm-4 col-xs-12">
	
			<div class="thumbnail">
			<img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1.jpg" class="img-responsive profile-box" alt="team person" rel="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1-hover.jpg">
				  <div class="caption">
					<h4><span>Alexis Callahan</span>, Social Media Expert</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>

				</div>
			</div>			  
		</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="thumbnail">
					<img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1.jpg" class="img-responsive profile-box" rel="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1-hover.jpg" alt="team person">
				  <div class="caption">
					<h4><span>Alexis Callahan</span>, Social Media Expert</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>
						
				</div>
			</div>			  
		</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="thumbnail">
					<img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1.jpg" class="img-responsive profile-box" rel="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1-hover.jpg" alt="team person">
				  <div class="caption">
					<h4><span>Alexis Callahan</span>, Social Media Expert</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>
						
				</div>
			</div>
		</div>
		</div>
		<div class="profile clearfix mbtm20">
			<div class="profile-image col-md-4  col-sm-6 col-xs-12"></div>
			<div class="profile-dis col-md-8 col-sm-6"></div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="thumbnail">
		<img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1.jpg" class="img-responsive profile-box" rel="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1-hover.jpg" alt="team person">
				  <div class="caption">
					<h4><span>Alexis Callahan</span>, Social Media Expert</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>

				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="thumbnail">
			<img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1.jpg" class="img-responsive profile-box" rel="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1-hover.jpg" alt="team person">
				  <div class="caption">
					<h4><span>Alexis Callahan</span>, Social Media Expert</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>

				</div>
			</div> 
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="thumbnail">
			<img src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1.jpg" class="img-responsive profile-box" rel="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/team_blog_1-hover.jpg" alt="team person">
				  <div class="caption">
					<h4><span>Alexis Callahan</span>, Social Media Expert</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>

				</div>
			</div>
		</div>
	</div>
	</div>
</section>

 
<?php get_footer(); ?>
<style>
header {display: none;}


</style>

<script>
jQuery.noConflict();
jQuery( document ).ready(function() {
    
   jQuery( ".profile-box" ).on( "click", function() {
       //alert('test');
		var replaceval = jQuery(this).attr("rel");
		var pfimage = jQuery(this).attr("src");
		jQuery(this).attr("rel",pfimage);
		jQuery(this).attr("src",replaceval);
   });

});
</script>

