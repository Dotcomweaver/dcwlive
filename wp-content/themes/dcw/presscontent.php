<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<div class="item col-md-4 col-sm-6" id="post-<?php the_ID(); ?>">
<div class="blog-box">
<div class="blog-box-inner">
<?php 
if ( comments_open() && ! post_password_required() ) :
//echo 'First If';
	if ( 'post' == get_post_type() ) :
	//echo 'second if'; ?>
        <div class="row">
            <div class="col-xs-6 col-md-12 col-lg-6"><span class="icomoon-icon-clock "></span><?php the_date('M j, Y'); ?></div>
            <div class="col-xs-6 col-md-12 col-lg-6 text-right"><span class="icomoon-icon-user"></span><?php the_author();?></div>
             <div class="col-xs-12">
             	 	
					<?php 
                    $sposttags = get_the_tags($post_id);
					if($sposttags): ?>
						<span class="icomoon-icon-bookmark-2"></span> 
                    <?php 
						foreach($sposttags as $stags){
                    ?>
                        <span class="label label-default"><?php echo $stags->name; ?></span>
                    <?php } ?>
                    <?php endif; ?>
             </div>
        </div>
        <hr>
<?php endif; 
endif; ?>
	<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php $title =  get_the_title(); $length = strlen($title); if($length > 55){ echo substr($title,0,55).'...'; }else{ echo $title; } ?></a></h2>

<?php
if( ! post_password_required() && ! is_attachment()):
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
if(!empty($feat_image)):
?>
	<div class="press-img"><img src="<?php echo $feat_image; ?>" class="img-responsive" /></div>
<?php
endif;
endif;
?>
<?php if ( is_search() ) { // Only display Excerpts for Search ?>
		
		<div class="info">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
<?php }else if('audio' == get_post_format()){?>
		
		<div class="info">
			<?php the_content(); ?>
        </div>
<?php }else { ?>
		
		<div class="info">
           		
			
            <?php
			global $more;
			$more = 0;
			?>
			<?php //the_content('',FALSE,''); ?>
			<?php   echo wp_trim_words( get_the_content(), 26 );?>
			
			<?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
<?php } ?>
	<a class="blog-but" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">Continue Reading »</a>	
</div>
</div>
</div>
<!-- .entry-meta -->
<!-- #post-<?php the_ID(); ?> -->