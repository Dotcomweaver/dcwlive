<?php
/*
Template Name:services
*/
?>


<?php get_header();?>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_services.png"></div>

<section class="service_view_page ptop60">
	<div class="container">
		<div class="row service-boxes">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href="/ecommerce"><div class="service-box">
			  		<div class="service-box-content-parent">
				  		<div class="service-box-content">
				  			<span class=""><img class="img-responsive" src="/wp-content/themes/dcw/img/marketing-icon-grey.png"></span>
						  	<h2>Ecommerce
						  		<?php// dynamic_sidebar('ecommerce');?>							
						</div>
					</div>
                                    </div></a>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href="/responsive-web-design"><div class="service-box">
			  		<div class="service-box-content-parent">
				  		<div class="service-box-content">
				  			<span class=""><img class="img-responsive" src="/wp-content/themes/dcw/img/responsive-design--grey.png"></span>
						  	<h2>Responsive Design</h2>
						  		<?php// dynamic_sidebar('responsive');?>								
						</div>
					</div>
                                    </div></a>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href="/web-applications"><div class="service-box">
			  		<div class="service-box-content-parent">
				  		<div class="service-box-content">
				  			<span class=""><img class="img-responsive" src="/wp-content/themes/dcw/img/web-application-grey.png"></span>
						  	<h2>Web Applications</h2>
						  	<?php// dynamic_sidebar('webappli');?>									
						</div>
					</div>
				</div></a>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href="/seo-sem"><div class="service-box">
			  		<div class="service-box-content-parent">
				  		<div class="service-box-content">
				  			<span class=""><img class="img-responsive" src="/wp-content/themes/dcw/img/seo-sem-grey.png"></span>
						  	<h2>SEO / SEM</h2>
						  	<?php// dynamic_sidebar('seo');?>							
						</div>
					</div>
				</div></a>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href="/mobile-application-mobile-website-development"><div class="service-box">
			  		<div class="service-box-content-parent">
				  		<div class="service-box-content">
				  			<span class=""><img class="img-responsive" src="/wp-content/themes/dcw/img/mobile-applications-grey.png"></span>
						  	<h2>Mobile Applications</h2>
						  	<?php// dynamic_sidebar('mobileappli');?>										
						</div>
					</div>
				</div></a>
			</div>
		</div>
	</div>
</section>
<section id="slide-1" class="sevicesSlide">
<div class="bcg" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#slide-1">
<div class="hsContainer">
<div class="hsContent" data-center="opacity: 0.7" data-106-top="opacity: 1" data-anchor-target="#slide-1 h2">
<div class="container">
<div class="row"><!-- Edit parallax content starts -->
<div class="col-md-12">
<h2>KEY PARTNERSHIPS</h2>
<p class="sub-desc">Our relationships with many premiere technology companies and services enable us to leverage the right combination of resources to develop the optimal website solution for your business.</p>

<div class="row divide">
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/oscomerce-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/magento-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/wordpress-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/shopify-logo.png" alt="" /></div>
</div>
<div class="row divide cols5 text-center">
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/loaded-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/sage-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/monsoon-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/authorize-logo.png" alt="" /></div>
<div class="col-md-3 col-sm-6"><img class="img-responsive center-block" src="../wp-content/themes/twentyeleven/images/sencha-logo.png" alt="" /></div>
</div>
</div>
<!-- EOF Parallax Content here -->

</div>
</div>
</div>
</div>
</div>
</section>
<section class="light-gray-wraper">
<div class="container">
<div class="row">
<div class="col-md-12 our_aprch">
<h3 class="text-center">Our Approach</h3>
<div class="row">
<div class="col-md-6 our-approach">We promise to listen carefully to your business needs, goals, and expectations. By understanding specifics, from your target customer to specific service requirements, we can create an actionable plan, execute it, and deliver a solution. Open communication and transparency throughout the life of a project are essential aspects of our working process, as are progress monitoring and product testing. We employ best practice methodologies at each stage of the process and pride ourselves on reaching project milestones on time. Above all, we understand that every business is unique, so we endeavor to add value to yours by customizing our solutions to your individual needs.
</div>
<div class="col-md-6 col-sm-12 col-xs-12 thank-you text-center">
<!--<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/109983974" width="300" height="150" allowfullscreen="allowfullscreen"></iframe></div>
<div class="embed-responsive embed-responsive-16by9">
<!--<iframe allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" src="https://player.vimeo.com/video/109983974" class="embed-responsive-item"></iframe>-->
<a data-toggle="modal" data-target="#vt_modal" rel="https://player.vimeo.com/video/109983974" href="javascript:void();" class="vt_play btn btn-default dropdown-toggle">
<i class="flaticon-play-button4"></i>
<img style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/menu_1-1-img.jpg" alt="Ideas to Implementation Webinar" class="img-responsive"></a>
	</div>
</div>
</div>
</div>
</div>
</div>
</section>
<?php get_footer(); ?>
