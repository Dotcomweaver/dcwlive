<?php
/*
Template Name: magento adwords
*/
get_header();
global $wp;  
$current_url = home_url(add_query_arg(array(),$wp->request));
?>
<?php 
if(isset($_POST['subForm'])){
  if(isset($_POST['g-recaptcha-response'])){
      $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
      echo '<h2>Please check the the captcha form.</h2>';
      exit;
    }
    $secretKey = "6LfiSisUAAAAABUp-TV9nq7wyzC_WPiXyMw2QMI2";
    $ip = $_SERVER['REMOTE_ADDR'];
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
    $responseKeys = json_decode($response,true);
    if(intval($responseKeys["success"]) !== 1) {
      echo '<h2>You are spammer ! Get the @$%K out</h2>';
    } else {
    $fullName = $_POST["full_name"];
    $eEmail = $_POST["email"];
    $pPhone = $_POST["phone"];
    $cCompany = $_POST["company"];
    $sSummary = $_POST["summary"];
    $workCountry = $_POST["work_country"];
    $thankYouPage = get_bloginfo('url').'/thank-you/';
    $thankRedirect = 'Location:'.$thankYouPage;

        $ch = curl_init();
    $curlConfig = array(
       CURLOPT_URL            => "https://www.pipelinedeals.com/web_lead",
       CURLOPT_POST           => true,
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_POSTFIELDS     => array(
           'w2lid' => '212f7ab56299',
           'thank_you_page' => $thankYouPage,
           'lead[full_name]' => $fullName,
           'lead[email]' => $eEmail,
           'lead[phone]' => $pPhone,
           'lead[work_country]' => $workCountry,
           'lead[company_name]' => $cCompany,
           'lead[summary]' => $sSummary,
       )
    );
    curl_setopt_array($ch, $curlConfig);
    $result = curl_exec($ch);
    curl_close($ch);
    header($thankRedirect);
    }
}
?>
<!-- service submenu-->
<div class="spl-adv-page">
  <div class="service-sub-menu">
  <div class="container">
    <div class="service-menu-list">
      <ul>
        <li> <a class="active" href="/ecommerce/"> ECOMMERCE </a> </li>
        <li> <a href="/custom-software/"> CUSTOM SOFTWARE</a></li>
        <li> <a href="/mobile-apps/"> MOBILE APPS </a> </li>
        <li> <a class="active" href="/magento-adwords/"> MAGENTO ADWORDS </a> </li>
      </ul>
    </div>
  </div>

</div>



<!-- service sub menu end-->


<!--banner start-->
   <div class="banner sticky_subnav">
    <div class="container">
      <div class="col-sm-12">
      <div class="col-sm-5 col-xs-12 pull-right">
          <div class="banner-right magento_banner_img">
           <?php 
            $image = get_field('image');?>
         <?php  if( !empty($image) ) { ?>

      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

    <?php } else { ?>
    <?php $banvideo = the_field('video');?>

      <?php if($banvideo){
        echo $banvideo; 
      } }?>
      </div>
        
          </div>
        <div class="col-sm-7 col-xs-12 pull-left">
          <div class="banner-left">
            <h1> <?php the_field('ecommerce_heading');?></h1>
            <?php the_field('banner_content');?>
          </div>
        </div>
        
      </div>
    </div>     
   </div>
 
   <!--banner end-->


   <section class="adwords_form">
     <div class="container">
       <div class="col-sm-12">
         <div class="col-sm-6 adwords_main_left">
           <div class="adwords_left">
             <?php the_field('form_left_section'); ?>
           </div>
         </div>
         <div class="col-sm-6 adwords_main_right">
              <div class="contact_left_area clearfix">
        <h3>GET A MAGENTO WEBSITE </h3>
          <form method="post" id="register-form" action="<?php echo $current_url; ?>/" name="myCntForm" method="post">  
            <!-- action="https://www.pipelinedeals.com/web_lead" -->
            <!-- <input type="hidden" name="w2lid" value="212f7ab56299" /> -->
            <!-- <input type="hidden" name="thank_you_page" value="https://dotcomweavers.staging.wpengine.com/thank-you/" /> -->
            <div class="form-one clearfix">
              <div class="col-sm-6 col-xs-12">
                  <div class="styled-input">
                    <input type="text" name="full_name" autocomplete="off" required />
                    <p class="label_place">Name*</p>
                    <span></span>
                  </div>  
              </div>
              <div class="col-sm-6 col-xs-12">
                  <div class="styled-input">
                    <input type="text" name="email" required />
                    <p class="label_place">Email*</p>
                    <span></span>
                  </div>  
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="styled-input">
                  <input type="text" name="phone" required />
                  <p class="label_place">Phone*</p>
                  <span></span>
                </div>  
            </div>

            <div class="col-sm-6 col-xs-12">
                <div class="floating-label input-txt">      
                    <input class="floating-input floating-text" type="text" name="company" placeholder=" ">
                    <span class="highlight"></span>
                    <p class="textarea_label">Company</p>
                </div>
            </div>
            
              <input type="hidden" name="work_country" placeholder="Country" class="form-control" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >
        
              <?php
            $curUrl = get_bloginfo('url');
            if($curUrl == 'https://dotcomweavers.staging.wpengine.com') { ?>
              <div class="col-sm-12 col-xs-12">
                <div class="floating-label message">      
                  <textarea class="floating-input floating-textarea" id="form_summery" name="summary" placeholder=" "></textarea>
                  <span class="highlight"></span>
                  <p class="textarea_label">How can we help you?</p>
                </div>
              </div>
            <?php } else { ?>
              <div class="col-sm-12 col-xs-12">
                <div class="floating-label">      
                  <textarea class="floating-input floating-textarea" name="summary" placeholder=" "></textarea>
                  <span class="highlight"></span>
                  <p class="textarea_label">How can we help you?</p>
                </div>
              </div>


            <?php } ?>

            <div class="form-footer">
              <div class="col-sm-6 col-xs-12 captcha-style">
                <div class="g-recaptcha" data-sitekey="6LfiSisUAAAAABBb-BlfVjfGQMTlK-AD6YnE6Ai4"></div>
                              <input type="hidden" title="Please verify this" class="required" name="keycode" id="keycode" />
              </div>
              <div class="col-sm-6 col-xs-12">
                <div class="form-group1 snd">
                                  <input type="submit" value="SEND MY MESSAGE" class="btn btn-warning col-sm-6 cpl-xs-12" name="subForm" />
                              </div>
                               <p class="we-reply">We’ll reply within one business day.</p>
              </div>
            </div>
          </form>
        </div>
         </div>
       </div>
     </div>
   </section>
   
   <!--related work-->
    <div class="project_div services">
         <div class="container">
            <h4 class="work_hdng"><?php the_field('work_heading');?></h4>
            <h3 class="mid_dcm"><?php the_field('work_content');?></h3>
            <div class="row photo_gallery ">
            <div class="owl-carousel_gal">
              <?php
              $portfolio_two = get_field('portfolio_items');
              $port_value = array();
              foreach ($portfolio_two as $portfoli) {
                $port_value[] = $portfoli->ID;
              }
            $folio_ids_arr =  $port_value;
  if (count($folio_ids_arr)) :
  
  $args = array(
          'post__in' => $folio_ids_arr,
          'post_type'=> 'portfolio',
          'order' => 'DESC',
          'posts_per_page' => -1
        );
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
               <div class="item <?php echo rtrim($posttags, " "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4><?php echo the_title();?></h4>
                       
            <p><?php
            // Get a list of terms for this post's custom taxonomy.
            $project_cats = get_the_terms($post->ID, 'keyfeature');
            // Renumber array.
            $project_cats = array_values($project_cats);
            for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
              <?php  echo $project_cats[$cat_count]->name; if ($cat_count<count($project_cats)-1){ echo ', '; }
            }
            ?></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;wp_reset_query();
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
          </div>
            </div>
         </div>
      </div>
   <!-- related work end-->
     <!--main body start-->
     <section class="main-body">
      <div class="container max-container adwords_second">
        
      <?php if( have_rows('highlights') ): ?>


  <?php while( have_rows('highlights') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          <div class="col-sm-5 img_middle imag magento_img">
                          <div class="large_img magento_logo google_adwords"> 
                          <h3 class="mobile-service-title">Magento Strategy</h3>
                          <h1 class="mobile-service-subtitle" style="font-size: 35px;">We’ll make Magento work for you</h1>                      
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                          </div>
                      </div>

          <div class="col-sm-7 content_middle adwords_strategy">
            <div class="purpose">
              <h3><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>
         </div>
            <?php endwhile; ?>



<?php endif; ?>
       
       
        </div>
      
  </section>
    <div class="clearfix"></div> 
   <!--main body end-->
     <!-- testimonial section start-->
      <!-- testimonial section end-->
    <!--graph section start-->
    <!-- <section class="main-body magento_specialist">
      <div class="container max-container adwords_second">
     <?php if( have_rows('highlights_loop2') ): ?>


  <?php while( have_rows('highlights_loop2') ): the_row(); 

    // vars
    $Text = get_sub_field('text');
    $heading = get_sub_field('heading');
    $logo = get_sub_field('logo');
    $content = get_sub_field('content');
    $image = get_sub_field('image');
    ?>

    <div class="col-sm-12" id="position">
          
         <div class="col-sm-5 img_middle imag adword_certified"> 
         <h3 class="mobile-service-title">Magento Certified</h3>
                          <h1 class="mobile-service-subtitle" style="font-size: 35px;">Our Magento specialists always go the extra mile for you</h1>                                            
                          <img class="img-responive" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                      </div>

          <div class="col-sm-7 content_middle adwords_strategy">
            <div class="purpose">
              <h3><?php echo $Text;?></h3>
              <div class="purpose-content">
                <h1><?php echo $heading;?></h1>
                <div class="purpose-content-text">
                <div class="purpose-content-logo">
                      <img class="img-responive" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
                  </div>
                  <p><?php echo $content;?></p>
                   
                </div>
                
              </div>
            </div>
          </div>

         
         </div>
            <?php endwhile; ?>



<?php endif; ?>
     </div>
   </section> -->

   <!--graph section end-->
    
 <div class="clearfix"></div>
   <!--store space section-->
   <!--  <section class="store-space">
    <div class="container">
      <div class="sub-container">
        <h3 class="text-center">HIGHLIGHT TEXT</h3>
        <h1 class="text-center">The Right Space for Your Store</h1>
        <?php //the_field('right_space',5160);?>
      </div>
    </div>
     
    </section>
 -->
   <!-- store space section end-->
     <div class="dcw-testimonial adword_page">
     <div class="container">
      <?php if( have_rows('testimonial') ):  while( have_rows('testimonial') ): the_row();
        $logo = get_sub_field('logo');
        $review = get_sub_field('review');
        $author = get_sub_field('author');
        $designation = get_sub_field('designation');
        $portfolio_name = get_sub_field('portfolio_name');
        $portfolio_link = get_sub_field('portfolio_link');
      ?>

       <h5> <img src="<?php bloginfo('template_url'); ?>/images/heart.png">  <span class="testimonial_brand">CLIENTS LOVE US</span></h5>
            <h4> <?php echo $review; ?> </h4>
              <div class="project-review">
         <!-- <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" /> -->
          <div class="project-reviewer">
             <p><?php echo $author; ?>, <a href="<?php echo get_site_url(); ?>/<?php echo $portfolio_link; ?>"><?php echo $portfolio_name; ?></a></p>

         <!--  <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked "></span>
<span class="fa fa-star checked "></span> -->
          </div>         
       </div>
          <?php endwhile; endif; ?>
    </div>
  </div>

  <!--work together section-->
  <div class="work-together clearfix">
    <div class="sub-container">
      <div class="col-sm-12">
        <div class="col-sm-4">
        <?php 
          $image = get_field('call_to_action');

          if( !empty($image) ): ?>

            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

          <?php endif; ?>
        </div>

        <div class="col-sm-8">
            <h1><?php the_field('work_together_title');?></h1>
            <div class="work-together-content">
                <p><?php the_field('work_together');?></p>
                <p class="touch"> <a href="<?php the_field('work_together_link');?>"> <?php the_field('work_together_link_content');?> </a> </p>
            </div>
        </div>
      </div>
    </div>
  </div>

</div>

  <!--work together section end-->
  
<?php get_footer(); ?>
<script>


var jQ = jQuery.noConflict();
jQ('.owl-carousel_gal').owlCarousel({
       //center:true,
       //margin:10,
      //autoplay:false,
       //autoplay:false,
   autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive : {
    // breakpoint from 0 up
    


0 : {
       loop:true,
        items:1,
        margin:5,
        stagePadding: 15,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
          
    },
    // breakpoint from 480 up
    480 : {
        loop:true,
        items:1,
        margin:5,
        stagePadding:50,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
        
    },
    768 : {
       loop:true,
        items:2,
        margin:5,
        stagePadding:110,
        center:true,
        smartSpeed:300,
        // nav:true
        autoplay:true,
        autoplayTimeout:1000
        //autoplayHoverPause:true,
        
    },
    // breakpoint from 768 up
    
    992 : {
        items:4,
        margin:5,
        stagePadding:5,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1024 : {
        items:2,
        margin:5,
        stagePadding:100,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1200 : {
      items : 4,
       autoplay:false,
       autoplayTimeout:1000,
      autoplayHoverPause:true,
      touchDrag: false,
      mouseDrag: false
        
    }
    }

})

</script>


<script>
var jQ = jQuery.noConflict();

if (jQ(window).width() > 1024) {
    var divHeight = jQ('.content_middle').height();
    jQ('.img_middle').css('height', divHeight+'px');
} 
</script>