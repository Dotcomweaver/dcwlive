<?php
/*
 Template Name: Knowlwdge Base
*/
get_header(); 
?>
<div class="work kb-banner clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/heading_icon_team.png"></div>

<!-- <section class="light-gray-wraper divider kb ptop40">
<div class="container kb-blog" id="container-blog ">
<div class="row">
<div class="col-md-12">
<div class="col-md-5">
<img class="img-responsive" alt="kb-img" src="/wp-content/uploads/2013/09/kb-img.png" />
</div>
<div class="col-md-7">
<h2>What your business can do with a knowledge base</h2>
<ul class="kblistitem">
	<li>Categorise information into a sophisticated hierachy</li>
	<li>Articles suggest other articles the reader may be interested in</li>
	<li>Share knowledge</li>
	<li>Knowledge base is easily searchable</li>
	<li>Complete web based help system</li>
</ul>
</div>
<div class="clearfix"></div>

</div>
</div>
</div>

</section> -->
<section class="divider ptop60 kb-sec">
<div class="vline"></div>
<div class="container">
<div class="row">
	<div class="col-sm-9 col-md-9">
		<?php echo do_shortcode('[qa]');?>  
		
        <div class="clearfix"></div>
        <?php if(function_exists('wp_paginate')) {
                                                wp_paginate();
                                                } 
            ?>
	</div>
	<div class="col-sm-3 col-md-3">
  <div class="blog_sidebar">
  <?php dynamic_sidebar('search');?>

  <div class="clearfix"></div>
  <div class="sidebar_header">
	  <h4> Categories </h4>
	  <hr />
  </div>
  	 <ul id="tags-list">
                <?php

                $post_type = 'qa_faqs';
                 
                // Get all the taxonomies for this post type
                $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
                 
                foreach( $taxonomies as $taxonomy ) :
                 
                    // Gets every "category" (term) in this taxonomy to get the respective posts
                    $terms = get_terms( $taxonomy );

                 
                    foreach( $terms as $term ) :
                         echo '<li><a href="' . get_tag_link ($term->name) . '" rel="tag">' . $term->name . '  </a></li>';
                     ?>
                   
                <?php endforeach;
                endforeach;?>
                </ul>

	<div class="clearfix"></div>
	
	<div class="sidebar_header">
		<h4>Instagram</h4>
		<hr />
	</div>
		<div class="clearfix"></div>
		<div class="instagram_gallery">
			<?php do_shortcode('[wp-instagram-gallery]');?>
		</div>

	<div class="clearfix"></div>
	<br />
	            <div class="related_posts">
                <div class="sidebar_header">
                    <h4>Blog</h4>
                    <hr />
                </div>

                  <?php 
                    $args = array( 'numberposts' => 3, 'post_status'=>"publish",'post_type'=>"post",'orderby'=>"post_date");
                    $postslist = get_posts( $args );
                     $title1 = get_the_title();
                    echo '<ul>';
                   
                     foreach ($postslist as $post) :  setup_postdata($post); ?>
                     <li> 
                     
                        <?php
                            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            if(!empty($feat_image)):
                        ?>
                            <div class="video_img">
                                 <img src="<?php echo $feat_image; ?>" class="img-responsive" />
                            </div>
                        <?php
                            endif;
                        ?>
                         <?php echo strlen($title1); ?>
                        <?php if(strlen($title1)>20){ //503?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"> <?php echo trim(substr(get_the_title(), 0, 100)).'...'; ?></a>
                        <?php } else{ ?>
                         <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"> <?php echo get_the_title(); ?></a>
                         <?php }?>
                         
                </li>
                <?php endforeach; ?>
                 </ul>

                <div class="clearfix"></div>
                <a href="/blog/" class="btn btn-default">View All</a>
            </div>
	<div class="blog_video_gallery">
		<div class="sidebar_header">
			<h4>Video Gallery</h4>
			<hr /> 
		</div>

		
	<?php
$queryObject = new WP_Query( 'post_type=video-testimonials&posts_per_page=3' );
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul>
    <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        $post_id = $post->ID;
        
$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
if($v_test_post_meta->vt_url != '') {
					
					$y_img = get_ytube_video_code($v_test_post_meta->vt_url);
					$te = $v_test_post_meta->vt_url;
					$start = strpos($te,"v=")+2;
					$end = strpos($te,"&");
					if( $end == "" || $end == 0 ){
						$fstr = substr($te,$start);
					} else {
						$length = $end-$start;
						$fstr = substr($te,$start,$length);
					}
					//print_r($v_test_post_meta);
					 ?>
					 	<li>
					 <?php if($v_test_post_meta->vt_url != ''):?>
                            <div class="video_img"> 
                            <img  alt="<?php the_title() ?> - Testimonial for NJ Web Design company" class="img-responsive" src="https://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" >
                        	 <!-- Play Button --> 
                            <a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/<?php echo trim($fstr); ?>" class="vt_play2" href="javascript:void();">
                                <span class="flaticon-play-button4"></span>
                            </a>
                            </div>
                        <?php endif; ?>
        					<!-- <a href="<?php //the_permalink(); ?>"> --><?php the_title(); ?><!-- </a> -->
                         </li>
    <?php
    }
}
    ?>
    </ul>
    
    <?php
}
?>
<div class="clearfix"></div>
		<a href="/video-testimonials/" class="btn btn-default">View All</a>
	</div>


	<div class="clearfix"></div>
	<br />

	<div class="sidebar_social">
		<div class="sidebar_header">
			<h4>Social</h4>
			<hr />
		</div>

		<ul class="social_icons">
			<li> <a href="https://www.facebook.com/dotcomweavers" target="_blank" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
			<li> <a href="http://twitter.com/Dotcomweavers" target="_blank" class="twitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
			<li> <a href="https://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="linkedin"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
			<!-- <li> <a href="https://www.pinterest.com/dotcomweaversnj/" target="_blank" class="pinterest"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li> -->
			<li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="google-plus"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
		</ul>
	</div>

	</div>
  
  </div>
</div>
</div>	 
</section>

<div class="triangleup"><a href="/white-papers/">
We Write White Papers!
<span class="sub_subtitle">Download Now</span>
</a></div>


<?php get_footer(); ?>
<style>
.client-icon img {

    margin: -49px auto;
}
</style>