<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<div class="item" id="post-<?php the_ID(); ?>">
<div class="blog-box-new">
<div class="blog-box-new-inner">
<div class="col-sm-4">
<div class="blog-img">
<?php
if( ! post_password_required() && ! is_attachment()):
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
if(!empty($feat_image)){
?>
	<a href="<?php the_permalink();?>"><img src="<?php echo $feat_image; ?>" class="img-responsive" /></a>
<?php
}else{?>
	<img src="/wp-content/themes/dcw/images/logo_slogan.jpg" class="img-responsive">
<?php }
endif;
?>
</div>
</div>
<div class="col-sm-8">
<div class="post-details">
<?php 
if ( comments_open() && ! post_password_required() ) :
//echo 'First If';

	if ( 'post' == get_post_type() ) :
	//echo 'second if'; ?>
        
            		<!-- <span class="icomoon-icon-user"></span><?php //the_author();?> -->
			<div class="post-meta">
			<?php 
			//query_posts( 'posts_per_page=6&orderby=rand' );
	        	$sposttags = get_the_tags($post_id);
			if($sposttags): ?>
		        <?php 
		        $i= 0;
					foreach($sposttags as $stags){
		        
		     if($i>0){
		     	echo '|';
		     }
		     echo '<a href="' . get_tag_link ($stags->term_id) . '" rel="tag">' . $stags->name . '  </a>';
		     	$i++;
		         } ?> |
	        <?php endif; ?>  <?php the_date('M j, Y'); ?>
	        </div>	
<?php endif; 
endif; ?>
	<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php $title =  get_the_title(); echo $title; /*$length = strlen($title); if($length > 55){ echo substr($title,0,55).'...'; }else{ echo $title; }*/ ?></a></h2>


<?php if ( is_search() ) { // Only display Excerpts for Search ?>
		
		<div class="info">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
<?php }else if('audio' == get_post_format()){?>
		
		<div class="info">
			<?php the_content(); ?>
        </div>
<?php }else { ?>
		
		<div class="info">
			<?php if(is_page('Press Releases')): ?>
            		
            		<?php the_content(); ?>
			<?php else: ?>
            		
			<?php   echo wp_trim_words( get_the_content(), 26 );?>
			<?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
            <?php endif; ?>
		</div><!-- .entry-content -->
<?php } ?>
	<!-- <a class="blog-but" href="<?php //the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php //the_title_attribute(); ?>">Continue Reading »</a>	 -->
	</div>
	</div>
</div>
</div>
</div>
<!-- .entry-meta -->
<!-- #post-<?php the_ID(); ?> -->


<!--<style>
.total-footer {
    position: static;
}
</style>-->