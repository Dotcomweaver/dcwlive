<?php
/*
Template Name: eCommerceseo
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_ecommerce.png"></div>

<div class="clearfix"></div>
<section class="devide">
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} // end while
} // end if
?>
</section>
<div class="clearfix"></div>
<div class="vline"></div>
<section class="no-padding featured hidden-xs" id="portfolio">
    <div class="container ">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="subtitle">Featured <span>Work</span></h2>
				<p class="text-center aboutxt">Check out some of the recent projects that brought greater success to our clients.</p>
            </div>
        </div>
    </div>
        <div class="container-fluid">
        <div class="row no-gutter dcw-works da-thumbs" id="da-thumbs">
        <?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 4;
        global $post;
	$folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
	//print_r($folio_post_meta);
	$folio_ids_arr = $folio_post_meta->services_folio;
	if (count($folio_ids_arr)) :
	
	$args = array(
					'post__in' => $folio_ids_arr,
					'post_type'=> 'portfolio',
					'order' => 'DESC',
					'posts_per_page' => -1
				);
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                  endforeach;
               
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
                    <div class="col-lg-3 col-sm-6 <?php echo rtrim($posttags, " "); ?>">
                        <div class="portfolio-box">
                                  <a href="<?php the_permalink(); ?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
							 <div class="line"></div>
                            <div class="project-category text-faded">
                                <h2><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h2>
                                 <a href="<?php the_permalink(); ?>"><?php echo wp_trim_words(strip_tags($the_content), 10); ?></a>
                            </div>
                            <div class="project-name">
							 <?php
                                                                $terms = apply_filters('taxonomy-images-list-the-terms', '', array(
                                                                    'taxonomy' => 'keyfeature',
                                                                    'posts_per_page' => 3));
                                                              foreach ((array) $terms as $term) {

                                                                    echo $term;
                                                                  
                                                                }
                                                                ?>

					<!--<a href="<?php //the_permalink(); ?>">DIG <br>DEEP</a>-->
						<br>
						 <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                            </div>                 
                        </div>
                    </div> 
                        </div>
                    </div><!-- col-sm-4 col-xs-6 -->
                    <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
        </div>
    </div>
</section>
<section class="no-padding featured visible-xs" id="portfolio">
<div class="featured_carousel">
    <div class="container ">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="subtitle">Featured <span>Work</span></h2>
                <p class="text-center aboutxt">Check out some of the recent projects that brought greater success to our clients.</p>
            </div>
        </div>
    </div>


<?php   wp_reset_query(); ?>

<div class="container-fluid">
   
        <div class="row no-gutter dcw-works da-thumbs" id="da-thumbs">
        <?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 4;
        global $post;
        $folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
    //print_r($folio_post_meta);
    $folio_ids_arr = $folio_post_meta->services_folio;
    if (count($folio_ids_arr)) :
    
    $args = array(
                    'post__in' => $folio_ids_arr,
                    'post_type'=> 'portfolio',
                    'order' => 'DESC',
                    'posts_per_page' => -1
                );
        query_posts( $args );?>

    <div class="owl-carousel owl-theme" id="featured_carousel">

            <?php  if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                  endforeach;
               
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
                
                    <div class="col-lg-3 col-sm-6 <?php echo rtrim($posttags, " "); ?>">
                       
                        <div class="portfolio-box">
                                  <a href="<?php the_permalink(); ?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                             <div class="line"></div>
                            <div class="project-category text-faded">
                                <h2><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h2>
                                 <a href="<?php the_permalink(); ?>"><?php echo wp_trim_words(strip_tags($the_content), 10); ?></a>
                            </div>
                            <div class="project-name">
                             <?php
                                                                $terms = apply_filters('taxonomy-images-list-the-terms', '', array(
                                                                    'taxonomy' => 'keyfeature',
                                                                    'posts_per_page' => 3));
                                                              foreach ((array) $terms as $term) {

                                                                    echo $term;
                                                                  
                                                                }
                                                                ?>

                    <!--<a href="<?php //the_permalink(); ?>">DIG <br>DEEP</a>-->
                        <br>
                         <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                            </div>                 
                        </div>
                    </div> 
                        </div>
                   
                    </div><!-- col-sm-4 col-xs-6 -->
                 
                    <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
          
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
 </div>
        </div>

    </div>
</div>

</section>
<div class="clearfix"></div>
<div class="vline"></div>
<section class="res-testmonilas">
	<div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 ">
                    <?php// echo do_shortcode('[jumptoservicesvt]'); ?>
                    
           <div id="video-testimonial-generic" class="carousel slide" data-ride="carousel">
           <?php $evideos = new WP_Query( array( 'page_id' => 6590, 'showposts' => -1 ));
              
              while ( $evideos->have_posts() ) : $evideos->the_post(); 
            
              ?>
    <?php if( have_rows('ecommerce') ): $i = 0; ?>
  <ol class="carousel-indicators">
    <?php while ( have_rows('ecommerce') ): the_row(); ?>
      <li data-target="#video-testimonial-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0) echo 'active'; ?>"></li>
    <?php $i++; endwhile; ?>  
  </ol>
<?php endif; ?>
<?php endwhile; 
        wp_reset_query();?>
      
        	
             <div class="row ">
              <h3 class="subtitle ecom_title"><span class="smalletxt">e</span>Commerce <span>Videos</span></h3>
                <div class="videotesti_container">           
                <div class="carousel-inner testimonial" role="listbox">
              <?php $evideos = new WP_Query( array( 'page_id' => 6590, 'showposts' => -1 ));
              
              while ( $evideos->have_posts() ) : $evideos->the_post(); 
             $counter = 1;

              ?>
                  <?php if( have_rows('ecommerce') ): ?>



  <?php while( have_rows('ecommerce') ): the_row(); 
  // vars
    $video = get_sub_field('video');
    $image = get_sub_field('image');
    $description = get_sub_field('description');
    $title = get_sub_field('title');

    ?>

  <div class="item <?php if($counter <= 1){echo " active"; } ?>">
                            
<div class="col-md-4 col-sm-5 col-xs-12 video-testimonials">
  <div class="video_img">
<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive">
<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="<?php echo $video; ?>" class="vt_play"><span class="flaticon-play-button4"></span></a>
</div></div>

<div class="col-md-8 col-sm-7 col-xs-12 ecomm_videos">
  <h2 class="ecomm_videos_title"><?php echo $title; ?></h2>
<div class="carousel-caption carousel-caption_top">
  <div class="quote-service">

    <?php echo  
                            wp_trim_words( $description, 40 )?> </div>
</div>
</div>
</div>
<?php $counter++;
   

   endwhile; ?>


<?php endif; ?>
<?php endwhile; 
        wp_reset_query();?>
    
       </div>
       <a class="left carousel-control" href="#video-testimonial-generic" role="button" data-slide="prev">
<span class="fa fa-angle-left rm_fa_left greytxt service_test" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>

<a class="right carousel-control" href="#video-testimonial-generic" role="button" data-slide="next">
<span class="fa fa-angle-right rm_fa_right greytxt service_test" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
  </div>
         </div>                    
               
            </div>        
                </div>
            </div>
  </div>
</section>      

<div class="clearfix"></div>
<div class="vline"></div>
<section class="blog_block">
	<div class = "container">
    <?php
 /* $arr_blog = get_posts(
  array(
  'numberposts' => -1,
  'post_status' => 'publish',
  'post_type' => 'post',
  )
  );*/
  $services_blog_post_meta = json_decode(get_post_meta(6590,'services_blog_post_meta', true));
  //print_r($services_blog_post_meta);
 $blog_head_text = $services_blog_post_meta->blog_head_text;
  $services_blog = $services_blog_post_meta->services_blog;

  $arr_blog = get_posts(
  array(
  'numberposts' => -1,
  'post_status' => 'publish',
  'post_type' => 'post',
  )
  );
  $svt_counter = 0;?>
<div class="col-md-10 col-md-offset-1">
    <h2 class="subtitle">Related  <span>Blogs</span></h2>
 <?php foreach($arr_blog as  $indv_blog)
  {

  $svt_counter++;
  if(isset($services_blog) && in_array($indv_blog->ID, $services_blog)){ 
//echo '<pre>';
  //  print_r($indv_blog);
    //exit;
   $post_thumbnail_id = get_post_thumbnail_id( $indv_blog->ID );
 $thumb = wp_get_attachment_image_src( $post_thumbnail_id, 'post'); 
 $yrdata= strtotime($indv_blog->post_date);
   ?>

           
                <div class="col-md-2 col-sm-3">
                    <div class="innovativetxt">
                    <?php if(!empty($thumb)){ ?>
                      <img src="<?php echo $thumb[0];?>"/>  
                    <?php }else{?>
                      <img src="/wp-content/themes/dcw/images/logo_slogan.jpg" class="img-responsive">
                    <?php }?>      
                    </div>
                     <p class="orngtxt blog_date text-center"><?php echo date('F d, Y', $yrdata); ?></p>
                </div>
                
                <div class="col-md-10 col-sm-9">
                <a href="<?php echo get_permalink($indv_blog);?>"><h2 class="blog_title orngtxt"><?php echo $indv_blog->post_title;?></h2></a>
                 <p>  <?php $content =  wp_trim_words($indv_blog->post_content, 35); echo $content; ?>
                <b><a href="<?php echo get_permalink($indv_blog);?>">Read More <i class="fa fa-long-arrow-right"></i></a></b></p>
                </div>
               <div class="clearfix"></div>
                
 
 <?php  }
  }
 
   
                   
  //print_r($arr_blog);

  ?>
     </div>
            
	</div>
</section>
<div class="clearfix"></div>
<div class="vline"></div>
<section class="ecmrce-accordians">
	<div class = "container">
  <h2 class="subtitle resfaq">More Questions about <span><span class="smalletxtb">e</span>Commerce SEO</span></h2>
	
<?php //echo do_shortcode('[jumptoservicesfaqs]');?>
  <?php
$faq_query = new WP_Query(
        array('post_type' => 'faq',
    'order' => 'ASC',
    'posts_per_page' => '-1',
           'cat'=>235
        )
);

//print_r($faq_query);
$flkey =0;?>
 <div class="panel-group" id="accordion-ecom">
 <?php while ($faq_query->have_posts()) : $faq_query->the_post();
   
    $title = get_the_title(); 
    $content = get_the_content();
   
      
        //$i = 0;
	
		if($flkey == 0){
			$flclass = '';
			$plus_minus = 'fa-caret-right';
		}else{
			$flclass = '';
			$plus_minus = 'fa-caret-right';
		}
        ?>
    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-ecom" href="#collapse<?php echo $flkey;?>">
                             <i class="fa <?php echo $plus_minus ?>"></i>
                              <span class="ttl"><?php echo $title;?></span>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse<?php echo $flkey;?>"  class="panel-collapse collapse">
                          <div class="panel-body"><?php echo $content?></div>
                        </div>
					</div>
        <?php $flkey++;
  
	?>
  
 <?php  endwhile;
?>
       </div>
	</div>
</section>
<script type="text/javascript">
  var jQ = jQuery.noConflict();
  jQ(document).ready(function() {
  jQ('#featured_carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  })
     })
 </script>
 
<?php get_footer(); ?>
