<?php
	/*
		Template Name: Contact
	*/
	get_header();
	//session_start();
	//global $wp_session;
	//$wp_session['token'] = 'yes';
	//$_SESSION['token'] = '1';
	/*echo "TOKen Checkup". $_SESSION['token'];
	echo "wp TOKen Checkup". $wp_session['token'];*/
	global $wp;  
$current_url = home_url(add_query_arg(array(),$wp->request));
?>
<?php 
if(isset($_POST['subForm'])){
	if(isset($_POST['g-recaptcha-response'])){
      $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
      echo '<h2>Please check the the captcha form.</h2>';
      exit;
    }
    $secretKey = "6LfiSisUAAAAABUp-TV9nq7wyzC_WPiXyMw2QMI2";
    $ip = $_SERVER['REMOTE_ADDR'];
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
    $responseKeys = json_decode($response,true);
    if(intval($responseKeys["success"]) !== 1) {
      echo '<h2>You are spammer ! Get the @$%K out</h2>';
    } else {
		$fullName = $_POST["full_name"];
		$eEmail = $_POST["email"];
		$pPhone = $_POST["phone"];
		$cCompany = $_POST["company"];
		$sSummary = $_POST["summary"];
		$workCountry = $_POST["work_country"];
		$thankYouPage = get_bloginfo('url').'/thank-you/';
		$thankRedirect = 'Location:'.$thankYouPage;

      	$ch = curl_init();
		$curlConfig = array(
		   CURLOPT_URL            => "https://www.pipelinedeals.com/web_lead",
		   CURLOPT_POST           => true,
		   CURLOPT_RETURNTRANSFER => true,
		   CURLOPT_POSTFIELDS     => array(
		       'w2lid' => '212f7ab56299',
		       'thank_you_page' => $thankYouPage,
		       'lead[full_name]' => $fullName,
		       'lead[email]' => $eEmail,
		       'lead[phone]' => $pPhone,
		       'lead[work_country]' => $workCountry,
		       'lead[company_name]' => $cCompany,
		       'lead[summary]' => $sSummary,
		   )
		);
		curl_setopt_array($ch, $curlConfig);
		$result = curl_exec($ch);
		curl_close($ch);
		header($thankRedirect);
    }
}

?>
<!-- contact us sub nav title-->
<div class="service-sub-menu text-navigation">
  <div class="container">
    <div class="service-menu-list why_submenu">
      <p class="text-center">WE’RE HERE TO HELP YOUR Business GROW</p>
    </div>
  </div>
</div>
<!-- contact us sub nav title end-->


<!-- banner contact us start-->
<div class="contact_banner" style="background : url(<?php bloginfo('template_url'); ?>/images/contact.png) center no-repeat;background-size: cover;">
	<div class="container">

		<div class="col-sm-7">
			<div class="contact_banner_text">
			<h1>Let's Talk</h1>
			<p>Want to discuss a project or get answers? Fill out the form and we’ll be in touch as soon as possible. </p>
			</div>
		</div>
		
	</div>
</div>
<!-- banner contact us end-->

<!--contact form section start-->
<div class="contact_us_form">
	<div class="container">
		<div class="contact_area clearfix">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="contact_left_area clearfix">
				<h3>SEND US A MESSAGE</h3>
					<form method="post" id="register-form" action="<?php echo $current_url; ?>/" name="myCntForm" method="post">	
						<!-- action="https://www.pipelinedeals.com/web_lead" -->
						<!-- <input type="hidden" name="w2lid" value="212f7ab56299" /> -->
						<!-- <input type="hidden" name="thank_you_page" value="https://dotcomweavers.staging.wpengine.com/thank-you/" /> -->
						<div class="form-one clearfix">
							<div class="col-sm-6 col-xs-12">
							  	<div class="styled-input">
			  						<input type="text" name="full_name" autocomplete="off" required />
		  							<p class="label_place">Name*</p>
			  						<span></span>
		  						</div>	
							</div>
							<div class="col-sm-6 col-xs-12">
							  	<div class="styled-input">
			  						<input type="text" name="email" required />
			  						<p class="label_place">Email*</p>
			  						<span></span>
		  						</div>	
							</div>
						</div>
						<div class="col-sm-6 col-xs-12">
						  	<div class="styled-input">
		  						<input type="text" name="phone" required />
		  						<p class="label_place">Phone*</p>
		  						<span></span>
	  						</div>	
						</div>

						<div class="col-sm-6 col-xs-12">
    						<div class="floating-label input-txt">      
      							<input class="floating-input floating-text" type="text" name="company" placeholder=" ">
      							<span class="highlight"></span>
      							<p class="textarea_label">Company</p>
    						</div>
						</div>
						
  						<input type="hidden" name="work_country" placeholder="Country" class="form-control" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >
				
  						<?php
						$curUrl = get_bloginfo('url');
						if($curUrl == 'https://dotcomweavers.staging.wpengine.com') { ?>
							<div class="col-sm-12 col-xs-12">
								<div class="floating-label message">      
									<textarea class="floating-input floating-textarea" id="form_summery" name="summary" placeholder=" "></textarea>
									<span class="highlight"></span>
									<p class="textarea_label">Tell us about your project.</p>
								</div>
							</div>
						<?php } else { ?>
							<div class="col-sm-12 col-xs-12">
								<div class="floating-label">      
									<textarea class="floating-input floating-textarea" name="summary" placeholder=" "></textarea>
									<span class="highlight"></span>
									<p class="textarea_label">Tell us about your project.</p>
								</div>
							</div>


						<?php } ?>

						<div class="form-footer">
							<div class="col-sm-6 col-xs-12 captcha-style">
								<div class="g-recaptcha" data-sitekey="6LfiSisUAAAAABBb-BlfVjfGQMTlK-AD6YnE6Ai4"></div>
	                            <input type="hidden" title="Please verify this" class="required" name="keycode" id="keycode" />
							</div>
							<div class="col-sm-6 col-xs-12">
								<div class="form-group1 snd">
	                                <input type="submit" value="SEND MY MESSAGE" class="btn btn-warning col-sm-6 cpl-xs-12" name="subForm" />
	                            </div>
	                             <p>We’ll reply within one business day.</p>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-sm-4 col-md-4 col-xs-12">
				<div class="get_box">
                        <h3>Get in touch.</h3>
                        
                        <p> <span> <i class="material-icons">&#xE55E;</i></span> <span> 15 Farview Terrace </span> <br> <span> Paramus, NJ 07652</span></p>
                    
                        <p> <span> <i class="material-icons">&#xE325;</i></span> <span> Give us a call.</span> <br>
							<span>201.880.6656<span></p>
                        <p> <span> <i class="material-icons">&#xE8AF;</i> </span> <a class="chat_message">Live Chat</a> <span> or</span> <br>                    <span>info@dotcomweavers.com</span></p>
                        
                        <p class="office_address"> <span> Offices in: NJ, NY, FL, and India.</span> </p>

                       <div class="form_social_icon">
                       	<ul>
                       		<li>  <a href="http://www.facebook.com/dotcomweavers" target="_blank"></a>  </li>
                       		<li>  <a href="https://www.instagram.com/dotcomweavers/" target="_blank"></a>  </li>
                       		<li>  <a href="http://twitter.com/Dotcomweavers" target="_blank"></a>  </li>
                       		<li>  <a href="https://www.linkedin.com/company/dotcomweavers-inc" target="_blank"></a>  </li>
                       	</ul>
                       </div>
                        
                    </div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>

