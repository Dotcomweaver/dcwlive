<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<?php
$author_id = $post->post_author;
$userImageMetaValue = get_user_meta( $author_id, 'dcw_user_avatar', true);
if(!empty($userImageMetaValue)){
    $authImage = wp_get_attachment_image_url($userImageMetaValue);
}else{
    $authImage = get_avatar_url($author_id);
}
$postthumburl = get_template_directory_uri().'/images/BlogBanner-blog.jpg';
if(empty($postthumburl)){
  $postthumburl = get_template_directory_uri().'/images/BlogBanner-blog.jpg';
}
?>
<!-- style="background: url(<?php get_template_directory_uri(); ?>/images/BlogBanner-blog.jpg); -->


<div class="blgin_banner" style="background: url(<?php echo $postthumburl; ?>) center no-repeat;background-size: cover;">
    <div class="container">
        <?php
          $author_id=$post->post_author; $metaKey = 'user_designation'; $single = true;
          $userMetaValue = get_user_meta( $author_id, $metaKey, $single );
          //$authname = the_field('author_name');
        ?>
        <?php //if( get_field('author_name') ){ ?>
         <!--  <h3><?php //the_field('author_name'); ?></h3> -->
        <?php //}else{?>
           <!-- <h3>Dotcomweavers</h3> -->
        <?php //}?>
       
               <h1><?php the_title(); ?></h1>
    </div>
</div>
<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/BlogBanner-blog.jpg" alt="clkmas"> -->
<div class="reed_pnmain">
         <div class="container">
            <div class="blog_sub_container">
               <div class="reed_tiitle">
                  <div class="media">
                     <div class="media-left">
                        <a href="javascript:void(0)">
                          <?php 

                      $image = get_field('author_image');

                      if( !empty($image) ){ ?>

                        <img class="media-object" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                      <?php }else{ ?>
                      <img class="media-object" src="/wp-content/themes/dcw/images/rsz_flat_logo.png" alt="Reed Paterson">
                      <?php }?>
                       
                        </a>
                     </div>
                     <div class="media-body">
                       <?php if( get_field('author_name') ){ ?>
                        <h4 class="media-heading"><?php the_field('author_name'); ?></h4>
                    <?php }else{?>
                       <h4 class="media-heading">DotcomWeavers</h4>
                    <?php }?>
                       
                        <?php if( get_field('author_role') ){ ?>
                         <p class="re_cnt"><?php the_field('author_role'); ?> at DotcomWeavers</p>
                      <?php }else{?>
                              <!-- <p class="re_cnt">Content Strategist at DotcomWeavers 123</p> -->
                        <?php } ?>
                        <p class="re_cnt2"><?php the_time('M d, Y'); ?>
                          <?php if(get_field('time_to_read')){ ?>
                             / Reading Time: <?php the_field('time_to_read'); ?> Minutes
                          <?php } ?>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="reed_inner">
                  <!-- <h4><?php //the_title(); ?></h4> -->
                  <div class="reed_bnr">
                     <div class="media">
                        <div class="media-left">
                        </div>
                        <?php
                            if ( has_post_thumbnail() ) { 
                            the_post_thumbnail( 'full' );
                            }
                        ?> 
                        
                     </div>
                  </div>
                  <div class="media-body">
                           <?php 
                            if ( have_posts() ) {
                              while ( have_posts() ) {
                                the_post(); 
                               the_content();
                              } // end while
                            } // end if
                            ?>
                        </div>
                 
                  
                  <ul class="lidots">
                     <li></li>
                     <li></li>
                     <li></li>
                  </ul>
               </div>
               <!-- tabs starts here -->
               <div class="blog_tabs">
                  <!-- Nav tabs -->
                  <div class="tabs_sub">
                     <div class="tabs_inn">
                        <?php $ppostId = get_the_ID(); $ttgas = get_the_tags( $ppostId );?>
                        <ul class="blog_tags">
                          <?php foreach ($ttgas as $ttga) {?>
                            <?php $ccolor = get_term_meta($ttga->term_id); 
                              $cclr = $ccolor[color][0];
                              if(empty($cclr)){
                                  $cclr = '#CC44D5';
                              }
                            ?>                            
                            <li style="background: <?php echo $cclr; ?>;"><?php echo $ttga->name; ?></li>
                          <?php } ?>
                           
                        </ul>
                         <ul class="blog_social">
                          <li>
                           <a href="http://www.twitter.com/share?url=<?php the_permalink(); ?>" target="_blank">
                              <i class="fab fa-twitter"></i>
                            </a>
                          </li>
                          <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                              <i class="far fa-thumbs-up"></i>
                            </a>
                          </li>
                          <li>
                            <?php
                            echo '<a href="https://plus.google.com/share?url='.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI].'" 
                            target="_blank"> <i class="fab fa-google-plus-g"></i></a>';
                            ?>
                           
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                     </div>
                     <div class="blogs_here">
                        <div class="media">
                           <div class="media-left">
                              

                              <a href="javascript:void(0)">
                              <!-- <img class="media-object" src="<?php //echo get_template_directory_uri(); ?>/images/BlogInnerThumb.jpg" alt="Reed Paterson"> -->
                               <a href="javascript:void(0)">
                          <?php 

                      $image = get_field('author_image');

                      if( !empty($image) ){ ?>

                        <img class="media-object" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                      <?php }else{ ?>
                       <img class="media-object" src="/wp-content/themes/dcw/images/rsz_flat_logo.png" alt="Reed Paterson">
                      <?php }?>
                       
                        </a>

                              </a>
                           </div>
                           <div class="media-body">
                                <?php if( get_field('author_name') ){ ?>
                                    <h4 class="media-heading"><?php the_field('author_name'); ?></h4>
                                <?php }else{?>
                                 <h4 class="media-heading">Dotcomweavers</h4>
                                <?php }?>
                                <?php if( get_field('author_role') ){ ?>
                                   <p class="blg_pr"><?php the_field('author_role'); ?></p>
                                   <p class="blog_pr1">DotcomWeavers</p>
                                <?php }else{?>
                                       <!--  <p class="blg_pr">Content Strategist at DotcomWeavers</p> -->
                                <?php } ?>
                            </div>
                        </div>
                     </div>
                     <ul class="prv_nxt clearfix">
                        <li class="prv"><?php previous_custom_post(); ?></li>
                        <li class="nxt"><?php next_custom_post(); ?></li>
                     </ul>
                     
                     <div class="clearfix"></div>
                  </div>
               </div>
               <!-- tabs ends here -->
            </div>
         </div>
      </div>

<script src="<?php bloginfo('template_url');?>/js/aos.js"></script>
<script type="text/javascript">
     AOS.init({
     offset: 200,
     easing: 'ease-in-sine',
     disable: 'mobile'
     // delay: 50,
     });
</script>

<?php get_footer(); ?>



<!-- <div class="blog_ecommerce clearfix">
    <div class="header-content">
        <div class="header-content-inner">
            <h1><?php //the_title(); ?></h1>
            <p></p>
        </div>
    </div>
</div> -->
<!-- <div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/Blog_icon.png"></div> -->
    <!-- <section class="divider ptop60">
        <div class="container" id="container-blog">
            <div class="row">
                <div class="col-md-12">
                    <?php //while ( have_posts() ) : the_post();
                        $category// = get_the_category( $post->ID);
                    //if($category != 'Home Slider'): ?>
                         <?php// get_template_part( 'content-single', get_post_format() ); ?>
                        <?php //    endif;  
                    //endwhile; ?>
                </div>
            </div>
        </div>
    </section> -->
