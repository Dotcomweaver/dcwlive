<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta property="og:image" content="<?php bloginfo('template_url');?>/img/brand-scroll.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<meta name="viewport" content="width=device-width">
<!-- <title><?php //wp_title(''); ?></title> -->
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/owl.theme.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/canvasdots.animation.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/fontsawesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/style.min.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Poppins:300,400,500,600,700" rel="stylesheet">
<?php wp_head(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16800069-1', 'auto', {'name':'gaTracker'});
  ga('gaTracker.send', 'pageview');

</script>

<!-- Autumn falling leaves js file is TweenMax.min.js -->
<!-- <script src="<?php //bloginfo('template_url');?>/js/TweenMax.min.js"></script> -->

</head>

<body>

<?php if(is_page( 479 ) ){?>
<!-- This is canvas animation head home banner with thumb beaver START-->
<!-- <div class="home">
<header>
      <div class="homedot_animation canvas-block">
       <canvas id="myCanvas1" style="position: absolute;"></canvas>
	      <div class="container">
            <div class="beavethmup">
            <div class="beavethmup_inner1">
            <div class="beavethmup_inner2">
              <a href="/about-beaver/"><img src="<?php bloginfo('template_url'); ?>/images/Beaver_ThumbsUp.png" class="img-responsive hidden-xs"></a>
              <a href="/about-beaver/"><img src="<?php bloginfo('template_url'); ?>/images/mobilebeaver.png" class="img-responsive visible-xs"></a>
              </div>
              </div>
            </div> 
            <div class="suprchrge_head">
              <h1>Supercharge Your Online Business</h1>
              <a href="/services/" class="swwd_btn">SEE WHAT WE DO</a>
            </div>       
        </div>
       </div> 
        <div class="clearfix"></div>
           <div class="h_services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce_g.png" alt="eCommerce" class="gr_icon">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce" class="wt_icon">
                  <h4>eCommerce</h4>
                  <p>From shopping cart upgrades to third party integrations, we consider your business requirements and develop solutions for them.</p>
                  <a href="/ecommerce" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive_g.png" alt="Responsive" class="gr_icon">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive" class="wt_icon">
                  <h4>Responsive</h4>
                  <p>We build high-quality responsive websites that scale to different devices for optimal user experience and higher conversions.</p>
                  <a href="/responsive-web-design" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication_g.png" alt="WebApplication" class="gr_icon">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication" class="wt_icon">
                  <h4>Web Applications</h4>
                  <p>From concept to completion, we create engaging Web applications that will streamline your eCommerce business.</p>
                  <a href="/web-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication_g.png" alt="MobileApplication" class="gr_icon">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication" class="wt_icon">
                  <h4>Mobile Apps</h4>
                  <p>Enhance user experience and encourage customer relationships as we take your website to the next level with mobile applications.</p>
                  <a href="/mobile-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
            </div>
          </div>
        </div>
       </header>
     </div> -->
     <!-- This is canvas animation head home banner with thumb beaver END-->

     <!-- This is old head home banner with man typing image START-->
<!-- <header>

        <div class="container">
          <h1>Supercharge Your Online Business</h1>
        </div>

        <div class="clearfix"></div>

        <div class="h_services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce">
                  <h4>eCommerce</h4>
                  <p>From shopping cart upgrades to third party integrations, we consider your business requirements and develop solutions for each of them.</p>
                  <a href="/ecommerce" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive">
                  <h4>Responsive</h4>
                  <p>We build high-quality responsive websites that scale to different devices for optimal user experience and higher conversions.</p>
                  <a href="/responsive-web-design" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication">
                  <h4>Web Applications</h4>
                  <p>From concept to completion, we create engaging Web applications that will streamline your business.</p>
                  <a href="/web-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication">
                  <h4>Mobile Apps</h4>
                  <p>Enhance user experience and encourage customer relationships as we take your website to the next level with mobile applications.</p>
                  <a href="/mobile-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
            </div>
          </div>
        </div>
  </header> -->
  <!-- This is old head home banner with man typing image END-->

<!-- This is old head home banner with man typing image and Autumn falling leaves START-->
<!-- <header>
<div  id="autumn_leavesfall">

</div>
        <div class="container">
          <h1>Supercharge Your Online Business</h1>
        </div>

        <div class="clearfix"></div>

        <div class="h_services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce">
                  <h4>eCommerce</h4>
                  <p>From shopping cart upgrades to third party integrations, we consider your business requirements and develop solutions for each of them.</p>
                  <a href="/ecommerce" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive">
                  <h4>Responsive</h4>
                  <p>We build high-quality responsive websites that scale to different devices for optimal user experience and higher conversions.</p>
                  <a href="/responsive-web-design" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication">
                  <h4>Web Applications</h4>
                  <p>From concept to completion, we create engaging Web applications that will streamline your business.</p>
                  <a href="/web-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication">
                  <h4>Mobile Apps</h4>
                  <p>Enhance user experience and encourage customer relationships as we take your website to the next level with mobile applications.</p>
                  <a href="/mobile-applications" class="btn btn-link">Read More <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
            </div>
          </div>
        </div>
</header> -->
<!-- This is old head home banner with man typing image and Autumn falling leaves END-->

<!-- Head home banner with man background image and google tringle review thing START-->
<div class="home baner_withgooglereview">
<header>
      <div class="home_orangenewbaner">
        <div class="container">
            <div class="suprchrge_head">
              <h1>Supercharge Your <br>Online Business</h1>
              <h3 class="homban_subhedline"><span>High-impact</span> web design, development,<br>
and marketing strategy</h3>
              <a href="/contact-us" class="swwd_btn">GET STARTED</a>
            </div>       
        </div>
        <div class="container gogle_reviewtribox">
          <div class="goglrievw_trianle">
            <a href="https://www.google.com/search?q=dotcomweavers&gws_rd=cr&dcr=0&ei=diLvWa6hIozsvgTQnLmoBQ#gws_rd=cr&lrd=0x89c2e52cd3dc44e9:0x49c0693ca393e2bd,1,," target="_blank">
              <h4>TOP RATED</h4>
              <p>4.8 <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i></p>
              <span>(51) Google Reviews</span>
            </a>
          </div>
        </div>
       </div> 
        <div class="clearfix"></div>
           <div class="h_services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <!-- <img src="<?php //bloginfo('template_url'); ?>/images/Icons/eCommerce_g.png" alt="eCommerce" class="gr_icon">
                  <img src="<?php //bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce" class="wt_icon"> -->
                  <h4>eCommerce <br>Websites</h4>
                  <p>Give your online customers an <strong>engaging shopping experience</strong> that builds brand excitement and earns you <strong>game-changing conversions.</strong></p>
                  <a href="/ecommerce" class="btn btn-link">See How <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <!-- <img src="<?php //bloginfo('template_url'); ?>/images/Icons/WebResponsive_g.png" alt="Responsive" class="gr_icon">
                <img src="<?php //bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive" class="wt_icon"> -->
                  <h4>Dynamic Web <br>Applications</h4>
                  <p>Beautiful and functional <strong>apps that people love to use</strong> get more shares, <strong>generate more revenue,</strong> and get featured on the App Store and Google Play.</p>
                  <a href="/web-applications" class="btn btn-link">See How <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <!-- <img src="<?php //bloginfo('template_url'); ?>/images/Icons/WebApplication_g.png" alt="WebApplication" class="gr_icon">
                <img src="<?php //bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication" class="wt_icon"> -->
                  <h4>Mobile App <br>Development</h4>
                  <p><strong>Power your ideas, products, services, and goals</strong> with custom-developed web appplications that <strong>separate your business from the competition.</strong></p>
                  <a href="/mobile-applications" class="btn btn-link">See How <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <!-- <img src="<?php //bloginfo('template_url'); ?>/images/Icons/MobileApplication_g.png" alt="MobileApplication" class="gr_icon">
                <img src="<?php //bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication" class="wt_icon"> -->
                  <h4>Social Media &amp; <br>Content Strategy</h4>
                  <p>Boost your online business with <strong>creative content marketing and social media plans</strong> that improve traffic, user engagement, and conversions.</p>
                  <a href="/services" class="btn btn-link">See How <i class="fa fa-chevron-circle-right"></i> </a>
                </div> 
              </div>
            </div>
          </div>
        </div>
       </header>
     </div>
<!-- Head home banner with man background image and google tringle review thing END-->


<div class="clearfix"></div>

      <div class="o_line"></div>

    <div class="clearfix"></div>
<?php } elseif (is_page( 6477 )){ ?>
 
<?php get_header(); ?>
<header>
  <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
    <div class="google-adwords">
    <h1> SUPERCHARGE <small> Your Online Business </small> </h1>
    <form class="cnt-frm " method="post" id="register-form" action="https://www.pipelinedeals.com/web_lead">

        <input type="hidden" name="w2lid" value="212f7ab56299" />
        <input type="hidden" name="thank_you_page" value="http://www.dotcomweavers.com/thank-you/" />
        <div class="col-sm-6">
         <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
        </div>
        
        <div class="col-sm-6">
          <input type="text" name="lead[company_name]" class="form-control"  placeholder="Company">
        </div>
        
        <div class="col-sm-6">
          <input type="text" name="lead[phone]" class="form-control" placeholder="Phone*" required>
        </div>

        <div class="col-sm-6">
          <input type="email" name="lead[email]" class="form-control"  placeholder="Email*" required>  
        </div>

        <input type="hidden" name="lead[work_country]" class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >  
        
        <div class="col-sm-12">
          <textarea type="text" name="lead[summary]" class="form-control cmnts" placeholder="How can we help you achieve your eCommerce goals? " /></textarea>
        </div>
        
        <div class="clearfix"></div> 
        <div class="snd">
          <div class="start_btn"><input type="submit" value="Submit" class="btn btn-warning" /></div>
          <div class="call_btn"> <a href="tel:8883156518"><input type="button" value="Call Now: 888.315.6518" class="btn btn-warning" /></a></div>
          <div class="clearfix"></div> 
        </div>
    
      </form>
      </div>
  </div> 
  <div class="clearfix"> </div>
</div>
         <div class="clearfix"></div>
              <div class="h_services adword-services">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-md-3">
                <div class="box">
                  <img src="<?php bloginfo('template_url'); ?>/images/Icons/eCommerce.png" alt="eCommerce">
                 <a href="/ecommerce"><h4>eCommerce Development</h4></a>
                  </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebResponsive.png" alt="Responsive">
                   <a href="/responsive-web-design"><h4>Responsive Design</h4></a>
                </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/WebApplication.png" alt="WebApplication">
                  <a href="/web-applications"><h4>Web Applications</h4></a>
                 </div> 
              </div>
              <div class="col-sm-6 col-md-3">
                <div class="box">
                <img src="<?php bloginfo('template_url'); ?>/images/Icons/MobileApplication.png" alt="MobileApplication">
                  <a href="/mobile-application-mobile-website-development"><h4>Mobile Applications</h4></a>
                 </div> 
              </div>
            </div>
          </div>
        </div>
 </header>
 <div class="clearfix"></div>

      <div class="o_line"></div>

    <div class="clearfix"></div>


    <script type="text/javascript">
      if(jQuery('header .google-adwords label').hasClass('error')){
        alert("has Class");
      };
    </script>

 <?php }?>
 <?php if(is_front_page()){?>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top home-nav">
  <?php } else {?>
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
  <?php }?>

<div class="topcallreq_sec">
  <div class="top_call">
      <a href="tel:8883156518" class="top_phone">CALL NOW</a>
 </div>
 <div class="top_rq">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
<div class="clearfix"> </div>
</div>
<div class="navbar-header col-md-4 col-sm-4 col-xs7">
<button type="button" class="navbar-toggle collapsed bs-1-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1 ">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-8 col-sm-6 hm-pg-flp">
<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <div class="top_nav">
    <a href="/contact-us/" class="top_phone"><i class="fa fa-phone"> </i>888.315.6518</a>
    <?php wp_nav_menu( array('menu' => 'socialmenu' )); ?>
  </div>
 <div class="flipper">
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
</div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>

</div>
<div class="flipper">
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
</div>
</div>
<button type="button" class="menu-rt  col-xs-5 button">Menu <span class="fa fa-bars " aria-hidden="true"></span></button>
</nav>

<!-- Mobile tablet slide navigation Start -->
  <div class="nav_multi_level2">
<ul>
<div class="slide_menubtn"><button type="button" class="menu-rt  col-xs-5 button"><span class="fa fa-bars " aria-hidden="true"></span></button>
</div>

<li class="company_link"><a href="/about-us">Company</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
    <ul class="submenu">
      <li><a href="/about-us">About Us</a></li>
      <li><a href="/our-process">Our Process</a></li>
      <li><a href="/partners">Partners</a></li>
      <li><a href="/team">Team</a></li>
      <li><a href="/about-beaver/">Beaver Bio</a></li>
      <li><a href="/journey-map/">Journey Map</a></li>
      <li><a href="/work-with-us/">Work With Us</a></li>
      <li><a href="/contact-us">Contact Us</a></li>
    </ul>
</li>

<li class="services_link">
<a href="/services/"> Services</a>
<span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
    <ul class="submenu">
      <li><a href="/ecommerce/">eCommerce</a>
          <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
            <ul class="submenu_inner">
                <li><a href="/b2b-solutions/">B2B Solutions</a></li>
                <li><a href="/magento-development/">Magento Development</a>
                  <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
                    <ul class="submenu_inner">
                        <li><a href="/magento-specialists/">Magento Specialists</a></li>
                    </ul>
                  </li>
                <li><a href="/strategy-consultation/">Strategy &amp; Consultation</a></li>
                <li><a href="/ecommerce-seo/">eCommerce SEO</a></li>
                <li><a href="/data-management/">Data Management</a></li>
            </ul>
      </li>
      <li><a href="/web-applications">Web Applications</a></li>
      <li><a href="/responsive-web-design">Responsive Design</a></li>
      <li><a href="/seo-sem">SEO/SEM</a></li>
      <li><a href="/mobile-application-mobile-website-development">Mobile Applications</a></li>
      <li><a href="/additional-services/">Additional Services</a></li>
    </ul>
</li>
<li>
  <a href="/work">Work</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
        <li><a href="/work">Portfolio</a></li>
        <li><a href="/case-studies">Case Studies</a></li>
        <li><a href="/before-after/">Before &amp; After</a></li>
      </ul>
</li>
<li>
  <a href="/social-hub">DCW Social</a>
      <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
      <li><a href="/social-hub">Social Hub</a></li>
      <li><a href="/tag/press-awards/" >Press Releases</a></li>
      </ul>
</li>
<li>
  <a href="/faq">Resources </a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu" style="display: block;">
          <li><a href="/faq">FAQs</a></li>
          <li><a href="/kb">Knowledge Base</a></li>
          <li><a href="/whitepapers/">White Papers</a></li>
      </ul>
</li>

<li><a href="/blog">Blog</a></li>
 <li><a href="/video-testimonials">Video Gallery</a></li>
</ul>
</div>

</div>

<!-- Mobile tablet slide navigation End -->
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" id="play-button" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>
<style>
@media only screen and (min-width:768px) and (max-width: 1024px){
.pattern-overlay iframe{top:0px !important;}	
header{height:auto;}
 }
@media(max-width:767px) {
.header-content-inner.hm_pg_cnt{top:30%;}header{height:auto;}
}

</style>

<script type="text/javascript">
   var jQ = jQuery.noConflict();
     jQ(document).ready(function(){
jQ('.nav_multi_level2 > ul li ul').hide();

jQ('.button.menu-rt.button').click(function(){
    jQ('.submenu-ac').toggleClass('submenu-ac');
    jQ('.active-submenu').toggleClass('active-submenu'); 
    jQ('ul.submenu, ul.submenu_inner').hide();
    jQ('.nav_multi_level2 > ul').toggleClass("main-ac");
  });
  
jQ('.menu-ac-btn').click(function(){
    jQ('.main-ac').removeClass("main-ac");
  });

jQ('.nav_multi_level2 > ul > li > ul.submenu li span').click(function(){
  jQ(this).prev().toggleClass('submenu-ac');
  jQ(this).parent().children('ul').slideToggle();
});

jQ('.nav_multi_level2 > ul > li > span.arrow').click(function(){

   if (jQ(this).prev().hasClass('active-submenu')){
   jQ(this).parent().children('ul.submenu').slideToggle();
   jQ(this).prev().toggleClass('active-submenu');
   
   }

    else {
  jQ('.nav_multi_level2 > ul > li > a').removeClass("active-submenu");   
  jQ('.nav_multi_level2 > ul > li > a').parent().children('ul.submenu').slideUp();     
  jQ(this).prev().addClass("active-submenu");
  jQ(this).parent().children('ul.submenu').slideDown();
  } 
  
  });

});
</script>


<!-- Autumn falling leaves js -->
<!-- <script type="text/javascript">

    /*
    a Pen by DIACO : twitter.com/Diaco_ml  ||  codepen.io/MAW
    powered by GSAP : https://www.greensock.com/
    */
    TweenLite.set("#autumn_leavesfall",{perspective:600})
    //TweenLite.set("img",{xPercent:"-50%",yPercent:"-50%"})

    var total = 30;
    var warp = document.getElementById("autumn_leavesfall"),  w = window.innerWidth , h = window.innerHeight;
     
     for (i=0; i<total; i++){ 
       var Div = document.createElement('div');
       TweenLite.set(Div,{attr:{class:'leaffall'},x:R(0,w),y:R(-200,-150),z:R(-200,200)});
       warp.appendChild(Div);
       animm(Div);
     }
     
     function animm(elm){   
       TweenMax.to(elm,R(6,15),{y:h+100,ease:Linear.easeNone,repeat:-1,delay:-15});
       TweenMax.to(elm,R(4,8),{x:'+=100',rotationZ:R(0,180),repeat:-1,yoyo:true,ease:Sine.easeInOut});
       TweenMax.to(elm,R(2,8),{rotationX:R(0,360),rotationY:R(0,360),repeat:-1,yoyo:true,ease:Sine.easeInOut,delay:-5});
     };

    function R(min,max) {return min+Math.random()*(max-min)};


    // a Pen by DIACO : twitter.com/Diaco_ml  ||  codepen.io/MAW

    // Autumn falling leaves close after 25 seconds
    setTimeout(function() {
        jQ('#autumn_leavesfall').hide("slow");
    }, 25000);

</script> -->
<!-- Autumn falling leaves js END-->
