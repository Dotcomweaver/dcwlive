<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title(''); ?></title>
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="http://www.dotcomweavers.com/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" type="text/css">
<!-- Custom Fonts -->
<link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,300,600,700|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="<?php bloginfo('template_url');?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php bloginfo('template_url');?>/css/owl.theme.css" rel="stylesheet">
<!-- Plugin CSS -->
<!-- <link rel="stylesheet" href="<?php //bloginfo('template_url');?>/css/animate.min.css" type="text/css">-->
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<!--[endif]-->
<?php wp_head(); ?>
<style>
header{background:none;}
.inner-left-side{padding-top: 0px;}
.affixed-top {margin-top: 100px;}
</style>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</head>
<body id="page-top" class="inr-head">
<div class="body-locked">
<nav id="mainNav" class="opacity-inner navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
<div class="navbar-header col-md-4 col-sm-4 col-xs-7">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>

<a class="navbar-brand page-scroll scrl dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand-scroll.png"></a>

<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-7 col-sm-6 hm-pg-flp">

<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <div class="top_nav">

    <?php wp_nav_menu( array('menu' => 'socialmenu' )); ?>
  <div class="flipper">
<div class="front">
<span class="blue-txt "><a href="/contact-us/"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div>
<div class="back">
<span class="blue-txt"><a href="/contact-us/"><i class="fa fa-pencil-square-o"></i> Request Quote</a> </span>
</div>
</div>
 </div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>
</div>
</div>

</nav>
<!-- FULLSCREEN MODAL CODE (.fullscreen) -->


<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>
<header>
<div class="portfolio-bnr">
<div class="header-content">
<div class="header-content-inner">
<h1>We are proud of our work</h1>
<p></p>
</div>
</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/client-cion.png"></div>
</header>
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="videoWrapper"></div>
</div>
</div>
</div>
</div>
