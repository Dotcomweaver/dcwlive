<?php
/*
Template Name: Team
*/
get_header();?>
<script type="text/javascript">
jQuery.noConflict();
jQuery( document ).ready(function() {
    
   jQuery( ".thumbnail" ).hover(function() {
       ///alert('test');
		var replaceval = jQuery(this).children('img').attr("rel");
		var pfimage = jQuery(this).children('img').attr("src");
		jQuery(this).children('img').attr("rel",pfimage);
		jQuery(this).children('img').attr("src",replaceval);
   });

});
</script>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon team-top-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/heading_icon_team.png"></div>


<section class="team_page mtop60">
	<div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="2"></li>
<!--	 <li data-target="#carousel-example-generic1" data-slide-to="3"></li>-->
  </ol>
 
  <!-- Wrapper for slides -->
   <div class="carousel-inner">
    <div class="item active">
        <img src="/wp-content/themes/dcw/images/TeamBanner_US.jpg" class="img-responsive" alt="team">
   
      <div class="carousel-caption">
   
      </div>
    </div>
    <div class="item">
  <img src="/wp-content/themes/dcw/images/TeamBanner_IN.jpg" class="img-responsive" alt="team">
      <div class="carousel-caption">
        
      </div>
    </div>
     <div class="item">
 <img src="/wp-content/themes/dcw/images/TeamBanner_ALL.jpg" class="img-responsive" alt="team">
      <div class="carousel-caption">
        
      </div>
    </div>
  </div>
 
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div> <!-- Carousel -->
</section>

<section class="team_blogs mtop60">
<div class="container">
    
     <?php           
                $team_query = new WP_Query(
                            array('post_type' => 'team',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        );  
				?>
     
  	<div class="row">
            
		<div class="images-row">
                     <?php //$content = get_the_content();
    while ($team_query->have_posts()) : $team_query->the_post();
    $teamlink = get_permalink( $id );
    $title = get_the_title(); 
    $prof_pic = get_field('professional_pic');
   $funky_pic = get_field('funky_pic');
    $designation = get_field('designation');
    $about = get_field('about');?>
		<div class="col-md-4 col-sm-4 col-xs-12">
	  			<div class="thumbnail">
			<?php if( !empty($prof_pic) ): ?>
                            <img src="<?php echo $prof_pic['url']; ?>" class="img-responsive profile-box" rel="<?php echo $funky_pic['url']; ?>" />
                            <img src="<?php echo $funky_pic['url']; ?>" class="img-responsive profile-box-mobiletab" rel="<?php echo $funky_pic['url']; ?>" />                            
                        <?php endif; ?>
				  <div class="caption">
					<h4><span><?php echo the_title();?></span>, <?php echo $designation; ?></h4>
					<p><?php echo $about; ?></p>

				</div>
			</div>	
                     
		</div>
                    <?php endwhile; ?>
			
			
			
			
			
		</div>
            <div class="profile clearfix mbtm20">
			<div class="profile-image col-md-4  col-sm-6 col-xs-12"></div>
			<div class="profile-dis col-md-8 col-sm-6"></div>
		</div>
                
<div class="triangleup blog_traingle"><a href="/our-mascot/">
You Met Our Team
<span class="sub_subtitle">Now Meet Our Mascot</span></a></div>   

		
	</div>
     
	</div>
</section>

 
<?php get_footer(); ?>
<style>
header {display: none;}


</style>

