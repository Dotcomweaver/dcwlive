<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	
		<?php if ( have_posts() ) : ?>

			<div class="blog_ecommerce clearfix">
			<div class="header-content">
				<div class="header-content-inner">
				        			<!-- <h1><?php //printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?></h1> -->
					<p></p>
				</div>
			</div>
				
			</div>
			<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/Blog_icon.png"></div>

			<div class="clearfix"></div></br>
			<div class="search_results">
			<div class="container">
			<br />
			<div class="vline"></div>
			 <h2 class="subtitle"> <?php printf( __( ' <small> Search Results for: </small> %s', 'twentyfifteen' ), get_search_query() ); ?> </h2>
			 <br />
			<div class="col-sm-9">
			<div id="posts" class="tmp-blog">
			<?php
			// Start the loop.
			$i = 1;  while ( have_posts() && $i<7 ) : the_post(); ?>

				<?php
				/*
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );

			// End the loop.
			$i++; endwhile;
			// Previous/next page navigation.
			 if(function_exists('wp_paginate')) {
                                                wp_paginate();
                                                } 
         

		// If no content, include the "No posts found" template.
		else {
			get_template_part( 'content', 'none' );
		}

		endif;
		?>
		</div>
			</div>
			<div class="col-sm-3">
  <div class="blog_sidebar">
  <?php dynamic_sidebar('search');?>

  <div class="clearfix"></div>
  <div class="sidebar_header">
	  <h4> Categories </h4>
	  <hr />
  </div>
  	<ul id="tags-list">
		<?php
			$tags = get_tags( array('orderby' => 'count', "type"      => "post", 'order' => 'DESC', 'number'=>20) );
			foreach ( (array) $tags as $tag ) {
			echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">' . $tag->name . '  </a></li>';
			}
		?>
		</li>
	</ul>

	<div class="clearfix"></div>
	
	<div class="sidebar_header">
		<h4>Instagram</h4>
		<hr />
	</div>
		<div class="clearfix"></div>
		<div class="instagram_gallery">
			<?php do_shortcode('[wp-instagram-gallery]');?>
		</div>

	<div class="clearfix"></div>
	<br />
	<div class="blog_video_gallery">
		<div class="sidebar_header">
			<h4>Video Gallery</h4>
			<hr />
		</div>

			<ul>
				<li>
                	<div class="video_img">
						<img src="https://img.youtube.com/vi/lWjCTOaQoyY/0.jpg" alt="video" class="img-responsive">
						<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="http://www.youtube.com/embed/lWjCTOaQoyY" class="vt_play2"><span class="flaticon-play-button4"></span></a>
					</div>
					"I’ll highly recommend DotcomWeavers to anybody <span>5 Likes</span>
				</li>

				<li>
                	<div class="video_img">
						<img src="https://img.youtube.com/vi/33oI9RP6C7Y/0.jpg" alt="video" class="img-responsive">
						<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="http://www.youtube.com/embed/33oI9RP6C7Y" class="vt_play2"><span class="flaticon-play-button4"></span></a>
					</div>
					"I’ll highly recommend DotcomWeavers to anybody <span>5 Likes</span>
				</li>

				<li>
                	<div class="video_img">
						<img src="https://img.youtube.com/vi/G7OULxmpoRA/0.jpg" alt="video" class="img-responsive">
						<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="http://www.youtube.com/embed/G7OULxmpoRA" class="vt_play2"><span class="flaticon-play-button4"></span></a>
					</div>
					"I’ll highly recommend DotcomWeavers to anybody <span>5 Likes</span>
				</li>
			</ul>
		<div class="clearfix"></div>
		<a href="/video-testimonials/" class="btn btn-default">View All</a>
	</div>

	<div class="clearfix"></div>
	<br />

	<div class="sidebar_social">
		<div class="sidebar_header">
			<h4>Social</h4>
			<hr />
		</div>

		<ul class="social_icons">
			<li> <a href="https://www.facebook.com/dotcomweavers" target="_blank" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
			<li> <a href="http://twitter.com/Dotcomweavers" target="_blank" class="twitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
			<li> <a href="https://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="linkedin"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
			<li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="google-plus"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
		</ul>
	</div>

	</div>
  
  </div>

			<div class="col-sm-3"></div>
			</div>
			</div>
			<div class="clearfix"> </div>
<br> <br> <br> <br><br>
<div class="triangleup">
    <a href="/contact-us/">
        Get an indepth look.
        <span class="sub_subtitle">Contact <b>Us</b></span>
    </a>
</div>
	

<?php get_footer(); ?>
