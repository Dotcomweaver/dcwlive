<?php
/*
Template Name: Mobile Applications
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1> <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_mobile_appli.png"></div>

<div class="container">
<div class="ecmmorce-iner clearfix">
                     
    <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
    <img class="img-responsive" src="/wp-content/themes/dcw/img/Mobile.png">
    </div>
	<div class="col-lg-6 col-sm-6 col-xs-12">
     <h3>IT’S TIME FOR YOU TO GET ONBOARD</h3>
     <p>Your shoppers’ personal mobile device is crucial to their shopping experience. People are using devices sequentially, and the time users spend on their smartphones is all part of the buying process. Don’t spend your time obsessing that conversions happen only at desktop and tablet. Our expertise using leading mobile web frameworks and our knowledge of iOS and Android platforms enable us to tiptoe your users along the path to purchase, whether they buy by mobile or later when they’re sitting on the couch at home.</p>

     </div>
</div>
 </div>
<?php echo do_shortcode('[jumptoservicesfolio]');?>

<section id="ecmrce-blg-slider clearfix">
	<div class="container content-headings">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="emcre-cnt">
			<h3>SIZE MATTERS</h3>
			<p>The real estate is small, but images must be big. For mobile users, easy access to products and services depends on easy viewing and easy touch. The bigger and sharper the images the better. Mobile use is a driving force in the retail purchase process, and we design mobile first-for the smaller device and adjust upward for larger screens.</p>
			</div>
			<div class="emcre-cnt">
			<h3>EYE TO THE FUTURE</h3>
			<p>We also take full advantage of the ExtJS5 framework enabling us to build mobile applications for the Android and iOS platforms. We leverage native applications like ExtJS5 plus JQuery, PhoneGap, Sencha and Bootstrap to extend the functionality of your mobile app by integrating device-specific customizations that optimize each device's unique capabilities. Mobile is dominating retail growth. The ability to extend and adapt functionality will help you get long-term mileage out of your mobile eCommerce investment.</p>
			</div>
			<div class="emcre-cnt">
			<h3>GRID DESIGN GEARED TOWARD FLOW</h3>
			<p>We leverage JQuery, PhoneGap and Sencha to develop a rich HTML5 mobile experience for users. These frameworks allow us to exploit the functionality built into newer smartphones and tablets. A lot of product/service research and comparison shopping takes place on mobile, and users need freedom to search and explore. This is important to the buying process. Front-end organization also matters. To ensure easy user access, we put the search bar right under page branding in visual hierarchy to get users what and where they want quickly.</p>
			</div>
			<div class="emcre-cnt">
			<h3>CHECKOUT: MAKES OR BREAKS THE BUYING PROCESS</h3>
			<p>Once customers decide to give you their money, your job is to speed them through checkout and eliminate pauses that might give them time to rethink. We develop simplified forms for checkout and integrate auto-fill browser capabilities to limit the number of clicks necessary to complete the checkout process. Forcing them to navigate to an outside pay service can lead to cart abandonment. We use third party integration tools to include several payment gateways and credit card options on the checkout page to keep users on your site.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="place-holder">
			<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="https://player.vimeo.com/video/109983904" class="vt_play">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
                 <img src="/wp-content/themes/dcw/img/5.jpg" alt="video" class="img-responsive">
				</a>
			</div>
                       <h2>Related Blog</h2>
                      <?php
                    $args=array(
                      'tag' => 'mobile-application-development',
                      'showposts'=>2,
                      'orderby' => 'date',
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                    
                      while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="place-holder">
			<p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?>
			<span><a href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-long-arrow-right"></i></a></span>
			</p>
			</div>
                       <?php
                      endwhile;
                    } //if ($my_query)
                  wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
		</div>
	</div>


</section>
<section class="video-testmonilas">
	<div class="container">
		<div class="row">
<!--			<div class="col-md-3 col-sm-3 col-xs-12">
				<p><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/ecmrc_video.jpg"></p>
			</div>-->
			<div class="col-md-9 col-sm-9 col-xs-12">
                            <?php echo do_shortcode('[jumptoservicesvt]');?>
                <!--	<span class="cma-left"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-left.png"></span>
				<p>his section of text is up to you. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
				<span>- First Name Last Name, President of Battlefield Collection</span>
				</p>
				<span class="cma-right"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-right.png"></span>-->
			</div>
		</div>
	</div>
</section>

<section class="ecmrce-accordians">

	<div class = "container">

  <h3>More Questions about Mobile Applications</h3>
	<?php echo do_shortcode('[jumptoservicesfaqs]');?>

  
	</div>
</section>


 
<?php get_footer(); ?>
