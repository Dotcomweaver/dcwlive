<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title(''); ?></title>
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="http://www.dotcomweavers.com/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css">
<!-- Custom Fonts -->
<link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,300,600,700|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<!--<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">-->
<!--[if lt IE 9]>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</head>

<body id="page-top">
<?php if(is_front_page()){?>
<header>
<section class="content-section video-section">
<div class="video-overlay-colors"></div>
<div class="pattern-overlay">
<iframe id="home_video"src="https://player.vimeo.com/video/156800368?background=1" style="top: -25%;position: absolute; width: 100% !important; height:100% !important;left:0;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<img class="img-responsive mobile-header-img hidden-lg hidden-sm hidden-md" src="<?php bloginfo('template_url'); ?>/images/mobile-image-office.jpg">
</div>
<div class="header-content-inner hm_pg_cnt">
<div class="container">
     <h1 class="home-page-nw-dng" style="font">Web Solutions <p class="sub">That <strong>Grow</strong> Your Business</p></h1>
<!--<h1 id="brand">
Create.<br>
Integrate.<br>
Convert.<br>
<span><a href="/contact-us/">Request Quote</a></span>
</h1>-->
<a id=""  class="vt_play2 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/158428659" data-target="#vt_modal" data-toggle="modal"><p> Watch a day in the life at DCW <i class="fa fa-play" aria-hidden="true"></i></p></a>
</div>
</div>
</section>
</header>
<?php }?>

<div class="homepage_contact" style="padding: 35px 0;text-align: center;">
  <h1 style="color: #000;">Contact Us</h1>
</div>

<div class="body-locked">
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
<div class="navbar-header col-md-4 col-sm-4 col-xs7">
<button type="button" class="navbar-toggle collapsed bs-1-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1 ">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
<a class="navbar-brand page-scroll scrl dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand-scroll.png"></a>
<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-7 col-sm-6 hm-pg-flp">

<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <div class="top_nav">

    <?php wp_nav_menu( array('menu' => 'socialmenu' )); ?>
  <div class="flipper">
<div class="front">
<span class="blue-txt "><a href="/contact-us/"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div>
<div class="back">
<span class="blue-txt"><a href="/contact-us/"><i class="fa fa-pencil-square-o"></i> Request Quote</a> </span>
</div>
</div>
 </div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>
</div>
</div>

</nav>
<!-- FULLSCREEN MODAL CODE (.fullscreen) -->


<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>

<style>

header{height: 75%;}

@media only screen and (min-width:768px) and (max-width: 1024px){
.pattern-overlay iframe{top:0px !important;}	
header{height:auto;}
 }
@media(max-width:767px) {
.header-content-inner.hm_pg_cnt{top:30%;}header{height:auto;}
}

</style>