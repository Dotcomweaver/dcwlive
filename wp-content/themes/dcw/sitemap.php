<?php
/*
 Template Name: Sitemap Page
*/
get_header(); 
?>
<div class="work clearfix" style="background: url(<?php bloginfo('template_url');?>/images/Casestudy_banner.jpg) center no-repeat;">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/heading_icon_team.png"></div>

<section class="light-gray-wraper sitemap mtop30">
        <div class="container">
        	<div class="row mtop40">
            	<div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Company</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'mainmenu' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Services</div>
                      <div class="panel-body">
                        <h5>Ecommerce</h5>
                        <?php wp_nav_menu( array('menu' => 'services submenu ecommerce' )); ?>
                        <h5>Custom Development</h5>
                        <?php wp_nav_menu( array('menu' => 'service-submenu-custom-development' )); ?>

                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Portfolio</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'work' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                                
                <div class="clearfix"></div>
                
               
               <!-- /col-md-4 -->
                
                
            </div><!-- /row -->
           
    </div>
</section>
       

<?php get_footer(); ?>