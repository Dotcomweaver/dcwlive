<?php
/*
Template Name: Test code
*/
get_header();?>
<h1><?php the_title(); ?></h1>
<?php 
$portfolio_two = get_field('portfolio_two');
?>

<div class="work_section">

   <div class="project_div bg_wht">
         <div class="container">
            <div class="row photo_gallery">
              <div class="owl-carousel_work">
            	<?php
				  //$folio_ids_arr = $folio_post_meta->services_folio;
				  //$folio_ids_arr = explode(",",$folio_post_meta->folio_head_text);

        			$portfolio_two = get_field('portfolio_items');
        			$port_value = array();
        			foreach ($portfolio_two as $portfoli) {
        				$port_value[] = $portfoli->ID;
        			}
				  	$folio_ids_arr =  $port_value;

					if (count($folio_ids_arr)) :
					
					$args = array(
									'post__in' => $folio_ids_arr,
									'post_type'=> 'portfolio',
									'order' => 'DESC',
									'posts_per_page' => -1
								);
				        query_posts( $args );

				             if (have_posts()) : $i = 0;
				            while (have_posts()) :
				                the_post();
				                $post_id = $post->ID;
				                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
				                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
				                //print_r($portfolio_post_meta);
				                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
				                $the_link = get_permalink();
				                $the_title = get_the_title();
				                $the_content = get_the_content('Read more');
				                $postclasses = '';
				                $posttags = '';
				                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
				                foreach ($portfolio_image_id_arr as $attach_img_id) :
				                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
				                endforeach;
				                foreach ($term_list as $item):
				                    $posttags .= $item . " ";
				                endforeach;
				                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
               <div class="item <?php echo rtrim($posttags," "); ?>">
                  <div class="pg_dcm_main">
                     <div class="img_div">
                        <a href="<?php the_permalink(); ?>"><img class="img-responive" src="<?php echo $portfolio_feat_image[0]; ?>"/></a>
                        <div class="plus_button"><a href="<?php the_permalink();?>"><i class="fas fa-plus"></i></a></div>
                        <div class="photo_des">
                           <h4> <a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h4>
                       
						<p>
							<?php

						// Get a list of terms for this post's custom taxonomy.
						$project_cats = get_the_terms($post->ID, 'keyfeature');
						// Renumber array.
						$project_cats = array_values($project_cats);
						//$x = $project_cats;
							//$x = preg_replace('/\s*,\s*/', ',', $x);

							//echo $x;
						for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
						  <?php  echo $project_cats[$cat_count]->name;if ($cat_count<count($project_cats)-1){echo",";}
						}
						?></p>
                           
                        </div>
                     </div>
                  </div>
               </div>
               <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
          </div>
          </div>  
         </div>
      </div>
<!-- Work block section close here -->

</div>
 <div class="clearfix"></div>


<?php get_footer(); ?>