<?php
/*
Template Name:mainservices
*/
?>


<?php get_header();?>

  <div class="service-sub-menu">
  <div class="container">
    <div class="service-menu-list">
      <ul>
        <li> <a href="<?php echo get_site_url(); ?>/ecommerce/"> ECOMMERCE </a> </li>
        <li> <a href="<?php echo get_site_url(); ?>/magento/"> Magento</a></li>
        <li> <a href="<?php echo get_site_url(); ?>/erp-integration/"> ERP</a> </li>
        <li> <a href="<?php echo get_site_url(); ?>/ecommerce-marketing/"> Marketing </a> </li>
        <li> <a href="<?php echo get_site_url(); ?>/custom-software/"> CUSTOM SOFTWARE</a></li>
        <li> <a href="<?php echo get_site_url(); ?>/mobile-apps/"> MOBILE APPS </a> </li>
        <!-- <li> <a href="/magento-adwords/"> MAGENTO ADWORDS </a> </li> -->
      </ul>
    </div>
  </div>

</div>


 <div class="main-service-banner sticky_subnav">
    <div class="container">
    <div class="col-sm-12">
    <div class="col-sm-5 col-xs-12 pull-right">
        <div class="banner-right service_landing_box">
            <?php 
                $countt = get_field('count_number');
                $image = get_field('banner_image');
                if(!empty($countt) || $countt == 0){ ?>
                  <div class="magento-project-counter">
                    <div class="site_lunched_box">
                        <div class="project_counter">
                            <h1><?php the_field('count_number'); ?></h1>
                      </div>
                      <div class="site_lunched_description">
                          <p><?php the_field('banner_box_content'); ?></p>
                      </div>
                      <div class="get_magento_site_btn">
                          <a href="<?php bloginfo('url');?><?php the_field('banner_box_link_url'); ?>"><?php the_field('banner_box_link_text'); ?></a>
                      </div>
                    </div>
                  </div>
                <?php } elseif( !empty($image) ) { ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php } else {
                    $banvideo = the_field('banner_video');
                    if($banvideo){
                        echo $banvideo; 
                    }
                }
            ?> 


           </div>
        </div>
       <div class="col-sm-7 col-xs-12 pull-left">
          <?php the_field('banner_text');?>
      </div>
      

    </div>
     
    </div>

  </div>
  <!--main service banner ends-->

  <!-- service navigation content-->
  <div class="service-navigation-content">
    <div class="container">
      <div class="col-sm-12">
        <?php if( have_rows('servicess_section') ): while( have_rows('servicess_section') ): the_row(); ?>
        <div class="col-sm-4 col-md-4">
          <div class="content-section">
            <a href="<?php the_sub_field('section_link'); ?>"><h3 class="m0"><?php the_sub_field('section_title'); ?></h3></a>
            <p><?php the_sub_field('section_content'); ?></p>
          </div>
          <div class="service-content-footer">
            <h4><?php the_sub_field('tags_section_title'); ?></h4>
            <div class="col-sm-12 p0 servic_list">
              <?php if( have_rows('tags_section') ): while( have_rows('tags_section') ): the_row(); ?>
                  <div class="col-sm-6 p0 ">
                      <i class="fa fa-circle" aria-hidden="true"></i>
                      <a href="<?php the_sub_field('tag_link'); ?>">
                          <?php the_sub_field('tag_name'); ?>
                      </a>
                  </div>
              <?php endwhile; endif; ?>
              
            </div>             
          </div>
        </div>
        <?php endwhile; endif; ?>

      </div>
    </div>
  </div>

  <!-- service navigation content end-->

<div class="clearfix"></div>
<div class="dcw-testimonial">
     <div class="container">
      <?php if( have_rows('testimonial') ):  while( have_rows('testimonial') ): the_row();
        $logo = get_sub_field('logo');
        $review = get_sub_field('review');
        $author = get_sub_field('author');
        $designation = get_sub_field('designation');
        $portfolio_name = get_sub_field('portfolio_name');
        $portfolio_link = get_sub_field('portfolio_link');
      ?>
       <h5> <img src="<?php bloginfo('template_url'); ?>/images/heart.png">  <span class="testimonial_brand">CLIENTS LOVE US</span></h5>
            <h4> <?php echo $review; ?> </h4>
              <div class="project-review">
         <!-- <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" /> -->
          <div class="project-reviewer">
             <p><?php echo $author; ?>, <a href="<?php echo get_site_url(); ?>/<?php echo $portfolio_link; ?>"><?php echo $portfolio_name; ?></a></p>

         <!--  <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked "></span>
<span class="fa fa-star checked "></span> -->
          </div>         
       </div>
          <?php endwhile; endif; ?>
    </div>
  </div>


  <!--work together section-->
  <div class="work-together service_main clearfix">
    <div class="sub-container">
      <div class="col-sm-12">
        <div class="col-sm-4">
          <?php 
          $image = get_field('call_to_action');

          if( !empty($image) ): ?>

            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

          <?php endif; ?>
        
        </div>

        <div class="col-sm-8">
          <h1><?php the_field('work_together_title');?></h1>
          <div class="work-together-content">
              <p><?php the_field('work_together');?></p>
              <p class="touch"> <a href="<?php the_field('work_together_link');?>"> <?php the_field('work_together_link_content');?> </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--work together section end-->



<?php get_footer(); ?>
