<?php
	/*
		Template Name: Contact2017design
	*/
	get_header();
	//session_start();
	//global $wp_session;
	//$wp_session['token'] = 'yes';
	//$_SESSION['token'] = '1';
	/*echo "TOKen Checkup". $_SESSION['token'];
	echo "wp TOKen Checkup". $wp_session['token'];*/
?>

<div class="work clearfix">
    <?php 
		if ( has_post_thumbnail() ) {
			the_post_thumbnail('full');
		}  ?>
		<div class="header-content">
			<div class="header-content-inner">
				<h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
				<p></p>
			</div>
		</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url')?>/img/heading_icon_contact_us.png"></div>
<div class="clearfix">
	<section>
		
		
		<div class="container">
			<div class="row">
				<div class="cnt_page">
					<h2>We’re here to help your business grow. </br> 
 <br> </h2>
					
				</div>
			</div>
		</div>
		
		<div class="cnt_bg">
		</div>
		<div class="container">
			<div class="cnt_md_page">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="get_box">
						<h3>Get in touch.</h3>
						
						<p><i class="material-icons">&#xE55E;</i>15 Farview Terrace<br>Paramus, NJ 07652</p>
					
						<p><i class="material-icons">&#xE325;</i> Give us a call.<br>
888.315.6518</p>
						<p><i class="material-icons">&#xE8AF;</i><a class="chat_message">Live Chat</a> or <br>info@dotcomweavers.com</p>
						
						<p>Offices in: NJ, NY, FL, and India. </p>
						
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="mtop601" id="-example">
						<form class="cnt-frm " method="post" id="register-form" action="https://www.pipelinedeals.com/web_lead" name="myCntForm">
							
							<input type="hidden" name="w2lid" value="212f7ab56299" />
							<input type="hidden" name="thank_you_page" value="https://dotcomweavers.com/thank-you/" />
							
							<div class="form-group has-feedback">
								<input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
							</div>
							<div class="form-group">
								<input type="text" name="lead[company_name]" class="form-control"  placeholder="Company">
							</div>
							<div class="form-group has-feedback">
								<input type="text" name="lead[phone]" class="form-control" placeholder="Phone*" required>
							</div>
							<div class="form-group">
							<input type="email" name="lead[email]" class="form-control"  placeholder="Email*" required>	</div>
							<div class="form-group">	
							<input type="hidden" name="lead[work_country]" class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >	</div>
							<!--<div class="form-group">
								<select name="lead[custom_fields][1464592]" class="form-control">
								<option value="">How did you hear about us ?</option>
								<option value="1952542">10 Best Design</option>
								<option value="1952545">Clutch</option>
								<option value="1952551">Conferences</option>
								<option value="1952548">Online Search</option>
								<option value="1952554">Publications </option>
								<option value="1952557">Social Media</option>
								<option value="1952560">Referral</option>
								<option value="1952563">Other</option>
								</select> 
							</div>-->
							<?php
								$curUrl = get_bloginfo('url');
								if($curUrl == 'https://dotcomweavers.staging.wpengine.com'){ ?>
									<div class="form-group">
										<textarea type="text" name="lead[summary]" class="form-control cmnts" id="form_summery" placeholder="Tell us about your project."></textarea>
									</div>
								<?php } else { ?>
									<div class="form-group">
										<textarea type="text" name="lead[summary]" class="form-control cmnts" placeholder="Tell us about your project."></textarea>
									</div>
								<?php } ?>
							
							<div class="g-recaptcha" data-sitekey="6LfQrCgUAAAAAIceSaiCAAgIoDh-rZOnqYeKP9XG"></div>
							<input type="hidden" title="Please verify this" class="required" name="keycode" id="keycode" /> 

							<div class="form-group1 snd">
								<input type="submit" value="SEND MY MESSAGE" class="btn btn-warning col-sm-6 cpl-xs-12" />
								<p>We’ll reply within one business day.</p>
							</div>
							
							<?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?>
							<?php endif; // end sidebar widget area ?>
						</form>
					</div>	
				</div>
			</div>
			<div class="cnt_hgt"></div>
		</div>
		
	</section>
</div>
<script>
	var JQ = jQuery.noConflict();
	JQ(document).ready(function(){
	    JQ(".chat_message").click(function(){
	        JQ(".olark-launch-button").click();
	    });
	    /*JQ("#form_summery").change(function(){
	        var olddata = document.getElementById("form_summery").value;
	        olddata += ' Lead from Staging';
	        document.getElementById("form_summery").value = olddata;
	    });*/
	    /*JQ( "#register-form" ).submit(function( event ) {
		  alert( "Handler for .submit() called." );
		  event.preventDefault();
		});*/

	});
	
</script>
<?php get_footer(); ?>
<style>
header {display: none;}
.total-footer{position: static;} 
.footer-contact{margin-top: 0px;}
.affix1 {position: fixed; top:65px;}
.affix-top1  #navbar-example {top: 100px;}
.body-locked{margin-bottom:0px;}
.client-icon img {margin: -49px auto;}
@media (min-width: 1025px){
.body-locked {margin-bottom: 418px;}
}
</style>
