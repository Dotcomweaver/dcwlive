<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta property="og:image" content="<?php bloginfo('template_url');?>/img/brand-scroll.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<meta name="viewport" content="width=device-width">
<!-- <title><?php //wp_title(''); ?></title> -->
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,300,600,700|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="<?php bloginfo('template_url');?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php bloginfo('template_url');?>/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/fontsawesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/style.min.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Raleway:300,400,500,600,700" rel="stylesheet">
<?php wp_head(); ?>
<style>
header{background:none;}
.inner-left-side{padding-top: 0px;}
.affixed-top {margin-top: 100px;}
</style>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</head>
<body id="page-top" class="inr-head">
  <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-38465042-2', 'auto'); ga('send', 'pageview'); </script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16800069-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());
  gtag('config', 'UA-16800069-1');
</script> 
<nav id="mainNav" class="opacity-inner navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
  <div class="topcallreq_sec">
  <div class="top_call">
  
    <a href="tel:8883156518" class="top_phone">CALL NOW</a>
 </div>
 <div class="top_rq">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
<div class="clearfix"> </div>
</div>
<div class="navbar-header col-md-3 col-sm-3 col-xs-7">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-9 col-sm-6 hm-pg-flp">
<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <!-- <div class="top_nav">
    <a href="/contact-us/" class="top_phone"><i class="fa fa-phone"> </i>888.315.6518</a>
    <?php //wp_nav_menu( array('menu' => 'socialmenu' )); ?>

  
 </div> -->
 <div class="flipper">
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> CONTACT US</a> </span>
</div>
<a href="/contact-us/" class="top_phone"><!-- <i class="fa fa-phone"> --> </i>888.315.6518</a>
</div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>
  
</div>
<div class="flipper">
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> CONTACT US</a> </span>
</div>
</div>
</div>
<button type="button" class="menu-rt  col-xs-5 button">Menu <span class="fa fa-bars " aria-hidden="true"></span></button>
</nav>
  <div class="nav_multi_level2">
<ul>
<div class="slide_menubtn"><button type="button" class="menu-rt  col-xs-5 button"><span class="fa fa-bars " aria-hidden="true"></span></button>
</div>
<li>
  <a href="/work">Work</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
        <li><a href="/work">Portfolio</a></li>
        <li><a href="/case-studies">Case Studies</a></li>
        <li><a href="/before-after/">Before &amp; After</a></li>
        <li><a href="/tag/press-awards/" >Press Releases</a></li>
        <li><a href="/video-testimonials">Video Testimonials</a></li>
      </ul>
</li>
<li>
    <a href="/ecommerce/">eCommerce</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
          <li><a href="/b2b-solutions/">B2B Solutions</a></li>
          <li><a href="/magento-development/">Magento Development</a>
            <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
              <ul class="submenu_inner">
                  <li><a href="/magento-specialists/">Magento Specialists</a></li>
              </ul>
            </li>
          <li><a href="/strategy-consultation/">Strategy &amp; Consultation</a></li>
          <li><a href="/ecommerce-seo/">eCommerce SEO</a></li>
          <li><a href="/data-management/">Data Management</a></li>
      </ul>
</li>
<li><a href="/web-applications/">Custom Applications</a></li>

<li class="services_link">
<a href="/services/"> Services</a>
<span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
    <ul class="submenu">
      <li><a href="/responsive-web-design">Responsive Website Design</a></li>
      <li><a href="/seo-sem">Search Engine Marketing</a></li>
      <li><a href="/mobile-applications/">Mobile Applications</a></li>
      <li><a href="/content-development/">Content Development</a></li>
      <!-- <li><a href="/additional-services/">Additional Services</a></li> -->
    </ul>
</li>

<li class="company_link"><a href="/about-us">Company</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
    <ul class="submenu">
      <li><a href="/about-us">About Us</a></li>
      <li><a href="/our-process">Our Process</a></li>
      <li><a href="/partners">Partners</a></li>
      <li><a href="/team">Team</a></li>
      <li><a href="/our-mascot/">Our Mascot</a></li>
      <li><a href="/journey-map/">Journey Map</a></li>
      <li><a href="/work-with-us/">Work With Us</a></li>
      <!-- <li><a href="/contact-us">Contact Us</a></li> -->
    </ul>
</li>
<li>
  <a href="/social-hub">DCW Social</a>
      <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
      <li><a href="/social-hub">Social Hub</a></li>
      
      </ul>
</li>
<li>
  <a href="/faq">Resources </a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu" style="display: block;">
          <li><a href="/blog">Blog</a></li>
          <li><a href="/faq">FAQs</a></li>
          <li><a href="/kb">Knowledge Base</a></li>
          <li><a href="/whitepapers/">White Papers</a></li>
      </ul>
</li>


 
</ul>
</div>

</div>
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="videoWrapper"></div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

  var jQ = jQuery.noConflict();
     jQ(document).ready(function(){

/* Multi Level 2 Menu S */

jQ('.nav_multi_level2 > ul li ul').hide();

jQ('.button.menu-rt.button').click(function(){
    jQ('.submenu-ac').toggleClass('submenu-ac');
    jQ('.active-submenu').toggleClass('active-submenu'); 
    jQ('ul.submenu, ul.submenu_inner').hide();
    jQ('.nav_multi_level2 > ul').toggleClass("main-ac");
  });
  
jQ('.menu-ac-btn').click(function(){
    jQ('.main-ac').removeClass("main-ac");
  });

jQ('.nav_multi_level2 > ul > li > ul.submenu li span').click(function(){
  jQ(this).prev().toggleClass('submenu-ac');
  jQ(this).parent().children('ul').slideToggle();
});

jQ('.nav_multi_level2 > ul > li > span.arrow').click(function(){

   if (jQ(this).prev().hasClass('active-submenu')){
   jQ(this).parent().children('ul.submenu').slideToggle();
   jQ(this).prev().toggleClass('active-submenu');
   
   }

    else {
  jQ('.nav_multi_level2 > ul > li > a').removeClass("active-submenu");   
  jQ('.nav_multi_level2 > ul > li > a').parent().children('ul.submenu').slideUp();     
  jQ(this).prev().addClass("active-submenu");
  jQ(this).parent().children('ul.submenu').slideDown();
  } 
   
  
  
  });

/* Optional  E */
  
});
</script>