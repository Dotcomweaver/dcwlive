<?php
/*
Template Name: socialhub
*/
get_header();
?>
<!-- Header for inner pages 
================================================== -->
<div class="work clearfix">
     <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
    
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_social_hub.png"></div>
<section class="light-gray-wraper  social-hub-page mtop60">
<div class="container">
<div class="row">
<div class="col-md-12">
<?php echo do_shortcode('[juicer name="www-dotcomweavers-com-weavers-hub"]');?>
</div>
</div>
</div>	
</section>
<?php get_footer(); ?>
<style>
.juicer-feed a img {
    float: inherit;
    width: 50px !important;
}
</style>