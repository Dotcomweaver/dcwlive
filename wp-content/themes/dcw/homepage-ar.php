<?php
/*
Template Name: arabic
*/
get_header('arabic');?>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

      <div class="o_line"></div>

    <div class="clearfix"></div>

<section class="no-padding hidden-xs" id="portfolio">

        <div class="featured_block">
          <div class="header-group">
  <?php the_field('featured_work', 10152); ?> 
             </div>
          </div>
        </div>
                 <div class="clearfix"></div>
        
        <div class="row no-gutter dcw-works da-thumbs" id="da-thumbs">
        <?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 8;
    global $post;
  $folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
  //print_r($folio_post_meta);
  $folio_ids_arr = $folio_post_meta->services_folio;
  if (count($folio_ids_arr)) :
  
  $args = array(
          'post__in' => $folio_ids_arr,
          'post_type'=> 'portfolio',
          'order' => 'DESC',
          'posts_per_page' => -1
        );
        query_posts( $args );

             if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
                    <div class="col-lg-3 col-sm-6 <?php echo rtrim($posttags, " "); ?>">
                        <div class="portfolio-box">
                                  <a href="<?php the_permalink(); ?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
               <div class="line"></div>
                            <div class="project-category text-faded">
                                <h2><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h2>
                                 <a href="<?php the_permalink(); ?>"><?php echo wp_trim_words(strip_tags($the_content), 10); ?></a>
                            </div>
                            <div class="project-name">
               <?php
                            $terms = apply_filters('taxonomy-images-list-the-terms', '', array(
                                'taxonomy' => 'keyfeature',
                                'posts_per_page' => 3));
                                foreach ((array) $terms as $term) {
                                echo $term;
                                }
                            ?>
              <br>
             <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                            </div>                 
                        </div>
                    </div> 
                        </div>
                    </div>
                    <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
        </div>

   
</section>

<section class="no-padding visible-xs" id="portfolio">

        <div class="featured_block">
          <div class="header-group">
             <?php the_field('featured_work', 10152); ?> 
             </div>
          </div>
        </div>
        <?php   wp_reset_query(); ?>
                 <div class="clearfix"></div>
        
        <div class="row no-gutter dcw-works da-thumbs" id="da-thumbs">
        <?php
        $portfolio_images_settings = json_decode(get_option('portfolio_images_settings'));
        $portfolio_images_num_display = 4;
    global $post;
  $folio_post = get_post();
        $folio_post_meta = json_decode(get_post_meta($folio_post->ID,'services_folio_post_meta', true));
  //print_r($folio_post_meta);
  $folio_ids_arr = $folio_post_meta->services_folio;
  if (count($folio_ids_arr)) :
  
  $args = array(
          'post__in' => $folio_ids_arr,
          'post_type'=> 'portfolio',
          'order' => 'DESC',
          'posts_per_page' => -1
        );
        query_posts( $args );
        
//print_r($args);

        ?>
         <div class="featured_carousel">
    <div class="owl-carousel owl-theme" id="featured_carousel">


            <?php if (have_posts()) : $i = 0;
            while (have_posts()) :
                the_post();
                $post_id = $post->ID;
                $portfolio_feat_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(600, 600), false, '');
                $portfolio_post_meta = json_decode(get_post_meta($post_id, 'portfolio_post_meta', true));
                //print_r($portfolio_post_meta);
                $term_list = wp_get_post_terms($post_id, 'filter_tags', array("fields" => "names"));
                $the_link = get_permalink();
                $the_title = get_the_title();
                $the_content = get_the_content('Read more');
                $postclasses = '';
                $posttags = '';
                $portfolio_image_id_arr = array_filter(explode(',', $portfolio_post_meta->portfolio_attached_image));
                foreach ($portfolio_image_id_arr as $attach_img_id) :
                    $attach_folio_img = wp_get_attachment_image_src($attach_img_id, 'medium');
                endforeach;
                foreach ($term_list as $item):
                    $posttags .= $item . " ";
                endforeach;
                if ($portfolio_feat_image[0] != '' && $portfolio_post_meta->folio_is_home == 'Y'):
                    ?>
                    <div class="col-lg-3 col-sm-6 <?php echo rtrim($posttags, " "); ?>">
                        <div class="portfolio-box">
                                  <a href="<?php the_permalink(); ?>"><img src="<?php echo $portfolio_feat_image[0]; ?>" class="img-responsive" alt="" /></a>
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
               <div class="line"></div>
                            <div class="project-category text-faded">
                                <h2><a href="<?php the_permalink(); ?>"><?php echo the_title();?></a></h2>
                                 <a href="<?php the_permalink(); ?>"><?php echo wp_trim_words(strip_tags($the_content), 10); ?></a>
                            </div>
                            <div class="project-name">
               <?php
                            $terms = apply_filters('taxonomy-images-list-the-terms', '', array(
                                'taxonomy' => 'keyfeature',
                                'posts_per_page' => 3));
                                foreach ((array) $terms as $term) {
                                echo $term;
                                }
                            ?>
              <br>
             <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                            </div>                 
                        </div>
                    </div> 
                        </div>
                    </div>
                    <?php
                    $i++;
                endif;
                if ($i == $portfolio_images_num_display): break;
                endif;
            endwhile;
            endif;
        else:
            ?>
            <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
</div></div>
        </div>
<div class="featured_button">
  <a href="/work" class="btn btn-default dcworng_btn">View All</a>
</div>
</section>
  <div class="clearfix"></div>
     <?php dynamic_sidebar('counter');?>
    <div class="clearfix"></div>

      <div class="o_line"></div>

    <div class="clearfix"></div>


<div class="our-result arabic">
      <div class="container">
        <div class="header-group">
           <?php the_field('our_results', 10152); ?> 
        </div>
        <div class="clearfix"></div>
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
                        <div class="carousel-inner" role="listbox">
                                            
              <div class="item active left">
                <div class="col-md-7 col-sm-6"> 
                  <div class="result-text">
                    <h4>Pump Products</h4>
                    <p> نحن مقتنعون جدا منتجات المضخة ، مع فريق DotcomWeavers. بصفتنا شركة تجارة إلكترونية ، يعد تخطيط مواقع الويب والتنقل أمرًا هامًا بالنسبة لنا. ساعدتنا DCW في تطوير موقع ويب فريد وفعال وسهل الاستخدام. هناك مستوى جيد من الثقة بين فريقنا وفريق DCW. موصى به للغاية لشركات التجارة الإلكترونية التي تتطلع إلى النمو. </p>
                  </div>
                    <div class="text-right">
                     <img src="/wp-content/uploads/2016/11/pump_client-1.png" class="img-responsive"> كريج ماكريكارد, مدير التجارة الإلكترونية ، منتجات المضخة  
                    </div>
                </div>  
                <div class="col-md-5 col-sm-6">
                  <img src="&lt;img" width="500" height="500" class="img-responsive wp-post-image" alt="" srcset="/wp-content/uploads/2016/11/-34">                </div>  
              </div>
                      
              <div class="item next left">
                <div class="col-md-7 col-sm-6"> 
                  <div class="result-text">
                    <h4>شركة Limo Ride</h4>
                    <p> لقد قمت بتعيين DotcomWeavers في أكثر من مناسبة على مدار السبعة أعوام الماضية. إن الجودة والتناسق الذي يقدمونه في خدماتهم لا مثيل له. فلم تواجهنى أي مشكلة أبدًا لم أجد لها حلًا عند DotcomWeavers. أنا دائمًا ما أصف خدمتهم أنها الأفضل من نوعها في هذا المجال.
                   </p>
                  </div>
                    <div class="text-right">
                     <img src="/wp-content/uploads/2016/11/limoride.png" class="img-responsive"> -جاري ليرانيان,  المدير التنفيذي ومؤسس شركة Limo Ride  
                    </div>
                </div>  
                <div class="col-md-5 col-sm-6">
                  <img src="&lt;img" width="500" height="500" class="img-responsive wp-post-image" alt="" srcset="/wp-content/uploads/2016/11/lride.png">                </div>  
              </div>
                            </div>
                   <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
         </div>

    </div>

    <div class="clearfix"></div>
     <!-- Press and awards start -->
    <div class="press_awards">
      <div class="container">

        <div class="o_line"></div>
        <div class="clearfix"></div>
        <div class="header-group">
            <?php the_field('press_awards', 10152); ?> 
        </div>
        
        <div class="clearfix"></div>

        <!-- PRESS icons start -->
        <table class="table">
          <tr>
            <td> <a href="https://clutch.co/profile/dotcomweavers" target="_blank"><img src="<?php bloginfo('template_url');?>/images/clutch.png" class="img-responsive visible-lg">
              <img src="<?php bloginfo('template_url');?>/images/clutch_WT.png" class="img-responsive hidden-lg">
            </a> </td>
            <td> <a href="http://www.10bestdesign.com/firms/" target="_blank"><img src="<?php bloginfo('template_url');?>/images/badge.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/badge_WT.png" class="img-responsive hidden-lg"></a></td>
            <td> <a href="http://www.webaward.org/winner.asp?eid=20742#.WDggzxp97Dd" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/web_awards.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/web_awards_WT.png" class="img-responsive hidden-lg"> </a> </td>
            <td> <a href="#" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/F50_WinnerEmail.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/F50_WinnerEmail_WT.png" class="img-responsive hidden-lg"> </a> </td>
          </tr>

          <tr>
            <td> <a href="http://www.njbiz.com/article/20170123/NJBIZ01/301239990/1291/business-is-clicking" target="_blank"><img src="<?php bloginfo('template_url');?>/images/NJBIZ_Colorlogo.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/NJBIZ_Colorlogo_WT.png" class="img-responsive hidden-lg"></a> </td>
            <td><a href="http://www.smartceo.com/advice-on-your-ecommerce-strategy-from-the-ceo-of-dotcomweavers/" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/smartceo.png" class="img-responsive visible-lg"> <img src="<?php bloginfo('template_url');?>/images/smartceo_WT.png" class="img-responsive hidden-lg"></a></td>
            <td><a href="http://web-development.cioreview.com/cxoinsight/the-google-buy-button-and-its-impact-on-mobile-ecommerce-nid-23201-cid-121.html" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/CIO.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/CIO_WT.png" class="img-responsive hidden-lg"> </a></td>
            <td> <a href="http://www.websitemagazine.com/content/blogs/posts/archive/2016/06/30/ux-design-that-connects-with-the-millennial-mindset.aspx" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/website_magazine.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/website_magazine_WT.png" class="img-responsive hidden-lg"> </a></td>
           </tr>

          <tr>
            <td> <a href="http://www.bizjournals.com/philadelphia/blog/guest-comment/2014/08/e-commerce-websites-3-mistakes-to-avoid.html" target="_blank"><img src="<?php bloginfo('template_url');?>/images/TheBusinessJournals.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/TheBusinessJournals_WT.png" class="img-responsive hidden-lg"></a> </td>
            <td><a href="http://www.huffingtonpost.com/amit-bhaiya/" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/The_Huffington_Post_logo.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/The_Huffington_Post_logo_WT.png" class="img-responsive hidden-lg"></a> </td>
            <td><a href="http://blog.lemonstand.com/9-expert-tips-designing-better-mobile-ecommerce-experience/" target="_blank"> <img src="<?php bloginfo('template_url');?>/images/Lemonstand-logo.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/Lemonstand-logo_WT.png" class="img-responsive hidden-lg"></a> </td>
            <td> <a href="http://ecommerce-news.internetretailer.com/search?w=dotcomweavers&asug=dotcom&isort=score" target="_blank"><img src="<?php bloginfo('template_url');?>/images/internet-retailer.png" class="img-responsive visible-lg"><img src="<?php bloginfo('template_url');?>/images/internet-retailer_WT.png" class="img-responsive hidden-lg"></a> </td>
          </tr>

        </table>
        <!-- PRESS icons ends -->
      </div>
    </div>
    <!-- press and awards ends -->
   
    <div class="clearfix"></div>
       <div class="ourcustomer">
      <div class="container">
        
        <div class="o_line"></div>
        
        <div class="clearfix"></div>

        <?php dynamic_sidebar('reviews');?>
        <div class="clearfix"></div>
        <a href="https://plus.google.com/+Dotcomweavers/about" target="_blank">
          <img src="<?php bloginfo('template_url');?>/images/new-google-logo-png.png" class="img-responsive google-icon">
        </a>
      </div>
    </div>
     <div class="clearfix"></div>
       <div class="video_gallery">
      <div class="container">
        <div class="o_line"></div>
        <div class="clearfix"></div>
        <div class="header-group">
        <?php the_field('video_gallery', 10152); ?> 
        </div>
         <div id="video_gallery"  data-interval="false" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
          <?php           
                $video_query = new WP_Query(
                            array('post_type' => 'videogallery',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        ); 
                        $active_class = "active"; ?>
                        <?php 
                      while ($video_query->have_posts()) : $video_query->the_post();
                      $v1=get_field('video1');
                      $v2=get_field('video2');
                      $v3=get_field('video3');
                      $i1=get_field('image1');
                      $i2=get_field('image2');
                      $i3=get_field('image3');
                      $t1=get_field('text1');
                      $t2=get_field('text2');
                      $t3=get_field('text3');
                     ?>
               <div class="item <?php echo $active_class; ?>">
              <div class="row no-gutter">
                <div class="col-sm-4 dcw-home-videos">
                  <div class="video-thumb hidden-xs">
                   <img src="<?php echo $i1['url']; ?>" class="img-responsive">
                    <a class="video_overlay vt_play2 dropdown-toggle" href="javascript:void();" rel="<?php echo $v1; ?>" data-target="#vt_modal" data-toggle="modal">
                    <div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
                      <div class="overlay-text">
                        <?php echo $t1; ?>
                      </div>
                    </a>
                  </div>
                  <div class="video-thumb hidden-xs">
                    <img src="<?php echo $i2['url']; ?>" class="img-responsive">
                    <a class="video_overlay vt_play2 dropdown-toggle" href="javascript:void();" rel="<?php echo $v2; ?>" data-target="#vt_modal" data-toggle="modal">
                    <div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
                      <div class="overlay-text">
                         <?php echo $t2; ?>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-sm-8 dcw-home-videos">
                  <div class="video-thumb">
                    <img src="<?php echo $i3['url']; ?>" class="img-responsive">
                    <a class="video_overlay vt_play2 dropdown-toggle" href="javascript:void();" rel="<?php echo $v3; ?>" data-target="#vt_modal" data-toggle="modal">
                    <div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
                      <div class="overlay-text">
                        <?php echo $t3; ?>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
              <?php 
                   $active_class = ""; 
                   endwhile; ?>
                   <?php wp_reset_postdata(); ?>
          </div>
        </div>
         <div class="clearfix"></div>
        <div class="view-all">
          <a href="/video-testimonials/" class="btn btn-default">View All</a>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="connect_with">
      <div class="container">
        <div class="o_line"></div>
        
        <div class="clearfix"></div>

        <div class="header-group">
          <?php the_field('get_in_touch_home', 10152); ?> 
        </div>

        <br>
        <br>
        

     
        <div class="clearfix"></div>
       <div class="triangleup"><a href="/contact-us/">
<span class="sub_subtitle">ابقى على تواصل</span>
</a></div>

      </div>
    </div>
     <div class="clearfix"></div>

<script type="text/javascript">
  var jQ = jQuery.noConflict();
  jQ(document).ready(function() {
  jQ('#featured_carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  })
 })
 </script>


<?php get_footer('arabic'); ?>

