<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title(''); ?></title>
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="http://www.dotcomweavers.com/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css">
<!-- Custom Fonts -->
<link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,300,600,700|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<!--<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">-->
<!--[if lt IE 9]>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</head>

<body id="page-top">
<div class="homepage_contact">
       
        <div class="container">
              <button type="button" class="close btn btn-link" data-dismiss="modal" aria-hidden="true">
<i class="clsee" style="color:#fff;">x</i></button>
            <div class="row">
                <div class="col-md-2 hidden-sm"></div>
                <div class="col-md-8">

                 <form class="cnt-frm" id="request-form1" method="post" action="https://www.pipelinedeals.com/web_lead">
                 <div class="form-group snd">
                 <p>Complete this form below to get your free quote.</p>
    <input type="submit" value="Submit Your Request" class="btn btn-warning" />
      <div class="clearfix"></div>
    </div>
<div class="row">
    <input type="hidden" name="w2lid" value="e836c7b7971f" />
    <input type="hidden" name="thank_you_page" value="http://dotcomweavers.staging.wpengine.com/thank-you/" />

    <!-- Basic Demographic Fields --> 
    <div class="col-md-6 col-sm-6">
    <div class="form-group has-feedback">
      <label><i class="fa fa-user"></i></label>
       <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name">
    </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group">
        <label><i class="fa fa-envelope"></i></label>
    <input type="email" name="lead[email]"class="form-control"  placeholder="Email*" required>  </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group">
        <label><i class="fa fa-building"></i></label>
    <input type="text" name="lead[company_name]"class="form-control"  placeholder="Company*" required>
        </div>
        </div>
        <div class="col-md-6 col-sm-6">
                <div class="form-group has-feedback">
                <label><i class="fa fa-phone"></i></label>
    <input type="text" name="lead[phone]"class="form-control" placeholder="Phone*" required>
    </div>
    </div>
    
       
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
    <label><i class="fa fa-comments"></i></label>
    <input type="hidden" name="lead[work_country]"class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >  </div>
    <div class="form-group">
    <input type="text" name="lead[summary]" class="form-control cmnts header" placeholder="Comments">   </div>
    
    

    </div>
    </div>
                </form> 
                    

                </div>

                <div class="col-md-2 hidden-sm"></div>
        </div>
        </div>
</div>
<?php if(is_front_page()){?>
<header>
<section class="content-section video-section">
<div class="video-overlay-colors"></div>
<div class="pattern-overlay">
<iframe id="home_video" src="https://player.vimeo.com/video/156800368?background=1" style="top: -25%;position: absolute; width: 100% !important; height:100% !important;left:0;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<img class="img-responsive mobile-header-img hidden-lg hidden-sm hidden-md" src="<?php bloginfo('template_url'); ?>/images/mobile-image-office.jpg">
</div>
<div class="header-content-inner hm_pg_cnt">
<div class="container">
    <h1 class="home-page-nw-dng" style="font">Web Solutions <p class="sub">That <strong>Grow</strong> Your Business</p></h1>
<!--<h1 id="brand">
Create.<br>
Integrate.<br>
Convert.<br>
<span><a href="/contact-us/">Request Quote</a></span>
</h1>-->
<a id="pause-button" class="vt_play2 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/158428659" data-target="#vt_modal" data-toggle="modal"><p>Watch a day in the life at DCW <i class="fa fa-play" aria-hidden="true"></i></p></a>
</div>
</div>
</section>
</header>
<?php }?>



<div class="body-locked">
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
<div class="navbar-header col-md-4 col-sm-4 col-xs7">
<button type="button" class="navbar-toggle collapsed bs-1-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1 ">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
<a class="navbar-brand page-scroll scrl dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand-scroll.png"></a>
<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-8 col-sm-6 hm-pg-flp">
<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <div class="top_nav">
    <a href="/contact-us/" class="top_phone"><i class="fa fa-phone"> </i>888.315.6518</a>
    <?php wp_nav_menu( array('menu' => 'socialmenu' )); ?>

  
 </div>
 <div class="flipper">
<!-- <div class="front">
<span class="blue-txt "><a href="/contact-us/"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div> -->
<div class="back">
<span class="blue-txt"><a href="#"><i class="fa fa-pencil-square-o"></i> Request Quote</a> </span>
</div>
</div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>
  
</div>
<div class="flipper">
<!-- <div class="front">
<span class="blue-txt "><a href="/contact-us"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div> -->
<div class="back">
<span class="blue-txt"><a href="#"><i class="fa fa-pencil-square-o"></i> Request Quote</a> </span>
</div>
</div>
</div>
<button type="button" class="menu-rt  col-xs-5 " data-toggle="modal" data-target="#menuModal">Menu <span class="fa fa-bars " aria-hidden="true"></span></button>
</nav>
<!-- FULLSCREEN MODAL CODE (.fullscreen) -->

<div class="modal fade fullscreen" id="menuModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content" style="color:#fff;">
<div class="modal-header" style="border:0;">
<button type="button" class="close btn btn-link" data-dismiss="modal" aria-hidden="true" title="close">
<span class="cltext">  Close </span><i class="clsee" style="color:#fff;">x</i></button> 
<h4 class="modal-title text-center"><span class="sr-only">main navigation</span></h4>
</div>
<div class="menu masthead__menu mbl-main-navigation">
<div class="row m-main-menu">
<div class="click-video col-md-6 col-sm-6 col-xs-3 text-left">
<a href="/" title="DCW" class="pp-up-logo" style="padding:0px; border:none;">
<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand-scroll-blk.png"></a></div>
<div class="click-video col-md-4 col-sm-5 col-xs-6 text-right menu-flips hm-pg-flp mdl-view-menu">
<div class="flip-container">
<div class="flipper">
<div class="front">
<span class="blue-txt "><i class="fa fa-phone"></i> 888.315.6518</span>
</div>
<!-- <div class="back">
<span class="blue-txt"><a href="/contact-us">Get Started </a></span>
</div> -->
</div>
</div>
</div>	
</div>
<a title="DCW" href="/" class="menu__title hidden-md hidden-sm hme-cion-mobile"><i class="fa fa-home"></i></a>
<ul class="menu__list">
<hr class="lines2">  

<li class="menu__item">

<a href="/about-us" class="menu__title">Company</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/about-us" class="menu__link" id="Why us-menu">About Us</a></li>
<li><a href="/our-process" class="menu__link" id="Our-Process-menu">Our Process</a></li>
<li><a href="/partners" class="menu__link" id="Partners-menu">Partners</a></li>
<li><a href="/team" class="menu__link" id="Team-menu">Team</a></li>
<li><a href="/contact-us" class="menu__link" id="Contact-us">Contact Us</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/services" class="menu__title">Services</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/ecommerce" class="menu__link" id="eCommerce-menu">eCommerce</a></li>
<li><a href="/web-applications" class="menu__link" id="Web-Application-menu">Web Applications</a></li>
<li><a href="/responsive-web-design" class="menu__link" id="Responsive-Design-menu">Responsive Design</a></li>
<li><a href="/seo-sem" class="menu__link" id="SEO/SEM-menu">SEO/SEM</a></li>
<li><a href="/mobile-application-mobile-website-development" class="menu__link" id="Mobile-Application-menu">Mobile Applications</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/work" class="menu__title">Work</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/work" class="menu__link" id="Portfolio-menu">Portfolio</a></li>
<li><a href="/case-studies" class="menu__link" id="Case-Study-menu">Case Studies</a></li>
<li><a href="/journey-map" class="menu__link" id="Journey-Map-menu">Journey Map</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/social-hub" class="menu__title">DCW Social</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/social-hub" class="menu__link" id="Social-Hub-menu">Social Hub</a></li>
<li><a href="/blog" class="menu__link" id="Blog-menu">Blog</a></li>
<li><a href="/press-releases" class="menu__link" id="PR-menu">Press Releases</a></li>
<li><a href="/video-testimonials" class="menu__link" id="Video-Testimonials-menu">Video Testimonials</a></li>
</ul>
</li>

<li class="menu__item lst-li">
<a href="/faq" class="menu__title">Resources</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/faq" class="menu__link" id="Social-Hub-menu">FAQs</a></li>
<li><a href="/kb" class="menu__link" id="PR-menu">Knowledge Base</a></li>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod == "IN")
{
?>
<li><a href="/careers" class="menu__link" id="Blog-menu">Careers</a></li>
<li><a href="/events" class="menu__link" id="Video-Testimonials-menu">Events</a></li>
<?php }?>
</ul>
</li>

<div class="row video-links-menu clearfix">
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983974" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/menu_1-1-img.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play2 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983975" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/3c.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play3 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983005" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div> 
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/menu_2img.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
</div>	
</ul>

</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.fullscreen -->
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" id="play-button" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>
<style>

header{height: 75%;}
@media only screen and (min-width:768px) and (max-width: 1024px){
.pattern-overlay iframe{top:0px !important;}	
header{height:auto;}
 }
@media(max-width:767px) {
.header-content-inner.hm_pg_cnt{top:30%;}header{height:auto;}
}

</style>