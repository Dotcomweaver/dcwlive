<?php
/**
 * The template part for displaying results in search pages
 *
  */
get_header('portfoliowork');
?>
<div class="item" id="post-<?php the_ID(); ?>">
<div class="blog-box-new">
<div class="blog-box-new-inner">
<div class="col-sm-4">
<div class="blog-img">
<?php
if( ! post_password_required() && ! is_attachment()):
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
if(!empty($feat_image)):
?>
	<img src="<?php echo $feat_image; ?>" class="img-responsive" />
<?php
endif;
endif;
?>
</div>
</div>
<div class="col-sm-8">
<div class="post-details">
<?php 
if ( comments_open() && ! post_password_required() ) :
//echo 'First If';

	if ( 'post' == get_post_type() ) :
	//echo 'second if'; ?>
        
            		<!-- <span class="icomoon-icon-user"></span><?php //the_author();?> -->
			<div class="post-meta">
			<?php 
			//query_posts( 'posts_per_page=6&orderby=rand' );
	        	$sposttags = get_the_tags($post_id);
			if($sposttags): ?>
		        <?php 
		        $i= 0;
					foreach($sposttags as $stags){
		        
		     if($i>0){
		     	echo '|';
		     }
		     echo '<a href="' . get_tag_link ($stags->term_id) . '" rel="tag">' . $stags->name . '  </a>';
		     	$i++;
		         } ?> |
	        <?php endif; ?>  <?php the_date('M j, Y'); ?>
	        </div>	
<?php endif; 
endif; ?>
<div id="container-blog">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<h2><a href="<?php echo the_permalink();?>"><?php echo the_title();?></a></h2>
	<?php //twentyfifteen_post_thumbnail(); ?>

		<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	

</article><!-- #post-## -->
</div>
</div>
</div>
</div>
</div>
</div>

