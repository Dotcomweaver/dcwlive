<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta property="og:image" content="<?php bloginfo('template_url');?>/img/brand-scroll.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<meta name="viewport" content="width=device-width">
<title><?php wp_title(''); ?></title>
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" type="text/css">
<!-- Custom Fonts -->
<link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,300,600,700|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="<?php bloginfo('template_url');?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php bloginfo('template_url');?>/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/creative.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/fontsawesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/style.min.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Raleway:300,400,500,600,700" rel="stylesheet">
<!-- Plugin CSS -->
<!-- <link rel="stylesheet" href="<?php //bloginfo('template_url');?>/css/animate.min.css" type="text/css">-->
<!-- Custom CSS -->

<!--[endif]-->
<?php wp_head(); ?>
<style>
header{background:none;}
.inner-left-side{padding-top: 0px;}
.affixed-top {margin-top: 100px;}
</style>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</head>
<body id="page-top" class="inr-head">
    
<nav id="mainNav" class="opacity-inner navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top">
  <div class="topcallreq_sec">
  <div class="top_call">
  
    <a href="tel:8883156518" class="top_phone">CALL NOW</a>
 </div>
 <div class="top_rq">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
<div class="clearfix"> </div>
</div>
<div class="navbar-header col-md-4 col-sm-4 col-xs-7">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll scrl-main dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>

<!-- <a class="navbar-brand page-scroll scrl dsk-logo" href="/" title="DCW"><img class="img-responsive" src="<?php //bloginfo('template_url'); ?>/img/brand-scroll.png"></a> -->

<a class="navbar-brand page-scroll mbl-logo" href="/" title="DCW"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a>
</div>
<div class="flip-container col-md-8 col-sm-6 hm-pg-flp">
<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   <div class="top_nav">
    <a href="/contact-us/" class="top_phone"><i class="fa fa-phone"> </i>888.315.6518</a>
    <?php wp_nav_menu( array('menu' => 'socialmenu' )); ?>

  
 </div>
 <div class="flipper">
<!-- <div class="front">
<span class="blue-txt "><a href="/contact-us/"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div> -->
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
</div>
  <?php wp_nav_menu( array('menu' => 'mainmenu', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>' )); ?>
  
</div>
<div class="flipper">
<!-- <div class="front">
<span class="blue-txt "><a href="/contact-us"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div> -->
<div class="back">
<span class="blue-txt-btn"><a href="/contact-us"> Request Quote</a> </span>
</div>
</div>
</div>
<button type="button" class="menu-rt col-md-2 col-xs-7" data-toggle="modal" data-target="#menuModal">Menu <span class="fa fa-bars " aria-hidden="true"></span></button>
</nav>
<!-- FULLSCREEN MODAL CODE (.fullscreen) -->

<div class="modal fade fullscreen" id="menuModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content" style="color:#fff;">
<div class="modal-header" style="border:0;">
<button type="button" class="close btn btn-link" data-dismiss="modal" aria-hidden="true">
<span class="cltext">  Close </span><i class="clsee" style="color:#fff;">x</i></button> 
<h4 class="modal-title text-center"><span class="sr-only">main navigation</span></h4>
</div>
<div class="menu masthead__menu mbl-main-navigation">
<div class="row m-main-menu">
<div class="click-video col-md-6 col-sm-6 col-xs-3 text-left">
<a href="/" title="DCW" class="pp-up-logo" style="padding:0px; border:none;">
<img class="img-responsive" src="<?php bloginfo('template_url'); ?>/img/brand.png"></a></div>
<div class="click-video col-md-4 col-sm-5 col-xs-6 text-right menu-flips hm-pg-flp mdl-view-menu">
<div class="flip-container">
<div class="flipper">
<div class="front">
<span class="blue-txt "><a href="/contact-us"><i class="fa fa-phone"></i> 888.315.6518</a></span>
</div>
<!-- <div class="back">
<span class="blue-txt"><a href="/contact-us">Get Started </a></span>
</div> -->
</div>
</div>
</div>	
</div>
<a title="DCW" href="/" class="menu__title hidden-md hidden-sm hme-cion-mobile"><i class="fa fa-home"></i></a>
<ul class="menu__list">
<hr class="lines2">  
<li class="menu__item">
<a href="/about-us" class="menu__title">Company</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/about-us" class="menu__link" id="Why us-menu">About Us</a></li>
<li><a href="/our-process" class="menu__link" id="Our-Process-menu">Our Process</a></li>
<li><a href="/partners" class="menu__link" id="Partners-menu">Partners</a></li>
<li><a href="/team" class="menu__link" id="Team-menu">Team</a></li>
<li><a href="/contact-us" class="menu__link" id="Contact-us">Contact Us</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/services" class="menu__title">Services</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/ecommerce" class="menu__link" id="eCommerce-menu">eCommerce</a>
<span class="accordin_sbmenu fa fa-angle-down"></span>
    <ul class="sub-menu">
      <li id="menu-item-6597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6597"><a href="/b2b-solutions/">B2B Solutions</a></li>
      <li id="menu-item-6596" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6596"><a href="/magento-development/">Magento Development</a></li>
      <li id="menu-item-6595" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6595"><a href="/strategy-consultation/">Strategy & Consultation</a></li>
      <li id="menu-item-6594" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6594"><a href="/ecommerce-seo/">eCommerce SEO</a></li>
      <li id="menu-item-6593" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6593"><a href="/data-management/">Data Management</a></li>
    </ul>
</li>
<li><a href="/web-applications" class="menu__link" id="Web-Application-menu">Web Applications</a></li>
<li><a href="/responsive-web-design" class="menu__link" id="Responsive-Design-menu">Responsive Design</a></li>
<li><a href="/seo-sem" class="menu__link" id="SEO/SEM-menu">SEO/SEM</a></li>
<li><a href="/mobile-application-mobile-website-development" class="menu__link" id="Mobile-Application-menu">Mobile Applications</a></li>
<li><a href="/additional-services/" class="menu__link" id="additional-services-menu">Additional Services</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/work" class="menu__title">Work</a>
<ul class="menu__submenu" style="display: block;">
<!--                                 <li><a target="_blank" href="#" class="menu__link" id="Featured-menu">Featured</a></li>-->
<li><a href="/work" class="menu__link" id="Portfolio-menu">Portfolio</a></li>
<li><a href="/case-studies" class="menu__link" id="Case-Study-menu">Case Studies</a></li>
<li><a href="/journey-map" class="menu__link" id="Journey-Map-menu">Journey Map</a></li>
</ul>
</li>

<li class="menu__item">
<a href="/social-hub" class="menu__title">DCW Social</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/social-hub" class="menu__link" id="Social-Hub-menu">Social Hub</a></li>
<li><a href="/blog" class="menu__link" id="Blog-menu">Blog</a></li>
<li><a href="/tag/press-awards/" class="menu__link" id="PR-menu">Press Releases</a></li>
<li><a href="/video-testimonials" class="menu__link" id="Video-Testimonials-menu">Video Testimonials</a></li>
</ul>
</li>
<li class="menu__item">
<a href="/faq" class="menu__title">Resources</a>
<ul class="menu__submenu" style="display: block;">
<li><a href="/faq" class="menu__link" id="Social-Hub-menu">FAQs</a></li>
<li><a href="/kb" class="menu__link" id="PR-menu">Knowledge Base</a></li>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod == "IN")
{
?>
<li><a href="/careers" class="menu__link" id="Blog-menu">Careers</a></li>
<li><a href="/events" class="menu__link" id="Video-Testimonials-menu">Events</a></li>
<?php }?>
</ul>
</li>
<div class="row video-links-menu clearfix">
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983974" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/menu_1-1-img.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play2 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983975" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/3c.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
<a class="vt_play3 btn btn-default dropdown-toggle" href="javascript:void();" rel="https://player.vimeo.com/video/109983005" data-target="#vt_modal" data-toggle="modal">
<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
<img class="img-responsive" alt="Ideas to Implementation Webinar" src="<?php bloginfo('template_url'); ?>/img/menu_2img.jpg" style="width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto;"></a>
</div>
</div>	
</ul>
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.fullscreen -->
<header>
<div class="portfolio-bnr">
<div class="header-content">
<div class="header-content-inner">
<h1>We are proud of our work</h1>
<p></p>
</div>
</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/client-cion.png"></div>
</header>
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="videoWrapper"></div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
  var jQ = jQuery.noConflict();
     jQ(document).ready(function(){
      if(jQ(window).width() <= 1024){
        
      jQ(".accordin_sbmenu").click(function(){

        jQ(".sub-menu").toggle("fast");
        jQ(".accordin_sbmenu").toggleClass("fa-angle-down fa-angle-up");

      });

      jQ(document).click(function (e) {
        if (!jQ(e.target).hasClass("accordin_sbmenu") 
         && jQ(e.target).parents(".sub-menu").length === 0) {
          jQ(".sub-menu").hide();
          }
        });
      }   
});

</script>