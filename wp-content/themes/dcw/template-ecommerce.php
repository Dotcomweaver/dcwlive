<?php
/*
Template Name: ecommerce
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_ecommerce.png"></div>

<div class="container">
<div class="ecmmorce-iner clearfix">
                     
    <div class="col-lg-6 col-sm-6 col-xs-12 text-right">
    <img class="img-responsive" src="/wp-content/themes/dcw/img/tabs-right-img.png">
    </div>
	<div class="col-lg-6 col-sm-6 col-xs-12">
     <h3>ONLINE STOREFRONTS TO ENGAGE AND DELIGHT YOUR CUSTOMERS</h3>
     <p>You're a savvy online retailer. Your goal: to meet the needs of your customer. Creating a user experience that makes consumer lives easier is the key to converting them from casual browsers to loyal customers. It is also the key to seeing your business grow. We dig deep to understand the needs of your customers. Then spanning architecture, functionality and navigation, we hit all the touch points that make for a meaningful user experience-one that will foster your connection with your customer and result in the success of your online store.</p>

     </div>
</div>
 </div>
<?php echo do_shortcode('[jumptoservicesfolio]');?>

<section id="ecmrce-blg-slider clearfix">
	<div class="container content-headings">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="emcre-cnt">
			<h3>PRODUCT CATALOG</h3>
			<p>A well-developed product catalog is important to the success of your business. Our custom catalogs are flexible enough to accommodate multilevel product categories, customizable product layouts and the option to add new products as your business grows. We can also implement a tier-based pricing function, which increases your agility to incentivize your best customers to buy from you again.</p>
			</div>
			<div class="emcre-cnt">
			<h3>THIRD PARTY SHOPPING CART INTEGRATIONS</h3>
			<p>We integrate your shopping cart with many third party applications to automate your shipping rates and compute sales tax to speed your customers through checkout. What's more, email links to abandoned shopping carts help you recover lost sales..</p>
			</div>
			<div class="emcre-cnt">
			<h3>ERP INTEGRATION</h3>
			<p>We integrate best-in-breed warehouse management systems like MAS 200, Microsoft Nav, StoneEdge and Quickbooks to improve your finance management, automated order fulfillment and customer service and incorporate stovepipe systems that help you satisfy diverse customer demands.</p>
			</div>
			<div class="emcre-cnt">
			<h3>SECURE CHECKOUT</h3>
			<p>Our sites include multiple payment gateways like Paypal, Google Wallet, Authorize.net, First data, BrainTree and Stripe; plus, encrypted credit card and order data capabilities keep your customers' personal information safe.</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="place-holder">
			<a href="javascript:void();" data-target="#vt_modal" data-toggle="modal" rel="https://player.vimeo.com/video/109983005" class="vt_play">
				<div class="videos-hover-color"><i class="flaticon-play-button4"></i></div>
                 <img src="/wp-content/themes/dcw/img/menu_2img.jpg" alt="video" class="img-responsive">
				</a>
			</div>
                       <h2>Related Blog</h2>
                      <?php
                    $args=array(
                      'tag' => 'ecommerce-3',
                      'showposts'=>2,
                      'orderby' => 'date',
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                    
                      while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="place-holder">
			<p><?php $content = $content = wp_trim_words(get_the_content(), 35); echo $content; ?>
			<span><a href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-long-arrow-right"></i></a></span>
			</p>
			</div>
                       <?php
                      endwhile;
                    } //if ($my_query)
                  wp_reset_query();  // Restore global post data stomped by the_post().
                ?>
		</div>
	</div>


</section>
<section class="video-testmonilas">
	<div class="container">
		<div class="row">
<!--			<div class="col-md-3 col-sm-3 col-xs-12">
				<p><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/ecmrc_video.jpg"></p>
			</div>-->
			<div class="col-md-9 col-sm-9 col-xs-12">
                            <?php echo do_shortcode('[jumptoservicesvt]');?>
                <!--	<span class="cma-left"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-left.png"></span>
				<p>his section of text is up to you. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
				<span>- First Name Last Name, President of Battlefield Collection</span>
				</p>
				<span class="cma-right"><img class="img-responsive" src="http://dotcomweavers.staging.wpengine.com/wp-content/themes/dcw/img/cama-right.png"></span>-->
			</div>
		</div>
	</div>
</section>

<section class="ecmrce-accordians">

	<div class = "container">

  <h3>More Questions about eCommerce</h3>
	<?php echo do_shortcode('[jumptoservicesfaqs]');?>

  
	</div>
</section>


 
<?php get_footer(); ?>
