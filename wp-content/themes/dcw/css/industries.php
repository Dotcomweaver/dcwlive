<?php
/*
Template Name:industries
*/

get_header();
?>

 <div class="industrial-sub-menu">
  <div class="container">
    <div class="industrial-menu-list">
      <ul>
        <li> <a href="#"> Marketplaces</a> </li>
        <li> <a href="#"> Industrial Parts & Machinery</a></li>
        <li> <a href="#"> Food & Perishables </a> </li>
        <li> <a href="#"> B2B eCommerce </a> </li>
      </ul>
    </div>
  </div>
</div>

<!-- main banner page starts here -->

<div class="main-industrial-banner">
    <div class="container">
    <div class="col-sm-12">
    <div class="col-sm-5 col-xs-12 pull-right industries_main">
        <img src="<?php bloginfo('template_url'); ?>/images/Our Services Video Thumb.jpg">
      </div>
       <div class="col-sm-7 col-xs-12 pull-left">
        <h1>Industries for You</h1>
       <p><?php the_field('banner_content',9335);?></p>

      </div>
      </div>     
    </div>
  </div>
<!-- main banner page ends here -->



<!-- service html start here -->
<div class="clearfix"></div>
<div class="ind_service">
   <div class="container">
    <?php if( have_rows('industry_services') ): ?>

    <?php while( have_rows('industry_services') ): the_row(); 

    // vars
    $image = get_sub_field('image');
    $content = get_sub_field('content');
    $link = get_sub_field('url');
    $title = get_sub_field('title');

    ?>

    <div class="col-sm-3 col-xs-6 service_cl">
         <div class="service_ind">
          <a class="btn_ind" href="#">
            <div class="ser_img">
              <img class="img-responsive"  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            </div>
            <h4><?php echo $title;?></h4>
            <p class="serviece_sub"><?php echo $content;?></p>
            <a class="btn_rt_arr" href="<?php echo $link;?>"><i class="fas fa-long-arrow-alt-right"></i></a>
            </a>   
         </div>         
      </div>

  <?php endwhile; ?>

<?php endif; ?>
     
    
   </div>
</div>
<!-- service html end here -->
 
 <!--work together section-->
  <div class="work-together2 clearfix">
    <div class="sub-container">
      <div class="col-sm-12">
        <div class="col-sm-4 col-xs-12">
          <img src="<?php bloginfo('template_url'); ?>/images/LetsWork.png">
        </div>

        <div class="col-sm-8 col-xs-12">
          <h1>Let's Work Together.</h1>
          <div class="work-together-content2">
            <p><?php the_field('work_together',9335);?></p>
       <p class="touch"> <a href="/contact-us/">  Let’s Talk! </a></p>
          </div>
        </div>
      </div>
    </div>
  </div>







<?php
get_footer(); ?> 
</html>


