<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="footer-contact">
    <div class="container ft-map-links">
        <div class="col-lg-4 col-sm-6 col-xs-12  itemscope="" itemtype="http://schema.org/Organization"">
<ul class="contact" itemscope="" itemtype="http://schema.org/Organization">
<meta itemprop="name" content="Dotcomweavers">
<li itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="col-sm-6 col-lg-6"><a href="https://www.google.com/maps/place/15+Farview+Terrace,+Paramus,+NJ+07652/@40.9227811,-74.0704101,17z/data=!3m1!4b1!4m2!3m1!1s0x89c2fa56876546e3:0xd2083047019d2f42"><span itemprop="streetAddress">15 Farview Terrace</span><br>
<span itemprop="addressLocality">Paramus</span>, <span itemprop="addressRegion">NJ</span> <span itemprop="postalCode">07652</span></a><br><a href="https://www.google.com/maps/place/15+Farview+Terrace,+Paramus,+NJ+07652/@40.9227811,-74.0704101,17z/data=!3m1!4b1!4m2!3m1!1s0x89c2fa56876546e3:0xd2083047019d2f42" class="getAdonMap" target="_blank"><span>Get Directions</span></a>
</li>
<li class="col-sm-6 col-lg-6"><span itemprop="telephone">(888) 315-6518</span><br>
(201) 880-6656</li>
<meta itemprop="url" content="www.dotcomweavers.com">
<li><a href="mailto:info@dotcomweavers.com">info@dotcomweavers.com</a></li>
</ul>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-12 socials ">
            <h2>Stay connected</h2>
            <ul class="social-network social-circle">

                <li><a title="Facebook" class="icoFacebook" target="_blank" href="http://www.facebook.com/dotcomweavers"><i class="fa fa-facebook"></i></a></li>
                <li><a title="Twitter" class="icoTwitter" target="_blank" href="http://twitter.com/Dotcomweavers"><i class="fa fa-twitter"></i></a></li>
                <li><a title="Linkedin" class="icoLinkedin" target="_blank" href="http://www.linkedin.com/company/dotcomweavers-inc"><i class="fa fa-linkedin"></i></a></li>
                <li><a title="Google +" class="icoGoogle" target="_blank" href="https://plus.google.com/+Dotcomweavers"><i class="fa fa-google-plus"></i></a></li>
                <li><a title="Rss" class="icoRss" target="_blank" href="#"><i class="fa fa-rss"></i></a></li>
                <li><a title="Rss" class="icoRss" target="_blank" href="#"><i class="fa fa-youtube-square"></i></a></li>


            </ul>		

        </div>
        <div class="col-lg-5 col-sm-12 col-xs-12 ft-submit ">
            <h2>STAY UP TO DATE WITH LATEST NEWS</h2>
            <div class="input-group">
                <input type="text" placeholder="Email" class="form-control">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default">Submit</button>
                </span>
            </div><!-- /input-group -->
        </div>

    </div>


</div>
    <div class="footer">
		<div class="container">
		<div class="col-lg-6 col-sm-6 col-xs-12">
			<p>Services | Work | Company | Blog | Social Hub | FAQs</p>
		</div>
		<div class="col-lg-6 col-sm-6 col-xs-12 text-right mbl-rts">
			<?php dynamic_sidebar('copyright');?>
		</div>
	</div>
	</div>
    <!-- jQuery -->
    <script src="<?php bloginfo('template_url');?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php bloginfo('template_url');?>/js/jquery.easing.min.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/jquery.fittext.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php bloginfo('template_url');?>/js/creative.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script>
	var j = jQuery.noConflict();
	j(document).ready(function($) {
  var owl = $("#owl-demo");
 
  owl.owlCarousel({
	 autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 7, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  });
 
  // Custom Navigation Events
  j(".next").click(function(){
    owl.trigger('owl.next');
  })
  j(".prev").click(function(){
    owl.trigger('owl.prev');
  })
  j(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })
  j(".stop").click(function(){
    owl.trigger('owl.stop');
  })
 
});
	</script>

<?php wp_footer(); ?>

</body>
</html>
