<?php
/*
Template Name: aboutarabic
*/
get_header('arabic');?>
<!--kalam test-->
<div class="work clearfix">
    
    <?php// if ( function_exists('yoast_breadcrumb') ) {
								//yoast_breadcrumb('<ol class="breadcrumb">','</ol>');
					//} ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url')?>/img/heading_icon_dcw.png"></div>

<section class="devide">
<div class="container">
	<div class="vline"></div>
        <div class="who-we-are">
	<?php dynamic_sidebar('whoweare');?>
        </div>
	<div class="clearfix"></div>

 	<div class="vline"></div>
        <div class="what-we-do">
	<?php dynamic_sidebar('whatwedo');?>
        </div>

 	<div class="clearfix"></div>

 	<div class="vline"></div>
        <div class="how-we-work">
	<?php dynamic_sidebar('howwework');?>
</div>
 	<div class="clearfix"></div>
</div>
</section>
<section class="devide">
<div class="container">
 	<div class="vline"></div>
        <div class="humble">
<?php dynamic_sidebar('humblebeginings');?>
</div>
	<div class="clearfix"></div>
	<div class="header-group">
       
          <div class="triangleup">
  		<a href="/our-process/">
			احصل على نظرة أعمق.
			<span class="sub_subtitle"><b>شاهد عملياتنا</b></span>
  		</a>
	</div>
      	</div>

</div>
</section>


 
<?php get_footer('arabic'); ?>
