<?php
/**
* The template for displaying the footer
*
* Contains the closing of the "site-content" div and all content after.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?>
</div>
<div class="skinnyftr"><span style="color:#fff">Meet our team at the <a target="blank" href="http://www.irce.com/">IRCE</a> in <span style="color:#F78260">Chicago, </span> <span style="color:#F78260">June 7th - 10th!</span></span></div>
<div class="sitefooter" id="mailchimpthankyou">  
<div class="total-footer">
<div class="footer-contact">
<div class="container ft-map-links">
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 socials">
<h2>Contact Us</h2>
<ul class="contact" itemscope="" itemtype="http://schema.org/Organization">
<meta itemprop="name" content="Dotcomweavers">
<li itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="col-sm-6 col-lg-6"><a target="blank" href="https://www.google.com/maps/place/15+Farview+Terrace,+Paramus,+NJ+07652/@40.9227811,-74.0704101,17z/data=!3m1!4b1!4m2!3m1!1s0x89c2fa56876546e3:0xd2083047019d2f42"><span itemprop="streetAddress">15 Farview Terrace</span><br>
<span itemprop="addressLocality">Paramus</span>, <span itemprop="addressRegion">NJ</span> <span itemprop="postalCode">07652</span></a><br><a href="https://www.google.com/maps/place/15+Farview+Terrace,+Paramus,+NJ+07652/@40.9227811,-74.0704101,17z/data=!3m1!4b1!4m2!3m1!1s0x89c2fa56876546e3:0xd2083047019d2f42" class="getAdonMap" target="_blank"><span>Get Directions</span></a>
</li>
<li class="col-sm-6 col-lg-6"><span itemprop="telephone">888.315.6518</span><br>
201.880.6656
<span><a href="mailto:info@dotcomweavers.com">info@dotcomweavers.com</a></span></li>
<meta itemprop="url" content="www.dotcomweavers.com">
<li></li>
</ul>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 socials stay-cnt">
<h2>Stay connected</h2>
<ul class="social-network social-circle">
<li><a title="Facebook" class="icoFacebook" target="_blank" href="http://www.facebook.com/dotcomweavers"><i class="fa fa-facebook"></i></a></li>
<li><a title="Twitter" class="icoTwitter" target="_blank" href="http://twitter.com/Dotcomweavers"><i class="fa fa-twitter"></i></a></li>
<li><a title="Linkedin" class="icoLinkedin" target="_blank" href="http://www.linkedin.com/company/dotcomweavers-inc"><i class="fa fa-linkedin"></i></a></li>
<li><a title="Google +" class="icoGoogle" target="_blank" href="https://plus.google.com/+Dotcomweavers"><i class="fa fa-google-plus"></i></a></li>
<li><a title="Youtube" class="icoRss" target="_blank" href="https://www.youtube.com/user/dotcomweavers"><i class="fa fa-youtube-square"></i></a></li>
</ul>
</div>
<div class="col-lg-12 col-sm-12 col-xs-12 ft-submit ">
<h2>STAY UP TO DATE WITH THE LATEST NEWS</h2>
<div class="input-group">
<?php dynamic_sidebar('newsletter');?>
</div><!-- /input-group -->
</div>
</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 revws">
<?php dynamic_sidebar('reviews');?>
</div>
</div>
</div>
<div class="footer">
<div class="container">
<div class="col-lg-6 col-sm-12 col-xs-12 ft-linss">
<p><a href="/services/">Services</a> | <a href="/work/">Work</a> | <a href="/about-us/">Company</a> | <a href="/blog/">Blog </a>| <a href="/social-hub/">Social Hub</a> | <a href="/sitemap/">Sitemap</a></p>
</div>
<div class="col-lg-6 col-sm-6 col-xs-12 text-right mbl-rts ">
<?php dynamic_sidebar('copyright');?>
</div>
</div>
</div>
</div>
</div>


<?php wp_footer(); ?>
<!-- jQuery -->
<!-- Bootstrap Core JavaScript -->
<script src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/custom.js"></script>


<!-- Plugin JavaScript -->

<!--<script src="<?php //bloginfo('template_url');?>/js/jquery.easing.min.js"></script>-->
<script src="<?php bloginfo('template_url');?>/js/jquery.fittext.js"></script>
<script src="<?php bloginfo('template_url');?>/js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php bloginfo('template_url');?>/js/creative.js"></script>

<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function () {
size_li = j("#mylist>li").size();
x=12;
j('#mylist>li:lt('+x+')').show();
j('#loadMore').click(function () {
    j(this).addClass('hidden');
// x= (x+8 <= size_li) ? x+8 : size_li;
x=j('#rowcount').val();
j('#mylist>li:lt('+x+')').show();
});

j('#showLess').click(function () {
x=(x-8<0) ? 8 : x-8;
j('#mylist>li').not(':lt('+x+')').hide();
});
//alert("Hello5");	
/* var iframe = $('#home_video')[0];
var player = $f(iframe);
j("#play").on('click', function () {
//alert("hiiii");
// player.api('pause');

}); */
//alert("Hello");	
});




</script>
<script>
/* var j = jQuery.noConflict();
j(document).ready(function () { 
	j( ".sidebar" ).addClass( "affix1" );
	}); */

</script> 

<script>
var j = jQuery.noConflict();
j(document).ready(function() {
	/*j('.sidebar').addClass( "affix-top1" );
	j('.sidebar').stickySidebar({
		sidebarTopMargin: 100,
		footerThreshold: 100
	});*/

});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>

 
<script type="text/javascript">

              jQuery.noConflict();
              jQuery('document').ready(function()
             {  
			  jQuery.validator.addMethod(
			   
								"regex",
								function(value, element, regexp) 
								{
									if (regexp.constructor != RegExp)
										regexp = new RegExp(regexp);
									else if (regexp.global)
										regexp.lastIndex = 0;
									return this.optional(element) || regexp.test(value);
								},
								"Please check your input."
						);
 
			 
                     jQuery("#request-form,#request-form1").validate({
                         rules: {
                             "lead[full_name]": "required", 
                             "lead[company_name]":"required", 
                              "lead[phone]": { 
                                  required:true, 
                                  regex: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/,
                                },
                             "lead[email]": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                 //email: true
                             }, 
                         },
                         messages: {
                             "lead[full_name]": "Please enter your Name", 
                             "lead[company_name]": "Please enter your Company Name", 
                             "lead[phone]": "Please enter Valid Phone number", 
                              "lead[email]": "Please enter a valid email address"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     });  
                     jQuery("#register-form").validate({
                         rules: {
                             "lead[full_name]": "required", 
                             "lead[company_name]":"required", 
                              "lead[phone]": { 
                                  required:true, 
                                  regex: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/,
                                },
                             "lead[email]": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                 //email: true
                             }, 
                         },
                         messages: {
                             "lead[full_name]": "Please enter your Name", 
                             "lead[company_name]": "Please enter your Company Name", 
                             "lead[phone]": "Please enter Valid Phone number", 
                              "lead[email]": "Please enter a valid email address"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     }); 

         });
</script>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod != "IN")
{
?>
<a title="Real Time Web Analytics" href="http://clicky.com/100706936"><img alt="Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100706936); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100706936ns.gif" /></p></noscript>
<?php }?>
</body>
</html>
