<?php
/*
Template Name: The Template for displaying all whitepapers posts.
*/
get_header();?>
<?php $banner = get_field('banner');?>
<div class="work clearfix whitepapers_singlebanner" style="background-image: url(<?php echo $banner['url']; ?> );">
          <div class="header-content">
        <div class="header-content-inner">
         <h1><?php echo the_title();?></h1>
          <p></p>
        </div>
      </div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/whitepaper_innericon.png"></div>
<div class="clearfix"></div>

       
<section class="devide whitepaper_singlepage">
  <div class="container">
    <div class="row">
        <div class="col-md-8">
          <div class="whppsleft">
         
          <?php 
if ( have_posts() ) {
  while ( have_posts() ) {
    the_post(); 
    the_content();
  } // end while
} // end if
?>
            </div>

        <?php echo the_field('info');?>


        </div>
        <div class="col-md-4">
          <div class="whpp_downlaod_sec">

        <span>We are here to help your business grow.</span>
  <p class="whpp_mailph"><i class="fa fa-envelope"></i>info@dotcomweavers.com  <span></span>  <i class="fa fa-phone"> </i> 201.880.6656  </p>

 <form class="cnt-frm " method="post" id="register-form" action="https://www.pipelinedeals.com/web_lead">

    <input type="hidden" name="w2lid" value="212f7ab56299" /> 
   
     <input type="hidden" name="thank_you_page" value="https://www.dotcomweavers.com/thank-you-2/" /> 

    <div class="form-group has-feedback">
     <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
  </div>
    <div class="form-group">
    <input type="text" name="lead[company_name]"class="form-control"  placeholder="Company">
    </div>
        <div class="form-group has-feedback">
    <input type="text" name="lead[phone]"class="form-control" placeholder="Phone*" required>
  </div>
  <div class="form-group has-feedback">
    <input type="email" name="lead[email]"class="form-control"  placeholder="Email*" required>  </div>
    <div class="form-group">
    <input type="hidden" name="lead[work_country]"class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >  </div>
     <div class="form-group">
   <select name="lead[custom_fields][1464592]" class="form-control">
  <option value="">How did you hear about us ?</option>
  <option value="1952542">10 Best Design</option>
  <option value="1952545">Clutch</option>
  <option value="1952551">Conferences</option>
  <option value="1952548">Online Search</option>
  <option value="1952554">Publications </option>
  <option value="1952557">Social Media</option>
  <option value="1952560">Referral</option>
  <option value="1952563">Other</option>
</select> 
    </div>
  <div class="form-group">
    <input type="text" name="lead[summary]" class="form-control cmnts" placeholder="Comments">  </div>
    
    
<div class="form-group snd">
 
<?php 

$file = get_field('attachment');
//echo $file;

if( $file ): ?>
  
   <input type="hidden" name="thank_you_page" value="https://www.dotcomweavers.com/thank-you-2/?f=<?php echo base64_encode($file); ?>" />
  <button type="submit" class="btn btn-warning col-sm-12 cpl-xs-12"> Download </button>

<?php endif; ?>
</div>
   <!--  <p>Thank You! You will hear from us within two business days.</p> -->

    </form>

        </div>
        </div>
    </div>
  </div>


</section>
<!-- <div class="triangleup"><a href="/contact-us/">
Need To Talk To Us?
<span class="sub_subtitle">Contact Us</span>
</a></div> -->
 
<?php get_footer(); ?>
