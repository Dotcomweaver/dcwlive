jQuery.noConflict();
jQuery(document).ready(function($) {
    // filter buttons
	$(".online_shopping").hide();
	$(".webdevelop").hide();
	$(".seo").hide();
	$(".show_cat").show();
	$('.sidenav').click(function(){ 
		var rel = $(this).attr('rel');
		if(rel=='showall'){
			$(".show_cat").show().animate(600);
			$(".online_shopping").hide();
			$(".webdevelop").hide();
			$(".seo").hide();
			
			$(".Online-Shopping-Carts").removeClass('active');
			$(".Web-Development-and-design").removeClass('active');
			$(".Search-Engine-Marketing").removeClass('active');
			$(".show_all").addClass('active');
		}else if(rel=='Online-Shopping-Carts'){
			$(".online_shopping").show().animate(600);
			$(".webdevelop").hide();
			$(".seo").hide();
			$(".show_cat").hide();
			
			$(".Online-Shopping-Carts").addClass('active');
			$(".Web-Development-and-design").removeClass('active');
			$(".Search-Engine-Marketing").removeClass('active');
			$(".show_all").removeClass('active');
		}else if(rel=='Web-Development-and-design'){
			$(".webdevelop").show().animate(600);
			$(".online_shopping").hide();
			$(".seo").hide();
			$(".show_cat").hide();
			
			$(".Online-Shopping-Carts").removeClass('active');
			$(".Web-Development-and-design").addClass('active');
			$(".Search-Engine-Marketing").removeClass('active');
			$(".show_all").removeClass('active');
		}else if(rel=='Search-Engine-Marketing'){
			$(".seo").show().animate(600);
			$(".online_shopping").hide();
			$(".webdevelop").hide();
			$(".show_cat").hide();
			
			$(".Online-Shopping-Carts").removeClass('active');
			$(".Web-Development-and-design").removeClass('active');
			$(".Search-Engine-Marketing").addClass('active');
			$(".show_all").removeClass('active');
		}
	 });
		$(".portfolio_filter li a").click(function() {
		$('.portfolio_filter li a').removeClass('current');
		$(this).addClass('current');
		});
		$("#showall").click(function(){
		$('.portfolio_filter li a').removeClass('current');
		});
   });