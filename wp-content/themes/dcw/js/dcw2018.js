jQuery.noConflict();
jQuery('document').ready(function()
{  
  jQuery.validator.addMethod(
    
  "regex",
  function(value, element, regexp) 
  {
  if (regexp.constructor != RegExp)
  regexp = new RegExp(regexp);
  else if (regexp.global)
  regexp.lastIndex = 0;
  return this.optional(element) || regexp.test(value);
  },
  "Please check your input."
  );
   
   
  jQuery("#request-form,#request-form1").validate({
      rules: {
          "lead[full_name]": "required", 
          "lead[phone]": { 
              required:true,
          },
          "lead[email]": {
             required: true,
             regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
          },
           
      },
      messages: {
          "lead[full_name]": "Please enter your name*", 
          "lead[phone]": "Please enter a valid Phone number*", 
          "lead[email]": "Please enter a valid email address*"
      },
     submitHandler: function(form) {
         form.submit();
     }
  });  
  jQuery("#register-form").validate({
     ignore: ":hidden:not(#keycode)", 
     rules: {
          "full_name": "required", 
          "phone": { 
              required:true,
              regex: /^[0-9]{10,13}$/,  
                     },
          "email": {
             required: true,
             regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                         }, 
          "keycode": {
                  required: function() {
                    if(grecaptcha.getResponse() == '') {
                        return true;
                    } else { return false; }
                  } 
              } 
     },
     messages: {
          "full_name": "Please enter your name*", 
          "phone": "Please enter a valid phone number*", 
          "email": "Please enter a valid email address*"
     },
     submitHandler: function(form) {
          var olddata = document.getElementById("form_summery").value;
          olddata += ' Lead from Staging';
          document.getElementById("form_summery").value = olddata;
          form.submit();  
     } 
  });

});





var jQ = jQuery.noConflict();
jQ(document).ready(function(){
    jQ(".chat_message").click(function(){
        jQ(".olark-launch-button").click();
    });
});
/* Home page imported script start*/

if (jQ(window).width() > 1024) {
    var divHeight = jQ('.content_middle').height();
    jQ('.img_middle').css('height', divHeight+'px');
}

/*Banner image window size start*/
/*if (jQ(window).width() > 768) {
   jQ(".homban_carousel11").css("height", jQ(window).height()-120 + "px");
}*/
/*Banner image window size End*/

/*Banner on click come to next section start*/
if (jQ(window).width() > 1024) {
   jQ(".home_orangenewbaner1").click(function(){
          jQ('html, body').animate({
         scrollTop: jQ(".project_div").offset().top - 80
     }, 1000);
     })
}
/*Banner on click come to next section End*/

// jQ(".banner-list-item, li, a").click(function(){
//  event.stopPropagation();
// });

/*portfolio cross button start*/
jQ(".mobiel_portf li a").on('click', function (e) {
    e.preventDefault();

    var active_or_not = this.parentElement;
    if(active_or_not.className == "active" ){
        var kk =jQ(this.parentElement);
        kk.removeClass('active');
        return false;
    }
    jQ('.mobiel_portf li.active').removeClass('active');
    jQ(this).parent().toggleClass('active');      
    
});

jQ(".testing li label").on('change', function () {
    jQ(this).parent().toggleClass('active').siblings().removeClass('active');
    jQ(this).parent().siblings().removeClass('active');
});
/*portfolio cross button END*/

/*portfolio mobile filter section start*/
jQ('.filter_bt a').click(function(){
    jQ('body').addClass('filter_open');
});
jQ('.over_lay').click(function(){
    jQ('body').removeClass('filter_open');
});
jQ('.mobile-port').click(function(){
    jQ('body').removeClass('filter_open');
});
/*portfolio mobile filter section End*/

jQ(".work_moreless").click(function() {
    jQ('#da-thumbs').toggleClass("more");
    jQ('.work_moreless').toggleClass("more");
    jQ('.work_moreless').text(jQ('.work_moreless').text() == 'View Less' ? 'View All' : 'View Less');
});

jQ(".filter_moreless").click(function() {
    jQ('.filter-content').toggleClass("more");
    jQ('.filter_moreless').toggleClass("more");
    jQ('.filter_moreless').text(jQ('.filter_moreless').text() == 'View Less' ? 'View All' : 'View Less');
});
jQ(".work-nav #21, .work-nav #20, .work-nav #176, .work-nav #228").click(function(){
    jQ('.work_moreless').hide();
    jQ('.filter_moreless').show();
});
jQ(".work-nav #all").click(function(){
    jQ('.work_moreless').show();
    jQ('.filter_moreless').hide();
});


/*Awards section start*/
jQ(function(){

    jQ('.slider').slick({

        speed: 6000,
        autoplay: true,
        autoplaySpeed: 0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        prevArrow: false,
        nextArrow: false,
         initialSlide: 3,
         pauseOnHover:true,
         pauseOnDotsHover:true 

    });
});
/*Awards section End*/

/*inner page work section start*/
jQ('.owl-carousel_gal').owlCarousel({
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive : {
  0 : {
       loop:true,
        items:1,
        margin:5,
        stagePadding: 15,
        center:true,
        autoplay:true,
        autoplayTimeout:1000
          
    },
    // breakpoint from 480 up
    480 : {
        loop:true,
        items:1,
        margin:5,
        stagePadding:50,
        center:true,
        autoplay:true,
        autoplayTimeout:1000
        
    },
    768 : {
       loop:true,
        items:2,
        margin:5,
        stagePadding:110,
        center:true,
        smartSpeed:300,
        // nav:true
        autoplay:true,
        autoplayTimeout:1000
        //autoplayHoverPause:true,
        
    },
    // breakpoint from 768 up
    
    992 : {
        items:4,
        margin:5,
        stagePadding:5,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1024 : {
        items:2,
        margin:5,
        stagePadding:100,
       center:true,
       margin:10,
       autoplay:true,
       autoplayTimeout:1000
        
    },
    1200 : {
      items : 4,
       autoplay:false,
       autoplayTimeout:1000,
      autoplayHoverPause:true,
      touchDrag: false,
      mouseDrag: false
        
    }
    }
});

/*Client Feedback section start*/
jQ('.carousel-control.left').click(function() {
  jQ('#myCarousel').carousel('prev');
});

jQ('.carousel-control.right').click(function() {
  jQ('#myCarousel').carousel('next');
});
jQ('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    center: true,
    nav:true,
    stagePadding: 200,
    margin:100,
     nav:false,
     fluidSpeed:true,
    autoplay:true,
     autoWidth:true,
      responsiveClass:true,
      slideSpeed: 200,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});
/*Client Feedback section End*/

/*Work and Blog section carosel Start*/
 jQ('.owl-carousel_work').owlCarousel({
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive : {
 0 : {
       loop:true,
        items:1,
        margin:5,
        stagePadding: 15,
        center:true,
        autoPlay:true,
        autoplayTimeout:1000
          
    },
    // breakpoint from 480 up
    480 : {
        loop:true,
        items:1,
        margin:5,
        stagePadding:50,
        center:true,
        autoplay:true,
        autoplayTimeout:1000
        
    },
    768 : {
       loop:true,
        items:2,
        margin:5,
        stagePadding:5,
        center:true,
        smartSpeed:300,
        // nav:true
        autoplay:true,
        autoplayTimeout:1000
        //autoplayHoverPause:true,
        
    },
    // breakpoint from 768 up
    
    992 : {
        items:4,
        margin:5,
        stagePadding:5,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1024 : {
        items:2,
        margin:5,
        stagePadding:100,
       center:true,
       margin:10,
       autoPlay:true,
       autoplayTimeout:1000
        
    },
    1200 : {
      items : 4,
       autoplay:false,
       autoplayTimeout:1000,
      autoplayHoverPause:true,
      touchDrag: false,
      mouseDrag: false
        
    }
    }
});
/*Work and Blog section carosel End*/


/*Why us page script start*/
jQuery.noConflict();
 jQuery( ".thumbnail" ).hover(function() {
       ///alert('test');
    var replaceval = jQuery(this).children('img').attr("rel");
    var pfimage = jQuery(this).children('img').attr("src");
    jQuery(this).children('img').attr("rel",pfimage);
    jQuery(this).children('img').attr("src",replaceval);
   });
  jQuery('.next').click(function(){
      jQuery("#sliderId").slick('slickNext');
    });

  jQuery('.collab_carousel').slick({
           // dots: true,
           infinite: true,
           speed: 500,
           fade: true,                                                                                                                                               
           cssEase: 'linear'
         });
   if (jQuery(window).width() > 1025) {
            
           jQuery('.collab_carousel_one').slick({
           infinite: true,
           speed: 500,
            asNavFor: '.collab_carousel_two',
           fade: true                                                                                              
         });

        jQuery('.collab_carousel_two').slick({
            infinite: true,
            nav:false,
            slidesToShow: 2,
            asNavFor: '.collab_carousel_one',
              responsive: [
               {
                 breakpoint: 768,
                 settings: {
                   slidesToShow: 1,
                   settings: "unslick",
                   slidesToScroll: 1
                 }
               }
               // You can unslick at a given breakpoint now by adding:
               // settings: "unslick"
               // instead of a settings object
             ]

  });
}

 /* Why us script end*/





/*percentage Scrolling starting*/
var docHeight = jQ(document).height(),
windowHeight = jQ(window).height(),
scrollPercent;

var test = docHeight / 4;
jQ(window).scroll(function() {
  var scrollPercent = parseInt(jQ(window).scrollTop() / (docHeight - windowHeight) * 100);
  jQ('.progressCounter').html(scrollPercent + '%');

});

jQ(document).scroll(function() {
    var y = jQ(this).scrollTop();
    if (y >= 667) {
      jQ('.progressCounter').css("display", "block");
    } else {
      jQ('.progressCounter').css("display", "none");
    }
});

jQ(document).ready(function(){
  jQ('.progressCounter').css("display", "none");
});
/*percentage Scrolling End*/

/*About section Video autoplay Start*/
// Load the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api?modestbranding=1";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;
function onYouTubePlayerAPIReady() {
player = new YT.Player('ytplayer', {
  // height: '360',
  // width: '640',
  // autoplay: 1,
  // controls:false,
  // showinfo:0,
  videoId: 'QDsQ-oz8D50',
   playerVars: {rel: 0, autoplay: 1,
  showinfo:0,fs:0}
});
}

jQ(window).scroll(function() {
    jQ("iframe").each( function() {
      //var h = $('.iframe_up').innerHeight()/6;
      //alert(h);
      var y = jQ('#yout');
      var d = jQ('.email_templates');
       if( jQ(window).scrollTop() > jQ(y).offset().top - 200 ) {
                    
            player.playVideo();
            player.mute();
        }

         else {
            //$(this).css('opacity',0);
              player.pauseVideo();
              player.mute();

        }
        if( jQ(window).scrollTop() > jQ(d).offset().top - 200 ) {
           player.pauseVideo();
              player.mute();
        }
       
    }); 
});
/*About section Video autoplay End*/


jQ('.nav_multi_level2 > ul li ul').hide();

jQ('.button.menu-rt.button').click(function(){
    jQ('.submenu-ac').toggleClass('submenu-ac');
    jQ('.active-submenu').toggleClass('active-submenu'); 
    jQ('ul.submenu, ul.submenu_inner').hide();
    jQ('.nav_multi_level2 > ul').toggleClass("main-ac");
});
  
jQ('.menu-ac-btn').click(function(){
    jQ('.main-ac').removeClass("main-ac");
});

jQ('.nav_multi_level2 > ul > li > ul.submenu li span').click(function(){
    jQ(this).prev().toggleClass('submenu-ac');
    jQ(this).parent().children('ul').slideToggle();
});

jQ('.nav_multi_level2 > ul > li > span.arrow').click(function(){

   if (jQ(this).prev().hasClass('active-submenu')){
     jQ(this).parent().children('ul.submenu').slideToggle();
     jQ(this).prev().toggleClass('active-submenu');
     
  }else {
    jQ('.nav_multi_level2 > ul > li > a').removeClass("active-submenu");   
    jQ('.nav_multi_level2 > ul > li > a').parent().children('ul.submenu').slideUp();     
    jQ(this).prev().addClass("active-submenu");
    jQ(this).parent().children('ul.submenu').slideDown();
  } 
  
});

if (jq(window).width() > 1025) {
   var path ="url(/wp-content/themes/dcw/images/Arrow.png)";
   
   jq( ".service-navigation" ).appendTo( ".main_nav_bar .service-dropdown" );
   jq("<span class='arr'> </span>").appendTo( ".eCommerce-nav li a" );
   jq("<span class='arr'> </span>").appendTo( ".megamenu-title a" );
   jq("<span class='arr'> </span>").appendTo( ".customdev-nav li a" );
   jq(".eCommerce-nav li a span.arr").css("background-image",path);
   jq(".megamenu-title a span.arr").css("background-image",path);
   jq(".customdev-nav li a span.arr").css("background-image",path);
}
jq('.light-gray-wraper .panel-body ul li').removeClass( 'service-dropdown' );
jq( '.left-side-list li a' ).on( 'click', function () {
  jq( '.left-side-list').find( 'li.orange-clr' ).removeClass( 'orange-clr' );
  jq( this ).parent( 'li' ).addClass( 'orange-clr' );
});
jq( ".service-dropdown" ).hover(function() {
  jq( "body" ).addClass("over-lay");
});
jq( ".service-dropdown" ).hover(function() {
  jq( ".sub-menu-overlay" ).appendTo("body");
});
jq(".service-dropdown").mouseleave(function(){
     jq( "body" ).removeClass("over-lay");
});
jq(".mobile_caret").click(function(){ 
  // jq("ul.nav.navbar-nav li ul.mobile_service_navigation").toggleClass("toggle");
  jq("ul.nav.navbar-nav li ul.mobile_service_navigation").slideToggle("slow");

})

/*portfolio inner page start*/
jq.noConflict();
    jq(document).ready(function() {
      if(jq(window).width()>1024){
        jq('#myContainer').multiscroll({
          //sectionsColor: ['#5ad0ff', '#000', '#ffdd00'],
            anchors: ['first1', 'first2', 'first3', 'first4' , 'first5' , 'first6' , 'first7'],
            menu: '#menu',
            navigation: true,
            normalScrollElements: '.normal, .spacer',
            responsiveWidth: 1025,
            css3: true,
            scrollOverflow: true,
                scrollOverflowKey: 'YWx2YXJvdHJpZ28uY29tXzBqTWMyTnliMnhzVDNabGNtWnNiM2M9QVU3',
                afterResponsive: function(){
                    console.log("after responsive...");
                },
                   navigationTooltips: ['first1', 'first2', 'first3', 'first4' , 'first5' , 'first6' , 'first7'],

            afterLoad: function(anchor, index){
              if(index==2 || index == 3){
                jq('#infoMenu li a').css('color', '#f2f2f2');
              }
          if(index==1){
            jq('#infoMenu li a').css('color', '#333');
          }
            }
        });
  }

});  
/*portfolio inner page End*/


