// JavaScript Document
var jQuer = jQuery.noConflict();
jQuery(function($){
			 var hash = window.location.hash;	
			 if(hash == ''){
				 $(".filter-content").hide();
				 $(".show_cat").show().animate(600);
				 $("#dropdownMenu1").html('Filter By Industry <span class="glyphicon glyphicon-th-large"></span>');
			 }else{				 
			 	var hashvalue = window.location.hash.substring(1); // document.URL.substr(document.URL.indexOf('#')+1);
			 	if(hashvalue == 'showall'){
					 $(".filter-content").hide();
					 $(".show_cat").show().animate(600);
					 $("#dropdownMenu1").html('Filter By Industry <span class="glyphicon glyphicon-th-large"></span>');
			 	}else{
			 	$(".show_cat").hide();
			 	$(".filter-content").html('<i class="fa fa-spinner fa-2x"></i>').show();
			 	$.ajax({
					url:"/wp-admin/admin-ajax.php",
					type:'POST',
					data:'action=portfolio_ajax_call&rel=' + hashvalue,
					success:function(data)
					 	{
						 if(data != 'null'){	
							var json_data = JSON.parse(data); 
							data1 = "";
							j=1;
							for( var i=0; i < json_data['portfolio'].length ; i++ ){
								if(json_data['portfolio'][i]['portfolio_image'] != null){
								data1 += '<div class="col-sm-4 col-xs-6"><div class="featured-box"><div class="overlay"><p class="overlaycase-txt"><a href="'+json_data['portfolio'][i]['link']+'">'+json_data['portfolio'][i]['content']+'</a></p><div class="hover-overlay clearfix"><a href="'+json_data['portfolio'][i]['link']+'"><i class="fa fa-eye fa-2x"></i><span>DIG <br>DEEP</span></a></div></div><a href="'+json_data['portfolio'][i]['portfolio_image']+'"><img src="'+json_data['portfolio'][i]['portfolio_image']+'" class="img-responsive" alt="" /></a><a class="full-box-link" href="'+json_data['portfolio'][i]['link']+'"></a></div></div>';
								
								}
							}
							$(".categorytermlist").removeClass('active');
							$(".filter-content").html(data1);
							
							$('.testimonial-category-filter').find('a[rel="'+hashvalue+'"]').addClass('active');
							var literm = 'Filter By Industry';
							$("ul.dropdown-menu-right").children().each(function(){ if($(this).children().attr('rel') == hashvalue){ literm = $(this).children().text(); } });
							$("#dropdownMenu1").html(literm+'<span class="glyphicon glyphicon-th-large"></span>');
							$(".filter-content").show();
					 	}else{
							$(".termslist").removeClass('active');
							$(".filter-content").html('<div class="well well-sm">No items found</div>');
					 	}
						},
						error:function(){
							/*alert("Something went wiered");*/ 
							return false;
						} 
					});
			 	}
			 }
	});