<?php
/*
 Template Name: Sitemap Page
*/
get_header(); 
?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url');?>/img/heading_icon_team.png"></div>

<section class="light-gray-wraper sitemap mtop30">
        <div class="container">
        	<div class="row mtop40">
            	<div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Company</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'topmenu' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Services</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'services' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Portfolio</div>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'work' )); ?>
                      </div>
                    </div>
                </div><!-- /col-md-4 -->
                
                                
                <div class="clearfix"></div>
                
               <!--  <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">DCW Social</div>
                      <div class="panel-body">
                        <?php //wp_nav_menu( array('menu' => 'Resources' )); ?>
                      </div>
                    </div>
                </div> --><!-- /col-md-4 -->
                <div class="col-sm-4">
                	<div class="panel panel-default">
                      <div class="panel-heading">Resources</div>
                      <?php
                    $country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
                     //print_r($country_cod);
                    if ($country_cod == "IN")
                    {
                    ?>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'aboutus' )); ?>
                      </div>
                    <?php }else{?>
                      <div class="panel-body">
                        <?php wp_nav_menu( array('menu' => 'Resources US' )); ?>
                      </div>
                    <?php }?>
                    </div>
                </div><!-- /col-md-4 -->
                
                
            </div><!-- /row -->
           
    </div>
</section>
       

<?php get_footer(); ?>