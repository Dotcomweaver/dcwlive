<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
	$post_id = get_the_ID();
	$tags = wp_get_post_tags($post_id);
?>
<div class="row">
<div class="vline"></div>
<div class="clearfix"></div>
<br /> 
    <div class="col-sm-9">
    <div class="blog-box-new">                
	
        
    <?php if ( is_search() ) :?>
        <div class="info">
        	<?php 
             the_excerpt(); ?>
         </div>
    <?php 
	else: ?>

        	
            <h2><?php echo the_title();?></h2>
        	<div class="single_blogimg"><?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail();
            }?></div>

             <?php 
            if ( comments_open() && ! post_password_required() ) :
            if ( 'post' == get_post_type() ) :?>
                <div class="blog_meta_data row">
                    <div class="col-sm-6">by <?php the_author();?></div>
                    <div class="col-sm-6 text-right">
                   <?php 
            //query_posts( 'posts_per_page=6&orderby=rand' );
                $sposttags = get_the_tags($post_id);
            if($sposttags): ?>
                <?php 
                $i= 0;
                    foreach($sposttags as $stags){
                
             if($i>0){
                echo '|';
             }
             echo '<a href="' . get_tag_link ($stags->term_id) . '" rel="tag">' . $stags->name . '  </a>';
                $i++;
                 } ?> |
            <?php endif; ?>  <?php the_date('M j, Y'); ?></div>
                </div>
            <?php
            endif;
            endif; ?>
           <?php the_content(); ?>
           <hr />
             <?php 

            $file = get_field('newsletter_url');

            if( $file ): ?>
                <div class="newsletter_listbtn">
                <a href="<?php echo $file; ?>" target="_blank" class="btn btn-default dcworng_btn">View Newsletter</a>
                </div>
            <?php endif; ?>
       
            <div class="post_social_share text-right">
                <?php dynamic_sidebar('share');?>
                 <span> Share the article </span> 
                <!-- <ul class="social_icons">
                    <li> <p>Share the article</p> </li>
                    <li> <a href="https://www.facebook.com/dotcomweavers" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                    <li> <a href="http://twitter.com/Dotcomweavers" target="_blank"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                    <li> <a href="https://www.instagram.com/dotcomweavers/" target="_blank"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                    <li> <a href="https://www.pinterest.com/dotcomweaversnj/" target="_blank" class="pinterest"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li>
                    <li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                    <!-- <li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </a> </li>
                   <li> <a href="mailto:type email address here?subject=I wanted to share this post with you from <?php// bloginfo('name'); ?>&body=<?php //the_title('','',true); ?>&#32;&#32;<?php// the_permalink(); ?>" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li> -->
                 <!--  <li><?php// echo do_shortcode('[email_link]');?></li>
                </ul>  -->
            </div>
         
    <?php endif; 
	//wp_reset_postdata();?>
    	<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<div class="addthis_sharing_toolbox"></div>
        <div class="post-tags">
            
        </div>
                <div class="single-post-comments row">
            <div class="col-sm-12">
            <?php
                wp_reset_query();
                if ( comments_open()) :
            ?>
            <?php 
                    comments_template( 'comments.php', true ); 
            ?>
            <?php 
                endif; 
            ?>
           <a href="/blog/" class="back_article"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to the Articles</a>
            </div>
        </div>
        <div class="clearfix"></div>
        
      </div>
      
    </div>
    <div class="col-sm-3 ind-blog-4">
        <div class="blog_sidebar">
          <?php dynamic_sidebar('search');?>

          <div class="clearfix"></div>
          <div class="sidebar_header">
              <h4> Categories </h4>
              <hr />
          </div>

            <ul id="tags-list">
                <?php
                    $tags = get_tags( array('orderby' => 'count', 'order' => 'DESC', 'number'=>20) );
                    foreach ( (array) $tags as $tag ) {
                    echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">' . $tag->name . '  </a></li>';
                    }
                ?>
                </li>
            </ul>

            <div class="clearfix"></div>

            <div class="related_posts <?php if($i != 1){ ?> mtop40 <?php } ?>">
                <div class="sidebar_header">
                    <h4>Related Post</h4>
                    <hr />
                </div>




                <?php 
                if($tags):
                    $first_tag = $tags[0]->term_id;
                    $args=array(
                    'tag__in' => array($first_tag),
                    'post__not_in' => array($post->ID),
                    'posts_per_page'=>3,
                    );
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) :
                    $i=1;?>

                    <ul>
                   <?php  while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <li>
                        <a href="<?php the_permalink(); ?>">
                        <?php
                            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            if(!empty($feat_image)):
                        ?>
                            <div class="video_img">
                                 <img src="<?php echo $feat_image; ?>" class="img-responsive" />
                            </div>
                        <?php
                            endif;
                        ?>
                            <?php the_title(); ?>
                        </a>
                        </li>
                    <?php
                    $i++;
                    endwhile;?>
                    </ul>

                  <?php  endif;
                    endif;
                    wp_reset_query();
                ?>    

            </div>
            
            
            <div class="clearfix"></div>
            
            <div class="sidebar_header">
                <h4>Instagram</h4>
                <hr />
            </div>

            <div class="clearfix"></div>
                <div class="instagram_gallery">
                    <?php do_shortcode('[wp-instagram-gallery]');?>
                </div>

            <div class="clearfix"></div>
            <br />

            <div class="clearfix"></div>

            <div class="blog_video_gallery">
        <div class="sidebar_header">
            <h4>Video Gallery</h4>
            <hr />
        </div>

        
    <?php
$queryObject = new WP_Query( 'post_type=video-testimonials&posts_per_page=3' );
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul>
    <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        $post_id = $post->ID;
        
$v_test_post_meta = json_decode(get_post_meta($post_id,'v_testimonials_post_meta', true));
if($v_test_post_meta->vt_url != '') {
                    
                    $y_img = get_ytube_video_code($v_test_post_meta->vt_url);
                    $te = $v_test_post_meta->vt_url;
                    $start = strpos($te,"v=")+2;
                    $end = strpos($te,"&");
                    if( $end == "" || $end == 0 ){
                        $fstr = substr($te,$start);
                    } else {
                        $length = $end-$start;
                        $fstr = substr($te,$start,$length);
                    }
                    //print_r($v_test_post_meta);
                     ?>
                        <li>
                     <?php if($v_test_post_meta->vt_url != ''):?>
                            <div class="video_img">
                            <img  alt="<?php the_title() ?> - Testimonial for NJ Web Design company" class="img-responsive" src="https://img.youtube.com/vi/<?php echo $y_img; ?>/0.jpg" >
                             <!-- Play Button -->
                            <a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/<?php echo trim($fstr); ?>" class="vt_play2" href="javascript:void();">
                                <span class="flaticon-play-button4"></span>
                            </a>
                            </div>
                        <?php endif; ?>
                            <!-- <a href="<?php //the_permalink(); ?>"> --><?php the_title(); ?><!-- </a> -->
                         </li>
    <?php
    }
}
    ?>
    </ul>
    
    <?php
}
?>
<div class="clearfix"></div>
        <a href="/video-testimonials/" class="btn btn-default">View All</a>
    </div>

            <div class="clearfix"></div>
            <br />

            <div class="sidebar_social">
                <div class="sidebar_header">
                    <h4>Social</h4>
                    <hr />
                </div>

                <ul class="social_icons">
                    <li> <a href="https://www.facebook.com/dotcomweavers" target="_blank" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                    <li> <a href="http://twitter.com/Dotcomweavers" target="_blank" class="twitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                    <li> <a href="https://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="linkedin"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                    <!-- <li> <a href="https://www.pinterest.com/dotcomweaversnj/" class="pinterest"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li> -->
                    <li> <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="google-plus"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                </ul>
            </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
<div>
<div>
<div class="clearfix"> </div>
<div class="portfolio_inner_footer text-center">
  <div class="container">
    <div class="col-sm-4">
      <?php previous_custom_post(); ?>
    </div>
    <div class="col-sm-4"><a href="/blog/">  BACK TO OUR BLOG</a></div>
    <div class="col-sm-4">
          <?php next_custom_post(); ?>
    </div>
  </div>
</div>
<br> <br> <br> 
<div class="triangleup">
    <a href="/contact-us/">
        We Also Make Videos
        <span class="sub_subtitle">View <b>Now</b></span>
    </a>
</div> 