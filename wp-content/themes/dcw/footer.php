<?php
/**
* The template for displaying the footer
*
* Contains the closing of the "site-content" div and all content after.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?> 
<!-- new letter start here -->

      <div class="email_templates">
         <div class="container">
            <div class="signup_templates_title">
               <!-- <p>PRACTICAL MONTHLY WEB STRATEGY TIPS </p> -->
               <h3><?php the_field('news_letter_heading', 7356);?></h3>
               <div class="email_block">
                  <?php dynamic_sidebar('newsletter');?>
               </div>
            </div>
         </div>
      </div>

<!-- new letter End here -->




 <footer class="sitefooter">
<!--
 <div class="badge-section">
   <div class="container">
     <div class="badge-logo">
    
       <ul>
         <li><img src="<?php //bloginfo('template_url');?>/images/magento_badge1.png"></li>
         <li> <img src="<?php //bloginfo('template_url');?>/images/woo_badge1.png">  </li>
         <li><img src="<?php //bloginfo('template_url');?>/images/wordpress_badge1.png"></li>
         <li> <img src="<?php //bloginfo('template_url');?>/images/shopify_badge1.png"> </li>
         <li> <img src="<?php //bloginfo('template_url');?>/images/larave_badge1.png"></li>
         <li><img src="<?php //bloginfo('template_url');?>/images/android_badge1.png"></li>
       </ul>

     </div>
   </div>
 </div> -->
      
 <div class="footer_dcw">
         <div class="container">
            <div class="row row_footer">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="footer_sub_one">
                     <?php dynamic_sidebar('footerabout');?>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="footer_sub_two">
                     <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="contact_dcw">
                           <?php dynamic_sidebar('footergiveuscall');?>
                           <i class="material-icons">phone</i>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="contact_dcw">
                          <?php dynamic_sidebar('footerleaveanote');?>
                           <i class="material-icons">mail_outline</i>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-4 col-xs-4 contacts">
                        <div class="contact_dcw">
                           <?php dynamic_sidebar('footercomevisit');?>
                           <i class="material-icons">pin_drop</i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row_footer_base">
               <div class="col-md-8 col-sm-8 col-xs-6">
                  <div class="privacy_content">
                    <!--  <p>(C) 2018 DotcomWeavers <sup>TM</sup>  &nbsp; | &nbsp;  right &nbsp;  | &nbsp; Privacy &nbsp;  | &nbsp;  Sitemap</p> -->
                    <?php dynamic_sidebar('footercopyrights');?>                    
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-6">
                  <div class="footer_socila_icons">
          
                     <ul>
                        <?php dynamic_sidebar('footersociallinks');?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="sticky_footermsg">
Book your <a href="/contact-us/">FREE CONSULTATION</a> with us @<strong>Booth </strong>#540 @<strong>IRCE </strong>2018 in Chicago | June 5-8
</div>  
<?php if(!is_front_page() && !is_home()){ ?> 
<div class="progressCounter">0%</div>
<?php } ?>
      
 
    </footer>

   <?php wp_footer(); ?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/owl.theme.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/canvasdots.animation.css" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/icons.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/fontsawesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('template_url'); ?>/css/juicer_custom.css" rel="stylesheet">

<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Poppins:300,400,500,600,700" rel="stylesheet"> -->


<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/slick.min.js"></script>

<!-- <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script> -->
<script src="<?php bloginfo('template_url');?>/js/froogaloop2.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/custom.js"></script>
<script src="<?php bloginfo('template_url');?>/js/dotcom.js"></script>

<script src="<?php bloginfo('template_url');?>/js/modernizr.custom.97074.js"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.hoverdir.js"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.fittext.js"></script>
<script src="<?php bloginfo('template_url');?>/js/wow.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/creative.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.flexisel.js"></script>
<script src="<?php bloginfo('template_url');?>/js/dcw2018.js"></script>

<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function () {
size_li = j("#mylist>li").size();
x=24;
j('#mylist>li:lt('+x+')').show();
j('#loadMore').click(function () {
    j(this).addClass('hidden');
// x= (x+8 <= size_li) ? x+8 : size_li;
x=j('#rowcount').val();
j('#mylist>li:lt('+x+')').show();
});

j('#showLess').click(function () {
x=(x-8<0) ? 8 : x-8;
j('#mylist>li').not(':lt('+x+')').hide();
});
 
});

</script>

<script src="<?php bloginfo('template_url');?>/js/jquery.validate.min.js"></script>
<!-- <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script> -->
<script src="<?php bloginfo('template_url');?>/js/additional-methods.min.js"></script>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod != "IN")
{
?>
<a title="Real Time Web Analytics" href="http://clicky.com/100706936"><img alt="Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100706936); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100706936ns.gif" /></p></noscript>
<?php }?>
<script type="text/javascript">
    adroll_adv_id = "CLTHEUMRDJEQVKSWZOBGQU";
    adroll_pix_id = "W7ZSUWIXBREQRG4DAKUW43";
        (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
 <?php if(ICL_LANGUAGE_CODE=='en'){?>
           <div class="sticky_footermsg">
        Book your <a href="/contact-us/">FREE consultation</a> with us @<strong>Booth </strong>#540 @<strong>IRCE </strong>in Chicago
        </div>
         <?php }else{?>
          <div class="sticky_footermsg">
      احجز<a href="/contact-us/">استشارتك المجانية معنا</a>@<strong>Booth </strong>#540 @<strong>IRCE </strong>في شيكاغو
      </div>
          <?php }?>

<!-- begin olark code -->
<script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
olark.identify('8908-923-10-4123');</script>
<!-- end olark code -->
<script>
jQuery(document).ready(function(){
jQuery('.top_phone').click(function(){
_gaq.push(['_trackEvent','link','click','tel'])
})
});
</script>


<style>
  .showpop{background-color: #000;}
</style>
</body>
</html>
