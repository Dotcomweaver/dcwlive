<?php
/**
* The template for displaying the footer
*
* Contains the closing of the "site-content" div and all content after.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?>
<!-- footer start -->
    <footer class="sitefooter">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <h4>Quick Links</h4>
            <ul class="quick-links">
              <li><a href="/video-testimonials/">Video Gallery</a></li>
              <li><a href="/social-hub/">Social Hub</a></li>
              <li><a href="/case-studies/">Case Studies</a></li>
              <li><a href="/contact-us/">Request A Quote</a></li>
            </ul>
            <div class="clearfix"></div>
            
            <address>
             15 Farview Terrace<br/> Paramus NJ <br /> Tel: 201.880.6656
            </address>
            
            <div class="clearfix"></div>

            <div class="social-icons">
            <a href="http://www.facebook.com/dotcomweavers" target="_blank" class="s_facebook"><i class="fa fa-facebook"></i></a>
            <a href="http://twitter.com/Dotcomweavers" target="_blank" class="s_twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://plus.google.com/+Dotcomweavers" target="_blank" class="s_google-plus"><i class="fa fa-google-plus"></i></a>
            <a href="http://www.linkedin.com/company/dotcomweavers-inc" target="_blank" class="s_linkedin"><i class="fa fa-linkedin"></i></a>
            <a href="https://www.youtube.com/user/dotcomweavers" target="_blank" class="s_youtube"><i class="fa fa-youtube-square"></i></a>
            <a href="https://www.instagram.com/dotcomweavers/" target="_blank" class="s_instagram"><i class="fa fa-instagram"></i></a>
            </div>

          </div>

          <div class="col-md-3 col-sm-6">
            <h4>Recent Blogs</h4>
              
                     <?php
                                $args = array( 'posts_per_page' => 3, 'orderby' => 'ASC', );
                                $lastposts = get_posts( $args );
                                foreach ( $lastposts as $post ) :
                                  setup_postdata( $post ); ?>
                                     <ul class="recent-blogs">
                                      <li><a href="<?php the_permalink();?>"><?php echo wp_trim_words( get_the_title(), 10, '..');?><br /> <strong><?php echo get_the_date(); ?></strong> </a> </li>
                                       </ul>
                                                                       
                                  <?php 
                                  endforeach; 
                                wp_reset_postdata(); ?>
             
          </div>
            <div class="col-md-3 col-sm-6">
            <h4>Our Twitter World</h4>
           
                <?php dynamic_sidebar('twitter');?>
          
          </div>
          <?php //dynamic_sidebar('twitter');?>
          <div class="col-md-3 col-sm-6">
            <h4>Newsletter</h4>
            <div class="newsletter">
            <div class="lettercontent">
             <p>Join our mailing list to receive news and announcements</p>
              <?php dynamic_sidebar('newsletter');?>
              </div>
            </div>
            <div class="link-inline">
              <a href="/sitemap/"> Sitemap </a> | <a href="/privacy-policy/"> Privacy Policy </a>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <p class="reserve-policy">© 2016 All Rights Reserved.</p>
    </footer>
    <!-- footer ends -->
<?php wp_footer(); ?>
<!-- jQuery -->
<!-- Bootstrap Core JavaScript -->
<!-- <script src="<?php //bloginfo('template_url');?>/js/jquery.js"></script> -->
<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/custom.js"></script>
<script src="<?php bloginfo('template_url');?>/js/modernizr.custom.97074.js"></script>
<script src="<?php bloginfo('template_url');?>/js/jquery.hoverdir.js"></script>


<!-- Plugin JavaScript -->

<!--<script src="<?php //bloginfo('template_url');?>/js/jquery.easing.min.js"></script>-->
<script src="<?php bloginfo('template_url');?>/js/jquery.fittext.js"></script>
<script src="<?php bloginfo('template_url');?>/js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php bloginfo('template_url');?>/js/creative.js"></script>

<script>
  
jQuery.noConflict();
jQuery( document ).ready(function() {
    
   jQuery( ".service_icons" ).hover(function() {
       ///alert('test');
        var replaceval = jQuery(this).children('img').attr("rel");
        var pfimage = jQuery(this).children('img').attr("src");
        jQuery(this).children('img').attr("rel",pfimage);
        jQuery(this).children('img').attr("src",replaceval);
   });

}); 

<-----------------Social icons----------------->

var jq = jQuery.noConflict();
 jq(document).ready(function($){
 
      $(window).load(function() { 
       $(".test1").css("display","block");
         $(".test2").css("display","none");
        })
     
      $(window).scroll(function() { // check if scroll event happened
        //console.log('hai'+$(document).scrollTop())
        if (($(document).scrollTop() > 1300)  &&  ($(document).scrollTop() < 1700))  {
        //alert('hai'); // check if user scrolled more than 50 from top of the browser window
        //console.log('hai'+$(document).scrollTop())
          $(".test2").css("display","block"); 
          $(".test1").css("display","none");
          // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
      
        }
     else {
        $(".test2").css("display","none");
       $(".test1").css("display","block");
        }
      });
    });
 
</script>


<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function () {
size_li = j("#mylist>li").size();
x=24;
j('#mylist>li:lt('+x+')').show();
j('#loadMore').click(function () {
    j(this).addClass('hidden');
// x= (x+8 <= size_li) ? x+8 : size_li;
x=j('#rowcount').val();
j('#mylist>li:lt('+x+')').show();
});

j('#showLess').click(function () {
x=(x-8<0) ? 8 : x-8;
j('#mylist>li').not(':lt('+x+')').hide();
});
//alert("Hello5");  
/* var iframe = $('#home_video')[0];
var player = $f(iframe);
j("#play").on('click', function () {
//alert("hiiii");
// player.api('pause');

}); */
//alert("Hello"); 
});




</script>
 

<script>
var j = jQuery.noConflict();
j(document).ready(function() {
  /*j('.sidebar').addClass( "affix-top1" );
  j('.sidebar').stickySidebar({
    sidebarTopMargin: 100,
    footerThreshold: 100
  });*/

});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script src='<?php bloginfo('template_url');?>/js/snowfall.jquery.min.js'></script>

<script type="text/javascript">

              jQuery.noConflict();
              jQuery('document').ready(function()
             {  
			  jQuery.validator.addMethod(
			   
								"regex",
								function(value, element, regexp) 
								{
									if (regexp.constructor != RegExp)
										regexp = new RegExp(regexp);
									else if (regexp.global)
										regexp.lastIndex = 0;
									return this.optional(element) || regexp.test(value);
								},
								"Please check your input."
						);
 
			 
                     jQuery("#request-form,#request-form1").validate({
                         rules: {
                             "lead[full_name]": "required", 
                             //"lead[company_name]":"required", 
                              "lead[phone]": { 
                                  required:true, 
                                 // regex: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/,
                                },
                             "lead[email]": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                 //email: true
                             }, 
                         },
                         messages: {
                             "lead[full_name]": "Please enter your name", 
                             //"lead[company_name]": "Please enter your Company Name", 
                             "lead[phone]": "Please enter a valid Phone number", 
                              "lead[email]": "Please enter a valid email address"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     });  
                     jQuery("#register-form").validate({
                         rules: {
                             "lead[full_name]": "required", 
                            // "lead[company_name]":"required", 
                              "lead[phone]": { 
                                  required:true, 
                                 // regex: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/,
                                },
                             "lead[email]": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                 //email: true
                             }, 
                         },
                         messages: {
                             "lead[full_name]": "Please enter your name", 
                            // "lead[company_name]": "Please enter your Company Name", 
                             "lead[phone]": "Please enter a valid phone number", 
                              "lead[email]": "Please enter a valid email address"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     }); 

         });
</script>
<?php
$country_cod = $_SERVER["HTTP_CF_IPCOUNTRY"]; 
//print_r($country_cod);
if ($country_cod != "IN")
{
?>
<a title="Real Time Web Analytics" href="http://clicky.com/100706936"><img alt="Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100706936); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100706936ns.gif" /></p></noscript>
<?php }?>
<script type="text/javascript">
    adroll_adv_id = "CLTHEUMRDJEQVKSWZOBGQU";
    adroll_pix_id = "W7ZSUWIXBREQRG4DAKUW43";
    /* OPTIONAL: provide email to improve user identification */
    /* adroll_email = "username@example.com"; */
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
</body>
</html>
