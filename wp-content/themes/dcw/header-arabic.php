<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta property="og:image" content="<?php bloginfo('template_url');?>/img/brand-scroll.png">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<meta name="viewport" content="width=device-width">
<!-- <title><?php //wp_title(''); ?></title> -->
<meta name="google-site-verification" content="pSnSiXbkNpJkdbYZOjDBsDuC29A9AY2NXybXcTmLLIE" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/twentyeleven/images/favicon.ico">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<!-- Bootstrap Core CSS -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"      rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,700i" rel="stylesheet">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/dcw2018.css?ver=1.0.0" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/css/style.min.css?ver=1.0.0" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/creative.css?ver=1.0.0" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/services.css?ver=1.0.0" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/progress.css?ver=1.0.0" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/aos.css" type="text/css">

<?php wp_head(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/2953.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16800069-1', 'auto', {'name':'gaTracker'});
  ga('gaTracker.send', 'pageview');

</script>

</head>

<body>

    <script type="text/javascript">
      if(jQuery('header .google-adwords label').hasClass('error')){
        alert("has Class");
      };
    </script>
   <nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-nav-bar mbl-navs affix-top home-nav arabicnav">
  
<div class="topcallreq_sec">
  <div class="top_call">
      <a href="tel:8883156518" class="top_phone">CALL NOW</a>
  </div>
  <div class="top_rq">
    <span class="blue-txt-btn"><a href="/contact-us"> CONTACT US </a> </span>
  </div>
  <div class="clearfix"> </div>
</div>

<div class="navbar-header col-md-3 col-sm-3 col-xs-7">
  <button type="button" class="navbar-toggle collapsed bs-1-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1 ">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand page-scroll scrl-main dsk-logo" href="/ar" title="DCW"><img class="img-responsive" src="/wp-content/themes/dcw/images/brand2.png"></a>
  <a class="navbar-brand page-scroll mbl-logo" href="/ar" title="DCW"><img class="img-responsive" src="/wp-content/themes/dcw/img/brand.png"></a>
</div>
<div class="flip-container col-md-9 col-sm-6 hm-pg-flp">
  <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
   
    <div class="flipper">

      <div class="back">
        <span class="blue-txt-btn"><a href="/contact-us"> CONTACT US </a> </span>
      </div>
            <a href="/contact-us/" class="top_phone"><!-- <i class="fa fa-phone"> --> </i>888.315.6518 1+</a>
          </div>
    <ul class="nav navbar-nav"><li id="menu-item-7515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7515"><a href="/work/">عمل</a>
<ul class="sub-menu">
  <li id="menu-item-7513" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7513"><a href="/work/">Portfolio</a></li>
  <li id="menu-item-7510" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7510"><a href="/white-papers/">Case Studies</a></li>
  <!-- <li id="menu-item-7511" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7511"><a href="/before-after/">Before &#038; After</a></li>
  <li id="menu-item-7512" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7512"><a href="/press-releases/">Press &#038; Awards</a></li>
  <li id="menu-item-7514" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7514"><a href="/video-testimonials/">Video Testimonials</a></li> -->
</ul>
</li>
<li id="menu-item-9324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-9324"><a href="/ecommerce/">التجارة الإلكترونية</a>
<ul class="sub-menu">
  <!-- <li id="menu-item-7518" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7518"><a href="/b2b-solutions/">B2B Solutions</a></li> -->
  <li id="menu-item-7519" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7519"><a href="/magento/">Magento Development</a>
 <!--  <ul class="sub-menu">
    <li id="menu-item-7520" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7520"><a href="/magento-specialists/">Magento Specialists</a></li>
  </ul>
</li> -->
  <!-- <li id="menu-item-7523" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7523"><a href="/strategy-and-consultation/">Strategy and Consultation</a></li>
  <li id="menu-item-7522" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7522"><a href="/ecommerce-seo/">eCommerce SEO</a></li>
  <li id="menu-item-7521" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7521"><a href="/data-management/">Data Management</a></li> -->
</ul>
</li>
<li id="menu-item-9326" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9326"><a href="/custom-software/">تطبيقات مخصصة</a></li>
<li id="menu-item-7536" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7536"><a href="/services/">خدمات</a>
<ul class="sub-menu">
  <!-- <li id="menu-item-7534" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7534"><a href="/responsive-website-design/">Responsive Website Design</a></li>
  <li id="menu-item-7535" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7535"><a href="/search-engine-marketing/">Search Engine Marketing</a></li> -->
  <li id="menu-item-7533" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7533"><a href="/mobile-apps/">Mobile Applications</a></li>
  <!-- <li id="menu-item-7531" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7531"><a href="/content-development/">Content Development</a></li> -->
</ul>
</li>
<li id="menu-item-7524" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7524"><a href="/الشركة/">الشركة</a>
<ul class="sub-menu">
  <li id="menu-item-7525" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7525"><a href="/الشركة/">معلومات عنا</a></li>
  <!-- <li id="menu-item-7527" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7527"><a href="/our-process/">Our Process</a></li> -->
  <li id="menu-item-7528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7528"><a href="/why-dotcomweavers/">Team</a></li>
  <!-- <li id="menu-item-7526" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7526"><a href="/our-mascot/">Our Mascot</a></li>
  <li id="menu-item-7529" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7529"><a href="/your-personal-project-tracking-tool/">Journey Map</a></li>
  <li id="menu-item-7530" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7530"><a href="/work-with-us/">Work With Us</a></li> -->
</ul>
</li>
<li id="menu-item-7542" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7542"><a href="/blog/">الموارد</a>
<ul class="sub-menu">
  <li id="menu-item-7537" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7537"><a href="/blog/">Blog</a></li>
  <!-- <li id="menu-item-7538" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7538"><a href="/knowledge-base/">Knowledge Base</a></li>
  <li id="menu-item-7543" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7543"><a href="/%d8%a7%d9%84%d9%85%d9%88%d8%a7%d8%b1%d8%af/">FAQs</a></li> -->
  <li id="menu-item-7539" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7539"><a href="/white-papers/">White Papers</a></li>
</ul>
</li>
</ul>  </div>
    <!--  <div class="flipper">
    <div class="back">
      <span class="blue-txt-btn"><a href="/contact-us"> CONTACT US </a> </span>
    </div>
  </div> -->
</div>
<button type="button" class="menu-rt  col-xs-5 button">Menu <span class="fa fa-bars " aria-hidden="true"></span></button>
</nav>
  <?php if(is_page('10152')){?>
 <div class="home baner_withgooglereview homban_caroselsec">
<header>

      <div class="home_orangenewbaner1">
        <div class="homban_carousel11 arabic_home_baner">
          <div class="item hombanr1" style="background:url(/wp-content/themes/dcw/images/Home_Banner.jpg) no-repeat;background-size:cover;">
            <!-- <div  class="snow_fall"></div> -->
            <div class="container-fluid">

            <div class="col-sm-12">
              <div class="col-sm-5 desktop-img">
                <img src="<?php bloginfo('template_url'); ?>/images/SBEMockup.png">
              </div>
              <div class="col-sm-7">
              <div class="banner-right">    
                 <div class="suprchrge_head">
                  <div class="header-group">
           <h1 class="top-space">تصاميم ويب <span class="highlight">عالية التأثير</span>
تطوير استراتيجيات التسويق الرقمي</h1>
        </div>
                  
                
                </div>
            
                 <div class="banner-list-item">
                  <ul>
                   <li><a href="/ecommerce/">eCommerce</a></li>
                   <li><a href="/custom-software/">Custom Software</a></li>
                    <li><a href="/mobile-apps/" style="border-right: 0px solid transparent" >Mobile Apps</a></li>
                  </ul>
                </div>
               
                </div>
              </div>
            </div>

            </div>
          </div>
        
        </div>
        
        
       </div> 
        <div class="clearfix"></div>
           <div class="h_services project_div">
          <div class="container">
          
          <div class="row services_ar">
              <?php 
               if( have_rows('homepage_services') ): ?>
              <?php while( have_rows('homepage_services') ): the_row(); 
               $title = get_sub_field('text');
                $content = get_sub_field('content');
                 $link = get_sub_field('link');
              //print_r($banner);
               ?>
            <div class="col-sm-6 col-md-3">
                <div class="box">
                  <h4><?php echo $title;?></h4>
                  <p><?php echo $content;?></p>
                 <?php echo $link;?>
                </div> 
              </div>
             <?php endwhile; ?>
            <?php endif; ?>
            </div>
           
          </div>
        </div>
       </header>
     </div>
     <?php }?>
<!-- Head home banner with carousel sliders and google tringle review thing END-->



<!-- Mobile tablet slide navigation Start -->
  <div class="nav_multi_level2">
<ul>
<div class="slide_menubtn"><button type="button" class="menu-rt  col-xs-5 button"><span class="fa fa-bars " aria-hidden="true"></span></button>
</div>
<li>
  <a href="/work/">Work</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
        <li><a href="/work/">Portfolio</a></li>
        <li><a href="/whitepapers/">Case Studies</a></li>
        <!-- <li><a href="/before-after/">Before &amp; After</a></li>
        <li><a href="/tag/press-awards/" >Press Releases</a></li>
        <li><a href="/video-testimonials">Video Testimonials</a></li> -->
      </ul>
</li>
<li>
    <a href="/ecommerce/">eCommerce</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu">
         <!--  <li><a href="/b2b-solutions/">B2B Solutions</a></li> -->
          <li><a href="/magento/">Magento Development</a>
           <img src="<?php bloginfo('template_url');?>/images/menu_arrow.png">
              <!-- <ul class="submenu_inner">
                  <li><a href="/magento-specialists/">Magento Specialists</a></li>
              </ul> -->
            </li>
          <!-- <li><a href="/strategy-consultation/">Strategy &amp; Consultation</a></li>
          <li><a href="/ecommerce-seo/">eCommerce SEO</a></li>
          <li><a href="/data-management/">Data Management</a></li> -->
      </ul>
</li>
<li><a href="/custom-software/">Custom Applications</a></li>

<li class="services_link">
<a href="/services/"> Services</a>
<span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
    <ul class="submenu">
     <!--  <li><a href="/responsive-web-design">Responsive Website Design</a></li>
      <li><a href="/seo-sem">Search Engine Marketing</a></li> -->
      <li><a href="/mobile-apps/">Mobile Applications</a></li>
    <!--   <li><a href="/content-development/">Content Development</a></li> -->
      <!-- <li><a href="/additional-services/">Additional Services</a></li> -->
    </ul>
</li>

<li class="company_link"><a href="/الشركة/">Company</a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
    <ul class="submenu">
      <li><a href="/الشركة/">معلومات عنا</a></li>
      <!-- <li><a href="/our-process">Our Process</a></li>
      <li><a href="/partners">Partners</a></li> -->
      <li><a href="/why-dotcomweavers/">Team</a></li>
      <li><a href="/our-mascot/">Our Mascot</a></li>
      <!-- <li><a href="/journey-map/">Journey Map</a></li>
      <li><a href="/work-with-us/">Work With Us</a></li> -->
      <!-- <li><a href="/contact-us">Contact Us</a></li> -->
    </ul>
</li>
<li>
  <a href="/blog/">Resources </a>
    <span class="arrow"><img src="<?php bloginfo('template_url');?>/images/menu_arrow.png"></span>
      <ul class="submenu" style="display: block;">
          <li><a href="/blog/">Blog</a></li>
          <!-- <li><a href="/faq">FAQs</a></li>
          <li><a href="/kb">Knowledge Base</a></li> -->
          <li><a href="/whitepapers/">White Papers</a></li>
      </ul>
</li>


 
</ul>
</div>

</div>

<!-- Mobile tablet slide navigation End -->
<div class="modal fade" id="vt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog hm-video">
<div class="modal-content">
<div class="modal-body gray-modal">
<button type="button" class="close" id="play-button" data-dismiss="modal" aria-hidden="true" title="close">&times;</button>
<div class="videoWrapper">
</div>
</div>
</div>
</div>
</div>
<style>
@media only screen and (min-width:768px) and (max-width: 1024px){
.pattern-overlay iframe{top:0px !important;}	
header{height:auto;}
 }
@media(max-width:767px) {
.header-content-inner.hm_pg_cnt{top:30%;}header{height:auto;}
}

</style>

<script type="text/javascript">
   var jQ = jQuery.noConflict();
     jQ(document).ready(function(){
jQ('.nav_multi_level2 > ul li ul').hide();

jQ('.button.menu-rt.button').click(function(){
    jQ('.submenu-ac').toggleClass('submenu-ac');
    jQ('.active-submenu').toggleClass('active-submenu'); 
    jQ('ul.submenu, ul.submenu_inner').hide();
    jQ('.nav_multi_level2 > ul').toggleClass("main-ac");
  });
  
jQ('.menu-ac-btn').click(function(){
    jQ('.main-ac').removeClass("main-ac");
  });

jQ('.nav_multi_level2 > ul > li > ul.submenu li span').click(function(){
  jQ(this).prev().toggleClass('submenu-ac');
  jQ(this).parent().children('ul').slideToggle();
});

jQ('.nav_multi_level2 > ul > li > span.arrow').click(function(){

   if (jQ(this).prev().hasClass('active-submenu')){
   jQ(this).parent().children('ul.submenu').slideToggle();
   jQ(this).prev().toggleClass('active-submenu');
   
   }

    else {
  jQ('.nav_multi_level2 > ul > li > a').removeClass("active-submenu");   
  jQ('.nav_multi_level2 > ul > li > a').parent().children('ul.submenu').slideUp();     
  jQ(this).prev().addClass("active-submenu");
  jQ(this).parent().children('ul.submenu').slideDown();
  } 
  
  });

});
</script>

<script>
  var jQ = jQuery.noConflict();
// jQ(".down-arrow, .clickable-div").click(function(){
//          jQ('html, body').animate({
//         scrollTop: jQ(".project_div").offset().top
//     }, 1000);
//     })
  if (jQ(window).width() > 1024) {
   jQ(".home_orangenewbaner1").click(function(){
          jQ('html, body').animate({
         scrollTop: jQ(".project_div").offset().top
     }, 1000);
     })
}

jQ(".banner-list-item, li, a").click(function(){
  event.stopPropagation();

})


  jQ(document).ready(function() {
  jQ('.homban_carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    smartSpeed:1000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
    })
  })
 </script>