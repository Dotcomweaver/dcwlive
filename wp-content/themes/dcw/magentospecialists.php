<?php
/*
Template Name: magentospecialists
*/
get_header();?>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
 
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/images/Stamp.png"></div>
<div class="clearfix"></div>
<section class="devide magento_specialistspage">
<div class="container">
<div class="vline"></div>

<h2 class="subtitle">We Live and <span>Breathe Magento</span></h2>
<p class="aboutxt text-center">Magento’s versatility and power make it best in class among eCommerce platforms. After more than a decade of working with Magento, we know it inside and out. Its suite of 8,000+ marketing and merchandising extensions allow unlimited customization and control. With Magento, our talented team of Magento specialists, developers, project managers and designers can create a unique, SEO- and sales-driven website with the functionality your business requires. With dozens of successful Magento projects in our portfolio, we have the bandwidth and expertise to supercharge your online business.</p>

	
<div class="row meet_msds">
	<div class="sub_headsec">
		 <h3 class="subhead_title">Meet our Magento <span>Specialists and Developers</span></h3>
		 <p class="aboutxt text-center">The DotcomWeavers Magento specialists are stars in their field. Thanks to the knowledge and expertise of our Magento-certified design and development team, we are proud to offer our clients a complete range of Magento services. Here are the current Magento specialists at DotcomWeavers:</p>
	 </div>
			<?php if( have_rows('magento_team') ): ?>

				<?php while( have_rows('magento_team') ): the_row(); 
				$certification = get_sub_field('certification');
				$position = get_sub_field('position');
				$name = get_sub_field('name');
				$image = get_sub_field('image');
				?>
	<div class="col-sm-4">
		<div class="ms_profileimg">
			<img src="<?php echo $image['url']; ?>" class="img-responsive" alt="<?php echo $image['alt'] ?>">
		</div>
		<div class="ms_profile_info">
			<h4><?php echo $name;?></h4>
<p class="ms_position"><?php echo $position; ?> @ DotcomWeavers</p>
<i class="ms_certified"><?php echo $certification; ?></i>
		</div>
	</div>
<?php endwhile; ?>

		<?php endif; ?>

</div>

<div class="row benfit_megentosec">
	<div class="sub_headsec">
		 <h3 class="subhead_title">The Benefits of a <span>Team of Magento Specialists and Certified Developers</span></h3>
		 <p class="aboutxt text-center">Magento is among the best eCommerce shopping cart software available. While Magento isn’t the right solution for every client project, we do use it whenever possible. Among the many benefits of working with DotcomWeavers for your next Magento-based eCommerce project include:</p>
	 </div>
	 <?php if( have_rows('magento_specialists') ): ?>

				<?php while( have_rows('magento_specialists') ): the_row(); 
				$icon = get_sub_field('icon');
				$title = get_sub_field('title');
				$content = get_sub_field('content');
				$image = get_sub_field('image');
				?>
<div class="row benfit_mssec">

	<div class="col-sm-6">
					
				<div class="ms_image">
					<img src="<?php echo $image['url']; ?>" class="img-responsive" alt="<?php echo $image['alt'] ?>" />
				</div>	
		
	</div>
	<div class="col-sm-6">
		<img src="<?php echo $icon['url']; ?>" class="icons" alt="<?php echo $icon['alt'] ?>" >
		<h2 class="sub_subtitle orngtxt"><?php echo $title; ?></h2>
		<p class="text-center innovative txt"><?php echo $content; ?></p>
	</div>
	
	
</div>
	<?php endwhile; ?>

		<?php endif; ?>

</div>

	<div class="sub_headsec">
		 <h3 class="subhead_title">Why Choose <span>Magento?</span></h3>
		 <p class="aboutxt text-center">Check out this video to see DotcomWeavers’ CEO, Amit Bhaiya, discuss his favorite reasons for businesses to choose the Magento platform for their online store.</p>
	 </div>

	 <div class="video-testimonials whychus_magentovideo">
		 <div class="video_img">
			<img class="img-responsive" src="/wp-content/themes/dcw/images/why_magento_tumb.png">
			<a data-target="#vt_modal" data-toggle="modal" rel="https://www.youtube.com/embed/XBTqTnXoFrQ" class="hvideo vt_play" href="javascript:void();">
			<span class="icomoon-icon-play-2"></span>
			</a>
		</div>
	</div>


</div>
	<div class="clearfix"></div>

</section>
<!--.row-->
<?php
	// calling the sidebar.php
	//get_sidebar();
    // calling footer.php
    get_footer();
?>

