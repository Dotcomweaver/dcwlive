<?php
/*
Template Name: blog
*/
get_header();?>
<!-- blog page main banner starts -->
<div class="blog_banner" style="background : url(<?php bloginfo('template_url'); ?>/images/BlogBanner-blog.jpg) center no-repeat;background-size: cover;">
    <div class="container">
      <?php
          $args = array(
              'posts_per_page' => 1,
              'meta_key' => 'meta-checkbox',
              'orderby' => 'date',
              'order' => 'ASC',
              'meta_value' => 'yes'
          );
          $featured = new WP_Query($args);
   
          if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
            <h4><?php the_author(); ?></h4>
            <h1><?php the_title();?></h1>
            <a href="<?php the_permalink();?>">Read MOre</a>

          <?php endwhile;endif;
      ?>
    </div>
</div>
<!-- blog page main banner starts -->
<div class="blog_articles">
    <div class="container">
        <h3 class="blog1_title">blog</h3>
        <h1 class="blog2_title">Practical articles for success online</h1>
      
        <?php if( $terms = get_tags() ) : ?>
          <form>
        <select onchange="location = this.options[this.selectedIndex].value;">
            <option>Sort by</option>
            <option class="view_all" value="<?php bloginfo('url');?>/blog/">VIEW ALL</option>
            <?php foreach ( $terms as $term ) :?>
            <option value="<?php bloginfo('url');?>/tag/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></option>
          <?php endforeach; ?>
        </select>​
        </form>
        <?php endif; ?>

        <div id="response">
          <div class="blog_pagination">
            <?php if(function_exists('wp_paginate')) {
              wp_paginate();
              } 
            ?>
          </div>
        </div>
        <div class="parent-mainlistitems">
        <?php 

          $wp_query = new WP_Query();
          $wp_query->query('posts_per_page=14' . '&paged='.$paged);
          $pageee = get_query_var('paged');if($pageee==0){$pageee=1;}
          $postCount = 14*($pageee-1)+1;
          $quoteCountval = (2*$pageee)-1;
          while ($wp_query->have_posts()) : $wp_query->the_post(); 
          

          
          ?>
          
          <?php //print_r($wp_query); ?>
                
          <div class="blogged_sub mainlistitems">
              
              <div class="col-sm-3 col-xs-3">
                  <?php $gettagID = get_the_tags( $post->ID );
                      $trmid = $gettagID[0]->term_id;
                      $color =  get_term_meta( $trmid );
                      $cvalue = $color[color][0];
                  ?>
                  <div class="art_cl" style="border-top: 3px solid <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>">
                      <div class="sub_art">
                          <a class="main-anchor" href="<?php the_permalink();?>">
                          <?php if( get_field('author_name') ){ ?>
                          <span><?php the_field('author_name'); ?></span>
                          <?php }else{?>
                          <span>Dotcomweavers</span>
                          <?php }?>
                        <h4> <?php echo mb_strimwidth(get_the_title(), 0, 60, '...');?> </h4>
                          <p> 
                              <?php
                                  $project_cats = get_the_terms($post->ID, 'post_tag');
							  		if(!empty($project_cats)){
                                  $project_cats = array_values($project_cats);
                                  for($cat_count=0; $cat_count<count($project_cats); $cat_count++) {?>
                                    <?php echo $project_cats[$cat_count]->name;if ($cat_count<count($project_cats)-1){echo',';}
                                  }}
                              ?>
                          </p>
                   
                          <div class="overlay_blog" style="background: <?php if(!empty($cvalue)){echo $cvalue;}else{echo '#FF6633';}?>"></div>
                        </a>

                      </div>
                      <a href="<?php the_permalink();?>" class="sub-anchor"><i class="fas fa-long-arrow-alt-right"></i></a>
                  </div>
              </div>
              
              <?php 
                $postCountval = $postCount % 6;
                $postCounttt = $postCount % 42;
                if($postCountval == 0 && $postCounttt != 0){
                    $quoteCount = $quoteCountval % 10;
                    $quoteCountval++;
                    if($quoteCount == 0){$quoteCount = 10;}
                    $fieldNum = 'quote_text'.$quoteCount;
                  ?>
                  <div class="col-sm-3 col-xs-3">
                      <div class="art_quote">
                          <h6><?php the_field($fieldNum,518);?></h6>
                      </div>
                  </div>

              <?php } ?> 
              <?php  $postCount++;  ?>
          </div>
     <?php endwhile;  ?>
    <?php wp_reset_postdata(); ?>
     
    </div>
    </div>
    <div class="blog_pagination">
      <?php if(function_exists('wp_paginate')) {
        wp_paginate();
        } 
      ?>
    </div>
</div>

<div class="clearfix"></div>

  
<!-- blog main page ends here -->
<script type="text/javascript">
 
  jQuery(function($){
  $('#filter').submit(function(){
    var filter = $('#filter');
    $.ajax({
      url:filter.attr('action'),
      data:filter.serialize(), // form data
      type:filter.attr('method'), // POST
      beforeSend:function(xhr){
        filter.find('button').text('Processing...'); // changing the button label
      },
      success:function(data){
        filter.find('button').text('Apply filter'); // changing the button label back
        $('#response').html(data);
         $('.mainlistitems').css("display","none");
        // alert("open");

         // insert data
      }
    });
    return false;
  });
});


  // var jQ = jQuery.noConflict();
  // jQ(".view_all").click(function(){
  //   alert("clicked")
  // });
function blogFilter(value) {
var filter = jQuery('#filter');
var filterUrl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
    //alert(value);
    jQuery.ajax({
      url:filter.attr('action'), // filterUrl
      data:"categoryfilter="+value+"&action=myfilter", // form data  filter.serialize()
      type:filter.attr('method'), // POST
      
      success:function(data){
         // changing the button label back
        jQuery('#response').html(data);
        jQuery('.parent-mainlistitems').css("display","none");
        // alert("open");

         // insert data
      }
    });
    return false;
}

</script>


<?php get_footer(); ?>
