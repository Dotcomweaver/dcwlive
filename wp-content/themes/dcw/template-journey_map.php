<?php
/*
Template Name: journey_map
*/
get_header();?>
<div class="work clearfix">
     <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
    
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_journey_map.png"></div>
<section class="light-gray-wraper journey-map mtop40">
    	<div class="journey-mapimg-div container"><img src="/wp-content/themes/dcw/images/journeymap.jpg" class="img-responsive center-block" alt="journey-map" /></div>
</section>

<div class="triangleup blog_traingle"><a href="/work-with-us/">
Want To Work With Us?
<span class="sub_subtitle">Let Us Know</span></a></div>

<?php get_footer(); ?>
