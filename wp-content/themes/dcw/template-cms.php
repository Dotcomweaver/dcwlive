<?php
/*
Template Name: CMS
*/
get_header();?>
<div class="ecommerce clearfix">
			<div class="header-content">
				<div class="header-content-inner">
				 <h1>CMS</h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/client-cion.png"></div>

<section class="light-gray-wraper divide faq_cms">
<div class="container">
<div class="row">
<div class="col-md-12">
<b>What is a Content Management System?</b>

A content management system (CMS), such as <a title="Wordpress" href="http://wordpress.org/" target="_blank">Wordpress</a> or <a title="Joomla" href="http://www.joomla.org/" target="_blank">Joomla</a>, gives you comprehensive control over all the text, design, documents, multimedia and other functional elements of your website. Use your CMS to manage each individual's privileges-what parts of the site they are allowed to modify and the user interface they use to do so. Our team of web developers in New Jersey can implement a powerful, completely automated CMS that's fully integrated within the structure of your website and transparent for your users.

<b>What can a CMS do for you?</b>

Once a CMS is installed on your server and implemented site-wide, you'll find that it makes it simple for you and anyone else you designate to modify the site, even if you have no specialized web development skills. It can save your webmaster weeks of development time on an ongoing basis, and could allow you to run your site without a web expert on staff.

</div>
<div class="col-md-12">
	<br>
<?php echo do_shortcode('[jumptofolio]');?>
</div>
<div class="col-md-12">
<b>Find out more</b>

Controlling your website content is key to operational efficiency and vital to the ongoing success of your online business. To find out more about how our web developers can help your company get ahead on the web, contact <a href="/contact-us/">Dotcomweavers</a> today.
</div>
</div>
</div>
</section>


 
<?php get_footer(); ?>
