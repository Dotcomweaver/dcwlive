<?php
/*
Template Name: process
*/
get_header();?>
<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/images/Your-journey.png"></div>
<?php 
if ( have_posts() ) {
    while ( have_posts() ) {
        the_post(); 
        the_content();
    } // end while
} // end if
?>

<script type="text/javascript">
var jQ = jQuery.noConflict();
jQ(document).ready(function(){

    jQ(".discovery_clk").click(function() {
    jQ('html,body').animate({
        scrollTop: jQ(".ourprs_discovery_sec").offset().top - 90}, 1000);
    });
    jQ(".planning_clk").click(function() {
    jQ('html,body').animate({
        scrollTop: jQ(".ourprs_planning_sec").offset().top - 90}, 1000);
    });
    jQ(".design_clk").click(function() {
    jQ('html,body').animate({
        scrollTop: jQ(".ourprs_design_sec").offset().top - 90}, 1000);
    });
    jQ(".develop_clk").click(function() {
    jQ('html,body').animate({
        scrollTop: jQ(".ourprs_develop_sec").offset().top - 90}, 2000);
    });
    jQ(".testing_clk").click(function() {
    jQ('html,body').animate({
        scrollTop: jQ(".ourprs_testing_sec").offset().top - 90}, 2000);
    });
    jQ(".launch_clk").click(function() {
    jQ('html,body').animate({
        scrollTop: jQ(".ourprs_launch_sec").offset().top -90}, 2000);
    });

    
  })
</script>

<?php get_footer(); ?>
