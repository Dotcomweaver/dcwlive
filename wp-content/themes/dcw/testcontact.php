<?php
/*
Template Name: testContact
*/
get_header();?>

<div class="work clearfix">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
	<div class="header-content">
		<div class="header-content-inner">
			<h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
			<p></p>
		</div>
	</div>
</div>
<div class="client-icon"><img class="img-responsive" src="<?php bloginfo('template_url')?>/img/heading_icon_contact_us.png"></div>
<section class="light-gray-wraper tlbg">
<div class="container">
    <div class="col-md-6 col-sm-6 cnt-hdng sm-bg1">
	<aside id="sidebar2" >
<div class="inside2">
<div class="" id="-example">
        <h2>Contact us</h2>
        <span>We are here to help your business grow.</span>
		<p><i class="fa fa-envelope"></i>info@dotcomweavers.com  <span></span>  <i class="fa fa-phone"> </i> 888.315.6518  <!--|  201.880.6656--></p>
     <form class="cnt-frm " method="post" action="https://www.pipelinedeals.com/web_lead">
    <input type="hidden" name="w2lid" value="87b462885a4d" />
    <input type="hidden" name="thank_you_page" value="http://dotcomweavers.staging.wpengine.com/thank-you/" />
    <!-- Basic Demographic Fields -->
	<div class="form-group has-feedback">
	 <input type="text" name="lead[full_name]" class="form-control" id="name" placeholder="Name*" value="" autocomplete="off" data-bv-field="first_name" required>
	</div>
	<div class="form-group">
    <input type="text" name="lead[company_name]"class="form-control"  placeholder="Company*" required>
	</div>
	<div class="form-group has-feedback">
    <input type="text" name="lead[phone]"class="form-control" placeholder="Phone*" required>
	</div>
	<div class="form-group">
    <input type="text" name="lead[email]"class="form-control"  placeholder="Email*" required>	</div>
    <div class="form-group">
    <input type="hidden" name="lead[work_country]"class="form-control"  placeholder="Country" value="<?php echo $_SERVER["HTTP_CF_IPCOUNTRY"]?>" >	</div>
	<div class="form-group">
    <input type="text" name="lead[summary]" class="form-control cmnts" placeholder="Comments">	</div>
<div class="form-group snd">
    <input type="submit" value="send"class="btn btn-warning col-sm-12 cpl-xs-12" /></div>
                    <?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?>
            <?php endif; // end sidebar widget area ?>
    </form>
	</div>	
</div>
</aside>	
	 </div>	
    <div class="col-md-6 col-sm-6 sm-bg">
<div class="main_content" id="content">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php 
        remove_filter ('the_content', 'wpautop');
        the_content();//get_template_part( 'content', 'page' ); ?>
        <?php //comments_template( '', true ); ?>
    	<?php endwhile; // end of the loop. ?>
		<p class="dm-text-footer">In hac</p>
	

</div>
    </div>
</div>	
</section>


<?php get_footer(); ?>
<style>
header {display: none;}
.total-footer{position: static;}
.footer-contact{margin-top: 0px;}

.body-locked{margin-bottom:0px;}
.client-icon img {margin: -49px auto;}
aside {
float: left;
max-width: 350px;
}
p.dm-text-footer {
    color: #EEEEEE;
}
#sidebar2.sticky {
float: none;
position: fixed;
top: 100px !important;
z-index: 6;
left: auto;
}
.inside2 {
    padding: 10px;
}
#sidebar2{}
</style>
<script>
	var $jq= jQuery.noConflict();
$jq(document).ready(function(){
			$jq('#sidebar2').stickySidebar({
				sidebarTopMargin: 20,
				footerThreshold: 100
			});
		
		});
		</script>
