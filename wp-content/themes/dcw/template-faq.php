<?php
/*
Template Name: faq
*/
get_header();?>
<div class="work clearfix">
			<div class="header-content">
				<div class="header-content-inner">
				 <h1><?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
     <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
</div>
<!-- Inner page first title and description starts here -->
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/img/heading_icon_faq.png"></div>
<section class="light-gray-wraper divide gryclr faqspge ">
<div class="container">
<div class="row">
<div class="col-md-12">
<div role="tabpanel" class="faq-group row">
    <!-- Nav tabs -->
<ul class="nav nav-stacked nav-pills col-sm-3" role="tablist">
	<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">General Questions</a></li>
	<li role="presentation" class=""><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">eCommerce</a></li>
	<li role="presentation"><a href="#responsive" aria-controls="messages" role="tab" data-toggle="tab">Responsive</a></li>
	<li role="presentation"><a href="#webdev" aria-controls="settings" role="tab" data-toggle="tab">Web Development</a></li>
	<li role="presentation"><a href="#webapp" aria-controls="settings" role="tab" data-toggle="tab">Web Applications</a></li>
	<li role="presentation"><a href="#seosem" aria-controls="settings" role="tab" data-toggle="tab">SEO/SEM</a></li>
	<li role="presentation"><a href="#mobile" aria-controls="settings" role="tab" data-toggle="tab">Mobile Application</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content col-sm-9">
<div role="tabpanel" class="tab-pane fade in active" id="home">
<div class="panel-group accord-cust" id="accordion-general">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general0">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. How much does DotcomWeavers charge to build a website?</span>
                            </a></h4>
</div>
<div id="general0" class="panel-collapse collapse">
<div class="panel-body">Website pricing is a direct reflection of the amount of design and development necessary to accomplish your project's goals. Since every website has its own unique set of requirements, we cannot offer a single, standardized pricing model. For more information about our pricing or to get a price quote for your project, please <a title="Contact Us" href="/contact-us" class="inner">contact us</a>.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general1">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. How long does it take DotcomWeavers to build a website?</span>
                            </a></h4>
</div>
<div id="general1" class="panel-collapse collapse">
<div class="panel-body">Website delivery time depends on several factors including project complexity, amount of custom development, client revisions, and more. A simple brochure website can take as little as 3 weeks while a more complicated <a href="/ecommerce" class="inner">ecommerce</a> site can take up to 8 weeks.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general2">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. Do you offer any warranties or guarantees?</span>
                            </a></h4>
</div>
<div id="general2" class="panel-collapse collapse">
<div class="panel-body">Yes, Dotcomweavers offers 1 month of free, comprehensive support after go-live. This includes fixing any "bugs" and minor graphical modifications.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general3">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. Can I edit the website myself?</span>
                            </a></h4>
</div>
<div id="general3" class="panel-collapse collapse">
<div class="panel-body">Yes. Most of DotcomWeavers' solutions come with a <a href="/content-management-systems" class="inner">Content Management System</a> (CMS) where any site administrator can update content and images.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general4">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. What are your maintenance fees after the site goes live?</span>
                            </a></h4>
</div>
<div id="general4" class="panel-collapse collapse">
<div class="panel-body">

Since most of our websites come with Content Management Systems, DotcomWeavers does not charge any maintenance fees after your site goes live. Your only ongoing expense for your website will be for web hosting and any software licensing (if necessary), which we will discuss with you during the project's development.

If you ever require DotcomWeavers for any additional development of site functionality, we can either provide a price quote for the project or bill you at our hourly rate.

</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general5">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">6. Will I own the site/code after it is designed and developed?</span>
                            </a></h4>
</div>
<div id="general5" class="panel-collapse collapse">
<div class="panel-body">Yes, all of our projects are performed as "works made for hire" meaning you will be the legal owner of your entire website.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general6">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">7. Can you host my website or is that something I would arrange myself?</span>
                            </a></h4>
</div>
<div id="general6" class="panel-collapse collapse">
<div class="panel-body">

While DotcomWeavers does not offer hosting directly*, we work with many of the popular third-party hosting providers including <a title="GoDaddy" href="http://www.godaddy.com/" class="inner">GoDaddy</a>, <a title="HostGator" href="http://www.hostgator.com/" class="inner">HostGator</a> and others. If your project has unique hosting requirements, we will work with you to find the best possible web hosting solution.

*In our experience, be wary of any webmaster that offers to host your site themselves. If you ever choose to switch developers or move hosting, they may try to "hold your site hostage"-meaning you'd have to start from scratch with your new developer instead of updating your existing site.

</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general7">
        <i class="fa fa-arrow-right"></i>
            <span class="ttl">8. Do you offer SEO or SEM?</span>
        </a>
</h4>
</div>
<div id="general7" class="panel-collapse collapse">
<div class="panel-body">Yes, DotcomWeavers has years of experience in <a title="SEO" href="/seo-sem" class="inner">online marketing</a> including <a href="/seo-sem" class="inner">Search Engine Optimization</a> (SEO) and Search Engine Marketing (SEM). We will work with you to define your web marketing goals and come up with a comprehensive strategy to help you achieve them.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general8">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">9. Is there a difference between web design and web development?</span>
                            </a></h4>
</div>
<div id="general8" class="panel-collapse collapse">
<div class="panel-body">Yes, web design refers to the "look and feel" of a website, while <a href="/web-development" class="inner">web development</a> is the actual behind-the-scenes coding that makes your website work. For example, you can design a contact form on a web page-but without developing the server code that processes the form information and generates an email to the site owner, the form does not have any function.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general9">
                            <i class="fa fa-arrow-right"></i>
                            <span class="ttl">10. Do your websites meet W3C Standards?</span>
                        </a></h4>
</div>
<div id="general9" class="panel-collapse collapse">
<div class="panel-body">Yes, depending on your site's requirements we can code to any W3C standard to avoid common coding errors and warnings.</div>
</div>
</div>
</div>
</div>

<div role="tabpanel" class="tab-pane fade in" id="home1">
<div class="panel-group accord-cust" id="accordion-general">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general110">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. As a new online retailer, how can I watch my spend and get the customization that sets my business   apart?</span>
                            </a></h4>
</div>
<div id="general110" class="panel-collapse collapse">
<div class="panel-body">Admittedly, you can never get exactly what you need to establish your successful online store right out of the box. Still, our combination of offerings helps you keep a lid on your costs, especially when you're starting out. We work with leading open-source partner companies and offer third-party extensions that will enhance your customers' end-to-end user experience. What's more, we can integrate best-of-breed enterprise platforms and adapt them to your specific business needs either from the get-go or as your business starts to grow.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general111">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. Will I be able to build out my website as my business grows?</span>
                            </a></h4>
</div>
<div id="general111" class="panel-collapse collapse">
<div class="panel-body">Absolutely! We build websites using scalable architecture. We provide you with a content management system (CMS) that is fully automated and integrated with the structure of your website to allow your site administrator or other employee with designated privileges to update content and images. Our CMS makes it easy to create new pages or delete obsolete ones, allowing addition or removal of items to your product catalog.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general112">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. As a small online retailer, how can I guarantee my customers' information will be secure at checkout?</span>
                            </a></h4>
</div>
<div id="general112" class="panel-collapse collapse">
<div class="panel-body">We integrate enterprise platform systems with your unique website to guarantee your customers' credit card and order data information is secure. In many cases, all credit card and transaction information is protected by the same level of security used by banks, a 256-bit SSL certificate. Our platforms accommodate PCI certification as well. What's more, in addition to developing websites that accept a variety of credit cards, our systems are interoperable with an array of payment gateways, which offer consumers and merchants the ability to safely and securely conduct business transactions online.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general113">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. How can I streamline the checkout process, and avoid costly mistakes?</span>
                            </a></h4>
</div>
<div id="general113" class="panel-collapse collapse">
<div class="panel-body">The efficiency of the checkout process can literally make or break an online purchase. Our websites include standard checkouts that ensure you have accurate information at your fingertips. This includes automated processes that can integrate shipping with major carriers and tax rate computation across states and countries. Plus, EDI integration with warehousing and inventory management and order management systems help to minimize out-of-stock mistakes and eliminate costly errors in product fulfillment. What's more, in many cases, our websites allow your customers to process multiple orders with the ease of one click.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general114">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. As my business grows, how can I personalize information and give perks to my premiere customers?</span>
                            </a></h4>
</div>
<div id="general114" class="panel-collapse collapse">
<div class="panel-body">

We can set up search filters that allow you to put customers into groups. We can also enable you to capture information and use it to target your repeat or volume customers with special offerings, money-saving discounts and tiered-pricing options.

</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general115">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">6. How can I set expectations for pricing and other fees?</span>
                            </a></h4>
</div>
<div id="general115" class="panel-collapse collapse">
<div class="panel-body">Typically, website pricing is contingent upon the amount of design and development required to fulfill your project goals. Those goals vary from project to project -- as do individual  retailer budgets. For these reasons, we do not offer a single standardized pricing model. But you can be sure at Dotcomweavers, we listen carefully and give thoughtful consideration to both your needs and how much you can or want to spend. Also keep in mind, following your launch date we provide one month of free, comprehensive support. This includes fixing system "bugs" and minor graphical modifications.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general116">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">7. Will my eCommerce website work on mobile sites?</span>
                            </a></h4>
</div>
<div id="general116" class="panel-collapse collapse">
<div class="panel-body">

As customers are looking for convenient and time-saving ways to shop,  mobile web sales are growing at a fast pace. To keep ahead of the curve, savvy online retailers are enabling their customers to experience optimal viewing across multiple platforms&mdash;from desktop, to mobile to tablet. When that is your objective, we use our expertise in responsive design to size and adapt images on your main site to mobile environments without compromising the user experience.

</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-general" href="#general117">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">8. Conversions are important, but does a low conversion rate signal 'game over'?</span>
                            </a></h4>
</div>
<div id="general117" class="panel-collapse collapse">
<div class="panel-body">Absolutely not. High conversion rates can turn more browsers into buyers, but there's more to the story. Remember, your website is iterative. We can provide you with analytics that help you measure your results to determine what in your marketing and/or site functionality is, or isn't, user friendly. Then you can test the waters to determine what will work better. As important, our value of seeing Dotcomweaver clients as partners results in our building long-term relationships with them. A website launch does not signal our work is done. Our clients typically choose to engage with us long after their site is launched to keep on top of changing trends and enhance their sites as their business grows.</div>
</div>
</div>


</div>
</div>


    
    
    
    


<div role="tabpanel" class="tab-pane fade" id="responsive">
<div class="panel-group accord-cust" id="accordion-responsive">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-responsive" href="#responsive0">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. How can I be sure that my website will display on all browsers?</span>
                            </a></h4>
</div>
<div id="responsive0" class="panel-collapse collapse">
<div class="panel-body">Browser compatibility is very important to growing your business. CSS, the base of responsive design, addresses this issue and allows your visitors to view your website with the precision of its design in most browsers&mdash;and, you're right, there are plenty of new ones. What's more, while some of the older browsers versions may be more challenging, our knowledge of web technology and our comprehensive testing during development enable us to work out any bugs.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-responsive" href="#responsive1">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. Our company website relies on photographs of varying sizes to showcase a diverse product line. How can I be sure the photographs will transpose accurately from wide-screen desktops to the smaller widths of tablets and mobile?</span>
                            </a></h4>
</div>
<div id="responsive1" class="panel-collapse collapse">
<div class="panel-body">We can help you to optimize your image content and help you downsize the resolution of your images so that they adapt to the size of the screen on which your customers view your site at any given time. The point is to optimize your images by preventing them from having a too-high resolution from the get-go. An approach recommended by many experts: Find a common ground. Design (images) for the smaller device and prepare to adjust up for larger screens.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-responsive" href="#responsive2">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. Does marketing become more difficult because responsive website design accommodates a range of screen sizes</span>
                            </a></h4>
</div>
<div id="responsive2" class="panel-collapse collapse">
<div class="panel-body">Many experts believe that marketing actually becomes simpler through a website developed with responsive website design. With responsive web design, you are driving people toward a single URL and a single site. There is no confusion over URL for either website search or email interaction because there is no ".m" or ".t." This makes your marketing efforts easier and less time-consuming for your customer.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-responsive" href="#responsive3">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. Creating a whole new platform seems like a monumental task. Is it possible to employ a responsive website by using a scalable approach?</span>
                            </a></h4>
</div>
<div id="responsive3" class="panel-collapse collapse">
<div class="panel-body">Yes, and, if you have a large site, some experts suggest this as a more manageable way to handle the transition. For example, you might consider creating a microsite as a base for your redesign or start with your most important pages and develop and expand from there. Combining your email template into your responsive website design foray is also a plus.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-responsive" href="#responsive4">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. Can I create a landing page and email templates with responsive website design and enable these two components to align with my newly created responsive website?</span>
                            </a></h4>
</div>
<div id="responsive4" class="panel-collapse collapse">
<div class="panel-body">Absolutely. In fact, increasingly experts are suggesting you use responsive design to create your landing page just as you do your website to engage users and ramp up conversions the moment they hit your site. Plus, responsive design definitely matters with email. In fact, research has shown, you will lose sales and subscriptions on devices with smaller screens. Consumers are "highly likely" to delete an email if they are unable to read it on their mobile device. This means you lose traffic, click-throughs and conversions.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-responsive" href="#responsive5">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">6. Creating a whole new platform by converting to responsive website design seems like a monumental project that costs lots of money and takes lots of time. How can I afford to spend time and money&mdash;especially when technology is changing at such a rapid pace?</span>
                            </a></h4>
</div>
<div id="responsive5" class="panel-collapse collapse">
<div class="panel-body">You're right. Research has shown that converting to a responsive website platform typically takes two to three times longer than it does to create a standalone site. And, the upfront costs are also typically higher. But ROI also typically follows when existing and new customers experience the convenience and consistency across all your sites. Plus, responsive website design now strengthens your base for the ongoing improvements you will require with the development of new screens and even more sophisticated access points. It will take only one release to execute each enhancement. That's a savings in time, manpower and money which also bolsters your bottom line.</div>
</div>
</div>
</div>
</div>
<div role="tabpanel" class="tab-pane fade" id="webdev">
<div class="panel-group accord-cust" id="accordion-webdev">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webdev" href="#webdev0">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. Will I be able to continually update the information on my website to keep it from looking stale?</span>
                            </a></h4>
</div>
<div id="webdev0" class="panel-collapse collapse">
<div class="panel-body">Our robust content management system makes it possible and easy for you to manage, tweak and update content regularly, keeping content both current and fresh.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webdev" href="#webdev1">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. How will I be able to keep my site dynamic?</span>
                            </a></h4>
</div>
<div id="webdev1" class="panel-collapse collapse">
<div class="panel-body">Our websites accommodate the interactive features that involve users, make your website adaptive and result in your website getting more hits.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webdev" href="#webdev2">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. My company offers a huge variety of products. How will I keep the interface simple to avoid confusing consumers?</span>
                            </a></h4>
</div>
<div id="webdev2" class="panel-collapse collapse">
<div class="panel-body">We build in efficient user controls to accommodate increased flexibility, and effectively use multiple screens to ensure that site visitors are able to see all choices and make selections easily. We also build in a filtering system that lets users choose by product, service or category.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webdev" href="#webdev3">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. How can I gauge the impact of my website on visitors?</span>
                            </a></h4>
</div>
<div id="webdev3" class="panel-collapse collapse">
<div class="panel-body">We incorporate usage tools, QuickBooks, Salesforce and FileMaker that provide audience data and reporting and allow you to evaluate the popularity of different pages, how long visitors stay on the site or a page and even the path with which they navigate through your site. We also integrate Google Analytics which supports features such as cross device and cross platform measurement and social reports. If you like, we can incorporate a "What's New" feature into our content management system that engages frequent users by allowing them to see what has changed since their last visit.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webdev" href="#webdev4">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. How much should I expect to pay for my website?</span>
                            </a></h4>
</div>
<div id="webdev4" class="panel-collapse collapse">
<div class="panel-body">The enormous range of website requirements&mdash;essentially whether you want a fully customized, unique website or open-source development&mdash;make it difficult to quote a flat rate for website development. Because we listen carefully to what you want to accomplish with your site, we can recommend a blend of offerings that will enable you to enhance your users' experiences at the same time that you control your costs &mdash;for example, leading open-source, third-party extensions and best-of-breed platforms combined with customized back-end feature. Come in and talk to us to research your best options in solution and cost.</div>
</div>
</div>
</div>
</div>
<div role="tabpanel" class="tab-pane fade" id="webapp">
<div class="panel-group accord-cust" id="accordion-webapp">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webapp" href="#webapp0">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. I started my business as a second and part-time career and early on employed apps that are siloed for the growth my business is experiencing today. Where do I go from here?</span>
                            </a></h4>
</div>
<div id="webapp0" class="panel-collapse collapse">
<div class="panel-body">We are accustomed to working with these situations. We have the capability to leverage newer serviced-based applications that will "speak with" your legacy systems. What's more, we can leverage these new service apps to integrate the functionalities of your current business requirements.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webapp" href="#webapp1">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. Can I integrate additional supplier outlets with their new product lines with the suppliers I am currently using?</span>
                            </a></h4>
</div>
<div id="webapp1" class="panel-collapse collapse">
<div class="panel-body">Absolutely. The web apps and platforms we use represent the latest in API technology development. This enables us to integrate parallel system request structures into a unified service that enables you to easily process customer requests.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webapp" href="#webapp2">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. My business processes incoming orders in varying formats: call centers, fax, email and online. Can I integrate all these order gateways?</span>
                            </a></h4>
</div>
<div id="webapp2" class="panel-collapse collapse">
<div class="panel-body">Absolutely. We can pattern multifunction channels into a unified order point message outlet.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webapp" href="#webapp3">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. I am combining some legacy apps with new technologies in an effort to better serve my customers. How do I know what's working and what's not?</span>
                            </a></h4>
</div>
<div id="webapp3" class="panel-collapse collapse">
<div class="panel-body">We can provide technology tools to capture metrics that highlight what pages attract the most traffic, how download time impacts user experience or what does or doesn't increase your average order volume (AOV) to name a few considerations. As a side note, our iterative rounds of design and client testing during development are a part of every site we produce to ensure you'll launch with the most efficient website for your business.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-webapp" href="#webapp4">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. I'm a start-up with limited funds. How can I develop a website from the best web apps that are out to meet customer needs and still be mindful of my budget??</span>
                            </a></h4>
</div>
<div id="webapp4" class="panel-collapse collapse">
<div class="panel-body">We frequently hear this concern and recommend starting off on a web application/development project informing us of your budget spend and limitations, along with your business requirements. We can then prioritize integration tasks and address your requirements in rank order.</div>
</div>
</div>
</div>
</div>
<div role="tabpanel" class="tab-pane fade" id="seosem">
<div class="panel-group accord-cust" id="accordion-seosem">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-seosem" href="#seosem0">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. I have heard that SEO for eCommerce websites differs from traditional SEO. Is this true, and, if so, why and how?</span>
                            </a></h4>
</div>
<div id="seosem0" class="panel-collapse collapse">
<div class="panel-body">Yes, that's true. SEO for eCommerce is more detailed than traditional SEO. Successful eCommerce SEO relies on many disciplines: conversion rate through optimization, analytics, web design and development preferably in eCommerce, economy, usability and user experience.  An understanding of buying behaviors, a knowledge of commerce and retail business functions, and social media marketing and communication are also essential. Because of these diverse requirements, experts recommend eRetailers turn to consultants or companies who have this depth and breadth of expertise.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-seosem" href="#seosem1">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. At what point in the development process should I expect to integrate SEO into the design process?</span>
                            </a></h4>
</div>
<div id="seosem1" class="panel-collapse collapse">
<div class="panel-body">Immediately.  SEO must be integrated into the business website early in the planning phase, before you even start working on your wireframes and design. Failing to carve out your SEO strategy on the front-end can cause unexpected problems when you're ready to launch or create obstacles to success later.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-seosem" href="#seosem2">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. How can I continue to manage SEO for new products when my product line is constantly changing, both seasonally and otherwise?</span>
                            </a></h4>
</div>
<div id="seosem2" class="panel-collapse collapse">
<div class="panel-body">This has to be handled carefully but is manageable with the right skill&mdash;another reason experts recommend that retailers turn to consultants and companies with SEO expertise. At DotcomWeavers, we understand that establishing good information architecture, website structure and internal link architecture at the outset are essential to rank new product pages well.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-seosem" href="#seosem3">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. If I use your services, or the services of another SEO professional, how long will it take me to get on Page 1 of Google?</span>
                            </a></h4>
</div>
<div id="seosem3" class="panel-collapse collapse">
<div class="panel-body">If you're initiating a paid Search Engine Marketing ad, you can be placed on the first page of Google within about 24 hours if the services firm uses Google AdWords. We do. An organic campaign, however, takes more time. Organic search involves natural listings. To produce these results, Google uses a series of metrics to ensure that your site is relevant. To have your site show here, you have to score well on that algorithmic test, and many variables are involved. To get a handle on your organic search potential, it is best to meet with DotcomWeavers' SEO team.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-seosem" href="#seosem4">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. For effective search, what is the best organization for my website?</span>
                            </a></h4>
</div>
<div id="seosem4" class="panel-collapse collapse">
<div class="panel-body">We subscribe to the sequence of home page, category then products. Next to the home page, category pages are the most popular and influential ones. Therefore, we treat category pages as individual home pages. We also believe in building deep links to product categories: guest blogging, content marketing, paid ads and social media work well in this hierarchy. This approach provides users with the best, most informative and fastest service.</div>
</div>
</div>
</div>
</div>
<div role="tabpanel" class="tab-pane fade" id="mobile">
<div class="panel-group accord-cust" id="accordion-mobile">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-mobile" href="#mobile0">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">1. I'm just starting my business and understand that mobile is a driving force in eCommerce. I want to spend start up money wisely, so if I initially invest in mobile may I eventually expand to a desktop and tablet version of my site?</span>
                            </a></h4>
</div>
<div id="mobile0" class="panel-collapse collapse">
<div class="panel-body">Without question. In fact, the uptick in mobile shopping usage is inspiring many of us in the development space to think mobile first, particularly with images, and design for these smaller devices, then fine-tune for larger screens. We are also mindful of overcrowding and eliminate details which will distract from your brand presentation. This also applies to content. Because we want your customer to be able to read the same content from one device to another, we advise breaking up text over multiple pages.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-mobile" href="#mobile1">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">2. My robust product line is dependent on an appealing display of photos to attract buyers. How can I reproduce this with limited mobile website real estate space?</span>
                            </a></h4>
</div>
<div id="mobile1" class="panel-collapse collapse">
<div class="panel-body">With mobile the online shopping powerhouse it is today, developers are well-versed on how to reproduce a visually robust product roster in a way that will attract customers. At DotcomWeavers, we have the know-how and leverage the frameworks like Sencha's ExtJS5, for example, and design with a grid that is conducive to the mobile format. We can help you downsize the resolution of your images so they adapt to the size of the screen on which your customers view your site at any given time. The point is to optimize your images by preventing them from having a too-high resolution from the get-go.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-mobile" href="#mobile2">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">3. Though mobile is growing, research shows that most buying, and therefore, most conversions occur on desktop sites. What's the point of adding mobile application development to my budget?</span>
                            </a></h4>
</div>
<div id="mobile2" class="panel-collapse collapse">
<div class="panel-body">Respectfully, don't be misled into believing that the actual point of purchase represents a conversion. Remember, buyers are doing a lot of research and comparison shopping via mobile, and the actual point of purchase may be the final stop on a buying journey that starts with mobile.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-mobile" href="#mobile3">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">4. My wife and I own a high-end clothing store in an upscale community. Is mobile just a trend or can we derive a benefit from adding a mobile application offering to our brick-and-mortar operation?</span>
                            </a></h4>
</div>
<div id="mobile3" class="panel-collapse collapse">
<div class="panel-body">You absolutely can derive a benefit from adding a mobile feature. For example, progressive brick-and-mortar stores will endorse geo-location based mobile beacons as the way to deliver digital and mobile couponing to keep the in-store shopping experience contemporary, not antiquated. In fact, we are seeing more examples of large, traditional retailers like Target turning to mobile as a chance to ratchet up their eCommerce channel.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-mobile" href="#mobile4">
                                <i class="fa fa-arrow-right"></i>
                                <span class="ttl">5. I understand that personalization is important to online shopping. How does mobile inform that trend?</span>
                            </a></h4>
</div>
<div id="mobile4" class="panel-collapse collapse">
<div class="panel-body">Personalized offers will be served to customers via their mobile devices&mdash;and this extends to what and where they're shopping. This is a big plus if you have a brick-and-mortar retail offering. The same goes for online shopping. To combat the deadly abandoned shopping cart, consumers will be retargeted on social media channels for products they put in their cart. This puts them one step closer to clicking the "buy now" button.</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="triangleup blog_traingle"><a href="/kb/">
Want Industry Insights?
<span class="sub_subtitle">Learn More</span></a></div>
 
<?php get_footer(); ?>
