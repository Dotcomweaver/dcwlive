<?php
/*
Template Name: whitepapers
*/
get_header();?>
<div class="work clearfix whitepapers_bann" style="background : url(<?php bloginfo('template_url'); ?>/images/Casestudy_banner.jpg) center no-repeat;background-size: cover;">
    <?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail('full');
}  ?>
			<div class="header-content">
				<div class="header-content-inner">
				 <h1> <?php $post_meta = get_post_meta($post->ID,'wpcf-bannercontent',true); echo $post_meta; ?></h1>
					<p></p>
				</div>
			</div>
</div>
<div class="client-icon"><img class="img-responsive" src="/wp-content/themes/dcw/images/whitepaperpage_icon.png"></div>
<section class="devide whitepapers_mainpage">
<div class="container">
	<div class="vline"></div>	
	<div class="row">
		<p class="aboutxt text-center col-md-10  col-md-offset-1 ">We work with clients across a wide range of industries and every new project increases our knowledge. 
		</br>As we learn, we want to share this valuable design, development, and marketing
information with everyone. In our White Papers section, we offer free reports and informational
articles on topics in eCommerce, SEO, and beyond to drive total business success.</p>
	</div>

	<div class="whitpp_colmuns">
		<?php           
                $wp_query = new WP_Query(
                            array('post_type' => 'whitepapers',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        );  
				?>
				<?php //$content = get_the_content();
    while ($wp_query->have_posts()) : $wp_query->the_post();
    $wplink = get_permalink( $id );
    $the_link = get_permalink();
    $title = get_field('title'); 
    $description = get_field('description');
	
   ?>
		<div class="col-sm-4">
			<a href="<?php echo $wplink;?>"><div class="whpprs_orngeimg_box">
				<div class="artcl_img">
					<img class="img-responsive" src=<?php echo the_post_thumbnail();?>
					<div class="wpp_orngoverlay">
						<?php echo $title; ?>
					</div>
					<!-- <div class="corner_flip">
						
					</div> -->
				</div>
				<div class="wpp_dcwlogo">
					<img class="img-responsive" src="/wp-content/themes/dcw/images/logo_gray.png" >
				</div></a>
			</div>
			<div class="wpp_cate_date">
				<span class="wpp_categry"><?php 
               $category = get_the_category( $post->ID );
               echo $category[0]->cat_name;?></span><span class="wpp_date"><?php echo get_the_date(); ?> </span>
			</div>
			<p class="wpp_artcltxt">
			<?php echo mb_strimwidth($description, 0, 250, '...');?>
			</p>
			<a href="<?php echo $wplink;?>" class="wpp_dwnbtn">Download</a>
			<div class="clearfix"></div>
		</div>
 <?php endwhile; ?>

		
	</div>
		
</div>
</div>
</section>
 
<?php get_footer(); ?>
